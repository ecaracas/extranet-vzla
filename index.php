<?php
ob_start('My_OB');
function My_OB($str, $flags)
{
	//remove UTF-8 BOM
	$str = preg_replace("/\xef\xbb\xbf/","",$str);

	return $str;
}
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
setlocale(LC_TIME, 'es_VE', 'es_VE.utf-8', 'es_VE.utf8');
date_default_timezone_set('America/Caracas');
// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',4);

require_once($yii);
Yii::createWebApplication($config)->run();
