<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>THE FACTORY HKA CORP - Extranet Panamá</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/plugins/bootstrap/css/bootstrap.min.css">
        <!-- Fonts  -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/simple-line-icons.css">
        <!-- Switchery -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/plugins/switchery/switchery.min.css">
        <!-- CSS Animate -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/animate.css">
        <!-- Custom styles for this theme -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/main.css">
        <!-- CSS STYLE -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/style.css">
        <script src="<?php echo Yii::app()->baseUrl ?>/js/vendor/jquery-1.11.1.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/js/vendor/jquery.browser.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/shortcuts/shortcuts.js"></script>
        <!-- Feature detection -->
        <script type="text/javascript"> 
            shortcut.add("backspace",function() { });
            shortcut.add("enter",function() { });
        </script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="assets/js/vendor/html5shiv.js"></script>
        <script src="assets/js/vendor/respond.min.js"></script>
        <![endif]-->
        <?php //Yii::app()->clientScript->scriptMap = array('jquery.js' => false,); ?>
    </head>
    <body>       
        <?php echo $content; ?>
        <div class="clear"></div>
        <!--Global JS -->        
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/navgoco/jquery.navgoco.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/pace/pace.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/fullscreen/jquery.fullscreen-min.js"></script>        
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/wizard/js/loader.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/js/src/app.js"></script>      
        <script src="<?php echo Yii::app()->baseUrl ?>/js/config.js"></script>
    </body>
</html>