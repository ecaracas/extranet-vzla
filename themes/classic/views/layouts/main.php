
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>THE FACTORY HKA CORP - Servicio tecnico Venezuela</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="http://www.thefactoryhka.com/ve/images/favicon.ico" type="image/x-icon">
        <!-- Bootstrap core CSS -->        
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/plugins/bootstrap/css/bootstrap.min.css">
        <!-- Fonts  -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/font-awesome.min.css">     
        <!-- Switchery -->       
        <!-- CSS Animate -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/animate.css">
        <!-- Custom styles for this theme -->

        <script src="<?php echo Yii::app()->baseUrl ?>/js/vendor/jquery-1.11.1.min.js"></script>

        <script src="<?php echo Yii::app()->baseUrl ?>/js/vendor/jquery.browser.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/js/jquery.validate.min.js"></script>  
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/main.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/plugins/pace/pace.css">
        <!-- CSS STYLE -->
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/onoffswitch/switch.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/plugins/messenger/css/messenger.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/plugins/messenger/css/messenger-theme-flat.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/plugins/daterangepicker/daterangepicker-bs3.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/jquery.filer.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/themes/jquery.filer-dragdropbox-theme.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/flickity.css">

        <link href="<?php echo Yii::app()->baseUrl ?>/js/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->baseUrl ?>/js/owl-carousel/owl.theme.css" rel="stylesheet">


        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/select2.min.css">

        <!-- Feature detection -->    
        <script>

            $(document).ready(function () {
                mensajes();
            });
            function mensajes() {
                var vector = $(".mensajes").val();

                if (vector != null) {
                    Messenger().post({
                        message: vector,
                        type: 'success',
                        hideAfter: 300,
                        showCloseButton: true
                    });
                }
            }


        </script>        
        <style type="text/css">
            .menu-tool{
                top: 0px !important;
            }
        </style>
    </head>
    <body>
        <section id="main-wrapper" class="theme-default">
            <header id="header">
                <!--logo start-->
                <div class="brand">
                    <a href="#" class="logo"><i> <img src="http://thefactoryhka.com/ve/images/favicon.ico"  width="22" height="17"/></i>EXTRANET <span>VENEZUELA</span></a>
                </div>
                <!--logo end-->
                <ul class="nav navbar-nav navbar-left">
                    <li class="toggle-navigation toggle-left">
                        <button class="sidebar-toggle" id="toggle-left">
                            <i class="fa fa-bars"></i>
                        </button>
                    </li>    
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li style="margin-left: -120px;" class=" dropdown profile hidden-xs">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="meta">
                                <span class="avatar">
                                    <?php if (Yii::app()->user->getState('photo') != NULL): ?>                      
                                        <img class="img-circle profile-image" src="<?php echo Yii::app()->baseUrl ?>/img/profile/<?php echo Yii::app()->user->getState('photo'); ?>" alt="profile">
                                    <?php elseif (Yii::app()->user->getState('photo') == NULL): ?>
                                        <img class="img-circle profile-image" src="<?php echo Yii::app()->baseUrl ?>/img/avatar.jpg" alt="profile">
                                    <?php endif; ?>
                                </span>                                
                                <span class="caret"></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight" role="menu">
                            <li>                                
                                <a href="<?php echo Yii::app()->createUrl('roles/usuarios/update/id/' . Yii::app()->user->id); ?>"><span class="icon"><i class="fa fa-user"></i></span>Mi Cuenta</a>                                
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('site/logout'); ?>"><span class="icon"><i class="fa fa-sign-out"></i></span>Cerrar Sesión</a>
                            </li>
                        </ul>
                    </li>
                    <li class="toggle-fullscreen hidden-xs">
                        <button type="button" class="btn btn-default expand" id="toggle-fullscreen">
                            <i class="fa fa-expand"></i>
                        </button>
                    </li>                   
                </ul>
            </header>
            <!--sidebar left start-->
            <aside class="sidebar sidebar-left">
                <div class="sidebar-profile">
                    <div class="avatar">
                        <?php if (Yii::app()->user->getState('photo')): ?>                      
                            <img class="img-circle profile-image" src="<?php echo Yii::app()->baseUrl ?>/img/profile/<?php echo Yii::app()->user->getState('photo'); ?>" alt="profile">
                        <?php else: ?>
                            <img class="img-circle profile-image" src="<?php echo Yii::app()->baseUrl ?>/img/profile/avatar.jpg" alt="profile">
                        <?php endif; ?>
                        <i class="on border-dark animated bounceIn"></i>                        
                    </div>
                    <div class="profile-body dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><h4><?php echo Yii::app()->params['empresa']; ?> <span class="caret"></span></h4></a>
                        <small style="color:#FFF; font-weight: bold;" class="title"><?php echo Yii::app()->user->getState('nombreusuario'); ?></small>
                        <br />
                        <small style="color:#FFF; font-weight: bold;" class="title"><?php echo Controller::nombrerol(); ?></small>

                    </div>
                </div>
                <nav>
                    <h5 class="sidebar-header">Menu</h5>
                    <ul class="nav nav-pills nav-stacked">
                        <?php
                        $menus = $this->ControlMenu('main');
                        foreach ($menus as $m) {
                             $url_index = explode('/', $m['menu_url']);
                            ?>
                            <li class="<?= strtoupper(Yii::app()->controller->id) == strtoupper(@$url_index[1]) ? 'open' : '' ?>">
                                <a href="<?php echo count($menus[$m['menu_id']]['data']) > 0 ? '#' : ($m['menu_url_tipo'] == 1 ? $m['menu_url'] : Yii::app()->createUrl($m['menu_url'])); ?>" title="<?php echo ucFirst($m['menu_descripcion']); ?>">
                                    <?php echo $m['menu_icono'] . ' ' . ucFirst($m['menu_descripcion']); ?>
                                </a>
                                <?php
                                if (count($menus[$m['menu_id']]['data']) > 0) {
                                    ?>
                                    <ul class="nav-sub">
                                        <?php foreach ($menus[$m['menu_id']]['data'] as $submenu) {
                                             $menu_ = explode('/',$submenu['menu_url']);
                                            ?>
                                            <li class="<?= strtoupper(Yii::app()->controller->id) == strtoupper(@$menu_[1]) ? 'active' : '' ?>">
                                                <a href="<?php echo $submenu['menu_url_tipo'] == 1 ? $submenu['menu_url'] : Yii::app()->createUrl($submenu['menu_url']); ?>" title="<?php echo ucFirst($submenu['menu_descripcion']); ?>">
                                                    <?php echo $submenu['menu_icono'] . ' ' . ucFirst($submenu['menu_descripcion']); ?>
                                                </a>
                                            </li>                                        
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                </nav>             
            <div class="version_" style="bottom: 17px;position: absolute;left: 43px;">
                <h6 style="color: #fff;">
                    <abbr title="Version">
                        <?= Yii::app()->params['version'] ?>
                    </abbr>
                </h6>
            </div>
            </aside>
            <!--sidebar left end-->
            <!--main content start-->
            <section class="main-content-wrapper">               
                <section style="margin-top: 0px !important;" id="main-content" class="animated fadeInUp">

                    <?php foreach (Yii::app()->user->getFlashes() as $key => $message) : ?>

                        <input type="hidden" class="mensajes" value="<?php echo $message; ?>">

                    <?php endforeach; ?>

                    <?php echo $content; ?>
                    <div class="clear"></div>

                </section>
            </section>
            <!--main content end-->
        </section>     

        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/navgoco/jquery.navgoco.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/pace/pace.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/fullscreen/jquery.fullscreen-min.js"></script>            
        <script src="<?php echo Yii::app()->baseUrl ?>/js/src/app.js"></script>      
        <script src="<?php echo Yii::app()->baseUrl ?>/js/jquery.validate.js"></script>      
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/shortcuts/shortcuts.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/js/switch/switch.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min.js"></script>       
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/messenger/js/messenger.min.js"></script>              
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/messenger/js/demo/location-sel.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/messenger/js/demo/theme-sel.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/messenger/js/demo/demo.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/messenger/js/demo/demo-messages.js"></script>  
        <script src="<?php echo Yii::app()->baseUrl ?>/js/jquery.ba-bbq.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/daterangepicker/moment.min.js"></script>
        <script src="<?php echo Yii::app()->baseUrl ?>/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/select2.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/propios.js"></script>

        <!--<script src="http://code.jquery.com/jquery-3.1.0.min.js"></script>-->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.filer.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/flickity.pkgd.min.js"></script>

        <script type="text/javascript">
            shortcut.add("backspace", function () { });
            shortcut.add("enter", function () { });
        </script>
        <?php
        Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {
        //use the same parameters that you had set in your widget else the datepicker will be refreshed by default
    $('#datepicker_for_due_date').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['es'],{'dateFormat':'yy-mm-dd'}));
    $('#datepicker_for_due_date_').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['es'],{'dateFormat':'yy-mm-dd'}));
}
");
        if (Yii::app()->params['IDGridview'] != NULL) {
            $rutaUrlGridviewBoxSwitch = Yii::app()->params['rutaUrlGridviewBoxSwitch'];
            $IDGridview = Yii::app()->params['IDGridview'];
            Yii::app()->clientScript->registerScript('LoadGridviewBoxSwitch', "GridviewBoxSwitch('{$IDGridview}','{$rutaUrlGridviewBoxSwitch}');");
            Yii::app()->clientScript->registerScript('UpdateGridviewBoxSwitch', " function UpdateGridviewBoxSwitch(id, data) { GridviewBoxSwitch('{$IDGridview}','{$rutaUrlGridviewBoxSwitch}'); }");
        }
        ?>       
        <!-- Form Modal -->
        <div class="modal fade modal-ajax-data" id="scrollingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow:auto;">
            <div class="modal-dialog" style="width: 794px;">
                <div class="modal-content" style=" height: 1122px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body" id="modal-body-content">

                    </div>                
                </div>
            </div>
        </div>
        <!-- End Form Modal -->



        <script src="<?php echo Yii::app()->baseUrl ?>/js/owl-carousel/owl.carousel.js"></script>
        
        <div id="Modalimagen" class="modal">

            <!-- The Close Button -->
            <span class="close close_modal" >&times;</span>
            <i class="fa fa-arrow-circle-o-left fa-4x btn_left" aria-hidden="true"></i>
            <div class="ctn_">
                <!-- Modal Content (The Image) -->

            </div>
            <i class="fa fa-arrow-circle-o-right fa-4x btn_right" aria-hidden="true"></i>
            <!-- Modal Caption (Image Text) -->
            <div id="caption"></div>
        </div>

    </body>
</html>
