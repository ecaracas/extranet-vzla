$(document).ready(function () {

    
    menu = function () {

        var act = $('.active').parents('li');
        act.addClass('open');
        $('.active').parents('ul').css({display: 'inline-block'});

    }

    menu();
    
    $(".js-example-basic-multiple").select2();


    $("#StOrdenServicio_telefono").keypress(function (e) {
      //  alert(e.which);
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       return false;
     }else{
         return true;
     }
    });
    


    var max_field = 5;
    var image_add = $(".image_add");
    x = 1;

    $("#add-picture_").click(function () {

        if (x < max_field) { //max input box allowed
            x++; //text box increment
            $(image_add).append('<div class="position_1"><input type="file" class="position_2" name="file_carga[]"/><a href="#" class="remove_field"> /Eliminar</a></div>'); //add input box
        }


    })


    $(image_add).on("click", ".remove_field", function () { //user click on remove text
        $(this).parent('div').remove();
        x--;
    })


    var estatus_historia = $('.visualiza_').val();
    
    if (estatus_historia == 2) {
        $('.visualiza_imag_').show();
    } else {
        $('.visualiza_imag_').hide();
    }
    
    if (estatus_historia == 3 ) {
        $('.visualiza_falla_').show();
    } else {
        $('.visualiza_falla_').hide();
    }

  if (estatus_historia == 5 ) {
        $('.visualiza_reparacion_').show();
    } else {
        $('.visualiza_reparacion_').hide();
    }


    $(".visualiza_").change(function () {
        var op = $(".visualiza_ option:selected").val();
        if (op == 2) {
            $('.visualiza_imag_').show();
        } else {
            $('.visualiza_imag_').hide();
        }
        if (op == 3) {
            $('.visualiza_falla_').show();
        } else {
            $('.visualiza_falla_').hide();
        }
        
         if (op == 5) {
            $('.visualiza_reparacion_').show();
        } else {
            $('.visualiza_reparacion_').hide();
        }

        $('.main-carousel').flickity({
            // options
            cellAlign: 'left',
            contain: true
        });

    });

    $('.item_click').on('click', function () {
        $('.ctn_').html(img_modal());
        $('.ctn_ img').hide();
        $('.ctn_ #img' + $(this).attr('id')).addClass('active').show();
        $('#Modalimagen').show();
    });

    $('.close_modal').on('click', function () {
        $('#Modalimagen').hide();
    });


    $('.btn_right').on('click', function () {
        $('.ctn_ .modal-content').hide();
        var $next = $('.ctn_ .active').removeClass('active').next();
        if ($next.length) {
            $next.addClass('active');
            $next.show();
        } else {
            $(".ctn_ img:first").addClass('active');
            $(".ctn_ img:first").show();
        }
    });

    $('.btn_left').on('click', function () {
        $('.ctn_ .modal-content').hide();
        var $last = $('.ctn_ .active').removeClass('active').prev();
        if ($last.length) {
            $last.addClass('active');
            $last.show();
        } else {
            $(".ctn_ img").last().addClass('active');
            $(".ctn_ img").last().show();
        }
    });
 

            



});

function img_modal() {
    var imagenes = '';
    var id = 1;
    $('.owl-item img').each(function () {
        if ($(this).attr('src')) {
            imagenes = imagenes + '<img class="modal-content" id="img' + id + '" src="' + $(this).attr('src') + '">';
        } else {
            imagenes = imagenes + '<img class="modal-content" id="img' + id + '" src="' + $(this).attr('data-src') + '">';

        }
        id++;
    });

    return imagenes;
}