
/* Script para mostrar la Cantidad de Registros en otro Lugar */
jQuery(function ($) {

    var valor = $('.summary').html();
    $('.paginationsize').html(valor);

    $('input[type=password]').addClass('form-control');
    $('input[type=text]').addClass('form-control');
    $('textarea').addClass('form-control');
    $('select').addClass('form-control');
    
    $('.form-control').attr('autocomplete','off');

    
    /*
     * Script que permite definir que caracteres son permitidos y cuales no
     * SCRIPT BEGIN
     */
    (function (a) {
        a.fn.validCampoFranz = function (b) {
            a(this).on({keypress: function (a) {
                    var c = a.which, d = a.keyCode, e = String.fromCharCode(c).toLowerCase(), f = b;
                    (-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || a.preventDefault()
                }})
        }
    })(jQuery);

    /* integer - Solo 0-9 */                        
    $('.input-integer').validCampoFranz('1234567890');

    /* integer - Solo 0-9 Space */                        
    $('.input-integer-space').validCampoFranz(' 1234567890');

    /* string - Solo 0-9 | A-Z */                         
    $('.input-alfanumerico').validCampoFranz('1234567890qwertyuiopasdfghjklñzxcvbnm');

    /* string - Solo 0-9 | A-Z */                         
    $('.input-alfanumerico-space').validCampoFranz(' 1234567890qwertyuiopasdfghjklñzxcvbnm');
    
    /* string - Solo A-Z */           
    $('.input-string').validCampoFranz('qwertyuiopasdfghjklñzxcvbnm');

    /* string - Solo A-Z */           
    $('.input-string-space').validCampoFranz(' qwertyuiopasdfghjklñzxcvbnm');

        /* string - Solo A-Z */           
    $('.input-namefile').validCampoFranz('qwertyuiopasdfghjklñzxcvbnm_-()+*');
    
    
     /* string - Solo 0-9 | A-Z */                         
    $('.input-email').validCampoFranz(' 1234567890qwertyuiopasdfghjklñzxcvbnm_.');
    
   /*
     * SCRIPT END
     */

$('.pagesize-select').tooltip();

//$('.tooltip-menssage').tooltip();

});




