 -- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 17-08-2016 a las 11:46:52
-- Versión del servidor: 5.5.47-cll
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `tfhka_site`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aplication_images`
--

CREATE TABLE IF NOT EXISTS `aplication_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `path` mediumtext NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `aplication_images`
--

INSERT INTO `aplication_images` (`id`, `name`, `path`, `active`) VALUES
(1, 'Abastos', 'aplicaciones/abastos.png', 1),
(2, 'Auto Lavados', 'aplicaciones/autolavado.png', 1),
(3, 'Bancos ', 'aplicaciones/bancos.png', 1),
(4, 'Boutique', 'aplicaciones/boutique.png', 1),
(5, 'Cafeterías', 'aplicaciones/cafeterias.png', 1),
(6, 'Carnicerias', 'aplicaciones/carnicerias.png', 1),
(7, 'Charcuterias', 'aplicaciones/charcuteria.png', 1),
(8, 'Farmacias', 'aplicaciones/farmacias.png', 1),
(9, 'Fruterias', 'aplicaciones/fruterias.png', 1),
(10, 'Heladerias', 'aplicaciones/heladerias.png', 1),
(11, 'HiperMercados', 'aplicaciones/hipermercados.png', 1),
(12, 'Kioskos', 'aplicaciones/kioskos.png', 1),
(13, 'Minitiendas', 'aplicaciones/minitiendas.png', 1),
(14, 'Panaderías', 'aplicaciones/panaderias.png', 1),
(15, 'Restaurantes', 'aplicaciones/restaurantes.png', 1),
(16, 'Loterías', 'aplicaciones/ventasdeloteria.png', 1),
(17, 'Delivery', 'aplicaciones/delivery.png', 1),
(18, 'Transporte', 'aplicaciones/transporte.png', 1),
(19, 'Boletería', 'aplicaciones/boleteria.png', 1),
(20, 'Tiendas', 'aplicaciones/tiendas.png', 1),
(21, 'Hospitales', 'aplicaciones/hospitales.png', 1),
(22, 'Hoteles', 'aplicaciones/hoteles.png', 1),
(23, 'Ferreterías', 'aplicaciones/ferreterias.png', 1),
(24, 'Aeropuestos', 'aplicaciones/aeropuerto.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `image_path` mediumtext COLLATE utf8_unicode_ci,
  `website` tinytext COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `brands`
--

INSERT INTO `brands` (`id`, `name`, `image_path`, `website`, `active`) VALUES
(1, 'Aclas', 'brands/images/aclas/52j_aclas.jpg', 'http://www.aclas.com', 1),
(2, 'Bixolon', 'brands/images/bixolon/11g_bixolon.jpg', 'http://www.bixolon.com', 1),
(3, 'Sonaray', 'brands/images/sonaray/68l_sonaray.png', 'http://www.sonarayled.com', 1),
(4, 'HKA', 'brands/images/hka/hka.jpg', 'none', 1),
(5, 'Dascom', 'brands/images/dascom/93m_dascom.jpg', 'http://www.dascomla.com/es', 1),
(6, 'Uniwell', 'brands/images/uniwell/50a_uniwell.jpg', 'http://www.uniwell.com', 1),
(7, 'OKI', 'brands/images/oki/92c_oki.jpg', 'http://www.oki.com', 1),
(8, 'Star Micronics', 'brands/images/starmicronics/14m_starmicronics.jpg', 'http://www.starmicronics.com', 1),
(9, 'Generico', NULL, 'none', 1),
(10, 'Pantum', 'brands/images/pantum/69x_pantum.jpg', 'http://www.pantum.com', 1),
(11, 'Ribao', 'brands/images/ribao/89p_ribao.jpg', 'http://www.ribaotechnology.com/', 1),
(12, 'Vellux', 'brands/images/vellux/45w_vellux.jpg', 'http://www.vwlluxsystems.com', 1),
(13, 'HKAPRINT ', 'brands/images/hkaprint/96b_hkaprint.png', 'http://hkaprint.com/', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands_country`
--

CREATE TABLE IF NOT EXISTS `brands_country` (
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `brand_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  KEY `fk_brands_country_countries1_idx` (`country_id`),
  KEY `fk_brands_country_brands1_idx` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `brands_country`
--

INSERT INTO `brands_country` (`country_id`, `brand_id`, `order`, `active`) VALUES
('ve', 1, 1, 1),
('ve', 2, 2, 1),
('all', 3, 3, 1),
('ve', 4, 1, 1),
('co', 5, 5, 1),
('mx', 5, 5, 1),
('pa', 5, 5, 1),
('pe', 5, 5, 1),
('us', 5, 5, 1),
('ve', 5, 5, 1),
('ve', 6, 6, 1),
('ve', 7, 7, 1),
('pa', 8, 8, 1),
('ve', 8, 8, 1),
('ve', 9, 9, 1),
('co', 10, 10, 1),
('ve', 10, 10, 1),
('ve', 11, 11, 1),
('ve', 12, 12, 1),
('co', 1, 1, 1),
('co', 2, 2, 1),
('co', 3, 3, 1),
('co', 4, 4, 1),
('co', 13, 6, 1),
('ve', 13, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contact_info`
--

CREATE TABLE IF NOT EXISTS `contact_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `coordinates` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` tinytext COLLATE utf8_unicode_ci,
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` tinytext COLLATE utf8_unicode_ci,
  `twitter` tinytext COLLATE utf8_unicode_ci,
  `youtube` tinytext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fk_contact_info_countries1_idx` (`country_id`),
  KEY `fk_contact_info_languages1_idx` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `contact_info`
--

INSERT INTO `contact_info` (`id`, `name`, `address`, `coordinates`, `phone_number`, `zipcode`, `country_id`, `language_id`, `facebook`, `twitter`, `youtube`) VALUES
(1, 'THE FACTORY HKA COLOMBIA', 'Calle 80, No. 69-70. Bodega 20. Bogotá – Colombia. ', '', '+571-311.30.63 ', NULL, 'co', 'es', NULL, NULL, NULL),
(2, 'THE FACTORY HKA C.A', 'Calle Callejón Gutiérrez, Edf. Riva, Local 2-1. Planta Baja. La california Norte. Caracas – Venezuela', '', '(+58.0212) MASTER 0212-2020811/  58-212-237.50.10 / 41.12/ 52.53 / 43.68/ 51.32 ', NULL, 've', 'es', NULL, NULL, NULL),
(3, 'The Factory HKA SA DE CV', 'Agricultura No. 88, Piso 2 Col. Escandón, Del Miguel Hidalgo. México – Distrito Federal', '', '+52 55 655.32.839 / 628.00.362 / 628.628.00.350 ', '11800', 'mx', 'es', NULL, NULL, NULL),
(4, 'THE FACTORY HKA DOMINICANA SRL', 'Calle 18 esq. Calle 20, Sector Villa Aura, Nave 3-A. Manoguayabo, Santo Domingo Oeste.', '', '(001)  809-567-1933. ', '', 'do', 'es', NULL, NULL, NULL),
(5, 'THE FACTORY HKA CORP', 'Ave. La Paz, entre calles 74A y 75. El Ingenio, Bethania. Ciudad de Panamá ', '', '+507 229.20.53 / 229.19.25 / 261.04.23 ', NULL, 'pa', 'es', NULL, NULL, NULL),
(6, 'THE FACTORY HKA PERU', 'Calle Tobago N° 265 Urb. Los Cedros de Villa – Chorrillos – Lima – Perú.', '', '+511 729.90.45 ', NULL, 'pe', 'es', NULL, NULL, NULL),
(7, 'THE FACTORY HKA BOLIVIA', 'Barrio Sirari, Calle Las Dalias Nro 7, Santa Cruz de la Sierra, Bolivia. \r\n', '', '+591 3 342.90.10 / 342.90.11 / 591 798.54.44 ', NULL, 'bo', 'es', NULL, NULL, NULL),
(8, 'THE FACTORY HKA', '1819 SE, St. Ft. Lauderdate, Florida – USA ', '', '+1 954 644.87.10', NULL, 'us', 'es', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `logo_path` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `flag_path` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(1) NOT NULL,
  `slogan` mediumtext COLLATE utf8_unicode_ci,
  `url` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `company_name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `coordinates` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` tinytext COLLATE utf8_unicode_ci,
  `facebook` tinytext COLLATE utf8_unicode_ci,
  `twitter` tinytext COLLATE utf8_unicode_ci,
  `youtube` tinytext COLLATE utf8_unicode_ci,
  `instagram` tinytext CHARACTER SET utf8,
  `script_seo` longtext COLLATE utf8_unicode_ci,
  `script_analytics` longtext COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `name`, `logo_path`, `flag_path`, `order`, `slogan`, `url`, `company_name`, `address`, `coordinates`, `phone_number`, `zipcode`, `facebook`, `twitter`, `youtube`, `instagram`, `script_seo`, `script_analytics`, `active`) VALUES
('all', 'The Factory HKA International', 'logos/tfhka-all.png', '', 0, NULL, '', 'The Factory HKA International', '1819 SE, St. Ft. Lauderdate, Florida &ndash; USA', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
('ar', 'The Factory HKA Argentina C.A', '', '', 10, NULL, 'thefactoryargentina', 'The Factory Argentina', 'Toronto', '', '000000', '0000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
('bo', 'The Factory HKA Bolivia', 'logos/tfhka-bo.jpg', 'banderas/bo.png', 3, NULL, 'http://www.thefactoryhka.com/bo', 'The Factory HKA Bolivia', 'Barrio Sirari, Calle Las Dalias Nro 7, Santa Cruz de la Sierra, Bolivia.', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3799.55948542955!2d-63.20353857268423!3d-17.765383747971406!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x93f1dd58712933e9%3A0x71792e6a186e0c0b!2sLas+Dalias%2C+Santa+Cruz+de+la+Sierra%2C+Bolivia!5e0!3m2!1ses!2sve!4v1443640752709" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '+591 3 342.90.10 / 342.90.11 /  591 798.54.44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
('co', 'The Factory HKA Colombia', 'logos/tfhka-co.png', 'banderas/co.png', 4, 'Representantes exclusivos para Colombia', 'http://www.thefactoryhka.com/co', 'The Factory HKA Colombia SAS. NIT: 900390126-6 © 2016', '<span>Calle 80, No. 69-70. Bodega 20. </span>Bogot&aacute; &ndash; Colombia.', '<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.ve/maps?q=4.688122,-74.08211&amp;num=1&amp;ie=UTF8&amp;ll=4.687999,-74.081631&amp;spn=0.003256,0.005284&amp;t=m&amp;z=14&amp;output=embed"></iframe>', '+571-311.30.63', NULL, 'https://www.facebook.com/The-Factory-HKA-Colombia-781707388521465/', 'https://twitter.com/thefactoryhkaco', '', 'https://www.instagram.com/thefactory.hka/', NULL, NULL, 1),
('do', 'The Factory HKA Dominicana SRL', 'logos/tfhka-do.png', 'banderas/rd.png', 5, 'Empresa L&iacute;der en Soluciones de Impresi&oacute;n Comercial a Nivel Internacional', 'http://www.thefactoryhka.com/rd', 'The Factory HKA Dominicana Srl.', 'Calle 18 esq. Calle 20, Sector Villa Aura, Nave 3-A. Manoguayabo, Santo Domingo Oeste.', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1892.4373983274659!2d-69.98754631056056!3d18.48303599687923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sve!4v1443197659667" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '(001)  809-567-1933 / (001) 567-2970', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
('mx', 'The Factory HKA M&eacute;xico SA de CV', 'logos/tfhka-mx.png', 'banderas/mx.png', 6, 'Empresa L&iacute;der en Soluciones de Impresi&oacute;n e Iluminaci&oacute,n Comercial a Nivel Internacional', 'http://www.thefactoryhka.com/mx', 'The Factory HKA M&eacute;xico SA de CV', 'Calle Zempoala #251. Piso 9 Col. Navarte, Delegaci&oacute;n Benito Ju&aacute;rez, C.P. 03020, Distrito Federal.', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d30106.11350119718!2d-99.177297!3d19.400988!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff6f461df30b%3A0xd7a74497e75a4bbb!2sAgricultura+88%2C+Escand%C3%B3n+I+Secc%2C+11800+Ciudad+de+M%C3%A9xico%2C+D.F.%2C+M%C3%A9xico!5e0!3m2!1ses-419!2s!4v1443734693685" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '+52 55 47520846', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
('pa', 'The Factory HKA Panam&aacute; Corp.', 'logos/tfhka-pa.png', 'banderas/pa.png', 7, 'Los l&iacute;deres del mercado en equipos fiscales', 'http://www.thefactoryhka.com/pa', 'The Factory HKA Corp.', 'Ave. La Paz, entre calles 74A y 75. El Ingenio, Bethania. Ciudad de Panam&aacute;', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31524.270477914026!2d-79.53956523074065!3d9.014971635951092!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zOcKwMDAnNTMuOSJOIDc5wrAzMScxOS40Ilc!5e0!3m2!1ses!2sve!4v1444392558012" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '+507 229.19.25 / 229.20.53 / 261.04.23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
('pe', 'The Factory HKA Per&uacute;', 'logos/tfhka-pe.png', 'banderas/pe.png', 8, 'Empresa L&iacute;der en Soluciones de Impresi&oacute;n e Iluminaci&oacute,n Comercial a Nivel Internacional', 'http://www.thefactoryhka.com/pe', 'The Factory HKA Per&uacute; C.A.', 'Calle Tobago N° 265 Urb. Los Cedros de Villa &ndash; Chorrillos &ndash; Lima &ndash; Per&uacute;.', '<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3901.3305259100866!2d-76.9737428334746!3d-12.0895100950954!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sve!4v1443642319011" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '+511 729.90.45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
('us', 'The Factory HKA USA', 'logos/tfhka-us.png', 'banderas/us.png', 1, 'Leading Company in Printing Solutions and Commercial Lighting at the International Level', 'http://www.thefactoryhka.com/usa', 'The Factory HKA USA', '1819 SE, 17 St. Ft. Lauderdate, Florida &ndash; USA', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3582.9348992137484!2d-80.1234645!3d26.1010437!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d9006af088ea43%3A0x8769e10746fbda49!2s1819+SE+17th+St%2C+Fort+Lauderdale%2C+FL+33316%2C+EE.+UU.!5e0!3m2!1ses!2sve!4v1443452810790" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '+1 954 644.87.10', '33316', NULL, NULL, NULL, NULL, NULL, NULL, 1),
('ve', 'The Factory HKA C.A.', 'logos/tfhka-ve.png', 'banderas/ve.png', 1, 'Los líderes del mercado en equipos fiscales', 'http://www.thefactoryhka.com/ve', 'The Factory HKA C.A.', 'Calle Callejón Gutiérrez, Edf. Riva, Local 2-1. Planta Baja. La california Norte. Caracas – Venezuela', '<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d2774.1210172263054!2d-66.82183302785627!3d10.48492849919076!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2sve!4v1443453494233" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '(+58.0212) MASTER 0212-2020811 / (+58 0212)237.50.10 / 237.41.12/ 237.52.53 / 237.43.68 / 237.51.32', '1070', NULL, NULL, NULL, NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edit_content`
--

CREATE TABLE IF NOT EXISTS `edit_content` (
  `id` int(12) NOT NULL,
  `date` date NOT NULL,
  `user_id` int(12) NOT NULL,
  `products_id` int(12) NOT NULL,
  `description` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `products_id_idx` (`products_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `featured_products_country`
--

CREATE TABLE IF NOT EXISTS `featured_products_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `featured_product_type_id` int(11) NOT NULL,
  `product_file_country_id` int(11) NOT NULL,
  `order` tinyint(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `url` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `url_type_id` int(11) NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_featured_products_country_countries1_idx` (`country_id`),
  KEY `fk_featured_products_country_products1_idx` (`product_id`),
  KEY `fk_featured_products_country_featured_products_type1_idx` (`featured_product_type_id`),
  KEY `fk_featured_products_country_products_files_countries1_idx` (`product_file_country_id`),
  KEY `fk_featured_products_country_url_types1_idx` (`url_type_id`),
  KEY `fk_featured_products_country_languages1_idx` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `featured_products_country`
--

INSERT INTO `featured_products_country` (`id`, `country_id`, `product_id`, `featured_product_type_id`, `product_file_country_id`, `order`, `active`, `url`, `url_type_id`, `language_id`, `path`) VALUES
(1, 'co', 0, 1, 0, 3, 1, '/productos/impresoras/spp-r310', 1, 'es', 'destacados/nuevo_lanzamiento_HKA_5803.jpg'),
(2, 'co', 0, 1, 1, 4, 1, '/productos/terminalesaio/pt05', 1, 'es', 'destacados/Producto_destacado_PT05.jpg'),
(3, 'co', 0, 1, 0, 2, 1, '/productos/balanzas/balanza-ls2', 1, 'es', 'destacados/Nuevo_lanzamiento_SPP-R310.jpg'),
(4, 'co', 0, 1, 0, 1, 1, '/facturacionElectronica/index', 1, 'es', '/destacados/Servicio_destacado_DFACTURE.jpg'),
(5, 'co', 0, 1, 0, 5, 1, '/productos/iluminacionled/SR-1030', 1, 'es', 'destacados/sonaray.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `featured_products_type`
--

CREATE TABLE IF NOT EXISTS `featured_products_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `featured_products_type`
--

INSERT INTO `featured_products_type` (`id`, `name`, `active`) VALUES
(1, 'nuevo lanzamiento HKA 5803', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` mediumtext CHARACTER SET utf8 NOT NULL,
  `path` mediumtext CHARACTER SET utf8 NOT NULL,
  `file_type_id` int(11) NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `version` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `order` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_files_languages1_idx` (`language_id`),
  KEY `fk_files_file_types1_idx` (`file_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=695 ;

--
-- Volcado de datos para la tabla `files`
--

INSERT INTO `files` (`id`, `name`, `path`, `file_type_id`, `language_id`, `version`, `active`, `order`) VALUES
(1, '58z_cr68afcarrousel.jpg', 'productos/cr68af/images/58z_cr68afcarrousel.jpg', 12, 'es', '1', 0, 0),
(2, '65k_cr68afbig.jpg', 'productos/cr68af/images/65k_cr68afbig.jpg', 13, 'es', '1', 1, 1),
(3, '64x_cr68afmedium.jpg', 'productos/cr68af/images/64x_cr68afmedium.jpg', 14, 'es', '1', 1, 1),
(4, '85p_cr68afsmall.jpg', 'productos/cr68af/images/85p_cr68afsmall.jpg', 15, 'es', '1', 1, 1),
(5, '27g_cr68afbig.jpg', 'productos/cr68af/images/27g_cr68afbig.jpg', 13, 'es', '1', 1, 2),
(6, '89u_cr68afmedium.jpg', 'productos/cr68af/images/89u_cr68afmedium.jpg', 14, 'es', '1', 1, 2),
(7, '86q_cr68afsmall.jpg', 'productos/cr68af/images/86q_cr68afsmall.jpg', 15, 'es', '1', 1, 2),
(8, '82a_cr68afbig.jpg', 'productos/cr68af/images/82a_cr68afbig.jpg', 13, 'es', '1', 1, 3),
(9, '37s_cr68afmedium.jpg', 'productos/cr68af/images/37s_cr68afmedium.jpg', 14, 'es', '1', 1, 3),
(10, '72p_cr68afsmall.jpg', 'productos/cr68af/images/72p_cr68afsmall.jpg', 15, 'es', '1', 1, 3),
(11, '91d_cr68afbig.jpg', 'productos/cr68af/images/91d_cr68afbig.jpg', 13, 'es', '1', 1, 4),
(12, '63m_cr68afmedium.jpg', 'productos/cr68af/images/63m_cr68afmedium.jpg', 14, 'es', '1', 1, 4),
(13, '70g_cr68afsmall.jpg', 'productos/cr68af/images/70g_cr68afsmall.jpg', 15, 'es', '1', 1, 4),
(14, '33d_Autorizaciones.pdf', 'productos/cr68af/files/33d_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(15, '77e_Autorizaciones.pdf', 'productos/crd81f/files/77e_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(16, '59p_crd81fcarrousel.jpg', 'productos/crd81f/images/59p_crd81fcarrousel.jpg', 12, 'es', '1', 0, 0),
(17, '39e_crd81fbig.jpg', 'productos/crd81f/images/39e_crd81fbig.jpg', 13, 'es', '1', 1, 1),
(18, '44p_crd81fmedium.jpg', 'productos/crd81f/images/44p_crd81fmedium.jpg', 14, 'es', '1', 1, 1),
(19, '89b_crd81fsmall.jpg', 'productos/crd81f/images/89b_crd81fsmall.jpg', 15, 'es', '1', 1, 1),
(20, '45h_crd81fbig.jpg', 'productos/crd81f/images/45h_crd81fbig.jpg', 13, 'es', '1', 1, 2),
(21, '83h_crd81fmedium.jpg', 'productos/crd81f/images/83h_crd81fmedium.jpg', 14, 'es', '1', 1, 2),
(22, '64f_crd81fsmall.jpg', 'productos/crd81f/images/64f_crd81fsmall.jpg', 15, 'es', '1', 1, 2),
(23, '62m_crd81fbig.jpg', 'productos/crd81f/images/62m_crd81fbig.jpg', 13, 'es', '1', 1, 3),
(24, '80q_crd81fmedium.jpg', 'productos/crd81f/images/80q_crd81fmedium.jpg', 14, 'es', '1', 1, 3),
(25, '58u_crd81fsmall.jpg', 'productos/crd81f/images/58u_crd81fsmall.jpg', 15, 'es', '1', 1, 3),
(26, '31i_crd81fbig.jpg', 'productos/crd81f/images/31i_crd81fbig.jpg', 13, 'es', '1', 1, 4),
(27, '55a_crd81fmedium.jpg', 'productos/crd81f/images/55a_crd81fmedium.jpg', 14, 'es', '1', 1, 4),
(28, '27p_crd81fsmall.jpg', 'productos/crd81f/images/27p_crd81fsmall.jpg', 15, 'es', '1', 1, 4),
(29, '42v_Autorizaciones.pdf', 'productos/nx-5400/files/42v_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(30, '90r_nx5400carrousel.jpg', 'productos/nx-5400/images/90r_nx5400carrousel.jpg', 12, 'es', '1', 0, 0),
(31, '65x_nx5400big.jpg', 'productos/nx-5400/images/65x_nx5400big.jpg', 13, 'es', '1', 1, 1),
(32, '68q_nx5400medium.jpg', 'productos/nx-5400/images/68q_nx5400medium.jpg', 14, 'es', '1', 1, 1),
(33, '17w_nx5400small.jpg', 'productos/nx-5400/images/17w_nx5400small.jpg', 15, 'es', '1', 1, 1),
(34, '95j_nx5400big.jpg', 'productos/nx-5400/images/95j_nx5400big.jpg', 13, 'es', '1', 1, 2),
(35, '45n_nx5400medium.jpg', 'productos/nx-5400/images/45n_nx5400medium.jpg', 14, 'es', '1', 1, 2),
(36, '20l_nx5400small.jpg', 'productos/nx-5400/images/20l_nx5400small.jpg', 15, 'es', '1', 1, 2),
(37, '38h_nx5400big.jpg', 'productos/nx-5400/images/38h_nx5400big.jpg', 13, 'es', '1', 1, 3),
(38, '16a_nx5400medium.jpg', 'productos/nx-5400/images/16a_nx5400medium.jpg', 14, 'es', '1', 1, 3),
(39, '68r_nx5400small.jpg', 'productos/nx-5400/images/68r_nx5400small.jpg', 15, 'es', '1', 1, 3),
(40, '57g_nx5400big.jpg', 'productos/nx-5400/images/57g_nx5400big.jpg', 13, 'es', '1', 1, 4),
(41, '22l_nx5400medium.jpg', 'productos/nx-5400/images/22l_nx5400medium.jpg', 14, 'es', '1', 1, 4),
(42, '55o_nx5400small.jpg', 'productos/nx-5400/images/55o_nx5400small.jpg', 15, 'es', '1', 1, 4),
(43, '45e_Autorizaciones.pdf', 'productos/pp1f3/files/45e_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(44, '96f_pp1f3carrousel.jpg', 'productos/pp1f3/images/96f_pp1f3carrousel.jpg', 12, 'es', '1', 0, 0),
(45, '57m_pp1f3big.jpg', 'productos/pp1f3/images/57m_pp1f3big.jpg', 13, 'es', '1', 1, 1),
(46, '23t_pp1f3medium.jpg', 'productos/pp1f3/images/23t_pp1f3medium.jpg', 14, 'es', '1', 1, 1),
(47, '66e_pp1f3small.jpg', 'productos/pp1f3/images/66e_pp1f3small.jpg', 15, 'es', '1', 1, 1),
(48, '95d_Autorizaciones.pdf', 'productos/srp-270/files/95d_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(49, '32q_srp270carrousel.jpg', 'productos/srp-270/images/32q_srp270carrousel.jpg', 12, 'es', '1', 0, 0),
(50, '66k_srp270big.jpg', 'productos/srp-270/images/66k_srp270big.jpg', 13, 'es', '1', 1, 1),
(51, '44c_srp270medium.jpg', 'productos/srp-270/images/44c_srp270medium.jpg', 14, 'es', '1', 1, 1),
(52, '14z_srp270small.jpg', 'productos/srp-270/images/14z_srp270small.jpg', 15, 'es', '1', 1, 1),
(53, '74y_srp270big.jpg', 'productos/srp-270/images/74y_srp270big.jpg', 13, 'es', '1', 1, 2),
(54, '58z_srp270medium.jpg', 'productos/srp-270/images/58z_srp270medium.jpg', 14, 'es', '1', 1, 2),
(55, '60u_srp270small.jpg', 'productos/srp-270/images/60u_srp270small.jpg', 15, 'es', '1', 1, 2),
(56, '72g_srp350carrousel.jpg', 'productos/srp-350/images/72g_srp350carrousel.jpg', 12, 'es', '1', 0, 0),
(57, '94o_srp350big.jpg', 'productos/srp-350/images/94o_srp350big.jpg', 13, 'es', '1', 1, 1),
(58, '29d_srp350medium.jpg', 'productos/srp-350/images/29d_srp350medium.jpg', 14, 'es', '1', 1, 1),
(59, '32t_srp350small.jpg', 'productos/srp-350/images/32t_srp350small.jpg', 15, 'es', '1', 1, 1),
(60, '13z_srp350big.jpg', 'productos/srp-350/images/13z_srp350big.jpg', 13, 'es', '1', 1, 2),
(61, '69c_srp350medium.jpg', 'productos/srp-350/images/69c_srp350medium.jpg', 14, 'es', '1', 1, 2),
(62, '33o_srp350small.jpg', 'productos/srp-350/images/33o_srp350small.jpg', 15, 'es', '1', 1, 2),
(63, '33w_srp350big.jpg', 'productos/srp-350/images/33w_srp350big.jpg', 13, 'es', '1', 1, 3),
(64, '54z_srp350medium.jpg', 'productos/srp-350/images/54z_srp350medium.jpg', 14, 'es', '1', 1, 3),
(65, '38s_srp350small.jpg', 'productos/srp-350/images/38s_srp350small.jpg', 15, 'es', '1', 1, 3),
(66, '76w_srp350big.jpg', 'productos/srp-350/images/76w_srp350big.jpg', 13, 'es', '1', 1, 4),
(67, '12l_srp350medium.jpg', 'productos/srp-350/images/12l_srp350medium.jpg', 14, 'es', '1', 1, 4),
(68, '14u_srp350small.jpg', 'productos/srp-350/images/14u_srp350small.jpg', 15, 'es', '1', 1, 4),
(69, '27y_Brochure.pdf', 'productos/srp-350/files/27y_Brochure.pdf', 2, 'es', '1', 1, 0),
(70, '62p_Autorizaciones.pdf', 'productos/srp-350/files/62p_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(71, '91y_microline1120carrousel.jpg', 'productos/microline-1120/images/91y_microline1120carrousel.jpg', 12, 'es', '1', 0, 0),
(72, '43e_microline1120big.jpg', 'productos/microline-1120/images/43e_microline1120big.jpg', 13, 'es', '1', 1, 1),
(73, '92o_microline1120medium.jpg', 'productos/microline-1120/images/92o_microline1120medium.jpg', 14, 'es', '1', 1, 1),
(74, '42d_microline1120small.jpg', 'productos/microline-1120/images/42d_microline1120small.jpg', 15, 'es', '1', 1, 1),
(75, '23m_microline1120big.jpg', 'productos/microline-1120/images/23m_microline1120big.jpg', 13, 'es', '1', 1, 2),
(76, '85m_microline1120medium.jpg', 'productos/microline-1120/images/85m_microline1120medium.jpg', 14, 'es', '1', 1, 2),
(77, '30n_microline1120small.jpg', 'productos/microline-1120/images/30n_microline1120small.jpg', 15, 'es', '1', 1, 2),
(78, '32t_Autorizaciones.pdf', 'productos/microline-1120/files/32t_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(79, '11a_hsp7000carrousel.jpg', 'productos/hsp7000/images/11a_hsp7000carrousel.jpg', 12, 'es', '1', 0, 0),
(80, '94j_hsp7000medium.jpg', 'productos/hsp7000/images/94j_hsp7000medium.jpg', 14, 'es', '1', 1, 1),
(81, '88p_hsp7000small.jpg', 'productos/hsp7000/images/88p_hsp7000small.jpg', 15, 'es', '1', 1, 1),
(82, '67r_hsp7000small.jpg', 'productos/hsp7000/images/67r_hsp7000small.jpg', 15, 'es', '1', 1, 2),
(83, '79i_hsp7000big.jpg', 'productos/hsp7000/images/79i_hsp7000big.jpg', 13, 'es', '1', 1, 1),
(84, '39w_hsp7000medium.jpg', 'productos/hsp7000/images/39w_hsp7000medium.jpg', 14, 'es', '1', 1, 2),
(85, '32d_hsp7000big.jpg', 'productos/hsp7000/images/32d_hsp7000big.jpg', 13, 'es', '1', 1, 2),
(86, '89n_papeltérmicomedium.jpg', 'productos/papeltermico/images/89n_papeltérmicomedium.jpg', 14, 'es', '1', 1, 1),
(87, '56p_papeltérmicosmall.jpg', 'productos/papeltermico/images/56p_papeltérmicosmall.jpg', 15, 'es', '1', 1, 1),
(88, '22e_papeltérmicosmall.jpg', 'productos/papeltermico/images/22e_papeltérmicosmall.jpg', 15, 'es', '1', 1, 2),
(89, '73n_papeltérmicosmall.jpg', 'productos/papeltermico/images/73n_papeltérmicosmall.jpg', 15, 'es', '1', 1, 3),
(90, '25o_papeltérmicosmall.jpg', 'productos/papeltermico/images/25o_papeltérmicosmall.jpg', 15, 'es', '1', 1, 4),
(91, '52g_crd81fjcarrousel.jpg', 'productos/crd81fj/images/52g_crd81fjcarrousel.jpg', 12, 'es', '1', 0, 0),
(92, '59n_crd81fjbig.jpg', 'productos/crd81fj/images/59n_crd81fjbig.jpg', 13, 'es', '1', 1, 1),
(93, '40u_crd81fjmedium.jpg', 'productos/crd81fj/images/40u_crd81fjmedium.jpg', 14, 'es', '1', 1, 1),
(94, '50e_crd81fjsmall.jpg', 'productos/crd81fj/images/50e_crd81fjsmall.jpg', 15, 'es', '1', 1, 1),
(95, '19j_crd81fjbig.jpg', 'productos/crd81fj/images/19j_crd81fjbig.jpg', 13, 'es', '1', 1, 2),
(96, '37t_crd81fjmedium.jpg', 'productos/crd81fj/images/37t_crd81fjmedium.jpg', 14, 'es', '1', 1, 2),
(97, '87t_crd81fjsmall.jpg', 'productos/crd81fj/images/87t_crd81fjsmall.jpg', 15, 'es', '1', 1, 2),
(98, '51a_crd81fjbig.jpg', 'productos/crd81fj/images/51a_crd81fjbig.jpg', 13, 'es', '1', 1, 3),
(99, '95e_crd81fjmedium.jpg', 'productos/crd81fj/images/95e_crd81fjmedium.jpg', 14, 'es', '1', 1, 3),
(100, '94v_crd81fjsmall.jpg', 'productos/crd81fj/images/94v_crd81fjsmall.jpg', 15, 'es', '1', 1, 3),
(101, '90j_crd81fjbig.jpg', 'productos/crd81fj/images/90j_crd81fjbig.jpg', 13, 'es', '1', 1, 4),
(102, '40t_crd81fjmedium.jpg', 'productos/crd81fj/images/40t_crd81fjmedium.jpg', 14, 'es', '1', 1, 4),
(103, '66v_crd81fjsmall.jpg', 'productos/crd81fj/images/66v_crd81fjsmall.jpg', 15, 'es', '1', 1, 4),
(104, '45d_Brochure.pdf', 'productos/crd81fj/files/45d_Brochure.pdf', 2, 'es', '1', 1, 0),
(105, '86e_Autorizaciones.pdf', 'productos/crd81fj/files/86e_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(106, '57p_cr68afjcarrousel.jpg', 'productos/cr68afj/images/57p_cr68afjcarrousel.jpg', 12, 'es', '1', 0, 0),
(107, '36s_cr68afjbig.jpg', 'productos/cr68afj/images/36s_cr68afjbig.jpg', 13, 'es', '1', 1, 1),
(108, '51k_cr68afjmedium.jpg', 'productos/cr68afj/images/51k_cr68afjmedium.jpg', 14, 'es', '1', 1, 1),
(109, '23p_cr68afjsmall.jpg', 'productos/cr68afj/images/23p_cr68afjsmall.jpg', 15, 'es', '1', 1, 1),
(110, '45e_cr68afjbig.jpg', 'productos/cr68afj/images/45e_cr68afjbig.jpg', 13, 'es', '1', 1, 2),
(111, '75r_cr68afjmedium.jpg', 'productos/cr68afj/images/75r_cr68afjmedium.jpg', 14, 'es', '1', 1, 2),
(112, '11q_cr68afjsmall.jpg', 'productos/cr68afj/images/11q_cr68afjsmall.jpg', 15, 'es', '1', 1, 2),
(113, '14z_cr68afjbig.jpg', 'productos/cr68afj/images/14z_cr68afjbig.jpg', 13, 'es', '1', 1, 3),
(114, '37v_cr68afjmedium.jpg', 'productos/cr68afj/images/37v_cr68afjmedium.jpg', 14, 'es', '1', 1, 3),
(115, '35f_cr68afjsmall.jpg', 'productos/cr68afj/images/35f_cr68afjsmall.jpg', 15, 'es', '1', 1, 3),
(116, '22e_cr68afjbig.jpg', 'productos/cr68afj/images/22e_cr68afjbig.jpg', 13, 'es', '1', 1, 4),
(117, '10w_cr68afjmedium.jpg', 'productos/cr68afj/images/10w_cr68afjmedium.jpg', 14, 'es', '1', 1, 4),
(118, '47u_cr68afjsmall.jpg', 'productos/cr68afj/images/47u_cr68afjsmall.jpg', 15, 'es', '1', 1, 4),
(134, '52q_hka112carrousel.jpg', 'productos/hka-112/images/52q_hka112carrousel.jpg', 12, 'es', '1', 0, 0),
(135, '70y_hka112big.jpg', 'productos/hka-112/images/70y_hka112big.jpg', 13, 'es', '1', 1, 1),
(136, '72h_hka112medium.jpg', 'productos/hka-112/images/72h_hka112medium.jpg', 14, 'es', '1', 1, 1),
(137, '90p_hka112small.jpg', 'productos/hka-112/images/90p_hka112small.jpg', 15, 'es', '1', 1, 1),
(138, '15b_hka112big.jpg', 'productos/hka-112/images/15b_hka112big.jpg', 13, 'es', '1', 1, 2),
(139, '65o_hka112medium.jpg', 'productos/hka-112/images/65o_hka112medium.jpg', 14, 'es', '1', 1, 2),
(140, '32l_hka112small.jpg', 'productos/hka-112/images/32l_hka112small.jpg', 15, 'es', '1', 1, 2),
(141, '99s_hka112big.jpg', 'productos/hka-112/images/99s_hka112big.jpg', 13, 'es', '1', 1, 3),
(142, '40w_hka112medium.jpg', 'productos/hka-112/images/40w_hka112medium.jpg', 14, 'es', '1', 1, 3),
(143, '40s_hka112small.jpg', 'productos/hka-112/images/40s_hka112small.jpg', 15, 'es', '1', 1, 3),
(144, '41s_hka112big.jpg', 'productos/hka-112/images/41s_hka112big.jpg', 13, 'es', '1', 1, 4),
(145, '51t_hka112medium.jpg', 'productos/hka-112/images/51t_hka112medium.jpg', 14, 'es', '1', 1, 4),
(146, '30p_hka112small.jpg', 'productos/hka-112/images/30p_hka112small.jpg', 15, 'es', '1', 1, 4),
(147, '37a_Brochure.pdf', 'productos/hka-112/files/37a_Brochure.pdf', 2, 'es', '1', 1, 0),
(148, '39h_Autorizaciones.pdf', 'productos/hka-112/files/39h_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(149, '84s_ps1carrousel.png', 'productos/ps1/images/84s_ps1carrousel.png', 12, 'es', '1', 0, 0),
(150, '85w_ps1big.jpg', 'productos/ps1/images/85w_ps1big.jpg', 13, 'es', '1', 1, 1),
(151, '62o_ps1medium.jpg', 'productos/ps1/images/62o_ps1medium.jpg', 14, 'es', '1', 1, 1),
(152, '56v_ps1small.jpg', 'productos/ps1/images/56v_ps1small.jpg', 15, 'es', '1', 1, 1),
(153, '58e_ps1big.jpg', 'productos/ps1/images/58e_ps1big.jpg', 13, 'es', '1', 1, 2),
(154, '94b_ps1medium.jpg', 'productos/ps1/images/94b_ps1medium.jpg', 14, 'es', '1', 1, 2),
(155, '93i_ps1small.jpg', 'productos/ps1/images/93i_ps1small.jpg', 15, 'es', '1', 1, 2),
(156, '90h_Brochure.pdf', 'productos/ps1/deleted/90h_Brochure.pdf', 2, 'es', '1', 1, 0),
(157, '16q_N. Covenin 3633-2000.pdf', 'productos/ps1/files/16q_N. Covenin 3633-2000.pdf', 5, 'es', '1', 1, 0),
(158, '47g_N. Covenin 3695-2001.pdf', 'productos/ps1/files/47g_N. Covenin 3695-2001.pdf', 4, 'es', '1', 1, 0),
(159, '16b_Autorizaciones.pdf', 'productos/ps1/files/16b_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(160, '83i_L.  Reforma Parcial.pdf', 'productos/ps1/files/83i_L.  Reforma Parcial.pdf', 1, 'es', '1', 1, 0),
(161, '49a_Manual de Usuario.pdf', 'productos/ps1/files/49a_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(177, '91p_srp280carrousel.jpg', 'productos/srp-280/images/91p_srp280carrousel.jpg', 12, 'es', '1', 0, 0),
(178, '57z_srp280big.jpg', 'productos/srp-280/images/57z_srp280big.jpg', 13, 'es', '1', 1, 1),
(179, '51l_srp280medium.jpg', 'productos/srp-280/images/51l_srp280medium.jpg', 14, 'es', '1', 1, 1),
(180, '44d_srp280small.jpg', 'productos/srp-280/images/44d_srp280small.jpg', 15, 'es', '1', 1, 1),
(181, '20j_srp280big.jpg', 'productos/srp-280/images/20j_srp280big.jpg', 13, 'es', '1', 1, 2),
(182, '35q_srp280medium.jpg', 'productos/srp-280/images/35q_srp280medium.jpg', 14, 'es', '1', 1, 2),
(183, '22b_srp280small.jpg', 'productos/srp-280/images/22b_srp280small.jpg', 15, 'es', '1', 1, 2),
(184, '29x_Brochure.pdf', 'productos/srp-280/files/29x_Brochure.pdf', 2, 'es', '1', 1, 0),
(185, '63g_Autorizaciones.pdf', 'productos/srp-280/files/63g_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(186, '22v_cr2100carrousel.jpg', 'productos/cr2100/images/22v_cr2100carrousel.jpg', 12, 'es', '1', 0, 0),
(187, '14n_cr2100big.jpg', 'productos/cr2100/images/14n_cr2100big.jpg', 13, 'es', '1', 1, 1),
(188, '67p_cr2100medium.jpg', 'productos/cr2100/images/67p_cr2100medium.jpg', 14, 'es', '1', 1, 1),
(189, '73k_cr2100small.jpg', 'productos/cr2100/images/73k_cr2100small.jpg', 15, 'es', '1', 1, 1),
(190, '37n_cr2100big.jpg', 'productos/cr2100/images/37n_cr2100big.jpg', 13, 'es', '1', 1, 2),
(191, '54h_cr2100medium.jpg', 'productos/cr2100/images/54h_cr2100medium.jpg', 14, 'es', '1', 1, 2),
(192, '80b_cr2100small.jpg', 'productos/cr2100/images/80b_cr2100small.jpg', 15, 'es', '1', 1, 2),
(193, '54h_Brochure.pdf', 'productos/cr2100/files/54h_Brochure.pdf', 2, 'es', '1', 1, 0),
(194, '33e_Autorizaciones.pdf', 'productos/cr2100/files/33e_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(195, '68x_srp812carrousel.jpg', 'productos/srp-812/images/68x_srp812carrousel.jpg', 12, 'es', '1', 0, 0),
(196, '83z_srp812big.jpg', 'productos/srp-812/images/83z_srp812big.jpg', 13, 'es', '1', 1, 1),
(197, '99a_srp812medium.jpg', 'productos/srp-812/images/99a_srp812medium.jpg', 14, 'es', '1', 1, 1),
(198, '30b_srp812small.jpg', 'productos/srp-812/images/30b_srp812small.jpg', 15, 'es', '1', 1, 1),
(199, '97y_srp812big.jpg', 'productos/srp-812/images/97y_srp812big.jpg', 13, 'es', '1', 1, 2),
(200, '25z_srp812medium.jpg', 'productos/srp-812/images/25z_srp812medium.jpg', 14, 'es', '1', 1, 2),
(201, '35p_srp812small.jpg', 'productos/srp-812/images/35p_srp812small.jpg', 15, 'es', '1', 1, 2),
(202, '70t_srp812big.jpg', 'productos/srp-812/images/70t_srp812big.jpg', 13, 'es', '1', 1, 3),
(203, '20o_srp812medium.jpg', 'productos/srp-812/images/20o_srp812medium.jpg', 14, 'es', '1', 1, 3),
(204, '63z_srp812small.jpg', 'productos/srp-812/images/63z_srp812small.jpg', 15, 'es', '1', 1, 3),
(205, '35n_srp812big.jpg', 'productos/srp-812/images/35n_srp812big.jpg', 13, 'es', '1', 1, 4),
(206, '98e_srp812medium.jpg', 'productos/srp-812/images/98e_srp812medium.jpg', 14, 'es', '1', 1, 4),
(207, '45n_srp812small.jpg', 'productos/srp-812/images/45n_srp812small.jpg', 15, 'es', '1', 1, 4),
(208, '88r_Brochure.pdf', 'productos/srp-812/files/88r_Brochure.pdf', 2, 'es', '1', 1, 0),
(209, '70o_Autorizaciones.pdf', 'productos/srp-812/files/70o_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(210, '53v_os2xcarrousel.jpg', 'productos/os2x/images/53v_os2xcarrousel.jpg', 12, 'es', '1', 1, 0),
(211, '86t_os2xbig.jpg', 'productos/os2x/images/86t_os2xbig.jpg', 13, 'es', '1', 1, 1),
(212, '33a_os2xmedium.jpg', 'productos/os2x/images/33a_os2xmedium.jpg', 14, 'es', '1', 1, 1),
(213, '79g_os2xsmall.jpg', 'productos/os2x/images/79g_os2xsmall.jpg', 15, 'es', '1', 1, 1),
(214, '16d_os2xbig.jpg', 'productos/os2x/images/16d_os2xbig.jpg', 13, 'es', '1', 1, 2),
(215, '79x_os2xmedium.jpg', 'productos/os2x/images/79x_os2xmedium.jpg', 14, 'es', '1', 1, 2),
(216, '14u_os2xsmall.jpg', 'productos/os2x/images/14u_os2xsmall.jpg', 15, 'es', '1', 1, 2),
(217, '69k_os2xbig.jpg', 'productos/os2x/images/69k_os2xbig.jpg', 13, 'es', '1', 1, 3),
(218, '19p_os2xmedium.jpg', 'productos/os2x/images/19p_os2xmedium.jpg', 14, 'es', '1', 1, 3),
(219, '86u_os2xsmall.jpg', 'productos/os2x/images/86u_os2xsmall.jpg', 15, 'es', '1', 1, 3),
(220, '40a_os2xbig.jpg', 'productos/os2x/images/40a_os2xbig.jpg', 13, 'es', '1', 1, 4),
(221, '25g_os2xmedium.jpg', 'productos/os2x/images/25g_os2xmedium.jpg', 14, 'es', '1', 1, 4),
(222, '21c_os2xsmall.jpg', 'productos/os2x/images/21c_os2xsmall.jpg', 15, 'es', '1', 1, 4),
(223, '44j_N. Covenin 3695-2001.pdf', 'productos/os2x/files/44j_N. Covenin 3695-2001.pdf', 4, 'es', '1', 1, 0),
(224, '65i_L.  Reforma Parcial.pdf', 'productos/os2x/files/65i_L.  Reforma Parcial.pdf', 1, 'es', '1', 1, 0),
(225, '35i_L. Certificado  Aprobacion.pdf', 'productos/os2x/files/35i_L. Certificado  Aprobacion.pdf', 10, 'es', '1', 1, 0),
(226, '85o_N. Covenin 3633-2000.pdf', 'productos/os2x/files/85o_N. Covenin 3633-2000.pdf', 5, 'es', '1', 1, 0),
(227, '98f_cr2300carrousel.jpg', 'productos/cr2300/images/98f_cr2300carrousel.jpg', 12, 'es', '1', 0, 0),
(228, '50i_cr2300big.jpg', 'productos/cr2300/images/50i_cr2300big.jpg', 13, 'es', '1', 1, 1),
(229, '89t_cr2300medium.jpg', 'productos/cr2300/images/89t_cr2300medium.jpg', 14, 'es', '1', 1, 1),
(230, '94g_cr2300small.jpg', 'productos/cr2300/images/94g_cr2300small.jpg', 15, 'es', '1', 1, 1),
(231, '69r_cr2300big.jpg', 'productos/cr2300/images/69r_cr2300big.jpg', 13, 'es', '1', 1, 2),
(232, '66k_cr2300medium.jpg', 'productos/cr2300/images/66k_cr2300medium.jpg', 14, 'es', '1', 1, 2),
(233, '76c_cr2300small.jpg', 'productos/cr2300/images/76c_cr2300small.jpg', 15, 'es', '1', 1, 2),
(234, '32c_cr2300big.jpg', 'productos/cr2300/images/32c_cr2300big.jpg', 13, 'es', '1', 1, 3),
(235, '42y_cr2300medium.jpg', 'productos/cr2300/images/42y_cr2300medium.jpg', 14, 'es', '1', 1, 3),
(236, '40f_cr2300small.jpg', 'productos/cr2300/images/40f_cr2300small.jpg', 15, 'es', '1', 1, 3),
(237, '68y_cr2300big.jpg', 'productos/cr2300/images/68y_cr2300big.jpg', 13, 'es', '1', 1, 4),
(238, '81y_cr2300medium.jpg', 'productos/cr2300/images/81y_cr2300medium.jpg', 14, 'es', '1', 1, 4),
(239, '47u_cr2300small.jpg', 'productos/cr2300/images/47u_cr2300small.jpg', 15, 'es', '1', 1, 4),
(240, '24t_Autorizaciones.pdf', 'productos/cr2300/files/24t_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(241, '94l_Brochure.pdf', 'productos/cr2300/files/94l_Brochure.pdf', 2, 'es', '1', 1, 0),
(242, '78i_pp9carrousel.jpg', 'productos/pp9/images/78i_pp9carrousel.jpg', 12, 'es', '1', 0, 0),
(243, '85v_pp9big.jpg', 'productos/pp9/images/85v_pp9big.jpg', 13, 'es', '1', 1, 1),
(244, '36c_pp9medium.jpg', 'productos/pp9/images/36c_pp9medium.jpg', 14, 'es', '1', 1, 1),
(245, '41y_pp9small.jpg', 'productos/pp9/images/41y_pp9small.jpg', 15, 'es', '1', 1, 1),
(246, '82u_pp9big.jpg', 'productos/pp9/images/82u_pp9big.jpg', 13, 'es', '1', 1, 2),
(247, '47z_pp9medium.jpg', 'productos/pp9/images/47z_pp9medium.jpg', 14, 'es', '1', 1, 2),
(248, '30y_pp9small.jpg', 'productos/pp9/images/30y_pp9small.jpg', 15, 'es', '1', 1, 2),
(249, '33c_pp9big.jpg', 'productos/pp9/images/33c_pp9big.jpg', 13, 'es', '1', 1, 3),
(250, '91k_pp9medium.jpg', 'productos/pp9/images/91k_pp9medium.jpg', 14, 'es', '1', 1, 3),
(251, '96r_pp9small.jpg', 'productos/pp9/images/96r_pp9small.jpg', 15, 'es', '1', 1, 3),
(252, '23x_pp9big.jpg', 'productos/pp9/images/23x_pp9big.jpg', 13, 'es', '1', 1, 4),
(253, '15u_pp9medium.jpg', 'productos/pp9/images/15u_pp9medium.jpg', 14, 'es', '1', 1, 4),
(254, '70b_pp9small.jpg', 'productos/pp9/images/70b_pp9small.jpg', 15, 'es', '1', 1, 4),
(255, '65h_Autorizaciones.pdf', 'productos/pp9/files/65h_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(256, '24z_Brochure.pdf', 'productos/pp9/files/24z_Brochure.pdf', 2, 'es', '1', 1, 0),
(268, '27u_p3100dlcarrousel.jpg', 'productos/p3100dl/images/27u_p3100dlcarrousel.jpg', 12, 'es', '1', 0, 0),
(269, '91c_p3100dlbig.jpg', 'productos/p3100dl/images/91c_p3100dlbig.jpg', 13, 'es', '1', 1, 1),
(270, '96l_p3100dlmedium.jpg', 'productos/p3100dl/images/96l_p3100dlmedium.jpg', 14, 'es', '1', 1, 1),
(271, '73y_p3100dlsmall.jpg', 'productos/p3100dl/images/73y_p3100dlsmall.jpg', 15, 'es', '1', 1, 1),
(272, '78m_p3100dlbig.jpg', 'productos/p3100dl/images/78m_p3100dlbig.jpg', 13, 'es', '1', 1, 2),
(273, '31x_p3100dlmedium.jpg', 'productos/p3100dl/images/31x_p3100dlmedium.jpg', 14, 'es', '1', 1, 2),
(274, '37d_p3100dlsmall.jpg', 'productos/p3100dl/images/37d_p3100dlsmall.jpg', 15, 'es', '1', 1, 2),
(275, '69d_p3100dlbig.jpg', 'productos/p3100dl/images/69d_p3100dlbig.jpg', 13, 'es', '1', 1, 3),
(276, '24j_p3100dlmedium.jpg', 'productos/p3100dl/images/24j_p3100dlmedium.jpg', 14, 'es', '1', 1, 3),
(277, '82v_p3100dlsmall.jpg', 'productos/p3100dl/images/82v_p3100dlsmall.jpg', 15, 'es', '1', 1, 3),
(278, '81c_p3100dlbig.jpg', 'productos/p3100dl/images/81c_p3100dlbig.jpg', 13, 'es', '1', 1, 4),
(279, '71d_p3100dlmedium.jpg', 'productos/p3100dl/images/71d_p3100dlmedium.jpg', 14, 'es', '1', 1, 4),
(280, '24o_p3100dlsmall.jpg', 'productos/p3100dl/images/24o_p3100dlsmall.jpg', 15, 'es', '1', 1, 4),
(281, '22p_Brochure.pdf', 'productos/p3100dl/files/22p_Brochure.pdf', 2, 'es', '1', 1, 0),
(282, '36p_Autorizaciones.pdf', 'productos/p3100dl/files/36p_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(283, '19z_dt230carrousel.jpg', 'productos/dt-230/images/19z_dt230carrousel.jpg', 12, 'es', '1', 1, 0),
(284, '72n_dt230big.jpg', 'productos/dt-230/images/72n_dt230big.jpg', 13, 'es', '1', 1, 1),
(285, '28z_dt230medium.jpg', 'productos/dt-230/images/28z_dt230medium.jpg', 14, 'es', '1', 1, 1),
(286, '50e_dt230small.jpg', 'productos/dt-230/images/50e_dt230small.jpg', 15, 'es', '1', 1, 1),
(287, '63r_dt230big.jpg', 'productos/dt-230/images/63r_dt230big.jpg', 13, 'es', '1', 1, 2),
(288, '96d_dt230medium.jpg', 'productos/dt-230/images/96d_dt230medium.jpg', 14, 'es', '1', 1, 2),
(289, '57p_dt230small.jpg', 'productos/dt-230/images/57p_dt230small.jpg', 15, 'es', '1', 1, 2),
(290, '36k_dt230big.jpg', 'productos/dt-230/images/36k_dt230big.jpg', 13, 'es', '1', 1, 3),
(291, '33g_dt230medium.jpg', 'productos/dt-230/images/33g_dt230medium.jpg', 14, 'es', '1', 1, 3),
(292, '84x_dt230small.jpg', 'productos/dt-230/images/84x_dt230small.jpg', 15, 'es', '1', 1, 3),
(293, '42m_Manual de Usuario.pdf', 'productos/dt-230/files/42m_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(294, '59l_Brochure.pdf', 'productos/dt-230/deleted/59l_Brochure.pdf', 2, 'es', '1', 0, 0),
(295, '27h_dt230big.jpg', 'productos/dt-230/images/27h_dt230big.jpg', 13, 'es', '1', 1, 4),
(296, '10y_dt230medium.jpg', 'productos/dt-230/images/10y_dt230medium.jpg', 14, 'es', '1', 1, 4),
(297, '92q_dt230small.jpg', 'productos/dt-230/images/92q_dt230small.jpg', 15, 'es', '1', 1, 4),
(298, '82v_hka57carrousel.jpg', 'productos/hka57/images/82v_hka57carrousel.jpg', 12, 'es', '1', 1, 0),
(299, '93o_hka57big.jpg', 'productos/hka57/images/93o_hka57big.jpg', 13, 'es', '1', 1, 1),
(300, '92q_hka57medium.jpg', 'productos/hka57/images/92q_hka57medium.jpg', 14, 'es', '1', 1, 1),
(301, '34u_hka57small.jpg', 'productos/hka57/images/34u_hka57small.jpg', 15, 'es', '1', 1, 1),
(302, '73e_hka57big.jpg', 'productos/hka57/images/73e_hka57big.jpg', 13, 'es', '1', 1, 2),
(303, '16e_hka57medium.jpg', 'productos/hka57/images/16e_hka57medium.jpg', 14, 'es', '1', 1, 2),
(304, '62v_hka57small.jpg', 'productos/hka57/images/62v_hka57small.jpg', 15, 'es', '1', 1, 2),
(305, '60d_hka57big.jpg', 'productos/hka57/images/60d_hka57big.jpg', 13, 'es', '1', 1, 3),
(306, '90p_hka57medium.jpg', 'productos/hka57/images/90p_hka57medium.jpg', 14, 'es', '1', 1, 3),
(307, '43x_hka57small.jpg', 'productos/hka57/images/43x_hka57small.jpg', 15, 'es', '1', 1, 3),
(308, '99d_hka57big.jpg', 'productos/hka57/images/99d_hka57big.jpg', 13, 'es', '1', 1, 4),
(309, '85a_hka57medium.jpg', 'productos/hka57/images/85a_hka57medium.jpg', 14, 'es', '1', 1, 4),
(310, '98k_hka57small.jpg', 'productos/hka57/images/98k_hka57small.jpg', 15, 'es', '1', 1, 4),
(311, '97k_Brochure.pdf', 'productos/hka57/files/97k_Brochure.pdf', 2, 'es', '1', 1, 0),
(312, '78o_Manual de Usuario.pdf', 'productos/hka57/files/78o_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(313, '64z_Manual de Usuario.pdf', 'productos/hka80/files/64z_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(314, '48y_Brochure.pdf', 'productos/hka80/files/48y_Brochure.pdf', 2, 'es', '1', 1, 0),
(315, '47d_tallydascom1125carrousel.jpg', 'productos/tallydascom1125/images/47d_tallydascom1125carrousel.jpg', 12, 'es', '1', 0, 0),
(316, '88n_tallydascom1500carrousel.jpg', 'productos/tallydascom1500/images/88n_tallydascom1500carrousel.jpg', 12, 'es', '1', 0, 0),
(317, '44o_tallydascom2610carrousel.jpg', 'productos/tallydascom2610/images/44o_tallydascom2610carrousel.jpg', 12, 'es', '1', 0, 0),
(318, '33q_Manual de Usuario.pdf', 'productos/tallydascom2610/files/33q_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(319, '10c_tallydascom2380carrousel.jpg', 'productos/tallydascom2380/images/10c_tallydascom2380carrousel.jpg', 12, 'es', '1', 0, 0),
(320, '76k_t2365carrousel.jpg', 'productos/t2365/images/76k_t2365carrousel.jpg', 12, 'es', '1', 0, 0),
(321, '86g_tallydascom2600carrousel.jpg', 'productos/tallydascom2600/images/86g_tallydascom2600carrousel.jpg', 12, 'es', '1', 0, 0),
(322, '84p_tallydascom1125big.jpg', 'productos/tallydascom1125/images/84p_tallydascom1125big.jpg', 13, 'es', '1', 1, 1),
(323, '17z_tallydascom1125medium.jpg', 'productos/tallydascom1125/images/17z_tallydascom1125medium.jpg', 14, 'es', '1', 1, 1),
(324, '54f_tallydascom1125small.jpg', 'productos/tallydascom1125/images/54f_tallydascom1125small.jpg', 15, 'es', '1', 1, 1),
(325, '46m_tallydascom1125big.jpg', 'productos/tallydascom1125/images/46m_tallydascom1125big.jpg', 13, 'es', '1', 1, 2),
(326, '21z_tallydascom1125medium.jpg', 'productos/tallydascom1125/images/21z_tallydascom1125medium.jpg', 14, 'es', '1', 1, 2),
(327, '77w_tallydascom1125small.jpg', 'productos/tallydascom1125/images/77w_tallydascom1125small.jpg', 15, 'es', '1', 1, 2),
(328, '12d_tallydascom1125big.jpg', 'productos/tallydascom1125/images/12d_tallydascom1125big.jpg', 13, 'es', '1', 1, 3),
(329, '87w_tallydascom1125medium.jpg', 'productos/tallydascom1125/images/87w_tallydascom1125medium.jpg', 14, 'es', '1', 1, 3),
(330, '51t_tallydascom1125small.jpg', 'productos/tallydascom1125/images/51t_tallydascom1125small.jpg', 15, 'es', '1', 1, 3),
(331, '92b_tallydascom1125big.jpg', 'productos/tallydascom1125/images/92b_tallydascom1125big.jpg', 13, 'es', '1', 1, 4),
(332, '98w_tallydascom1125medium.jpg', 'productos/tallydascom1125/images/98w_tallydascom1125medium.jpg', 14, 'es', '1', 1, 4),
(333, '97j_tallydascom1125small.jpg', 'productos/tallydascom1125/images/97j_tallydascom1125small.jpg', 15, 'es', '1', 1, 4),
(334, '45j_tallydascom1225carrousel.jpg', 'productos/tallydascom1225/images/45j_tallydascom1225carrousel.jpg', 12, 'es', '1', 0, 0),
(335, '38e_p2506wcarrousel.jpg', 'productos/p2506w/images/38e_p2506wcarrousel.jpg', 12, 'es', '1', 1, 0),
(336, '67t_p2506wbig.jpg', 'productos/p2506w/images/67t_p2506wbig.jpg', 13, 'es', '1', 1, 1),
(337, '87n_p2506wmedium.jpg', 'productos/p2506w/images/87n_p2506wmedium.jpg', 14, 'es', '1', 1, 1),
(338, '85i_p2506wsmall.jpg', 'productos/p2506w/images/85i_p2506wsmall.jpg', 15, 'es', '1', 1, 1),
(339, '22h_p2506wbig.jpg', 'productos/p2506w/images/22h_p2506wbig.jpg', 13, 'es', '1', 1, 2),
(340, '37t_p2506wmedium.jpg', 'productos/p2506w/images/37t_p2506wmedium.jpg', 14, 'es', '1', 1, 2),
(341, '29s_p2506wsmall.jpg', 'productos/p2506w/images/29s_p2506wsmall.jpg', 15, 'es', '1', 1, 2),
(342, '13b_p2506wbig.jpg', 'productos/p2506w/images/13b_p2506wbig.jpg', 13, 'es', '1', 1, 3),
(343, '88y_p2506wmedium.jpg', 'productos/p2506w/images/88y_p2506wmedium.jpg', 14, 'es', '1', 1, 3),
(344, '35j_p2506wsmall.jpg', 'productos/p2506w/images/35j_p2506wsmall.jpg', 15, 'es', '1', 1, 3),
(345, '44m_p2506wbig.jpg', 'productos/p2506w/images/44m_p2506wbig.jpg', 13, 'es', '1', 1, 4),
(346, '67l_p2506wmedium.jpg', 'productos/p2506w/images/67l_p2506wmedium.jpg', 14, 'es', '1', 1, 4),
(347, '17b_p2506wsmall.jpg', 'productos/p2506w/images/17b_p2506wsmall.jpg', 15, 'es', '1', 1, 4),
(348, '14v_7010carrousel.jpg', 'productos/7010/images/14v_7010carrousel.jpg', 12, 'es', '1', 1, 0),
(349, '72n_7010big.jpg', 'productos/7010/images/72n_7010big.jpg', 13, 'es', '1', 1, 1),
(350, '80p_7010medium.jpg', 'productos/7010/images/80p_7010medium.jpg', 14, 'es', '1', 1, 1),
(351, '87s_7010big.jpg', 'productos/7010/images/87s_7010big.jpg', 13, 'es', '1', 1, 2),
(352, '28k_7010medium.jpg', 'productos/7010/images/28k_7010medium.jpg', 14, 'es', '1', 1, 2),
(353, '55d_Manual de Usuario.pdf', 'productos/7010/files/55d_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(354, '27y_7206carrousel.jpg', 'productos/7206/images/27y_7206carrousel.jpg', 12, 'es', '1', 1, 0),
(355, '90y_7206big.jpg', 'productos/7206/images/90y_7206big.jpg', 13, 'es', '1', 1, 1),
(356, '11z_7206medium.jpg', 'productos/7206/images/11z_7206medium.jpg', 14, 'es', '1', 1, 1),
(357, '32q_7206big.jpg', 'productos/7206/images/32q_7206big.jpg', 13, 'es', '1', 1, 2),
(358, '55g_7206medium.jpg', 'productos/7206/images/55g_7206medium.jpg', 14, 'es', '1', 1, 2),
(359, '38o_7206big.jpg', 'productos/7206/images/38o_7206big.jpg', 13, 'es', '1', 1, 3),
(360, '77k_7206medium.jpg', 'productos/7206/images/77k_7206medium.jpg', 14, 'es', '1', 1, 3),
(361, '60x_7106carrousel.jpg', 'productos/7106/images/60x_7106carrousel.jpg', 12, 'es', '1', 1, 0),
(362, '11a_7106big.jpg', 'productos/7106/images/11a_7106big.jpg', 13, 'es', '1', 1, 1),
(363, '29g_7106medium.jpg', 'productos/7106/images/29g_7106medium.jpg', 14, 'es', '1', 1, 1),
(364, '92l_7106big.jpg', 'productos/7106/images/92l_7106big.jpg', 13, 'es', '1', 1, 2),
(365, '22y_7106medium.jpg', 'productos/7106/images/22y_7106medium.jpg', 14, 'es', '1', 1, 2),
(366, '94n_mip480carrousel.jpg', 'productos/mip-480/images/94n_mip480carrousel.jpg', 12, 'es', '1', 1, 0),
(367, '97r_mip480big.jpg', 'productos/mip-480/images/97r_mip480big.jpg', 13, 'es', '1', 1, 1),
(368, '28e_mip480medium.jpg', 'productos/mip-480/images/28e_mip480medium.jpg', 14, 'es', '1', 1, 1),
(369, '39h_mip480medium.jpg', 'productos/mip-480/images/39h_mip480medium.jpg', 14, 'es', '1', 1, 2),
(370, '72w_mip480small.jpg', 'productos/mip-480/images/72w_mip480small.jpg', 15, 'es', '1', 1, 2),
(371, '25h_dp540carrousel.jpg', 'productos/dp-540/images/25h_dp540carrousel.jpg', 12, 'es', '1', 1, 0),
(372, '23k_dp540big.jpg', 'productos/dp-540/images/23k_dp540big.jpg', 13, 'es', '1', 1, 1),
(373, '98w_dp540medium.jpg', 'productos/dp-540/images/98w_dp540medium.jpg', 14, 'es', '1', 1, 1),
(374, '15y_Ficha Tecnica.pdf', 'productos/dp-540/files/15y_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(375, '50x_Guia de Inicio Rapido.pdf', 'productos/dp-540/files/50x_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(376, '74p_dp550carrousel.jpg', 'productos/dp-550/images/74p_dp550carrousel.jpg', 12, 'es', '1', 1, 0),
(377, '54e_dp550big.jpg', 'productos/dp-550/images/54e_dp550big.jpg', 13, 'es', '1', 1, 1),
(378, '88e_dp550medium.jpg', 'productos/dp-550/images/88e_dp550medium.jpg', 14, 'es', '1', 1, 1),
(379, '20y_Ficha Tecnica.pdf', 'productos/dp-550/files/20y_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(380, '42c_Guia de Inicio Rapido.pdf', 'productos/dp-550/files/42c_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(381, '95s_hka1080carrousel.jpg', 'productos/hka1080/images/95s_hka1080carrousel.jpg', 12, 'es', '1', 0, 0),
(382, '23s_hka1080big.jpg', 'productos/hka1080/images/23s_hka1080big.jpg', 13, 'es', '1', 1, 1),
(383, '53o_hka1080medium.jpg', 'productos/hka1080/images/53o_hka1080medium.jpg', 14, 'es', '1', 1, 1),
(384, '23y_hka1080big.jpg', 'productos/hka1080/images/23y_hka1080big.jpg', 13, 'es', '1', 1, 2),
(385, '84n_hka1080medium.jpg', 'productos/hka1080/images/84n_hka1080medium.jpg', 14, 'es', '1', 1, 2),
(386, '33l_hka1080big.jpg', 'productos/hka1080/images/33l_hka1080big.jpg', 13, 'es', '1', 1, 4),
(387, '20e_hka1080medium.jpg', 'productos/hka1080/images/20e_hka1080medium.jpg', 14, 'es', '1', 1, 4),
(388, '50l_hka1080big.jpg', 'productos/hka1080/images/50l_hka1080big.jpg', 13, 'es', '1', 1, 3),
(389, '37o_hka1080medium.jpg', 'productos/hka1080/images/37o_hka1080medium.jpg', 14, 'es', '1', 1, 3),
(390, '47t_Brochure.pdf', 'productos/hka1080/files/47t_Brochure.pdf', 2, 'es', '1', 1, 0),
(391, '34v_Brochure.pdf', 'productos/srp-350/files/34v_Brochure.pdf', 2, 'es', '1', 1, 0),
(392, '62g_Autorizaciones.pdf', 'productos/srp-350/files/62g_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(393, '78a_Brochure.pdf', 'productos/srp-350/files/78a_Brochure.pdf', 2, 'es', '1', 1, 0),
(394, '20u_Autorizaciones.pdf', 'productos/srp-350/files/20u_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(395, '28s_Brochure.pdf', 'productos/p3100dl/files/28s_Brochure.pdf', 2, 'es', '1', 1, 0),
(396, '60j_Autorizaciones.pdf', 'productos/p3100dl/files/60j_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(397, '12w_Brochure.pdf', 'productos/hsp7000/files/12w_Brochure.pdf', 2, 'es', '1', 1, 0),
(398, '16v_Autorizaciones.pdf', 'productos/hsp7000/files/16v_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(399, '75f_Brochure.pdf', 'productos/hsp7000/files/75f_Brochure.pdf', 2, 'es', '1', 1, 0),
(400, '39e_Autorizaciones.pdf', 'productos/hsp7000/files/39e_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(403, '19m_Brochure.pdf', 'productos/crd81fj/files/19m_Brochure.pdf', 2, 'es', '1', 1, 0),
(404, '28c_Autorizaciones.pdf', 'productos/crd81fj/files/28c_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(405, '37k_Brochure.pdf', 'productos/crd81fj/files/37k_Brochure.pdf', 2, 'es', '1', 1, 0),
(406, '25r_Autorizaciones.pdf', 'productos/crd81fj/files/25r_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(407, '59m_papeltérmicobig.jpg', 'productos/papeltermico/images/59m_papeltérmicobig.jpg', 13, 'es', '1', 1, 1),
(408, '52o_papeltérmicomedium.jpg', 'productos/papeltermico/images/52o_papeltérmicomedium.jpg', 14, 'es', '1', 1, 2),
(409, '67c_papeltérmicobig.jpg', 'productos/papeltermico/images/67c_papeltérmicobig.jpg', 13, 'es', '1', 1, 2),
(410, '56s_papeltérmicomedium.jpg', 'productos/papeltermico/images/56s_papeltérmicomedium.jpg', 14, 'es', '1', 1, 3),
(411, '45j_papeltérmicobig.jpg', 'productos/papeltermico/images/45j_papeltérmicobig.jpg', 13, 'es', '1', 1, 3),
(412, '94p_papeltérmicomedium.jpg', 'productos/papeltermico/images/94p_papeltérmicomedium.jpg', 14, 'es', '1', 1, 4),
(413, '16u_papeltérmicobig.jpg', 'productos/papeltermico/images/16u_papeltérmicobig.jpg', 13, 'es', '1', 1, 4),
(414, '74d_bc2000uvcarrousel.jpg', 'productos/bc-2000uv/images/74d_bc2000uvcarrousel.jpg', 12, 'es', '1', 0, 0),
(415, '93r_bc2000uvmedium.jpg', 'productos/bc-2000uv/images/93r_bc2000uvmedium.jpg', 14, 'es', '1', 1, 1),
(416, '14w_bc2000uvsmall.jpg', 'productos/bc-2000uv/images/14w_bc2000uvsmall.jpg', 15, 'es', '1', 1, 1),
(417, '22b_bcd1000dgcarrousel.jpg', 'productos/bcd1000dg/images/22b_bcd1000dgcarrousel.jpg', 12, 'es', '1', 0, 0),
(418, '39q_bcd1000dgbig.jpg', 'productos/bcd1000dg/images/39q_bcd1000dgbig.jpg', 13, 'es', '1', 1, 1),
(419, '14m_bcd1000dgmedium.jpg', 'productos/bcd1000dg/images/14m_bcd1000dgmedium.jpg', 14, 'es', '1', 1, 1),
(420, '28m_bcd1000dgsmall.jpg', 'productos/bcd1000dg/images/28m_bcd1000dgsmall.jpg', 15, 'es', '1', 1, 1),
(421, '52l_bcd1000dgbig.jpg', 'productos/bcd1000dg/images/52l_bcd1000dgbig.jpg', 13, 'es', '1', 1, 2),
(422, '73e_bcd1000dgmedium.jpg', 'productos/bcd1000dg/images/73e_bcd1000dgmedium.jpg', 14, 'es', '1', 1, 2),
(423, '56b_bcd1000dgsmall.jpg', 'productos/bcd1000dg/images/56b_bcd1000dgsmall.jpg', 15, 'es', '1', 1, 2),
(424, '26n_gavetacarrousel.jpg', 'productos/gaveta/images/26n_gavetacarrousel.jpg', 12, 'es', '1', 0, 0),
(425, '10w_gavetabig.jpg', 'productos/gaveta/images/10w_gavetabig.jpg', 13, 'es', '1', 1, 1),
(426, '97b_gavetamedium.jpg', 'productos/gaveta/images/97b_gavetamedium.jpg', 14, 'es', '1', 1, 1),
(427, '24o_gavetasmall.jpg', 'productos/gaveta/images/24o_gavetasmall.jpg', 15, 'es', '1', 1, 1),
(428, '24l_gavetabig.jpg', 'productos/gaveta/images/24l_gavetabig.jpg', 13, 'es', '1', 1, 2),
(429, '44s_gavetamedium.jpg', 'productos/gaveta/images/44s_gavetamedium.jpg', 14, 'es', '1', 1, 2),
(430, '87y_gavetasmall.jpg', 'productos/gaveta/images/87y_gavetasmall.jpg', 15, 'es', '1', 1, 2),
(431, '58a_gavetabig.jpg', 'productos/gaveta/images/58a_gavetabig.jpg', 13, 'es', '1', 1, 3),
(432, '57j_gavetamedium.jpg', 'productos/gaveta/images/57j_gavetamedium.jpg', 14, 'es', '1', 1, 3),
(433, '83z_gavetasmall.jpg', 'productos/gaveta/images/83z_gavetasmall.jpg', 15, 'es', '1', 1, 3),
(434, '10w_hkausblinkcarrousel.jpg', 'productos/hkausb-link/images/10w_hkausblinkcarrousel.jpg', 12, 'es', '1', 0, 0),
(435, '69w_hkausblinkbig.jpg', 'productos/hkausb-link/images/69w_hkausblinkbig.jpg', 13, 'es', '1', 1, 1),
(436, '95d_hkausblinkmedium.jpg', 'productos/hkausb-link/images/95d_hkausblinkmedium.jpg', 14, 'es', '1', 1, 1),
(437, '35j_hkausblinksmall.jpg', 'productos/hkausb-link/images/35j_hkausblinksmall.jpg', 15, 'es', '1', 1, 1),
(438, '46y_hka232carrousel.jpg', 'productos/hka-232/images/46y_hka232carrousel.jpg', 12, 'es', '1', 0, 0),
(439, '80o_hka232big.jpg', 'productos/hka-232/images/80o_hka232big.jpg', 13, 'es', '1', 1, 1),
(440, '88b_hka232medium.jpg', 'productos/hka-232/images/88b_hka232medium.jpg', 14, 'es', '1', 1, 1),
(441, '38u_hka232small.jpg', 'productos/hka-232/images/38u_hka232small.jpg', 15, 'es', '1', 1, 1),
(442, '27l_hka232big.jpg', 'productos/hka-232/images/27l_hka232big.jpg', 13, 'es', '1', 1, 2),
(443, '83t_hka232medium.jpg', 'productos/hka-232/images/83t_hka232medium.jpg', 14, 'es', '1', 1, 2),
(444, '68y_hka232small.jpg', 'productos/hka-232/images/68y_hka232small.jpg', 15, 'es', '1', 1, 2),
(445, '69z_hka232big.jpg', 'productos/hka-232/images/69z_hka232big.jpg', 13, 'es', '1', 1, 3),
(446, '75p_hka232medium.jpg', 'productos/hka-232/images/75p_hka232medium.jpg', 14, 'es', '1', 1, 3),
(447, '76v_hka232small.jpg', 'productos/hka-232/images/76v_hka232small.jpg', 15, 'es', '1', 1, 3),
(448, '90l_hka232big.jpg', 'productos/hka-232/images/90l_hka232big.jpg', 13, 'es', '1', 1, 4),
(449, '83a_hka232medium.jpg', 'productos/hka-232/images/83a_hka232medium.jpg', 14, 'es', '1', 1, 4),
(450, '71v_hka232small.jpg', 'productos/hka-232/images/71v_hka232small.jpg', 15, 'es', '1', 1, 4),
(451, '85b_vb30rcarrousel.jpg', 'productos/vb-30r/images/85b_vb30rcarrousel.jpg', 12, 'es', '1', 0, 0),
(452, '11s_vb30rbig.jpg', 'productos/vb-30r/images/11s_vb30rbig.jpg', 13, 'es', '1', 1, 1),
(453, '78t_vb30rmedium.jpg', 'productos/vb-30r/images/78t_vb30rmedium.jpg', 14, 'es', '1', 1, 1),
(454, '81b_vb30rsmall.jpg', 'productos/vb-30r/images/81b_vb30rsmall.jpg', 15, 'es', '1', 1, 1),
(455, '17v_cd5240carrousel.jpg', 'productos/cd5240/images/17v_cd5240carrousel.jpg', 12, 'es', '1', 0, 0),
(456, '14q_cd5240big.jpg', 'productos/cd5240/images/14q_cd5240big.jpg', 13, 'es', '1', 1, 1),
(457, '63w_cd5240medium.jpg', 'productos/cd5240/images/63w_cd5240medium.jpg', 14, 'es', '1', 1, 1),
(458, '26d_cd5240small.jpg', 'productos/cd5240/images/26d_cd5240small.jpg', 15, 'es', '1', 1, 1),
(459, '85v_srp350big.jpg', 'productos/srp-350/images/85v_srp350big.jpg', 13, 'es', '1', 1, 1),
(460, '65d_srp350medium.jpg', 'productos/srp-350/images/65d_srp350medium.jpg', 14, 'es', '1', 1, 1),
(461, '70b_srp350big.jpg', 'productos/srp-350/images/70b_srp350big.jpg', 13, 'es', '1', 1, 2),
(462, '89j_srp350medium.jpg', 'productos/srp-350/images/89j_srp350medium.jpg', 14, 'es', '1', 1, 2),
(463, '26o_srp350big.jpg', 'productos/srp-350/images/26o_srp350big.jpg', 13, 'es', '1', 1, 3),
(464, '72r_srp350medium.jpg', 'productos/srp-350/images/72r_srp350medium.jpg', 14, 'es', '1', 1, 3),
(465, '50q_ls21530eccarrousel.jpg', 'productos/ls21530ec/images/50q_ls21530eccarrousel.jpg', 12, 'es', '1', 0, 0),
(466, '93t_ls21530ecbig.jpg', 'productos/ls21530ec/images/93t_ls21530ecbig.jpg', 13, 'es', '1', 1, 1),
(467, '44g_ls21530ecmedium.jpg', 'productos/ls21530ec/images/44g_ls21530ecmedium.jpg', 14, 'es', '1', 1, 1),
(468, '14z_ls21530ecsmall.jpg', 'productos/ls21530ec/images/14z_ls21530ecsmall.jpg', 15, 'es', '1', 1, 1),
(469, '12b_ls21530ecbig.jpg', 'productos/ls21530ec/images/12b_ls21530ecbig.jpg', 13, 'es', '1', 1, 2),
(470, '74y_ls21530ecmedium.jpg', 'productos/ls21530ec/images/74y_ls21530ecmedium.jpg', 14, 'es', '1', 1, 2),
(471, '23h_ls21530ecsmall.jpg', 'productos/ls21530ec/images/23h_ls21530ecsmall.jpg', 15, 'es', '1', 1, 2),
(472, '80n_ls21530ecbig.jpg', 'productos/ls21530ec/images/80n_ls21530ecbig.jpg', 13, 'es', '1', 1, 3),
(473, '87h_ls21530ecmedium.jpg', 'productos/ls21530ec/images/87h_ls21530ecmedium.jpg', 14, 'es', '1', 1, 3),
(474, '82h_ls21530ecsmall.jpg', 'productos/ls21530ec/images/82h_ls21530ecsmall.jpg', 15, 'es', '1', 1, 3),
(475, '25x_ls21530ecbig.jpg', 'productos/ls21530ec/images/25x_ls21530ecbig.jpg', 13, 'es', '1', 1, 4),
(476, '98z_ls21530ecmedium.jpg', 'productos/ls21530ec/images/98z_ls21530ecmedium.jpg', 14, 'es', '1', 1, 4),
(477, '70a_ls21530ecsmall.jpg', 'productos/ls21530ec/images/70a_ls21530ecsmall.jpg', 15, 'es', '1', 1, 4),
(478, '25n_Brochure.pdf', 'productos/ls21530ec/files/25n_Brochure.pdf', 2, 'es', '1', 1, 0),
(479, '16f_L. Certificado  Aprobacion.pdf', 'productos/ls21530ec/files/16f_L. Certificado  Aprobacion.pdf', 10, 'es', '1', 1, 0),
(480, '74l_Manual de Usuario.pdf', 'productos/ls21530ec/files/74l_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(481, '33s_N. Covenin 3633-2000.pdf', 'productos/ls21530ec/files/33s_N. Covenin 3633-2000.pdf', 5, 'es', '1', 1, 0),
(482, '27k_N. Covenin 3695-2001.pdf', 'productos/ls21530ec/files/27k_N. Covenin 3695-2001.pdf', 4, 'es', '1', 1, 0),
(483, '35d_L.  Reforma Parcial.pdf', 'productos/ls21530ec/files/35d_L.  Reforma Parcial.pdf', 1, 'es', '1', 1, 0),
(484, '39w_Brochure.pdf', 'productos/cr68afj/files/39w_Brochure.pdf', 2, 'es', '1', 1, 0),
(485, '90g_Autorizaciones.pdf', 'productos/cr68afj/files/90g_Autorizaciones.pdf', 16, 'es', '1', 1, 0),
(486, '66j_Utilitario.zip', 'productos/tallydascom1125/files/66j_Utilitario.zip', 20, 'es', '1', 1, 0),
(487, '77n_Ficha Tecnica.pdf', 'productos/tallydascom1125/files/77n_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(488, '19p_Drivers.zip', 'productos/tallydascom1125/files/19p_Drivers.zip', 19, 'es', '1', 1, 0),
(489, '93m_Guia de Inicio Rapido.pdf', 'productos/tallydascom1125/files/93m_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(490, '11x_Ficha Tecnica.pdf', 'productos/tallydascom1500/files/11x_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(491, '20i_Ficha Tecnica.pdf', 'productos/tallydascom2610/files/20i_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(492, '68j_Ficha Tecnica.pdf', 'productos/tallydascom2380/files/68j_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(493, '67r_Ficha Tecnica.pdf', 'productos/t2365/files/67r_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(494, '43x_Ficha Tecnica.pdf', 'productos/tallydascom2600/files/43x_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(495, '80u_Guia de Inicio Rapido.pdf', 'productos/tallydascom1225/files/80u_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(496, '47f_Utilitario.zip', 'productos/tallydascom1225/files/47f_Utilitario.zip', 20, 'es', '1', 1, 0),
(497, '82r_Drivers.zip', 'productos/tallydascom1225/files/82r_Drivers.zip', 19, 'es', '1', 1, 0),
(498, '91t_tallydascom1225big.jpg', 'productos/tallydascom1225/images/91t_tallydascom1225big.jpg', 13, 'es', '1', 1, 1),
(499, '58n_tallydascom1225big.jpg', 'productos/tallydascom1225/images/58n_tallydascom1225big.jpg', 13, 'es', '1', 1, 2),
(500, '63b_Brochure.pdf', 'productos/p2506w/files/63b_Brochure.pdf', 2, 'es', '1', 1, 0),
(501, '82n_Guia de Inicio Rapido.pdf', 'productos/7206/files/82n_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(502, '30n_Guia de Inicio Rapido.pdf', 'productos/7106/files/30n_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(503, '56n_Manual de Usuario.pdf', 'productos/mip-480/files/56n_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(504, '77i_Ficha Tecnica.pdf', 'productos/mip-480/files/77i_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(505, '74e_Guia de Inicio Rapido.pdf', 'productos/mip-480/files/74e_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(506, '18a_dp520carrousel.jpg', 'productos/dp-520/images/18a_dp520carrousel.jpg', 12, 'es', '1', 1, 0),
(507, '69x_dp520big.jpg', 'productos/dp-520/images/69x_dp520big.jpg', 13, 'es', '1', 1, 1),
(508, '93e_dp520medium.jpg', 'productos/dp-520/images/93e_dp520medium.jpg', 14, 'es', '1', 1, 1),
(509, '31j_Ficha Tecnica.pdf', 'productos/dp-520/files/31j_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(510, '28d_Guia de Inicio Rapido.pdf', 'productos/dp-520/files/28d_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(511, '25c_hka1080big.jpg', 'productos/hka1080/images/25c_hka1080big.jpg', 13, 'es', '1', 1, 2),
(512, '69a_hka1080medium.jpg', 'productos/hka1080/images/69a_hka1080medium.jpg', 14, 'es', '1', 1, 2),
(513, '17q_hka1080carrousel.jpg', 'productos/hka1080/images/17q_hka1080carrousel.jpg', 12, 'es', '1', 1, 0),
(514, '64r_hka1080big.jpg', 'productos/hka1080/images/64r_hka1080big.jpg', 13, 'es', '1', 1, 4),
(515, '70v_hka1080medium.jpg', 'productos/hka1080/images/70v_hka1080medium.jpg', 14, 'es', '1', 1, 4),
(516, '88h_hka380carrousel.jpg', 'productos/hka380/images/88h_hka380carrousel.jpg', 12, 'es', '1', 1, 0),
(517, '17a_hka380big.jpg', 'productos/hka380/images/17a_hka380big.jpg', 13, 'es', '1', 1, 1),
(518, '22n_hka380medium.jpg', 'productos/hka380/images/22n_hka380medium.jpg', 14, 'es', '1', 1, 1),
(519, '36e_hka380small.jpg', 'productos/hka380/images/36e_hka380small.jpg', 15, 'es', '1', 1, 1),
(520, '99k_hka380big.jpg', 'productos/hka380/images/99k_hka380big.jpg', 13, 'es', '1', 1, 2),
(521, '32n_hka380medium.jpg', 'productos/hka380/images/32n_hka380medium.jpg', 14, 'es', '1', 1, 2),
(522, '50s_hka380small.jpg', 'productos/hka380/images/50s_hka380small.jpg', 15, 'es', '1', 1, 2),
(523, '73j_hka380big.jpg', 'productos/hka380/images/73j_hka380big.jpg', 13, 'es', '1', 1, 3),
(524, '16k_hka380medium.jpg', 'productos/hka380/images/16k_hka380medium.jpg', 14, 'es', '1', 1, 3),
(525, '62z_hka380small.jpg', 'productos/hka380/images/62z_hka380small.jpg', 15, 'es', '1', 1, 3),
(526, '50r_hka380big.jpg', 'productos/hka380/images/50r_hka380big.jpg', 13, 'es', '1', 1, 4),
(527, '88f_hka380medium.jpg', 'productos/hka380/images/88f_hka380medium.jpg', 14, 'es', '1', 1, 4),
(528, '87q_hka380small.jpg', 'productos/hka380/images/87q_hka380small.jpg', 15, 'es', '1', 1, 4),
(529, '24z_ao1xcarrousel.jpg', 'productos/ao1x/images/24z_ao1xcarrousel.jpg', 12, 'es', '1', 1, 0),
(530, '20h_ao1xbig.jpg', 'productos/ao1x/images/20h_ao1xbig.jpg', 13, 'es', '1', 1, 1),
(531, '89o_ao1xmedium.jpg', 'productos/ao1x/images/89o_ao1xmedium.jpg', 14, 'es', '1', 1, 1),
(532, '50i_ao1xsmall.jpg', 'productos/ao1x/images/50i_ao1xsmall.jpg', 15, 'es', '1', 1, 1),
(533, '34k_ao1xbig.jpg', 'productos/ao1x/images/34k_ao1xbig.jpg', 13, 'es', '1', 1, 2),
(534, '57b_ao1xmedium.jpg', 'productos/ao1x/images/57b_ao1xmedium.jpg', 14, 'es', '1', 1, 2),
(535, '97s_ao1xsmall.jpg', 'productos/ao1x/images/97s_ao1xsmall.jpg', 15, 'es', '1', 1, 2),
(536, '64b_ao1xbig.jpg', 'productos/ao1x/images/64b_ao1xbig.jpg', 13, 'es', '1', 1, 3),
(537, '92g_ao1xmedium.jpg', 'productos/ao1x/images/92g_ao1xmedium.jpg', 14, 'es', '1', 1, 3),
(538, '85b_ao1xsmall.jpg', 'productos/ao1x/images/85b_ao1xsmall.jpg', 15, 'es', '1', 1, 3),
(539, '46g_ao1xbig.jpg', 'productos/ao1x/images/46g_ao1xbig.jpg', 13, 'es', '1', 1, 4),
(540, '98l_ao1xmedium.jpg', 'productos/ao1x/images/98l_ao1xmedium.jpg', 14, 'es', '1', 1, 4),
(541, '94y_ao1xsmall.jpg', 'productos/ao1x/images/94y_ao1xsmall.jpg', 15, 'es', '1', 1, 4),
(542, '49k_ao4xcarrousel.jpg', 'productos/ao4x/images/49k_ao4xcarrousel.jpg', 12, 'es', '1', 1, 0),
(543, '35z_ao4xbig.jpg', 'productos/ao4x/images/35z_ao4xbig.jpg', 13, 'es', '1', 1, 1),
(544, '67s_ao4xmedium.jpg', 'productos/ao4x/images/67s_ao4xmedium.jpg', 14, 'es', '1', 1, 1),
(545, '77v_ao4xsmall.jpg', 'productos/ao4x/images/77v_ao4xsmall.jpg', 15, 'es', '1', 1, 1),
(546, '73p_ao4xbig.jpg', 'productos/ao4x/images/73p_ao4xbig.jpg', 13, 'es', '1', 1, 2),
(547, '41d_ao4xmedium.jpg', 'productos/ao4x/images/41d_ao4xmedium.jpg', 14, 'es', '1', 1, 2),
(548, '70m_ao4xsmall.jpg', 'productos/ao4x/images/70m_ao4xsmall.jpg', 15, 'es', '1', 1, 2);
INSERT INTO `files` (`id`, `name`, `path`, `file_type_id`, `language_id`, `version`, `active`, `order`) VALUES
(549, '30u_ao4xbig.jpg', 'productos/ao4x/images/30u_ao4xbig.jpg', 13, 'es', '1', 1, 3),
(550, '82o_ao4xmedium.jpg', 'productos/ao4x/images/82o_ao4xmedium.jpg', 14, 'es', '1', 1, 3),
(551, '88c_ao4xsmall.jpg', 'productos/ao4x/images/88c_ao4xsmall.jpg', 15, 'es', '1', 1, 3),
(552, '41o_ao4xbig.jpg', 'productos/ao4x/images/41o_ao4xbig.jpg', 13, 'es', '1', 1, 4),
(553, '24t_ao4xmedium.jpg', 'productos/ao4x/images/24t_ao4xmedium.jpg', 14, 'es', '1', 1, 4),
(554, '31o_ao4xsmall.jpg', 'productos/ao4x/images/31o_ao4xsmall.jpg', 15, 'es', '1', 1, 4),
(555, '98h_pt05carrousel.jpg', 'productos/pt05/images/98h_pt05carrousel.jpg', 12, 'es', '1', 1, 0),
(556, '18p_pt05big.jpg', 'productos/pt05/images/18p_pt05big.jpg', 13, 'es', '1', 1, 1),
(557, '70o_pt05medium.jpg', 'productos/pt05/images/70o_pt05medium.jpg', 14, 'es', '1', 1, 1),
(558, '66e_pt05small.jpg', 'productos/pt05/images/66e_pt05small.jpg', 15, 'es', '1', 1, 1),
(559, '36c_pt05big.jpg', 'productos/pt05/images/36c_pt05big.jpg', 13, 'es', '1', 1, 2),
(560, '73j_pt05medium.jpg', 'productos/pt05/images/73j_pt05medium.jpg', 14, 'es', '1', 1, 2),
(561, '48r_pt05small.jpg', 'productos/pt05/images/48r_pt05small.jpg', 15, 'es', '1', 1, 2),
(562, '50n_pt05big.jpg', 'productos/pt05/images/50n_pt05big.jpg', 13, 'es', '1', 1, 3),
(563, '44n_pt05medium.jpg', 'productos/pt05/images/44n_pt05medium.jpg', 14, 'es', '1', 1, 3),
(564, '21a_pt05small.jpg', 'productos/pt05/images/21a_pt05small.jpg', 15, 'es', '1', 1, 3),
(565, '40y_pt05big.jpg', 'productos/pt05/images/40y_pt05big.jpg', 13, 'es', '1', 1, 4),
(566, '84a_pt05medium.jpg', 'productos/pt05/images/84a_pt05medium.jpg', 14, 'es', '1', 1, 4),
(567, '37k_pt05small.jpg', 'productos/pt05/images/37k_pt05small.jpg', 15, 'es', '1', 1, 4),
(568, '92c_Brochure.pdf', 'productos/pt05/files/92c_Brochure.pdf', 2, 'es', '1', 1, 0),
(569, '97j_Guia de Inicio Rapido.pdf', 'productos/pt05/files/97j_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(570, '66p_Brochure.pdf', 'productos/ao4x/files/66p_Brochure.pdf', 2, 'es', '1', 1, 0),
(571, '75a_Manual de Usuario.pdf', 'productos/ao4x/files/75a_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(572, '22e_Brochure.pdf', 'productos/ao1x/files/22e_Brochure.pdf', 2, 'es', '1', 1, 0),
(573, '27q_Manual de Usuario.pdf', 'productos/ao1x/files/27q_Manual de Usuario.pdf', 3, 'es', '1', 1, 0),
(574, '73q_Brochure.pdf', 'productos/hka1080/files/73q_Brochure.pdf', 2, 'es', '1', 1, 0),
(575, '45m_tallydascom1125carrousel.png', 'productos/tallydascom1125/images/45m_tallydascom1125carrousel.png', 12, 'es', '1', 1, 0),
(576, '73r_tallydascom1500carrousel.png', 'productos/tallydascom1500/images/73r_tallydascom1500carrousel.png', 12, 'es', '1', 1, 0),
(577, '43d_tallydascom1500big.jpg', 'productos/tallydascom1500/images/43d_tallydascom1500big.jpg', 13, 'es', '1', 1, 1),
(578, '80u_tallydascom1500medium.png', 'productos/tallydascom1500/images/80u_tallydascom1500medium.png', 14, 'es', '1', 1, 1),
(579, '10q_tallydascom2610carrousel.jpg', 'productos/tallydascom2610/images/10q_tallydascom2610carrousel.jpg', 12, 'es', '1', 1, 0),
(580, '55t_tallydascom2610big.jpg', 'productos/tallydascom2610/images/55t_tallydascom2610big.jpg', 13, 'es', '1', 1, 1),
(581, '51t_tallydascom2610medium.jpg', 'productos/tallydascom2610/images/51t_tallydascom2610medium.jpg', 14, 'es', '1', 1, 1),
(582, '34g_tallydascom2380carrousel.png', 'productos/tallydascom2380/images/34g_tallydascom2380carrousel.png', 12, 'es', '1', 1, 0),
(583, '49o_tallydascom2380big.png', 'productos/tallydascom2380/images/49o_tallydascom2380big.png', 13, 'es', '1', 1, 1),
(584, '85l_tallydascom2380medium.png', 'productos/tallydascom2380/images/85l_tallydascom2380medium.png', 14, 'es', '1', 1, 1),
(585, '24s_tallydascom2380big.png', 'productos/tallydascom2380/images/24s_tallydascom2380big.png', 13, 'es', '1', 1, 2),
(586, '52y_tallydascom2380medium.png', 'productos/tallydascom2380/images/52y_tallydascom2380medium.png', 14, 'es', '1', 1, 2),
(587, '89w_tallydascom2600carrousel.jpg', 'productos/tallydascom2600/images/89w_tallydascom2600carrousel.jpg', 12, 'es', '1', 1, 0),
(588, '94s_tallydascom2600big.jpg', 'productos/tallydascom2600/images/94s_tallydascom2600big.jpg', 13, 'es', '1', 1, 1),
(589, '23v_tallydascom2600medium.jpg', 'productos/tallydascom2600/images/23v_tallydascom2600medium.jpg', 14, 'es', '1', 1, 1),
(590, '13c_tallydascom1225carrousel.jpg', 'productos/tallydascom1225/images/13c_tallydascom1225carrousel.jpg', 12, 'es', '1', 1, 0),
(591, '50i_tallydascom1225medium.jpg', 'productos/tallydascom1225/images/50i_tallydascom1225medium.jpg', 14, 'es', '1', 1, 1),
(592, '56h_tallydascom1225medium.jpg', 'productos/tallydascom1225/images/56h_tallydascom1225medium.jpg', 14, 'es', '1', 1, 2),
(593, '14g_t2365carrousel.png', 'productos/t2365/images/14g_t2365carrousel.png', 12, 'es', '1', 1, 0),
(594, '57o_t2365big.png', 'productos/t2365/images/57o_t2365big.png', 13, 'es', '1', 1, 1),
(595, '77w_t2365medium.png', 'productos/t2365/images/77w_t2365medium.png', 14, 'es', '1', 1, 1),
(596, '45y_hka5803big.jpg', 'productos/hka5803/images/45y_hka5803big.jpg', 13, 'es', '1', 1, 1),
(597, '44g_hka5803medium.jpg', 'productos/hka5803/images/44g_hka5803medium.jpg', 14, 'es', '1', 1, 1),
(598, '73g_hka5803carrousel.jpg', 'productos/hka5803/images/73g_hka5803carrousel.jpg', 12, 'es', '1', 1, 0),
(599, '22x_hka5803big.jpg', 'productos/hka5803/images/22x_hka5803big.jpg', 13, 'es', '1', 1, 2),
(600, '43h_hka5803medium.jpg', 'productos/hka5803/images/43h_hka5803medium.jpg', 14, 'es', '1', 1, 2),
(601, '77j_hka5803big.jpg', 'productos/hka5803/images/77j_hka5803big.jpg', 13, 'es', '1', 1, 3),
(602, '73w_hka5803medium.jpg', 'productos/hka5803/images/73w_hka5803medium.jpg', 14, 'es', '1', 1, 3),
(603, '73l_sppr310carrousel.jpg', 'productos/spp-r310/images/73l_sppr310carrousel.jpg', 12, 'es', '1', 1, 0),
(604, '52f_sppr310big.jpg', 'productos/spp-r310/images/52f_sppr310big.jpg', 13, 'es', '1', 1, 1),
(605, '84h_sppr310medium.jpg', 'productos/spp-r310/images/84h_sppr310medium.jpg', 14, 'es', '1', 1, 1),
(606, '48o_sppr310big.jpg', 'productos/spp-r310/images/48o_sppr310big.jpg', 13, 'es', '1', 1, 2),
(607, '78s_sppr310medium.jpg', 'productos/spp-r310/images/78s_sppr310medium.jpg', 14, 'es', '1', 1, 2),
(608, '62j_sppr310big.jpg', 'productos/spp-r310/images/62j_sppr310big.jpg', 13, 'es', '1', 1, 3),
(609, '63r_sppr310medium.jpg', 'productos/spp-r310/images/63r_sppr310medium.jpg', 14, 'es', '1', 1, 3),
(610, '43u_sppr310big.jpg', 'productos/spp-r310/images/43u_sppr310big.jpg', 13, 'es', '1', 1, 4),
(611, '72r_sppr310meidum.jpg', 'productos/spp-r310/images/72r_sppr310meidum.jpg', 14, 'es', '1', 1, 4),
(616, '63l_ls2carrousel.jpg', 'productos/ls2/images/63l_ls2carrousel.jpg', 12, 'es', '1', 1, 0),
(617, '12l_ls2big.jpg', 'productos/ls2/images/12l_ls2big.jpg', 13, 'es', '1', 1, 1),
(618, '57u_ls2medium.jpg', 'productos/ls2/images/57u_ls2medium.jpg', 14, 'es', '1', 1, 1),
(619, '41s_ls2big.jpg', 'productos/ls2/images/41s_ls2big.jpg', 13, 'es', '1', 1, 2),
(620, '52b_ls2medium.jpg', 'productos/ls2/images/52b_ls2medium.jpg', 14, 'es', '1', 1, 2),
(621, '13l_ls2big.jpg', 'productos/ls2/images/13l_ls2big.jpg', 13, 'es', '1', 1, 3),
(622, '63w_ls2medium.jpg', 'productos/ls2/images/63w_ls2medium.jpg', 14, 'es', '1', 1, 3),
(623, '82g_ls2big.jpg', 'productos/ls2/images/82g_ls2big.jpg', 13, 'es', '1', 1, 4),
(624, '99x_ls2medium.jpg', 'productos/ls2/images/99x_ls2medium.jpg', 14, 'es', '1', 1, 4),
(625, '83q_ts4carrousel.jpg', 'productos/ts4/images/83q_ts4carrousel.jpg', 12, 'es', '1', 1, 0),
(626, '87a_ts4big.jpg', 'productos/ts4/images/87a_ts4big.jpg', 13, 'es', '1', 1, 1),
(627, '49i_ts4medium.jpg', 'productos/ts4/images/49i_ts4medium.jpg', 14, 'es', '1', 1, 1),
(628, '55m_ts4big.jpg', 'productos/ts4/images/55m_ts4big.jpg', 13, 'es', '1', 1, 2),
(629, '67k_ts4medium.jpg', 'productos/ts4/images/67k_ts4medium.jpg', 14, 'es', '1', 1, 2),
(630, '33h_ts4big.jpg', 'productos/ts4/images/33h_ts4big.jpg', 13, 'es', '1', 1, 3),
(631, '34u_ts4medium.jpg', 'productos/ts4/images/34u_ts4medium.jpg', 14, 'es', '1', 1, 3),
(632, '36i_ts4big.jpg', 'productos/ts4/images/36i_ts4big.jpg', 13, 'es', '1', 1, 4),
(633, '56k_ts4medium.jpg', 'productos/ts4/images/56k_ts4medium.jpg', 14, 'es', '1', 1, 4),
(634, '64y_lh30carrousel.jpg', 'productos/lh-30/images/64y_lh30carrousel.jpg', 12, 'es', '1', 1, 0),
(635, '62k_lh30big.jpg', 'productos/lh-30/images/62k_lh30big.jpg', 13, 'es', '1', 1, 1),
(636, '80i_lh30medium.jpg', 'productos/lh-30/images/80i_lh30medium.jpg', 14, 'es', '1', 1, 1),
(637, '30x_lh30big.jpg', 'productos/lh-30/images/30x_lh30big.jpg', 13, 'es', '1', 1, 2),
(638, '34r_lh30medium.jpg', 'productos/lh-30/images/34r_lh30medium.jpg', 14, 'es', '1', 1, 2),
(639, '88z_lh30big.jpg', 'productos/lh-30/images/88z_lh30big.jpg', 13, 'es', '1', 1, 3),
(640, '57a_lh30medium.jpg', 'productos/lh-30/images/57a_lh30medium.jpg', 14, 'es', '1', 1, 3),
(641, '51v_lh30big.jpg', 'productos/lh-30/images/51v_lh30big.jpg', 13, 'es', '1', 1, 4),
(642, '88d_lh30medium.jpg', 'productos/lh-30/images/88d_lh30medium.jpg', 14, 'es', '1', 1, 4),
(643, '45j_Brochure.pdf', 'productos/hka5803/files/45j_Brochure.pdf', 2, 'es', '1', 1, 0),
(644, '62h_Brochure.pdf', 'productos/spp-r310/files/62h_Brochure.pdf', 2, 'es', '1', 1, 0),
(645, '30c_hka80carrousel.jpg', 'productos/hka80/images/30c_hka80carrousel.jpg', 12, 'es', '1', 1, 0),
(646, '72t_hka80big.jpg', 'productos/hka80/images/72t_hka80big.jpg', 13, 'es', '1', 1, 1),
(647, '15v_hka80medium.jpg', 'productos/hka80/images/15v_hka80medium.jpg', 14, 'es', '1', 1, 1),
(648, '65f_hka80big.jpg', 'productos/hka80/images/65f_hka80big.jpg', 13, 'es', '1', 1, 2),
(649, '42a_hka80medium.jpg', 'productos/hka80/images/42a_hka80medium.jpg', 14, 'es', '1', 1, 2),
(650, '84i_hka80big.jpg', 'productos/hka80/images/84i_hka80big.jpg', 13, 'es', '1', 1, 3),
(651, '80g_hka80medium.jpg', 'productos/hka80/images/80g_hka80medium.jpg', 14, 'es', '1', 1, 3),
(652, '29y_Driver Linux.rar', 'productos/hka80/files/29y_Driver Linux.rar', 7, 'es', '1', 1, 0),
(653, '18x_Standar Printer Demo.rar', 'productos/hka80/files/18x_Standar Printer Demo.rar', 21, 'es', '1', 1, 0),
(654, '38p_Utilitario.rar', 'productos/hka80/files/38p_Utilitario.rar', 20, 'es', '1', 1, 0),
(655, '68l_Guia de Inicio Rapido.pdf', 'productos/hka80/files/68l_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(656, '80q_Guia de Inicio Rapido.pdf', 'productos/hka57/files/80q_Guia de Inicio Rapido.pdf', 17, 'es', '1', 1, 0),
(657, '24a_Drivers.rar', 'productos/hka57/files/24a_Drivers.rar', 19, 'es', '1', 1, 0),
(658, '32i_Standar Printer Demo.rar', 'productos/hka57/files/32i_Standar Printer Demo.rar', 21, 'es', '1', 1, 0),
(659, '68h_Driver Linux.rar', 'productos/hka57/files/68h_Driver Linux.rar', 7, 'es', '1', 1, 0),
(660, '22b_Utilitario.rar', 'productos/hka57/files/22b_Utilitario.rar', 20, 'es', '1', 1, 0),
(661, '76f_Ficha Tecnica.pdf', 'productos/dt-230/files/76f_Ficha Tecnica.pdf', 18, 'es', '1', 1, 0),
(662, '63i_ps1carrousel.jpg', 'productos/ps1/images/63i_ps1carrousel.jpg', 12, 'es', '1', 1, 0),
(663, '23t_ls21530eccarrousel.jpg', 'productos/ls21530ec/images/23t_ls21530eccarrousel.jpg', 12, 'es', '1', 1, 0),
(664, '95j_pp1f3carrousel.jpg', 'productos/pp1f3/images/95j_pp1f3carrousel.jpg', 12, 'es', '1', 1, 0),
(665, '36d_srp270carrousel.jpg', 'productos/srp-270/images/36d_srp270carrousel.jpg', 12, 'es', '1', 1, 0),
(666, '58v_srp350carrousel.jpg', 'productos/srp-350/images/58v_srp350carrousel.jpg', 12, 'es', '1', 1, 0),
(667, '78y_microline1120carrousel.jpg', 'productos/microline-1120/images/78y_microline1120carrousel.jpg', 12, 'es', '1', 1, 0),
(668, '51k_hsp7000carrousel.jpg', 'productos/hsp7000/images/51k_hsp7000carrousel.jpg', 12, 'es', '1', 1, 0),
(669, '91o_hka112carrousel.jpg', 'productos/hka-112/images/91o_hka112carrousel.jpg', 12, 'es', '1', 1, 0),
(670, '17l_srp280carrousel.jpg', 'productos/srp-280/images/17l_srp280carrousel.jpg', 12, 'es', '1', 1, 0),
(671, '13i_srp812carrousel.jpg', 'productos/srp-812/images/13i_srp812carrousel.jpg', 12, 'es', '1', 1, 0),
(672, '54h_pp9carrousel.jpg', 'productos/pp9/images/54h_pp9carrousel.jpg', 12, 'es', '1', 1, 0),
(673, '98l_p3100dlcarrousel.jpg', 'productos/p3100dl/images/98l_p3100dlcarrousel.jpg', 12, 'es', '1', 1, 0),
(674, '37s_cr68afcarrousel.jpg', 'productos/cr68af/images/37s_cr68afcarrousel.jpg', 12, 'es', '1', 1, 0),
(675, '85z_crd81fcarrousel.jpg', 'productos/crd81f/images/85z_crd81fcarrousel.jpg', 12, 'es', '1', 1, 0),
(676, '37v_nx5400carrousel.jpg', 'productos/nx-5400/images/37v_nx5400carrousel.jpg', 12, 'es', '1', 1, 0),
(677, '85p_crd81fjcarrousel.jpg', 'productos/crd81fj/images/85p_crd81fjcarrousel.jpg', 12, 'es', '1', 1, 0),
(678, '46m_cr68afjcarrousel.jpg', 'productos/cr68afj/images/46m_cr68afjcarrousel.jpg', 12, 'es', '1', 1, 0),
(679, '49b_cr2100carrousel.jpg', 'productos/cr2100/images/49b_cr2100carrousel.jpg', 12, 'es', '1', 1, 0),
(680, '62o_cr2300carrousel.jpg', 'productos/cr2300/images/62o_cr2300carrousel.jpg', 12, 'es', '1', 1, 0),
(681, '81u_bc2000uvcarrousel.jpg', 'productos/bc-2000uv/images/81u_bc2000uvcarrousel.jpg', 12, 'es', '1', 1, 0),
(682, '73t_bcd1000dgcarrousel.jpg', 'productos/bcd1000dg/images/73t_bcd1000dgcarrousel.jpg', 12, 'es', '1', 1, 0),
(683, '83g_gavetacarrousel.jpg', 'productos/gaveta/images/83g_gavetacarrousel.jpg', 12, 'es', '1', 1, 0),
(684, '70z_hkausblinkcarrousel.jpg', 'productos/hkausb-link/images/70z_hkausblinkcarrousel.jpg', 12, 'es', '1', 1, 0),
(685, '64l_vb30rcarrousel.jpg', 'productos/vb-30r/images/64l_vb30rcarrousel.jpg', 12, 'es', '1', 1, 0),
(686, '22g_cd5240carrousel.jpg', 'productos/cd5240/images/22g_cd5240carrousel.jpg', 12, 'es', '1', 1, 0),
(687, '79n_hka232carrousel.jpg', 'productos/hka-232/images/79n_hka232carrousel.jpg', 12, 'es', '1', 1, 0),
(688, '63p_Drivers.rar', 'productos/p2506w/files/63p_Drivers.rar', 19, 'es', '1', 1, 0),
(689, '49j_Brochure.pdf', 'productos/dt-230/files/49j_Brochure.pdf', 2, 'es', '1', 1, 0),
(690, '15z_Autorizaciones.pdf', 'productos/dt-230/files/15z_Autorizaciones.pdf', 16, 'es', '2', 1, 0),
(691, '91f_Brochure.pdf', 'productos/hka80/files/91f_Brochure.pdf', 2, 'es', '1', 1, 0),
(692, '11n_Autorizaciones.pdf', 'productos/hka80/files/11n_Autorizaciones.pdf', 16, 'es', '2', 1, 0),
(693, '10j_Brochure.pdf', 'productos/tallydascom1125/files/10j_Brochure.pdf', 2, 'es', '1', 1, 0),
(694, '39y_Autorizaciones.pdf', 'productos/tallydascom1125/files/39y_Autorizaciones.pdf', 16, 'es', '2', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `file_types`
--

CREATE TABLE IF NOT EXISTS `file_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `icon_path` tinytext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `file_types`
--

INSERT INTO `file_types` (`id`, `name`, `active`, `icon_path`) VALUES
(1, 'L.  Reforma Parcial', 1, ''),
(2, 'Brochure', 1, ''),
(3, 'Manual de Usuario', 1, ''),
(4, 'N. Covenin 3695-2001', 1, ''),
(5, 'N. Covenin 3633-2000', 1, ''),
(6, 'Driver Android', 1, ''),
(7, 'Driver Linux', 1, ''),
(8, 'Driver Mac', 1, ''),
(9, 'Driver Windows', 1, ''),
(10, 'L. Certificado  Aprobacion', 1, ''),
(11, 'Video', 1, ''),
(12, 'Img Carrousel', 1, ''),
(13, 'Img Big', 1, ''),
(14, 'Img Medium', 1, ''),
(15, 'Img Small', 1, ''),
(16, 'Autorizaciones', 1, ''),
(17, 'Guia de Inicio Rapido', 1, NULL),
(18, 'Ficha Tecnica', 1, NULL),
(19, 'Drivers', 1, NULL),
(20, 'Utilitario', 1, NULL),
(21, 'Standar Printer Demo', 1, NULL),
(22, 'CAS', 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` text COLLATE utf8_spanish_ci NOT NULL,
  `name` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `menu_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `items`
--

INSERT INTO `items` (`id`, `item`, `name`, `menu_id`, `active`) VALUES
(1, 'roles_admin', 'Administrar Permisos', 1, 1),
(2, 'productos_admin', 'Administrar Productos', 2, 1),
(3, 'brands_admin', 'Administrar Marcas', 2, 1),
(4, 'Typeproducts_admin', 'Administrar Tipo de Productos', 2, 1),
(5, 'countries_admin', 'Adminitrar Paises.', 3, 1),
(7, 'news_admin', 'Administra  Noticias', 5, 1),
(8, 'sliders_admin', 'Administrar Sliders', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `languages`
--

INSERT INTO `languages` (`id`, `name`, `active`) VALUES
('en', 'English', 1),
('es', 'Español', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mainmenu`
--

CREATE TABLE IF NOT EXISTS `mainmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_mainmenu_languages_idx` (`language_id`),
  KEY `fk_mainmenu_mainmenu1_idx` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Volcado de datos para la tabla `mainmenu`
--

INSERT INTO `mainmenu` (`id`, `name`, `active`, `parent_id`, `language_id`) VALUES
(1, 'Inicio', 1, NULL, 'es'),
(2, 'Productos', 1, NULL, 'es'),
(3, 'Nosotros', 1, NULL, 'es'),
(4, 'Utilitarios', 1, NULL, 'es'),
(5, 'Noticias', 1, NULL, 'es'),
(6, 'Acc&oacute;n Social', 1, NULL, 'es'),
(7, 'Contacto', 1, NULL, 'es'),
(8, 'Servicio', 1, NULL, 'es'),
(9, 'Empleo', 1, NULL, 'es'),
(11, 'Cajas Registradoras', 1, 2, 'es'),
(13, 'Balanzas', 1, 2, 'es'),
(15, 'Perif&eacute;ricos y otros', 1, 2, 'es'),
(16, 'Impresoras', 1, 2, 'es'),
(17, 'Luminarias Led', 1, 2, 'es'),
(18, 'Iluminaci&oacute;n Led', 1, 2, 'es'),
(19, 'HKA Print', 1, 2, 'es'),
(20, 'Factura Electr&oacute;nica', 1, 2, 'es'),
(21, 'B&aacute;sculas', 1, 2, 'es'),
(22, 'DCaja', 1, 20, 'es'),
(23, 'DFactura', 1, 20, 'es'),
(24, 'DIntegraciones', 1, 20, 'es'),
(27, 'Terminales AIO Touch', 1, 2, 'es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mainmenu_countries`
--

CREATE TABLE IF NOT EXISTS `mainmenu_countries` (
  `mainmenu_id` int(11) NOT NULL,
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `url` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `url_type_id` int(11) NOT NULL,
  KEY `fk_mainmenu_countries_mainmenu1_idx` (`mainmenu_id`),
  KEY `fk_mainmenu_countries_countries1_idx` (`country_id`),
  KEY `fk_mainmenu_countries_type_urls1_idx` (`url_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mainmenu_countries`
--

INSERT INTO `mainmenu_countries` (`mainmenu_id`, `country_id`, `order`, `active`, `url`, `url_type_id`) VALUES
(1, 've', 1, 1, 'site/index', 1),
(2, 've', 2, 1, 'products/index', 1),
(3, 've', 3, 1, 'nosotros/index', 1),
(4, 've', 4, 1, '/utilitarios', 1),
(5, 've', 5, 1, 'noticias/index', 1),
(6, 've', 6, 0, 'site/socialAction', 1),
(7, 've', 7, 1, 'contacto/index', 1),
(1, 'us', 1, 1, 'site/index', 1),
(2, 'us', 2, 1, 'products/printer', 1),
(3, 'us', 3, 1, 'nosotros/index', 1),
(5, 'us', 4, 1, 'noticias/index', 1),
(1, 'co', 1, 1, 'site/index', 1),
(2, 'co', 2, 1, 'productos/impresora', 1),
(3, 'co', 3, 1, 'nosotros/index', 1),
(5, 'co', 4, 1, 'noticias/index', 1),
(8, 'co', 5, 1, 'services/index', 1),
(7, 'co', 6, 1, 'contacto/index', 1),
(1, 'bo', 1, 1, 'site/index', 1),
(2, 'bo', 2, 1, 'productos/impresora', 1),
(3, 'bo', 3, 1, 'nosotros/index', 1),
(5, 'bo', 4, 1, 'noticias/index', 1),
(8, 'bo', 5, 1, 'services/index', 1),
(7, 'bo', 6, 1, 'contacto/index', 1),
(1, 'mx', 1, 1, 'site/index', 1),
(2, 'mx', 2, 1, 'products/printers', 1),
(3, 'mx', 3, 1, 'nosotros/index', 1),
(5, 'mx', 4, 1, 'noticias/index', 1),
(8, 'mx', 5, 1, 'services/index', 1),
(9, 'mx', 6, 1, 'site/bolsaempleo', 1),
(7, 'mx', 7, 1, 'contacto/index', 1),
(1, 'pe', 1, 1, 'site/index', 1),
(2, 'pe', 2, 1, 'producto/impresora', 1),
(3, 'pe', 3, 1, 'nosotros/index', 1),
(5, 'pe', 4, 1, 'noticias/index', 1),
(7, 'pe', 5, 1, 'contacto/index', 1),
(1, 'pa', 1, 1, 'site/index', 1),
(2, 'pa', 2, 1, 'products/index', 1),
(3, 'pa', 3, 1, 'nosotros/index', 1),
(4, 'pa', 4, 1, '/utilitarios', 1),
(7, 'pa', 5, 1, 'contacto/index', 1),
(1, 'do', 1, 1, 'site/index', 1),
(2, 'do', 2, 1, 'products/index', 1),
(3, 'do', 3, 1, 'nosotros/index', 1),
(4, 'do', 4, 1, '/utilitarios', 1),
(5, 'do', 5, 1, 'noticias/index', 1),
(7, 'do', 7, 1, 'contacto/index', 1),
(11, 'co', 1, 0, 'productos/impresoras', 1),
(13, 'co', 3, 1, 'productos/balanzas', 1),
(15, 'co', 12, 0, 'productos/impresoras', 1),
(20, 'co', 0, 1, 'facturacionElectronica/index', 1),
(16, 'co', 2, 1, 'productos/impresoras', 1),
(27, 'co', 1, 1, 'productos/terminalesaio', 1),
(18, 'co', 5, 1, 'productos/iluminacionled', 1),
(16, 've', 1, 1, 'productos/impresoras', 1),
(13, 've', 3, 1, 'productos/balanzas', 1),
(11, 've', 2, 1, 'productos/cajas', 1),
(15, 've', 4, 1, 'productos/perifericos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `icon` tinytext CHARACTER SET utf8,
  `position` tinyint(1) NOT NULL,
  `url` text COLLATE utf8_spanish_ci,
  `category` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `parent` int(11) NOT NULL,
  `module` tinytext COLLATE utf8_spanish_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `icon`, `position`, `url`, `category`, `active`, `parent`, `module`) VALUES
(1, 'Roles', '', 1, 'menus/users', 'main', 1, 1, 'menus'),
(2, 'Productos', '', 2, 'products/products/index', 'main', 1, 1, 'products'),
(3, 'Countries', '', 3, 'countries/countries/index', 'main', 1, 1, 'countries'),
(4, 'Slider', '', 4, 'sliders/sliders/index', 'main', 1, 1, 'sliders'),
(5, 'News', '', 5, 'news/news/index', 'main', 1, 1, 'news');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `publishing_date` date NOT NULL,
  `active` tinyint(1) NOT NULL,
  `image_path` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_news_languages1_idx` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `publishing_date`, `active`, `image_path`, `language_id`) VALUES
(1, 'The Factory HKA presentó muestras de innovación tecnológica fiscal en el EuroShop 2014\n', 'Celebrado en Dusseldorf, Alemania del 16 al 20 de febrero, El Euroshop está denominado como la Feria Líder de Retail en el mundo. En ella, se dieron cita los principales actores del sector, expositores que abarcan desde líderes globales del mercado hasta empresas jóvenes emergentes presentando avances en tecnología retail y eficiencia sostenible energética. Por su parte, The Factory HKA de la mano con Bixolon mostró la nueva generación de módulos fiscales desarrollados por nuestra empresa y que están presentes en los nuevos modelos de impresoras térmicas y portátiles de Bixolon. Dichos módulos, están diseñados para adaptarse a las necesidades de los países EMEA (Europa, Medio Este y África) donde The Factory HKA actualmente expande su mercado. En el evento, The Factory HKA presentó también soluciones de Facturación Electrónica asociado a los sistemas POS que son integrables a los países del área.', '2014-03-05', 1, 'noticias/96r_euroshop.jpg', 'es'),
(2, 'The Factory Hka por primera vez en Bolivia a través de Expoteleinfo', '“THE FACTORY HKA, es una empresa internacional, ágil y moderna con oficinas en USA, Venezuela, Perú, México, República Dominicana, Colombia y Panamá, y, a través de nuestra sociedad con DASCOM, tenemos represen - tantes en toda América y proyección mundial. Brindamos una amplia gama de opciones a nuestros clientes, siempre en busca de los mejores procesos de trabajo para garantizar la calidad de nuestros servicios y productos”. Nos comento María Mejía, Directora Comercial de The FACTORY HKA de Venezuela. “Nuestro equipo de trabajo está conformado por un grupo de profesionales con una amplia experiencia dedicados al: desarrollo, ventas, ensamble y soporte técnico de Equipos Electrónicos, Balanzas, Iluminación LED, Cajas registradoras, Impresoras y los distintos sistemas, para soportarlos y dar valor agregado a nuestros clientes”. ¿A qué atribuye el éxito de The Factory? Uno de nuestros factores claves de éxito es la conformación de un departamento de Investigación y Desarrollo en constante crecimiento, donde contamos con más de 70 profesionales de varias ramas de la ingeniería y diseño, tales como: Ingeniería Electrónica, Ingeniería Eléctrica, Ingeniería Informática, Ingeniería de Sistemas, Ingeniería Mecánica y Diseño Mecánico, Diseño Industrial y Diseño Gráfico, gracias a todo lo cual tenemos la capacidad de: Diseño de hardware y desarrollo de circuitos y tarjetas electrónicas para sistemas embebidos, basados en tecnología SMD. Desarrollo de Firmware de sistemas fiscales y sistemas embebidos en general. ç Desarrollo de Software e Interfaces para aplicaciones asociadas a nuestros productos en múltiples plataformas de desarrollo y sistemas operativos. Diseño de metalmecánica y estructuras de montaje para los equipos electrónicos desarrollados. Diseño y desarrollo de aplicaciones web. Diseño gráfico para complementar los desarrollos web y campañas publicitarias y mercadeo. Más de diez años en el mercado nos dan la experiencia necesaria para conocer los gustos de nuestros clientes lo suficiente para ofrecerles exactamente lo que buscan. Nuestro equipo de ventas se encargara de responder todas sus preguntas, y asesorarlo al momento de la adquisición de cualquiera de nuestros productos. Adicionalmente contamos una unidad de producción en la cual se realiza el ensamblaje de los módulos desarrollados por la empresa en la que se tienen líneas de producción adaptables según el producto a ensamblar. En esta oportunidad estamos llegando a Expoteleinfo 2014 con la intención de incursionar en el mercado Boliviano y presentarles nuestros productos de alta calidad, les traemos una amplia gama de Balanzas, Impresoras de Punto de Venta y Matriz de Punto, Impresoras Móviles, Impresoras Térmicas, Cajas Registradoras y Toners. Los invitamos a visitar el stand de la empresa líder en soluciones de impresión comercial a nivel internacional, THE FACTORY HKA. <br><br> Fuente: http://www.teleinfopress.com/', '2013-09-24', 1, 'noticias/34n_expoteleinfo.jpg', 'es'),
(3, 'The Factory HKA Presente en NRF 2014', 'Denominado ya como el Retail’s Big Show 2014, dicho evento contó con los grandes en materia de software retail a nivel internacional convirtiéndose como su propio nombre lo dice en el Gran Show retail de este año. Por su parte, The Factory HKA estuvo representada por su Gerente de Tecnologia Ing. Manuel Rojas Duque y su Gerente de Desarrollo de Hardware Ing. Hector Pinedo, quienes mostraron la nueva generación de módulos fiscales desarrollados por nuestra empresa y que están presentes en los nuevos modelos de impresoras Bixolon, asi como también el anuncio oficial de la incorporación de la Facturación Electrónica a nuestro portafolio de soluciones fiscales. Los Módulos fiscales de nueva generación desarrollados por The Factory HKA, están diseñados para adaptarse a las necesidades de los países donde se aplica o esta en proceso de aplicarse el uso de equipos fiscales, por otra parte un paso mas adelante se incorpora la facturación electrónica a nuestra gama de soluciones.', '2016-04-01', 1, 'noticias/25e_nrf.jpg', 'es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news_country`
--

CREATE TABLE IF NOT EXISTS `news_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `new_id` int(11) NOT NULL,
  `url` tinytext CHARACTER SET utf8 NOT NULL,
  `url_type_id` int(11) NOT NULL,
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_news_country_countries1_idx` (`country_id`),
  KEY `fk_news_country_type_urls1_idx` (`url_type_id`),
  KEY `fk_news_country_news1_idx` (`new_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `news_country`
--

INSERT INTO `news_country` (`id`, `new_id`, `url`, `url_type_id`, `country_id`, `order`) VALUES
(1, 1, 'noticia1', 1, 'co', 1),
(2, 2, 'noticia2', 1, 'co', 2),
(3, 3, 'noticia3', 1, 'co', 3),
(4, 2, 'noticias2', 1, 've', 2),
(5, 3, 'noticias3', 1, 've', 3),
(6, 1, 'noticias1', 1, 've', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_unicode_ci,
  `model` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `url` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `brand_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_products_brands1_idx` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=61 ;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `model`, `url`, `active`, `brand_id`) VALUES
(1, 'CR68AF', 'CR68AF', 'aclas-cr68af', 1, 1),
(2, 'CRD81F', 'CRD81F', 'aclas-crd81f', 1, 1),
(3, 'NX-5400', 'NX-5400', 'uniwell-nx5400', 1, 6),
(4, 'SRP-350 IFA', 'SRP-350 IFA', 'imperesora-apuesta-sro350', 0, 2),
(5, 'PP1F3', 'PP1F3', 'impresora-aclas-pp1f3', 1, 1),
(6, 'SRP-270', 'SRP-270', 'impresora-fiscal-srp270', 1, 2),
(7, 'Impresora Fiscal SRP-350', 'SRP-350', 'impresora-fiscal-srp350', 1, 2),
(8, 'Impresora ML1120', 'MICROLINE-1120', 'impresora-microline-1120', 1, 7),
(9, 'Impresora Fiscal HSP7000', 'HSP7000', 'impresora-fiscal-hsp7000', 1, 8),
(10, 'Papel Térmico', 'Papel Térmico', 'papel-termico', 1, 9),
(11, 'Caja Registradora CRD81FJ', 'CRD81FJ', 'aclas-crd81fj', 1, 1),
(12, 'Caja Registradora CR68AFJ', 'CR68AFJ', 'aclas-cr68afj', 1, 1),
(14, 'Impresora Fiscal HKA112', 'HKA-112', 'impresora-fiscal-hka112', 1, 4),
(15, 'Balanza PS1', 'PS1', 'balanza-ps1', 1, 1),
(17, 'Impresora Fiscal SRP-280', 'SRP-280', 'impresora-fiscal-srp280', 1, 2),
(18, 'Caja Registradora CR2100', 'CR2100', 'aclas-cr2100', 1, 1),
(19, 'Impresora Fiscal SRP-812', 'SRP-812', 'impresora-fiscal-srp812', 1, 2),
(20, 'Balanza OS2X', 'OS2X', 'balanza-os2x', 1, 1),
(21, 'Caja Registradora CR2300', 'CR2300', 'aclas-cr2300', 1, 1),
(22, 'Impresora Fiscal PP9', 'PP9', 'impresora-fiscal-pp9', 1, 1),
(24, 'Impresora Fiscal P3100DL', 'P3100DL', 'impresora-fiscal-p3100dl', 1, 10),
(25, 'Impresora Punto de Venta DT-230', 'DT-230', 'punto-venta-dt230', 1, 5),
(26, 'Impresora Punto de Venta HKA57', 'HKA57', 'impresora-hka57', 1, 4),
(27, 'Impresora Punto de Venta HKA80', 'HKA80', 'impresora-hka80', 1, 4),
(28, 'Impresora Tally Dascom 1125', 'Tally Dascom 1125', 'impresora-tally-1125', 1, 5),
(29, 'Impresora Tally Dascom 1500', 'Tally Dascom 1500', 'impresora-tally-1500', 1, 5),
(30, 'Impresora Tally Dascom 2610', 'Tally Dascom 2610', 'impresora-tally-2610', 1, 5),
(31, 'Impresora Tally Dascom 2380', 'Tally Dascom 2380', 'impresora-tally-dascom-2380', 1, 5),
(32, 'Impresora Tally T2365', 'T2365', 'impresora-t2365', 1, 5),
(33, 'Impresora Tally Dascom 2600', 'Tally Dascom 2600', 'impresora-tally-2600', 1, 5),
(34, 'Impresora Tally Dascom 1225', 'Tally Dascom 1225', 'impresora-tally-1225', 1, 5),
(35, 'Impresora Láser P2506W', 'P2506W', 'impresora-laser-p2506w', 1, 10),
(36, 'Impresora Térmica 7010', '7010', 'impresora-termica-7010', 1, 5),
(37, 'Impresora T&eacute;rmica 7206', '7206', 'impresora-termica-7206', 1, 5),
(38, 'Impresora T&eacute;rmica 7106', '7106', 'impresora-termica-7106', 1, 5),
(39, 'Impresora Especialidad MIP-480', 'MIP-480', 'impresora-especialidad-mip4080', 1, 5),
(40, 'Impresora Móvil DP-520', 'DP-520', 'impresora-movil-dp520', 0, 5),
(41, 'Impresora Móvil DP-540', 'DP-540', 'impresora-movil-dp540', 0, 5),
(42, 'Impresora Móvil DP-550', 'DP-550', 'impresora-movil-dp550', 0, 5),
(43, 'Terminal AIO Touch HKA 1080', 'HKA 1080', 'aio-hka1080', 1, 4),
(44, 'BC-2000UV', 'BC-2000UV', 'perifericos-y-otros-bc2000uv', 1, 11),
(45, 'BCD1000DG ', 'BCD1000DG ', 'perifericos-y-otros-bcd1000dg', 1, 2),
(46, 'Gaveta', 'Gaveta', 'perifericos-y-otros-gaveta', 1, 9),
(47, 'HKA USB-LINK', 'HKA USB-LINK', 'perifericos-y-otros-hkausblink', 1, 4),
(48, 'HKA-232', 'HKA-232', 'perifericos-y-otros-hka232', 1, 4),
(49, 'VB-30R', 'VB-30R', 'perifericos-y-otros-vb-30R', 1, 12),
(50, 'CD5240', 'CD5240', 'perifericos-y-otros-cd5240', 1, 1),
(51, 'LS21530EC', 'LS21530EC', 'bascula-ls2', 1, 1),
(52, 'HKA 380', 'HKA 380', 'hka-380', 1, 4),
(53, 'AO1X', 'AO1X', 'ao1x', 1, 1),
(54, 'AO4X', 'AO4X', 'ao4x', 1, 1),
(55, 'PT05', 'PT05', 'pt05', 1, 1),
(56, 'HKA 5803', 'HKA 5803', 'hka5803', 1, 4),
(57, 'SPP-R310', 'SPP-R310', 'spp-r310', 1, 2),
(58, 'Balanzas LS2', 'LS2', 'balanza-ls2', 1, 1),
(59, 'Balanza TS4', 'TS4', 'ts4', 1, 1),
(60, 'Balanza LH-30', 'LH-30', 'lh-30', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_files_countries`
--

CREATE TABLE IF NOT EXISTS `products_files_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `file_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_file_country_countries1_idx` (`country_id`),
  KEY `fk_product_file_country_file_types1_idx` (`file_id`),
  KEY `fk_product_file_country_products1_idx` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1080 ;

--
-- Volcado de datos para la tabla `products_files_countries`
--

INSERT INTO `products_files_countries` (`id`, `country_id`, `file_id`, `product_id`, `active`) VALUES
(1, 'bo', 1, 1, 0),
(2, 've', 1, 1, 0),
(3, 'bo', 2, 1, 0),
(4, 've', 2, 1, 1),
(5, 'bo', 3, 1, 0),
(6, 've', 3, 1, 1),
(7, 'bo', 4, 1, 0),
(8, 've', 4, 1, 1),
(9, 'bo', 5, 1, 0),
(10, 've', 5, 1, 1),
(11, 'bo', 6, 1, 0),
(12, 've', 6, 1, 1),
(13, 'bo', 7, 1, 0),
(14, 've', 7, 1, 1),
(15, 'bo', 8, 1, 0),
(16, 've', 8, 1, 1),
(17, 'bo', 9, 1, 0),
(18, 've', 9, 1, 1),
(19, 'bo', 10, 1, 0),
(20, 've', 10, 1, 1),
(21, 'bo', 11, 1, 0),
(22, 've', 11, 1, 1),
(23, 'bo', 12, 1, 0),
(24, 've', 12, 1, 1),
(25, 'bo', 13, 1, 0),
(26, 've', 13, 1, 1),
(27, 'bo', 14, 1, 0),
(28, 've', 14, 1, 1),
(29, 've', 15, 2, 1),
(30, 've', 16, 2, 0),
(31, 've', 17, 2, 1),
(32, 've', 18, 2, 1),
(33, 've', 19, 2, 1),
(34, 've', 20, 2, 1),
(35, 've', 21, 2, 1),
(36, 've', 22, 2, 1),
(37, 've', 23, 2, 1),
(38, 've', 24, 2, 1),
(39, 've', 25, 2, 1),
(40, 've', 26, 2, 1),
(41, 've', 27, 2, 1),
(42, 've', 28, 2, 1),
(43, 've', 29, 3, 1),
(44, 've', 30, 3, 0),
(45, 've', 31, 3, 1),
(46, 've', 32, 3, 1),
(47, 've', 33, 3, 1),
(48, 've', 34, 3, 1),
(49, 've', 35, 3, 1),
(50, 've', 36, 3, 1),
(51, 've', 37, 3, 1),
(52, 've', 38, 3, 1),
(53, 've', 39, 3, 1),
(54, 've', 40, 3, 1),
(55, 've', 41, 3, 1),
(56, 've', 42, 3, 1),
(57, 've', 43, 5, 1),
(58, 've', 44, 5, 0),
(59, 've', 45, 5, 1),
(60, 've', 46, 5, 1),
(61, 've', 47, 5, 1),
(62, 've', 48, 6, 1),
(63, 've', 49, 6, 0),
(64, 've', 50, 6, 1),
(65, 've', 51, 6, 1),
(66, 've', 52, 6, 1),
(67, 've', 53, 6, 1),
(68, 've', 54, 6, 1),
(69, 've', 55, 6, 1),
(70, 've', 56, 7, 0),
(71, 've', 57, 7, 1),
(72, 've', 58, 7, 1),
(73, 've', 59, 7, 1),
(74, 've', 60, 7, 1),
(75, 've', 61, 7, 1),
(76, 've', 62, 7, 1),
(77, 've', 63, 7, 1),
(78, 've', 64, 7, 1),
(79, 've', 65, 7, 1),
(80, 've', 66, 7, 1),
(81, 've', 67, 7, 1),
(82, 've', 68, 7, 1),
(83, 've', 69, 7, 1),
(84, 've', 70, 7, 1),
(85, 've', 71, 8, 0),
(86, 've', 72, 8, 1),
(87, 've', 73, 8, 1),
(88, 've', 74, 8, 1),
(89, 've', 75, 8, 1),
(90, 've', 76, 8, 1),
(91, 've', 77, 8, 1),
(92, 've', 78, 8, 1),
(93, 've', 79, 9, 0),
(94, 've', 80, 9, 1),
(95, 've', 81, 9, 1),
(96, 've', 82, 9, 1),
(97, 've', 83, 9, 1),
(98, 've', 84, 9, 1),
(99, 've', 85, 9, 1),
(100, 've', 86, 10, 1),
(101, 've', 87, 10, 1),
(102, 've', 88, 10, 1),
(103, 've', 89, 10, 1),
(104, 've', 90, 10, 1),
(105, 've', 91, 11, 0),
(106, 've', 92, 11, 1),
(107, 've', 93, 11, 1),
(108, 've', 94, 11, 1),
(109, 've', 95, 11, 1),
(110, 've', 96, 11, 1),
(111, 've', 97, 11, 1),
(112, 've', 98, 11, 1),
(113, 've', 99, 11, 1),
(114, 've', 100, 11, 1),
(115, 've', 101, 11, 1),
(116, 've', 102, 11, 1),
(117, 've', 103, 11, 1),
(118, 've', 104, 11, 1),
(119, 've', 105, 11, 1),
(120, 've', 106, 12, 0),
(121, 've', 107, 12, 1),
(122, 've', 108, 12, 1),
(123, 've', 109, 12, 1),
(124, 've', 110, 12, 1),
(125, 've', 111, 12, 1),
(126, 've', 112, 12, 1),
(127, 've', 113, 12, 1),
(128, 've', 114, 12, 1),
(129, 've', 115, 12, 1),
(130, 've', 116, 12, 1),
(131, 've', 117, 12, 1),
(132, 've', 118, 12, 1),
(148, 've', 134, 14, 0),
(149, 've', 135, 14, 1),
(150, 've', 136, 14, 1),
(151, 've', 137, 14, 1),
(152, 've', 138, 14, 1),
(153, 've', 139, 14, 1),
(154, 've', 140, 14, 1),
(155, 've', 141, 14, 1),
(156, 've', 142, 14, 1),
(157, 've', 143, 14, 1),
(158, 've', 144, 14, 1),
(159, 've', 145, 14, 1),
(160, 've', 146, 14, 1),
(161, 've', 147, 14, 1),
(162, 've', 148, 14, 1),
(163, 've', 149, 15, 0),
(164, 've', 150, 15, 1),
(165, 've', 151, 15, 1),
(166, 've', 152, 15, 1),
(167, 've', 153, 15, 1),
(168, 've', 154, 15, 1),
(169, 've', 155, 15, 1),
(170, 've', 156, 15, 1),
(171, 've', 157, 15, 1),
(172, 've', 158, 15, 1),
(173, 've', 159, 15, 1),
(174, 've', 160, 15, 1),
(175, 've', 161, 15, 1),
(191, 've', 177, 17, 0),
(192, 've', 178, 17, 1),
(193, 've', 179, 17, 1),
(194, 've', 180, 17, 1),
(195, 've', 181, 17, 1),
(196, 've', 182, 17, 1),
(197, 've', 183, 17, 1),
(198, 've', 184, 17, 1),
(199, 've', 185, 17, 1),
(200, 've', 186, 18, 0),
(201, 've', 187, 18, 1),
(202, 've', 188, 18, 1),
(203, 've', 189, 18, 1),
(204, 've', 190, 18, 1),
(205, 've', 191, 18, 1),
(206, 've', 192, 18, 1),
(207, 've', 193, 18, 1),
(208, 've', 194, 18, 1),
(209, 've', 195, 19, 0),
(210, 've', 196, 19, 1),
(211, 've', 197, 19, 1),
(212, 've', 198, 19, 1),
(213, 've', 199, 19, 1),
(214, 've', 200, 19, 1),
(215, 've', 201, 19, 1),
(216, 've', 202, 19, 1),
(217, 've', 203, 19, 1),
(218, 've', 204, 19, 1),
(219, 've', 205, 19, 1),
(220, 've', 206, 19, 1),
(221, 've', 207, 19, 1),
(222, 've', 208, 19, 1),
(223, 've', 209, 19, 1),
(224, 've', 210, 20, 1),
(225, 've', 211, 20, 1),
(226, 've', 212, 20, 1),
(227, 've', 213, 20, 1),
(228, 've', 214, 20, 1),
(229, 've', 215, 20, 1),
(230, 've', 216, 20, 1),
(231, 've', 217, 20, 1),
(232, 've', 218, 20, 1),
(233, 've', 219, 20, 1),
(234, 've', 220, 20, 1),
(235, 've', 221, 20, 1),
(236, 've', 222, 20, 1),
(237, 've', 223, 20, 1),
(238, 've', 224, 20, 1),
(239, 've', 225, 20, 1),
(240, 've', 226, 20, 1),
(241, 've', 227, 21, 0),
(242, 've', 228, 21, 1),
(243, 've', 229, 21, 1),
(244, 've', 230, 21, 1),
(245, 've', 231, 21, 1),
(246, 've', 232, 21, 1),
(247, 've', 233, 21, 1),
(248, 've', 234, 21, 1),
(249, 've', 235, 21, 1),
(250, 've', 236, 21, 1),
(251, 've', 237, 21, 1),
(252, 've', 238, 21, 1),
(253, 've', 239, 21, 1),
(254, 've', 240, 21, 1),
(255, 've', 241, 21, 1),
(256, 've', 242, 22, 0),
(257, 've', 243, 22, 1),
(258, 've', 244, 22, 1),
(259, 've', 245, 22, 1),
(260, 've', 246, 22, 1),
(261, 've', 247, 22, 1),
(262, 've', 248, 22, 1),
(263, 've', 249, 22, 1),
(264, 've', 250, 22, 1),
(265, 've', 251, 22, 1),
(266, 've', 252, 22, 1),
(267, 've', 253, 22, 1),
(268, 've', 254, 22, 1),
(269, 've', 255, 22, 1),
(270, 've', 256, 22, 1),
(282, 've', 268, 24, 0),
(283, 've', 269, 24, 1),
(284, 've', 270, 24, 1),
(285, 've', 271, 24, 1),
(286, 've', 272, 24, 1),
(287, 've', 273, 24, 1),
(288, 've', 274, 24, 1),
(289, 've', 275, 24, 1),
(290, 've', 276, 24, 1),
(291, 've', 277, 24, 1),
(292, 've', 278, 24, 1),
(293, 've', 279, 24, 1),
(294, 've', 280, 24, 1),
(295, 've', 281, 24, 1),
(296, 've', 282, 24, 1),
(297, 'bo', 283, 25, 1),
(298, 'co', 283, 25, 1),
(299, 'mx', 283, 25, 1),
(300, 'pe', 283, 25, 1),
(301, 'bo', 284, 25, 1),
(302, 'co', 284, 25, 1),
(303, 'mx', 284, 25, 1),
(304, 'pe', 284, 25, 1),
(305, 'bo', 285, 25, 1),
(306, 'co', 285, 25, 1),
(307, 'mx', 285, 25, 1),
(308, 'pe', 285, 25, 1),
(309, 'bo', 286, 25, 1),
(310, 'co', 286, 25, 1),
(311, 'mx', 286, 25, 1),
(312, 'pe', 286, 25, 1),
(313, 'bo', 287, 25, 1),
(314, 'co', 287, 25, 1),
(315, 'mx', 287, 25, 1),
(316, 'pe', 287, 25, 1),
(317, 'bo', 288, 25, 1),
(318, 'co', 288, 25, 1),
(319, 'mx', 288, 25, 1),
(320, 'pe', 288, 25, 1),
(321, 'bo', 289, 25, 1),
(322, 'co', 289, 25, 1),
(323, 'do', 289, 25, 1),
(324, 'mx', 289, 25, 1),
(325, 'bo', 290, 25, 1),
(326, 'co', 290, 25, 1),
(327, 'mx', 290, 25, 1),
(328, 'pe', 290, 25, 1),
(329, 'bo', 291, 25, 1),
(330, 'co', 291, 25, 1),
(331, 'mx', 291, 25, 1),
(332, 'pe', 291, 25, 1),
(333, 'bo', 292, 25, 1),
(334, 'co', 292, 25, 1),
(335, 'mx', 292, 25, 1),
(336, 'pe', 292, 25, 1),
(337, 'bo', 293, 25, 1),
(338, 'co', 293, 25, 1),
(339, 'mx', 293, 25, 1),
(340, 'pe', 293, 25, 1),
(341, 'bo', 294, 25, 0),
(342, 'co', 294, 25, 0),
(343, 'mx', 294, 25, 0),
(344, 'pe', 294, 25, 0),
(345, 'bo', 295, 25, 1),
(346, 'co', 295, 25, 1),
(347, 'mx', 295, 25, 1),
(348, 'pe', 295, 25, 1),
(349, 'bo', 296, 25, 1),
(350, 'co', 296, 25, 1),
(351, 'mx', 296, 25, 1),
(352, 'pe', 296, 25, 1),
(353, 'bo', 297, 25, 1),
(354, 'co', 297, 25, 1),
(355, 'mx', 297, 25, 1),
(356, 'pe', 297, 25, 1),
(357, 'bo', 298, 26, 1),
(358, 'co', 298, 26, 1),
(359, 'mx', 298, 26, 1),
(360, 'bo', 299, 26, 1),
(361, 'co', 299, 26, 1),
(362, 'mx', 299, 26, 1),
(363, 'bo', 300, 26, 1),
(364, 'co', 300, 26, 1),
(365, 'mx', 300, 26, 1),
(366, 'bo', 301, 26, 1),
(367, 'co', 301, 26, 1),
(368, 'mx', 301, 26, 1),
(369, 'bo', 302, 26, 1),
(370, 'co', 302, 26, 1),
(371, 'mx', 302, 26, 1),
(372, 'bo', 303, 26, 1),
(373, 'co', 303, 26, 1),
(374, 'mx', 303, 26, 1),
(375, 'bo', 304, 26, 1),
(376, 'co', 304, 26, 1),
(377, 'mx', 304, 26, 1),
(378, 'bo', 305, 26, 1),
(379, 'co', 305, 26, 1),
(380, 'mx', 305, 26, 1),
(381, 'bo', 306, 26, 1),
(382, 'co', 306, 26, 1),
(383, 'mx', 306, 26, 1),
(384, 'bo', 307, 26, 1),
(385, 'co', 307, 26, 1),
(386, 'mx', 307, 26, 1),
(387, 'bo', 308, 26, 1),
(388, 'co', 308, 26, 1),
(389, 'mx', 308, 26, 1),
(390, 'bo', 309, 26, 1),
(391, 'co', 309, 26, 1),
(392, 'mx', 309, 26, 1),
(393, 'bo', 310, 26, 1),
(394, 'co', 310, 26, 1),
(395, 'mx', 310, 26, 1),
(396, 'bo', 311, 26, 1),
(397, 'co', 311, 26, 1),
(398, 'mx', 311, 26, 1),
(399, 'bo', 312, 26, 1),
(400, 'co', 312, 26, 1),
(401, 'mx', 312, 26, 1),
(402, 'bo', 313, 27, 1),
(403, 'co', 313, 27, 1),
(404, 'mx', 313, 27, 1),
(405, 'pe', 313, 27, 1),
(406, 'bo', 314, 27, 1),
(407, 'co', 314, 27, 1),
(408, 'mx', 314, 27, 1),
(409, 'pe', 314, 27, 1),
(410, 'bo', 315, 28, 1),
(411, 'co', 315, 28, 1),
(412, 'mx', 315, 28, 1),
(413, 'pe', 315, 28, 1),
(414, 'bo', 316, 29, 1),
(415, 'co', 316, 29, 1),
(416, 'mx', 316, 29, 1),
(417, 'pe', 316, 29, 1),
(418, 'bo', 317, 30, 1),
(419, 'co', 317, 30, 1),
(420, 'mx', 317, 30, 1),
(421, 'pe', 317, 30, 1),
(422, 'mx', 318, 30, 1),
(423, 'pe', 318, 30, 1),
(424, 'bo', 319, 31, 1),
(425, 'co', 319, 31, 1),
(426, 'mx', 319, 31, 1),
(427, 'pe', 319, 31, 1),
(428, 'bo', 320, 32, 1),
(429, 'co', 320, 32, 1),
(430, 'mx', 320, 32, 1),
(431, 'pe', 320, 32, 1),
(432, 'bo', 321, 33, 1),
(433, 'co', 321, 33, 1),
(434, 'mx', 321, 33, 1),
(435, 'pe', 321, 33, 1),
(436, 'bo', 322, 28, 1),
(437, 'co', 322, 28, 1),
(438, 'mx', 322, 28, 1),
(439, 'pe', 322, 28, 1),
(440, 'bo', 323, 28, 1),
(441, 'co', 323, 28, 1),
(442, 'mx', 323, 28, 1),
(443, 'pe', 323, 28, 1),
(444, 'bo', 324, 28, 1),
(445, 'co', 324, 28, 1),
(446, 'mx', 324, 28, 1),
(447, 'pe', 324, 28, 1),
(448, 'bo', 325, 28, 1),
(449, 'co', 325, 28, 1),
(450, 'mx', 325, 28, 1),
(451, 'pe', 325, 28, 1),
(452, 'bo', 326, 28, 1),
(453, 'co', 326, 28, 1),
(454, 'mx', 326, 28, 1),
(455, 'pe', 326, 28, 1),
(456, 'bo', 327, 28, 1),
(457, 'co', 327, 28, 1),
(458, 'mx', 327, 28, 1),
(459, 'pe', 327, 28, 1),
(460, 'bo', 328, 28, 1),
(461, 'co', 328, 28, 1),
(462, 'mx', 328, 28, 1),
(463, 'pe', 328, 28, 1),
(464, 'bo', 329, 28, 1),
(465, 'co', 329, 28, 1),
(466, 'mx', 329, 28, 1),
(467, 'pe', 329, 28, 1),
(468, 'bo', 330, 28, 1),
(469, 'co', 330, 28, 1),
(470, 'mx', 330, 28, 1),
(471, 'pe', 330, 28, 1),
(472, 'bo', 331, 28, 1),
(473, 'co', 331, 28, 1),
(474, 'mx', 331, 28, 1),
(475, 'pe', 331, 28, 1),
(476, 'bo', 332, 28, 1),
(477, 'co', 332, 28, 1),
(478, 'mx', 332, 28, 1),
(479, 'pe', 332, 28, 1),
(480, 'bo', 333, 28, 1),
(481, 'co', 333, 28, 1),
(482, 'mx', 333, 28, 1),
(483, 'pe', 333, 28, 1),
(484, 'co', 334, 34, 1),
(485, 'co', 335, 35, 1),
(486, 'co', 336, 35, 1),
(487, 'co', 337, 35, 1),
(488, 'co', 338, 35, 1),
(489, 'co', 339, 35, 1),
(490, 'co', 340, 35, 1),
(491, 'co', 341, 35, 1),
(492, 'co', 342, 35, 1),
(493, 'co', 343, 35, 1),
(494, 'co', 344, 35, 1),
(495, 'co', 345, 35, 1),
(496, 'co', 346, 35, 1),
(497, 'co', 347, 35, 1),
(498, 'bo', 348, 36, 1),
(499, 'co', 348, 36, 1),
(500, 'mx', 348, 36, 1),
(501, 'pe', 348, 36, 1),
(502, 'bo', 349, 36, 1),
(503, 'co', 349, 36, 1),
(504, 'mx', 349, 36, 1),
(505, 'pe', 349, 36, 1),
(506, 'bo', 350, 36, 1),
(507, 'co', 350, 36, 1),
(508, 'mx', 350, 36, 1),
(509, 'pe', 350, 36, 1),
(510, 'bo', 351, 36, 1),
(511, 'co', 351, 36, 1),
(512, 'bo', 352, 36, 1),
(513, 'co', 352, 36, 1),
(514, 'mx', 352, 36, 1),
(515, 'pe', 352, 36, 1),
(516, 'bo', 353, 36, 1),
(517, 'co', 353, 36, 1),
(518, 'mx', 353, 36, 1),
(519, 'pe', 353, 36, 1),
(520, 'bo', 354, 37, 1),
(521, 'co', 354, 37, 1),
(522, 'mx', 354, 37, 1),
(523, 'pe', 354, 37, 1),
(524, 'bo', 355, 37, 1),
(525, 'co', 355, 37, 1),
(526, 'mx', 355, 37, 1),
(527, 'pe', 355, 37, 1),
(528, 'bo', 356, 37, 1),
(529, 'co', 356, 37, 1),
(530, 'mx', 356, 37, 1),
(531, 'pe', 356, 37, 1),
(532, 'bo', 357, 37, 1),
(533, 'co', 357, 37, 1),
(534, 'mx', 357, 37, 1),
(535, 'pe', 357, 37, 1),
(536, 'bo', 358, 37, 1),
(537, 'co', 358, 37, 1),
(538, 'mx', 358, 37, 1),
(539, 'pe', 358, 37, 1),
(540, 'bo', 359, 37, 1),
(541, 'co', 359, 37, 1),
(542, 'mx', 359, 37, 1),
(543, 'pe', 359, 37, 1),
(544, 'bo', 360, 37, 1),
(545, 'co', 360, 37, 1),
(546, 'mx', 360, 37, 1),
(547, 'pe', 360, 37, 1),
(548, 'bo', 361, 38, 1),
(549, 'co', 361, 38, 0),
(550, 'mx', 361, 38, 1),
(551, 'pe', 361, 38, 1),
(552, 'bo', 362, 38, 1),
(553, 'co', 362, 38, 1),
(554, 'mx', 362, 38, 1),
(555, 'pe', 362, 38, 1),
(556, 'bo', 363, 38, 1),
(557, 'co', 363, 38, 1),
(558, 'mx', 363, 38, 1),
(559, 'pe', 363, 38, 1),
(560, 'bo', 364, 38, 1),
(561, 'co', 364, 38, 1),
(562, 'mx', 364, 38, 1),
(563, 'pe', 364, 38, 1),
(564, 'bo', 365, 38, 1),
(565, 'co', 365, 38, 1),
(566, 'mx', 365, 38, 1),
(567, 'pe', 365, 38, 1),
(568, 'bo', 366, 39, 1),
(569, 'co', 366, 39, 1),
(570, 'mx', 366, 39, 1),
(571, 'pe', 366, 39, 1),
(572, 'bo', 367, 39, 1),
(573, 'co', 367, 39, 1),
(574, 'mx', 367, 39, 1),
(575, 'pe', 367, 39, 1),
(576, 'bo', 368, 39, 1),
(577, 'co', 368, 39, 1),
(578, 'mx', 368, 39, 1),
(579, 'pe', 368, 39, 1),
(580, 'bo', 369, 39, 1),
(581, 'co', 369, 39, 0),
(582, 'mx', 369, 39, 1),
(583, 'pe', 369, 39, 1),
(584, 'bo', 370, 39, 1),
(585, 'co', 370, 39, 0),
(586, 'mx', 370, 39, 1),
(587, 'pe', 370, 39, 1),
(588, 'bo', 371, 41, 1),
(589, 'co', 371, 41, 1),
(590, 'mx', 371, 41, 1),
(591, 'pe', 371, 41, 1),
(592, 'bo', 372, 41, 1),
(593, 'co', 372, 41, 1),
(594, 'mx', 372, 41, 1),
(595, 'pe', 372, 41, 1),
(596, 'bo', 373, 41, 1),
(597, 'co', 373, 41, 1),
(598, 'mx', 373, 41, 1),
(599, 'pe', 373, 41, 1),
(600, 'bo', 374, 41, 1),
(601, 'co', 374, 41, 1),
(602, 'mx', 374, 41, 1),
(603, 'pe', 374, 41, 1),
(604, 'bo', 375, 41, 1),
(605, 'co', 375, 41, 1),
(606, 'mx', 375, 41, 1),
(607, 'pe', 375, 41, 1),
(608, 'bo', 376, 42, 1),
(609, 'co', 376, 42, 1),
(610, 'mx', 376, 42, 1),
(611, 'pe', 376, 42, 1),
(612, 'bo', 377, 42, 1),
(613, 'co', 377, 42, 1),
(614, 'mx', 377, 42, 1),
(615, 'pe', 377, 42, 1),
(616, 'bo', 378, 42, 1),
(617, 'co', 378, 42, 1),
(618, 'mx', 378, 42, 1),
(619, 'pe', 378, 42, 1),
(620, 'bo', 379, 42, 1),
(621, 'co', 379, 42, 1),
(622, 'mx', 379, 42, 1),
(623, 'pe', 379, 42, 1),
(624, 'bo', 380, 42, 1),
(625, 'co', 380, 42, 1),
(626, 'mx', 380, 42, 1),
(627, 'pe', 380, 42, 1),
(628, 'bo', 381, 43, 1),
(629, 'bo', 382, 43, 1),
(630, 'bo', 383, 43, 1),
(631, 'bo', 384, 43, 1),
(632, 'bo', 385, 43, 1),
(633, 'bo', 386, 43, 1),
(634, 'bo', 387, 43, 1),
(635, 'bo', 388, 43, 1),
(636, 'bo', 389, 43, 1),
(637, 'bo', 390, 43, 1),
(638, 'do', 391, 7, 1),
(639, 'do', 392, 7, 1),
(640, 'pa', 393, 7, 1),
(641, 'pa', 394, 7, 1),
(642, 'do', 395, 24, 1),
(643, 'do', 396, 24, 1),
(644, 'pa', 397, 9, 1),
(645, 'pa', 398, 9, 1),
(646, 've', 399, 9, 1),
(647, 've', 400, 9, 1),
(650, 'do', 403, 11, 1),
(651, 'do', 404, 11, 1),
(652, 'pa', 405, 11, 1),
(653, 'pa', 406, 11, 1),
(654, 'do', 407, 10, 1),
(655, 've', 407, 10, 1),
(656, 'do', 86, 10, 1),
(657, 'do', 87, 10, 1),
(658, 've', 408, 10, 1),
(659, 've', 409, 10, 1),
(660, 'do', 88, 10, 1),
(661, 'do', 408, 10, 1),
(662, 'do', 409, 10, 1),
(663, 've', 410, 10, 1),
(664, 've', 411, 10, 1),
(665, 've', 412, 10, 1),
(666, 've', 413, 10, 1),
(667, 'do', 89, 10, 1),
(668, 'do', 410, 10, 1),
(669, 'do', 411, 10, 1),
(670, 'do', 90, 10, 1),
(671, 'do', 412, 10, 1),
(672, 'do', 413, 10, 1),
(673, 've', 414, 44, 0),
(674, 've', 415, 44, 1),
(675, 've', 416, 44, 1),
(676, 've', 417, 45, 0),
(677, 've', 418, 45, 1),
(678, 've', 419, 45, 1),
(679, 've', 420, 45, 1),
(680, 've', 421, 45, 1),
(681, 've', 422, 45, 1),
(682, 've', 423, 45, 1),
(683, 've', 424, 46, 0),
(684, 've', 425, 46, 1),
(685, 've', 426, 46, 1),
(686, 've', 427, 46, 1),
(687, 've', 428, 46, 1),
(688, 've', 429, 46, 1),
(689, 've', 430, 46, 1),
(690, 've', 431, 46, 1),
(691, 've', 432, 46, 1),
(692, 've', 433, 46, 1),
(693, 've', 434, 47, 0),
(694, 've', 435, 47, 1),
(695, 've', 436, 47, 1),
(696, 've', 437, 47, 1),
(697, 've', 438, 48, 0),
(698, 've', 439, 48, 1),
(699, 've', 440, 48, 1),
(700, 've', 441, 48, 1),
(701, 've', 442, 48, 1),
(702, 've', 443, 48, 1),
(703, 've', 444, 48, 1),
(704, 've', 445, 48, 1),
(705, 've', 446, 48, 1),
(706, 've', 447, 48, 1),
(707, 've', 448, 48, 1),
(708, 've', 449, 48, 1),
(709, 've', 450, 48, 1),
(710, 've', 451, 49, 0),
(711, 've', 452, 49, 1),
(712, 've', 453, 49, 1),
(713, 've', 454, 49, 1),
(714, 've', 455, 50, 0),
(715, 've', 456, 50, 1),
(716, 've', 457, 50, 1),
(717, 've', 458, 50, 1),
(718, 'do', 459, 7, 1),
(719, 'do', 460, 7, 1),
(720, 'do', 461, 7, 1),
(721, 'do', 462, 7, 1),
(722, 'do', 463, 7, 1),
(723, 'do', 464, 7, 1),
(724, 've', 465, 51, 0),
(725, 've', 466, 51, 1),
(726, 've', 467, 51, 1),
(727, 've', 468, 51, 1),
(728, 've', 469, 51, 1),
(729, 've', 470, 51, 1),
(730, 've', 471, 51, 1),
(731, 've', 472, 51, 1),
(732, 've', 473, 51, 1),
(733, 've', 474, 51, 1),
(734, 've', 475, 51, 1),
(735, 've', 476, 51, 1),
(736, 've', 477, 51, 1),
(737, 've', 478, 51, 1),
(738, 've', 479, 51, 1),
(739, 've', 480, 51, 1),
(740, 've', 481, 51, 1),
(741, 've', 482, 51, 1),
(742, 've', 483, 51, 1),
(743, 'mx', 351, 36, 1),
(744, 'pe', 351, 36, 1),
(745, 'do', 91, 11, 0),
(746, 'pa', 91, 11, 0),
(747, 'do', 92, 11, 1),
(748, 'pa', 92, 11, 1),
(749, 'do', 93, 11, 1),
(750, 'pa', 93, 11, 1),
(751, 'do', 94, 11, 1),
(752, 'pa', 94, 11, 1),
(753, 'do', 95, 11, 1),
(754, 'pa', 95, 11, 1),
(755, 'do', 96, 11, 1),
(756, 'pa', 96, 11, 1),
(757, 'do', 97, 11, 1),
(758, 'pa', 97, 11, 1),
(759, 'do', 98, 11, 1),
(760, 'pa', 98, 11, 1),
(761, 'do', 99, 11, 1),
(762, 'pa', 99, 11, 1),
(763, 'do', 100, 11, 1),
(764, 'pa', 100, 11, 1),
(765, 'do', 101, 11, 1),
(766, 'pa', 101, 11, 1),
(767, 'do', 102, 11, 1),
(768, 'pa', 102, 11, 1),
(769, 'do', 103, 11, 1),
(770, 'pa', 103, 11, 1),
(771, 'pa', 79, 9, 0),
(772, 'pa', 80, 9, 1),
(773, 'pa', 81, 9, 1),
(774, 'pa', 83, 9, 1),
(775, 'pa', 82, 9, 1),
(776, 'pa', 84, 9, 1),
(777, 'pa', 85, 9, 1),
(778, 've', 484, 12, 1),
(779, 've', 485, 12, 1),
(793, 'bo', 486, 28, 1),
(794, 'co', 486, 28, 0),
(795, 'mx', 487, 28, 1),
(796, 'pe', 487, 28, 1),
(797, 'bo', 488, 28, 1),
(798, 'co', 488, 28, 0),
(799, 'bo', 489, 28, 1),
(800, 'co', 489, 28, 0),
(801, 'bo', 490, 29, 1),
(802, 'co', 490, 29, 1),
(803, 'mx', 490, 29, 1),
(804, 'pe', 490, 29, 1),
(805, 'bo', 491, 30, 1),
(806, 'co', 491, 30, 1),
(807, 'mx', 491, 30, 1),
(808, 'pe', 491, 30, 1),
(809, 'bo', 492, 31, 1),
(810, 'co', 492, 31, 1),
(811, 'mx', 492, 31, 1),
(812, 'pe', 492, 31, 1),
(813, 'bo', 493, 32, 1),
(814, 'co', 493, 32, 1),
(815, 'mx', 493, 32, 1),
(816, 'pe', 493, 32, 1),
(817, 'bo', 494, 33, 1),
(818, 'co', 494, 33, 1),
(819, 'mx', 494, 33, 1),
(820, 'pe', 494, 33, 1),
(821, 'bo', 495, 34, 1),
(822, 'co', 495, 34, 1),
(823, 'bo', 496, 34, 1),
(824, 'co', 496, 34, 1),
(825, 'bo', 497, 34, 1),
(826, 'co', 497, 34, 1),
(827, 'bo', 498, 34, 1),
(828, 'co', 498, 34, 1),
(829, 'bo', 499, 34, 1),
(830, 'co', 499, 34, 1),
(831, 'co', 500, 35, 1),
(832, 'bo', 501, 37, 1),
(833, 'co', 501, 37, 1),
(834, 'mx', 501, 37, 1),
(835, 'pe', 501, 37, 1),
(836, 'mx', 502, 38, 1),
(837, 'pe', 502, 38, 1),
(838, 'mx', 503, 39, 1),
(839, 'pe', 503, 39, 1),
(840, 'bo', 504, 39, 1),
(841, 'co', 504, 39, 1),
(842, 'mx', 504, 39, 1),
(843, 'pe', 504, 39, 1),
(844, 'mx', 505, 39, 1),
(845, 'pe', 505, 39, 1),
(846, 'bo', 506, 40, 1),
(847, 'co', 506, 40, 1),
(848, 'mx', 506, 40, 1),
(849, 'pe', 506, 40, 1),
(850, 'bo', 507, 40, 1),
(851, 'co', 507, 40, 1),
(852, 'mx', 507, 40, 1),
(853, 'pe', 507, 40, 1),
(854, 'bo', 508, 40, 1),
(855, 'co', 508, 40, 1),
(856, 'mx', 508, 40, 1),
(857, 'pe', 508, 40, 1),
(858, 'bo', 509, 40, 1),
(859, 'co', 509, 40, 1),
(860, 'mx', 509, 40, 1),
(861, 'pe', 509, 40, 1),
(862, 'mx', 510, 40, 1),
(863, 'pe', 510, 40, 1),
(864, 'co', 210, 20, 1),
(865, 'co', 211, 20, 1),
(866, 'co', 212, 20, 1),
(867, 'co', 213, 20, 1),
(868, 'co', 214, 20, 1),
(869, 'co', 215, 20, 1),
(870, 'co', 216, 20, 1),
(871, 'co', 217, 20, 1),
(872, 'co', 218, 20, 1),
(873, 'co', 219, 20, 1),
(874, 'co', 220, 20, 1),
(875, 'co', 221, 20, 1),
(876, 'co', 222, 20, 1),
(877, 'co', 382, 43, 1),
(878, 'co', 383, 43, 1),
(879, 'co', 511, 43, 1),
(880, 'co', 512, 43, 1),
(881, 'co', 388, 43, 1),
(882, 'co', 389, 43, 1),
(883, 'bo', 513, 43, 1),
(884, 'co', 513, 43, 1),
(885, 'co', 514, 43, 1),
(886, 'co', 515, 43, 1),
(887, 'co', 516, 52, 1),
(888, 'co', 517, 52, 1),
(889, 'co', 518, 52, 1),
(890, 'co', 519, 52, 1),
(891, 'co', 520, 52, 1),
(892, 'co', 521, 52, 1),
(893, 'co', 522, 52, 1),
(894, 'co', 523, 52, 1),
(895, 'co', 524, 52, 1),
(896, 'co', 525, 52, 1),
(897, 'co', 526, 52, 1),
(898, 'co', 527, 52, 1),
(899, 'co', 528, 52, 1),
(900, 'co', 529, 53, 1),
(901, 'co', 530, 53, 1),
(902, 'co', 531, 53, 1),
(903, 'co', 532, 53, 1),
(904, 'co', 533, 53, 1),
(905, 'co', 534, 53, 1),
(906, 'co', 535, 53, 1),
(907, 'co', 536, 53, 1),
(908, 'co', 537, 53, 1),
(909, 'co', 538, 53, 1),
(910, 'co', 539, 53, 1),
(911, 'co', 540, 53, 1),
(912, 'co', 541, 53, 1),
(913, 'co', 542, 54, 1),
(914, 'co', 543, 54, 1),
(915, 'co', 544, 54, 1),
(916, 'co', 545, 54, 1),
(917, 'co', 546, 54, 1),
(918, 'co', 547, 54, 1),
(919, 'co', 548, 54, 1),
(920, 'co', 549, 54, 1),
(921, 'co', 550, 54, 1),
(922, 'co', 551, 54, 1),
(923, 'co', 552, 54, 1),
(924, 'co', 553, 54, 1),
(925, 'co', 554, 54, 1),
(926, 'co', 555, 55, 1),
(927, 'co', 556, 55, 1),
(928, 'co', 557, 55, 1),
(929, 'co', 558, 55, 1),
(930, 'co', 559, 55, 1),
(931, 'co', 560, 55, 1),
(932, 'co', 561, 55, 1),
(933, 'co', 562, 55, 1),
(934, 'co', 563, 55, 1),
(935, 'co', 564, 55, 1),
(936, 'co', 565, 55, 1),
(937, 'co', 566, 55, 1),
(938, 'co', 567, 55, 1),
(939, 'co', 568, 55, 1),
(940, 'co', 569, 55, 1),
(941, 'co', 570, 54, 1),
(942, 'co', 571, 54, 1),
(943, 'co', 572, 53, 1),
(944, 'co', 573, 53, 1),
(945, 'co', 574, 43, 1),
(946, 'bo', 575, 28, 1),
(947, 'co', 575, 28, 1),
(948, 'mx', 575, 28, 1),
(949, 'pe', 575, 28, 1),
(950, 'co', 576, 29, 1),
(951, 'co', 577, 29, 1),
(952, 'co', 578, 29, 1),
(953, 'co', 579, 30, 1),
(954, 'co', 580, 30, 1),
(955, 'co', 581, 30, 1),
(956, 'co', 582, 31, 1),
(957, 'co', 583, 31, 1),
(958, 'co', 584, 31, 1),
(959, 'co', 585, 31, 1),
(960, 'co', 586, 31, 1),
(961, 'co', 587, 33, 1),
(962, 'co', 588, 33, 1),
(963, 'co', 589, 33, 1),
(964, 'co', 590, 34, 1),
(965, 'co', 591, 34, 1),
(966, 'co', 592, 34, 1),
(967, 'co', 593, 32, 1),
(968, 'co', 594, 32, 1),
(969, 'co', 595, 32, 1),
(970, 'co', 596, 56, 1),
(971, 'co', 597, 56, 1),
(972, 'co', 598, 56, 1),
(973, 'co', 599, 56, 1),
(974, 'co', 600, 56, 1),
(975, 'co', 601, 56, 1),
(976, 'co', 602, 56, 1),
(977, 'co', 603, 57, 1),
(978, 'co', 604, 57, 1),
(979, 'co', 605, 57, 1),
(980, 'co', 606, 57, 1),
(981, 'co', 607, 57, 1),
(982, 'co', 608, 57, 1),
(983, 'co', 609, 57, 1),
(984, 'co', 610, 57, 1),
(985, 'co', 611, 57, 1),
(986, 'co', 616, 58, 1),
(987, 'co', 617, 58, 1),
(988, 'co', 618, 58, 1),
(989, 'co', 619, 58, 1),
(990, 'co', 620, 58, 1),
(991, 'co', 621, 58, 1),
(992, 'co', 622, 58, 1),
(993, 'co', 623, 58, 1),
(994, 'co', 624, 58, 1),
(995, 'co', 625, 59, 1),
(996, 'co', 626, 59, 1),
(997, 'co', 627, 59, 1),
(998, 'co', 628, 59, 1),
(999, 'co', 629, 59, 1),
(1000, 'co', 630, 59, 1),
(1001, 'co', 631, 59, 1),
(1002, 'co', 632, 59, 1),
(1003, 'co', 633, 59, 1),
(1004, 'co', 634, 60, 1),
(1005, 'co', 635, 60, 1),
(1006, 'co', 636, 60, 1),
(1007, 'co', 637, 60, 1),
(1008, 'co', 638, 60, 1),
(1009, 'co', 639, 60, 1),
(1010, 'co', 640, 60, 1),
(1011, 'co', 641, 60, 1),
(1012, 'co', 642, 60, 1),
(1013, 'co', 643, 56, 1),
(1014, 'co', 644, 57, 1),
(1015, 'co', 645, 27, 1),
(1016, 'co', 646, 27, 1),
(1017, 'co', 647, 27, 1),
(1018, 'co', 648, 27, 1),
(1019, 'co', 649, 27, 1),
(1020, 'co', 650, 27, 1),
(1021, 'co', 651, 27, 1),
(1022, 'co', 652, 27, 1),
(1023, 'co', 653, 27, 1),
(1024, 'co', 654, 27, 1),
(1025, 'co', 655, 27, 1),
(1026, 'co', 656, 26, 1),
(1027, 'co', 657, 26, 1),
(1028, 'co', 658, 26, 1),
(1029, 'co', 659, 26, 1),
(1030, 'co', 660, 26, 1),
(1031, 'co', 661, 25, 1),
(1032, 've', 662, 15, 1),
(1033, 've', 663, 51, 1),
(1034, 've', 664, 5, 1),
(1035, 've', 665, 6, 1),
(1036, 've', 666, 7, 1),
(1037, 've', 667, 8, 1),
(1038, 'pa', 668, 9, 1),
(1039, 've', 668, 9, 1),
(1040, 've', 669, 14, 1),
(1041, 've', 670, 17, 1),
(1042, 've', 671, 19, 1),
(1043, 've', 672, 22, 1),
(1044, 've', 673, 24, 1),
(1045, 've', 336, 35, 1),
(1046, 've', 337, 35, 1),
(1047, 've', 338, 35, 1),
(1048, 've', 339, 35, 1),
(1049, 've', 340, 35, 1),
(1050, 've', 341, 35, 1),
(1051, 've', 342, 35, 1),
(1052, 've', 343, 35, 1),
(1053, 've', 344, 35, 1),
(1054, 've', 345, 35, 1),
(1055, 've', 346, 35, 1),
(1056, 've', 347, 35, 1),
(1057, 've', 674, 1, 1),
(1058, 've', 675, 2, 1),
(1059, 've', 676, 3, 1),
(1060, 've', 677, 11, 1),
(1061, 've', 678, 12, 1),
(1062, 've', 679, 18, 1),
(1063, 've', 680, 21, 1),
(1064, 've', 681, 44, 1),
(1065, 've', 682, 45, 1),
(1066, 've', 683, 46, 1),
(1067, 've', 684, 47, 1),
(1068, 've', 685, 49, 1),
(1069, 've', 686, 50, 1),
(1070, 've', 687, 48, 1),
(1071, 'co', 487, 28, 1),
(1072, 'co', 688, 35, 1),
(1073, 've', 689, 25, 1),
(1074, 've', 690, 25, 1),
(1075, 've', 283, 25, 0),
(1076, 've', 691, 27, 1),
(1077, 've', 692, 27, 1),
(1078, 've', 693, 28, 1),
(1079, 've', 694, 28, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_types_countries`
--

CREATE TABLE IF NOT EXISTS `products_types_countries` (
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `order` tinyint(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  KEY `fk_products_country_countries1_idx` (`country_id`),
  KEY `fk_products_country_products1_idx` (`product_id`),
  KEY `fk_products_types_country_types1_idx` (`product_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `products_types_countries`
--

INSERT INTO `products_types_countries` (`country_id`, `product_id`, `product_type_id`, `order`, `active`) VALUES
('bo', 1, 2, 1, 0),
('bo', 25, 8, 1, 1),
('mx', 25, 8, 1, 1),
('pe', 25, 8, 1, 1),
('bo', 26, 8, 1, 1),
('mx', 26, 8, 1, 1),
('bo', 27, 8, 1, 1),
('mx', 27, 8, 1, 1),
('pe', 27, 8, 1, 1),
('bo', 28, 9, 1, 1),
('mx', 28, 9, 1, 1),
('pe', 28, 9, 1, 1),
('bo', 29, 9, 1, 1),
('mx', 29, 9, 1, 1),
('pe', 29, 9, 1, 1),
('bo', 30, 9, 1, 1),
('mx', 30, 9, 1, 1),
('pe', 30, 9, 1, 1),
('bo', 31, 9, 1, 1),
('mx', 31, 9, 1, 1),
('pe', 31, 9, 1, 1),
('bo', 32, 9, 1, 1),
('mx', 32, 9, 1, 1),
('pe', 32, 9, 1, 1),
('bo', 33, 9, 1, 1),
('mx', 33, 9, 1, 1),
('pe', 33, 9, 1, 1),
('co', 25, 8, 1, 1),
('co', 27, 8, 1, 1),
('co', 26, 8, 1, 1),
('co', 28, 9, 1, 1),
('co', 29, 9, 1, 1),
('co', 30, 9, 1, 1),
('co', 31, 9, 1, 1),
('co', 32, 9, 1, 1),
('co', 33, 9, 1, 1),
('co', 34, 9, 1, 1),
('co', 35, 10, 1, 1),
('ve', 1, 2, 1, 1),
('ve', 2, 2, 1, 1),
('ve', 3, 2, 1, 1),
('ve', 4, 1, 1, 1),
('ve', 5, 1, 1, 1),
('ve', 6, 1, 1, 1),
('ve', 7, 1, 1, 1),
('ve', 8, 1, 1, 1),
('ve', 9, 1, 1, 1),
('ve', 10, 5, 1, 1),
('ve', 11, 2, 1, 1),
('ve', 12, 2, 1, 1),
('ve', 14, 1, 1, 1),
('ve', 15, 4, 1, 1),
('ve', 17, 1, 1, 1),
('ve', 18, 2, 1, 1),
('ve', 19, 1, 1, 1),
('ve', 20, 4, 1, 1),
('ve', 21, 2, 1, 1),
('ve', 22, 1, 1, 1),
('ve', 24, 1, 1, 1),
('bo', 36, 13, 1, 1),
('co', 36, 13, 1, 1),
('mx', 36, 13, 1, 1),
('pe', 36, 13, 1, 1),
('bo', 37, 13, 1, 1),
('co', 37, 13, 1, 1),
('mx', 37, 13, 1, 1),
('pe', 37, 13, 1, 1),
('bo', 38, 13, 1, 1),
('co', 38, 13, 1, 1),
('mx', 38, 13, 1, 1),
('pe', 38, 13, 1, 1),
('bo', 39, 11, 1, 1),
('co', 39, 11, 1, 1),
('mx', 39, 11, 1, 1),
('pe', 39, 11, 1, 1),
('bo', 40, 12, 1, 1),
('co', 40, 12, 1, 1),
('mx', 40, 12, 1, 1),
('pe', 40, 12, 1, 1),
('bo', 41, 12, 1, 1),
('co', 41, 12, 1, 1),
('mx', 41, 12, 1, 1),
('pe', 41, 12, 1, 1),
('bo', 42, 12, 1, 1),
('co', 42, 12, 1, 1),
('mx', 42, 12, 1, 1),
('pe', 42, 12, 1, 1),
('bo', 43, 14, 1, 1),
('do', 7, 1, 1, 1),
('pa', 7, 1, 1, 1),
('do', 24, 1, 1, 1),
('pa', 9, 1, 1, 1),
('do', 11, 2, 1, 1),
('pa', 11, 2, 1, 1),
('do', 10, 5, 1, 1),
('ve', 44, 6, 1, 1),
('ve', 45, 6, 1, 1),
('ve', 46, 6, 1, 1),
('ve', 47, 6, 1, 1),
('ve', 48, 6, 1, 1),
('ve', 49, 6, 1, 1),
('ve', 50, 6, 1, 1),
('ve', 51, 4, 1, 1),
('bo', 34, 9, 1, 1),
('co', 20, 4, 1, 1),
('co', 43, 14, 1, 1),
('co', 52, 16, 1, 1),
('co', 53, 14, 1, 1),
('co', 54, 14, 1, 1),
('co', 55, 14, 1, 1),
('co', 56, 12, 1, 1),
('co', 57, 12, 1, 1),
('co', 58, 4, 1, 1),
('co', 59, 4, 1, 1),
('co', 60, 4, 1, 1),
('ve', 35, 3, 1, 1),
('ve', 25, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_aplication`
--

CREATE TABLE IF NOT EXISTS `product_aplication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `product_id` int(11) NOT NULL,
  `aplication_images_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_aplication_images_id` (`aplication_images_id`),
  KEY `fk_product_id` (`id`),
  KEY `fk_product_id_idx` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Volcado de datos para la tabla `product_aplication`
--

INSERT INTO `product_aplication` (`id`, `active`, `product_id`, `aplication_images_id`, `order`) VALUES
(23, 1, 27, 12, 1),
(24, 1, 27, 14, 2),
(25, 1, 27, 5, 3),
(26, 1, 27, 8, 4),
(27, 1, 27, 16, 5),
(28, 1, 25, 12, 1),
(29, 1, 25, 5, 2),
(30, 1, 25, 9, 3),
(31, 1, 25, 14, 4),
(32, 1, 26, 12, 1),
(33, 1, 26, 14, 2),
(34, 1, 26, 5, 3),
(35, 1, 26, 8, 4),
(36, 1, 56, 17, 1),
(37, 1, 56, 18, 2),
(38, 1, 56, 19, 3),
(39, 1, 56, 13, 4),
(40, 1, 57, 17, 1),
(41, 1, 57, 19, 2),
(42, 1, 57, 18, 3),
(43, 1, 57, 15, 4),
(44, 1, 43, 15, 1),
(45, 1, 43, 8, 2),
(46, 1, 43, 5, 3),
(47, 1, 43, 11, 4),
(48, 1, 53, 15, 1),
(49, 1, 53, 8, 2),
(50, 1, 53, 11, 3),
(51, 1, 53, 7, 4),
(52, 1, 54, 15, 1),
(53, 1, 54, 4, 2),
(54, 1, 54, 11, 3),
(55, 1, 54, 13, 4),
(56, 1, 55, 15, 1),
(57, 1, 55, 11, 2),
(58, 1, 55, 4, 3),
(59, 1, 55, 5, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_type_texts`
--

CREATE TABLE IF NOT EXISTS `product_type_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image_path` mediumtext CHARACTER SET utf8 NOT NULL,
  `active` tinyint(1) NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `product_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_type_texts_country_languages1_idx` (`language_id`),
  KEY `fk_product_type_texts_country_types1_idx` (`product_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `product_type_texts`
--

INSERT INTO `product_type_texts` (`id`, `name`, `description`, `image_path`, `active`, `language_id`, `product_type_id`) VALUES
(1, 'Impresoras Fiscales', 'Impresoras Térmicas, \r\nImpresoras de Matriz de Puntos y la única Impresora Láser en el mercado,\r\n todas homologadas por el SENIAT, que se adaptan a las necesidades del \r\nmercado de retail en Venezuela.\r\n<br>', 'categorias/impresorasfiscales/images/54a_impresorasfiscales.png', 1, 'es', 1),
(2, 'Cajas Registradoras', 'Con memoria de \r\nauditoria y bateria incluida (modelos CR2100, CRD81Fj y CRD68FJ) \r\nfacilidad de uso, comodidad, economía y practicidad.<br>', 'categorias/cajasregistradoras/images/21q_cajasregistradoras.jpg', 1, 'es', 2),
(3, 'Impresoras Estandar', 'Impresoras inalámbricas y de alta velocidad que puede utilizar tanto en su oficina como en la comodidad de su hogar.<br>', 'categorias/impresorasestandar/images/38u_impresorasestandar.png', 1, 'es', 3),
(4, 'Balanzas', 'Equipo de fácil programación y uso; preciso y confiable, certificado por SENCAMER<br>', 'categorias/balanzas/images/38y_balanzas.jpg', 1, 'es', 4),
(5, 'Papel Termico', 'Somos \r\nFabricantes de rollos de papel térmico a la medida, distribución al \r\nmayor de cajas a lo largo de todo el territorio Venezolano.<br>', 'categorias/papeltermico/images/34k_papeltérmico.jpg', 1, 'es', 5),
(6, 'Perifericos y Otros', 'Periféricos y consumibles para complementar su equipo fiscal.\r\n<br>', 'categorias/perifericosyotros/images/34i_perifericosyotros.jpg', 1, 'es', 6),
(7, 'Impresoras  Punto de Venta', 'La impresora POS térmica \r\ndirecta Dascom DT230 ofrece fiabilidad, precisión y desempeño con un \r\ntamaño compacto, donde el espacio es limitado.								<br>', 'categorias/impresoraspuntodeventa/images/28x_impresorasdepuntodeventa.png', 1, 'es', 8),
(8, 'Impresoras Matriz de Puntos', '<div>\r\n								<div>\r\n									Impresoras seriales matriz de punto de 9 y 24 pines con interfaces USB, serial y paralela.								</div>\r\n							</div><br>', 'categorias/impresorasmatrizdepuntos/images/23a_impresorasmatrizdepuntos.png', 1, 'es', 9),
(9, 'Impresoras Láser', 'Impresoras láser con \r\nconectividad Wi-Fi que incrementan la productividad gracias a su \r\nvelocidad y al bajo costo por página impresa.								<br>', 'categorias/impresoraslaser/images/31a_impresorasláser.png', 1, 'es', 10),
(10, 'Impresoras de Especialidad', 'Impresora de impacto diseñada principalmente para la impresión de notas de entrega en vehiculos								<br>', 'categorias/impresorasdeespecialidad/images/27u_impresorasdeespecialidad.png', 1, 'es', 11),
(11, 'Impresoras Móviles', 'Impresoras de etiquetas rentable para la oficina móvil, bodega, y otros ambientes no fijos<br>', 'categorias/impresorasmoviles/images/41n_impresorasmóviles.png', 1, 'es', 12),
(12, 'Impresoras Térmicas', 'Impresora Térmica de desempeño\r\n industrial con un diseño fuerte para volumenes de producción en ventas \r\nal detal y de manufactura. <br>', 'categorias/impresorastermicas/images/66k_impresorastérmicas.png', 1, 'es', 13),
(13, 'Terminales AIO TOUCH', 'Equipo de fácil programación y uso.<br>', 'categorias/terminalesaiotouch/images/97v_terminalespos.png', 1, 'es', 14),
(14, 'Impresoras Fiscales PA', 'Impresoras térmicas y \r\nde matriz de punto todas homologadas por la DGI, que se adaptan a las \r\nnecesidades del mercado de retail en Panamá.<br>', 'categorias/impresorasfiscales/images/78x_impresorasfiscales.jpg', 1, 'es', 1),
(15, 'Impresoras Fiscales DO', 'La \r\nBIXOLON SRP-350 es una impresora fiscal que brinda la mayor rapidez de \r\nimpresión en el mercado, cuenta con un diseño robusto y seguro \r\ncumpliendo así las normativas fiscales del país.<br>', 'categorias/impresorasfiscales/images/95q_impresorasfiscales.jpg', 1, 'es', 1),
(16, 'Cajas Registradoras DO', 'La \r\nCRD81FJ es una Caja Registradora fabricada con los más elevados \r\nestándares de calidad, robustez y seguridad. Ofrece un elegante diseño, \r\nbrindando una independización a la hora de llevar las operaciones del \r\ndía a día del negocio. <br>', 'categorias/cajasregistradoras/images/51v_cajasregistradoras.jpg', 1, 'es', 2),
(17, 'Papel Termico DO', 'Somos Distribuidores de rollos de papel térmico a la medida, a lo largo de todo el pais.<br>', 'categorias/papeltermico/images/54e_papeltérmico.jpg', 1, 'es', 5),
(18, 'Point Of Sale Printers ', 'The direct thermal POS printer\r\n Dascom DT230 offers reliability, accuracy and performance in a compact \r\npackage where space is limited<br>', 'categorias/impresorasfiscales/images/35e_impresorasfiscales.jpg', 1, 'en', 8),
(19, 'Serial dot Matrix Printers', 'Serial Printers Dot Matrix 9 and 24 pins with USB, serial and parallel interfaces.                     \r\n\r\n                                    <br>', 'categorias/impresorasmatrizdepuntos/images/91i_impresorasmatrizdepuntos.jpg', 1, 'en', 9),
(20, 'Thermal Printers', 'Thermal pr nter industrial \r\nperformance with strong design to production volumes in retail and \r\nmanufacturing.                     \r\n\r\n                                    <br>', 'categorias/impresorastermicas/images/23a_impresorastérmicas.jpg', 1, 'en', 13),
(21, 'Specialty Printers', 'Impact printer designed primarily for printing delivery notes in vehicles<br>', 'categorias/impresorasdeespecialidad/images/87b_impresorasdeespecialidad.jpg', 1, 'en', 11),
(22, 'Movil Printers', 'Label Printers profitable for the mobile office, warehouse, and other non-fixed environments<br>', 'categorias/impresorasmoviles/images/44p_impresorasmóviles.jpg', 1, 'en', 12),
(23, 'Impresoras', 'Impresoras', 'categorias/impresoras/images/79h_impresoras.jpg', 1, 'es', 7),
(24, 'Soluciones Moviles', 'Solucione Moviles Prueba<br>', 'categorias/solucionesmoviles/images/94l_solucionesmóviles.png', 1, 'es', 15),
(25, 'Colectores de Datos', 'Colectores de Datos<br>', 'categorias/colectoresdedatos/images/65i_colectoresdedatos.png', 1, 'es', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` longtext CHARACTER SET utf8,
  `name` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `description`, `name`, `active`) VALUES
(1, 'Administrador', 'Administrador', 1),
(2, 'Editar noticias de la pagina.', 'Editor', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_items`
--

CREATE TABLE IF NOT EXISTS `roles_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rol_id` (`rol_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `roles_items`
--

INSERT INTO `roles_items` (`id`, `rol_id`, `item_id`, `active`) VALUES
(1, 1, 1, 1),
(2, 1, 7, 1),
(3, 1, 3, 1),
(4, 1, 2, 1),
(5, 1, 4, 1),
(6, 1, 5, 1),
(8, 2, 7, 1),
(10, 2, 3, 0),
(11, 2, 2, 0),
(12, 2, 4, 0),
(13, 2, 5, 0),
(14, 2, 1, 0),
(15, 1, 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `address` text COLLATE utf8_spanish_ci NOT NULL,
  `phone1` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `phone2` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `phone3` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `staff` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `web` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `city` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `image_path` mediumtext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Centro de servicio Autorizados' AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id`, `name`, `address`, `phone1`, `phone2`, `phone3`, `staff`, `web`, `active`, `city`, `image_path`) VALUES
(1, 'AMTECK', 'Av. Carreta 24# 83-41, Bogotá D.C., Colombia.', '+5712368718', '', '', 'Jorge E. Viafara R.', 'http://www.amteck.com.co', 1, 'Bogotá D.C.', 'logo_amteck_sas.png'),
(2, 'INGECOM S.A.S', 'calle 40n # 3n-95 Cali, Colombia', '572 6662221', '', '', 'Diego Hernán Ortiz', '', 1, 'Bogotá D.C.', 'logo_ingecom_sas.png'),
(3, 'MES CARIBE S.A.S.', 'Cra. 54 N 74-134 Oficina 201, Barranquilla, Colombia.', '+5753680666', '', '', 'Nilson de la Hoz Rodríguez ', '', 1, 'Barranquilla', 'logo_mes_caribe_sas.jpeg'),
(4, 'TECNOMICROS', 'Calle 55A No 27-22 Barrio Bolarqui, Bucaramanga, Colombia.', '+5776959888 ', '3188481264', '', 'FABIOLA OJEDA', 'http://www.tecnomicros.com', 1, 'Bucaramanga', 'logo_tecnomicros.png'),
(5, 'TECNOMICROS', 'Carrera 78 No 32F-04 Barrio Laureles-Nogales, Medellín, Colombia.', '+5744444293', '3166917117', '', 'LEIDY YULIETH RUIZ', 'http://www.tecnomicros.com', 1, 'Bucaramanga', 'logo_tecnomicros.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_country`
--

CREATE TABLE IF NOT EXISTS `service_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `country_id` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cas_id` (`service_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `service_country`
--

INSERT INTO `service_country` (`id`, `service_id`, `country_id`, `order`, `active`) VALUES
(1, 1, 'co', 1, 1),
(2, 2, 'co', 2, 1),
(3, 3, 'co', 3, 1),
(4, 4, 'co', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_slider_languages1_idx` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `sliders`
--

INSERT INTO `sliders` (`id`, `image_path`, `active`, `language_id`) VALUES
(1, 'sliders/hkaportatil.jpg', 0, 'es'),
(2, 'sliders/29f_hka57.jpg', 0, 'es'),
(3, 'sliders/76t_todo.jpg', 0, 'es'),
(4, 'sliders/62v_hkaprint.jpg', 1, 'es'),
(5, 'sliders/67p_facturacionelectronica.jpg', 1, 'es'),
(6, 'sliders/11m_hka80.jpg', 0, 'es'),
(7, 'sliders/49i_cambiohorario.jpg', 0, 'es'),
(8, 'sliders/11r_solucionesdeimpresion.jpg', 1, 'es'),
(9, 'sliders/90w_terminalesaiotouch.jpg', 1, 'es'),
(10, 'sliders/15d_balanzas.jpg', 1, 'es'),
(11, 'sliders/75n_luminariasiluminacionled.jpg', 1, 'es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sliders_countries`
--

CREATE TABLE IF NOT EXISTS `sliders_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `slider_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `url` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `url_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_slider_country_countries1_idx` (`country_id`),
  KEY `fk_slider_country_slider1_idx` (`slider_id`),
  KEY `fk_sliders_countries_url_types1_idx` (`url_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `sliders_countries`
--

INSERT INTO `sliders_countries` (`id`, `country_id`, `slider_id`, `order`, `active`, `url`, `url_type_id`) VALUES
(1, 'co', 1, 1, 1, 'productos/impresoras', 1),
(2, 'co', 2, 2, 1, 'productos/impresoras', 1),
(3, 'co', 3, 3, 1, 'productos/impresoras', 1),
(4, 'co', 4, 6, 1, 'http://hkaprint.com/', 2),
(5, 'co', 5, 1, 1, 'facturacionElectronica/index', 1),
(6, 'co', 6, 6, 1, 'productos/impresoras', 1),
(7, 'co', 7, 7, 1, 'cambiohorario', 1),
(8, 'co', 8, 2, 1, 'productos/impresoras', 1),
(9, 'co', 9, 3, 1, 'productos/terminalesaio', 1),
(10, 'co', 10, 5, 1, 'productos/balanzas', 1),
(11, 'co', 11, 5, 1, 'productos/iluminacionled', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `texts`
--

CREATE TABLE IF NOT EXISTS `texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext COLLATE utf8_unicode_ci,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `section` tinytext COLLATE utf8_unicode_ci,
  `active` tinyint(4) NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_texts_languages1_idx` (`language_id`),
  KEY `fk_texts_countries1_idx` (`country_id`),
  KEY `fk_texts_products1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=270 ;

--
-- Volcado de datos para la tabla `texts`
--

INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(1, 'Descripcion', '<div>\r\n							<div>CAJA REGISTRADORA CRD81F\r\n<br><br>\r\n<ul><li>Única Caja Registradora que trabaja hasta 72 horas sin suministro eléctrico.</li><li>Capacidad para imprimir los datos fiscales del cliente (suministra crédito fiscal).</li><li>Distribuidores y Centros de servicio técnico a nivel nacional.</li></ul></div>\r\n					</div><br>', 'product', 0, 'es', 'bo', 1),
(2, 'Descripcion', '<div>\r\n							<div>CAJA REGISTRADORA CRD81F\r\n<br><br>\r\n<ul><li>Única Caja Registradora que trabaja hasta 72 horas sin suministro eléctrico.</li><li>Capacidad para imprimir los datos fiscales del cliente (suministra crédito fiscal).</li><li>Distribuidores y Centros de servicio técnico a nivel nacional.</li></ul></div>\r\n					</div><br>', 'product', 1, 'es', 've', 1),
(3, 'Caracteristicas', '<table border="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>Maneja hasta 25000 productos.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>99 departamentos programables.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Memoria fiscal con capacidad para 2200 reportes Z (6 a&ntilde;os).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Capacidad para imprimir la raz&oacute;n social y R.I.F. del cliente.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Papel de 57 mm de ancho.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>No borra la programaci&oacute;n de productos.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bater&iacute;a interna que permite utilizar la Caja Registradora hasta por 72 horas sin conexi&oacute;n a la red el&eacute;ctrica.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Impresor de alta velocidad t&eacute;rmico: Cliente y Auditoria. (40-50mm/s).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 tasas de impuestos programables mas Exento.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>160 teclas de ventas directas (productos y/o Departamentos).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Display Operador de 8 l&iacute;neas y 22 caracteres por l&iacute;nea.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Totalmente programable por PC. (Software incluido).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Velocidad de impresi&oacute;n programable.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Display de cliente de 9 d&iacute;gitos</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 Cajeros programables.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Men&uacute;s y Cajeros protegidos por clave (NO se utiliza llave).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 Medios de Pago (Tarjeta de cr&eacute;dito, Tickets, Cheque y Efectivo).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja 37 tipos de c&oacute;digos de barra.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Gaveta de dinero removible con 8 compartimientos para monedas y 5 para billetes.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Variedad de reportes (Z, X, PLU, Departamentos, IVA, Cajeros, Gaveta)</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja descuentos y recargos por porcentaje.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja descuento por monto.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Control de inventario (Software incluido).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Obtenci&oacute;n de reportes.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Software para la preparaci&oacute;n del libro de ventas.(Software incluido).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Anulaci&oacute;n bajo clave.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Consulta de precios durante la venta.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 0, 'es', 'bo', 1),
(4, 'Caracteristicas', '<table border="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>Maneja hasta 25000 productos.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>99 departamentos programables.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Memoria fiscal con capacidad para 2200 reportes Z (6 a&ntilde;os).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Capacidad para imprimir la raz&oacute;n social y R.I.F. del cliente.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Papel de 57 mm de ancho.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>No borra la programaci&oacute;n de productos.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bater&iacute;a interna que permite utilizar la Caja Registradora hasta por 72 horas sin conexi&oacute;n a la red el&eacute;ctrica.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Impresor de alta velocidad t&eacute;rmico: Cliente y Auditoria. (40-50mm/s).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 tasas de impuestos programables mas Exento.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>160 teclas de ventas directas (productos y/o Departamentos).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Display Operador de 8 l&iacute;neas y 22 caracteres por l&iacute;nea.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Totalmente programable por PC. (Software incluido).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Velocidad de impresi&oacute;n programable.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Display de cliente de 9 d&iacute;gitos</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 Cajeros programables.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Men&uacute;s y Cajeros protegidos por clave (NO se utiliza llave).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 Medios de Pago (Tarjeta de cr&eacute;dito, Tickets, Cheque y Efectivo).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja 37 tipos de c&oacute;digos de barra.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Gaveta de dinero removible con 8 compartimientos para monedas y 5 para billetes.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Variedad de reportes (Z, X, PLU, Departamentos, IVA, Cajeros, Gaveta)</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja descuentos y recargos por porcentaje.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja descuento por monto.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Control de inventario (Software incluido).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Obtenci&oacute;n de reportes.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Software para la preparaci&oacute;n del libro de ventas.(Software incluido).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Anulaci&oacute;n bajo clave.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Consulta de precios durante la venta.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 've', 1),
(5, 'Descripcion CRD81F', '<div>\r\n							<div>CAJA REGISTRADORA CRD81F\r\n<br><br>\r\n<ul><li>Única Caja Registradora que trabaja hasta 72 horas sin suministro eléctrico.</li><li>Capacidad para imprimir los datos fiscales del cliente (suministra crédito fiscal).</li><li>Distribuidores y Centros de servicio técnico a nivel nacional.</li></ul></div>\r\n					</div><br>', 'product', 1, 'es', 've', 2),
(6, 'Caracteristicas CRD81F', '<table border="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>Maneja hasta 3000 productos.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>8 departamentos programables.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Memoria fiscal con capacidad para 2200 reportes Z (6 a&ntilde;os).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Capacidad para imprimir la raz&oacute;n social y R.I.F. del cliente.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Papel de 57 mm de ancho.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>No borra la programaci&oacute;n de productos.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bater&iacute;a interna que permite utilizar la Caja Registradora hasta por 72 horas sin conexi&oacute;n a la red el&eacute;ctrica.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Impresor de alta velocidad t&eacute;rmico: Cliente y Auditoria. (40-50mm/s).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 tasas de impuestos programables mas Exento.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>120 teclas de ventas directas (productos y/o Departamentos).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Display Operador de 1 l&iacute;nea rotativa de 14 caracteres.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Totalmente programable por PC.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Velocidad de impresi&oacute;n programable.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Display de cliente de 14 d&iacute;gitos rotativos.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 Cajeros programables.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Men&uacute;s y Cajeros protegidos por clave (NO se utiliza llave).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 Medios de Pago (Tarjeta de cr&eacute;dito, Tickets, Cheque y Efectivo).</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja 37 tipos de c&oacute;digos de barra.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Gaveta de dinero removible con 8 compartimientos para monedas y 5 para billetes.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Variedad de reportes (Z, X, PLU, Departamentos, IVA, Cajeros, Gaveta)</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja descuentos por porcentaje.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja descuento por monto.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Control de inventario.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Anulaci&oacute;n bajo clave.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Consulta de precios durante la venta.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 've', 2),
(7, 'Descripcion NX5400 ', '<div>\r\n							<div>CAJA REGISTRADORA NX5400\r\n<br><br>\r\n\r\n<ul><li>Capacidad para imprimir los datos fiscales del cliente(suministra crédito fiscal).</li><li>Conexión en red RS-485 hasta 32 cajas registradoras.</li><li>Distribuidores y Centros de servicio técnico a nivel nacional.</li></ul></div>\r\n					</div><br>', 'product', 1, 'es', 've', 3),
(8, 'Caracteristicas NX5400 ', '<div><ul><li>Impresor matricial.</li><li>Ticket de cliente y ticket de auditoría.</li><li>Papel Bond de 44.5mm de ancho.</li><li>Capacidad hasta 4480 PLU´s.</li><li>10 Grupos principales, 98 Grupos A y 25 Grupos B.</li><li>Puerto RS-232 para comunicación con PC.</li><li>Puerto RS-485 para conexión en red (Hasta 32 equipos).</li><li>4 Tasas de impuesto más 1 Exento.</li><li>5 medios de pago ( Tarjeta 1 y 2, cheque, tickets y efectivo).</li><li>Recargos y descuentos por porcentaje y por monto.</li><li>Gaveta de dinero con 9 compartimientos para monedas y 5 para billetes.</li><li>3 niveles de precios para PLU´s.</li><li>23 cajeros programables.</li><li>Memoria fiscal para 1850 reportes Z.</li><li>Diversdidad de reportes (Z, X, PLU´s, ventas por hora, ventas por cajero, ventas por grupo, etc).</li><li>PLU´s programables en teclado directamente.</li><li>20 teclas de departamentos directos.</li><li>Capacidad para llevar inventario.</li><li>Velocidad de impresión 5 líneas por segundo.</li><li>Capacidad para imprimir los datos del cliente (credito fiscal).</li></ul></div><br>', 'product', 1, 'es', 've', 3),
(9, 'Descripcion PP1F3 ', '<div>\r\n							<div>IMPRESORA FISCAL PP1F3\r\n<br><br>\r\n<ul><li>Capacidad para imprimir los datos fiscales del cliente(suministra crédito fiscal).</li><li>Única impresora fiscal con display de precio al cliente.</li><li>Única impresora fiscal que trabaja hasta 72 horas sin suministro de corriente eléctrica.</li><li>Conexión con los mejores software del país.</li><li>Distribuidores y Centros de servicio técnico a nivel nacional.</li></ul></div>\r\n					</div><br>', 'product', 1, 'es', 've', 5),
(10, 'Caracteristicas PP1F3 ', '<div><ul><li>Impresora térmica.</li><li>2 estaciones de impresión.</li><li>Papel térmico de 57mm de ancho.</li><li>Cortador de papel manual.</li><li>Puerto controlador de Gaveta.</li><li>Única impresora fiscal con display incluido.</li><li>Lector fiscal directo por panel de la impresora.</li><li>3 Tasas de impuesto programables más 1 Exento.</li><li>8 Líneas de Logo.</li><li>8 Líneas de pie de página.</li><li>32 caracteres por línea.</li><li>16 medios de pago.</li><li>Manejo de cajeros.</li><li>Memoria fiscal 2000 reportes Z.</li><li>Facturas, notas de crédito, documentos no fiscales.</li><li>Velocidad de impresión 12 líneas por segundo.</li></ul></div><br>', 'product', 1, 'es', 've', 5),
(11, 'Descripcion SRP-270', '<div>\r\n							<div>IMPRESORA FISCAL SRP-270\r\n<br><br>\r\n<ul><li>Capacidad para imprimir los datos fiscales del cliente(suministra crédito fiscal).</li><li>Conexión con los mejores del software país.</li><li>Distribuidores y Centros de servicio técnico a nivel nacional.</li></ul></div>\r\n					</div><br>', 'product', 1, 'es', 've', 6),
(12, 'Caracteristicas SRP-270', '<div><ul><li>Impresora matricial.</li><li>1 estación de impresión original y copia.</li><li>Papel químico de 75mm de ancho.</li><li>Cortadora de papel automático.</li><li>Puerto controlador de Gaveta.</li><li>Puerto controlador de display (20 dígitos por 2 líneas).</li><li>Puerto para lector fiscal.</li><li>3 Tasas de impuesto más 1 Exento.</li><li>8 Líneas de Logo.</li><li>8 Líneas de pie de página.</li><li>42 caracteres por línea.</li><li>16 medios de pago.</li><li>Manejo de cajeros.</li></ul>\r\n</div><br>', 'product', 1, 'es', 've', 6),
(13, 'Descripcion SRP-350', '<div>\r\n							<div>IMPRESORA FISCAL BIXOLON SRP-350<br><br>La\r\n BIXOLON SRP-350 es una impresora fiscal que brinda la mayor rapidez de \r\nimpresión en el mercado, cuenta con un diseño robusto y seguro \r\ncumpliendo así las normativas fiscales del país.La impresora \r\nposee un cortador automático de papel, impresión de códigos de barra, \r\nconexiones múltiples como Rs232 para comunicaciones y gavetas de dinero \r\nexternas.Diseño compacto y elegante, con la mayor rapidez de impresión y altas prestaciones en rendimiento.</div>\r\n					</div><br>', 'product', 1, 'es', 've', 7),
(14, 'Caracteristicas SRP-350', '<div><ul><li>Impresora térmica.</li><li>1 estación de impresión.</li><li>Papel térmico de 80mm de ancho.</li><li>Cortadora de papel manual.</li><li>Puerto controlador de Gaveta.</li><li>Puerto controlador de display (20 dígitos por 2 líneas).</li><li>Puerto para lector fiscal.</li><li>3 Tasas de impuesto más 1 Exento.</li><li>8 Líneas de Logo.</li><li>8 Líneas de pie de página.</li><li>54 caracteres por línea (Modo Expandido).</li><li>16 medios de pago.</li><li>Manejo de cajeros.</li><li>Memoria fiscal 2000 reportes Z.</li><li>Facturas, notas de crédito, documentos no fiscales.</li><li>Velocidad de impresión 200mm por segundo.</li><li>Electronic Journal de 1Gb de Capacidad.</li><li>Almacena hasta 500.000 copias de facturas.</li><li>Capacidad para imprimir códigos de barra.</li><li>Reportes de auditoría por fecha, numero y/o tipo de documento.</li><li>Sistema "Carga fácil" para cargar el papel.</li></ul></div><br>', 'product', 1, 'es', 've', 7),
(15, 'Descripcion MICROLINE 1120', '<div>\r\n							<div>IMPRESORA OKI MICROLINE 1120<br><br>La\r\n OKI MICROLINE 1120 es una impresora manufacturada con elevados \r\nestándares de calidad, los cuales se transforman en un elegante diseño, \r\nofreciendo robustez y seguridad sin dejar de cumplir con las normativas \r\nde ley.El cabezal de impresión de esta máquina, tiene un diseño \r\núnico en el mercado. Este diseño consta de 9 agujas en una disposición \r\ndiagonal, la cual permite realizar impresiones más rápidas y limpias, ya\r\n que, esto evita que las agujas rocen entre sí.Al permitir la \r\nfácil recirculación de las micro virutas de papel, estas no se quedan \r\natoradas entre las agujas, dándole así más vida útil al cabezal.Además,\r\n el cabezal imprime bidireccionalmente, por lo que se mejora \r\nconsiderablemente la velocidad de impresión. Es la impresora que estaban\r\n esperando, gracias a sus 10,000 horas de Tiempo Promedio Entre Fallas \r\n(MTBF), 67% por encima de su competidora más cercana. Conecte la \r\nimpresora MICROLINE 1120 y ésta se mantendrá imprimiendo día tras día.</div>\r\n					</div><br>', 'product', 1, 'es', 've', 8),
(16, 'Caracteristicas MICROLINE-1120', '<div><ul><li>Impresora Matriz de Punto.</li><li>1 estación de impresión.</li><li>Hojas papel bond de hasta 5 copias tamaña carta.</li><li>Cortadora de papel manual.</li><li>Hojas continuas, multipartes y sencillas.</li><li>Puerto controlador de display (20 dígitos por 2 líneas).</li><li>Puerto para lector fiscal.</li><li>3 Tasas de impuesto más 1 Exento.</li><li>8 Líneas de Logo.</li><li>8 Líneas de pie de página.</li><li>Capacidad de impresión de 80 columnas reales.</li><li>16 medios de pago.</li><li>Manejo de cajeros.</li><li>Memoria fiscal 2000 reportes Z.</li><li>Facturas, notas de crédito, documentos no fiscales.</li><li>Velocidad de impresión 333 contactos por segundo.</li><li>Electronic Journal de 1Gb de Capacidad.</li><li>Almacena hasta 500.000 copias de facturas.</li></ul></div><br>', 'product', 1, 'es', 've', 8),
(17, 'Descripcion Papel Termico', '<div>\r\n							<div>\r\n\r\nPapel<br>\r\n\r\nDiferentes tipos de papel,\r\nEstán especialmente diseñados para cada aplicación.\r\n\r\nTienen una excelente calidad de imagen y garantizan una perfecta definición de los códigos de barras.\r\n\r\nTienen diferentes niveles de durabilidad y sensibilidad, adecuados a su aplicación final.\r\n\r\nGarantizan un perfecto comportamiento durante su manipulación y uso.\r\n\r\n</div>\r\n					</div><br>', 'product', 1, 'es', 've', 10),
(18, 'Caracteristicas Papel Térmico', 'Papel soporte sobre el que se aplican las capas de estucado. Toda la \r\ncelulosa y el papel soporte de la gama se produce en fabricas de grupo \r\nTermax.<br><br>\r\nCapa de Preestuco o precapa que cubre el soporte para garantizar una \r\nsuperficie uniforme y lisa. A mayor lisura de esta capa, mejor \r\nresolución y definición de esta imagen.<br><br>\r\nCapa térmica: es la capa superior formada por una gran cantidad de \r\ncompuestos químicos que sometidos al calor reaccionan entre si y \r\ndesarrollan la imagen. Los tres principales componentes de esta capa \r\nson: un colorante, un desarrollador de color y un sensibilizador. \r\n<br>', 'product', 1, 'es', 've', 10),
(19, 'Tipos Papel Térmico', 'Están especialmente diseñados para cada aplicación.\r\n\r\nTienen una excelente calidad de imagen y garantizan una perfecta definición de los códigos de barras.\r\n\r\nTienen diferentes niveles de durabilidad y sensibilidad, adecuados a su aplicación final.\r\n\r\nGarantizan un perfecto comportamiento durante su manipulación y uso.<br>', 'product', 1, 'es', 've', 10),
(20, 'Especificaciones Papel Térmico', 'Aplicaciones:\r\nRecibos en puntos de venta (POS), Extractos Bancarios (ATM) y Fax.<br>\r\n\r\nSensibilidad:\r\nEstandar.<br>\r\n\r\nDurabilidad de la imagen:\r\n5 años.<br>', 'product', 1, 'es', 've', 10),
(21, 'Ventajas Papel Térmico', 'Es rápida, compacta y de funcionamiento silencioso.\r\n<br>\r\nEs fiable: la impresión es nítida y clara con una resolución optima que \r\npermite leer la imagen perfectamente mediante lectores de códigos de \r\nbarras.\r\n<br>\r\nEs ecológica y económica: no se utiliza toner, cinta u otros \r\nconsumibles. Requiere de poca energía para su funcionamiento y tiene un \r\nbajo coste de mantenimiento. <br>', 'product', 1, 'es', 've', 10),
(22, 'Descripcion CRD81FJ', '<div>\r\n							<div>CAJA REGISTRADORA CRD81FJ<br><br>La\r\n CRD81FJ es una Caja Registradora fabricada con los más elevados \r\nestándares de calidad, robustez y seguridad. Ofrece un elegante diseño, \r\nbrindando una independización a la hora de llevar las operaciones del \r\ndía a día del negocio.Permite una fácil manipulación mediante un \r\nteclado plano y un display. La misma es programable tanto como por PC \r\ncomo manualmente.Maneja hasta 8 departamentos y hasta 3000 \r\nproductos. Capacidad de manejar 4 cajeros y brindar rapidez a la hora de\r\n facturar con 120 teclas de ventas directas.</div>\r\n					</div><br>', 'product', 1, 'es', 've', 11),
(23, 'Caracteristicas CRD81FJ', '<div><ul><li>Trabaja hasta 72 Horas sin suministro de Corriente Electrica</li><li>Capacidad para Imprimir los Datos Fiscales del Cliente (da Credito Fiscal)</li><li>Maneja hasta 3000 productos.</li><li>8 departamentos programables.</li><li>Memoria Fiscal con capacidad   de 2200 reportes Z.</li><li>Memoria de Auditoria Electronica 2GB.</li><li>Capacidad para imprimir la razon social y RIF del cliente.</li><li>Papel de 57 mm de ancho.</li><li>Bateria interna que permite utilizar la Caja Registradora hasta por 72 horas \r\n        sin conexion a la red electrica.</li><li>4  Tasas de impuestos programables mas Exento.</li><li>120 Teclas de ventas directas.</li><li>Programable por PC.</li><li>4 Cajeros programables.</li><li>Maneja 37 tipos de codigo de barras.</li><li>Gaveta de dinero removible.</li><li>Maneja descuentos por porcentaje.</li><li>Maneja descuentos por montos.</li><li>Control de inventarios.</li><li>Anulacione bajo clave.</li><li>4 Medios de pago.</li></ul></div><br>', 'product', 1, 'es', 've', 11),
(24, 'Descripcion CR68AFJ', '<div>\r\n							<div>\r\nCAJA REGISTRADORA CR68AFJ<br><br>\r\nLa CR68AFJ, es una Caja registradora Fiscal fabricada bajo estrictos estándares \r\n    de calidad, que dan a su moderno diseño, la seguridad y la robustez necesaria \r\n    para cumplir con las normativas fiscales vigentes en el país.\r\n\r\nEs un equipo diseñado y adaptado a las nuevas exigencias tecnológicas, \r\n    lo cual brinda una mayor facilidad de manejo y programación.\r\n\r\nEs un equipo de diseño compacto y de altas prestaciones en rendimiento, \r\n    capacidad de almacenamiento e interconexión con otros dispositivos.\r\n\r\n<ul><li>Única Caja Registradora que trabaja hasta 72 horas sin suministro \r\n        eléctrico.</li><li>Capacidad para imprimir los datos fiscales del cliente (suministra crédito fiscal).</li><li>Distribuidores y Centros de servicio técnico a nivel nacional.</li></ul></div>\r\n					</div><br>', 'product', 1, 'es', 've', 12),
(25, 'Caracteristicas CR68AFJ', '<div><ul><li>Memoria de Auditoria electronica de 2GB.</li><li>Maneja hasta 25000 productos.</li><li>99 departamentos programables.</li><li>Memoria fiscal con capacidad para 2200 reportes Z (6 años).</li><li>Capacidad para imprimir la razón social y R.I.F. del cliente.</li><li>Papel de 57 mm de ancho.</li><li>No borra la programación de productos.</li><li>Batería interna que permite utilizar la Caja Registradora hasta por 72 horas \r\n        sin conexión a la red eléctrica.</li><li>Impresor de alta velocidad térmico: Cliente y Auditoria. (40-50mm/s).</li><li>4 tasas de impuestos programables mas Exento.</li><li>160 teclas de ventas directas (productos y/o Departamentos).</li><li>Display Operador de 8 l_íneas y 22 caracteres por línea.</li><li>Totalmente programable por PC. (Software incluido).</li><li>Velocidad de impresión programable.</li><li>Display de cliente de 9 dígitos</li><li>4 Cajeros programables.</li><li>Menús y Cajeros protegidos por clave (NO se utiliza llave).</li><li>4 Medios de Pago (Tarjeta de crédito, Tickets, Cheque y Efectivo).</li><li>Maneja 37 tipos de códigos de barra.</li><li>Gaveta de dinero removible con 8 compartimientos para monedas y 5 para billetes.</li><li>Variedad de reportes (Z, X, PLU, Departamentos, IVA, Cajeros, Gaveta)</li><li>Maneja descuentos y recargos por porcentaje.</li><li>Maneja descuento por monto.</li><li>Control de inventario (Software incluido).</li><li>Obtención de reportes.</li><li>Software para la preparación del libro de ventas.(Software incluido).</li><li>Anulación bajo clave.</li><li>Consulta de precios durante la venta.</li></ul>\r\n<ul><li>Comunicación con PC vía RS232 (Cable incluido).</li><li>Comunicación con Teclado PS2.</li><li>Comunicación con Lector de Código de Barra.</li><li>Comunicación en red entre cajas registradoras mediante HUB Serial (opcional).</li><li>Manejo en red de balanza (Varias maquinas usan una misma balanza).</li><li>El equipo es actualizado totalmente por PC, por lo que no se tienen que sustituir Eprom''''s, \r\n        memorias o algún dispositivo para realizar cambios de Versión o actualizaciones \r\n        en el firmware de la máquina, incluyendo la actualización por la reconversión \r\n        de la moneda y las disposiciones de la R591 y R592.</li></ul></div><br>', 'product', 1, 'es', 've', 12),
(28, 'Descripcion HKA 112', '<div>\r\n							<div>IMPRESORA FISCAL HKA 112<br><br>La\r\n HKA112 es una Impresora Fiscal térmica que cuenta con un elegante y \r\nmoderno diseño. Incorpora un display adicional, con el fin de mostrar al\r\n cliente información de su compra.Permite la impresión de \r\ndocumentos a través del manejo del panel de control, siendo así \r\nindependiente de un POS para impresión de ciertos documentos fiscales y \r\nno fiscales. La impresora cuenta con un panel de control integrado que \r\nposee las funciones del hand held utilizado por el SENIAT.Es un equipo de diseño compacto y alto rendimiento, con múltiples conexión como USB y Rs232.</div>\r\n					</div><br>', 'product', 1, 'es', 've', 14),
(29, 'Caracteristicas HKA 112', '<div><ul><li>Impresora térmica de 80mm con corte automático</li><li>Velocidad de impresión de 120-150mm/s</li><li>Memoria de auditoría electrónica de 2GB</li><li>Sistema de carga fácil de papel</li><li>8 líneas de logo</li><li>8 líneas de pie de página</li><li>54 caracteres por línea (en modo expandido)</li><li>Comunicación serial RS232 o USB</li><li>Memoria fiscal con capacidad de 2000 Reportes Z</li><li>Impresión de logotipos, códigos de barra y QR</li><li>Puerto controlador de Gaveta (RS232 - 24 Volts)</li><li>3 tasas de impuesto más 1 exento y 16 medios de pago</li></ul>\r\n<h3>Novedades</h3>\r\n	\r\n<ul><li>Pantalla frontal LCD de 2 líneas de 16 caracteres</li><li>Panel de control inteligente para emisión de reportes X y Z protegido con contraseña</li><li>Puerto de comunicación USB integrado</li><li>Display de precios integrado LCD</li><li>Exclusivo diseño compacto</li><li>RS232 conector USB tipo B para comunicación con PC</li><li>Apagado parcial (standby) por botón frontal</li></ul></div><br>', 'product', 1, 'es', 've', 14),
(30, 'Descripcion PS1', '<div>\r\n							<div><ul><li>100% compatible con las cajas registradoras Aclas</li><li>Incluye batería recargable capaz de trabajar 24 horas</li><li>Versatilidad de uso: Modalidad sólo peso y \r\nModalidad mostrador (peso, precio e importe)</li><li>Poste de display opcional</li><li>Fácil movilidad (2,9 Kg)</li></ul></div>\r\n					</div><br>', 'product', 1, 'es', 've', 15),
(31, 'Caracteristicas PS1', '<div><ul><li>Almacena 70 PLUs</li><li>Función de precio fijo</li><li>Peso máximo: 15 Kg</li><li>La balanza entra en stand-by automáticamente\r\n (definido por el usuario)</li><li>Capaz de activarse por cualquier carga u operación\r\n debido al mecanismo de agitación interior</li><li>Ajuste de la luz de fondo, capaces de ahorrar energía\r\n eléctrica y prolongar la vida útil de la batería seca</li><li>2 Display LCD de 3 líneas, capaces de mostrar el peso,\r\n el precio unitario y el precio total en la misma pantalla</li><li>Con cinco teclas de acumulación capaces de acumular\r\n cinco vendedores al mismo tiempo, mientras que las balanzas\r\n electrónicas tradicionales sólo acumulan un vendedor</li><li>Capaz de calcular el vuelto\r\n automáticamente “Cambio”</li><li>Capaz de mostrar el reporte diario de venta</li></ul></div><br>', 'product', 1, 'es', 've', 15),
(34, 'Descripcion SRP-280', 'La impresora fiscal SRP-280 de matriz de puntos cuenta con alto \r\ndesempeño, de compacto y elegante diseño comparte la robustez y \r\nfiabilidad característica de los equipos Bixolon.\r\n<br>\r\nEsta impresora se ajusta a las regulaciones de ley establecidas SENIAT \r\npor el para máquinas fiscales bajo la providencia SENIAT/GF/00055 de \r\nfecha 18 julio del 2014.<br>', 'product', 1, 'es', 've', 17),
(35, 'Caracteristicas SRP-280', '<div><ul><li>Impresión por Matriz de Puntos</li><li>Rollo Bond de 75mm</li><li>Posee Auto Cutter</li><li>120 caracteres por línea por producto</li><li>Memoria Fiscal para almacenar hasta 2000 Reportes Z</li><li>Memoria de Auditoria Electrónica de 2GB de almacenamiento</li><li>Comunicación vía puerto serial con PC, display externo y gaveta</li><li>Generación de facturas, notas de crédito y documentos no fiscales</li><li>3 tasas de impuesto programables mas 1 exento</li><li>8 líneas de encabezado y 8 líneas de pié de página</li><li>Maneja hasta 30 cajeros y 16 medios de pago</li></ul></div><br>', 'product', 1, 'es', 've', 17),
(36, 'Descripcion CR2100', 'Equipo de impresión térmica de nueva tegnología y fabricada bajo altos estándares \r\nde calidad. Permite una fácil manipulación debido a que cuenta con un teclado plano \r\ny un display enmarcados en su clásico diseño.<br>\r\nProgramable manualmente o mediante una PC, esta caja registradora se ajusta a las \r\nregulaciones de ley establecidas por el SENIAT.<br>', 'product', 1, 'es', 've', 18),
(37, 'Caracteristicas CR2100', '<table border="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>Memoria Fiscal con capacidad de 2200 Reportes Z</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Generaci&oacute;n de facturas, notas de cr&eacute;dito y notas de d&eacute;bito</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bateria Interna Recargable con tapa de mantenimeinto.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Trabaja hasta 74 horas sin suministro de Corriente El&eacute;ctrica</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Reportes: diario Z, X, reporte de Cajero, entre otros.</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>3 tasas de impuestos programables mas exento</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4 cajeros programables</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Control de inventarios</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja 4 medios de pago</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>120 teclas de ventas directas</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Maneja 37 tipos de c&oacute;digo de barras</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Manejo de lectores de C&oacute;digos de Barras</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Alarma indicadora de Bater&iacute;a Baja</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Modo de ahorro de energ&iacute;a Autom&aacute;tico</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Memoria con capacidad de 2GB</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Gaveta de dinero removible</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 've', 18),
(38, 'Descripcion SRP-812', '<div>\r\n							<div>La Impresora Fiscal BIXOLON SRP-812 es una impresora fiscal que brinda una alta \r\nvelocidad de impresión y un diseño compacto y seguro, combinado con la robustez \r\ny elegancia características de los equipos BIXOLON.\r\n \r\nEsta impresora se ajusta a las regulaciones de ley establecidas por el SENIAT \r\n    para máquinas fiscales bajo la providencia SENIAT/GF/00056 de fecha 28 de \r\n    agosto de 2014.</div>\r\n					</div><br>', 'product', 1, 'es', 've', 19),
(39, 'Caracteristicas SRP-812', '<div><ul><li>Dimensiones: 203x 145 x 146 mm.</li><li>Peso: 1,6Kg sin rollo de papel.</li><li>Resolución: 180DPI.</li><li>Impresión Térmica directa, alta velocidad de impresión.</li><li>Rollo de 80mm con auto-cutter.</li><li>Memoria fiscal con capacidad de almacenamiento de hasta 4000 reportes Z.</li><li>Memoria de Auditoría Electrónica de 2GB de almacenamiento.</li><li>Panel de Control frontal con tres LEDs indicadores de estado y un botón de alimentación de papel.</li><li>Generación de Facturas, Notas de Crédito, Notas de Débito y Documentos No Fiscales.</li><li>Interfaces de comunicación por puerto RS232 y puerto USB tipo B para PC.</li><li>Puertos para manejo de gaveta y display.</li><li>Tres (3) Tasas de impuesto programables, más un (1) Exento.</li><li>10 líneas de encabezado y 10 líneas de pie de página.</li><li>Impresión de logos gráficos y códigos de Barra.</li><li>Capacidad de programación de hasta 30 cajeros y 24 medios de pagos.</li><li>Funciones para cambio de formato de letra fuente: tamaño, negrita, centrado y fondo invertido.</li></ul></div><br>', 'product', 1, 'es', 've', 19),
(40, 'Descripcion OS2X', '<p>OS2X Balanza Con dise&ntilde;o Simple elegante y robusta, para trabajar en interfaz con sistemas de punto de venta y/o Cajas registradoras, posee protocolos de f&aacute;cil integraci&oacute;n adaptado al mercado, con rendimiento de alta sensibilidad, impermeable, anti-incrustantes y f&aacute;cil de limpiar. Se alimenta directamente desde un puerto USB o ECR, su consumo es de tan solo 5V 500mA. Maneja un sistema de ajuste de la aceleraci&oacute;n de la gravedad y su ajuste de par&aacute;metros m&eacute;tricos o calibraci&oacute;n mediante el software de la balanza.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 've', 20),
(41, 'Caracteristicas OS2X', '<table border="1" cellpadding="1" cellspacing="1" style="height:623px; width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Caracter&iacute;sticas</strong></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Clase de precisi&oacute;n:</strong></td>\r\n			<td>III</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tara: </strong></td>\r\n			<td>-5.998Kg | -14.998Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Intervalo de verificaci&oacute;n: </strong></td>\r\n			<td>e=2g/5g</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Resoluci&oacute;n interna o precisi&oacute;n: </strong></td>\r\n			<td>1/30000</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Protector de sobrecarga</strong></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Teclado: </strong></td>\r\n			<td>Al pasar el 120%</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Software: </strong></td>\r\n			<td>OS2 Manager</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Compatibilidad: </strong></td>\r\n			<td>POS, ECR</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Capacidad:</strong></td>\r\n			<td>15Kg | 30Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Par&aacute;metros f&iacute;sicos</strong></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Botones t&aacute;ctiles:</strong></td>\r\n			<td>Bot&oacute;n de Encendido, Tara y Cero</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Pantalla:</strong></td>\r\n			<td>LCD 5x8 campos</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Dimensiones: </strong></td>\r\n			<td>34x26x10cm</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Peso: </strong></td>\r\n			<td>3.7Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Bandeja: </strong></td>\r\n			<td>Acero inoxidable de 34x26x1.5cm</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Patas para base: </strong></td>\r\n			<td>De tornillo, ajustables</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Base: </strong></td>\r\n			<td>Met&aacute;lica</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Fuente de alimentaci&oacute;n: </strong></td>\r\n			<td>120V AC a 5V DC, 500mA</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Interfaces:</strong></td>\r\n			<td>RS 232, RJ-11</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Entorno operativo</strong></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Temperatura de trabajo:</strong></td>\r\n			<td>0 &deg;C ~ +40 &deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Temperatura de almacenamiento: </strong></td>\r\n			<td>-20 &deg;C ~ +70 &deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Humedad relativa:</strong></td>\r\n			<td>5% ~ 85%</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 've', 20),
(42, 'Descripcion CR2300', 'Equipo de impresión térmica de nueva tecnología y fabricada bajo altos estándares \r\nde calidad. Permite una fácil manipulación debido a que cuenta con un teclado plano \r\ny un display enmarcados en su clásico diseño.<br>\r\nProgramable manualmente o mediante una PC, esta caja registradora se ajusta a las \r\nregulaciones de ley establecidas por el SENIAT.<br><br>\r\n<b>MODO RESTAURANT</b><br>\r\nEste equipo está dotado con la capacidad de llevar paralelamente hasta \r\n100 cuentas a la vez y modificar cualquiera de ellas así como de \r\nimprimir dichas cuentas, este sistema la hace especialmente útil para la\r\n toma de pedidos y la generación de respaldos escritos de esas órdenes \r\npara  organizar los pedidos.\r\n<br>', 'product', 1, 'es', 've', 21),
(43, 'Caracteristicas CR2300', '<div><ul><li>Memoria Fiscal con capacidad de 2200 Reportes Z</li><li>Generación de facturas, notas de crédito, notas de débito y documentos no fiscales</li><li>Bateria Interna Recargable con tapa de mantenimeinto</li><li>Trabaja hasta 72 horas sin suministro de Corriente Eléctrica</li><li>Reportes: diario Z, X, reporte de Cajero, entre otros.</li><li>3 tasas de impuestos programables mas exento</li><li>4 cajeros programables</li><li>Control de inventarios</li><li>Maneja 4 medios de pago</li><li>160 teclas de ventas directas</li><li>Maneja 37 tipos de código de barras</li><li>Manejo de lectores de Códigos de Barras</li><li>Alarma indicadora de Batería Baja</li><li>Modo de ahorro de energía Automático</li><li>Memoria con capacidad de 2GB</li><li>Gaveta de dinero removible</li></ul></div><br>', 'product', 1, 'es', 've', 21),
(44, 'Descripcion PP9', 'La impresora <b>ACLAS PP9</b> es una impresora fiscal térmica que cuenta\r\n con un diseño compacto y moderno. Posee una batería interna, lo que \r\npermite continuar trabajando, aún desconectado de la toma eléctrica. \r\nGracias a su display incorporado y su precio atractivo, es la solución \r\nperfecta para negocios pequeños y medianos.<br><br>\r\nPermite la impresión de documentos a través del manejo del panel de \r\ncontrol, siendo así independiente de un POS para impresión de ciertos \r\ndocumentos fiscales y no fiscales. La impresora cuenta con un panel de \r\ncontrol integrado que posee las funciones del hand held utilizado por el\r\n SENIAT.<br><br>\r\n	Esta impresora se ajusta a las regulaciones de ley establecidas por el \r\nSENIAT para máquinas fiscales bajo la providencia SENIAT/GF/00060 de \r\nfecha 7 enero del 2015.\r\n<br>', 'product', 1, 'es', 've', 22),
(45, 'Caracteristicas PP9', '<div><ul><li>Dimensiones: 138 x 236 x 192 mm.</li><li>Peso: 1,60 kg sin rollo de papel.</li><li>Resolución de 203 DPI.</li><li>Panel de frontal con un (1) y dos (2) Leds.</li><li>Impresión térmica con cortador manual</li><li>Generación de facturas, notas de crédito, notas de débito y documentos no fiscales.</li><li>Impresión de logos gráficos, códigos de Barra y Códigos QR.</li><li>Memoria Fiscal con capacidad de almacenamiento de hasta 2000 Reportes Z.</li><li>Memoria de Auditoría Electrónica de 2GB de almacenamiento.</li><li>Capacidad de programación de hasta 30 cajeros y 24 medios de pagos.</li><li>8 líneas de encabezado y 8 líneas de píe de página.</li><li>Tres (3) tasas de impuesto programables, más un (1) exento.</li><li>Comunicación vía puerto serial con PC, y un puerto USB Tipo B.</li><li>Batería Interna incluida.</li><li>Puerto para gaveta de 12V.</li></ul></div><br>', 'product', 1, 'es', 've', 22),
(48, 'Descripcion P3100DL ', 'La impresora fiscal <b>P3100DL</b> es una impresora láser, ideal para \r\nmedianas y grandes empresas, su cartucho maneja grandes cargas de \r\ntrabajo, cuando se mantiene en modo de reposo se ahorra más energía, \r\nproporciona impresiones rápidas y eficientes para cualquiera que sea la \r\ntarea.<br><br>\r\n\r\n\r\n	Esta impresora se ajusta a las regulaciones de ley establecidas por el \r\nSENIAT para máquinas fiscales bajo la providencia SENIAT/GF/00058 de \r\nfecha 7 enero del 2015<br>', 'product', 1, 'es', 've', 24),
(49, 'Caracteristicas P3100DL ', '<div><ul><li>Dimensiones: 370 x 370 x 279 mm.</li><li>Peso: 9 kg sin tóner.</li><li>Resolución de 1200x600 DPI.</li><li>Panel de control contiene un botón y dos indicadores LED.</li><li>Impresión a láser tamaño carta</li><li>Configuración vertical (80 a 136 caracteres) y horizontal (187 Caracteres)</li><li>Generación de facturas, notas de crédito, notas de débito y documentos no fiscales.</li><li>Impresión de logos gráficos, códigos de Barra y Códigos QR y PDF417</li><li>Memoria Fiscal con capacidad de almacenamiento de hasta 4000 Reportes Z.</li><li>Memoria de Auditoría Electrónica de 2GB de almacenamiento.</li><li>Capacidad de programación de hasta 30 cajeros y 24 medios de pagos.</li><li>8 líneas de encabezado y 8 líneas de píe de página.</li><li>Tres (3) tasas de impuesto programables, más un (1) exento.</li><li>Comunicación vía puerto serial con PC, y un puerto USB Tipo B.</li><li>Puerto para Display.</li></ul></div><br>', 'product', 1, 'es', 've', 24),
(50, 'Descripcion DT-230 CO', '<p>Impresora Fiscal Dascom DT-230<br />\r\n&nbsp;</p>\r\n\r\n<p>La impresora que reune la robustez y fiabilidad de sus predecesoras, y las completa con una velocidad de impresi&oacute;n superior y nuevas funcionalidades como la emisi&oacute;n&nbsp; de notas de d&eacute;bito, la impresi&oacute;n de c&oacute;digos QR y la conexi&oacute;n a trav&eacute;s de puerto USB.</p>\r\n', 'product', 1, 'es', 'co', 25),
(51, 'Caracteristicas DT-230 CO', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th>Velocidad de impresi&oacute;n</th>\r\n			<td>260 mm / seg, 10,2 cent&iacute;metros por segundo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>72 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Directo, Impresi&oacute;n t&eacute;rmica por l&iacute;nea</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho del papel</th>\r\n			<td>80mm o 58mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi, 8pts/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>150 kilometros de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>RAM: 1Mbytes; Flash: 4Mbytes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad est&aacute;ndar</th>\r\n			<td>USB y gaveta de dinero</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad opcional</th>\r\n			<td>Serie, Paralelo, Ethernet y WiFi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>ESC / POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro m&aacute;ximo del rollo externo</th>\r\n			<td>83 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro n&uacute;cleo del rollo</th>\r\n			<td>13 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>Aproximadamente 1,8 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico L x W x H</th>\r\n			<td>147 &times; 198 &times; 146mm, 5.8x7.8x5.7 pulgadas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 100-240V / 50-60Hz DC 24V &plusmn; 5%, 2,1 A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura de funcionamiento</th>\r\n			<td>5-45 &deg; C, 41 a 113 &deg; F</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10-95% sin condensaci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Cortador</th>\r\n			<td>2.000.000 recortes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cubierta Splash, placa de pared para colgar</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>2 a&ntilde;os</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 25),
(52, 'Descripcion DT-230 PE', 'La\r\n impresora que reúne la robustez y fiabilidad de sus predecesoras, y las\r\n completa con una velocidad de impresión superior y nuevas \r\nfuncionalidades como la emisión&nbsp; de notas de débito, la impresión de \r\ncódigos QR y la conexión a través de puerto USB.<br>', 'product', 1, 'es', 'pe', 25),
(53, 'Caracteristicas DT-230 PE', '<div>\r\n			<div>\r\n				<span><span>• Impresión de códigos QR. (Sección de cabezal)<br>\r\n• Conexión USB. (Sección posterior)<br>\r\n• Sistema de carga fácil de papel. (Sección de papel)<br>\r\n• Velocidad de impresión de 200mm por segundo. (Imprimiendo un documento)<br>\r\n• Memoria de auditoria de 2GB, capaz de almacenar más de 700,000 documentos.<br>\r\n(Sección lateral)</span>\r\n</span>\r\n			</div>\r\n		</div><br>', 'product', 1, 'es', 'pe', 25),
(54, 'Descripcion DT-230 MX', 'La impresora POS térmica directa Dascom DT230<br>\r\n    ofrece fiabilidad, precisión y desempeño con un<br>\r\n    tamaño compacto, donde el espacio es limitado.<br>\r\n    El diseño amigable de la impresora permite tener<br>\r\n    ahorro de espacio con una cubierta de fácil<br>\r\n    apertura y un mecanismo de fácil carga de papel.<br>\r\n    Un panel sencillo permite un operación simple y<br>\r\n    efectiva haciendola fácil de usar.<br>\r\n    El mecanimos diseñado con una construcción<br>\r\n    robusta asegura una operación confiable, algo<br>\r\nindispensable para todas las operaciones en<br>\r\nambientes de transacciones<br>', 'product', 1, 'es', 'mx', 25),
(55, 'Caracteristicas DT-230 MX', '<div>\r\n\r\n			<div>\r\n				<span><div>Alta Velocidad de Impresión<br>\r\n   Fácil Carga de Papel<br>\r\n   USB incluído<br>\r\n  Flexibilidad de Interfaces Opcionales<br>\r\n  Incluyendo Wi-Fi<br>\r\n   Soporta Emulación ESC/POS<br>\r\n   Cortador Estándar de Alto Desempeño<br>\r\nSoporta Impresión en Modo de Página</div>\r\n</span>\r\n			</div>\r\n\r\n		</div><br>', 'product', 1, 'es', 'mx', 25),
(56, 'Descripcion DT-230 BO', 'La impresora que reune la robustez y fiabilidad de sus predecesoras, y \r\nlas completa con una velocidad de impresión superior y nuevas \r\nfuncionalidades como la emisión&nbsp; de notas de débito, la impresión de \r\ncódigos QR y la conexión a través de puerto USB.<br>', 'product', 1, 'es', 'bo', 25),
(57, 'Caracteristicas DT-230 BO', '<ul><li>Velocidad de impresión\r\n			260 mm / seg, 10,2 centímetros por segundo\r\n		</li><li>\r\n		\r\n			Ancho de impresión\r\n			72 mm\r\n</li><li>		\r\n		\r\n			Método de impresión\r\n			Directo, Impresión térmica por línea\r\n	  </li><li>\r\n		\r\n			Ancho del papel\r\n			80mm o 58mm\r\n</li><li>	  \r\n		\r\n			Resolución\r\n			203 dpi, 8pts/mm\r\n</li><li>	  \r\n		\r\n			Vida del cabezal de impresión\r\n			150 kilometros de papel\r\n</li><li>	  \r\n		\r\n			Memoria\r\n			RAM: 1Mbytes; Flash: 4Mbytes\r\n	  \r\n		\r\n			</li><li>Conectividad estándar\r\n			USB y gaveta de dinero\r\n</li><li>	  \r\n		\r\n			Conectividad opcional\r\n			Serie, Paralelo, Ethernet y WiFi\r\n	  \r\n		\r\n			</li><li>Emulación\r\n			ESC / POS\r\n	  \r\n		\r\n			</li><li>Diámetro máximo del rollo externo\r\n			83 mm</li><li>Diámetro núcleo del rollo\r\n			13 mm\r\n	  </li><li>\r\n		\r\n			Peso físico\r\n			Aproximadamente 1,8 kg\r\n</li><li>	  \r\n		\r\n			Tamaño físico L x W x H\r\n			147 × 198 × 146mm, 5.8x7.8x5.7 pulgadas\r\n</li><li>	  \r\n		\r\n			Voltaje\r\n			AC 100-240V / 50-60Hz DC 24V ± 5%, 2,1 A\r\n</li><li>	  \r\n		\r\n			Temperatura de funcionamiento\r\n			5-45 ° C, 41 a 113 ° F\r\n	  \r\n		\r\n			</li><li>Humedad\r\n			10-95% sin condensación\r\n	  \r\n		\r\n			</li><li>Cortador\r\n			2.000.000 recortes\r\n	  \r\n		\r\n			</li><li>Opciones\r\n			Cubierta Splash, placa de pared para colgar\r\n</li><li>	  \r\n		\r\n			Garantía\r\n			2 años </li></ul>  \r\n        <br>', 'product', 1, 'es', 'bo', 25),
(58, 'Descripcion HKA57 ', '<span>Impresora térmica de punto de venta. Su consumo de hasta 40% menos papel, se traduce en ahorros ahorro manteniendo su calidad.<br><br>Comunicación\r\n serial y USB, un puerto para gavetas de dinero y un amplio set de \r\nlibrerías y herramientas garantizan una integración rápida y amigable \r\ncon cualquier software comercial<br><br>Ideal si se requiere un equipo compacto de alto rendimiento a un precio accesible. Todo esto sumado al soporte Premium de HKA</span><br>', 'product', 1, 'es', 'bo', 26),
(59, 'Descripcion HKA57 ', '<span>Impresora térmica de punto de venta. Su consumo de hasta 40% menos papel, se traduce en ahorros ahorro manteniendo su calidad.<br><br>Comunicación\r\n serial y USB, un puerto para gavetas de dinero y un amplio set de \r\nlibrerías y herramientas garantizan una integración rápida y amigable \r\ncon cualquier software comercial<br><br>Ideal si se requiere un equipo compacto de alto rendimiento a un precio accesible. Todo esto sumado al soporte Premium de HKA</span><br>', 'product', 1, 'es', 'co', 26);
INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(60, 'Descripcion HKA57 ', '<span>Impresora térmica de punto de venta. Su consumo de hasta 40% menos papel, se traduce en ahorros ahorro manteniendo su calidad.<br><br>Comunicación\r\n serial y USB, un puerto para gavetas de dinero y un amplio set de \r\nlibrerías y herramientas garantizan una integración rápida y amigable \r\ncon cualquier software comercial<br><br>Ideal si se requiere un equipo compacto de alto rendimiento a un precio accesible. Todo esto sumado al soporte Premium de HKA</span><br>', 'product', 1, 'es', 'mx', 26),
(61, 'Caracteristicas HKA57 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n lineal t&eacute;rmica directa</td>\r\n		</tr>\r\n		<tr>\r\n		</tr>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>100 mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203dpi, 8 puntos/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>100mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Interfaces</th>\r\n			<td>Serial (RS232), USB, Gaveta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Modo pagina</th>\r\n			<td>No soportado</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>20 KB Memoria RAM<br />\r\n			2 MB Memoria Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Chino GB18030 24x24 (Simplificado/Chino Tradicional)<br />\r\n			Alfanum&eacute;rico ASCII 9x17 - 12x24<br />\r\n			Soporta fuentes definidas por usuario<br />\r\n			42 tipos de c&oacute;digo (Code page)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barra</th>\r\n			<td>UPC-A UPC-E, EAN8, EAN13, CODE39, ITF, CODEBAR, CODE128, CODE93</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sensores</th>\r\n			<td>Sin sensor de detecci&oacute;n de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indicadores LED</th>\r\n			<td>Indicador de encendido LED Verde<br />\r\n			Indicador de papel Indicador de error LED Rojo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Energ&iacute;a</th>\r\n			<td>Adaptador de corriente externo<br />\r\n			Entrada 100 -240 Vca 50-60Hz<br />\r\n			Salida 24 Vcc +/- 5% 2 A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel</th>\r\n			<td>Papel T&eacute;rmico est&aacute;ndar 58mm de ancho<br />\r\n			Cubierta de apertura hacia arriba<br />\r\n			Sistema de carga f&aacute;cil<br />\r\n			Rasgado Manual</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Comandos</th>\r\n			<td>Emulaci&oacute;n ESC/POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Especificaciones f&iacute;sicas</th>\r\n			<td>Condiciones de operaci&oacute;n 0~40 &deg;C/20~85HR<br />\r\n			Condiciones de Almacenamiento -20~70 &deg;C/5~95HR</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>186(largo)x114(ancho)x128(alto)mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>930 g</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida TPH</th>\r\n			<td>150 km</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Controladores</th>\r\n			<td>Windows 8<br />\r\n			32bit: Windows(Win7/Vista/XP/2000)<br />\r\n			64bit: Windows(Win7/Vista/XP/2000)<br />\r\n			Linux/Unix<br />\r\n			OPOS</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 26),
(62, 'Caracteristicas HKA57 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n lineal t&eacute;rmica directa</td>\r\n		</tr>\r\n		<tr>\r\n		</tr>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>100 mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203dpi, 8 puntos/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>100mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Interfaces</th>\r\n			<td>Serial (RS232), USB, Gaveta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Modo pagina</th>\r\n			<td>No soportado</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>20 KB Memoria RAM<br />\r\n			2 MB Memoria Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Chino GB18030 24x24 (Simplificado/Chino Tradicional)<br />\r\n			Alfanum&eacute;rico ASCII 9x17 - 12x24<br />\r\n			Soporta fuentes definidas por usuario<br />\r\n			42 tipos de c&oacute;digo (Code page)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barra</th>\r\n			<td>UPC-A UPC-E, EAN8, EAN13, CODE39, ITF, CODEBAR, CODE128, CODE93</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sensores</th>\r\n			<td>Sin sensor de detecci&oacute;n de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indicadores LED</th>\r\n			<td>Indicador de encendido LED Verde<br />\r\n			Indicador de papel Indicador de error LED Rojo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Energ&iacute;a</th>\r\n			<td>Adaptador de corriente externo<br />\r\n			Entrada 100 -240 Vca 50-60Hz<br />\r\n			Salida 24 Vcc +/- 5% 2 A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel</th>\r\n			<td>Papel T&eacute;rmico est&aacute;ndar 58mm de ancho<br />\r\n			Cubierta de apertura hacia arriba<br />\r\n			Sistema de carga f&aacute;cil<br />\r\n			Rasgado Manual</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Comandos</th>\r\n			<td>Emulaci&oacute;n ESC/POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Especificaciones f&iacute;sicas</th>\r\n			<td>Condiciones de operaci&oacute;n 0~40 &deg;C/20~85HR<br />\r\n			Condiciones de Almacenamiento -20~70 &deg;C/5~95HR</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>186(largo)x114(ancho)x128(alto)mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>930 g</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida TPH</th>\r\n			<td>150 km</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Controladores</th>\r\n			<td>Windows 8<br />\r\n			32bit: Windows(Win7/Vista/XP/2000)<br />\r\n			64bit: Windows(Win7/Vista/XP/2000)<br />\r\n			Linux/Unix<br />\r\n			OPOS</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 26),
(63, 'Caracteristicas HKA57 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n lineal t&eacute;rmica directa</td>\r\n		</tr>\r\n		<tr>\r\n		</tr>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>100 mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203dpi, 8 puntos/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>100mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Interfaces</th>\r\n			<td>Serial (RS232), USB, Gaveta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Modo pagina</th>\r\n			<td>No soportado</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>20 KB Memoria RAM<br />\r\n			2 MB Memoria Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Chino GB18030 24x24 (Simplificado/Chino Tradicional)<br />\r\n			Alfanum&eacute;rico ASCII 9x17 - 12x24<br />\r\n			Soporta fuentes definidas por usuario<br />\r\n			42 tipos de c&oacute;digo (Code page)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barra</th>\r\n			<td>UPC-A UPC-E, EAN8, EAN13, CODE39, ITF, CODEBAR, CODE128, CODE93</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sensores</th>\r\n			<td>Sin sensor de detecci&oacute;n de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indicadores LED</th>\r\n			<td>Indicador de encendido LED Verde<br />\r\n			Indicador de papel Indicador de error LED Rojo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Energ&iacute;a</th>\r\n			<td>Adaptador de corriente externo<br />\r\n			Entrada 100 -240 Vca 50-60Hz<br />\r\n			Salida 24 Vcc +/- 5% 2 A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel</th>\r\n			<td>Papel T&eacute;rmico est&aacute;ndar 58mm de ancho<br />\r\n			Cubierta de apertura hacia arriba<br />\r\n			Sistema de carga f&aacute;cil<br />\r\n			Rasgado Manual</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Comandos</th>\r\n			<td>Emulaci&oacute;n ESC/POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Especificaciones f&iacute;sicas</th>\r\n			<td>Condiciones de operaci&oacute;n 0~40 &deg;C/20~85HR<br />\r\n			Condiciones de Almacenamiento -20~70 &deg;C/5~95HR</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>186(largo)x114(ancho)x128(alto)mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>930 g</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida TPH</th>\r\n			<td>150 km</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Controladores</th>\r\n			<td>Windows 8<br />\r\n			32bit: Windows(Win7/Vista/XP/2000)<br />\r\n			64bit: Windows(Win7/Vista/XP/2000)<br />\r\n			Linux/Unix<br />\r\n			OPOS</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 26),
(64, 'Descripcion HKA80', '<p>Impresora t&eacute;rmica de punto de venta, ideal para puntos de venta con alto rendimiento</p>\r\n\r\n<p>Rapidez y versatilidad &uacute;nica: sus puertos Serial, USB, Ethernet o WiFi, unido a su puerto para gavetas de dinero y su cortador autom&aacute;tico de papel la hacen ideal para puntos de venta.</p>\r\n\r\n<p>Su velocidad y robustez la ubican desde el mostrador a la cocina o el almac&eacute;n y su amplio set de librer&iacute;as y herramientas garantizan una integraci&oacute;n r&aacute;pida y amigable con el software de su preferencia.</p>\r\n', 'product', 1, 'es', 'bo', 27),
(65, 'Descripcion HKA80', '<p>Impresora t&eacute;rmica de punto de venta, ideal para puntos de venta con alto rendimiento</p>\r\n\r\n<p>Rapidez y versatilidad &uacute;nica: sus puertos Serial, USB, Ethernet o WiFi, unido a su puerto para gavetas de dinero y su cortador autom&aacute;tico de papel la hacen ideal para puntos de venta.</p>\r\n\r\n<p>Su velocidad y robustez la ubican desde el mostrador a la cocina o el almac&eacute;n y su amplio set de librer&iacute;as y herramientas garantizan una integraci&oacute;n r&aacute;pida y amigable con el software de su preferencia.</p>\r\n', 'product', 1, 'es', 'co', 27),
(66, 'Descripcion HKA80', '<p>Impresora t&eacute;rmica de punto de venta, ideal para puntos de venta con alto rendimiento</p>\r\n\r\n<p>Rapidez y versatilidad &uacute;nica: sus puertos Serial, USB, Ethernet o WiFi, unido a su puerto para gavetas de dinero y su cortador autom&aacute;tico de papel la hacen ideal para puntos de venta.</p>\r\n\r\n<p>Su velocidad y robustez la ubican desde el mostrador a la cocina o el almac&eacute;n y su amplio set de librer&iacute;as y herramientas garantizan una integraci&oacute;n r&aacute;pida y amigable con el software de su preferencia.</p>\r\n', 'product', 1, 'es', 'mx', 27),
(67, 'Descripcion HKA80', '<p>Impresora t&eacute;rmica de punto de venta, ideal para puntos de venta con alto rendimiento</p>\r\n\r\n<p>Rapidez y versatilidad &uacute;nica: sus puertos Serial, USB, Ethernet o WiFi, unido a su puerto para gavetas de dinero y su cortador autom&aacute;tico de papel la hacen ideal para puntos de venta.</p>\r\n\r\n<p>Su velocidad y robustez la ubican desde el mostrador a la cocina o el almac&eacute;n y su amplio set de librer&iacute;as y herramientas garantizan una integraci&oacute;n r&aacute;pida y amigable con el software de su preferencia.</p>\r\n', 'product', 1, 'es', 'pe', 27),
(68, 'Caracteristicas HKA80', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n lineal t&eacute;rmica directa</td>\r\n		</tr>\r\n		<tr>\r\n		</tr>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>200~230 mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203dpi, 8 puntos/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>72mm/s (576 puntos)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Interfaces</th>\r\n			<td>WiFi, Serial (RS232), Paralelo, Ethernet, USB, Gaveta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>1 MB Memoria RAM<br />\r\n			4 MB Memoria Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Chino GB18030 24x24 (Simplificado/Chino Tradicional)<br />\r\n			Alfanum&eacute;rico ASCII 9x17 - 12x24<br />\r\n			Soporta fuentes definidas por usuario<br />\r\n			14 tipos de c&oacute;digo (Code page)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Gr&aacute;ficos</th>\r\n			<td>Soporta mapas de bits de diversas densidades. Tama&ntilde;o m&aacute;ximo de cada mapa de bit 64Kb. Tama&ntilde;o total del mapa de bit 256Kb.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barra</th>\r\n			<td>1D: UPC-A UPC-E, EAN8, EAN13, CODE39, ITF, CODEBAR, CODE128, CODE93<br />\r\n			2D: PDF417, QR Code</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sensores</th>\r\n			<td>Sensor de fin de papel y sensor de tapa abierta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indicadores LED</th>\r\n			<td>Indicador de encendido LED Verde<br />\r\n			Indicador de papel/Indicador de error LED Rojo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Energ&iacute;a</th>\r\n			<td>Adaptador de corriente externo<br />\r\n			Entrada 100-240 Vca 50-60Hz<br />\r\n			Salida 24 Vcc +/- 5% 2 A Interfaz A-1009-3P</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel</th>\r\n			<td>Papel T&eacute;rmico est&aacute;ndar 79,5mm(+/-0.5)/57.5(+/-0.5)mm de ancho<br />\r\n			Cubierta de apertura hacia arriba<br />\r\n			Sistema de carga f&aacute;cil<br />\r\n			Rasgado Manual o cortadora autom&aacute;tica<br />\r\n			Di&aacute;metro externo m&aacute;ximo 83mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Comandos</th>\r\n			<td>Emulaci&oacute;n ESC/POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ruido</th>\r\n			<td>Nivel de ruido menor a 50dbA a una velocidad de impresi&oacute;n de 230mm/s (seg&uacute;n regulaci&oacute;n ISO7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Especificaciones f&iacute;sicas</th>\r\n			<td>Condiciones de operaci&oacute;n 5~45 &deg;C/10~95%HR<br />\r\n			Condiciones de Almacenamiento -10~50 &deg;C/10~95%HR</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>195(largo) x 177(ancho) x 147(alto)mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>1,8 Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad</th>\r\n			<td>Tiempo de vida TPH: 150km<br />\r\n			Tiempo de vida del cortador: 2 000 000 cortes en promedio<br />\r\n			MTBF 360 000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Controladores</th>\r\n			<td>Windows 8<br />\r\n			32bit: Windows(Win7/Vista/XP/2000)<br />\r\n			64bit: Windows(Win7/Vista/XP/2000)<br />\r\n			Linux/Unix<br />\r\n			OPOS</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 27),
(69, 'Caracteristicas HKA80', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n lineal t&eacute;rmica directa</td>\r\n		</tr>\r\n		<tr>\r\n		</tr>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>200~230 mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203dpi, 8 puntos/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>72mm/s (576 puntos)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Interfaces</th>\r\n			<td>WiFi, Serial (RS232), Paralelo, Ethernet, USB, Gaveta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>1 MB Memoria RAM<br />\r\n			4 MB Memoria Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Chino GB18030 24x24 (Simplificado/Chino Tradicional)<br />\r\n			Alfanum&eacute;rico ASCII 9x17 - 12x24<br />\r\n			Soporta fuentes definidas por usuario<br />\r\n			14 tipos de c&oacute;digo (Code page)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Gr&aacute;ficos</th>\r\n			<td>Soporta mapas de bits de diversas densidades. Tama&ntilde;o m&aacute;ximo de cada mapa de bit 64Kb. Tama&ntilde;o total del mapa de bit 256Kb.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barra</th>\r\n			<td>1D: UPC-A UPC-E, EAN8, EAN13, CODE39, ITF, CODEBAR, CODE128, CODE93<br />\r\n			2D: PDF417, QR Code</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sensores</th>\r\n			<td>Sensor de fin de papel y sensor de tapa abierta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indicadores LED</th>\r\n			<td>Indicador de encendido LED Verde<br />\r\n			Indicador de papel/Indicador de error LED Rojo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Energ&iacute;a</th>\r\n			<td>Adaptador de corriente externo<br />\r\n			Entrada 100-240 Vca 50-60Hz<br />\r\n			Salida 24 Vcc +/- 5% 2 A Interfaz A-1009-3P</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel</th>\r\n			<td>Papel T&eacute;rmico est&aacute;ndar 79,5mm(+/-0.5)/57.5(+/-0.5)mm de ancho<br />\r\n			Cubierta de apertura hacia arriba<br />\r\n			Sistema de carga f&aacute;cil<br />\r\n			Rasgado Manual o cortadora autom&aacute;tica<br />\r\n			Di&aacute;metro externo m&aacute;ximo 83mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Comandos</th>\r\n			<td>Emulaci&oacute;n ESC/POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ruido</th>\r\n			<td>Nivel de ruido menor a 50dbA a una velocidad de impresi&oacute;n de 230mm/s (seg&uacute;n regulaci&oacute;n ISO7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Especificaciones f&iacute;sicas</th>\r\n			<td>Condiciones de operaci&oacute;n 5~45 &deg;C/10~95%HR<br />\r\n			Condiciones de Almacenamiento -10~50 &deg;C/10~95%HR</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>195(largo) x 177(ancho) x 147(alto)mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>1,8 Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad</th>\r\n			<td>Tiempo de vida TPH: 150km<br />\r\n			Tiempo de vida del cortador: 2 000 000 cortes en promedio<br />\r\n			MTBF 360 000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Controladores</th>\r\n			<td>Windows 8<br />\r\n			32bit: Windows(Win7/Vista/XP/2000)<br />\r\n			64bit: Windows(Win7/Vista/XP/2000)<br />\r\n			Linux/Unix<br />\r\n			OPOS</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 27),
(70, 'Caracteristicas HKA80', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n lineal t&eacute;rmica directa</td>\r\n		</tr>\r\n		<tr>\r\n		</tr>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>200~230 mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203dpi, 8 puntos/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>72mm/s (576 puntos)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Interfaces</th>\r\n			<td>WiFi, Serial (RS232), Paralelo, Ethernet, USB, Gaveta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>1 MB Memoria RAM<br />\r\n			4 MB Memoria Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Chino GB18030 24x24 (Simplificado/Chino Tradicional)<br />\r\n			Alfanum&eacute;rico ASCII 9x17 - 12x24<br />\r\n			Soporta fuentes definidas por usuario<br />\r\n			14 tipos de c&oacute;digo (Code page)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Gr&aacute;ficos</th>\r\n			<td>Soporta mapas de bits de diversas densidades. Tama&ntilde;o m&aacute;ximo de cada mapa de bit 64Kb. Tama&ntilde;o total del mapa de bit 256Kb.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barra</th>\r\n			<td>1D: UPC-A UPC-E, EAN8, EAN13, CODE39, ITF, CODEBAR, CODE128, CODE93<br />\r\n			2D: PDF417, QR Code</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sensores</th>\r\n			<td>Sensor de fin de papel y sensor de tapa abierta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indicadores LED</th>\r\n			<td>Indicador de encendido LED Verde<br />\r\n			Indicador de papel/Indicador de error LED Rojo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Energ&iacute;a</th>\r\n			<td>Adaptador de corriente externo<br />\r\n			Entrada 100-240 Vca 50-60Hz<br />\r\n			Salida 24 Vcc +/- 5% 2 A Interfaz A-1009-3P</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel</th>\r\n			<td>Papel T&eacute;rmico est&aacute;ndar 79,5mm(+/-0.5)/57.5(+/-0.5)mm de ancho<br />\r\n			Cubierta de apertura hacia arriba<br />\r\n			Sistema de carga f&aacute;cil<br />\r\n			Rasgado Manual o cortadora autom&aacute;tica<br />\r\n			Di&aacute;metro externo m&aacute;ximo 83mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Comandos</th>\r\n			<td>Emulaci&oacute;n ESC/POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ruido</th>\r\n			<td>Nivel de ruido menor a 50dbA a una velocidad de impresi&oacute;n de 230mm/s (seg&uacute;n regulaci&oacute;n ISO7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Especificaciones f&iacute;sicas</th>\r\n			<td>Condiciones de operaci&oacute;n 5~45 &deg;C/10~95%HR<br />\r\n			Condiciones de Almacenamiento -10~50 &deg;C/10~95%HR</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>195(largo) x 177(ancho) x 147(alto)mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>1,8 Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad</th>\r\n			<td>Tiempo de vida TPH: 150km<br />\r\n			Tiempo de vida del cortador: 2 000 000 cortes en promedio<br />\r\n			MTBF 360 000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Controladores</th>\r\n			<td>Windows 8<br />\r\n			32bit: Windows(Win7/Vista/XP/2000)<br />\r\n			64bit: Windows(Win7/Vista/XP/2000)<br />\r\n			Linux/Unix<br />\r\n			OPOS</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 27),
(71, 'Caracteristicas HKA80', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n lineal t&eacute;rmica directa</td>\r\n		</tr>\r\n		<tr>\r\n		</tr>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>200~230 mm/s</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203dpi, 8 puntos/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>72mm/s (576 puntos)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Interfaces</th>\r\n			<td>WiFi, Serial (RS232), Paralelo, Ethernet, USB, Gaveta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>1 MB Memoria RAM<br />\r\n			4 MB Memoria Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Chino GB18030 24x24 (Simplificado/Chino Tradicional)<br />\r\n			Alfanum&eacute;rico ASCII 9x17 - 12x24<br />\r\n			Soporta fuentes definidas por usuario<br />\r\n			14 tipos de c&oacute;digo (Code page)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Gr&aacute;ficos</th>\r\n			<td>Soporta mapas de bits de diversas densidades. Tama&ntilde;o m&aacute;ximo de cada mapa de bit 64Kb. Tama&ntilde;o total del mapa de bit 256Kb.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barra</th>\r\n			<td>1D: UPC-A UPC-E, EAN8, EAN13, CODE39, ITF, CODEBAR, CODE128, CODE93<br />\r\n			2D: PDF417, QR Code</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sensores</th>\r\n			<td>Sensor de fin de papel y sensor de tapa abierta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indicadores LED</th>\r\n			<td>Indicador de encendido LED Verde<br />\r\n			Indicador de papel/Indicador de error LED Rojo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Energ&iacute;a</th>\r\n			<td>Adaptador de corriente externo<br />\r\n			Entrada 100-240 Vca 50-60Hz<br />\r\n			Salida 24 Vcc +/- 5% 2 A Interfaz A-1009-3P</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel</th>\r\n			<td>Papel T&eacute;rmico est&aacute;ndar 79,5mm(+/-0.5)/57.5(+/-0.5)mm de ancho<br />\r\n			Cubierta de apertura hacia arriba<br />\r\n			Sistema de carga f&aacute;cil<br />\r\n			Rasgado Manual o cortadora autom&aacute;tica<br />\r\n			Di&aacute;metro externo m&aacute;ximo 83mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Comandos</th>\r\n			<td>Emulaci&oacute;n ESC/POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ruido</th>\r\n			<td>Nivel de ruido menor a 50dbA a una velocidad de impresi&oacute;n de 230mm/s (seg&uacute;n regulaci&oacute;n ISO7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Especificaciones f&iacute;sicas</th>\r\n			<td>Condiciones de operaci&oacute;n 5~45 &deg;C/10~95%HR<br />\r\n			Condiciones de Almacenamiento -10~50 &deg;C/10~95%HR</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>195(largo) x 177(ancho) x 147(alto)mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>1,8 Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad</th>\r\n			<td>Tiempo de vida TPH: 150km<br />\r\n			Tiempo de vida del cortador: 2 000 000 cortes en promedio<br />\r\n			MTBF 360 000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Controladores</th>\r\n			<td>Windows 8<br />\r\n			32bit: Windows(Win7/Vista/XP/2000)<br />\r\n			64bit: Windows(Win7/Vista/XP/2000)<br />\r\n			Linux/Unix<br />\r\n			OPOS</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 27),
(72, 'Descripcion  Tally  1125', 'La Tally Dascom \r\n1125 es una impresora serial matriz de punto de 24 pines y 80 columnas, \r\ncon interfaces USB, serial y paralela. Con un tamaño compacto, la 1125 \r\nes perfecta para entornos industriales y aplicaciones de oficina donde \r\nel espacio es limitado.\r\n\r\nCon bajos costos de funcionamiento, la impresión de alta calidad y un\r\n panel de control fácil de utilizar es ideal para pequeñas empresas, \r\nlistados bajo volumen e informes, así­ como notas de despacho y \r\netiquetas.\r\n\r\nAdemás de estas aplicaciones, la 1125 es también recomendada para sistemas de medición y control.\r\n\r\nSe encuentra entre las impresoras matriciales de precio más bajo \r\ndisponible en la actualidad. La 1125 también ofrece una cinta de larga \r\nduración dando a esta&nbsp;impresora gastos de funcionamiento muy bajos.\r\n\r\nPara la impresión de copias es considerablemente menos costoso&nbsp;y hace\r\n que sea una alternativa viable de&nbsp;impresoras de inyección de tinta de \r\nbajo costo.<br>', 'product', 1, 'es', 'bo', 28),
(73, 'Descripcion  Tally  1125', 'La Tally Dascom \r\n1125 es una impresora serial matriz de punto de 24 pines y 80 columnas, \r\ncon interfaces USB, serial y paralela. Con un tamaño compacto, la 1125 \r\nes perfecta para entornos industriales y aplicaciones de oficina donde \r\nel espacio es limitado.\r\n\r\nCon bajos costos de funcionamiento, la impresión de alta calidad y un\r\n panel de control fácil de utilizar es ideal para pequeñas empresas, \r\nlistados bajo volumen e informes, así­ como notas de despacho y \r\netiquetas.\r\n\r\nAdemás de estas aplicaciones, la 1125 es también recomendada para sistemas de medición y control.\r\n\r\nSe encuentra entre las impresoras matriciales de precio más bajo \r\ndisponible en la actualidad. La 1125 también ofrece una cinta de larga \r\nduración dando a esta&nbsp;impresora gastos de funcionamiento muy bajos.\r\n\r\nPara la impresión de copias es considerablemente menos costoso&nbsp;y hace\r\n que sea una alternativa viable de&nbsp;impresoras de inyección de tinta de \r\nbajo costo.<br>', 'product', 1, 'es', 'co', 28),
(74, 'Descripcion  Tally  1125', 'La Tally Dascom \r\n1125 es una impresora serial matriz de punto de 24 pines y 80 columnas, \r\ncon interfaces USB, serial y paralela. Con un tamaño compacto, la 1125 \r\nes perfecta para entornos industriales y aplicaciones de oficina donde \r\nel espacio es limitado.\r\n\r\nCon bajos costos de funcionamiento, la impresión de alta calidad y un\r\n panel de control fácil de utilizar es ideal para pequeñas empresas, \r\nlistados bajo volumen e informes, así­ como notas de despacho y \r\netiquetas.\r\n\r\nAdemás de estas aplicaciones, la 1125 es también recomendada para sistemas de medición y control.\r\n\r\nSe encuentra entre las impresoras matriciales de precio más bajo \r\ndisponible en la actualidad. La 1125 también ofrece una cinta de larga \r\nduración dando a esta&nbsp;impresora gastos de funcionamiento muy bajos.\r\n\r\nPara la impresión de copias es considerablemente menos costoso&nbsp;y hace\r\n que sea una alternativa viable de&nbsp;impresoras de inyección de tinta de \r\nbajo costo.<br>', 'product', 1, 'es', 'mx', 28),
(75, 'Descripcion  Tally  1125', 'La Tally Dascom \r\n1125 es una impresora serial matriz de punto de 24 pines y 80 columnas, \r\ncon interfaces USB, serial y paralela. Con un tamaño compacto, la 1125 \r\nes perfecta para entornos industriales y aplicaciones de oficina donde \r\nel espacio es limitado.\r\n\r\nCon bajos costos de funcionamiento, la impresión de alta calidad y un\r\n panel de control fácil de utilizar es ideal para pequeñas empresas, \r\nlistados bajo volumen e informes, así­ como notas de despacho y \r\netiquetas.\r\n\r\nAdemás de estas aplicaciones, la 1125 es también recomendada para sistemas de medición y control.\r\n\r\nSe encuentra entre las impresoras matriciales de precio más bajo \r\ndisponible en la actualidad. La 1125 también ofrece una cinta de larga \r\nduración dando a esta&nbsp;impresora gastos de funcionamiento muy bajos.\r\n\r\nPara la impresión de copias es considerablemente menos costoso&nbsp;y hace\r\n que sea una alternativa viable de&nbsp;impresoras de inyección de tinta de \r\nbajo costo.<br>', 'product', 1, 'es', 'pe', 28),
(76, 'Caracteristicas Tally 1125', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n<br />\r\n			&nbsp;</th>\r\n			<td>10 cpi: 375cps (High Speed Draft),&nbsp; 83cps (LQ) 12cps: 300<br />\r\n			20 cpi: 500cps (High Speed Draft), 166cps (LQ)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Matriz de punto serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>295 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20cpi, Espaciado Proporcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de L&iacute;nea</th>\r\n			<td>1, 2, 3, 4, 6, 8 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal</th>\r\n			<td>400 Millones de impactos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria / B&uacute;fer</th>\r\n			<td>68 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>USB 2.0, paralelo (Bi-direccional) y Serial RS232</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>Epson LQ (ESC P/2), IBM ProPrinter (2390 Plus)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, OCR-A/B*, 10 C&oacute;digos de Barra</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo del papel est&aacute;ndar</th>\r\n			<td>Tractor de fricci&oacute;n y bandeja de hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hoja sencilla y continua</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original +4 (5- formas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>52-100 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del Papel</th>\r\n			<td>76,2mm a 256,5mm ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de Impresora</th>\r\n			<td>Aproximadamente 5Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o Impresora</th>\r\n			<td>(Alto x Ancho x Profundidad): 157,5mm x&nbsp;358mm x 284,5mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Modelos disponibles: 120V o 220V (180 V- 264 V) / 50 a 60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>28,8W en operaci&oacute;n / 3.8W en Reposo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>15.000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000/XP/Vista/7 y Server 2003</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C to +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% to 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt; 51dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 A&ntilde;o en Centro de Servicio.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 'bo', 28),
(77, 'Caracteristicas Tally 1125', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n<br />\r\n			&nbsp;</th>\r\n			<td>10 cpi: 375cps (High Speed Draft),&nbsp; 83cps (LQ) 12cps: 300<br />\r\n			20 cpi: 500cps (High Speed Draft), 166cps (LQ)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Matriz de punto serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>295 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20cpi, Espaciado Proporcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de L&iacute;nea</th>\r\n			<td>1, 2, 3, 4, 6, 8 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal</th>\r\n			<td>400 Millones de impactos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria / B&uacute;fer</th>\r\n			<td>68 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>USB 2.0, paralelo (Bi-direccional) y Serial RS232</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>Epson LQ (ESC P/2), IBM ProPrinter (2390 Plus)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, OCR-A/B*, 10 C&oacute;digos de Barra</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo del papel est&aacute;ndar</th>\r\n			<td>Tractor de fricci&oacute;n y bandeja de hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hoja sencilla y continua</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original +4 (5- formas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>52-100 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del Papel</th>\r\n			<td>76,2mm a 256,5mm ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de Impresora</th>\r\n			<td>Aproximadamente 5Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o Impresora</th>\r\n			<td>(Alto x Ancho x Profundidad): 157,5mm x&nbsp;358mm x 284,5mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Modelos disponibles: 120V o 220V (180 V- 264 V) / 50 a 60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>28,8W en operaci&oacute;n / 3.8W en Reposo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>15.000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000/XP/Vista/7 y Server 2003</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C to +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% to 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt; 51dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 A&ntilde;o en Centro de Servicio.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 'co', 28),
(78, 'Caracteristicas Tally 1125', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n<br />\r\n			&nbsp;</th>\r\n			<td>10 cpi: 375cps (High Speed Draft),&nbsp; 83cps (LQ) 12cps: 300<br />\r\n			20 cpi: 500cps (High Speed Draft), 166cps (LQ)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Matriz de punto serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>295 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20cpi, Espaciado Proporcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de L&iacute;nea</th>\r\n			<td>1, 2, 3, 4, 6, 8 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal</th>\r\n			<td>400 Millones de impactos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria / B&uacute;fer</th>\r\n			<td>68 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>USB 2.0, paralelo (Bi-direccional) y Serial RS232</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>Epson LQ (ESC P/2), IBM ProPrinter (2390 Plus)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, OCR-A/B*, 10 C&oacute;digos de Barra</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo del papel est&aacute;ndar</th>\r\n			<td>Tractor de fricci&oacute;n y bandeja de hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hoja sencilla y continua</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original +4 (5- formas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>52-100 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del Papel</th>\r\n			<td>76,2mm a 256,5mm ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de Impresora</th>\r\n			<td>Aproximadamente 5Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o Impresora</th>\r\n			<td>(Alto x Ancho x Profundidad): 157,5mm x&nbsp;358mm x 284,5mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Modelos disponibles: 120V o 220V (180 V- 264 V) / 50 a 60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>28,8W en operaci&oacute;n / 3.8W en Reposo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>15.000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000/XP/Vista/7 y Server 2003</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C to +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% to 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt; 51dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 A&ntilde;o en Centro de Servicio.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 'mx', 28),
(79, 'Caracteristicas Tally 1125', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n<br />\r\n			&nbsp;</th>\r\n			<td>10 cpi: 375cps (High Speed Draft),&nbsp; 83cps (LQ) 12cps: 300<br />\r\n			20 cpi: 500cps (High Speed Draft), 166cps (LQ)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Matriz de punto serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>295 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20cpi, Espaciado Proporcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de L&iacute;nea</th>\r\n			<td>1, 2, 3, 4, 6, 8 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal</th>\r\n			<td>400 Millones de impactos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria / B&uacute;fer</th>\r\n			<td>68 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>USB 2.0, paralelo (Bi-direccional) y Serial RS232</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>Epson LQ (ESC P/2), IBM ProPrinter (2390 Plus)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, OCR-A/B*, 10 C&oacute;digos de Barra</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo del papel est&aacute;ndar</th>\r\n			<td>Tractor de fricci&oacute;n y bandeja de hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hoja sencilla y continua</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original +4 (5- formas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>52-100 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del Papel</th>\r\n			<td>76,2mm a 256,5mm ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de Impresora</th>\r\n			<td>Aproximadamente 5Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o Impresora</th>\r\n			<td>(Alto x Ancho x Profundidad): 157,5mm x&nbsp;358mm x 284,5mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Modelos disponibles: 120V o 220V (180 V- 264 V) / 50 a 60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>28,8W en operaci&oacute;n / 3.8W en Reposo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>15.000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000/XP/Vista/7 y Server 2003</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C to +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% to 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt; 51dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 A&ntilde;o en Centro de Servicio.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 'pe', 28),
(80, 'Descripcion Tally 1500 MP', '<p>La impresora matriz serial de gama media DASCOM 1500 est&aacute; dise&ntilde;ada para aplicaciones donde el rendimiento y el manejo flexible del papel son vitales.</p>\r\n\r\n<p><br />\r\nPapel f&aacute;cil de cargar, ya sea en el frente o la parte trasera de la impresora. Para un control preciso de papel y permitir la alimentaci&oacute;n de papel continuo, el tractor de empuje tambi&eacute;n puede ser utilizado como un tractor de arrastre.<br />\r\n<br />\r\nLa impresora 1500 cuenta con un cabezal de impresi&oacute;n de 24 hilos, para proporcionar la mejor calidad de impresi&oacute;n para aplicaciones de oficina adecuados para la impresi&oacute;n de formato ancho estrecho.Todas las impresoras son compatibles con gr&aacute;ficos y c&oacute;digos de barras populares.</p>\r\n\r\n<p><br />\r\nCon la conmutaci&oacute;n autom&aacute;tica de interfaces USB y paralelo de serie y una interfaz opcional Ethernet o serial pronto disponible,estas nuevas impresoras de matriz de puntos ofrece una soluci&oacute;n rentable.<br />\r\n<br />\r\nCon altas velocidades de impresi&oacute;n y la capacidad de manejar diferentes tipos de medios de impresi&oacute;n, incluyendo etiquetas, facturas, informes y formularios de m&uacute;ltiples partes, que son muy adecuadas para aplicaciones de oficina,incluyendo venta al por menor, el gobierno local, la sanidad, el almacenamiento y la distribuci&oacute;n.</p>\r\n', 'product', 1, 'es', 'bo', 29),
(81, 'Descripcion Tally 1500 MP', '<p>La impresora matriz serial de gama media DASCOM 1500 est&aacute; dise&ntilde;ada para aplicaciones donde el rendimiento y el manejo flexible del papel son vitales.</p>\r\n\r\n<p><br />\r\nPapel f&aacute;cil de cargar, ya sea en el frente o la parte trasera de la impresora. Para un control preciso de papel y permitir la alimentaci&oacute;n de papel continuo, el tractor de empuje tambi&eacute;n puede ser utilizado como un tractor de arrastre.<br />\r\n<br />\r\nLa impresora 1500 cuenta con un cabezal de impresi&oacute;n de 24 hilos, para proporcionar la mejor calidad de impresi&oacute;n para aplicaciones de oficina adecuados para la impresi&oacute;n de formato ancho estrecho.Todas las impresoras son compatibles con gr&aacute;ficos y c&oacute;digos de barras populares.</p>\r\n\r\n<p><br />\r\nCon la conmutaci&oacute;n autom&aacute;tica de interfaces USB y paralelo de serie y una interfaz opcional Ethernet o serial pronto disponible,estas nuevas impresoras de matriz de puntos ofrece una soluci&oacute;n rentable.<br />\r\n<br />\r\nCon altas velocidades de impresi&oacute;n y la capacidad de manejar diferentes tipos de medios de impresi&oacute;n, incluyendo etiquetas, facturas, informes y formularios de m&uacute;ltiples partes, que son muy adecuadas para aplicaciones de oficina,incluyendo venta al por menor, el gobierno local, la sanidad, el almacenamiento y la distribuci&oacute;n.</p>\r\n', 'product', 1, 'es', 'co', 29),
(82, 'Descripcion Tally 1500 MP', '<p>La impresora matriz serial de gama media DASCOM 1500 est&aacute; dise&ntilde;ada para aplicaciones donde el rendimiento y el manejo flexible del papel son vitales.</p>\r\n\r\n<p><br />\r\nPapel f&aacute;cil de cargar, ya sea en el frente o la parte trasera de la impresora. Para un control preciso de papel y permitir la alimentaci&oacute;n de papel continuo, el tractor de empuje tambi&eacute;n puede ser utilizado como un tractor de arrastre.<br />\r\n<br />\r\nLa impresora 1500 cuenta con un cabezal de impresi&oacute;n de 24 hilos, para proporcionar la mejor calidad de impresi&oacute;n para aplicaciones de oficina adecuados para la impresi&oacute;n de formato ancho estrecho.Todas las impresoras son compatibles con gr&aacute;ficos y c&oacute;digos de barras populares.</p>\r\n\r\n<p><br />\r\nCon la conmutaci&oacute;n autom&aacute;tica de interfaces USB y paralelo de serie y una interfaz opcional Ethernet o serial pronto disponible,estas nuevas impresoras de matriz de puntos ofrece una soluci&oacute;n rentable.<br />\r\n<br />\r\nCon altas velocidades de impresi&oacute;n y la capacidad de manejar diferentes tipos de medios de impresi&oacute;n, incluyendo etiquetas, facturas, informes y formularios de m&uacute;ltiples partes, que son muy adecuadas para aplicaciones de oficina,incluyendo venta al por menor, el gobierno local, la sanidad, el almacenamiento y la distribuci&oacute;n.</p>\r\n', 'product', 1, 'es', 'mx', 29),
(83, 'Descripcion Tally 1500 MP', '<p>La impresora matriz serial de gama media DASCOM 1500 est&aacute; dise&ntilde;ada para aplicaciones donde el rendimiento y el manejo flexible del papel son vitales.</p>\r\n\r\n<p><br />\r\nPapel f&aacute;cil de cargar, ya sea en el frente o la parte trasera de la impresora. Para un control preciso de papel y permitir la alimentaci&oacute;n de papel continuo, el tractor de empuje tambi&eacute;n puede ser utilizado como un tractor de arrastre.<br />\r\n<br />\r\nLa impresora 1500 cuenta con un cabezal de impresi&oacute;n de 24 hilos, para proporcionar la mejor calidad de impresi&oacute;n para aplicaciones de oficina adecuados para la impresi&oacute;n de formato ancho estrecho.Todas las impresoras son compatibles con gr&aacute;ficos y c&oacute;digos de barras populares.</p>\r\n\r\n<p><br />\r\nCon la conmutaci&oacute;n autom&aacute;tica de interfaces USB y paralelo de serie y una interfaz opcional Ethernet o serial pronto disponible,estas nuevas impresoras de matriz de puntos ofrece una soluci&oacute;n rentable.<br />\r\n<br />\r\nCon altas velocidades de impresi&oacute;n y la capacidad de manejar diferentes tipos de medios de impresi&oacute;n, incluyendo etiquetas, facturas, informes y formularios de m&uacute;ltiples partes, que son muy adecuadas para aplicaciones de oficina,incluyendo venta al por menor, el gobierno local, la sanidad, el almacenamiento y la distribuci&oacute;n.</p>\r\n', 'product', 1, 'es', 'pe', 29);
INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(84, 'Caracteristicas Tally 1500 MP', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>24 pines: A 10, 12 cpi; &nbsp;Borrador: 400, 500 cps; LQ: 133, 160 cps</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Serial de Impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>400 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracateres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de linea</th>\r\n			<td>1, 2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>400 millones de golpes por pin</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (Bi-direccional), USB 2.0</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>IBM&reg; Proprinter XL, Epson&reg; ESC/P, Oki&reg;&nbsp;Superset Commands</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Quadrato, OCR-B, OCR-A<br />\r\n			10 c&oacute;digos de barra</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Tractor de empuje y arrastre, fricci&oacute;n para hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>1 + 5 copias</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Espesor Papel</th>\r\n			<td>Max. 0.35mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Papel</th>\r\n			<td>Papel Continuo y hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>3 - 10&quot; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>6,4 Kg.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(Ancho x Largo x Alto): &nbsp;398.7 X 330,2 X 134.6 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Modelos de 120V/220V</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>51 Watts imprimiendo, m&aacute;x. 7 Watts en espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>hasta 20,000 p&aacute;ginas por mes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista, Windows 7</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% relativa (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>Menos de 52dB(A) modo silencioso</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Garant&iacute;a\r\n			<p>Nota: Producto disponible s&oacute;lo para Latino America - Anunciado pronto para Canad&aacute; y USA</p>\r\n			</td>\r\n			<td>Un a&ntilde;o de retorno al almac&eacute;n</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 29),
(85, 'Caracteristicas Tally 1500 MP', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>24 pines: A 10, 12 cpi; &nbsp;Borrador: 400, 500 cps; LQ: 133, 160 cps</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Serial de Impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>400 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracateres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de linea</th>\r\n			<td>1, 2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>400 millones de golpes por pin</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (Bi-direccional), USB 2.0</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>IBM&reg; Proprinter XL, Epson&reg; ESC/P, Oki&reg;&nbsp;Superset Commands</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Quadrato, OCR-B, OCR-A<br />\r\n			10 c&oacute;digos de barra</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Tractor de empuje y arrastre, fricci&oacute;n para hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>1 + 5 copias</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Espesor Papel</th>\r\n			<td>Max. 0.35mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Papel</th>\r\n			<td>Papel Continuo y hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>3 - 10&quot; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>6,4 Kg.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(Ancho x Largo x Alto): &nbsp;398.7 X 330,2 X 134.6 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Modelos de 120V/220V</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>51 Watts imprimiendo, m&aacute;x. 7 Watts en espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>hasta 20,000 p&aacute;ginas por mes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista, Windows 7</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% relativa (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>Menos de 52dB(A) modo silencioso</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Garant&iacute;a\r\n			<p>Nota: Producto disponible s&oacute;lo para Latino America - Anunciado pronto para Canad&aacute; y USA</p>\r\n			</td>\r\n			<td>Un a&ntilde;o de retorno al almac&eacute;n</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 29),
(86, 'Caracteristicas Tally 1500 MP', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>24 pines: A 10, 12 cpi; &nbsp;Borrador: 400, 500 cps; LQ: 133, 160 cps</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Serial de Impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>400 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracateres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de linea</th>\r\n			<td>1, 2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>400 millones de golpes por pin</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (Bi-direccional), USB 2.0</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>IBM&reg; Proprinter XL, Epson&reg; ESC/P, Oki&reg;&nbsp;Superset Commands</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Quadrato, OCR-B, OCR-A<br />\r\n			10 c&oacute;digos de barra</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Tractor de empuje y arrastre, fricci&oacute;n para hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>1 + 5 copias</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Espesor Papel</th>\r\n			<td>Max. 0.35mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Papel</th>\r\n			<td>Papel Continuo y hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>3 - 10&quot; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>6,4 Kg.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(Ancho x Largo x Alto): &nbsp;398.7 X 330,2 X 134.6 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Modelos de 120V/220V</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>51 Watts imprimiendo, m&aacute;x. 7 Watts en espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>hasta 20,000 p&aacute;ginas por mes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista, Windows 7</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% relativa (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>Menos de 52dB(A) modo silencioso</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Garant&iacute;a\r\n			<p>Nota: Producto disponible s&oacute;lo para Latino America - Anunciado pronto para Canad&aacute; y USA</p>\r\n			</td>\r\n			<td>Un a&ntilde;o de retorno al almac&eacute;n</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 29),
(87, 'Caracteristicas Tally 1500 MP', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>24 pines: A 10, 12 cpi; &nbsp;Borrador: 400, 500 cps; LQ: 133, 160 cps</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Serial de Impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>400 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracateres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de linea</th>\r\n			<td>1, 2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>400 millones de golpes por pin</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (Bi-direccional), USB 2.0</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>IBM&reg; Proprinter XL, Epson&reg; ESC/P, Oki&reg;&nbsp;Superset Commands</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Quadrato, OCR-B, OCR-A<br />\r\n			10 c&oacute;digos de barra</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Tractor de empuje y arrastre, fricci&oacute;n para hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>1 + 5 copias</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Espesor Papel</th>\r\n			<td>Max. 0.35mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Papel</th>\r\n			<td>Papel Continuo y hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>3 - 10&quot; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>6,4 Kg.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(Ancho x Largo x Alto): &nbsp;398.7 X 330,2 X 134.6 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Modelos de 120V/220V</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>51 Watts imprimiendo, m&aacute;x. 7 Watts en espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>hasta 20,000 p&aacute;ginas por mes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista, Windows 7</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% relativa (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>Menos de 52dB(A) modo silencioso</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Garant&iacute;a\r\n			<p>Nota: Producto disponible s&oacute;lo para Latino America - Anunciado pronto para Canad&aacute; y USA</p>\r\n			</td>\r\n			<td>Un a&ntilde;o de retorno al almac&eacute;n</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 29),
(88, 'Descripcion Tally 2610', '<p>La impresora Tally Dascom 2610 es una m&aacute;quina confiable y con estilo&nbsp;muy destacada. El rendimiento y carga de trabajo, ergonom&iacute;a, facilidad de mantenimiento, excelente precio -&nbsp;todo ha sido incorporado para asegurarse de que es la &uacute;nica opci&oacute;n aceptable para el cliente.&nbsp;<br />\r\n&nbsp;<br />\r\nFormularios continuos y hojas sueltas son alimentados en la parte frontal para una carga r&aacute;pida y f&aacute;cil. Esto asegura una trayectoria recta para las formas dando como resultado&nbsp;el transporte del papel confiable, incluso en formularios de&nbsp;7 partes. La carga de papel es m&aacute;s f&aacute;cil. El operador no tiene que buscar a tientas por encima o alrededor de la impresora para obtener acceso a los tractores situados en la parte posterior.&nbsp;<br />\r\n&nbsp;<br />\r\nLa intervenci&oacute;n del usuario se reduce al m&iacute;nimo con una serie de funcionalidades dise&ntilde;adas para asegurar que los productos funcionan con rendimiento y confiabilidad &oacute;ptimos.</p>\r\n', 'product', 1, 'es', 'bo', 30),
(89, 'Descripcion Tally 2610', '<p>La impresora Tally Dascom 2610 es una m&aacute;quina confiable y con estilo&nbsp;muy destacada. El rendimiento y carga de trabajo, ergonom&iacute;a, facilidad de mantenimiento, excelente precio -&nbsp;todo ha sido incorporado para asegurarse de que es la &uacute;nica opci&oacute;n aceptable para el cliente.&nbsp;<br />\r\n&nbsp;<br />\r\nFormularios continuos y hojas sueltas son alimentados en la parte frontal para una carga r&aacute;pida y f&aacute;cil. Esto asegura una trayectoria recta para las formas dando como resultado&nbsp;el transporte del papel confiable, incluso en formularios de&nbsp;7 partes. La carga de papel es m&aacute;s f&aacute;cil. El operador no tiene que buscar a tientas por encima o alrededor de la impresora para obtener acceso a los tractores situados en la parte posterior.&nbsp;<br />\r\n&nbsp;<br />\r\nLa intervenci&oacute;n del usuario se reduce al m&iacute;nimo con una serie de funcionalidades dise&ntilde;adas para asegurar que los productos funcionan con rendimiento y confiabilidad &oacute;ptimos.</p>\r\n', 'product', 1, 'es', 'co', 30),
(90, 'Descripcion Tally 2610', '<p>La impresora Tally Dascom 2610 es una m&aacute;quina confiable y con estilo&nbsp;muy destacada. El rendimiento y carga de trabajo, ergonom&iacute;a, facilidad de mantenimiento, excelente precio -&nbsp;todo ha sido incorporado para asegurarse de que es la &uacute;nica opci&oacute;n aceptable para el cliente.&nbsp;<br />\r\n&nbsp;<br />\r\nFormularios continuos y hojas sueltas son alimentados en la parte frontal para una carga r&aacute;pida y f&aacute;cil. Esto asegura una trayectoria recta para las formas dando como resultado&nbsp;el transporte del papel confiable, incluso en formularios de&nbsp;7 partes. La carga de papel es m&aacute;s f&aacute;cil. El operador no tiene que buscar a tientas por encima o alrededor de la impresora para obtener acceso a los tractores situados en la parte posterior.&nbsp;<br />\r\n&nbsp;<br />\r\nLa intervenci&oacute;n del usuario se reduce al m&iacute;nimo con una serie de funcionalidades dise&ntilde;adas para asegurar que los productos funcionan con rendimiento y confiabilidad &oacute;ptimos.</p>\r\n', 'product', 1, 'es', 'mx', 30),
(91, 'Descripcion Tally 2610', '<p>La impresora Tally Dascom 2610 es una m&aacute;quina confiable y con estilo&nbsp;muy destacada. El rendimiento y carga de trabajo, ergonom&iacute;a, facilidad de mantenimiento, excelente precio -&nbsp;todo ha sido incorporado para asegurarse de que es la &uacute;nica opci&oacute;n aceptable para el cliente.&nbsp;<br />\r\n&nbsp;<br />\r\nFormularios continuos y hojas sueltas son alimentados en la parte frontal para una carga r&aacute;pida y f&aacute;cil. Esto asegura una trayectoria recta para las formas dando como resultado&nbsp;el transporte del papel confiable, incluso en formularios de&nbsp;7 partes. La carga de papel es m&aacute;s f&aacute;cil. El operador no tiene que buscar a tientas por encima o alrededor de la impresora para obtener acceso a los tractores situados en la parte posterior.&nbsp;<br />\r\n&nbsp;<br />\r\nLa intervenci&oacute;n del usuario se reduce al m&iacute;nimo con una serie de funcionalidades dise&ntilde;adas para asegurar que los productos funcionan con rendimiento y confiabilidad &oacute;ptimos.</p>\r\n', 'product', 1, 'es', 'pe', 30),
(92, 'Caracteristicas Tally 2610', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>680 cps, 570 cps (Impresi&oacute;n m&aacute;s r&aacute;pida); 500 cps (Borrador Alta Velocidad), 333 cps (Borrador), 111 cps (LQ) a 10 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Matriz de punto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Columnas</th>\r\n			<td>137</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>442 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de l&iacute;nea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>500 millones de impactos por aguja</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (bidireccional), USB 2.0 y Ethernet (10/100 Base T) est&aacute;ndar; RS-232 opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n est&aacute;ndar</th>\r\n			<td>IBM Proprinter XL24, Epson ESC/P2, MTPL + Barcode + LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Roman, Sans Serif, Prestige, Script, OCR-A / B , 14 c&oacute;digos de barras e impresi&oacute;n de car&aacute;cteres grandes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta negra: 8 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de papel est&aacute;ndar</th>\r\n			<td>Tractor de empuje y bandeja de hojas sueltas; tractor posterior opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas sueltas, formas cont&iacute;nuas, formularios cont&iacute;nuos de m&uacute;ltiples partes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 6 copias (Formularios de 7 partes)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>60-120 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>Ancho: 76 a 420 mm; Largo: 76 a 599 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>10,6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico</th>\r\n			<td>(Alto x Ancho x Profundidad)&nbsp;600 x 265 x 200 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>110 / 230 VAC, 50Hz a 60Hz,&nbsp;Autoajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>M&aacute;x 7.6W en modo de espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000 / XP / Vista /7 / 8 y Server 2003 / 2008, SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>10&deg; C a 40&deg; C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;52dB (A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o en centro de servicio</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 30),
(93, 'Caracteristicas Tally 2610', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>680 cps, 570 cps (Impresi&oacute;n m&aacute;s r&aacute;pida); 500 cps (Borrador Alta Velocidad), 333 cps (Borrador), 111 cps (LQ) a 10 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Matriz de punto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Columnas</th>\r\n			<td>137</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>442 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de l&iacute;nea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>500 millones de impactos por aguja</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (bidireccional), USB 2.0 y Ethernet (10/100 Base T) est&aacute;ndar; RS-232 opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n est&aacute;ndar</th>\r\n			<td>IBM Proprinter XL24, Epson ESC/P2, MTPL + Barcode + LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Roman, Sans Serif, Prestige, Script, OCR-A / B , 14 c&oacute;digos de barras e impresi&oacute;n de car&aacute;cteres grandes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta negra: 8 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de papel est&aacute;ndar</th>\r\n			<td>Tractor de empuje y bandeja de hojas sueltas; tractor posterior opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas sueltas, formas cont&iacute;nuas, formularios cont&iacute;nuos de m&uacute;ltiples partes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 6 copias (Formularios de 7 partes)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>60-120 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>Ancho: 76 a 420 mm; Largo: 76 a 599 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>10,6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico</th>\r\n			<td>(Alto x Ancho x Profundidad)&nbsp;600 x 265 x 200 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>110 / 230 VAC, 50Hz a 60Hz,&nbsp;Autoajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>M&aacute;x 7.6W en modo de espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000 / XP / Vista /7 / 8 y Server 2003 / 2008, SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>10&deg; C a 40&deg; C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;52dB (A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o en centro de servicio</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 30),
(94, 'Caracteristicas Tally 2610', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>680 cps, 570 cps (Impresi&oacute;n m&aacute;s r&aacute;pida); 500 cps (Borrador Alta Velocidad), 333 cps (Borrador), 111 cps (LQ) a 10 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Matriz de punto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Columnas</th>\r\n			<td>137</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>442 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de l&iacute;nea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>500 millones de impactos por aguja</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (bidireccional), USB 2.0 y Ethernet (10/100 Base T) est&aacute;ndar; RS-232 opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n est&aacute;ndar</th>\r\n			<td>IBM Proprinter XL24, Epson ESC/P2, MTPL + Barcode + LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Roman, Sans Serif, Prestige, Script, OCR-A / B , 14 c&oacute;digos de barras e impresi&oacute;n de car&aacute;cteres grandes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta negra: 8 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de papel est&aacute;ndar</th>\r\n			<td>Tractor de empuje y bandeja de hojas sueltas; tractor posterior opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas sueltas, formas cont&iacute;nuas, formularios cont&iacute;nuos de m&uacute;ltiples partes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 6 copias (Formularios de 7 partes)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>60-120 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>Ancho: 76 a 420 mm; Largo: 76 a 599 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>10,6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico</th>\r\n			<td>(Alto x Ancho x Profundidad)&nbsp;600 x 265 x 200 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>110 / 230 VAC, 50Hz a 60Hz,&nbsp;Autoajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>M&aacute;x 7.6W en modo de espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000 / XP / Vista /7 / 8 y Server 2003 / 2008, SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>10&deg; C a 40&deg; C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;52dB (A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o en centro de servicio</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 30),
(95, 'Caracteristicas Tally 2610', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>680 cps, 570 cps (Impresi&oacute;n m&aacute;s r&aacute;pida); 500 cps (Borrador Alta Velocidad), 333 cps (Borrador), 111 cps (LQ) a 10 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Matriz de punto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Columnas</th>\r\n			<td>137</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>442 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de l&iacute;nea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>500 millones de impactos por aguja</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (bidireccional), USB 2.0 y Ethernet (10/100 Base T) est&aacute;ndar; RS-232 opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n est&aacute;ndar</th>\r\n			<td>IBM Proprinter XL24, Epson ESC/P2, MTPL + Barcode + LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Roman, Sans Serif, Prestige, Script, OCR-A / B , 14 c&oacute;digos de barras e impresi&oacute;n de car&aacute;cteres grandes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta negra: 8 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de papel est&aacute;ndar</th>\r\n			<td>Tractor de empuje y bandeja de hojas sueltas; tractor posterior opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas sueltas, formas cont&iacute;nuas, formularios cont&iacute;nuos de m&uacute;ltiples partes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 6 copias (Formularios de 7 partes)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>60-120 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>Ancho: 76 a 420 mm; Largo: 76 a 599 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>10,6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico</th>\r\n			<td>(Alto x Ancho x Profundidad)&nbsp;600 x 265 x 200 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>110 / 230 VAC, 50Hz a 60Hz,&nbsp;Autoajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>M&aacute;x 7.6W en modo de espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000 / XP / Vista /7 / 8 y Server 2003 / 2008, SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>10&deg; C a 40&deg; C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;52dB (A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o en centro de servicio</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 30),
(96, 'Descripcion Tally 2380', 'Los modelos 2365 y 2380 son impresoras de matriz de alta velocidad \r\nrobustos y de gran alcance para el funcionamiento de alta resistencia en\r\n las condiciones más extremas. Fiable, la producción para altas cargas \r\nde trabajo está garantizada por muchos años después de compra de la \r\nimpresora. La impresión es nítida, fuerte, oscuro y absolutamente \r\nlegible, gracias al cabezal de impresión 24 pines.\r\n\r\nBajos costes de funcionamiento y durabilidad en los entornos más \r\nexigentes de impresión hacen que estos productos de excelente relación \r\ncalidad-precio.\r\n\r\nEl funcionamiento completamente automatizado, junto con recorrido del\r\n papel de alimentación directa , permite la facilidad de uso y \r\nprácticamente elimina los errores causados ??por la intervención del \r\nusuario.\r\n\r\nEstos productos se corresponden con las aplicaciones de Microsoft \r\nWindows y altamente compatible con otros entornos de hospedaje de \r\noperación, incluyendo los sistemas de legado.<br>', 'product', 1, 'es', 'bo', 31),
(97, 'Descripcion Tally 2380', 'Los modelos 2365 y 2380 son impresoras de matriz de alta velocidad \r\nrobustos y de gran alcance para el funcionamiento de alta resistencia en\r\n las condiciones más extremas. Fiable, la producción para altas cargas \r\nde trabajo está garantizada por muchos años después de compra de la \r\nimpresora. La impresión es nítida, fuerte, oscuro y absolutamente \r\nlegible, gracias al cabezal de impresión 24 pines.\r\n\r\nBajos costes de funcionamiento y durabilidad en los entornos más \r\nexigentes de impresión hacen que estos productos de excelente relación \r\ncalidad-precio.\r\n\r\nEl funcionamiento completamente automatizado, junto con recorrido del\r\n papel de alimentación directa , permite la facilidad de uso y \r\nprácticamente elimina los errores causados ??por la intervención del \r\nusuario.\r\n\r\nEstos productos se corresponden con las aplicaciones de Microsoft \r\nWindows y altamente compatible con otros entornos de hospedaje de \r\noperación, incluyendo los sistemas de legado.<br>', 'product', 1, 'es', 'co', 31),
(98, 'Descripcion Tally 2380', 'Los modelos 2365 y 2380 son impresoras de matriz de alta velocidad \r\nrobustos y de gran alcance para el funcionamiento de alta resistencia en\r\n las condiciones más extremas. Fiable, la producción para altas cargas \r\nde trabajo está garantizada por muchos años después de compra de la \r\nimpresora. La impresión es nítida, fuerte, oscuro y absolutamente \r\nlegible, gracias al cabezal de impresión 24 pines.\r\n\r\nBajos costes de funcionamiento y durabilidad en los entornos más \r\nexigentes de impresión hacen que estos productos de excelente relación \r\ncalidad-precio.\r\n\r\nEl funcionamiento completamente automatizado, junto con recorrido del\r\n papel de alimentación directa , permite la facilidad de uso y \r\nprácticamente elimina los errores causados ??por la intervención del \r\nusuario.\r\n\r\nEstos productos se corresponden con las aplicaciones de Microsoft \r\nWindows y altamente compatible con otros entornos de hospedaje de \r\noperación, incluyendo los sistemas de legado.<br>', 'product', 1, 'es', 'mx', 31),
(99, 'Descripcion Tally 2380', 'Los modelos 2365 y 2380 son impresoras de matriz de alta velocidad \r\nrobustos y de gran alcance para el funcionamiento de alta resistencia en\r\n las condiciones más extremas. Fiable, la producción para altas cargas \r\nde trabajo está garantizada por muchos años después de compra de la \r\nimpresora. La impresión es nítida, fuerte, oscuro y absolutamente \r\nlegible, gracias al cabezal de impresión 24 pines.\r\n\r\nBajos costes de funcionamiento y durabilidad en los entornos más \r\nexigentes de impresión hacen que estos productos de excelente relación \r\ncalidad-precio.\r\n\r\nEl funcionamiento completamente automatizado, junto con recorrido del\r\n papel de alimentación directa , permite la facilidad de uso y \r\nprácticamente elimina los errores causados ??por la intervención del \r\nusuario.\r\n\r\nEstos productos se corresponden con las aplicaciones de Microsoft \r\nWindows y altamente compatible con otros entornos de hospedaje de \r\noperación, incluyendo los sistemas de legado.<br>', 'product', 1, 'es', 'pe', 31),
(100, 'Caracteristicas Tally 2380', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>En 10, 12 cpi: Borrador r&aacute;pido 800, 1000; Borrador 667, 800; NLQ 222, 267</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impacto Serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>605 p&aacute;ginas por hora; 2365HD 680 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>5, 6, 7.5, 8.6, 10, 12, 15, 17.1, 20 cpi (Depende de Emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida Cabezal</th>\r\n			<td>750 millones de caracteres (borrador)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>Paralelo y Ethernet 10/100BaseT Bi-direccional con un buffer de 128Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>RS232 Serial, Twinax/Coax, Ethernet IPDS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>IBM&reg; Proprinter, Epson&reg; LQ (ESC P/2), Genicom ANSI, MTPL + Barcode/LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Opcional</th>\r\n			<td>PCL3 (rugged writer), IGP Versi&oacute;n: IGP/PGL, Code V, MT660, Printronix P6000, HP2564C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador R&aacute;pido, Borrador, Borrador Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, Kix/Royal, OCRA,OCR B, 22 c&oacute;digos de barra &amp; fuentes LCP escalables, C&oacute;digo de barras de correo Inteligente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 20 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Alimentador de papel con bandeja de recepci&oacute;n y AGA; 2do alimentador trasero s&oacute;lo en la T2380-2T</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel Opcional</th>\r\n			<td>Tractor de papel secundario (instalable por usuario), pedestal para papel, cesta, cortador de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Orginal + 5 (6-copias); original +1 (2-copias) alimentador trasero T2380-2T</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Papel Continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>60-120gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>Largo: 3&quot; min, 22&quot; max; Ancho: 3&quot; min, 16.5&quot; max</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>19 Kg (42 lbs.)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 280mm x 623mm x 432mm (Aprox. 11.0x24.6x17.0 pulgadas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 230V a 120V (seleccionable)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>60 Watts imprimiendo, Max 18 Watts hibernando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>Hasta 50,000 p&aacute;ginas mensuales</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>12,500 horas @ 25% ciclo de trabajo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTTR</th>\r\n			<td>30 minutos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista y SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>20% a 80%</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>59dBA (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>90 d&iacute;&shy;as de garant&iacute;a en sitio</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Para aplicaciones de OCR, se recomienda que los lectores se prueban con impresiones de muestra suministradas por DASCOM antes de comprar. El papel es un medio muy variable y se debe probar antes de comprar.La vida consumible citada es el promedio de la impresora cuando se utiliza en condiciones normales de operaci&oacute;n.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 31),
(101, 'Caracteristicas Tally 2380', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>En 10, 12 cpi: Borrador r&aacute;pido 800, 1000; Borrador 667, 800; NLQ 222, 267</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impacto Serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>605 p&aacute;ginas por hora; 2365HD 680 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>5, 6, 7.5, 8.6, 10, 12, 15, 17.1, 20 cpi (Depende de Emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida Cabezal</th>\r\n			<td>750 millones de caracteres (borrador)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>Paralelo y Ethernet 10/100BaseT Bi-direccional con un buffer de 128Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>RS232 Serial, Twinax/Coax, Ethernet IPDS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>IBM&reg; Proprinter, Epson&reg; LQ (ESC P/2), Genicom ANSI, MTPL + Barcode/LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Opcional</th>\r\n			<td>PCL3 (rugged writer), IGP Versi&oacute;n: IGP/PGL, Code V, MT660, Printronix P6000, HP2564C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador R&aacute;pido, Borrador, Borrador Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, Kix/Royal, OCRA,OCR B, 22 c&oacute;digos de barra &amp; fuentes LCP escalables, C&oacute;digo de barras de correo Inteligente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 20 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Alimentador de papel con bandeja de recepci&oacute;n y AGA; 2do alimentador trasero s&oacute;lo en la T2380-2T</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel Opcional</th>\r\n			<td>Tractor de papel secundario (instalable por usuario), pedestal para papel, cesta, cortador de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Orginal + 5 (6-copias); original +1 (2-copias) alimentador trasero T2380-2T</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Papel Continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>60-120gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>Largo: 3&quot; min, 22&quot; max; Ancho: 3&quot; min, 16.5&quot; max</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>19 Kg (42 lbs.)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 280mm x 623mm x 432mm (Aprox. 11.0x24.6x17.0 pulgadas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 230V a 120V (seleccionable)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>60 Watts imprimiendo, Max 18 Watts hibernando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>Hasta 50,000 p&aacute;ginas mensuales</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>12,500 horas @ 25% ciclo de trabajo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTTR</th>\r\n			<td>30 minutos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista y SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>20% a 80%</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>59dBA (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>90 d&iacute;&shy;as de garant&iacute;a en sitio</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Para aplicaciones de OCR, se recomienda que los lectores se prueban con impresiones de muestra suministradas por DASCOM antes de comprar. El papel es un medio muy variable y se debe probar antes de comprar.La vida consumible citada es el promedio de la impresora cuando se utiliza en condiciones normales de operaci&oacute;n.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 31),
(102, 'Caracteristicas Tally 2380', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>En 10, 12 cpi: Borrador r&aacute;pido 800, 1000; Borrador 667, 800; NLQ 222, 267</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impacto Serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>605 p&aacute;ginas por hora; 2365HD 680 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>5, 6, 7.5, 8.6, 10, 12, 15, 17.1, 20 cpi (Depende de Emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida Cabezal</th>\r\n			<td>750 millones de caracteres (borrador)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>Paralelo y Ethernet 10/100BaseT Bi-direccional con un buffer de 128Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>RS232 Serial, Twinax/Coax, Ethernet IPDS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>IBM&reg; Proprinter, Epson&reg; LQ (ESC P/2), Genicom ANSI, MTPL + Barcode/LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Opcional</th>\r\n			<td>PCL3 (rugged writer), IGP Versi&oacute;n: IGP/PGL, Code V, MT660, Printronix P6000, HP2564C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador R&aacute;pido, Borrador, Borrador Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, Kix/Royal, OCRA,OCR B, 22 c&oacute;digos de barra &amp; fuentes LCP escalables, C&oacute;digo de barras de correo Inteligente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 20 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Alimentador de papel con bandeja de recepci&oacute;n y AGA; 2do alimentador trasero s&oacute;lo en la T2380-2T</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel Opcional</th>\r\n			<td>Tractor de papel secundario (instalable por usuario), pedestal para papel, cesta, cortador de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Orginal + 5 (6-copias); original +1 (2-copias) alimentador trasero T2380-2T</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Papel Continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>60-120gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>Largo: 3&quot; min, 22&quot; max; Ancho: 3&quot; min, 16.5&quot; max</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>19 Kg (42 lbs.)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 280mm x 623mm x 432mm (Aprox. 11.0x24.6x17.0 pulgadas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 230V a 120V (seleccionable)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>60 Watts imprimiendo, Max 18 Watts hibernando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>Hasta 50,000 p&aacute;ginas mensuales</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>12,500 horas @ 25% ciclo de trabajo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTTR</th>\r\n			<td>30 minutos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista y SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>20% a 80%</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>59dBA (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>90 d&iacute;&shy;as de garant&iacute;a en sitio</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Para aplicaciones de OCR, se recomienda que los lectores se prueban con impresiones de muestra suministradas por DASCOM antes de comprar. El papel es un medio muy variable y se debe probar antes de comprar.La vida consumible citada es el promedio de la impresora cuando se utiliza en condiciones normales de operaci&oacute;n.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 31);
INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(103, 'Caracteristicas Tally 2380', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>En 10, 12 cpi: Borrador r&aacute;pido 800, 1000; Borrador 667, 800; NLQ 222, 267</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impacto Serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>605 p&aacute;ginas por hora; 2365HD 680 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>5, 6, 7.5, 8.6, 10, 12, 15, 17.1, 20 cpi (Depende de Emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida Cabezal</th>\r\n			<td>750 millones de caracteres (borrador)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>Paralelo y Ethernet 10/100BaseT Bi-direccional con un buffer de 128Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>RS232 Serial, Twinax/Coax, Ethernet IPDS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>IBM&reg; Proprinter, Epson&reg; LQ (ESC P/2), Genicom ANSI, MTPL + Barcode/LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Opcional</th>\r\n			<td>PCL3 (rugged writer), IGP Versi&oacute;n: IGP/PGL, Code V, MT660, Printronix P6000, HP2564C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador R&aacute;pido, Borrador, Borrador Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, Kix/Royal, OCRA,OCR B, 22 c&oacute;digos de barra &amp; fuentes LCP escalables, C&oacute;digo de barras de correo Inteligente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 20 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Alimentador de papel con bandeja de recepci&oacute;n y AGA; 2do alimentador trasero s&oacute;lo en la T2380-2T</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel Opcional</th>\r\n			<td>Tractor de papel secundario (instalable por usuario), pedestal para papel, cesta, cortador de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Orginal + 5 (6-copias); original +1 (2-copias) alimentador trasero T2380-2T</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Papel Continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>60-120gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>Largo: 3&quot; min, 22&quot; max; Ancho: 3&quot; min, 16.5&quot; max</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>19 Kg (42 lbs.)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 280mm x 623mm x 432mm (Aprox. 11.0x24.6x17.0 pulgadas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 230V a 120V (seleccionable)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>60 Watts imprimiendo, Max 18 Watts hibernando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>Hasta 50,000 p&aacute;ginas mensuales</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>12,500 horas @ 25% ciclo de trabajo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTTR</th>\r\n			<td>30 minutos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista y SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>20% a 80%</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>59dBA (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>90 d&iacute;&shy;as de garant&iacute;a en sitio</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Para aplicaciones de OCR, se recomienda que los lectores se prueban con impresiones de muestra suministradas por DASCOM antes de comprar. El papel es un medio muy variable y se debe probar antes de comprar.La vida consumible citada es el promedio de la impresora cuando se utiliza en condiciones normales de operaci&oacute;n.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 31),
(104, 'Descripcion T2365', 'Las impresoras 2365 MC y la 2380 son impresoras matriciales fuertes y\r\n poderosas para operaciones de trabajo pesado bajo condiciones \r\nextremas.Confiabilidad, grandes cargas de trabajo estan garantizadas \r\nsiguiendo las indicaciones al momento de la compra.La impresión es \r\nní­tida, fuerte, oscura y absolutamente legible, gracias al cabezal de \r\nimpresión de 24 pines.\r\n\r\nBajos costes de funcionamiento y durabilidad en las más difí­ciles \r\ncondiciones de impresión hacen de estos productos una excelente relación\r\n calidad-precio.\r\n\r\nEl funcionamiento completamente automatizado, junto con recorrido de \r\npapel directo, permite facilidad de uso y prácticamenteelimina los \r\nerrores causados por la intervención del usuario.\r\n\r\nEl producto está bien adaptado para las aplicaciones de Microsoft \r\nWindows, y totalmente adaptado a otros sistemas operativos, incluyendo \r\nsistemas heredados.  \r\n        <br>', 'product', 1, 'es', 'bo', 32),
(105, 'Descripcion T2365', 'Las impresoras 2365 MC y la 2380 son impresoras matriciales fuertes y\r\n poderosas para operaciones de trabajo pesado bajo condiciones \r\nextremas.Confiabilidad, grandes cargas de trabajo estan garantizadas \r\nsiguiendo las indicaciones al momento de la compra.La impresión es \r\nní­tida, fuerte, oscura y absolutamente legible, gracias al cabezal de \r\nimpresión de 24 pines.\r\n\r\nBajos costes de funcionamiento y durabilidad en las más difí­ciles \r\ncondiciones de impresión hacen de estos productos una excelente relación\r\n calidad-precio.\r\n\r\nEl funcionamiento completamente automatizado, junto con recorrido de \r\npapel directo, permite facilidad de uso y prácticamenteelimina los \r\nerrores causados por la intervención del usuario.\r\n\r\nEl producto está bien adaptado para las aplicaciones de Microsoft \r\nWindows, y totalmente adaptado a otros sistemas operativos, incluyendo \r\nsistemas heredados.  \r\n        <br>', 'product', 1, 'es', 'co', 32),
(106, 'Descripcion T2365', 'Las impresoras 2365 MC y la 2380 son impresoras matriciales fuertes y\r\n poderosas para operaciones de trabajo pesado bajo condiciones \r\nextremas.Confiabilidad, grandes cargas de trabajo estan garantizadas \r\nsiguiendo las indicaciones al momento de la compra.La impresión es \r\nní­tida, fuerte, oscura y absolutamente legible, gracias al cabezal de \r\nimpresión de 24 pines.\r\n\r\nBajos costes de funcionamiento y durabilidad en las más difí­ciles \r\ncondiciones de impresión hacen de estos productos una excelente relación\r\n calidad-precio.\r\n\r\nEl funcionamiento completamente automatizado, junto con recorrido de \r\npapel directo, permite facilidad de uso y prácticamenteelimina los \r\nerrores causados por la intervención del usuario.\r\n\r\nEl producto está bien adaptado para las aplicaciones de Microsoft \r\nWindows, y totalmente adaptado a otros sistemas operativos, incluyendo \r\nsistemas heredados.  \r\n        <br>', 'product', 1, 'es', 'mx', 32),
(107, 'Descripcion T2365', 'Las impresoras 2365 MC y la 2380 son impresoras matriciales fuertes y\r\n poderosas para operaciones de trabajo pesado bajo condiciones \r\nextremas.Confiabilidad, grandes cargas de trabajo estan garantizadas \r\nsiguiendo las indicaciones al momento de la compra.La impresión es \r\nní­tida, fuerte, oscura y absolutamente legible, gracias al cabezal de \r\nimpresión de 24 pines.\r\n\r\nBajos costes de funcionamiento y durabilidad en las más difí­ciles \r\ncondiciones de impresión hacen de estos productos una excelente relación\r\n calidad-precio.\r\n\r\nEl funcionamiento completamente automatizado, junto con recorrido de \r\npapel directo, permite facilidad de uso y prácticamenteelimina los \r\nerrores causados por la intervención del usuario.\r\n\r\nEl producto está bien adaptado para las aplicaciones de Microsoft \r\nWindows, y totalmente adaptado a otros sistemas operativos, incluyendo \r\nsistemas heredados.  \r\n        <br>', 'product', 1, 'es', 'pe', 32),
(108, 'Caracteristicas T2365', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>En 10, 12 cpi: Borrador rapida 700, 840; Borrador 533, 640; NLQ 180, 215</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impacto Serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>605 p&aacute;ginas por hora; 2365HD 587 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>5, 6, 7.5, 8.6, 10, 12, 15, 17.1, 20 cpi (Depende de Emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida Cabezal</th>\r\n			<td>500 millones de caracteres (borrador)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>Paralelo y Ethernet 10/100BaseT Bi-direccional con un buffer de 128Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>RS232 Serial, Twinax/Coax, Ethernet IPDS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>IBM&reg; Proprinter, Epson&reg; LQ (ESC P/2), Genicom ANSI, MTPL + Barcode/LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Opcional</th>\r\n			<td>PCL3 (rugged writer), IGP Versi&oacute;n: IGP/PGL, Code V, MT660, Printronix P6000, HP2564C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador R&aacute;pido, Borrador, Borrador Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, Kix/Royal, OCRA,OCR B, 22 c&oacute;digos de barra &amp; fuentes LCP escalables, C&oacute;digo de barras de correo Inteligente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 20 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Alimentador de papel con bandeja de recepci&oacute;n y AGA</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel Opcional</th>\r\n			<td>Tractor de papel secundario (instalable por usuario), pedestal para papel, cesta, cortador de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 8 (9-copias)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Papel Continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>60-120gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>3.94&quot; a 16.54&quot; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>19 Kg (42 lbs.)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 280mm x 623mm x 432mm (Aprox. 11.0x24.6x17.0 inches)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 230V a 120V (seleccionable)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>60 Watts imprimiendo, Max 18 Watts hibernando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Certificaciones</th>\r\n			<td>Energy Star calificado, Ozone-free. RoHS, Microsoft, certificaci&oacute;n ISO</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>Hasta 48,000 p&aacute;ginas mensuales</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>12,500 horas @ 25% ciclo de trabajo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista y SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>20% a 80%</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>59dBA (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>90 d&iacute;&shy;as de garant&iacute;a en sitio</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Para aplicaciones de OCR, se recomienda que los lectores se prueban con impresiones de muestra suministradas por DASCOM antes de comprar. El papel es un medio muy variable y se debe probar antes de comprar.La vida consumible citada es el promedio de la impresora cuando se utiliza en condiciones normales de operaci&oacute;n.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 32),
(109, 'Caracteristicas T2365', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>En 10, 12 cpi: Borrador rapida 700, 840; Borrador 533, 640; NLQ 180, 215</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impacto Serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>605 p&aacute;ginas por hora; 2365HD 587 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>5, 6, 7.5, 8.6, 10, 12, 15, 17.1, 20 cpi (Depende de Emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida Cabezal</th>\r\n			<td>500 millones de caracteres (borrador)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>Paralelo y Ethernet 10/100BaseT Bi-direccional con un buffer de 128Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>RS232 Serial, Twinax/Coax, Ethernet IPDS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>IBM&reg; Proprinter, Epson&reg; LQ (ESC P/2), Genicom ANSI, MTPL + Barcode/LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Opcional</th>\r\n			<td>PCL3 (rugged writer), IGP Versi&oacute;n: IGP/PGL, Code V, MT660, Printronix P6000, HP2564C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador R&aacute;pido, Borrador, Borrador Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, Kix/Royal, OCRA,OCR B, 22 c&oacute;digos de barra &amp; fuentes LCP escalables, C&oacute;digo de barras de correo Inteligente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 20 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Alimentador de papel con bandeja de recepci&oacute;n y AGA</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel Opcional</th>\r\n			<td>Tractor de papel secundario (instalable por usuario), pedestal para papel, cesta, cortador de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 8 (9-copias)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Papel Continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>60-120gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>3.94&quot; a 16.54&quot; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>19 Kg (42 lbs.)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 280mm x 623mm x 432mm (Aprox. 11.0x24.6x17.0 inches)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 230V a 120V (seleccionable)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>60 Watts imprimiendo, Max 18 Watts hibernando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Certificaciones</th>\r\n			<td>Energy Star calificado, Ozone-free. RoHS, Microsoft, certificaci&oacute;n ISO</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>Hasta 48,000 p&aacute;ginas mensuales</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>12,500 horas @ 25% ciclo de trabajo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista y SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>20% a 80%</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>59dBA (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>90 d&iacute;&shy;as de garant&iacute;a en sitio</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Para aplicaciones de OCR, se recomienda que los lectores se prueban con impresiones de muestra suministradas por DASCOM antes de comprar. El papel es un medio muy variable y se debe probar antes de comprar.La vida consumible citada es el promedio de la impresora cuando se utiliza en condiciones normales de operaci&oacute;n.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 32),
(110, 'Caracteristicas T2365', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>En 10, 12 cpi: Borrador rapida 700, 840; Borrador 533, 640; NLQ 180, 215</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impacto Serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>605 p&aacute;ginas por hora; 2365HD 587 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>5, 6, 7.5, 8.6, 10, 12, 15, 17.1, 20 cpi (Depende de Emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida Cabezal</th>\r\n			<td>500 millones de caracteres (borrador)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>Paralelo y Ethernet 10/100BaseT Bi-direccional con un buffer de 128Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>RS232 Serial, Twinax/Coax, Ethernet IPDS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>IBM&reg; Proprinter, Epson&reg; LQ (ESC P/2), Genicom ANSI, MTPL + Barcode/LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Opcional</th>\r\n			<td>PCL3 (rugged writer), IGP Versi&oacute;n: IGP/PGL, Code V, MT660, Printronix P6000, HP2564C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador R&aacute;pido, Borrador, Borrador Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, Kix/Royal, OCRA,OCR B, 22 c&oacute;digos de barra &amp; fuentes LCP escalables, C&oacute;digo de barras de correo Inteligente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 20 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Alimentador de papel con bandeja de recepci&oacute;n y AGA</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel Opcional</th>\r\n			<td>Tractor de papel secundario (instalable por usuario), pedestal para papel, cesta, cortador de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 8 (9-copias)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Papel Continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>60-120gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>3.94&quot; a 16.54&quot; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>19 Kg (42 lbs.)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 280mm x 623mm x 432mm (Aprox. 11.0x24.6x17.0 inches)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 230V a 120V (seleccionable)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>60 Watts imprimiendo, Max 18 Watts hibernando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Certificaciones</th>\r\n			<td>Energy Star calificado, Ozone-free. RoHS, Microsoft, certificaci&oacute;n ISO</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>Hasta 48,000 p&aacute;ginas mensuales</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>12,500 horas @ 25% ciclo de trabajo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista y SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>20% a 80%</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>59dBA (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>90 d&iacute;&shy;as de garant&iacute;a en sitio</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Para aplicaciones de OCR, se recomienda que los lectores se prueban con impresiones de muestra suministradas por DASCOM antes de comprar. El papel es un medio muy variable y se debe probar antes de comprar.La vida consumible citada es el promedio de la impresora cuando se utiliza en condiciones normales de operaci&oacute;n.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 32),
(111, 'Caracteristicas T2365', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>En 10, 12 cpi: Borrador rapida 700, 840; Borrador 533, 640; NLQ 180, 215</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impacto Serial</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Desempe&ntilde;o</th>\r\n			<td>605 p&aacute;ginas por hora; 2365HD 587 p&aacute;ginas por hora (ECMA 132)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>5, 6, 7.5, 8.6, 10, 12, 15, 17.1, 20 cpi (Depende de Emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tiempo de vida Cabezal</th>\r\n			<td>500 millones de caracteres (borrador)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Est&aacute;ndar</th>\r\n			<td>Paralelo y Ethernet 10/100BaseT Bi-direccional con un buffer de 128Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>RS232 Serial, Twinax/Coax, Ethernet IPDS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Est&aacute;ndar</th>\r\n			<td>IBM&reg; Proprinter, Epson&reg; LQ (ESC P/2), Genicom ANSI, MTPL + Barcode/LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n Opcional</th>\r\n			<td>PCL3 (rugged writer), IGP Versi&oacute;n: IGP/PGL, Code V, MT660, Printronix P6000, HP2564C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador R&aacute;pido, Borrador, Borrador Copia, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, Kix/Royal, OCRA,OCR B, 22 c&oacute;digos de barra &amp; fuentes LCP escalables, C&oacute;digo de barras de correo Inteligente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 20 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Alimentador de papel con bandeja de recepci&oacute;n y AGA</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel Opcional</th>\r\n			<td>Tractor de papel secundario (instalable por usuario), pedestal para papel, cesta, cortador de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 8 (9-copias)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Papel Continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>60-120gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>3.94&quot; a 16.54&quot; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>19 Kg (42 lbs.)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 280mm x 623mm x 432mm (Aprox. 11.0x24.6x17.0 inches)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>AC 230V a 120V (seleccionable)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>60 Watts imprimiendo, Max 18 Watts hibernando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Certificaciones</th>\r\n			<td>Energy Star calificado, Ozone-free. RoHS, Microsoft, certificaci&oacute;n ISO</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de trabajo</th>\r\n			<td>Hasta 48,000 p&aacute;ginas mensuales</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>12,500 horas @ 25% ciclo de trabajo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 9x/ME, 2000, 2003 Server, XP, Vista y SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+10&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>20% a 80%</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>59dBA (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>90 d&iacute;&shy;as de garant&iacute;a en sitio</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Para aplicaciones de OCR, se recomienda que los lectores se prueban con impresiones de muestra suministradas por DASCOM antes de comprar. El papel es un medio muy variable y se debe probar antes de comprar.La vida consumible citada es el promedio de la impresora cuando se utiliza en condiciones normales de operaci&oacute;n.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 32),
(112, 'Descripcion Tally 2600 ', 'La impresora Tally Dascom 2600 es una máquina confiable y con estilo&nbsp;muy\r\n destacada. El rendimiento y carga de trabajo, ergonomía, facilidad de \r\nmantenimiento, excelente precio -&nbsp;todo ha sido incorporado para \r\nasegurarse de que es la única opción aceptable para el cliente.&nbsp;<br>\r\n&nbsp;<br>\r\nFormularios continuos y hojas sueltas son alimentados en la parte \r\nfrontal para una carga rápida y fácil. Esto asegura una trayectoria \r\nrecta para las formas dando como resultado&nbsp;el transporte del papel \r\nconfiable, incluso en formularios de&nbsp;7 partes. La carga de papel es más \r\nfácil. El operador no tiene que buscar a tientas por encima o alrededor \r\nde la impresora para obtener acceso a los tractores situados en la parte\r\n posterior.&nbsp;<br>\r\n&nbsp;<br>\r\nLa intervención del usuario se reduce al mínimo con una serie de \r\nfuncionalidades diseñadas para asegurar que los productos funcionan con \r\nrendimiento y confiabilidad óptimos.<br>', 'product', 1, 'es', 'bo', 33),
(113, 'Descripcion Tally 2600 ', 'La impresora Tally Dascom 2600 es una máquina confiable y con estilo&nbsp;muy\r\n destacada. El rendimiento y carga de trabajo, ergonomía, facilidad de \r\nmantenimiento, excelente precio -&nbsp;todo ha sido incorporado para \r\nasegurarse de que es la única opción aceptable para el cliente.&nbsp;<br>\r\n&nbsp;<br>\r\nFormularios continuos y hojas sueltas son alimentados en la parte \r\nfrontal para una carga rápida y fácil. Esto asegura una trayectoria \r\nrecta para las formas dando como resultado&nbsp;el transporte del papel \r\nconfiable, incluso en formularios de&nbsp;7 partes. La carga de papel es más \r\nfácil. El operador no tiene que buscar a tientas por encima o alrededor \r\nde la impresora para obtener acceso a los tractores situados en la parte\r\n posterior.&nbsp;<br>\r\n&nbsp;<br>\r\nLa intervención del usuario se reduce al mínimo con una serie de \r\nfuncionalidades diseñadas para asegurar que los productos funcionan con \r\nrendimiento y confiabilidad óptimos.<br>', 'product', 1, 'es', 'co', 33),
(114, 'Descripcion Tally 2600 ', 'La impresora Tally Dascom 2600 es una máquina confiable y con estilo&nbsp;muy\r\n destacada. El rendimiento y carga de trabajo, ergonomía, facilidad de \r\nmantenimiento, excelente precio -&nbsp;todo ha sido incorporado para \r\nasegurarse de que es la única opción aceptable para el cliente.&nbsp;<br>\r\n&nbsp;<br>\r\nFormularios continuos y hojas sueltas son alimentados en la parte \r\nfrontal para una carga rápida y fácil. Esto asegura una trayectoria \r\nrecta para las formas dando como resultado&nbsp;el transporte del papel \r\nconfiable, incluso en formularios de&nbsp;7 partes. La carga de papel es más \r\nfácil. El operador no tiene que buscar a tientas por encima o alrededor \r\nde la impresora para obtener acceso a los tractores situados en la parte\r\n posterior.&nbsp;<br>\r\n&nbsp;<br>\r\nLa intervención del usuario se reduce al mínimo con una serie de \r\nfuncionalidades diseñadas para asegurar que los productos funcionan con \r\nrendimiento y confiabilidad óptimos.<br>', 'product', 1, 'es', 'mx', 33),
(115, 'Descripcion Tally 2600 ', 'La impresora Tally Dascom 2600 es una máquina confiable y con estilo&nbsp;muy\r\n destacada. El rendimiento y carga de trabajo, ergonomía, facilidad de \r\nmantenimiento, excelente precio -&nbsp;todo ha sido incorporado para \r\nasegurarse de que es la única opción aceptable para el cliente.&nbsp;<br>\r\n&nbsp;<br>\r\nFormularios continuos y hojas sueltas son alimentados en la parte \r\nfrontal para una carga rápida y fácil. Esto asegura una trayectoria \r\nrecta para las formas dando como resultado&nbsp;el transporte del papel \r\nconfiable, incluso en formularios de&nbsp;7 partes. La carga de papel es más \r\nfácil. El operador no tiene que buscar a tientas por encima o alrededor \r\nde la impresora para obtener acceso a los tractores situados en la parte\r\n posterior.&nbsp;<br>\r\n&nbsp;<br>\r\nLa intervención del usuario se reduce al mínimo con una serie de \r\nfuncionalidades diseñadas para asegurar que los productos funcionan con \r\nrendimiento y confiabilidad óptimos.<br>', 'product', 1, 'es', 'pe', 33),
(116, 'Caracteristicas Tally 2600 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>680 cps, 570 cps (Impresi&oacute;n m&aacute;s r&aacute;pida); 500 cps (Borrador Alta Velocidad), 333 cps (Borrador), 111 cps (LQ) a 10 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Matriz de punto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Columnas</th>\r\n			<td>80</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>442 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de l&iacute;nea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>500 millones de impactos por aguja</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (bidireccional), USB 2.0 y Ethernet (10/100 Base T) est&aacute;ndar; RS-232 opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n est&aacute;ndar</th>\r\n			<td>IBM Proprinter XL24, Epson ESC/P2, MTPL + Barcode + LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Roman, Sans Serif, Prestige, Script, OCR-A / B , 14 c&oacute;digos de barras e impresi&oacute;n de car&aacute;cteres grandes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta negra: 8 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de papel est&aacute;ndar</th>\r\n			<td>Tractor de empuje y bandeja de hojas sueltas; tractor posterior opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas sueltas, formas cont&iacute;nuas, formularios cont&iacute;nuos de m&uacute;ltiples partes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 6 copias (Formularios de 7 partes)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>60-120 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>Ancho: 76 a 420 mm; Largo: 76 a 599 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>10,6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico</th>\r\n			<td>(Alto x Ancho x Profundidad)&nbsp;600 x 265 x 200 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>110 / 230 VAC, 50Hz a 60Hz,&nbsp;Autoajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>M&aacute;x 7.6W en modo de espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000 / XP / Vista /7 / 8 y Server 2003 / 2008, SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>10&deg; C a 40&deg; C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;52dB (A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o en centro de servicio</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 33),
(117, 'Caracteristicas Tally 2600 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>680 cps, 570 cps (Impresi&oacute;n m&aacute;s r&aacute;pida); 500 cps (Borrador Alta Velocidad), 333 cps (Borrador), 111 cps (LQ) a 10 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Matriz de punto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Columnas</th>\r\n			<td>80</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>442 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de l&iacute;nea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>500 millones de impactos por aguja</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (bidireccional), USB 2.0 y Ethernet (10/100 Base T) est&aacute;ndar; RS-232 opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n est&aacute;ndar</th>\r\n			<td>IBM Proprinter XL24, Epson ESC/P2, MTPL + Barcode + LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Roman, Sans Serif, Prestige, Script, OCR-A / B , 14 c&oacute;digos de barras e impresi&oacute;n de car&aacute;cteres grandes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta negra: 8 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de papel est&aacute;ndar</th>\r\n			<td>Tractor de empuje y bandeja de hojas sueltas; tractor posterior opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas sueltas, formas cont&iacute;nuas, formularios cont&iacute;nuos de m&uacute;ltiples partes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 6 copias (Formularios de 7 partes)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>60-120 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>Ancho: 76 a 420 mm; Largo: 76 a 599 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>10,6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico</th>\r\n			<td>(Alto x Ancho x Profundidad)&nbsp;600 x 265 x 200 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>110 / 230 VAC, 50Hz a 60Hz,&nbsp;Autoajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>M&aacute;x 7.6W en modo de espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000 / XP / Vista /7 / 8 y Server 2003 / 2008, SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>10&deg; C a 40&deg; C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;52dB (A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o en centro de servicio</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 33),
(118, 'Caracteristicas Tally 2600 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>680 cps, 570 cps (Impresi&oacute;n m&aacute;s r&aacute;pida); 500 cps (Borrador Alta Velocidad), 333 cps (Borrador), 111 cps (LQ) a 10 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Matriz de punto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Columnas</th>\r\n			<td>80</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>442 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de l&iacute;nea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>500 millones de impactos por aguja</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (bidireccional), USB 2.0 y Ethernet (10/100 Base T) est&aacute;ndar; RS-232 opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n est&aacute;ndar</th>\r\n			<td>IBM Proprinter XL24, Epson ESC/P2, MTPL + Barcode + LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Roman, Sans Serif, Prestige, Script, OCR-A / B , 14 c&oacute;digos de barras e impresi&oacute;n de car&aacute;cteres grandes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta negra: 8 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de papel est&aacute;ndar</th>\r\n			<td>Tractor de empuje y bandeja de hojas sueltas; tractor posterior opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas sueltas, formas cont&iacute;nuas, formularios cont&iacute;nuos de m&uacute;ltiples partes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 6 copias (Formularios de 7 partes)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>60-120 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>Ancho: 76 a 420 mm; Largo: 76 a 599 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>10,6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico</th>\r\n			<td>(Alto x Ancho x Profundidad)&nbsp;600 x 265 x 200 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>110 / 230 VAC, 50Hz a 60Hz,&nbsp;Autoajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>M&aacute;x 7.6W en modo de espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000 / XP / Vista /7 / 8 y Server 2003 / 2008, SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>10&deg; C a 40&deg; C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;52dB (A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o en centro de servicio</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 33),
(119, 'Caracteristicas Tally 2600 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>680 cps, 570 cps (Impresi&oacute;n m&aacute;s r&aacute;pida); 500 cps (Borrador Alta Velocidad), 333 cps (Borrador), 111 cps (LQ) a 10 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Matriz de punto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Columnas</th>\r\n			<td>80</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Hasta 360 x 360 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Rendimiento (ECMA 132)</th>\r\n			<td>442 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20 cpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de l&iacute;nea</th>\r\n			<td>2, 3, 4, 6, 8, 12 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal de impresi&oacute;n</th>\r\n			<td>500 millones de impactos por aguja</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>256 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (bidireccional), USB 2.0 y Ethernet (10/100 Base T) est&aacute;ndar; RS-232 opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n est&aacute;ndar</th>\r\n			<td>IBM Proprinter XL24, Epson ESC/P2, MTPL + Barcode + LCP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Borrador Alta Velocidad, Borrador, Courier, Roman, Sans Serif, Prestige, Script, OCR-A / B , 14 c&oacute;digos de barras e impresi&oacute;n de car&aacute;cteres grandes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta negra: 8 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de papel est&aacute;ndar</th>\r\n			<td>Tractor de empuje y bandeja de hojas sueltas; tractor posterior opcional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas sueltas, formas cont&iacute;nuas, formularios cont&iacute;nuos de m&uacute;ltiples partes</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>Original + 6 copias (Formularios de 7 partes)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>60-120 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o del papel</th>\r\n			<td>Ancho: 76 a 420 mm; Largo: 76 a 599 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso f&iacute;sico</th>\r\n			<td>10,6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o f&iacute;sico</th>\r\n			<td>(Alto x Ancho x Profundidad)&nbsp;600 x 265 x 200 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>110 / 230 VAC, 50Hz a 60Hz,&nbsp;Autoajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>M&aacute;x 7.6W en modo de espera</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>20,000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows 2000 / XP / Vista /7 / 8 y Server 2003 / 2008, SAP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>10&deg; C a 40&deg; C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>10% a 85% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;52dB (A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o en centro de servicio</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 33),
(120, 'Descripcion Tally 1225', '<p>La Tally Dascom 1225 es una impresora matriz de punto de 24 pines y 136 columnas con interfaces USB y paralela. El carro ancho es perfecto para entornos industriales y aplicaciones de oficina.</p>\r\n\r\n<p>Con bajos costes de funcionamiento, impresiones de alta calidad y un panel de control f&aacute;cil de utilizar es ideal para bajos vol&uacute;menes de informes de cotizaci&oacute;n,facturas y otros documentos importantes, as&iacute; como las notas de despacho y etiquetas.</p>\r\n\r\n<p>Adem&aacute;s de estas aplicaciones, la 1225 tambi&eacute;n es adecuada para sistemas de medici&oacute;n y control.</p>\r\n\r\n<p>Entre las impresoras matriciales de precio m&aacute;s bajo disponible en la actualidad la 1225 tambi&eacute;n ofrece una cinta de larga duraci&oacute;n que significa menos cambios de cinta, menos tiempo de inactividad y reducci&oacute;n en los costos de funcionamiento</p>\r\n', 'product', 1, 'es', 'co', 34),
(121, 'Caracteristicas Tally 1225', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Velocidad de Impresi&oacute;n</strong></td>\r\n			<td>10 cpi: 375 cps (Borrador R&aacute;pido), 250 cps (Borrador), 83 cps (LQ)&nbsp;<br />\r\n			12 cpi: 300 cps (Borrador), 100 cps (LQ)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>M&eacute;todo de Impresi&oacute;n</strong></td>\r\n			<td>Serial matriz de impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Resoluci&oacute;n</strong></td>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Desempe&ntilde;o (ECMA 132)</strong></td>\r\n			<td>336 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Densidad de caracteres</strong></td>\r\n			<td>10, 12, 15, 17.1, 20cpi, Espaciado proporcional</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Densidad de linea</strong></td>\r\n			<td>1, 2, 3, 4, 6, 8 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Vida cabezal de impresi&oacute;n</strong></td>\r\n			<td>400 Millones de golpes por pin</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Memoria</strong></td>\r\n			<td>48 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Conectividad</strong></td>\r\n			<td>USB 2.0 y paralelo (Bi-direccional)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Emulaci&oacute;n</strong></td>\r\n			<td>Epson LQ (ESC P/2), IBM ProPrinter (2390 Plus)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Fuentes</strong></td>\r\n			<td>Draft, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, OCR-A/B*, 10 C&oacute;digos de barras</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Consumibles</strong></td>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Manejo de Papel</strong></td>\r\n			<td>Tractor y bandeja de hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tipo de papel</strong></td>\r\n			<td>Hojas sueltas y continuas</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>N&uacute;mero de Copias</strong></td>\r\n			<td>Original +4 (5-copias)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Peso Papel</strong></td>\r\n			<td>52-100 gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tama&ntilde;o Papel</strong></td>\r\n			<td>3&ldquo; a 16.53&ldquo; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Peso</strong></td>\r\n			<td>Aprox. 18lbs</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tama&ntilde;o</strong></td>\r\n			<td>(HxWxD): 6.8&ldquo;x23.1&ldquo;x13.1&ldquo;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Voltaje</strong></td>\r\n			<td>230 V</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Frecuencia</strong></td>\r\n			<td>50Hz a 60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>consumo</strong></td>\r\n			<td>max 7.6W en espera</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Confiabilidad MTBF</strong></td>\r\n			<td>10.000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Drivers</strong></td>\r\n			<td>Windows 2000, 2003 Server, XP, Vista, Windows 7</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Temperatura</strong></td>\r\n			<td>+5&deg;C a +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Humedad</strong></td>\r\n			<td>10% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Ac&uacute;stica</strong></td>\r\n			<td>&lt; 55dB(A) (ISO 7779)</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 34),
(122, 'Descripcion P2506W CO', '<p>Impresora L&aacute;ser de Monocromo Inal&aacute;mbrica que destaca en Velocidad, Memoria y Capacidad. Puede ser usada tanto en casa como en oficina. Ahorre en costos y energ&iacute;a. Instalela f&aacute;cil y conectela como quiera, por USB o WiFi. Su alta velocidad de impresi&oacute;n le permite tener el trabajo listo a tiempo. Imprima hasta 1.600 p&aacute;ginas por toner y maneje un volumen mensual de hasta 15.000 p&aacute;ginas. Aproveche ahora!</p>\r\n', 'product', 1, 'es', 'co', 35),
(123, 'Caracteristicas P2506W ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">\r\n			<p>Velocidad de Impresi&oacute;n &nbsp;</p>\r\n			</th>\r\n			<td>22 ppm(A4) / 23 ppm(Carta)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Electrofotogr&aacute;fico l&aacute;ser monocrom&aacute;tica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Max 1200x1200 DPI</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Retardo desde el comando de impresi&oacute;n</th>\r\n			<td>7.8 seg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumible</th>\r\n			<td>Cartucho de t&oacute;ner PB-260</td>\r\n		</tr>\r\n		<tr>\r\n			<th>B&uacute;fer</th>\r\n			<td>128 MB</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Hi-Speed USB 2.0 -WIFI 802.11 b/g/n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>P&aacute;ginas por cartucho</th>\r\n			<td>1600 p&aacute;ginas aproximadamente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>4,75 kg con el cartucho de t&oacute;ner</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Gramaje</th>\r\n			<td>60-163 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>34 x 22 x 18 cm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nivel de ruido</th>\r\n			<td>52 dB</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Sobres, etiquetas, trasparencias, tarjetas postales, y cartulina.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>52-100 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sistema Operativo</th>\r\n			<td>Windows Xp, Server, 7, 8, 10. Linux Debian, y Mac OSX</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 35);
INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(124, 'Descripcion IT 7010', 'La impresora térmica 7010 ofrece un desempeño industrial en un diseño \r\nfuerte para volumenes de producción en ventas al detaly de manufactura.<br>\r\n<br>\r\nEl diseño amistoso de la impresora permita el ahorro de espacio, haciendo la 7010 ideal para espacios pequeños.&nbsp;<br>\r\n<br>\r\nEl gran display LCD da una vista clara del estatus de la impresora asi \r\ncomo el cambio automático entre emulación Zebra® y Datamax®asegurando \r\nuna fácil configuración. La conectividad vamas alla de puertos paralelo y\r\n USB, la 7010 ofrece un gran abanico de opciones como puerto Ethernet, \r\nun cortador y separador.<br>\r\n<br>\r\nLa 7010R integra un separador con rebobinado linear para guardado de papel y un fácil manejo de basura.<br>\r\n<br>\r\nLa 7010-300 con una resolución de 300 dpi es ideal para etiquetas con logos, imagenes y graficas.<br>', 'product', 1, 'es', 'bo', 36),
(125, 'Descripcion IT 7010', 'La impresora térmica 7010 ofrece un desempeño industrial en un diseño \r\nfuerte para volumenes de producción en ventas al detaly de manufactura.<br>\r\n<br>\r\nEl diseño amistoso de la impresora permita el ahorro de espacio, haciendo la 7010 ideal para espacios pequeños.&nbsp;<br>\r\n<br>\r\nEl gran display LCD da una vista clara del estatus de la impresora asi \r\ncomo el cambio automático entre emulación Zebra® y Datamax®asegurando \r\nuna fácil configuración. La conectividad vamas alla de puertos paralelo y\r\n USB, la 7010 ofrece un gran abanico de opciones como puerto Ethernet, \r\nun cortador y separador.<br>\r\n<br>\r\nLa 7010R integra un separador con rebobinado linear para guardado de papel y un fácil manejo de basura.<br>\r\n<br>\r\nLa 7010-300 con una resolución de 300 dpi es ideal para etiquetas con logos, imagenes y graficas.<br>', 'product', 1, 'es', 'co', 36),
(126, 'Descripcion IT 7010', 'La impresora térmica 7010 ofrece un desempeño industrial en un diseño \r\nfuerte para volumenes de producción en ventas al detaly de manufactura.<br>\r\n<br>\r\nEl diseño amistoso de la impresora permita el ahorro de espacio, haciendo la 7010 ideal para espacios pequeños.&nbsp;<br>\r\n<br>\r\nEl gran display LCD da una vista clara del estatus de la impresora asi \r\ncomo el cambio automático entre emulación Zebra® y Datamax®asegurando \r\nuna fácil configuración. La conectividad vamas alla de puertos paralelo y\r\n USB, la 7010 ofrece un gran abanico de opciones como puerto Ethernet, \r\nun cortador y separador.<br>\r\n<br>\r\nLa 7010R integra un separador con rebobinado linear para guardado de papel y un fácil manejo de basura.<br>\r\n<br>\r\nLa 7010-300 con una resolución de 300 dpi es ideal para etiquetas con logos, imagenes y graficas.<br>', 'product', 1, 'es', 'mx', 36),
(127, 'Descripcion IT 7010', 'La impresora térmica 7010 ofrece un desempeño industrial en un diseño \r\nfuerte para volumenes de producción en ventas al detaly de manufactura.<br>\r\n<br>\r\nEl diseño amistoso de la impresora permita el ahorro de espacio, haciendo la 7010 ideal para espacios pequeños.&nbsp;<br>\r\n<br>\r\nEl gran display LCD da una vista clara del estatus de la impresora asi \r\ncomo el cambio automático entre emulación Zebra® y Datamax®asegurando \r\nuna fácil configuración. La conectividad vamas alla de puertos paralelo y\r\n USB, la 7010 ofrece un gran abanico de opciones como puerto Ethernet, \r\nun cortador y separador.<br>\r\n<br>\r\nLa 7010R integra un separador con rebobinado linear para guardado de papel y un fácil manejo de basura.<br>\r\n<br>\r\nLa 7010-300 con una resolución de 300 dpi es ideal para etiquetas con logos, imagenes y graficas.<br>', 'product', 1, 'es', 'pe', 36),
(128, 'Caracteristicas IT 7010 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>10 ips (203 dpi) / 8 ips (300 spi)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Directo o transferencia t&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de Impresi&oacute;n</th>\r\n			<td>0.25&ldquo; a 32&ldquo; (6.4 mm a 812 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Modelos disponibles a 203 dpi / 300dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>fuente</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida Cabezal de Impresi&oacute;n</th>\r\n			<td>50 km o 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (IEEE-1284), Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta, papel/etiquetas t&eacute;rmicas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o papel</th>\r\n			<td>1.00&ldquo; a 4.65&ldquo; (25.4 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo del papel</th>\r\n			<td>0.25&ldquo; a 32.0&ldquo; (6.35 mm a 812.8)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>8&ldquo; (200 mm) di&aacute;metro externo. Tama&ntilde;o de n&uacute;cleo 1.5&ldquo; - 3&quot; (38 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero ajustable y sensor delantero de alta resoluci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cinta</th>\r\n			<td>Tinta interna o externa. Cera, Cera/Resina o tipo de resina</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de la Cinta</th>\r\n			<td>3.40 pulgadas (86.5 mm) maximo di&aacute;metro externo. 1476 ft (450 m) largo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de la Cinta</th>\r\n			<td>4.5 pulgadas (114.0 mm) max</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barras</th>\r\n			<td>C&oacute;digos de barras: Standard barcodes include: Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey, UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>13.3 kg, 17.6 kg (Modelo con rebobinado)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 265mm x 255mm x 490mm,&nbsp;<br />\r\n			385mm x 255mm x 490mm (Modelo con rebobinado)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Rango autom&aacute;tico 100-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 11W en espera, 130W trabajando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Rebobinado (7010R), Cortador autom&aacute;tico, Separador</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios suministrados: cinta de muestra, n&uacute;cleo de cinta, sujetadores de cinta, l&aacute;piz para limpieza de cabezal, gu&iacute;a de inicio, CD: Manuales, Drivers y EasyConfig Tool.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 36),
(129, 'Caracteristicas IT 7010 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>10 ips (203 dpi) / 8 ips (300 spi)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Directo o transferencia t&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de Impresi&oacute;n</th>\r\n			<td>0.25&ldquo; a 32&ldquo; (6.4 mm a 812 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Modelos disponibles a 203 dpi / 300dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>fuente</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida Cabezal de Impresi&oacute;n</th>\r\n			<td>50 km o 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (IEEE-1284), Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta, papel/etiquetas t&eacute;rmicas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o papel</th>\r\n			<td>1.00&ldquo; a 4.65&ldquo; (25.4 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo del papel</th>\r\n			<td>0.25&ldquo; a 32.0&ldquo; (6.35 mm a 812.8)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>8&ldquo; (200 mm) di&aacute;metro externo. Tama&ntilde;o de n&uacute;cleo 1.5&ldquo; - 3&quot; (38 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero ajustable y sensor delantero de alta resoluci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cinta</th>\r\n			<td>Tinta interna o externa. Cera, Cera/Resina o tipo de resina</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de la Cinta</th>\r\n			<td>3.40 pulgadas (86.5 mm) maximo di&aacute;metro externo. 1476 ft (450 m) largo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de la Cinta</th>\r\n			<td>4.5 pulgadas (114.0 mm) max</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barras</th>\r\n			<td>C&oacute;digos de barras: Standard barcodes include: Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey, UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>13.3 kg, 17.6 kg (Modelo con rebobinado)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 265mm x 255mm x 490mm,&nbsp;<br />\r\n			385mm x 255mm x 490mm (Modelo con rebobinado)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Rango autom&aacute;tico 100-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 11W en espera, 130W trabajando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Rebobinado (7010R), Cortador autom&aacute;tico, Separador</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios suministrados: cinta de muestra, n&uacute;cleo de cinta, sujetadores de cinta, l&aacute;piz para limpieza de cabezal, gu&iacute;a de inicio, CD: Manuales, Drivers y EasyConfig Tool.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 36),
(130, 'Caracteristicas IT 7010 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>10 ips (203 dpi) / 8 ips (300 spi)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Directo o transferencia t&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de Impresi&oacute;n</th>\r\n			<td>0.25&ldquo; a 32&ldquo; (6.4 mm a 812 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Modelos disponibles a 203 dpi / 300dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>fuente</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida Cabezal de Impresi&oacute;n</th>\r\n			<td>50 km o 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (IEEE-1284), Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta, papel/etiquetas t&eacute;rmicas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o papel</th>\r\n			<td>1.00&ldquo; a 4.65&ldquo; (25.4 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo del papel</th>\r\n			<td>0.25&ldquo; a 32.0&ldquo; (6.35 mm a 812.8)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>8&ldquo; (200 mm) di&aacute;metro externo. Tama&ntilde;o de n&uacute;cleo 1.5&ldquo; - 3&quot; (38 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero ajustable y sensor delantero de alta resoluci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cinta</th>\r\n			<td>Tinta interna o externa. Cera, Cera/Resina o tipo de resina</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de la Cinta</th>\r\n			<td>3.40 pulgadas (86.5 mm) maximo di&aacute;metro externo. 1476 ft (450 m) largo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de la Cinta</th>\r\n			<td>4.5 pulgadas (114.0 mm) max</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barras</th>\r\n			<td>C&oacute;digos de barras: Standard barcodes include: Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey, UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>13.3 kg, 17.6 kg (Modelo con rebobinado)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 265mm x 255mm x 490mm,&nbsp;<br />\r\n			385mm x 255mm x 490mm (Modelo con rebobinado)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Rango autom&aacute;tico 100-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 11W en espera, 130W trabajando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Rebobinado (7010R), Cortador autom&aacute;tico, Separador</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios suministrados: cinta de muestra, n&uacute;cleo de cinta, sujetadores de cinta, l&aacute;piz para limpieza de cabezal, gu&iacute;a de inicio, CD: Manuales, Drivers y EasyConfig Tool.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 36),
(131, 'Caracteristicas IT 7010 ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>10 ips (203 dpi) / 8 ips (300 spi)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Directo o transferencia t&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de Impresi&oacute;n</th>\r\n			<td>0.25&ldquo; a 32&ldquo; (6.4 mm a 812 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Modelos disponibles a 203 dpi / 300dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>fuente</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida Cabezal de Impresi&oacute;n</th>\r\n			<td>50 km o 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Paralelo (IEEE-1284), Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta, papel/etiquetas t&eacute;rmicas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o papel</th>\r\n			<td>1.00&ldquo; a 4.65&ldquo; (25.4 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo del papel</th>\r\n			<td>0.25&ldquo; a 32.0&ldquo; (6.35 mm a 812.8)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>8&ldquo; (200 mm) di&aacute;metro externo. Tama&ntilde;o de n&uacute;cleo 1.5&ldquo; - 3&quot; (38 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero ajustable y sensor delantero de alta resoluci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cinta</th>\r\n			<td>Tinta interna o externa. Cera, Cera/Resina o tipo de resina</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de la Cinta</th>\r\n			<td>3.40 pulgadas (86.5 mm) maximo di&aacute;metro externo. 1476 ft (450 m) largo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de la Cinta</th>\r\n			<td>4.5 pulgadas (114.0 mm) max</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barras</th>\r\n			<td>C&oacute;digos de barras: Standard barcodes include: Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey, UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>13.3 kg, 17.6 kg (Modelo con rebobinado)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 265mm x 255mm x 490mm,&nbsp;<br />\r\n			385mm x 255mm x 490mm (Modelo con rebobinado)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Rango autom&aacute;tico 100-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 11W en espera, 130W trabajando</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Rebobinado (7010R), Cortador autom&aacute;tico, Separador</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios suministrados: cinta de muestra, n&uacute;cleo de cinta, sujetadores de cinta, l&aacute;piz para limpieza de cabezal, gu&iacute;a de inicio, CD: Manuales, Drivers y EasyConfig Tool.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 36),
(132, 'Descripcion IT 7206', 'La impresora térmica Tally Dascom 7206 brinda desempeño industrial en \r\nambiente de oficina.El mecanismo utiliza una construcción totalmente \r\nmetálica garantizando un funcionamiento fiable, una deber para todos los\r\n entornos industriales..<br>\r\n<br>\r\nEl diseño de uso fácil de las impresoras permite un ahorro de espacio \r\nvertical de apertura un mecanismode impresión fácil de usar asegura una \r\ncarga rápida y fácil de las etiquetas y cintas.El sistema de control \r\ninteligente de la cinta se detiene para evitar que la cinta se arrugue y\r\n obtener una calidad de impresión excelente.<br>\r\n<br>\r\nLa 7206 ofrece una amplia gama de características y opciones que por lo \r\ngeneral sólo se encuentran en equipos de alto rendimiento.La 7206 se \r\nintegra fácilmente en los sistemas existentes, tambien disponible con \r\nuna&nbsp; interfaz Ethernet .<br>\r\n<br>\r\nAdemás de esto, la 7206 también está disponible con opciones tales como \r\nun cortador, un separador para satisfacer las necesidades de la \r\nindustria.<br>\r\n<br>\r\nLa 7206-300 con una resolución de 300 dpi es ideal para imprimir etiquetas pequeñas con logotipos, imágenes y gráficos.<br>', 'product', 1, 'es', 'bo', 37),
(133, 'Descripcion IT 7206', 'La impresora térmica Tally Dascom 7206 brinda desempeño industrial en \r\nambiente de oficina.El mecanismo utiliza una construcción totalmente \r\nmetálica garantizando un funcionamiento fiable, una deber para todos los\r\n entornos industriales..<br>\r\n<br>\r\nEl diseño de uso fácil de las impresoras permite un ahorro de espacio \r\nvertical de apertura un mecanismode impresión fácil de usar asegura una \r\ncarga rápida y fácil de las etiquetas y cintas.El sistema de control \r\ninteligente de la cinta se detiene para evitar que la cinta se arrugue y\r\n obtener una calidad de impresión excelente.<br>\r\n<br>\r\nLa 7206 ofrece una amplia gama de características y opciones que por lo \r\ngeneral sólo se encuentran en equipos de alto rendimiento.La 7206 se \r\nintegra fácilmente en los sistemas existentes, tambien disponible con \r\nuna&nbsp; interfaz Ethernet .<br>\r\n<br>\r\nAdemás de esto, la 7206 también está disponible con opciones tales como \r\nun cortador, un separador para satisfacer las necesidades de la \r\nindustria.<br>\r\n<br>\r\nLa 7206-300 con una resolución de 300 dpi es ideal para imprimir etiquetas pequeñas con logotipos, imágenes y gráficos.<br>', 'product', 1, 'es', 'co', 37),
(134, 'Descripcion IT 7206', 'La impresora térmica Tally Dascom 7206 brinda desempeño industrial en \r\nambiente de oficina.El mecanismo utiliza una construcción totalmente \r\nmetálica garantizando un funcionamiento fiable, una deber para todos los\r\n entornos industriales..<br>\r\n<br>\r\nEl diseño de uso fácil de las impresoras permite un ahorro de espacio \r\nvertical de apertura un mecanismode impresión fácil de usar asegura una \r\ncarga rápida y fácil de las etiquetas y cintas.El sistema de control \r\ninteligente de la cinta se detiene para evitar que la cinta se arrugue y\r\n obtener una calidad de impresión excelente.<br>\r\n<br>\r\nLa 7206 ofrece una amplia gama de características y opciones que por lo \r\ngeneral sólo se encuentran en equipos de alto rendimiento.La 7206 se \r\nintegra fácilmente en los sistemas existentes, tambien disponible con \r\nuna&nbsp; interfaz Ethernet .<br>\r\n<br>\r\nAdemás de esto, la 7206 también está disponible con opciones tales como \r\nun cortador, un separador para satisfacer las necesidades de la \r\nindustria.<br>\r\n<br>\r\nLa 7206-300 con una resolución de 300 dpi es ideal para imprimir etiquetas pequeñas con logotipos, imágenes y gráficos.<br>', 'product', 1, 'es', 'mx', 37),
(135, 'Descripcion IT 7206', 'La impresora térmica Tally Dascom 7206 brinda desempeño industrial en \r\nambiente de oficina.El mecanismo utiliza una construcción totalmente \r\nmetálica garantizando un funcionamiento fiable, una deber para todos los\r\n entornos industriales..<br>\r\n<br>\r\nEl diseño de uso fácil de las impresoras permite un ahorro de espacio \r\nvertical de apertura un mecanismode impresión fácil de usar asegura una \r\ncarga rápida y fácil de las etiquetas y cintas.El sistema de control \r\ninteligente de la cinta se detiene para evitar que la cinta se arrugue y\r\n obtener una calidad de impresión excelente.<br>\r\n<br>\r\nLa 7206 ofrece una amplia gama de características y opciones que por lo \r\ngeneral sólo se encuentran en equipos de alto rendimiento.La 7206 se \r\nintegra fácilmente en los sistemas existentes, tambien disponible con \r\nuna&nbsp; interfaz Ethernet .<br>\r\n<br>\r\nAdemás de esto, la 7206 también está disponible con opciones tales como \r\nun cortador, un separador para satisfacer las necesidades de la \r\nindustria.<br>\r\n<br>\r\nLa 7206-300 con una resolución de 300 dpi es ideal para imprimir etiquetas pequeñas con logotipos, imágenes y gráficos.<br>', 'product', 1, 'es', 'pe', 37),
(136, 'Caracteristicas IT 7206', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>6 ips</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Transferencia t&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo impresi&oacute;n</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi / 300dpi modelo disponible</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Posici&oacute;n de impresi&oacute;n</th>\r\n			<td>&plusmn; 0.0625&quot; (1.66mm) Tolerancia de izquierda a derecha</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuente</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal</th>\r\n			<td>50 km &oacute; 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT), Paralelo (IEEE-1284)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta, papel t&eacute;rmico/etiquetas,</td>\r\n		</tr>\r\n		<tr>\r\n			<th>&nbsp;</th>\r\n			<td>0.77&ldquo;a 4.65&ldquo; (19.6 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de papel</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>Internamente: 5&ldquo; (125 mm) . Externamente 8&ldquo; con sujetador opcional.<br />\r\n			Tama&ntilde;o del n&uacute;cleo 1&ldquo; - 3&quot; (25 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero ajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de cinta</th>\r\n			<td>Tinta del lado interno y externo. Cera, Cera/Resina</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de la cinta</th>\r\n			<td>2.9&ldquo; (74 mm) di&aacute;metro maximo, 360m largo, 1&ldquo; nucleos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digos de barra</th>\r\n			<td>Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey,UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>4.5 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 270mm x 231mm x 289mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>220-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 13W en espera, 159W en funcionamiento</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cortador autom&aacute;tico, Separador, Interfaces Paralelas y LAN</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios suministrados: cinta de muestra, n&uacute;cleo de cinta, sujetadores de cinta, l&aacute;piz para limpieza de cabezal, gu&iacute;a de inicio, CD: Manuales, Drivers y EasyConfig Tool.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 37),
(137, 'Caracteristicas IT 7206', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>6 ips</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Transferencia t&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo impresi&oacute;n</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi / 300dpi modelo disponible</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Posici&oacute;n de impresi&oacute;n</th>\r\n			<td>&plusmn; 0.0625&quot; (1.66mm) Tolerancia de izquierda a derecha</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuente</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal</th>\r\n			<td>50 km &oacute; 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT), Paralelo (IEEE-1284)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta, papel t&eacute;rmico/etiquetas,</td>\r\n		</tr>\r\n		<tr>\r\n			<th>&nbsp;</th>\r\n			<td>0.77&ldquo;a 4.65&ldquo; (19.6 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de papel</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>Internamente: 5&ldquo; (125 mm) . Externamente 8&ldquo; con sujetador opcional.<br />\r\n			Tama&ntilde;o del n&uacute;cleo 1&ldquo; - 3&quot; (25 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero ajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de cinta</th>\r\n			<td>Tinta del lado interno y externo. Cera, Cera/Resina</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de la cinta</th>\r\n			<td>2.9&ldquo; (74 mm) di&aacute;metro maximo, 360m largo, 1&ldquo; nucleos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digos de barra</th>\r\n			<td>Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey,UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>4.5 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 270mm x 231mm x 289mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>220-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 13W en espera, 159W en funcionamiento</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cortador autom&aacute;tico, Separador, Interfaces Paralelas y LAN</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios suministrados: cinta de muestra, n&uacute;cleo de cinta, sujetadores de cinta, l&aacute;piz para limpieza de cabezal, gu&iacute;a de inicio, CD: Manuales, Drivers y EasyConfig Tool.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 37),
(138, 'Caracteristicas IT 7206', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>6 ips</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Transferencia t&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo impresi&oacute;n</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi / 300dpi modelo disponible</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Posici&oacute;n de impresi&oacute;n</th>\r\n			<td>&plusmn; 0.0625&quot; (1.66mm) Tolerancia de izquierda a derecha</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuente</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal</th>\r\n			<td>50 km &oacute; 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT), Paralelo (IEEE-1284)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta, papel t&eacute;rmico/etiquetas,</td>\r\n		</tr>\r\n		<tr>\r\n			<th>&nbsp;</th>\r\n			<td>0.77&ldquo;a 4.65&ldquo; (19.6 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de papel</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>Internamente: 5&ldquo; (125 mm) . Externamente 8&ldquo; con sujetador opcional.<br />\r\n			Tama&ntilde;o del n&uacute;cleo 1&ldquo; - 3&quot; (25 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero ajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de cinta</th>\r\n			<td>Tinta del lado interno y externo. Cera, Cera/Resina</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de la cinta</th>\r\n			<td>2.9&ldquo; (74 mm) di&aacute;metro maximo, 360m largo, 1&ldquo; nucleos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digos de barra</th>\r\n			<td>Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey,UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>4.5 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 270mm x 231mm x 289mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>220-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 13W en espera, 159W en funcionamiento</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cortador autom&aacute;tico, Separador, Interfaces Paralelas y LAN</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios suministrados: cinta de muestra, n&uacute;cleo de cinta, sujetadores de cinta, l&aacute;piz para limpieza de cabezal, gu&iacute;a de inicio, CD: Manuales, Drivers y EasyConfig Tool.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 37),
(139, 'Caracteristicas IT 7206', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de impresi&oacute;n</th>\r\n			<td>6 ips</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Transferencia t&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo impresi&oacute;n</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi / 300dpi modelo disponible</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Posici&oacute;n de impresi&oacute;n</th>\r\n			<td>&plusmn; 0.0625&quot; (1.66mm) Tolerancia de izquierda a derecha</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuente</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del cabezal</th>\r\n			<td>50 km &oacute; 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT), Paralelo (IEEE-1284)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta, papel t&eacute;rmico/etiquetas,</td>\r\n		</tr>\r\n		<tr>\r\n			<th>&nbsp;</th>\r\n			<td>0.77&ldquo;a 4.65&ldquo; (19.6 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de papel</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>Internamente: 5&ldquo; (125 mm) . Externamente 8&ldquo; con sujetador opcional.<br />\r\n			Tama&ntilde;o del n&uacute;cleo 1&ldquo; - 3&quot; (25 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero ajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de cinta</th>\r\n			<td>Tinta del lado interno y externo. Cera, Cera/Resina</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de la cinta</th>\r\n			<td>2.9&ldquo; (74 mm) di&aacute;metro maximo, 360m largo, 1&ldquo; nucleos</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digos de barra</th>\r\n			<td>Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey,UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>4.5 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 270mm x 231mm x 289mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>220-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 13W en espera, 159W en funcionamiento</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cortador autom&aacute;tico, Separador, Interfaces Paralelas y LAN</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios suministrados: cinta de muestra, n&uacute;cleo de cinta, sujetadores de cinta, l&aacute;piz para limpieza de cabezal, gu&iacute;a de inicio, CD: Manuales, Drivers y EasyConfig Tool.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 37),
(140, 'Descripcion IT 7106', '<span>La impresora \r\ntérmica Dascom 7106 brinda confiabilidad, precision y desempeño en un \r\ntamaño para escritorio, donde el espacio es limitado.<br>\r\n<br>\r\nEl diseño de uso fácil de la impresora permite un ahorro de espacio de \r\napertura vertical y un mecanismo de impresión fácil de usar asegura una \r\ncarga rápida de papel.El panel transparente permite un funcionamiento \r\nsimple y eficaz por lo que es muy sencillo de utilizar.<br>\r\n<br>\r\nEl mecanismo utiliza una construcción totalmente metálica garantizando \r\nun funcionamiento fiable, una deber para todos los entornos \r\nindustriales.<br>\r\n<br>\r\nLa 7106 ofrece una amplia gama de características y opciones que por lo \r\ngeneral sólo se encuentran en equipos de alto rendimiento. Por ejemplo, \r\nse tieneuna fuente de alimentación interna que proporciona un \r\nfuncionamiento más fiable evitando los problemas normalmente asociados \r\ncon fuentes de alimentación externas.<br>\r\n<br>\r\nLa 7106 se integra fácilmente en los sistemas existentes, tambien \r\ndisponible con una&nbsp; interfaz Ethernet . Además de esto, la 7106 también \r\nestá disponible con opcionestales como un cortador un separador para \r\nsatisfacer las necesidades de la industria.</span><br>', 'product', 1, 'es', 'bo', 38),
(141, 'Descripcion IT 7106', '<span>La impresora \r\ntérmica Dascom 7106 brinda confiabilidad, precision y desempeño en un \r\ntamaño para escritorio, donde el espacio es limitado.<br>\r\n<br>\r\nEl diseño de uso fácil de la impresora permite un ahorro de espacio de \r\napertura vertical y un mecanismo de impresión fácil de usar asegura una \r\ncarga rápida de papel.El panel transparente permite un funcionamiento \r\nsimple y eficaz por lo que es muy sencillo de utilizar.<br>\r\n<br>\r\nEl mecanismo utiliza una construcción totalmente metálica garantizando \r\nun funcionamiento fiable, una deber para todos los entornos \r\nindustriales.<br>\r\n<br>\r\nLa 7106 ofrece una amplia gama de características y opciones que por lo \r\ngeneral sólo se encuentran en equipos de alto rendimiento. Por ejemplo, \r\nse tieneuna fuente de alimentación interna que proporciona un \r\nfuncionamiento más fiable evitando los problemas normalmente asociados \r\ncon fuentes de alimentación externas.<br>\r\n<br>\r\nLa 7106 se integra fácilmente en los sistemas existentes, tambien \r\ndisponible con una&nbsp; interfaz Ethernet . Además de esto, la 7106 también \r\nestá disponible con opcionestales como un cortador un separador para \r\nsatisfacer las necesidades de la industria.</span><br>', 'product', 1, 'es', 'co', 38),
(142, 'Descripcion IT 7106', '<span>La impresora \r\ntérmica Dascom 7106 brinda confiabilidad, precision y desempeño en un \r\ntamaño para escritorio, donde el espacio es limitado.<br>\r\n<br>\r\nEl diseño de uso fácil de la impresora permite un ahorro de espacio de \r\napertura vertical y un mecanismo de impresión fácil de usar asegura una \r\ncarga rápida de papel.El panel transparente permite un funcionamiento \r\nsimple y eficaz por lo que es muy sencillo de utilizar.<br>\r\n<br>\r\nEl mecanismo utiliza una construcción totalmente metálica garantizando \r\nun funcionamiento fiable, una deber para todos los entornos \r\nindustriales.<br>\r\n<br>\r\nLa 7106 ofrece una amplia gama de características y opciones que por lo \r\ngeneral sólo se encuentran en equipos de alto rendimiento. Por ejemplo, \r\nse tieneuna fuente de alimentación interna que proporciona un \r\nfuncionamiento más fiable evitando los problemas normalmente asociados \r\ncon fuentes de alimentación externas.<br>\r\n<br>\r\nLa 7106 se integra fácilmente en los sistemas existentes, tambien \r\ndisponible con una&nbsp; interfaz Ethernet . Además de esto, la 7106 también \r\nestá disponible con opcionestales como un cortador un separador para \r\nsatisfacer las necesidades de la industria.</span><br>', 'product', 1, 'es', 'mx', 38),
(143, 'Descripcion IT 7106', '<span>La impresora \r\ntérmica Dascom 7106 brinda confiabilidad, precision y desempeño en un \r\ntamaño para escritorio, donde el espacio es limitado.<br>\r\n<br>\r\nEl diseño de uso fácil de la impresora permite un ahorro de espacio de \r\napertura vertical y un mecanismo de impresión fácil de usar asegura una \r\ncarga rápida de papel.El panel transparente permite un funcionamiento \r\nsimple y eficaz por lo que es muy sencillo de utilizar.<br>\r\n<br>\r\nEl mecanismo utiliza una construcción totalmente metálica garantizando \r\nun funcionamiento fiable, una deber para todos los entornos \r\nindustriales.<br>\r\n<br>\r\nLa 7106 ofrece una amplia gama de características y opciones que por lo \r\ngeneral sólo se encuentran en equipos de alto rendimiento. Por ejemplo, \r\nse tieneuna fuente de alimentación interna que proporciona un \r\nfuncionamiento más fiable evitando los problemas normalmente asociados \r\ncon fuentes de alimentación externas.<br>\r\n<br>\r\nLa 7106 se integra fácilmente en los sistemas existentes, tambien \r\ndisponible con una&nbsp; interfaz Ethernet . Además de esto, la 7106 también \r\nestá disponible con opcionestales como un cortador un separador para \r\nsatisfacer las necesidades de la industria.</span><br>', 'product', 1, 'es', 'pe', 38),
(144, 'Caracteristicas  IT 7106', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>6 ips</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Transferencia Directa</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de Impresi&oacute;n</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>50 km &oacute; 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT), Paralelo (IEEE-1284)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Papel t&eacute;rmico y etiquetas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>0.77&ldquo;a 4.65&ldquo; (19.6 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo del Papel</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>Imternamente: 5&ldquo; (125 mm) . Externamente 8&ldquo; Con sostenedor opcional. Tama&ntilde;o del n&uacute;cleo 1&ldquo;-3&quot; (25 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero completamente ajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digos de barra</th>\r\n			<td>Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey,UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>3.6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 183mm x 231mm x 289mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>220-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 12.9W en espera, 196W en operaci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cortador autom&aacute;tico, Separador, Interfaces Paralelas y LAN</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios incluidos: l&aacute;piz para limpieza de cabezal , Gu&iacute;a de inicio, CD con: Manuales, Drivers y EasyConfig Tool</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 38);
INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(145, 'Caracteristicas  IT 7106', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>6 ips</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Transferencia Directa</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de Impresi&oacute;n</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>50 km &oacute; 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT), Paralelo (IEEE-1284)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Papel t&eacute;rmico y etiquetas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>0.77&ldquo;a 4.65&ldquo; (19.6 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo del Papel</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>Imternamente: 5&ldquo; (125 mm) . Externamente 8&ldquo; Con sostenedor opcional. Tama&ntilde;o del n&uacute;cleo 1&ldquo;-3&quot; (25 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero completamente ajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digos de barra</th>\r\n			<td>Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey,UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>3.6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 183mm x 231mm x 289mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>220-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 12.9W en espera, 196W en operaci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cortador autom&aacute;tico, Separador, Interfaces Paralelas y LAN</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios incluidos: l&aacute;piz para limpieza de cabezal , Gu&iacute;a de inicio, CD con: Manuales, Drivers y EasyConfig Tool</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 38),
(146, 'Caracteristicas  IT 7106', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>6 ips</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Transferencia Directa</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de Impresi&oacute;n</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>50 km &oacute; 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT), Paralelo (IEEE-1284)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Papel t&eacute;rmico y etiquetas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>0.77&ldquo;a 4.65&ldquo; (19.6 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo del Papel</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>Imternamente: 5&ldquo; (125 mm) . Externamente 8&ldquo; Con sostenedor opcional. Tama&ntilde;o del n&uacute;cleo 1&ldquo;-3&quot; (25 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero completamente ajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digos de barra</th>\r\n			<td>Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey,UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>3.6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 183mm x 231mm x 289mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>220-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 12.9W en espera, 196W en operaci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cortador autom&aacute;tico, Separador, Interfaces Paralelas y LAN</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios incluidos: l&aacute;piz para limpieza de cabezal , Gu&iacute;a de inicio, CD con: Manuales, Drivers y EasyConfig Tool</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 38),
(147, 'Caracteristicas  IT 7106', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">Velocidad de Impresi&oacute;n</th>\r\n			<td>6 ips</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>4.10 pulgadas, 104.1 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Transferencia Directa</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo de Impresi&oacute;n</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>203 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>7 styles, True Type</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>50 km &oacute; 6 meses</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>16 Mb SDRAM; 4 Mb Flash</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial (RS-232C), USB (Versi&oacute;n 1.1)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad Opcional</th>\r\n			<td>Ethernet LAN (10/100 BaseT), Paralelo (IEEE-1284)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>Cambio autom&aacute;tico Zebra ZPL-II y Datamax I-Class, DMX400, Prodigy Plus</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Papel t&eacute;rmico y etiquetas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>0.77&ldquo;a 4.65&ldquo; (19.6 mm a 118.1 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Largo del Papel</th>\r\n			<td>0.25&ldquo;a 32&ldquo; (6.35mm a 812.8 mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Di&aacute;metro del rollo</th>\r\n			<td>Imternamente: 5&ldquo; (125 mm) . Externamente 8&ldquo; Con sostenedor opcional. Tama&ntilde;o del n&uacute;cleo 1&ldquo;-3&quot; (25 mm - 76 mm).</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Indexado</th>\r\n			<td>Sensor trasero completamente ajustable</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digos de barra</th>\r\n			<td>Code11. Code39, Code93, Code128, I2of5, S2of5, EAN8, EAN13, ANSI CODABAR, LOGMARS, MSI, Plessey,UPC A/E, POSTNET, Planet, Code49, PDF417, CODABLOCK, Maxicode, DataMatrix, QR Code, RSS, TLC39</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>3.6 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td>(HxWxD): 183mm x 231mm x 289mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>220-240VAC, 50-60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumo</th>\r\n			<td>max 12.9W en espera, 196W en operaci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2003, Vista, Win 7 (32/64 Bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>+5&deg;C a +35&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Humedad</th>\r\n			<td>30% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ac&uacute;stica</th>\r\n			<td>&lt;55 dB(A) (ISO 7779)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Opciones</th>\r\n			<td>Cortador autom&aacute;tico, Separador, Interfaces Paralelas y LAN</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nota</th>\r\n			<td>Accesorios incluidos: l&aacute;piz para limpieza de cabezal , Gu&iacute;a de inicio, CD con: Manuales, Drivers y EasyConfig Tool</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 38),
(148, 'Descripcion MIP-480', 'La MIP480 es una impresora móvil de \r\nimpacto, diseñada para la entrega de facturas, notas de venta y otros \r\ndocumentos impresos en camionetas y camiones comerciales de reparto y \r\nentrega en sitio. La compatibilidad con sistemas diseñados para \r\ndispositivos móviles (handhelds) utilizados por vendedores y \r\nrepartidores es completa. Con alimentación nominal de 12 – 24V, es ideal\r\n para la operación en camiones donde puede ser conectada a la batería \r\ndel vehículo con un adaptador de corriente disponible. La combinación de\r\n flexibilidad, velocidad y operación confiable, da como resultado una \r\nexcepcional impresora ideal para rutas de entregas y otras aplicaciones \r\nmóviles. Su resistencia está asegurada por su MTBF de 15.000 horas y 2 \r\nmillones de caracteres de la cabeza de impresión. La vida de las cintas \r\npuede ser de hasta seis meses en la impresión de documentos estándar de \r\nentrega en ruta.\r\n\r\nLa impresora móvil MIP480 es la mejor \r\nopción para impresiónde facturas en las industrias de alimentos y \r\nbebidas, venta al mayoreo, servicios de emergencia, manejo de residuos y\r\n materiales peligrosos, seguridad pública y otras que necesiten \r\ndispositivos confiables en la automatización de las rutas de entrega y \r\nen la parte final de su operación en línea. La impresora móvil MIP480 \r\npuede imprimir hasta 480 caracteres por segundo (400 cps a 10 caracteres\r\n por pulgada) y su cabeza de impresión de 24 agujas imprime texto claro y\r\n gráficos en formatos de hasta un original y 3 copias en formas \r\ncontinuas. La impresora es compatible con las emulaciones más \r\nimportantes incluyendo IBM® Proprinter® X24E, EPSON ® ESC/P2 e Intermec \r\n6920. La configuración estándar incluye conectividad serial RS-232, \r\nParalelo, USB 2.0 y Bluetooth.<br>', 'product', 1, 'es', 'bo', 39),
(149, 'Descripcion MIP-480', 'La MIP480 es una impresora móvil de \r\nimpacto, diseñada para la entrega de facturas, notas de venta y otros \r\ndocumentos impresos en camionetas y camiones comerciales de reparto y \r\nentrega en sitio. La compatibilidad con sistemas diseñados para \r\ndispositivos móviles (handhelds) utilizados por vendedores y \r\nrepartidores es completa. Con alimentación nominal de 12 – 24V, es ideal\r\n para la operación en camiones donde puede ser conectada a la batería \r\ndel vehículo con un adaptador de corriente disponible. La combinación de\r\n flexibilidad, velocidad y operación confiable, da como resultado una \r\nexcepcional impresora ideal para rutas de entregas y otras aplicaciones \r\nmóviles. Su resistencia está asegurada por su MTBF de 15.000 horas y 2 \r\nmillones de caracteres de la cabeza de impresión. La vida de las cintas \r\npuede ser de hasta seis meses en la impresión de documentos estándar de \r\nentrega en ruta.\r\n\r\nLa impresora móvil MIP480 es la mejor \r\nopción para impresiónde facturas en las industrias de alimentos y \r\nbebidas, venta al mayoreo, servicios de emergencia, manejo de residuos y\r\n materiales peligrosos, seguridad pública y otras que necesiten \r\ndispositivos confiables en la automatización de las rutas de entrega y \r\nen la parte final de su operación en línea. La impresora móvil MIP480 \r\npuede imprimir hasta 480 caracteres por segundo (400 cps a 10 caracteres\r\n por pulgada) y su cabeza de impresión de 24 agujas imprime texto claro y\r\n gráficos en formatos de hasta un original y 3 copias en formas \r\ncontinuas. La impresora es compatible con las emulaciones más \r\nimportantes incluyendo IBM® Proprinter® X24E, EPSON ® ESC/P2 e Intermec \r\n6920. La configuración estándar incluye conectividad serial RS-232, \r\nParalelo, USB 2.0 y Bluetooth.<br>', 'product', 1, 'es', 'co', 39),
(150, 'Descripcion MIP-480', 'La MIP480 es una impresora móvil de \r\nimpacto, diseñada para la entrega de facturas, notas de venta y otros \r\ndocumentos impresos en camionetas y camiones comerciales de reparto y \r\nentrega en sitio. La compatibilidad con sistemas diseñados para \r\ndispositivos móviles (handhelds) utilizados por vendedores y \r\nrepartidores es completa. Con alimentación nominal de 12 – 24V, es ideal\r\n para la operación en camiones donde puede ser conectada a la batería \r\ndel vehículo con un adaptador de corriente disponible. La combinación de\r\n flexibilidad, velocidad y operación confiable, da como resultado una \r\nexcepcional impresora ideal para rutas de entregas y otras aplicaciones \r\nmóviles. Su resistencia está asegurada por su MTBF de 15.000 horas y 2 \r\nmillones de caracteres de la cabeza de impresión. La vida de las cintas \r\npuede ser de hasta seis meses en la impresión de documentos estándar de \r\nentrega en ruta.\r\n\r\nLa impresora móvil MIP480 es la mejor \r\nopción para impresiónde facturas en las industrias de alimentos y \r\nbebidas, venta al mayoreo, servicios de emergencia, manejo de residuos y\r\n materiales peligrosos, seguridad pública y otras que necesiten \r\ndispositivos confiables en la automatización de las rutas de entrega y \r\nen la parte final de su operación en línea. La impresora móvil MIP480 \r\npuede imprimir hasta 480 caracteres por segundo (400 cps a 10 caracteres\r\n por pulgada) y su cabeza de impresión de 24 agujas imprime texto claro y\r\n gráficos en formatos de hasta un original y 3 copias en formas \r\ncontinuas. La impresora es compatible con las emulaciones más \r\nimportantes incluyendo IBM® Proprinter® X24E, EPSON ® ESC/P2 e Intermec \r\n6920. La configuración estándar incluye conectividad serial RS-232, \r\nParalelo, USB 2.0 y Bluetooth.<br>', 'product', 1, 'es', 'mx', 39),
(151, 'Descripcion MIP-480', 'La MIP480 es una impresora móvil de \r\nimpacto, diseñada para la entrega de facturas, notas de venta y otros \r\ndocumentos impresos en camionetas y camiones comerciales de reparto y \r\nentrega en sitio. La compatibilidad con sistemas diseñados para \r\ndispositivos móviles (handhelds) utilizados por vendedores y \r\nrepartidores es completa. Con alimentación nominal de 12 – 24V, es ideal\r\n para la operación en camiones donde puede ser conectada a la batería \r\ndel vehículo con un adaptador de corriente disponible. La combinación de\r\n flexibilidad, velocidad y operación confiable, da como resultado una \r\nexcepcional impresora ideal para rutas de entregas y otras aplicaciones \r\nmóviles. Su resistencia está asegurada por su MTBF de 15.000 horas y 2 \r\nmillones de caracteres de la cabeza de impresión. La vida de las cintas \r\npuede ser de hasta seis meses en la impresión de documentos estándar de \r\nentrega en ruta.\r\n\r\nLa impresora móvil MIP480 es la mejor \r\nopción para impresiónde facturas en las industrias de alimentos y \r\nbebidas, venta al mayoreo, servicios de emergencia, manejo de residuos y\r\n materiales peligrosos, seguridad pública y otras que necesiten \r\ndispositivos confiables en la automatización de las rutas de entrega y \r\nen la parte final de su operación en línea. La impresora móvil MIP480 \r\npuede imprimir hasta 480 caracteres por segundo (400 cps a 10 caracteres\r\n por pulgada) y su cabeza de impresión de 24 agujas imprime texto claro y\r\n gráficos en formatos de hasta un original y 3 copias en formas \r\ncontinuas. La impresora es compatible con las emulaciones más \r\nimportantes incluyendo IBM® Proprinter® X24E, EPSON ® ESC/P2 e Intermec \r\n6920. La configuración estándar incluye conectividad serial RS-232, \r\nParalelo, USB 2.0 y Bluetooth.<br>', 'product', 1, 'es', 'pe', 39),
(152, 'Caracteristicas MIP-480', '\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px"><p>Velocidad de Impresi&oacute;n</p></th>\r\n			<td>(caracteres por segundo a 10 caracteres por pulgada): 400cps (Calidad borrador r&aacute;pido); 133cps (Calidad carta rapida). Velocidad de cambio de linea: 6 pulgadas por segundo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Matriz serial de impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>180 x 120 dpi Borrador, 180 x 360 dpi Carta, 1 / 360&rdquo; todos los puntos direccionables</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Modos de Caracteres</th>\r\n			<td>Ancho doble, altura doble, doble golpe, Italic</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Juego de caracteres</th>\r\n			<td>C&oacute;digo de p&aacute;gina 437, 850, 860, 863, 865, 851, 852, 853, 855, 857, 866, USSR Gost, 864, 437G, 920, 858, 925. DIN como se aplica a OCR-A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20, 24 cpi (depende de la emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>3, 4, 5, 6, 8</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>250 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>64Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial RS232, USB 2.0, Bluetooth</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>IBM 2390+ (includes Pro printer XL24E), Epson ESC/P2, Intermec 6820</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Roman, Sans Serif, Courier, Bold, Prestige, Script, Orator, Gothic, OCR-A, OCR-B.C&oacute;digos de Barra: UPC-A, UPC-E, EAN8,EAN13, Code 39, Code 128, Interleaved 2/5, Industrial 2/5</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 4 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Deposito de papel, posicionamiento autom&aacute;tico de formularios continuos cero arrancar. Tractor con plataformas de tractores especiales para carga de papel r&aacute;pida, alimentaci&oacute;n por fricci&oacute;n platina para hojas sueltas.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>original + 3</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas continuas y sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>Aprox. 14 lbs.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>Ancho hoja continua &ndash; 4.01-10.51 pulgadas, Ancho hoja suelta &ndash; 4.01-10.51 pulgadas,Largo &ndash; 2.99-14.33 pulgadas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>Aprox. 14 lbs.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td style="text-align:left">(H x W x D) 140 x 365 x 263 mm (Aprox. 5.5 X 14.4 X 10.3 pulgadas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Nominal 12-24V DC alimentado desde el vehiculo o con bateria externa.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Frecuencia</th>\r\n			<td>Adaptador AC: 100-240 VAC, 50-60Hz (opcional)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conformidad</th>\r\n			<td>CA C22.2 No. 950; EN60950; GS Mark; UL 746C; CE-Mark; E-Mark; Directivas de EU</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>15000 Horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2000</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>57 dB(A)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o para devolver a deposito</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 39),
(153, 'Caracteristicas MIP-480', '\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px"><p>Velocidad de Impresi&oacute;n</p></th>\r\n			<td>(caracteres por segundo a 10 caracteres por pulgada): 400cps (Calidad borrador r&aacute;pido); 133cps (Calidad carta rapida). Velocidad de cambio de linea: 6 pulgadas por segundo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Matriz serial de impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>180 x 120 dpi Borrador, 180 x 360 dpi Carta, 1 / 360&rdquo; todos los puntos direccionables</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Modos de Caracteres</th>\r\n			<td>Ancho doble, altura doble, doble golpe, Italic</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Juego de caracteres</th>\r\n			<td>C&oacute;digo de p&aacute;gina 437, 850, 860, 863, 865, 851, 852, 853, 855, 857, 866, USSR Gost, 864, 437G, 920, 858, 925. DIN como se aplica a OCR-A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20, 24 cpi (depende de la emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>3, 4, 5, 6, 8</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>250 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>64Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial RS232, USB 2.0, Bluetooth</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>IBM 2390+ (includes Pro printer XL24E), Epson ESC/P2, Intermec 6820</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Roman, Sans Serif, Courier, Bold, Prestige, Script, Orator, Gothic, OCR-A, OCR-B.C&oacute;digos de Barra: UPC-A, UPC-E, EAN8,EAN13, Code 39, Code 128, Interleaved 2/5, Industrial 2/5</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 4 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Deposito de papel, posicionamiento autom&aacute;tico de formularios continuos cero arrancar. Tractor con plataformas de tractores especiales para carga de papel r&aacute;pida, alimentaci&oacute;n por fricci&oacute;n platina para hojas sueltas.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>original + 3</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas continuas y sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>Aprox. 14 lbs.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>Ancho hoja continua &ndash; 4.01-10.51 pulgadas, Ancho hoja suelta &ndash; 4.01-10.51 pulgadas,Largo &ndash; 2.99-14.33 pulgadas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>Aprox. 14 lbs.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td style="text-align:left">(H x W x D) 140 x 365 x 263 mm (Aprox. 5.5 X 14.4 X 10.3 pulgadas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Nominal 12-24V DC alimentado desde el vehiculo o con bateria externa.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Frecuencia</th>\r\n			<td>Adaptador AC: 100-240 VAC, 50-60Hz (opcional)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conformidad</th>\r\n			<td>CA C22.2 No. 950; EN60950; GS Mark; UL 746C; CE-Mark; E-Mark; Directivas de EU</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>15000 Horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2000</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>57 dB(A)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o para devolver a deposito</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 39),
(154, 'Caracteristicas MIP-480', '\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px"><p>Velocidad de Impresi&oacute;n</p></th>\r\n			<td>(caracteres por segundo a 10 caracteres por pulgada): 400cps (Calidad borrador r&aacute;pido); 133cps (Calidad carta rapida). Velocidad de cambio de linea: 6 pulgadas por segundo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Matriz serial de impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>180 x 120 dpi Borrador, 180 x 360 dpi Carta, 1 / 360&rdquo; todos los puntos direccionables</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Modos de Caracteres</th>\r\n			<td>Ancho doble, altura doble, doble golpe, Italic</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Juego de caracteres</th>\r\n			<td>C&oacute;digo de p&aacute;gina 437, 850, 860, 863, 865, 851, 852, 853, 855, 857, 866, USSR Gost, 864, 437G, 920, 858, 925. DIN como se aplica a OCR-A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20, 24 cpi (depende de la emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>3, 4, 5, 6, 8</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>250 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>64Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial RS232, USB 2.0, Bluetooth</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>IBM 2390+ (includes Pro printer XL24E), Epson ESC/P2, Intermec 6820</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Roman, Sans Serif, Courier, Bold, Prestige, Script, Orator, Gothic, OCR-A, OCR-B.C&oacute;digos de Barra: UPC-A, UPC-E, EAN8,EAN13, Code 39, Code 128, Interleaved 2/5, Industrial 2/5</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 4 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Deposito de papel, posicionamiento autom&aacute;tico de formularios continuos cero arrancar. Tractor con plataformas de tractores especiales para carga de papel r&aacute;pida, alimentaci&oacute;n por fricci&oacute;n platina para hojas sueltas.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>original + 3</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas continuas y sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>Aprox. 14 lbs.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>Ancho hoja continua &ndash; 4.01-10.51 pulgadas, Ancho hoja suelta &ndash; 4.01-10.51 pulgadas,Largo &ndash; 2.99-14.33 pulgadas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>Aprox. 14 lbs.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td style="text-align:left">(H x W x D) 140 x 365 x 263 mm (Aprox. 5.5 X 14.4 X 10.3 pulgadas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Nominal 12-24V DC alimentado desde el vehiculo o con bateria externa.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Frecuencia</th>\r\n			<td>Adaptador AC: 100-240 VAC, 50-60Hz (opcional)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conformidad</th>\r\n			<td>CA C22.2 No. 950; EN60950; GS Mark; UL 746C; CE-Mark; E-Mark; Directivas de EU</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>15000 Horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2000</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>57 dB(A)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o para devolver a deposito</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'mx', 39),
(155, 'Caracteristicas MIP-480', '\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px"><p>Velocidad de Impresi&oacute;n</p></th>\r\n			<td>(caracteres por segundo a 10 caracteres por pulgada): 400cps (Calidad borrador r&aacute;pido); 133cps (Calidad carta rapida). Velocidad de cambio de linea: 6 pulgadas por segundo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Matriz serial de impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>180 x 120 dpi Borrador, 180 x 360 dpi Carta, 1 / 360&rdquo; todos los puntos direccionables</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Modos de Caracteres</th>\r\n			<td>Ancho doble, altura doble, doble golpe, Italic</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Juego de caracteres</th>\r\n			<td>C&oacute;digo de p&aacute;gina 437, 850, 860, 863, 865, 851, 852, 853, 855, 857, 866, USSR Gost, 864, 437G, 920, 858, 925. DIN como se aplica a OCR-A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de caracteres</th>\r\n			<td>10, 12, 15, 17.1, 20, 24 cpi (depende de la emulaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Densidad de Linea</th>\r\n			<td>3, 4, 5, 6, 8</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida del Cabezal</th>\r\n			<td>250 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de Cabezal</th>\r\n			<td>24 pines</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Memoria</th>\r\n			<td>64Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Serial RS232, USB 2.0, Bluetooth</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>IBM 2390+ (includes Pro printer XL24E), Epson ESC/P2, Intermec 6820</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Roman, Sans Serif, Courier, Bold, Prestige, Script, Orator, Gothic, OCR-A, OCR-B.C&oacute;digos de Barra: UPC-A, UPC-E, EAN8,EAN13, Code 39, Code 128, Interleaved 2/5, Industrial 2/5</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumibles</th>\r\n			<td>Cinta de 4 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Manejo de Papel</th>\r\n			<td>Deposito de papel, posicionamiento autom&aacute;tico de formularios continuos cero arrancar. Tractor con plataformas de tractores especiales para carga de papel r&aacute;pida, alimentaci&oacute;n por fricci&oacute;n platina para hojas sueltas.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>N&uacute;mero de copias</th>\r\n			<td>original + 3</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Hojas continuas y sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso de papel</th>\r\n			<td>Aprox. 14 lbs.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o de papel</th>\r\n			<td>Ancho hoja continua &ndash; 4.01-10.51 pulgadas, Ancho hoja suelta &ndash; 4.01-10.51 pulgadas,Largo &ndash; 2.99-14.33 pulgadas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>Aprox. 14 lbs.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tama&ntilde;o</th>\r\n			<td style="text-align:left">(H x W x D) 140 x 365 x 263 mm (Aprox. 5.5 X 14.4 X 10.3 pulgadas)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Voltaje</th>\r\n			<td>Nominal 12-24V DC alimentado desde el vehiculo o con bateria externa.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Frecuencia</th>\r\n			<td>Adaptador AC: 100-240 VAC, 50-60Hz (opcional)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conformidad</th>\r\n			<td>CA C22.2 No. 950; EN60950; GS Mark; UL 746C; CE-Mark; E-Mark; Directivas de EU</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Confiabilidad MTBF</th>\r\n			<td>15000 Horas</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Drivers</th>\r\n			<td>Windows XP, 2000</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Temperatura</th>\r\n			<td>57 dB(A)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Garant&iacute;a</th>\r\n			<td>1 a&ntilde;o para devolver a deposito</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'pe', 39),
(156, 'Descripcion DP-540', 'Impresión Fiable\r\n\r\nConstruída para sobrevivir en el mundo real: déjelo caer en&nbsp; el piso \r\nde concreto de una bodega, sacudirla mientras el &nbsp; camión cae bache tras\r\n bache, llevarla desde el camión bajo la lluvia, y ver como la DP-540 se\r\n mantiene imprimiendo.\r\n\r\nProvee la Conectividad y la Compatibilidad que necesita\r\n\r\nSi la conexión que necesita es por cable o inalámbrica, la DP-540 es \r\ncapaz de proveer ambas cosas. Con USB, Serial RS-232, Bluetooth y WiFi, \r\nla opciones disponibles le asegu-ran una adaptación a sus necesidades.\r\n\r\nFácil de Usar\r\n\r\n<span>La pantalla LCD en ángulo proporciona mensajes legibles y fáciles de \r\nentender, y los botones del panel de control están claramente marcados.<br>\r\nCon carga de papel fácil y una manera fácil de cortar ma-nualmente los recibos a medida que son impresos.</span><br>', 'product', 1, 'es', 'bo', 41),
(157, 'Descripcion DP-540', 'Impresión Fiable\r\n\r\nConstruída para sobrevivir en el mundo real: déjelo caer en&nbsp; el piso \r\nde concreto de una bodega, sacudirla mientras el &nbsp; camión cae bache tras\r\n bache, llevarla desde el camión bajo la lluvia, y ver como la DP-540 se\r\n mantiene imprimiendo.\r\n\r\nProvee la Conectividad y la Compatibilidad que necesita\r\n\r\nSi la conexión que necesita es por cable o inalámbrica, la DP-540 es \r\ncapaz de proveer ambas cosas. Con USB, Serial RS-232, Bluetooth y WiFi, \r\nla opciones disponibles le asegu-ran una adaptación a sus necesidades.\r\n\r\nFácil de Usar\r\n\r\n<span>La pantalla LCD en ángulo proporciona mensajes legibles y fáciles de \r\nentender, y los botones del panel de control están claramente marcados.<br>\r\nCon carga de papel fácil y una manera fácil de cortar ma-nualmente los recibos a medida que son impresos.</span><br>', 'product', 1, 'es', 'co', 41),
(158, 'Descripcion DP-540', 'Impresión Fiable\r\n\r\nConstruída para sobrevivir en el mundo real: déjelo caer en&nbsp; el piso \r\nde concreto de una bodega, sacudirla mientras el &nbsp; camión cae bache tras\r\n bache, llevarla desde el camión bajo la lluvia, y ver como la DP-540 se\r\n mantiene imprimiendo.\r\n\r\nProvee la Conectividad y la Compatibilidad que necesita\r\n\r\nSi la conexión que necesita es por cable o inalámbrica, la DP-540 es \r\ncapaz de proveer ambas cosas. Con USB, Serial RS-232, Bluetooth y WiFi, \r\nla opciones disponibles le asegu-ran una adaptación a sus necesidades.\r\n\r\nFácil de Usar\r\n\r\n<span>La pantalla LCD en ángulo proporciona mensajes legibles y fáciles de \r\nentender, y los botones del panel de control están claramente marcados.<br>\r\nCon carga de papel fácil y una manera fácil de cortar ma-nualmente los recibos a medida que son impresos.</span><br>', 'product', 1, 'es', 'mx', 41),
(159, 'Descripcion DP-540', 'Impresión Fiable\r\n\r\nConstruída para sobrevivir en el mundo real: déjelo caer en&nbsp; el piso \r\nde concreto de una bodega, sacudirla mientras el &nbsp; camión cae bache tras\r\n bache, llevarla desde el camión bajo la lluvia, y ver como la DP-540 se\r\n mantiene imprimiendo.\r\n\r\nProvee la Conectividad y la Compatibilidad que necesita\r\n\r\nSi la conexión que necesita es por cable o inalámbrica, la DP-540 es \r\ncapaz de proveer ambas cosas. Con USB, Serial RS-232, Bluetooth y WiFi, \r\nla opciones disponibles le asegu-ran una adaptación a sus necesidades.\r\n\r\nFácil de Usar\r\n\r\n<span>La pantalla LCD en ángulo proporciona mensajes legibles y fáciles de \r\nentender, y los botones del panel de control están claramente marcados.<br>\r\nCon carga de papel fácil y una manera fácil de cortar ma-nualmente los recibos a medida que son impresos.</span><br>', 'product', 1, 'es', 'pe', 41),
(160, 'Caracteristicas DP-540', '<span>Método de impresión\r\n			Térmica directa\r\n		\r\n		\r\n			Resolución\r\n			203 dpi (8 puntos mm)\r\n	  \r\n		\r\n			Velocidad de impresión\r\n			Hasta 2,8 ips\r\n	  \r\n		\r\n			Fuentes\r\n			de ~ 6 cpi a 34 cpi en función de emulación elegida, además de la ampliación\r\n	  \r\n		\r\n			Códigos de barras\r\n			39, 128, Intlv 2 de 5, Codabar, EAN08 y 13, PDF-417, y la UPC, en función de emulación elegida\r\n	  \r\n		\r\n			Descargable\r\n			Fuentes, gráficos, logotipos y códigos de barras adicionales\r\n	  \r\n		\r\n			Conectividad estándar\r\n			USB y Serial\r\n	  \r\n		\r\n			Conectividad opcional\r\n			Bluetooth y Wi-Fi (802.11 b, G)\r\n	  \r\n		\r\n			Emulaciones\r\n			Zebra CPCL y ZPL, O''Neil, Printek\r\n	  \r\n		\r\n			Drivers\r\n			Drivers para Windows 98, 2000, XP, WIN 7, Windows CE / Pocket PC, Windows Mobile, Palm OS utilidades de impresión y SDKs\r\n	  \r\n		\r\n			Utilidades\r\n			Programa de instalación de la impresora remota proporciona una \r\ncómoda configuración a través de una interfaz de Windows desde cualquier\r\n ordenador\r\n	  \r\n		\r\n			Rollo de papel Tamaño\r\n			Diámetro máximo de rollo 1.75 "(4.3 cm), anchos de rollo de 2.90" a 4.125 "(07.04 a 10.05 cm)\r\n	  \r\n		\r\n			Capacidad de la bobina\r\n			Aproximadamente 65 ''(19.8 m), o aproximadamente 130 6 recibos "de largo\r\n	  \r\n		\r\n			Tamaño del núcleo\r\n			0,75 "(1,9 cm)\r\n	  \r\n		\r\n			Tipo de pila\r\n			Ion de litio de 2 celdas recargable (7,4 V, 2200 mAh)\r\n	  \r\n		\r\n			Duración de la bateria\r\n			Más de 3,500 pulgadas impresas por carga\r\n	  \r\n		\r\n			Recarga\r\n			2 horas a través de cargador de automóvil\r\n	  \r\n		\r\n			Peso físico\r\n			20 onzas (0,57 kg) con batería\r\n	  \r\n		\r\n			Tamaño físico (L x W x H)\r\n			6,1 "x 5,9" x 2,7 "(15,5 x 15,0 x 6,9 cm)\r\n	  \r\n		\r\n			Voltaje de alimentación\r\n			100 - 240 V amplia gama de detección automática\r\n	  \r\n		\r\n			Aprobaciones\r\n			FCC Clase B, Marca CE, RoHS, WEEE, adaptadores de CA aprobados por UL, E-marca de los adaptadores en los vehículos\r\n	  \r\n		\r\n			Temperatura de funcionamiento\r\n			-4 ° F a 122 ° F (-20 ° C a 50 ° C)\r\n	  \r\n		\r\n			Temperatura de almacenamiento\r\n			-4 ° F a 140 ° F (-20 ° C a 60 ° C)\r\n	  \r\n		\r\n			Humedad\r\n			10% a 90% RH (sin condensación)\r\n	  \r\n		\r\n			MTBF\r\n			10,000 horas\r\n		\r\n		<span>\r\n			Opciones:\r\n			<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>\r\n			Adaptador de corriente para vehículo, 12 a 24 VDC<br>\r\n			Tirante<br>\r\n			Clip de cinturón<br>\r\n			Cables de datos, RS232 o USB<br>\r\n			Estuche ambiental<br>\r\n			Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'bo', 41),
(161, 'Caracteristicas DP-540', '<span>Método de impresión\r\n			Térmica directa\r\n		\r\n		\r\n			Resolución\r\n			203 dpi (8 puntos mm)\r\n	  \r\n		\r\n			Velocidad de impresión\r\n			Hasta 2,8 ips\r\n	  \r\n		\r\n			Fuentes\r\n			de ~ 6 cpi a 34 cpi en función de emulación elegida, además de la ampliación\r\n	  \r\n		\r\n			Códigos de barras\r\n			39, 128, Intlv 2 de 5, Codabar, EAN08 y 13, PDF-417, y la UPC, en función de emulación elegida\r\n	  \r\n		\r\n			Descargable\r\n			Fuentes, gráficos, logotipos y códigos de barras adicionales\r\n	  \r\n		\r\n			Conectividad estándar\r\n			USB y Serial\r\n	  \r\n		\r\n			Conectividad opcional\r\n			Bluetooth y Wi-Fi (802.11 b, G)\r\n	  \r\n		\r\n			Emulaciones\r\n			Zebra CPCL y ZPL, O''Neil, Printek\r\n	  \r\n		\r\n			Drivers\r\n			Drivers para Windows 98, 2000, XP, WIN 7, Windows CE / Pocket PC, Windows Mobile, Palm OS utilidades de impresión y SDKs\r\n	  \r\n		\r\n			Utilidades\r\n			Programa de instalación de la impresora remota proporciona una \r\ncómoda configuración a través de una interfaz de Windows desde cualquier\r\n ordenador\r\n	  \r\n		\r\n			Rollo de papel Tamaño\r\n			Diámetro máximo de rollo 1.75 "(4.3 cm), anchos de rollo de 2.90" a 4.125 "(07.04 a 10.05 cm)\r\n	  \r\n		\r\n			Capacidad de la bobina\r\n			Aproximadamente 65 ''(19.8 m), o aproximadamente 130 6 recibos "de largo\r\n	  \r\n		\r\n			Tamaño del núcleo\r\n			0,75 "(1,9 cm)\r\n	  \r\n		\r\n			Tipo de pila\r\n			Ion de litio de 2 celdas recargable (7,4 V, 2200 mAh)\r\n	  \r\n		\r\n			Duración de la bateria\r\n			Más de 3,500 pulgadas impresas por carga\r\n	  \r\n		\r\n			Recarga\r\n			2 horas a través de cargador de automóvil\r\n	  \r\n		\r\n			Peso físico\r\n			20 onzas (0,57 kg) con batería\r\n	  \r\n		\r\n			Tamaño físico (L x W x H)\r\n			6,1 "x 5,9" x 2,7 "(15,5 x 15,0 x 6,9 cm)\r\n	  \r\n		\r\n			Voltaje de alimentación\r\n			100 - 240 V amplia gama de detección automática\r\n	  \r\n		\r\n			Aprobaciones\r\n			FCC Clase B, Marca CE, RoHS, WEEE, adaptadores de CA aprobados por UL, E-marca de los adaptadores en los vehículos\r\n	  \r\n		\r\n			Temperatura de funcionamiento\r\n			-4 ° F a 122 ° F (-20 ° C a 50 ° C)\r\n	  \r\n		\r\n			Temperatura de almacenamiento\r\n			-4 ° F a 140 ° F (-20 ° C a 60 ° C)\r\n	  \r\n		\r\n			Humedad\r\n			10% a 90% RH (sin condensación)\r\n	  \r\n		\r\n			MTBF\r\n			10,000 horas\r\n		\r\n		<span>\r\n			Opciones:\r\n			<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>\r\n			Adaptador de corriente para vehículo, 12 a 24 VDC<br>\r\n			Tirante<br>\r\n			Clip de cinturón<br>\r\n			Cables de datos, RS232 o USB<br>\r\n			Estuche ambiental<br>\r\n			Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'co', 41),
(162, 'Caracteristicas DP-540', '<span>Método de impresión\r\n			Térmica directa\r\n		\r\n		\r\n			Resolución\r\n			203 dpi (8 puntos mm)\r\n	  \r\n		\r\n			Velocidad de impresión\r\n			Hasta 2,8 ips\r\n	  \r\n		\r\n			Fuentes\r\n			de ~ 6 cpi a 34 cpi en función de emulación elegida, además de la ampliación\r\n	  \r\n		\r\n			Códigos de barras\r\n			39, 128, Intlv 2 de 5, Codabar, EAN08 y 13, PDF-417, y la UPC, en función de emulación elegida\r\n	  \r\n		\r\n			Descargable\r\n			Fuentes, gráficos, logotipos y códigos de barras adicionales\r\n	  \r\n		\r\n			Conectividad estándar\r\n			USB y Serial\r\n	  \r\n		\r\n			Conectividad opcional\r\n			Bluetooth y Wi-Fi (802.11 b, G)\r\n	  \r\n		\r\n			Emulaciones\r\n			Zebra CPCL y ZPL, O''Neil, Printek\r\n	  \r\n		\r\n			Drivers\r\n			Drivers para Windows 98, 2000, XP, WIN 7, Windows CE / Pocket PC, Windows Mobile, Palm OS utilidades de impresión y SDKs\r\n	  \r\n		\r\n			Utilidades\r\n			Programa de instalación de la impresora remota proporciona una \r\ncómoda configuración a través de una interfaz de Windows desde cualquier\r\n ordenador\r\n	  \r\n		\r\n			Rollo de papel Tamaño\r\n			Diámetro máximo de rollo 1.75 "(4.3 cm), anchos de rollo de 2.90" a 4.125 "(07.04 a 10.05 cm)\r\n	  \r\n		\r\n			Capacidad de la bobina\r\n			Aproximadamente 65 ''(19.8 m), o aproximadamente 130 6 recibos "de largo\r\n	  \r\n		\r\n			Tamaño del núcleo\r\n			0,75 "(1,9 cm)\r\n	  \r\n		\r\n			Tipo de pila\r\n			Ion de litio de 2 celdas recargable (7,4 V, 2200 mAh)\r\n	  \r\n		\r\n			Duración de la bateria\r\n			Más de 3,500 pulgadas impresas por carga\r\n	  \r\n		\r\n			Recarga\r\n			2 horas a través de cargador de automóvil\r\n	  \r\n		\r\n			Peso físico\r\n			20 onzas (0,57 kg) con batería\r\n	  \r\n		\r\n			Tamaño físico (L x W x H)\r\n			6,1 "x 5,9" x 2,7 "(15,5 x 15,0 x 6,9 cm)\r\n	  \r\n		\r\n			Voltaje de alimentación\r\n			100 - 240 V amplia gama de detección automática\r\n	  \r\n		\r\n			Aprobaciones\r\n			FCC Clase B, Marca CE, RoHS, WEEE, adaptadores de CA aprobados por UL, E-marca de los adaptadores en los vehículos\r\n	  \r\n		\r\n			Temperatura de funcionamiento\r\n			-4 ° F a 122 ° F (-20 ° C a 50 ° C)\r\n	  \r\n		\r\n			Temperatura de almacenamiento\r\n			-4 ° F a 140 ° F (-20 ° C a 60 ° C)\r\n	  \r\n		\r\n			Humedad\r\n			10% a 90% RH (sin condensación)\r\n	  \r\n		\r\n			MTBF\r\n			10,000 horas\r\n		\r\n		<span>\r\n			Opciones:\r\n			<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>\r\n			Adaptador de corriente para vehículo, 12 a 24 VDC<br>\r\n			Tirante<br>\r\n			Clip de cinturón<br>\r\n			Cables de datos, RS232 o USB<br>\r\n			Estuche ambiental<br>\r\n			Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'mx', 41),
(163, 'Caracteristicas DP-540', '<span>Método de impresión\r\n			Térmica directa\r\n		\r\n		\r\n			Resolución\r\n			203 dpi (8 puntos mm)\r\n	  \r\n		\r\n			Velocidad de impresión\r\n			Hasta 2,8 ips\r\n	  \r\n		\r\n			Fuentes\r\n			de ~ 6 cpi a 34 cpi en función de emulación elegida, además de la ampliación\r\n	  \r\n		\r\n			Códigos de barras\r\n			39, 128, Intlv 2 de 5, Codabar, EAN08 y 13, PDF-417, y la UPC, en función de emulación elegida\r\n	  \r\n		\r\n			Descargable\r\n			Fuentes, gráficos, logotipos y códigos de barras adicionales\r\n	  \r\n		\r\n			Conectividad estándar\r\n			USB y Serial\r\n	  \r\n		\r\n			Conectividad opcional\r\n			Bluetooth y Wi-Fi (802.11 b, G)\r\n	  \r\n		\r\n			Emulaciones\r\n			Zebra CPCL y ZPL, O''Neil, Printek\r\n	  \r\n		\r\n			Drivers\r\n			Drivers para Windows 98, 2000, XP, WIN 7, Windows CE / Pocket PC, Windows Mobile, Palm OS utilidades de impresión y SDKs\r\n	  \r\n		\r\n			Utilidades\r\n			Programa de instalación de la impresora remota proporciona una \r\ncómoda configuración a través de una interfaz de Windows desde cualquier\r\n ordenador\r\n	  \r\n		\r\n			Rollo de papel Tamaño\r\n			Diámetro máximo de rollo 1.75 "(4.3 cm), anchos de rollo de 2.90" a 4.125 "(07.04 a 10.05 cm)\r\n	  \r\n		\r\n			Capacidad de la bobina\r\n			Aproximadamente 65 ''(19.8 m), o aproximadamente 130 6 recibos "de largo\r\n	  \r\n		\r\n			Tamaño del núcleo\r\n			0,75 "(1,9 cm)\r\n	  \r\n		\r\n			Tipo de pila\r\n			Ion de litio de 2 celdas recargable (7,4 V, 2200 mAh)\r\n	  \r\n		\r\n			Duración de la bateria\r\n			Más de 3,500 pulgadas impresas por carga\r\n	  \r\n		\r\n			Recarga\r\n			2 horas a través de cargador de automóvil\r\n	  \r\n		\r\n			Peso físico\r\n			20 onzas (0,57 kg) con batería\r\n	  \r\n		\r\n			Tamaño físico (L x W x H)\r\n			6,1 "x 5,9" x 2,7 "(15,5 x 15,0 x 6,9 cm)\r\n	  \r\n		\r\n			Voltaje de alimentación\r\n			100 - 240 V amplia gama de detección automática\r\n	  \r\n		\r\n			Aprobaciones\r\n			FCC Clase B, Marca CE, RoHS, WEEE, adaptadores de CA aprobados por UL, E-marca de los adaptadores en los vehículos\r\n	  \r\n		\r\n			Temperatura de funcionamiento\r\n			-4 ° F a 122 ° F (-20 ° C a 50 ° C)\r\n	  \r\n		\r\n			Temperatura de almacenamiento\r\n			-4 ° F a 140 ° F (-20 ° C a 60 ° C)\r\n	  \r\n		\r\n			Humedad\r\n			10% a 90% RH (sin condensación)\r\n	  \r\n		\r\n			MTBF\r\n			10,000 horas\r\n		\r\n		<span>\r\n			Opciones:\r\n			<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>\r\n			Adaptador de corriente para vehículo, 12 a 24 VDC<br>\r\n			Tirante<br>\r\n			Clip de cinturón<br>\r\n			Cables de datos, RS232 o USB<br>\r\n			Estuche ambiental<br>\r\n			Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'pe', 41),
(164, 'Descripcion DP-550', 'Impresión Rentable\r\n\r\nConstruída para sobrevivir a mundo real: caídas en piso de&nbsp; concreto \r\nde la bodega, sacudidas mientras el camión cae&nbsp;&nbsp; bache tras bache, \r\nportarla desde el camión bajo la lluvia, y ver como la DP-540 se \r\nmantiene imprimiendo.\r\n\r\nFácil de Usar\r\n\r\nLa DP-550 almacena cinco formatos para realizar cambios con facilidad\r\n y rápidamente - tiene una memoria amplia para múltiples plantillas, \r\nfuentes, e imágenes gráficas, una&nbsp; impresora que cubre diferentes \r\nnecesidades en el mismo turno.\r\n\r\nLa carga de papel es sencilla y las etiquetas y recibos se pueden desprender fácilmente mientras son impresos.\r\n\r\nProvee la Conectividad y la Compatibilidad que necesita\r\n\r\nSi conexión que necesita es por cable o inalámbrica, la DP-550 es \r\ncapaz de proveer ambas cosas. Con USB, Serial RS-232, Bluetooth y WiFi, \r\nlas opciones disponibles le aseguran una adaptación a sus necesidades.<br>', 'product', 1, 'es', 'bo', 42),
(165, 'Descripcion DP-550', 'Impresión Rentable\r\n\r\nConstruída para sobrevivir a mundo real: caídas en piso de&nbsp; concreto \r\nde la bodega, sacudidas mientras el camión cae&nbsp;&nbsp; bache tras bache, \r\nportarla desde el camión bajo la lluvia, y ver como la DP-540 se \r\nmantiene imprimiendo.\r\n\r\nFácil de Usar\r\n\r\nLa DP-550 almacena cinco formatos para realizar cambios con facilidad\r\n y rápidamente - tiene una memoria amplia para múltiples plantillas, \r\nfuentes, e imágenes gráficas, una&nbsp; impresora que cubre diferentes \r\nnecesidades en el mismo turno.\r\n\r\nLa carga de papel es sencilla y las etiquetas y recibos se pueden desprender fácilmente mientras son impresos.\r\n\r\nProvee la Conectividad y la Compatibilidad que necesita\r\n\r\nSi conexión que necesita es por cable o inalámbrica, la DP-550 es \r\ncapaz de proveer ambas cosas. Con USB, Serial RS-232, Bluetooth y WiFi, \r\nlas opciones disponibles le aseguran una adaptación a sus necesidades.<br>', 'product', 1, 'es', 'co', 42);
INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(166, 'Descripcion DP-550', 'Impresión Rentable\r\n\r\nConstruída para sobrevivir a mundo real: caídas en piso de&nbsp; concreto \r\nde la bodega, sacudidas mientras el camión cae&nbsp;&nbsp; bache tras bache, \r\nportarla desde el camión bajo la lluvia, y ver como la DP-540 se \r\nmantiene imprimiendo.\r\n\r\nFácil de Usar\r\n\r\nLa DP-550 almacena cinco formatos para realizar cambios con facilidad\r\n y rápidamente - tiene una memoria amplia para múltiples plantillas, \r\nfuentes, e imágenes gráficas, una&nbsp; impresora que cubre diferentes \r\nnecesidades en el mismo turno.\r\n\r\nLa carga de papel es sencilla y las etiquetas y recibos se pueden desprender fácilmente mientras son impresos.\r\n\r\nProvee la Conectividad y la Compatibilidad que necesita\r\n\r\nSi conexión que necesita es por cable o inalámbrica, la DP-550 es \r\ncapaz de proveer ambas cosas. Con USB, Serial RS-232, Bluetooth y WiFi, \r\nlas opciones disponibles le aseguran una adaptación a sus necesidades.<br>', 'product', 1, 'es', 'mx', 42),
(167, 'Descripcion DP-550', 'Impresión Rentable\r\n\r\nConstruída para sobrevivir a mundo real: caídas en piso de&nbsp; concreto \r\nde la bodega, sacudidas mientras el camión cae&nbsp;&nbsp; bache tras bache, \r\nportarla desde el camión bajo la lluvia, y ver como la DP-540 se \r\nmantiene imprimiendo.\r\n\r\nFácil de Usar\r\n\r\nLa DP-550 almacena cinco formatos para realizar cambios con facilidad\r\n y rápidamente - tiene una memoria amplia para múltiples plantillas, \r\nfuentes, e imágenes gráficas, una&nbsp; impresora que cubre diferentes \r\nnecesidades en el mismo turno.\r\n\r\nLa carga de papel es sencilla y las etiquetas y recibos se pueden desprender fácilmente mientras son impresos.\r\n\r\nProvee la Conectividad y la Compatibilidad que necesita\r\n\r\nSi conexión que necesita es por cable o inalámbrica, la DP-550 es \r\ncapaz de proveer ambas cosas. Con USB, Serial RS-232, Bluetooth y WiFi, \r\nlas opciones disponibles le aseguran una adaptación a sus necesidades.<br>', 'product', 1, 'es', 'pe', 42),
(168, 'Caracteristicas DP-550', '<span>\r\n			Método de impresión\r\n			Térmica directa\r\n		\r\n		\r\n			Resolución\r\n			203 dpi (8 puntos mm)\r\n	  \r\n		\r\n			Velocidad de impresión\r\n			Hasta 3,3 ips\r\n	  \r\n		\r\n			Fuentes\r\n			de ~ 6 cpi a 34 cpi en función de emulación elegida, además de la ampliación\r\n	  \r\n		\r\n			Códigos de barras\r\n			39, 128, Intlv 2 de 5, Codabar, EAN08 y 13, PDF-417, y la UPC, en función de emulación elegida\r\n	  \r\n		\r\n			Descargable\r\n			Fuentes, gráficos, logotipos y códigos de barras adicionales\r\n	  \r\n		\r\n			Conectividad estándar\r\n			USB y Serial\r\n	  \r\n		\r\n			Conectividad opcional\r\n			Bluetooth y Wi-Fi (802.11 b, G)\r\n	  \r\n		\r\n			Emulaciones\r\n			Zebra CPCL y ZPL, O''Neil, Printek, Extech, E-Sim\r\n	  \r\n		\r\n			Drivers\r\n			Drivers para Windows 98, 2000, XP, WIN 7, Windows CE / Pocket PC, Windows Mobile, Palm OS utilidades de impresión y SDKs\r\n	  \r\n		\r\n			Utilidades\r\n			Programa de instalación de la impresora remota proporciona una \r\ncómoda configuración a través de una interfaz de Windows desde cualquier\r\n ordenador\r\n	  \r\n		\r\n			Rollo de papel Tamaño\r\n			Diámetro máximo del rollo 2 .. 625 "(6.7.3 cm), anchos de rollo de 3,0" a 4.125 "(7.6 a 10.5 cm)\r\n	  \r\n		\r\n			Capacidad de la bobina\r\n			Aproximadamente 171 ''(52.1 m)\r\n	  \r\n		\r\n			Tamaño del núcleo\r\n			0.75/1.0 "(19/25 mm)\r\n	  \r\n		\r\n			Tipo de pila\r\n			Ion de litio de 2 celdas recargable (7,4 V, 2200 mAh)\r\n	  \r\n		\r\n			Duración de la bateria\r\n			500 páginas o más de 3,500 pulgadas impresas por carga (ej. wi fi)\r\n	  \r\n		\r\n			Recarga\r\n			2 horas a través de cargador de a bordo o el cargador externo opcional\r\n	  \r\n		\r\n			Peso físico\r\n			1,75 libras (0,79 kg) con batería\r\n	  \r\n		\r\n			Tamaño físico (L x W x H)\r\n			7,0 "x 5,8" x 3,3 "(17,8 x 14,8 x 3,3 cm)\r\n	  \r\n		\r\n			Voltaje de alimentación\r\n			1100 - 240 V amplia gama de detección automática\r\n	  \r\n		\r\n			Aprobaciones\r\n			FCC Clase B, Marca CE, RoHS, WEEE, adaptadores de CA aprobados por UL, E-marca de los adaptadores en los vehículos\r\n	  \r\n		\r\n			Temperatura de funcionamiento\r\n			-4 ° F a 122 ° F (-20 ° C a 50 ° C)\r\n	  \r\n		\r\n			Temperatura de almacenamiento\r\n			-4 ° F a 140 ° F (-20 ° C a 60 ° C)\r\n	  \r\n		\r\n			Humedad\r\n			10% a 90% RH (sin condensación)\r\n	  \r\n		\r\n			MTBF\r\n			10,000 horas\r\n		\r\n		<span>\r\n			Opciones:\r\n			<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>\r\n			Adaptador de corriente para vehículo, 12 a 24 VDC<br>\r\n			Tirante<br>\r\n			Clip de cinturón<br>\r\n			Cables de datos, RS232 o USB<br>\r\n			Estuche ambiental<br>\r\n			Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'bo', 42),
(169, 'Caracteristicas DP-550', '<span>\r\n			Método de impresión\r\n			Térmica directa\r\n		\r\n		\r\n			Resolución\r\n			203 dpi (8 puntos mm)\r\n	  \r\n		\r\n			Velocidad de impresión\r\n			Hasta 3,3 ips\r\n	  \r\n		\r\n			Fuentes\r\n			de ~ 6 cpi a 34 cpi en función de emulación elegida, además de la ampliación\r\n	  \r\n		\r\n			Códigos de barras\r\n			39, 128, Intlv 2 de 5, Codabar, EAN08 y 13, PDF-417, y la UPC, en función de emulación elegida\r\n	  \r\n		\r\n			Descargable\r\n			Fuentes, gráficos, logotipos y códigos de barras adicionales\r\n	  \r\n		\r\n			Conectividad estándar\r\n			USB y Serial\r\n	  \r\n		\r\n			Conectividad opcional\r\n			Bluetooth y Wi-Fi (802.11 b, G)\r\n	  \r\n		\r\n			Emulaciones\r\n			Zebra CPCL y ZPL, O''Neil, Printek, Extech, E-Sim\r\n	  \r\n		\r\n			Drivers\r\n			Drivers para Windows 98, 2000, XP, WIN 7, Windows CE / Pocket PC, Windows Mobile, Palm OS utilidades de impresión y SDKs\r\n	  \r\n		\r\n			Utilidades\r\n			Programa de instalación de la impresora remota proporciona una \r\ncómoda configuración a través de una interfaz de Windows desde cualquier\r\n ordenador\r\n	  \r\n		\r\n			Rollo de papel Tamaño\r\n			Diámetro máximo del rollo 2 .. 625 "(6.7.3 cm), anchos de rollo de 3,0" a 4.125 "(7.6 a 10.5 cm)\r\n	  \r\n		\r\n			Capacidad de la bobina\r\n			Aproximadamente 171 ''(52.1 m)\r\n	  \r\n		\r\n			Tamaño del núcleo\r\n			0.75/1.0 "(19/25 mm)\r\n	  \r\n		\r\n			Tipo de pila\r\n			Ion de litio de 2 celdas recargable (7,4 V, 2200 mAh)\r\n	  \r\n		\r\n			Duración de la bateria\r\n			500 páginas o más de 3,500 pulgadas impresas por carga (ej. wi fi)\r\n	  \r\n		\r\n			Recarga\r\n			2 horas a través de cargador de a bordo o el cargador externo opcional\r\n	  \r\n		\r\n			Peso físico\r\n			1,75 libras (0,79 kg) con batería\r\n	  \r\n		\r\n			Tamaño físico (L x W x H)\r\n			7,0 "x 5,8" x 3,3 "(17,8 x 14,8 x 3,3 cm)\r\n	  \r\n		\r\n			Voltaje de alimentación\r\n			1100 - 240 V amplia gama de detección automática\r\n	  \r\n		\r\n			Aprobaciones\r\n			FCC Clase B, Marca CE, RoHS, WEEE, adaptadores de CA aprobados por UL, E-marca de los adaptadores en los vehículos\r\n	  \r\n		\r\n			Temperatura de funcionamiento\r\n			-4 ° F a 122 ° F (-20 ° C a 50 ° C)\r\n	  \r\n		\r\n			Temperatura de almacenamiento\r\n			-4 ° F a 140 ° F (-20 ° C a 60 ° C)\r\n	  \r\n		\r\n			Humedad\r\n			10% a 90% RH (sin condensación)\r\n	  \r\n		\r\n			MTBF\r\n			10,000 horas\r\n		\r\n		<span>\r\n			Opciones:\r\n			<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>\r\n			Adaptador de corriente para vehículo, 12 a 24 VDC<br>\r\n			Tirante<br>\r\n			Clip de cinturón<br>\r\n			Cables de datos, RS232 o USB<br>\r\n			Estuche ambiental<br>\r\n			Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'co', 42),
(170, 'Caracteristicas DP-550', '<span>\r\n			Método de impresión\r\n			Térmica directa\r\n		\r\n		\r\n			Resolución\r\n			203 dpi (8 puntos mm)\r\n	  \r\n		\r\n			Velocidad de impresión\r\n			Hasta 3,3 ips\r\n	  \r\n		\r\n			Fuentes\r\n			de ~ 6 cpi a 34 cpi en función de emulación elegida, además de la ampliación\r\n	  \r\n		\r\n			Códigos de barras\r\n			39, 128, Intlv 2 de 5, Codabar, EAN08 y 13, PDF-417, y la UPC, en función de emulación elegida\r\n	  \r\n		\r\n			Descargable\r\n			Fuentes, gráficos, logotipos y códigos de barras adicionales\r\n	  \r\n		\r\n			Conectividad estándar\r\n			USB y Serial\r\n	  \r\n		\r\n			Conectividad opcional\r\n			Bluetooth y Wi-Fi (802.11 b, G)\r\n	  \r\n		\r\n			Emulaciones\r\n			Zebra CPCL y ZPL, O''Neil, Printek, Extech, E-Sim\r\n	  \r\n		\r\n			Drivers\r\n			Drivers para Windows 98, 2000, XP, WIN 7, Windows CE / Pocket PC, Windows Mobile, Palm OS utilidades de impresión y SDKs\r\n	  \r\n		\r\n			Utilidades\r\n			Programa de instalación de la impresora remota proporciona una \r\ncómoda configuración a través de una interfaz de Windows desde cualquier\r\n ordenador\r\n	  \r\n		\r\n			Rollo de papel Tamaño\r\n			Diámetro máximo del rollo 2 .. 625 "(6.7.3 cm), anchos de rollo de 3,0" a 4.125 "(7.6 a 10.5 cm)\r\n	  \r\n		\r\n			Capacidad de la bobina\r\n			Aproximadamente 171 ''(52.1 m)\r\n	  \r\n		\r\n			Tamaño del núcleo\r\n			0.75/1.0 "(19/25 mm)\r\n	  \r\n		\r\n			Tipo de pila\r\n			Ion de litio de 2 celdas recargable (7,4 V, 2200 mAh)\r\n	  \r\n		\r\n			Duración de la bateria\r\n			500 páginas o más de 3,500 pulgadas impresas por carga (ej. wi fi)\r\n	  \r\n		\r\n			Recarga\r\n			2 horas a través de cargador de a bordo o el cargador externo opcional\r\n	  \r\n		\r\n			Peso físico\r\n			1,75 libras (0,79 kg) con batería\r\n	  \r\n		\r\n			Tamaño físico (L x W x H)\r\n			7,0 "x 5,8" x 3,3 "(17,8 x 14,8 x 3,3 cm)\r\n	  \r\n		\r\n			Voltaje de alimentación\r\n			1100 - 240 V amplia gama de detección automática\r\n	  \r\n		\r\n			Aprobaciones\r\n			FCC Clase B, Marca CE, RoHS, WEEE, adaptadores de CA aprobados por UL, E-marca de los adaptadores en los vehículos\r\n	  \r\n		\r\n			Temperatura de funcionamiento\r\n			-4 ° F a 122 ° F (-20 ° C a 50 ° C)\r\n	  \r\n		\r\n			Temperatura de almacenamiento\r\n			-4 ° F a 140 ° F (-20 ° C a 60 ° C)\r\n	  \r\n		\r\n			Humedad\r\n			10% a 90% RH (sin condensación)\r\n	  \r\n		\r\n			MTBF\r\n			10,000 horas\r\n		\r\n		<span>\r\n			Opciones:\r\n			<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>\r\n			Adaptador de corriente para vehículo, 12 a 24 VDC<br>\r\n			Tirante<br>\r\n			Clip de cinturón<br>\r\n			Cables de datos, RS232 o USB<br>\r\n			Estuche ambiental<br>\r\n			Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'mx', 42),
(171, 'Caracteristicas DP-550', '<span>\r\n			Método de impresión\r\n			Térmica directa\r\n		\r\n		\r\n			Resolución\r\n			203 dpi (8 puntos mm)\r\n	  \r\n		\r\n			Velocidad de impresión\r\n			Hasta 3,3 ips\r\n	  \r\n		\r\n			Fuentes\r\n			de ~ 6 cpi a 34 cpi en función de emulación elegida, además de la ampliación\r\n	  \r\n		\r\n			Códigos de barras\r\n			39, 128, Intlv 2 de 5, Codabar, EAN08 y 13, PDF-417, y la UPC, en función de emulación elegida\r\n	  \r\n		\r\n			Descargable\r\n			Fuentes, gráficos, logotipos y códigos de barras adicionales\r\n	  \r\n		\r\n			Conectividad estándar\r\n			USB y Serial\r\n	  \r\n		\r\n			Conectividad opcional\r\n			Bluetooth y Wi-Fi (802.11 b, G)\r\n	  \r\n		\r\n			Emulaciones\r\n			Zebra CPCL y ZPL, O''Neil, Printek, Extech, E-Sim\r\n	  \r\n		\r\n			Drivers\r\n			Drivers para Windows 98, 2000, XP, WIN 7, Windows CE / Pocket PC, Windows Mobile, Palm OS utilidades de impresión y SDKs\r\n	  \r\n		\r\n			Utilidades\r\n			Programa de instalación de la impresora remota proporciona una \r\ncómoda configuración a través de una interfaz de Windows desde cualquier\r\n ordenador\r\n	  \r\n		\r\n			Rollo de papel Tamaño\r\n			Diámetro máximo del rollo 2 .. 625 "(6.7.3 cm), anchos de rollo de 3,0" a 4.125 "(7.6 a 10.5 cm)\r\n	  \r\n		\r\n			Capacidad de la bobina\r\n			Aproximadamente 171 ''(52.1 m)\r\n	  \r\n		\r\n			Tamaño del núcleo\r\n			0.75/1.0 "(19/25 mm)\r\n	  \r\n		\r\n			Tipo de pila\r\n			Ion de litio de 2 celdas recargable (7,4 V, 2200 mAh)\r\n	  \r\n		\r\n			Duración de la bateria\r\n			500 páginas o más de 3,500 pulgadas impresas por carga (ej. wi fi)\r\n	  \r\n		\r\n			Recarga\r\n			2 horas a través de cargador de a bordo o el cargador externo opcional\r\n	  \r\n		\r\n			Peso físico\r\n			1,75 libras (0,79 kg) con batería\r\n	  \r\n		\r\n			Tamaño físico (L x W x H)\r\n			7,0 "x 5,8" x 3,3 "(17,8 x 14,8 x 3,3 cm)\r\n	  \r\n		\r\n			Voltaje de alimentación\r\n			1100 - 240 V amplia gama de detección automática\r\n	  \r\n		\r\n			Aprobaciones\r\n			FCC Clase B, Marca CE, RoHS, WEEE, adaptadores de CA aprobados por UL, E-marca de los adaptadores en los vehículos\r\n	  \r\n		\r\n			Temperatura de funcionamiento\r\n			-4 ° F a 122 ° F (-20 ° C a 50 ° C)\r\n	  \r\n		\r\n			Temperatura de almacenamiento\r\n			-4 ° F a 140 ° F (-20 ° C a 60 ° C)\r\n	  \r\n		\r\n			Humedad\r\n			10% a 90% RH (sin condensación)\r\n	  \r\n		\r\n			MTBF\r\n			10,000 horas\r\n		\r\n		<span>\r\n			Opciones:\r\n			<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>\r\n			Adaptador de corriente para vehículo, 12 a 24 VDC<br>\r\n			Tirante<br>\r\n			Clip de cinturón<br>\r\n			Cables de datos, RS232 o USB<br>\r\n			Estuche ambiental<br>\r\n			Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'pe', 42),
(172, 'Descripcion HKA 1080', 'La HKA 1080 posee un diseño compacto, robusto y elegante. Ideal para pequeñas y medianas empresas.\r\nPantalla táctil de 10”.\r\nImpresora 80 mm con cortadora.\r\nPantalla para cliente.\r\nLectores de tarjetas de banda magnética.\r\nApariencia simple y con estilo.\r\nDe fácil uso.\r\nDiseño Seguro y Confiable.\r\nDiseñado para aplicaciones que requieren un rendimiento superior.\r\nSDK disponible para Android.<br>', 'product', 1, 'es', 'bo', 43),
(173, 'Caracteristicas HKA 1080', '<span>Componentes\r\n		\r\n		<span>\r\n			<span>\r\n				<span>\r\n					<span>\r\n						Impresora\r\n						Térmica con cortadora de 80 mm\r\n					\r\n					\r\n						Pantalla para cliente\r\n						4 lineas LCD\r\n					\r\n					\r\n						WiFI\r\n						802.11a / b / g / n\r\n					\r\n					\r\n						Bluetooth\r\n						Bluetooth 4.0\r\n					\r\n				</span></span>\r\n			</span>\r\n		</span>\r\n		\r\n			Equipo\r\n		\r\n		<span>\r\n			<span>\r\n				<span>\r\n					<span>\r\n						UPC\r\n						ARM Cortec A9 4GHz\r\n					\r\n					\r\n						RAM\r\n						1GB DDR2\r\n					\r\n					\r\n						Sistema operativo\r\n						Android, Linux\r\n					\r\n				</span></span>\r\n			</span>\r\n		</span>\r\n		\r\n			Monitor\r\n		\r\n		<span>\r\n			<span>\r\n				<span>\r\n					<span>\r\n						Tamaño\r\n						9.7" monitor LCD\r\n					\r\n					\r\n						Brillo\r\n						250 nits\r\n					\r\n					\r\n						Resolución\r\n						1024 x 768 píxeles\r\n					\r\n					\r\n						Pantalla táctil\r\n						Capacitiva multi-touch\r\n					\r\n					\r\n						Inclinación\r\n						15°/35°\r\n					\r\n				</span></span>\r\n			</span>\r\n		</span>\r\n		\r\n			Dispositivos de almacenamiento\r\n		\r\n		<span>\r\n			<span>\r\n				<span>\r\n					<span>\r\n						Disco duro\r\n						4GB de Flash\r\n					\r\n					\r\n						Tarjetas\r\n						Expansion a 32GB MicroSD (TF Card)\r\n					\r\n				</span></span>\r\n			</span>\r\n		</span>\r\n		\r\n			E/S puertos\r\n		\r\n		<span>\r\n			<span>\r\n				<span>\r\n					<span>\r\n						Serie\r\n						2 RS232\r\n					\r\n					\r\n						USB\r\n						2 USB 2.0\r\n					\r\n					\r\n						LAN puerto de red\r\n						1 RJ-45 (10/100M)\r\n					\r\n					\r\n						Gaveta\r\n						1 RJ-11 (24V)\r\n					\r\n					\r\n						Interfaz de audio\r\n						1 host Audio 1 X 3G\r\n					\r\n				</span></span>\r\n			</span>\r\n		</span>\r\n		\r\n			Opcional\r\n		\r\n		<span>\r\n			<span>\r\n				<span>\r\n					<span>\r\n						Lector de tarjetas magnéticas\r\n						Lector de tarjetas de banda magnética (MSR), ID lector de tarjetas\r\n					\r\n					\r\n						3G\r\n						WCDMA\r\n					\r\n				</span></span>\r\n			</span>\r\n		</span>\r\n		\r\n			Otros\r\n		\r\n		<span>\r\n			<span>\r\n				<span>\r\n					<span>\r\n						Fuente de poder\r\n						Externa 24V fuente de alimentación\r\n					\r\n					\r\n						Color\r\n						Blanco/Negro\r\n					\r\n					\r\n						Peso\r\n						3.0 Kg\r\n					\r\n					\r\n						Dimensiones\r\n						282 × 250 × 170 ( mm )</span></span></span></span></span><br>', 'product', 1, 'es', 'bo', 43),
(174, 'Descripcion SRP-350', '<div>\r\n							<div>IMPRESORA FISCAL BIXOLON SRP-350<br><br>La\r\n BIXOLON SRP-350 es una impresora fiscal que brinda la mayor rapidez de \r\nimpresión en el mercado, cuenta con un diseño robusto y seguro \r\ncumpliendo así las normativas fiscales del país.La impresora \r\nposee un cortador automático de papel, impresión de códigos de barra, \r\nconexiones múltiples como Rs232 para comunicaciones y gavetas de dinero \r\nexternas.Diseño compacto y elegante, con la mayor rapidez de impresión y altas prestaciones en rendimiento.</div>\r\n					</div><br>', 'product', 1, 'es', 'do', 7),
(175, 'Descripcion SRP-350', '<div>\r\n							<div>IMPRESORA FISCAL BIXOLON SRP-350<br><br>La\r\n BIXOLON SRP-350 es una impresora fiscal que brinda la mayor rapidez de \r\nimpresión en el mercado, cuenta con un diseño robusto y seguro \r\ncumpliendo así las normativas fiscales del país.La impresora \r\nposee un cortador automático de papel, impresión de códigos de barra, \r\nconexiones múltiples como Rs232 para comunicaciones y gavetas de dinero \r\nexternas.Diseño compacto y elegante, con la mayor rapidez de impresión y altas prestaciones en rendimiento.</div>\r\n					</div><br>', 'product', 1, 'es', 'pa', 7),
(176, 'Caracteristicas SRP-350', '<div><ul><li>Impresora térmica.</li><li>1 estación de impresión.</li><li>Papel térmico de 80mm de ancho.</li><li>Cortadora de papel manual.</li><li>Puerto controlador de Gaveta.</li><li>Puerto controlador de display (20 dígitos por 2 líneas).</li><li>Puerto para lector fiscal.</li><li>3 Tasas de impuesto más 1 Exento.</li><li>8 Líneas de Logo.</li><li>8 Líneas de pie de página.</li><li>54 caracteres por línea (Modo Expandido).</li><li>16 medios de pago.</li><li>Manejo de cajeros.</li><li>Memoria fiscal 2000 reportes Z.</li><li>Facturas, notas de crédito, documentos no fiscales.</li><li>Velocidad de impresión 200mm por segundo.</li><li>Electronic Journal de 1Gb de Capacidad.</li><li>Almacena hasta 500.000 copias de facturas.</li><li>Capacidad para imprimir códigos de barra.</li><li>Reportes de auditoría por fecha, numero y/o tipo de documento.</li><li>Sistema "Carga fácil" para cargar el papel.</li></ul></div><br>', 'product', 1, 'es', 'do', 7),
(177, 'Caracteristicas SRP-350', '<div><ul><li>Impresora térmica.</li><li>1 estación de impresión.</li><li>Papel térmico de 80mm de ancho.</li><li>Cortadora de papel manual.</li><li>Puerto controlador de Gaveta.</li><li>Puerto controlador de display (20 dígitos por 2 líneas).</li><li>Puerto para lector fiscal.</li><li>3 Tasas de impuesto más 1 Exento.</li><li>8 Líneas de Logo.</li><li>8 Líneas de pie de página.</li><li>54 caracteres por línea (Modo Expandido).</li><li>16 medios de pago.</li><li>Manejo de cajeros.</li><li>Memoria fiscal 2000 reportes Z.</li><li>Facturas, notas de crédito, documentos no fiscales.</li><li>Velocidad de impresión 200mm por segundo.</li><li>Electronic Journal de 1Gb de Capacidad.</li><li>Almacena hasta 500.000 copias de facturas.</li><li>Capacidad para imprimir códigos de barra.</li><li>Reportes de auditoría por fecha, numero y/o tipo de documento.</li><li>Sistema "Carga fácil" para cargar el papel.</li></ul></div><br>', 'product', 1, 'es', 'pa', 7),
(178, 'Descripcion P3100DL ', 'La impresora fiscal <b>P3100DL</b> es una impresora láser, ideal para \r\nmedianas y grandes empresas, su cartucho maneja grandes cargas de \r\ntrabajo, cuando se mantiene en modo de reposo se ahorra más energía, \r\nproporciona impresiones rápidas y eficientes para cualquiera que sea la \r\ntarea.<br><br>\r\n\r\n\r\n	Esta impresora se ajusta a las regulaciones de ley establecidas por el \r\nSENIAT para máquinas fiscales bajo la providencia SENIAT/GF/00058 de \r\nfecha 7 enero del 2015<br>', 'product', 1, 'es', 'do', 24),
(179, 'Caracteristicas P3100DL ', '<div><ul><li>Dimensiones: 370 x 370 x 279 mm.</li><li>Peso: 9 kg sin tóner.</li><li>Resolución de 1200x600 DPI.</li><li>Panel de control contiene un botón y dos indicadores LED.</li><li>Impresión a láser tamaño carta</li><li>Configuración vertical (80 a 136 caracteres) y horizontal (187 Caracteres)</li><li>Generación de facturas, notas de crédito, notas de débito y documentos no fiscales.</li><li>Impresión de logos gráficos, códigos de Barra y Códigos QR y PDF417</li><li>Memoria Fiscal con capacidad de almacenamiento de hasta 4000 Reportes Z.</li><li>Memoria de Auditoría Electrónica de 2GB de almacenamiento.</li><li>Capacidad de programación de hasta 30 cajeros y 24 medios de pagos.</li><li>8 líneas de encabezado y 8 líneas de píe de página.</li><li>Tres (3) tasas de impuesto programables, más un (1) exento.</li><li>Comunicación vía puerto serial con PC, y un puerto USB Tipo B.</li><li>Puerto para Display.</li></ul></div><br>', 'product', 1, 'es', 'do', 24),
(180, 'Descripcion HSP7000', '<div>\r\n							<div>IMPRESORA HSP7000<br><br>La\r\n HSP7000 es una impresora muy potente, además de contar con un diseño \r\núnico y atractivo. Esta impresora presenta un mecanismo de impresión \r\ntérmica de alta calidad, además de componentes de impresora matricial de\r\n alta velocidad, de tal manera que permite la impresión de recibos y \r\ncheques de manera simultánea.La resolución de impresión es de 203\r\n ppp, lo cual permite la impresión de textos y logos gráficos de gran \r\ncalidad, además de códigos de barras, cupones promocionales, entre \r\notros.La HSP7000 incluye sensores de cubierta abierta y \r\nrecuperación automática de estado para carga de papel, con la finalidad \r\nde reducir los tiempos de inactividad.</div>\r\n					</div><br>', 'product', 1, 'es', 'pa', 9),
(181, 'Descripcion HSP7000', '<div>\r\n							<div>IMPRESORA HSP7000<br><br>La\r\n HSP7000 es una impresora muy potente, además de contar con un diseño \r\núnico y atractivo. Esta impresora presenta un mecanismo de impresión \r\ntérmica de alta calidad, además de componentes de impresora matricial de\r\n alta velocidad, de tal manera que permite la impresión de recibos y \r\ncheques de manera simultánea.La resolución de impresión es de 203\r\n ppp, lo cual permite la impresión de textos y logos gráficos de gran \r\ncalidad, además de códigos de barras, cupones promocionales, entre \r\notros.La HSP7000 incluye sensores de cubierta abierta y \r\nrecuperación automática de estado para carga de papel, con la finalidad \r\nde reducir los tiempos de inactividad.</div>\r\n					</div><br>', 'product', 1, 'es', 've', 9),
(182, 'Caracteristicas HSP7000', '<div><ul><li>Puerto controlador de Gaveta.</li><li>16 medios de pago.</li><li>Puerto controlador de display.</li><li>Manejo de cajeros.</li><li>Puerto para lector fiscal.</li><li>Facturas, Notas de Crédito, Documentos No Fiscales.</li><li>3 tasas de impuesto + 1 exento.</li><li>Impresión de Cheques.</li><li>8 líneas para encabezado.</li><li>Memoria Fiscal con capacidad de 2000 reportes Z.</li><li>Logo Gráfico y Códigos de Barra configurables.</li><li>Memoria de Auditoria de capacidad 1GB que permite.</li><li>8 líneas de pié de página.</li><li>Almacena hasta 500.000 copias de documentos.</li></ul>\r\n<h4>Estación de Recibo:</h4>\r\n<ul><li>Impresora de recibo térmica.</li><li>Velocidad de hasta 32 líneas por segundo.</li><li>Papel térmico de 80mm.</li><li>Máximo diãmetro del papel 83mm.</li><li>Sistema de carga fácil de papel.</li><li>54 caracteres por linea (en modo expandido).</li></ul>\r\n<h4>Estación de Cheques y Validación:</h4>\r\n<ul><li>Impresora Matricial.</li><li>Impresora Frontal de Cheques y Endoso</li><li>Validación de documentos (una línea)</li><li>Velocidad de hasta 4.82 líneas por segundo.</li><li>Lector MICR (para captura automática de los datos del cheque).</li></ul></div><br>', 'product', 1, 'es', 'pa', 9),
(183, 'Caracteristicas HSP7000', '<div><ul><li>Puerto controlador de Gaveta.</li><li>16 medios de pago.</li><li>Puerto controlador de display.</li><li>Manejo de cajeros.</li><li>Puerto para lector fiscal.</li><li>Facturas, Notas de Crédito, Documentos No Fiscales.</li><li>3 tasas de impuesto + 1 exento.</li><li>Impresión de Cheques.</li><li>8 líneas para encabezado.</li><li>Memoria Fiscal con capacidad de 2000 reportes Z.</li><li>Logo Gráfico y Códigos de Barra configurables.</li><li>Memoria de Auditoria de capacidad 1GB que permite.</li><li>8 líneas de pié de página.</li><li>Almacena hasta 500.000 copias de documentos.</li></ul>\r\n<h4>Estación de Recibo:</h4>\r\n<ul><li>Impresora de recibo térmica.</li><li>Velocidad de hasta 32 líneas por segundo.</li><li>Papel térmico de 80mm.</li><li>Máximo diãmetro del papel 83mm.</li><li>Sistema de carga fácil de papel.</li><li>54 caracteres por linea (en modo expandido).</li></ul>\r\n<h4>Estación de Cheques y Validación:</h4>\r\n<ul><li>Impresora Matricial.</li><li>Impresora Frontal de Cheques y Endoso</li><li>Validación de documentos (una línea)</li><li>Velocidad de hasta 4.82 líneas por segundo.</li><li>Lector MICR (para captura automática de los datos del cheque).</li></ul></div><br>', 'product', 1, 'es', 've', 9),
(185, 'Descripcion CRD81FJ', '<div>\r\n							<div>CAJA REGISTRADORA CRD81FJ<br><br>La\r\n CRD81FJ es una Caja Registradora fabricada con los más elevados \r\nestándares de calidad, robustez y seguridad. Ofrece un elegante diseño, \r\nbrindando una independización a la hora de llevar las operaciones del \r\ndía a día del negocio.Permite una fácil manipulación mediante un \r\nteclado plano y un display. La misma es programable tanto como por PC \r\ncomo manualmente.Maneja hasta 8 departamentos y hasta 3000 \r\nproductos. Capacidad de manejar 4 cajeros y brindar rapidez a la hora de\r\n facturar con 120 teclas de ventas directas.</div>\r\n					</div><br>', 'product', 1, 'es', 'do', 11),
(186, 'Descripcion CRD81FJ', '<div>\r\n							<div>CAJA REGISTRADORA CRD81FJ<br><br>La\r\n CRD81FJ es una Caja Registradora fabricada con los más elevados \r\nestándares de calidad, robustez y seguridad. Ofrece un elegante diseño, \r\nbrindando una independización a la hora de llevar las operaciones del \r\ndía a día del negocio.Permite una fácil manipulación mediante un \r\nteclado plano y un display. La misma es programable tanto como por PC \r\ncomo manualmente.Maneja hasta 8 departamentos y hasta 3000 \r\nproductos. Capacidad de manejar 4 cajeros y brindar rapidez a la hora de\r\n facturar con 120 teclas de ventas directas.</div>\r\n					</div><br>', 'product', 1, 'es', 'pa', 11),
(187, 'Caracteristicas Papel Térmico', 'Papel soporte sobre el que se aplican las capas de estucado. Toda la \r\ncelulosa y el papel soporte de la gama se produce en fabricas de grupo \r\nTermax.<br><br>\r\nCapa de Preestuco o precapa que cubre el soporte para garantizar una \r\nsuperficie uniforme y lisa. A mayor lisura de esta capa, mejor \r\nresolución y definición de esta imagen.<br><br>\r\nCapa térmica: es la capa superior formada por una gran cantidad de \r\ncompuestos químicos que sometidos al calor reaccionan entre si y \r\ndesarrollan la imagen. Los tres principales componentes de esta capa \r\nson: un colorante, un desarrollador de color y un sensibilizador. \r\n<br>', 'product', 1, 'es', 'do', 10),
(188, 'Descripcion Papel Termico', '<div>\r\n							<div>\r\n\r\nPapel<br>\r\n\r\nDiferentes tipos de papel,\r\nEstán especialmente diseñados para cada aplicación.\r\n\r\nTienen una excelente calidad de imagen y garantizan una perfecta definición de los códigos de barras.\r\n\r\nTienen diferentes niveles de durabilidad y sensibilidad, adecuados a su aplicación final.\r\n\r\nGarantizan un perfecto comportamiento durante su manipulación y uso.\r\n\r\n</div>\r\n					</div><br>', 'product', 1, 'es', 'do', 10),
(189, 'Especificaciones Papel Térmico', 'Aplicaciones:\r\nRecibos en puntos de venta (POS), Extractos Bancarios (ATM) y Fax.<br>\r\n\r\nSensibilidad:\r\nEstandar.<br>\r\n\r\nDurabilidad de la imagen:\r\n5 años.<br>', 'product', 1, 'es', 'do', 10),
(190, 'Tipos Papel Térmico', 'Están especialmente diseñados para cada aplicación.\r\n\r\nTienen una excelente calidad de imagen y garantizan una perfecta definición de los códigos de barras.\r\n\r\nTienen diferentes niveles de durabilidad y sensibilidad, adecuados a su aplicación final.\r\n\r\nGarantizan un perfecto comportamiento durante su manipulación y uso.<br>', 'product', 1, 'es', 'do', 10),
(191, 'Ventajas Papel Térmico', 'Es rápida, compacta y de funcionamiento silencioso.\r\n<br>\r\nEs fiable: la impresión es nítida y clara con una resolución optima que \r\npermite leer la imagen perfectamente mediante lectores de códigos de \r\nbarras.\r\n<br>\r\nEs ecológica y económica: no se utiliza toner, cinta u otros \r\nconsumibles. Requiere de poca energía para su funcionamiento y tiene un \r\nbajo coste de mantenimiento. <br>', 'product', 1, 'es', 'do', 10),
(192, 'Descripcion BC-2000', 'CONTADOR DE BILLETES BC-2000<br><br>El contador de Billetes BC-2000 hará más fácil su trabajo, su sistema único de alimentación asegura su buen desempeño en el conteo de billetes de diferente calidad.<br>', 'product', 1, 'es', 've', 44),
(193, 'Caracteristicas BC-2000', '<p>Especificaciones T&eacute;cnicas</p>\r\n\r\n<ul>\r\n	<li>Velocidad de Conteo: 800, 1100, 1500 billetes/min.</li>\r\n	<li>Capacidad de la Tolva: 400 billetes.</li>\r\n	<li>Capacidad del Apilador: 200 billetes.</li>\r\n	<li>Tama&ntilde;o de billetes: 120x50 hasta 175x90mm.</li>\r\n	<li>Espesor de billetes: 0.05-0.2mm.</li>\r\n	<li>Pantalla de Conteo: 4 d&iacute;gitos.</li>\r\n	<li>Pantalla de Lote: 3 d&iacute;gitos.</li>\r\n	<li>Requerimientos de Alimentaci&oacute;n 110V &oacute; 220V.</li>\r\n	<li>Consumo de Energ&iacute;a 50W (en Operaci&oacute;n).</li>\r\n	<li>Dimensiones: 280mm x 225mm x 250mm.</li>\r\n	<li>Temperatura de Trabajo 0 - 40 Grados Cent&iacute;grados.</li>\r\n	<li>Humedad Relativa: 30 - 75%.</li>\r\n	<li>Peso Neto: 5.1 kg.</li>\r\n</ul>\r\n\r\n<p>Modos de Conteo:</p>\r\n\r\n<ul>\r\n	<li>Conteo Cont&iacute;nuo.</li>\r\n	<li>Conteo por Lotes.</li>\r\n	<li>Conteo con Acumulaci&oacute;n.</li>\r\n	<li>Inicio Manual o Autom&aacute;tico.</li>\r\n</ul>\r\n\r\n<p>Modos de Detecci&oacute;n:</p>\r\n\r\n<ul>\r\n	<li>Detecci&oacute;n Doble.</li>\r\n	<li>Detecci&oacute;n de Ancho.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 've', 44),
(194, 'Descripcion BCD1000DG', 'VISOR DE PRECION BIXOLON BCD1000DG&nbsp;<br><br>El Visor de Precios Bixolon, es otra de la innovaciones que The Factory HKA C.A, incluye dentro de su selecta gama de productos.El BCD-1000 de Bixolon, es un potente visor de precios de tecnología VFD, con dos líneas de caracteres de 20 dígitos cada una, su metodologí de conexión RS-232. lo hace compatible con cualquier equipo que maneje este protocolo, incluyendo nuestras impresoras fiscales Aclas y Bixolon.Su mástil de altura variable lo hace un dispositvo con una gran flexibilidad al momento de darle ubicación dentro de su negocio.El color negro, lo hace la pareja perfecta para las Impresoras Fiscales Bixolon y Aclas, la dupla perfecta.<br>', 'product', 1, 'es', 've', 45),
(195, 'Caracteristicas BCD1000DG', '<ul><li>Dos líneas de caracteres de 20 dígitos c/u.</li><li>Mástil de altura variable.</li></ul><br>', 'product', 1, 'es', 've', 45),
(196, 'Descripcion Gaveta', 'Diseñada para nuestras Cajas Registradoras.<br><br><ul><li>Posee Prensa - Billetes Totalmente Metálico.</li><li>Contiene un Frontal Especial para introducir Cheques.</li><li>Su bandeja está dividida en 4 compartimientos ajustables.</li><li>Contiene 8 compartimientos para Monedas (removible).</li><li>Conexión para cualquiera de nuestras Impresoras Fiscales ACLAS, KUBE y SAMSUNG.</li><li>Posee Frontal Metálico.</li><li>Color Negro.</li></ul><br>', 'product', 1, 'es', 've', 46),
(197, 'Caracteristicas Gaveta', '<h4>Características Técnicas</h4><ul><li>Prensa billetes metallico.</li><li>Ranura para guardar cheques.</li></ul><br>', 'product', 1, 'es', 've', 46),
(198, 'Caracteristicas HKA USB-LINK', '<h4>Características Técnicas</h4><ul><li>Soporta interfaz RS232.</li><li>Cumple con todas las espicificaciones y estándares USB.</li><li>Tasa de transferencia de datos estandares de USB.</li><li>Compatibilidad con: Windows (todos), Linux y Mac OS 8.6/9/10.</li></ul><br>', 'product', 1, 'es', 've', 47),
(199, 'Caracteristicas HKA-232', '<h4>Características Técnicas</h4><ul><li>Soporta interfaz serial RS232.</li><li>Cumple con todas las espicificaciones y estándares USB.</li><li>Tasa de transferencia de datos de hasta 230Kbps.</li><li>Funciona con cámaras digitales, GPS,PDA, modems y todos los modelos y marcas de equipos fiscales.</li><li>Compatibilidad con: Windows(todos), Linux y Mac OS 8.6/9/10</li></ul><br>', 'product', 1, 'es', 've', 48),
(200, 'Caracteristicas VB-30R', '<h4>Los Localizadores Inalámbricos ideales.</h4><h4>Caracteriticas.</h4><ul><li>Transmisor/Receptor.</li><li>Tres niveles de volumen.</li><li>Despliegue de datos en tiempo real.</li><li>Soporta botones de tres funciones.</li><li>Equipo de fácil instalación.</li><li>Soporta 744 botones y 24 localizadores (pagers).</li><li>Adaptador AC/DC.</li></ul><h4>Botones:</h4><ul><li>Botones de una y tres funciones programables.</li><li>Duración de batería interna 2 años aprox.</li><li>Dimensiones 60x21mm.</li><li>Distancia efectiva de alcance 50 - 100m.</li></ul><h4>Pagers:</h4><ul><li>Alerta por medio de tonos, vibración, tono/vibración, vibración baja y vibración alta.</li><li>Localización individual o por grupo.</li><li>Capta 8 tipos de código.</li><li>Memoria de mensajes.</li><li>Alertas programables.</li><li>Pantalla LCD.</li><li>Alerta de batería baja.</li></ul><br>', 'product', 1, 'es', 've', 49),
(201, 'Descripcion CD5240', 'VISOR DE PRECIOS ACLAS CD5240<br><br>El Visor de Precios Aclas, es la segunda innovación en materia de visores de precios que The Factory HKA C.A, incluye dentro de su selecta gama de productos.El CD-5240 de Aclas, es un potente visor de precios de tecnología LCD, con dos líneas de caracteres de 20 dígitos cada una, su metodología de conexión RS-232. lo hace compatible con cualquier equipo que maneje este protocolo, incluyendo nuestras impresoras fiscales Aclas y Bixolon.Incluye además dos (2) mástiles uno pequeño y otro de altura regular media con la finalidad de satisfacer las necesidades del cliente.El color gris claro, viene a complementar el dispay de color negro Bixolon.<br>', 'product', 1, 'es', 've', 50),
(202, 'Caracteristicas CD5240', '<ul><li>Dos líneas de caracteres de 20 dígitos c/u.</li><li>Dos mastiles de altura variable.</li></ul><br>', 'product', 1, 'es', 've', 50),
(203, 'Descripcion LS21530EC', 'BALANZA LS21530EC<br><br>Es una balanza elegante de robusto diseño, este dispositivo cuenta con 2 Display en su poste con cuatro líneas de información con el fin de mostrar al cliente el peso, precio e importe de su compra más una línea de información o publicidad.Dos tipos de teclados, uno de acceso directo doble nivel y un teclado numérico con funciones de programación. Alta capacidad de almacenamiento de PLUs, impresión térmica de alta calidad que permite imprimir recibos y etiquetas auto adhesivas.Dispositivo de fácil programación por teclado o por software con sistemas de seguridad que bloquea y desbloquea la balanza por medio de un código escogido por el cliente.Impresión de códigos de barras y puerto de comunicación por Ethernet con dos tipos de calibración 15Kg y 30 Kg.<br>', 'product', 1, 'es', 've', 51),
(204, 'Caracteristicas LS21530EC', '<ul><li>Peso (15/30 Kg): esto es porque de 0 Kg hasta 15 Kg, la mínima división o variación que reflejará la medición de la balanza es 5 gramos. Luego a partir de los 15 Kg en adelante, la mínima división será de 10 gramos. Siendo el peso máximo medible por parte de la balanza de 30 Kg. La balanza imprime a partir de 100 gramos de peso.</li><li>Capacidad de almacenamiento hasta 12.000 plus.</li><li>Impresión es del tipo térmica, se puede utilizar papel normal térmico o etiquetas autoadhesivas. El tamaño de la etiqueta es configurable por medio del programa editor de etiquetas. Medida estándar es de 40 x 60 mm, siendo la máxima dimensión de ancho de etiqueta de 60 mm para la impresión.</li><li>Teclado Plano de 112 teclas de acceso directo doble nivel, para un total de 224 teclas configurables mediante programación.</li><li>Display tipo Torre cuadrado doble cara, de cuatro líneas (1 línea de información, 1 línea de peso, 1 línea de precio unitario, 1 línea importe total).</li><li>Impresión de Códigos de Barra (Ean13, Int25).</li><li>Puerto de comunicación Ethernet RJ45, TCP-IP tipo estándar.</li><li>Fácil programación directamente desde la propia balanza.</li><li>Dimensiones: 384mm (ancho) x 478mm (longitud) x 196mm (altura).</li><li>Peso aproximado. 7.5 Kg.</li></ul><br>', 'product', 1, 'es', 've', 51),
(205, 'Caracteristicas CRD81FJ', '<div><ul><li>Trabaja hasta 72 Horas sin suministro de Corriente Electrica</li><li>Capacidad para Imprimir los Datos Fiscales del Cliente (da Credito Fiscal)</li><li>Maneja hasta 3000 productos.</li><li>8 departamentos programables.</li><li>Memoria Fiscal con capacidad   de 2200 reportes Z.</li><li>Memoria de Auditoria Electronica 2GB.</li><li>Capacidad para imprimir la razon social y RIF del cliente.</li><li>Papel de 57 mm de ancho.</li><li>Bateria interna que permite utilizar la Caja Registradora hasta por 72 horas \r\n        sin conexion a la red electrica.</li><li>4  Tasas de impuestos programables mas Exento.</li><li>120 Teclas de ventas directas.</li><li>Programable por PC.</li><li>4 Cajeros programables.</li><li>Maneja 37 tipos de codigo de barras.</li><li>Gaveta de dinero removible.</li><li>Maneja descuentos por porcentaje.</li><li>Maneja descuentos por montos.</li><li>Control de inventarios.</li><li>Anulacione bajo clave.</li><li>4 Medios de pago.</li></ul></div><br>', 'product', 1, 'es', 'do', 11),
(206, 'Caracteristicas CRD81FJ', '<div><ul><li>Trabaja hasta 72 Horas sin suministro de Corriente Electrica</li><li>Capacidad para Imprimir los Datos Fiscales del Cliente (da Credito Fiscal)</li><li>Maneja hasta 3000 productos.</li><li>8 departamentos programables.</li><li>Memoria Fiscal con capacidad   de 2200 reportes Z.</li><li>Memoria de Auditoria Electronica 2GB.</li><li>Capacidad para imprimir la razon social y RIF del cliente.</li><li>Papel de 57 mm de ancho.</li><li>Bateria interna que permite utilizar la Caja Registradora hasta por 72 horas \r\n        sin conexion a la red electrica.</li><li>4  Tasas de impuestos programables mas Exento.</li><li>120 Teclas de ventas directas.</li><li>Programable por PC.</li><li>4 Cajeros programables.</li><li>Maneja 37 tipos de codigo de barras.</li><li>Gaveta de dinero removible.</li><li>Maneja descuentos por porcentaje.</li><li>Maneja descuentos por montos.</li><li>Control de inventarios.</li><li>Anulacione bajo clave.</li><li>4 Medios de pago.</li></ul></div><br>', 'product', 1, 'es', 'pa', 11),
(207, 'Caracteristicas Tally 1225', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Velocidad de Impresi&oacute;n</strong></td>\r\n			<td>10 cpi: 375 cps (Borrador R&aacute;pido), 250 cps (Borrador), 83 cps (LQ)&nbsp;<br />\r\n			12 cpi: 300 cps (Borrador), 100 cps (LQ)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>M&eacute;todo de Impresi&oacute;n</strong></td>\r\n			<td>Serial matriz de impacto</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Resoluci&oacute;n</strong></td>\r\n			<td>Hasta 360 x 360 dpi</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Desempe&ntilde;o (ECMA 132)</strong></td>\r\n			<td>336 p&aacute;ginas por hora</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Densidad de caracteres</strong></td>\r\n			<td>10, 12, 15, 17.1, 20cpi, Espaciado proporcional</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Densidad de linea</strong></td>\r\n			<td>1, 2, 3, 4, 6, 8 lpi</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Vida cabezal de impresi&oacute;n</strong></td>\r\n			<td>400 Millones de golpes por pin</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Memoria</strong></td>\r\n			<td>48 Kb</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Conectividad</strong></td>\r\n			<td>USB 2.0 y paralelo (Bi-direccional)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Emulaci&oacute;n</strong></td>\r\n			<td>Epson LQ (ESC P/2), IBM ProPrinter (2390 Plus)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Fuentes</strong></td>\r\n			<td>Draft, Courier, Roman, Sans Serif, Prestige, Script, Orator, Gothic, Souvenir, OCR-A/B*, 10 C&oacute;digos de barras</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Consumibles</strong></td>\r\n			<td>Cinta Negra: 7 millones de caracteres</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Manejo de Papel</strong></td>\r\n			<td>Tractor y bandeja de hojas sueltas</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tipo de papel</strong></td>\r\n			<td>Hojas sueltas y continuas</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>N&uacute;mero de Copias</strong></td>\r\n			<td>Original +4 (5-copias)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Peso Papel</strong></td>\r\n			<td>52-100 gsm</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tama&ntilde;o Papel</strong></td>\r\n			<td>3&ldquo; a 16.53&ldquo; ancho</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Peso</strong></td>\r\n			<td>Aprox. 18lbs</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tama&ntilde;o</strong></td>\r\n			<td>(HxWxD): 6.8&ldquo;x23.1&ldquo;x13.1&ldquo;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Voltaje</strong></td>\r\n			<td>230 V</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Frecuencia</strong></td>\r\n			<td>50Hz a 60Hz</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>consumo</strong></td>\r\n			<td>max 7.6W en espera</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Confiabilidad MTBF</strong></td>\r\n			<td>10.000 horas</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Drivers</strong></td>\r\n			<td>Windows 2000, 2003 Server, XP, Vista, Windows 7</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Temperatura</strong></td>\r\n			<td>+5&deg;C a +40&deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Humedad</strong></td>\r\n			<td>10% a 80% (sin condensaci&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Ac&uacute;stica</strong></td>\r\n			<td>&lt; 55dB(A) (ISO 7779)</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'bo', 34),
(208, 'Descripcion Tally 1225', '<p>La Tally Dascom 1225 es una impresora matriz de punto de 24 pines y 136 columnas con interfaces USB y paralela. El carro ancho es perfecto para entornos industriales y aplicaciones de oficina.</p>\r\n\r\n<p>Con bajos costes de funcionamiento, impresiones de alta calidad y un panel de control f&aacute;cil de utilizar es ideal para bajos vol&uacute;menes de informes de cotizaci&oacute;n,facturas y otros documentos importantes, as&iacute; como las notas de despacho y etiquetas.</p>\r\n\r\n<p>Adem&aacute;s de estas aplicaciones, la 1225 tambi&eacute;n es adecuada para sistemas de medici&oacute;n y control.</p>\r\n\r\n<p>Entre las impresoras matriciales de precio m&aacute;s bajo disponible en la actualidad la 1225 tambi&eacute;n ofrece una cinta de larga duraci&oacute;n que significa menos cambios de cinta, menos tiempo de inactividad y reducci&oacute;n en los costos de funcionamiento</p>\r\n', 'product', 1, 'es', 'bo', 34),
(209, 'Descripcion DP-520', 'Pequeña y LigeraLo último en portabilidad, la DP-520 es una gran opción para &nbsp; abastecer sus necesidades de impresión móvil. Es tan&nbsp;&nbsp; pequeña y ligera, que apenas notará que lo está llevando.Fácil de UsarFácil de operar con sólo un botón, esta impresora puede ser&nbsp; operada con un mínimo entrenamiento.Con carga de papel fácil y una manera fácil de cortar manualmente los recibos a medida que son impresos.Provee la Conectividad y la Compatibilidad Que NecesitaTanto si es una conexión por cable o inalámbrica lo que necesita, la DP-520 posee ambas opciones. Con el modelo Serial RS-232 y el modelo Serial/Bluetooth, las opciones disponibles son seguras para adaptarse a sus necesidades.RentableRetorne su inversión con creces en un corto período con esta impresora de bajo costo.<br>', 'product', 1, 'es', 'bo', 40),
(210, 'Descripcion DP-520', 'Pequeña y LigeraLo último en portabilidad, la DP-520 es una gran opción para &nbsp; abastecer sus necesidades de impresión móvil. Es tan&nbsp;&nbsp; pequeña y ligera, que apenas notará que lo está llevando.Fácil de UsarFácil de operar con sólo un botón, esta impresora puede ser&nbsp; operada con un mínimo entrenamiento.Con carga de papel fácil y una manera fácil de cortar manualmente los recibos a medida que son impresos.Provee la Conectividad y la Compatibilidad Que NecesitaTanto si es una conexión por cable o inalámbrica lo que necesita, la DP-520 posee ambas opciones. Con el modelo Serial RS-232 y el modelo Serial/Bluetooth, las opciones disponibles son seguras para adaptarse a sus necesidades.RentableRetorne su inversión con creces en un corto período con esta impresora de bajo costo.<br>', 'product', 1, 'es', 'co', 40),
(211, 'Descripcion DP-520', 'Pequeña y LigeraLo último en portabilidad, la DP-520 es una gran opción para &nbsp; abastecer sus necesidades de impresión móvil. Es tan&nbsp;&nbsp; pequeña y ligera, que apenas notará que lo está llevando.Fácil de UsarFácil de operar con sólo un botón, esta impresora puede ser&nbsp; operada con un mínimo entrenamiento.Con carga de papel fácil y una manera fácil de cortar manualmente los recibos a medida que son impresos.Provee la Conectividad y la Compatibilidad Que NecesitaTanto si es una conexión por cable o inalámbrica lo que necesita, la DP-520 posee ambas opciones. Con el modelo Serial RS-232 y el modelo Serial/Bluetooth, las opciones disponibles son seguras para adaptarse a sus necesidades.RentableRetorne su inversión con creces en un corto período con esta impresora de bajo costo.<br>', 'product', 1, 'es', 'mx', 40),
(212, 'Descripcion DP-520', 'Pequeña y LigeraLo último en portabilidad, la DP-520 es una gran opción para &nbsp; abastecer sus necesidades de impresión móvil. Es tan&nbsp;&nbsp; pequeña y ligera, que apenas notará que lo está llevando.Fácil de UsarFácil de operar con sólo un botón, esta impresora puede ser&nbsp; operada con un mínimo entrenamiento.Con carga de papel fácil y una manera fácil de cortar manualmente los recibos a medida que son impresos.Provee la Conectividad y la Compatibilidad Que NecesitaTanto si es una conexión por cable o inalámbrica lo que necesita, la DP-520 posee ambas opciones. Con el modelo Serial RS-232 y el modelo Serial/Bluetooth, las opciones disponibles son seguras para adaptarse a sus necesidades.RentableRetorne su inversión con creces en un corto período con esta impresora de bajo costo.<br>', 'product', 1, 'es', 'pe', 40),
(213, 'Caracteristicas DP-520', '<span>• Fácil de usar... con una sola mano, sistema fácil de<br>carga de papel</span><span><br>• Tamaño ultra-portable con el tamaño más pequeño -<br>pesa sólo 446 gramos, con batería y media<br></span><span>• Capacidad de etiquetas sin revestimiento para reducir los<br>desechos - más etiquetas por rollo<br></span><span>• Variedad de opciones de montaje - soporte para el<br>cinturón, soporte de pared, montaje de carrito, soporte<br>para vehículo</span><br>', 'product', 1, 'es', 'mx', 40),
(214, 'Caracteristicas DP-520', '<span>• Fácil de usar... con una sola mano, sistema fácil de<br>carga de papel</span><span><br>• Tamaño ultra-portable con el tamaño más pequeño -<br>pesa sólo 446 gramos, con batería y media<br></span><span>• Capacidad de etiquetas sin revestimiento para reducir los<br>desechos - más etiquetas por rollo<br></span><span>• Variedad de opciones de montaje - soporte para el<br>cinturón, soporte de pared, montaje de carrito, soporte<br>para vehículo</span><br>', 'product', 1, 'es', 'pe', 40);
INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(215, 'Caracteristicas DP-520 BO CO', '<span>Método de impresiónTérmica directaResolución203 dpi (8 puntos mm)Velocidad de impresiónHasta 2,0 ipsFuentesFuentes: 7 x 24, 10 x 24. Software de utilidad Font Editor permite la personalización de fuentes de caracteres por el usuarioConjunto de caracteresConjunto de caracteres IBM ® 224, CP437Conectividad estándarSerie RS232 (conector RJ12)Conectividad opcionalBluetoothEmulaciónEpson ESC lenguaje de impresora POSDriversWin98, WinNT, Win2k, WinXP, WIN 7.UtilidadesPrograma de instalación de la impresora remota proporciona una cómoda configuración a través de una interfaz de Windows desde cualquier ordenador.Tamaño del rollo de papelMáximo diámetro del rollo 2,16 "(57,5 cm), anchos de rollo a 2.26" (55 mm)Capacidad de la bobinaAproximadamente 124 ''(37.7 m)Tipo de rolloAdmite papel con y sin núcleoTipo de pilaRecargable NiMH de células 5 (6V, 1800 mAH)AprobacionesMarca CE, adaptadores de CA aprobados por UL, E-mark en los adaptadores en el vehículo, AJAPeso físico1,0 libras (446g) con batería y papelTamaño físico (L x W x H)7,0 "x 5,8" x 3,3 "(17,8 x 14,8 x 3,3 cm)Voltaje de alimentación120-240 V amplia gama de detección automáticaTemperatura de funcionamiento32 ° F a 122 ° F (0 ° C a 50 ° C)Temperatura de almacenamiento-4 ° F a 140 ° F (-20 ° C a 60 ° C)Humedad10% a 90% RH (sin condensación)MTBF&gt; 13 millones de líneas de impresión (aproximadamente 3.500 rollos de papel)<span>Opciones:<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>Adaptador de corriente para vehículo, 12 a 24 VDC<br>Clip de cinturón<br>D9 a RJ12 Cable de datos con conector jack para carga alternativa<br>Estuche ambiental<br>Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'bo', 40),
(216, 'Caracteristicas DP-520 BO CO', '<span>Método de impresiónTérmica directaResolución203 dpi (8 puntos mm)Velocidad de impresiónHasta 2,0 ipsFuentesFuentes: 7 x 24, 10 x 24. Software de utilidad Font Editor permite la personalización de fuentes de caracteres por el usuarioConjunto de caracteresConjunto de caracteres IBM ® 224, CP437Conectividad estándarSerie RS232 (conector RJ12)Conectividad opcionalBluetoothEmulaciónEpson ESC lenguaje de impresora POSDriversWin98, WinNT, Win2k, WinXP, WIN 7.UtilidadesPrograma de instalación de la impresora remota proporciona una cómoda configuración a través de una interfaz de Windows desde cualquier ordenador.Tamaño del rollo de papelMáximo diámetro del rollo 2,16 "(57,5 cm), anchos de rollo a 2.26" (55 mm)Capacidad de la bobinaAproximadamente 124 ''(37.7 m)Tipo de rolloAdmite papel con y sin núcleoTipo de pilaRecargable NiMH de células 5 (6V, 1800 mAH)AprobacionesMarca CE, adaptadores de CA aprobados por UL, E-mark en los adaptadores en el vehículo, AJAPeso físico1,0 libras (446g) con batería y papelTamaño físico (L x W x H)7,0 "x 5,8" x 3,3 "(17,8 x 14,8 x 3,3 cm)Voltaje de alimentación120-240 V amplia gama de detección automáticaTemperatura de funcionamiento32 ° F a 122 ° F (0 ° C a 50 ° C)Temperatura de almacenamiento-4 ° F a 140 ° F (-20 ° C a 60 ° C)Humedad10% a 90% RH (sin condensación)MTBF&gt; 13 millones de líneas de impresión (aproximadamente 3.500 rollos de papel)<span>Opciones:<span>Adaptador de CA de la pared, de 100 a 240 VAC<br>Adaptador de corriente para vehículo, 12 a 24 VDC<br>Clip de cinturón<br>D9 a RJ12 Cable de datos con conector jack para carga alternativa<br>Estuche ambiental<br>Baterías de repuesto en paquetes múltiples</span></span></span><br>', 'product', 1, 'es', 'co', 40),
(217, 'Caracteristicas OS2X', '<table border="1" cellpadding="1" cellspacing="1" style="height:623px; width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Caracter&iacute;sticas</strong></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Clase de precisi&oacute;n:</strong></td>\r\n			<td>III</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Tara: </strong></td>\r\n			<td>-5.998Kg | -14.998Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Intervalo de verificaci&oacute;n: </strong></td>\r\n			<td>e=2g/5g</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Resoluci&oacute;n interna o precisi&oacute;n: </strong></td>\r\n			<td>1/30000</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Protector de sobrecarga</strong></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Teclado: </strong></td>\r\n			<td>Al pasar el 120%</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Software: </strong></td>\r\n			<td>OS2 Manager</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Compatibilidad: </strong></td>\r\n			<td>POS, ECR</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Capacidad:</strong></td>\r\n			<td>15Kg | 30Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Par&aacute;metros f&iacute;sicos</strong></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Botones t&aacute;ctiles:</strong></td>\r\n			<td>Bot&oacute;n de Encendido, Tara y Cero</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Pantalla:</strong></td>\r\n			<td>LCD 5x8 campos</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Dimensiones: </strong></td>\r\n			<td>34x26x10cm</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Peso: </strong></td>\r\n			<td>3.7Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Bandeja: </strong></td>\r\n			<td>Acero inoxidable de 34x26x1.5cm</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Patas para base: </strong></td>\r\n			<td>De tornillo, ajustables</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Base: </strong></td>\r\n			<td>Met&aacute;lica</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Fuente de alimentaci&oacute;n: </strong></td>\r\n			<td>120V AC a 5V DC, 500mA</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Interfaces:</strong></td>\r\n			<td>RS 232, RJ-11</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Entorno operativo</strong></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Temperatura de trabajo:</strong></td>\r\n			<td>0 &deg;C ~ +40 &deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Temperatura de almacenamiento: </strong></td>\r\n			<td>-20 &deg;C ~ +70 &deg;C</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Humedad relativa:</strong></td>\r\n			<td>5% ~ 85%</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 'co', 20),
(218, 'Descripcion OS2X', '<p>OS2X Balanza Con dise&ntilde;o Simple elegante y robusta, para trabajar en interfaz con sistemas de punto de venta y/o Cajas registradoras, posee protocolos de f&aacute;cil integraci&oacute;n adaptado al mercado, con rendimiento de alta sensibilidad, impermeable, anti-incrustantes y f&aacute;cil de limpiar. Se alimenta directamente desde un puerto USB o ECR, su consumo es de tan solo 5V 500mA. Maneja un sistema de ajuste de la aceleraci&oacute;n de la gravedad y su ajuste de par&aacute;metros m&eacute;tricos o calibraci&oacute;n mediante el software de la balanza.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'product', 1, 'es', 'co', 20),
(219, 'Descripcion HKA 1080 CO', '<p>HKA 1080 Terminal Punto de Venta, posee un dise&ntilde;o compacto, robusto y elegante, equipado con un CPU ARM de 4 GHz, impresor t&eacute;rmico de 3 pulgadas y una pantalla t&aacute;ctil de 9.7 Pulgadas Multi-Touch. Ideal para Pyme.</p>\r\n', 'product', 1, 'es', 'co', 43),
(220, 'Caracteristicas HKA 1080 CO', '<table border="0" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Equipo</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>CPU: </strong>ARM Cortec A9 4GHz</p>\r\n\r\n			<p><strong>Sistema Operativo: </strong>Android, Linux</p>\r\n\r\n			<p><strong>Memoria RAM: </strong>1GB DDR2</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Monitor</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Tama&ntilde;o: </strong>9.7&quot; monitor LCD</p>\r\n\r\n			<p><strong>Brillo: </strong>250 nits</p>\r\n\r\n			<p><strong>Resoluci&oacute;n: </strong>1024 x 768 p&iacute;xeles</p>\r\n\r\n			<p><strong>Pantalla: </strong>T&aacute;ctil Capacitiva multi-touch</p>\r\n\r\n			<p><strong>Inclicaci&oacute;n: </strong>15&deg;/35&deg;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Dispositivos de Almacenamiento</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Disco Duro: </strong>4GB de Flash</p>\r\n\r\n			<p><strong>Tarjetas: </strong>Expansion a 32GB MicroSD (TF Card)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Puertos E/S</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Serie: </strong>2 RS232</p>\r\n\r\n			<p><strong>USB: </strong>2 USB 2.0</p>\r\n\r\n			<p><strong>LAN: </strong>Puerto de red 1 RJ-45 (10/100M)</p>\r\n\r\n			<p><strong>Gaveta: </strong>1 RJ-11 (24V)</p>\r\n\r\n			<p><strong>Interfaz de audio: </strong>1 host Audio 1 X 3G</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Componentes</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Impresora: </strong>T&eacute;rmica con cortadora de 80 mm</p>\r\n\r\n			<p><strong>Pantalla para cliente: </strong>4 lineas LCD</p>\r\n\r\n			<p><strong>WiFi: </strong>802.11a</p>\r\n\r\n			<p><strong>Bluetooth: </strong>Bluetooth 4.0</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Opcional</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Lector de tarjetas magn&eacute;ticas: </strong>Lector de tarjetas de banda magn&eacute;tica (MSR), ID lector de tarjetas</p>\r\n\r\n			<p><strong>3G: </strong>WCDMA</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Otros</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Fuente de poder: </strong>Externa 24V fuente de alimentaci&oacute;n</p>\r\n\r\n			<p><strong>Color: </strong>Blanco/Negro</p>\r\n\r\n			<p><strong>Peso: </strong>3Kg</p>\r\n\r\n			<p><strong>Dimensiones: </strong>282 &times; 250 &times; 170 ( mm )</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 43),
(221, 'Descripcion AO1X', '<p>All In One ARM POS AO1X Punto de venta, Dise&ntilde;ado bajo el concepto &quot;CLOUD POS&quot; que combina un procesador ARM de alta eficiencia y una baja calidad de consumo en los sistemas Windows CE, Linux y Android, cuenta con una pantalla m&aacute;s brillante que la pantalla com&uacute;n del mercado, consumiendo 1/20 en comparaci&oacute;n a los POS/PC mercado, ahorrando 200 Kw Hr por a&ntilde;o.</p>\r\n\r\n<p>Cuenta con puerto ethernet para transacciones en l&iacute;nea o Software POS via Web, Lector de Tarjetas Micro SD para expancion hasta 32 Gb, Puerto para gaveta, UPS 3Ah y perifericos opcionales como I-Button Control de Acceso, GPRS, Lector RFID entre otros.</p>\r\n', 'product', 1, 'es', 'co', 53),
(222, 'Caracteristicas AO1X', '<table border="0" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Tarjeta Principal</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Procesador: </strong>ARM 13 1GHz</p>\r\n\r\n			<p><strong>Flash ROM: </strong>512MB</p>\r\n\r\n			<p><strong>Almacenamiento externo: </strong>Tarjeta micro SD expandible a 32GB</p>\r\n\r\n			<p><strong>Flash ROM: </strong>512MB</p>\r\n\r\n			<p><strong>Fuente de poder: </strong>DC 13.8V</p>\r\n\r\n			<p><strong>Sistema operativo: </strong>Windows CE / Linux / Android</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Pantalla</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Display del operador: </strong>7&rdquo; 600*800 TFT LCD</p>\r\n\r\n			<p><strong>Display del cliente: </strong>32*144 LCD / 65*132 LCD</p>\r\n\r\n			<p><strong>Panel t&aacute;ctil: </strong>T&aacute;ctil capacitivo</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Puertos E/S</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Puerto serial: </strong>RJ11 y 4p4c</p>\r\n\r\n			<p><strong>Puerto LAN: </strong>RJ45, 10M / 100M</p>\r\n\r\n			<p><strong>Puerto USB: </strong>4 USB 2.0; 1 mini USB (OTG)</p>\r\n\r\n			<p><strong>Puerto para gavetas: </strong>1 RJ11, DC-24V</p>\r\n\r\n			<p><strong>Ranura para tarjeta SD: </strong>1 tarjeta SD</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Impresora</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Impresora: </strong>T&eacute;rmica</p>\r\n\r\n			<p><strong>Papel: </strong>T&eacute;rmico 57mm/2&quot;</p>\r\n\r\n			<p><strong>Velocidad de impresi&oacute;n: </strong>Max. 150mm/s</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Opciones</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>MSR: </strong>3 v&iacute;as para lectura magn&eacute;tica</p>\r\n\r\n			<p><strong>RFID: </strong>M&oacute;dulo lector Mifare</p>\r\n\r\n			<p><strong>I-Button: </strong>Control de acceso</p>\r\n\r\n			<p><strong>Wireles: </strong>LAN 802.11 b / g / n</p>\r\n\r\n			<p><strong>GRPS: </strong>850 / 900 / 1800 / 1900 MHz</p>\r\n\r\n			<p><strong>WCDMA: </strong>850 / 900 / 2100MHz</p>\r\n\r\n			<p><strong>Bater&iacute;a: </strong>7.4V 3Ah</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Dimensiones</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Dimensiones: </strong>altura 272mm * ancho 152mm * profundidad 106mm</p>\r\n\r\n			<p><strong>Peso: </strong>0.8Kg</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 53),
(223, 'Descripcion AO4X', '<p>All In One ARM POS AO4X Punto de venta, Dise&ntilde;ado bajo el concepto &quot;CLOUD POS&quot; que combina un procesador ARM de alta eficiencia y una baja calidad de consumo en los sistemas Windows CE, Linux y Android, cuenta con una pantalla m&aacute;s brillante que la pantalla com&uacute;n del mercado, consumiendo 1/20 en comparaci&oacute;n a los POS/PC mercado, ahorrando 200 Kw Hr por a&ntilde;o. Cuenta con puerto ethernet para transacciones en l&iacute;nea o Software POS via Web, Lector de Tarjetas Micro SD para expancion hasta 32 Gb, Puerto para gaveta, UPS 3Ah y perifericos opcionales como I-Button Control de Acceso, GPRS, Lector RFID entre otros.</p>\r\n', 'product', 1, 'es', 'co', 54),
(224, 'Caracteristicas AO4X', '<table border="0" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Tarjeta Principal</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Procesador: </strong>Single-core ARM 13 1GHz / Dual-core ARM 15 1 GHz</p>\r\n\r\n			<p><strong>Flash ROM: </strong>512MB / 8GB (eMMC)</p>\r\n\r\n			<p><strong>SD RAM: </strong>512MB / 1GB</p>\r\n\r\n			<p><strong>Flash ROM: </strong>512MB</p>\r\n\r\n			<p><strong>Fuente de poder: </strong>DC 13.8V</p>\r\n\r\n			<p><strong>Altavoz: </strong>8 Ohms,1W</p>\r\n\r\n			<p><strong>Sistema operativo: </strong>Windows CE / Linux / Android</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Pantalla</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Pantalla del operador: </strong>10&rdquo; TFT LCD (1280*800)</p>\r\n\r\n			<p><strong>Pantalla del cliente: </strong>2 l&iacute;neas 32*144 LCD / 4 l&iacute;neas 65*132 LCD</p>\r\n\r\n			<p><strong>Panel t&aacute;ctil: </strong>T&aacute;ctil capacitivo</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Puertos E/S</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Puerto serial: </strong>RJ11 y 4p4c</p>\r\n\r\n			<p><strong>Puerto LAN: </strong>RJ45, 10M / 100M</p>\r\n\r\n			<p><strong>Puerto USB: </strong>4 USB 2.0; 1 mini USB (OTG)</p>\r\n\r\n			<p><strong>Puerto para gavetas: </strong>1 RJ11, DC-24V</p>\r\n\r\n			<p><strong>Ranura para tarjeta SD: </strong>1 tarjeta SD</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Impresora</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Impresora: </strong>T&eacute;rmica de coradora autom&aacute;ica</p>\r\n\r\n			<p><strong>Papel: </strong>T&eacute;rmico 57mm/2&quot;</p>\r\n\r\n			<p><strong>Velocidad de impresi&oacute;n: </strong>Max. 150mm/s</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Opciones</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>MSR: </strong>3 v&iacute;as para lectura magn&eacute;tica</p>\r\n\r\n			<p><strong>RFID: </strong>M&oacute;dulo lector RFID</p>\r\n\r\n			<p><strong>I-Button: </strong>Control de acceso I-Button</p>\r\n\r\n			<p><strong>Wireles: </strong>LAN 802.11 b / g / n</p>\r\n\r\n			<p><strong>GRPS: </strong>850 / 900 / 21800 / 1900 MHz</p>\r\n\r\n			<p><strong>WCDMA: </strong>850 / 900 / 2100MHz</p>\r\n\r\n			<p><strong>Bater&iacute;a: </strong>7.4V 3Ah</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Dimensiones</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Dimensiones: </strong>297x276x135mm</p>\r\n\r\n			<p><strong>Peso: </strong>1.5Kg</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 54),
(225, 'Descripcion PT05', '<p>Terminal de punto de venta, con dise&ntilde;o elegante y compacto capaz de adaptarce a cualquier entorno del mercado. Destaca en rendimiento gracias a su disco de estado s&oacute;lido en conjunto con el procesador Intel Dual Core ATOM, y a los perif&eacute;ricos incluidos y opcionales que el mismo ofrece. Visor incluido de cliente VFD matricial, opcional a pantalla de 10/12/15 pulgadas full color. Impresor t&eacute;rmico incluido de 3 pulgadas con auto-Cutter con una velocidad de impresi&oacute;n de 150mm/s. Pantalla de operador t&aacute;ctil capacitiva de 15 Pulgadas m&aacute;s brillante y sensible que la resistiva, con inclinaci&oacute;n variable entre 30&deg; a 75&deg;, operando bajo la plataforma Windows 7.</p>\r\n', 'product', 1, 'es', 'co', 55),
(226, 'Caracteristicas PT05', '<table border="0" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Tarjeta Principal</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Tarjeta principal: </strong>Tarjeta de control industrial ATOM de bajo consumo</p>\r\n\r\n			<p><strong>Procesador: </strong>Intel dual core ATOM N2600 1.6GHz</p>\r\n\r\n			<p><strong>RAM: </strong>2GB</p>\r\n\r\n			<p><strong>Capacidad de almacenamiento: </strong>SSD de estado s&oacute;lido de 32GB / 60GB / 120GB</p>\r\n\r\n			<p><strong>Fuente de poder: </strong>12V 5A</p>\r\n\r\n			<p><strong>Sistema Operativo: </strong>Windows 7 y POS Ready</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Visor</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Visor del operador: </strong>15&rdquo;(1024*768)</p>\r\n\r\n			<p><strong>Panel T&aacute;ctil: </strong>Capacitivo</p>\r\n\r\n			<p><strong>Visor de cliente (opcional): </strong>2*20 doble l&iacute;nea VFD/ 24*128 matriz de punto VFD/ 9 d&iacute;gitos VFD<br />\r\n			Visor de 15&rdquo;/12&rdquo;/10&rdquo; Full color</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Puertos E/S</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Puerto serial: </strong>1 Externo, 1 Interno</p>\r\n\r\n			<p><strong>Puerto Ethernet: </strong>1</p>\r\n\r\n			<p><strong>Puerto para gavetas: </strong>1</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Impresor</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Impresora: </strong>T&eacute;rmica de coradora autom&aacute;ica</p>\r\n\r\n			<p><strong>Papel: </strong>T&eacute;rmico 57mm/2&quot;</p>\r\n\r\n			<p><strong>Velocidad de impresi&oacute;n: </strong>Max. 150mm/s</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Opciones</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>Impresor 2&rdquo; Con/Sin Cutter: 57mm*80mm</p>\r\n\r\n			<p>3&rdquo; con cutter&gt; 80mm*80mm</p>\r\n\r\n			<p><strong>Velocidad de impresi&oacute;n: </strong>150mm/s</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Caracter&iacute;sticas f&iacute;sicas</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Dimensiones: </strong>373mmx241mmx498mm</p>\r\n\r\n			<p><strong>Peso: </strong>4.5Kg (no incluye visor de cliente)</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 55),
(227, 'Descripcion HKA 380', '<p>HKA 380 Puede trabajar como Colector de datos o PDA (Personal Digital Assistant), es un equipo de alto rendimiento gracias a su CPU Quad-Core de 1.4GHz, sistema operativo de Android 4.2.2 y emplea una pantalla t&aacute;ctil de 3.5 pulgadas IPS, lo cual resulta en mayor velocidad y alcance, 4-5 veces mayor que el producto tradicional de un n&uacute;cleo.</p>\r\n\r\n<p>Posee varios modos de adquisici&oacute;n de datos incluyendo NFC, RFID, C&oacute;digo de barras unidimensional (1D) y bidimensional (2D), IR y Recopilaci&oacute;n de Im&aacute;genes. integra para la transmisi&oacute;n de datos GPRS, 3G, WIFI, Bluetooth.</p>\r\n\r\n<p>Es ideal para la gesti&oacute;n log&iacute;stica, Verificaci&oacute;n de almacenes, Gesti&oacute;n de inventario, Ventas M&oacute;viles, Rastreo de productos, atenci&oacute;n hospitalaria, Ventas al por mayor, gesti&oacute;n de productos y muchos ambientes m&aacute;s.</p>\r\n', 'product', 1, 'es', 'co', 52),
(228, 'Caracteristicas HKA 380', '<table border="0" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Par&aacute;metros B&aacute;sicos</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>CPU: </strong>Samsung Exynos 4412 Quad-Core 1.4GHz</p>\r\n\r\n			<p><strong>Sistema Operativo: </strong>Android 4.2.2</p>\r\n\r\n			<p><strong>Memoria RAM: </strong>DDR3 1GB</p>\r\n\r\n			<p><strong>Memoria NandFlash: </strong>4GB</p>\r\n\r\n			<p><strong>Memoria Extendida: </strong>Micro SD (TF) 32G m&aacute;ximo</p>\r\n\r\n			<p><strong>Pantalla: </strong>3.5 Pulgadas, Pantalla IPS multit&aacute;ctil capacitiva, 320x480</p>\r\n\r\n			<p><strong>Teclado: </strong>24 teclas f&iacute;sicas (incluyendo teclas num&eacute;ricas, de funciones y encendido), 2 botones laterales para uso personalizado</p>\r\n\r\n			<p><strong>Bater&iacute;a: </strong>Bater&iacute;a de pol&iacute;mero de litio recargable de 4000mAH (opcional bater&iacute;a de pol&iacute;mero de litio de amplia capacidad, 6000mAH)</p>\r\n\r\n			<p><strong>Dimensaiones: </strong>156x72x31mm (Alto x Ancho x Largo)</p>\r\n\r\n			<p><strong>Peso: </strong>312g (Incluyendo la bater&iacute;a de 4000mAh)</p>\r\n\r\n			<p><strong>C&aacute;mara: </strong>8 Megap&iacute;xeles con AutoFoco y Flash</p>\r\n\r\n			<p><strong>Ranura para tarjetas: </strong>Micro SD (TF)x1, SIMx1, PSAMx1</p>\r\n\r\n			<p><strong>Accesorios: </strong>USB 2.0x1, Interfaz Multifunci&oacute;n sin contacto (soporta USB, RS232, RS485 y otras transmisiones de datos) x1</p>\r\n\r\n			<p><strong>Audio: </strong>Altavoz x1, Micr&oacute;fono x1, Aud&iacute;fonos x1</p>\r\n\r\n			<p><strong>Telefon&iacute;a: </strong>Soporte para llamadas de voz m&oacute;viles en Alta Definici&oacute;n</p>\r\n\r\n			<p><strong>Vibrador: </strong>Si</p>\r\n\r\n			<p><strong>Volumen: </strong>Alto volumen de altavoz de hasta 90dB, f&aacute;cil de escuchar en un ambiente ruidoso</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Par&aacute;metros de Adquisici&oacute;n de Datos</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>C&oacute;digos de Barras: </strong>C&oacute;digos 1D: Code39, Codabar, Code128, Code93, Entrelazado 2 de 5, UPC-A, UPC-E, EAN8, EAN13, EAN128, MSI, ISBN/ISSN<br />\r\n			C&oacute;digos 2D: PDF417, QR (QR1, QR2, MicroQR) Datamatrix, GS1-DataBarTM (RSS Limitado, RSS-14, RSS-14 apilado, RSS Expandido)</p>\r\n\r\n			<p><strong>NFC/RFID : </strong>Soporta ISO/IEC 18092, ISO/IEC 21481, ISO/IEC 14443 Tipos A, B, B&rsquo;, JIS(X)6319-4, ISO/IEC 15693</p>\r\n\r\n			<p><strong>WiFi: </strong>IEEE 802.11a/b/g/n, posorta 2.4GHz, 5.0GHz modo frecuencia dual</p>\r\n\r\n			<p><strong>Bluetooth: </strong>Soporta versi&oacute;n de protocolo 4.0+EDR</p>\r\n\r\n			<p><strong>Bandeja: </strong>Acero inoxidable de 34x26x1.5cm</p>\r\n\r\n			<p><strong>Ubicaci&oacute;n por sat&eacute;lite : </strong>GPS/A-GPS, Beidou</p>\r\n\r\n			<p><strong>Infrarrojo: </strong>Comunicaci&oacute;n por Infrarrojo 38K</p>\r\n\r\n			<p><strong>GPRS/3G: </strong>WCDMA, CDMA2000, GPRS, GSM</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Condiciones de Trabajo</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Temperatura de trabajo: </strong>-10&deg;C ~ +50&deg;C</p>\r\n\r\n			<p><strong>Temperatura de almacenamiento: </strong>-20&deg;C ~ +70&deg;C</p>\r\n\r\n			<p><strong>Humedad relativa: </strong>5%RH ~ 90%RH (sin condensaci&oacute;n)</p>\r\n\r\n			<p><strong>Protecci&oacute;n industrial: </strong>Grado de protecci&oacute;n IP65, 6 lados. Resiste el impacto de ca&iacute;das a 1,5 m sobre suelo de cemento varias veces dentro del margen de temperatura de operaci&oacute;n</p>\r\n\r\n			<p><strong>Componentes est&aacute;ndar: </strong>Cargador x1, Bater&iacute;a recargable de litio-pol&iacute;mero x1, Cable Mini USB x1, Disco de Programa x1, Cuerda para mu&ntilde;eca x1</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 52),
(229, 'Descripcion SPP-R310', 'Impresora móvil más versátil y robusta del mercado, desarrollada para \r\ntrabajar en todo tipo de ambientes. Cuenta con la mejor calidad de \r\nconstrucción y un alto nivel de respuesta. Posee conexión a múltiples \r\ndispositivos con interfaz Bluetooth, wifi, impresor de 3", a su vez \r\ncuenta con bater&amp;a de 2600mAh y puede imprimir papel térmico y \r\netiquetas auto-adhesivas ampliando sus áreas de trabajo en todos los \r\nsectores del mercado.', 'product', 1, 'es', 'co', 57),
(230, 'Caracteristicas SPP-R310', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n T&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Velocidad de Impresi&oacute;n</th>\r\n			<td>100mm/s papel de recibo / 80mm/s papel continuo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n de Impresi&oacute;n</th>\r\n			<td>203 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Fuentes</th>\r\n			<td>Fuente A: 12 &times; 24 Fuente B: 9 &times; 17 Fuente C: 9 &times; 24</td>\r\n		</tr>\r\n		<tr>\r\n			<th>C&oacute;digo de Barras</th>\r\n			<td>1 Dimensi&oacute;n: UPC-A, UPC-E, C&oacute;digo 39, C&oacute;digo 93, C&oacute;digo 128, EAN-8, EAN-13, ITF<br />\r\n			2 Dimensi&oacute;n: PDF417, Matriz de datos, MaxiCode, C&oacute;digo QR, Barra de datos GS1, C&oacute;digo Azteca</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Controladores</th>\r\n			<td>Windows XP(32/64bit) / WEPOS / 2003 Server(32/64bit) / VISTA(32/64 bit) / 2008 Server(32/64bit) / 7(32/64bit) / 8(32/64bit) / 10(32/64bit)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sensores</th>\r\n			<td>Fin de papel y tapa abierta</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Emulaci&oacute;n</th>\r\n			<td>BXL / POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel</th>\r\n			<td>T&eacute;rmico Recibo: 50mm / Continuo: 49mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Cargador de Bater&iacute;</th>\r\n			<td>Entrada 100 ~ 240 VAC 0,5A / Salida 8.4 VDC 0.8 A</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Bater&iacute;</th>\r\n			<td>Ion Litio 7,4VDC 2600 mA/h</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Lector de Tarjetas</th>\r\n			<td>MSR ISO 7810/7811/7812, Carriles triples (1 &amp; 2 &amp; 3 carriles) Encriptaci&oacute;n T- DES y gesti&oacute;n de teclas DUKPT</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>105mm x 126mm x 58mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>1Kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Incluye</th>\r\n			<td>Bater&iacute;a, Cargador de la bater&iacute;a, Papel, Broche para cintur&oacute;n, CD</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 57),
(231, 'Caracteristicas HKA5803', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Impresi&oacute;n T&eacute;rmica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Papel &nbsp;</th>\r\n			<td>T&eacute;rmico 2&quot; Pulgadas (58mm)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Ancho de Impresi&oacute;n</th>\r\n			<td>32 Caracteres/L&iacute;nea (Altura de fuente 24 puntos)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Caracteres compatibles</th>\r\n			<td>GBK,BIG5,Unicode(UTF8)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Vida &Uacute;til</th>\r\n			<td>50KM ?Densidad de Impresi&oacute;n 25%?</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n de Impresi&oacute;n</th>\r\n			<td>209 ppp</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Interfaces</th>\r\n			<td>RS232C/Bluetooth/USB</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conexi&oacute;n</th>\r\n			<td>Bluetooth V2.0 o 4.0(IOS)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Alimantaci&oacute;n</th>\r\n			<td>Bater&iacute;a Recargable 1400 mAh/7.4V</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Longitud de Impresi&oacute;n</th>\r\n			<td>100-130 m(12.5% Densidad de impresi&oacute;n)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Alarma/avisos</th>\r\n			<td>Papel agotado, bater&iacute;a baja, no convencional</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Carga de papel</th>\r\n			<td>Compartimiento carga F&aacute;cil de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Cortador de papel</th>\r\n			<td>Manual</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Protocolo</th>\r\n			<td>Compatible con comandos ESC/POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Accesorios Incluidos</th>\r\n			<td>Papel de impresi&oacute;n t&eacute;rmica, adaptador, bater&iacute;a y estuche protector</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 56),
(232, 'Descripcion HKA5803', '<p>La impresora m&oacute;vil HKA 5803, cuenta con una construcci&oacute;n durable, un estuche protector incluido y una bater&iacute;a recargable de 1400 mAh que permite la impresi&oacute;n a la intemperie, ideal para redes de transporte de encomiendas, facturaci&oacute;n en espacios de dif&iacute;cil acceso entre otros sectores del mercado. Cuenta con impresor t&eacute;rmico de 2&quot; Pulgadas, conexi&oacute;n USB, Bluetooth y RS232, f&aacute;cil de emparejar a dispositivos m&oacute;viles Android y Windows.</p>\r\n', 'product', 1, 'es', 'co', 56),
(233, 'Texto', '¡ LOS LÍDERES DEL MERCADO!', 'nosotros', 1, 'es', 'co', NULL),
(234, 'Texto', 'Somos una empresa internacional, ágil y moderna con presencia en Venezuela, Panamá, Colombia, México, Perú, República Dominicana, Bolivia y USA. A través de nuestros distribuidores autorizados y empresas aliadas tenemos representantes en toda América con proyección mundial. Brindamos una amplia gama de soluciones a nuestros clientes, siempre en busca de garantizar la calidad de nuestros servicios y productos destinados al sector del comercio en general.', 'nosotros', 1, 'es', 'co', NULL),
(235, 'Texto', 'Misión\n\n', 'nosotros', 1, 'es', 'co', NULL),
(236, 'Texto', 'Generar soluciones y productos de Alta Tecnologia y valor agregado para el comercio, a través de un equipo de trabajo capacitado y ágil brindando a nuestros Clientes en Latinoamericá efectivos de calidad. ', 'nosotros', 1, 'es', 'co', NULL),
(237, 'Texto', 'Visión', 'nosotros', 1, 'es', 'co', NULL),
(238, 'Texto', 'Ser una empresa con reconocimiento internacional, líder en innovación tecnológica en el desarrollo de soluciones fiscales y productos para el comercio en general. ', 'nosotros', 1, 'es', 'co', NULL),
(239, 'Texto', 'Desarrollo', 'nosotros', 1, 'es', 'co', NULL),
(240, 'Texto', 'Hardware y Circuitos <br>\r\nFirmware de sistemas fiscales<br>\r\nSoftware<br>\r\nDesarrollos Web<br>', 'nosotros', 1, 'es', 'co', NULL),
(241, 'Texto', 'Soporte', 'nosotros', 1, 'es', 'co', NULL),
(242, 'Texto', 'Asesoría Técnica <br>\r\nIntegración de Sistemas<br>\r\nServicio Tecnico<br>', 'nosotros', 1, 'es', 'co', NULL),
(243, 'Texto', 'Ventas', 'nosotros', 1, 'es', 'co', NULL),
(244, 'Texto', 'Asesoría <br>\r\nOfertas<br>\r\nExperiencia<br>', 'nosotros', 1, 'es', 'co', NULL),
(245, 'Texto', 'Ensamblaje', 'nosotros', 1, 'es', 'co', NULL),
(246, 'Texto', 'Modulos Fiscales<br>\r\nDesarrollos<br>\r\nProductos<br>', 'nosotros', 1, 'es', 'co', NULL),
(247, 'Texto', 'ACCION SOCIAL', 'nosotros', 1, 'es', 'co', NULL),
(248, 'Texto', 'Showakai KWF en el 1st Kuro-Obi Cup 2010 Orlando, USA', 'nosotros', 1, 'es', 'co', NULL),
(249, 'Texto', '	En The Factory HKA, nos enorgullece decir que somos patrocinantes del Equipo Campeón Showakai Karate-Do de Venezuela.<br />\r\n			<br />\r\n			SHOWAKAI KWF el pasado 04 de diciembre participó en el 1st KURO-OBI CUP 2010 que se realizó en el Ramada Orlando Celebration Resort and Convention Center, USA.<br />\r\n			<br />\r\n			SHOWAKAI KWF considera de la máxima importancia estimular a nuestros alumnos a que asuman retos deportivos internacionales, ya que, vivir esta gran experiencia los hace madurar en disciplina, trabajo en equipo y a conocer la responsabilidad y orgullo que implica competir defendiendo los colores patrios.', 'nosotros', 1, 'es', 'co', NULL),
(250, 'Texto', 'The Factory Softball, un equipo de campeones', 'nosotros', 1, 'es', 'co', NULL),
(251, 'Texto', 'El equipo de softball de THE FACTORY HKA nace en el año 2009 producto de la iniciativa de un grupo de empleados de la empresa que en su afán de desempeñar una labor deportiva dentro de la institución deciden unir sus esfuerzos para emprender su meta; a todas estas ganas se unieron el aporte económico por parte de la empresa quienes guiados por su Directiva los Señores Horacio Pinto y Alberto Acosta le aportan una gran ayuda para la adquisición de sus equipos deportivos e inscripciones en Campeonatos tan importante en el mundo de este deporte como lo es el YMCA en San Martin ; una vez conjugados estas dos potencias como lo son las ganas y el desempeño de cada uno de los miembros del equipo y el aporte que hace la empresa lograron en la temporada 2010-2011 obtener varios premios como lo fueron...', 'nosotros', 1, 'es', 'co', NULL),
(252, 'Texto', 'Padrinazgo de joven genio fue una aventura triunfa', 'nosotros', 1, 'es', 'co', NULL),
(253, 'Texto', '<strong>Pilar Pascual</strong>.- En 2008 se puso en marcha el sue&ntilde;o de&nbsp;<strong>un chamo de 11 a&ntilde;os que quer&iacute;a destronar, nada m&aacute;s y nada menos, a Bill Gate</strong>s. Y a los 17 a&ntilde;os hab&iacute;a dado el primer paso para lograrlo, pues en mayo recibi&oacute; en el&nbsp;<strong>Palacio de los Eventos de Maracaibo</strong>&nbsp;el diploma que lo acredita como ingeniero en Inform&aacute;tica.', 'nosotros', 1, 'es', 'co', NULL),
(254, 'Descripcion LS2', '<p>Balanza ACLAS LS2Z, versatilidad y robustez en un producto de alta competitividad. Su dise&ntilde;o de larga durabilidad permite el uso constante evitando fallas por desgaste. Teclado totalmente lavable a prueba de l&iacute;quidos. Interfaz de red con varias balanzas ACLAS LS2x y software m&aacute;nager LINK64, que permite administrar la seguridad de la balanza, transmitir datos a alta velocidad, administrar la configuraci&oacute;n del PLU, tipo de papel, bloqueo y desbloqueo de teclas, c&oacute;digo de barras personalizadas entre otras caracter&iacute;sticas.</p>\r\n', 'product', 1, 'es', 'co', 58),
(255, 'Caracteristicas LS2', '<table border="0" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Hardware</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Visor: </strong>2 LCD con retroiluminaci&oacute;n autom&aacute;tica, 4 l&iacute;neas</p>\r\n\r\n			<p><strong>Impresora: </strong>T&eacute;rmica, 56mmx206 puntos por pulgada</p>\r\n\r\n			<p><strong>Velocidad de impresi&oacute;n: </strong>80 mm/segundo</p>\r\n\r\n			<p><strong>Teclado: </strong>Num&eacute;rico y Funciones / Teclado plano de acceso directo de 112 teclas doble nivel</p>\r\n\r\n			<p><strong>Bandeja: </strong></p>\r\n\r\n			<p><strong>Teclado: </strong>Num&eacute;rico y Funciones / Teclado plano de acceso directo de 112 teclas doble nivel</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>Peso de la balanza: </strong>7,5Kg</p>\r\n\r\n			<p><strong>Interfaces Ethernet: </strong>RJ45, TCP-IP est&aacute;ndar</p>\r\n\r\n			<p><strong>Papel: </strong>T&eacute;rmico de 57mm / Etiqueta de 57mmx40mm</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Caracter&iacute;sticas</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Rango de peso: </strong>100g - 30Kg</p>\r\n\r\n			<p><strong>Exactitud: </strong>1/30000</p>\r\n\r\n			<p><strong>Consumo de energ&iacute;a: </strong>Modo de bajo consumo 3W, en Impresi&oacute;n 30W</p>\r\n\r\n			<p><strong>Alerta de sobrecarga: </strong>Alarma cuando se sobrepasa el 100.015% del peso m&aacute;ximo</p>\r\n\r\n			<p><strong>Protecci&oacute;n de sobrecarga: </strong>Protecci&oacute;n mec&aacute;nica cuando sobre pasa 120%</p>\r\n\r\n			<p><strong>Capacidad de PLU: </strong>12.000 Productos</p>\r\n\r\n			<p><strong>C&oacute;digo de barras: </strong>Ean13, Int25</p>\r\n\r\n			<p><strong>Precisi&oacute;n clase: </strong>III</p>\r\n\r\n			<p><strong>Tara: </strong>-14.998Kg</p>\r\n\r\n			<p><strong>Intervalo de verificaci&oacute;n: </strong>e=5/10g</p>\r\n\r\n			<p><strong>Dise&ntilde;o de etiquetas: </strong>Personalizado por software</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Otros</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Humedad: </strong>5%-84% H.R</p>\r\n\r\n			<p><strong>Temperatura de trabajo: </strong>0&deg;C - 40&deg;C</p>\r\n\r\n			<p><strong>Dimensiones: </strong>364mmx429mmx478,3mm</p>\r\n\r\n			<p><strong>Especificaci&oacute;n de energ&iacute;a: </strong>120 Vca, 50Hz / 60Hz 1A</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 58),
(256, 'Descripcion TS4', '<p>El primer All In One t&aacute;ctil con impresor y balanza incluidos, Dise&ntilde;ado bajo el concepto &quot;CLOUD POS&quot; que combina un procesador ARM de alta eficiencia y una baja calidad de consumo en los sistemas Windows CE y Linux, cuenta con una pantalla m&aacute;s brillante que la pantalla com&uacute;n del mercado, consumiendo 1/20 en comparaci&oacute;n a los POS/PC y balanzas del mercado, ahorrando 200 Kw Hr por a&ntilde;o, y un puerto ethernet para transacciones en l&iacute;nea o Software POS v&iacute;a Web.</p>\r\n', 'product', 1, 'es', 'co', 59),
(257, 'Caracteristicas TS4', '\r\n\r\n<table border="0" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td><p><strong>Tarjeta Principal</strong></p></td>\r\n			<td>\r\n			<p><strong>Procesardor: </strong>ARM 11 600MHz CPU</p>\r\n\r\n			<p><strong>Fuente de Poder: </strong>DC 13,4V, Bater&iacute;a</p>\r\n\r\n			<p><strong>Altavoz: </strong>8Ohms, 1W</p>\r\n\r\n			<p><strong>Sistema Operativo: </strong>Windows CE / Linux / Android</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Visor</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Visor de operador: </strong> 10&quot; TFT LCD (1280x800)</p>\r\n\r\n			<p><strong>Visor de cliente: </strong>2 l&iacute;neas 31x144LCD / 4 l&iacute;neas 65x132 LCD</p>\r\n\r\n			<p><strong>Panel T&aacute;ctil: </strong>T&aacute;ctil capacitivo</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Puertos I/O</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Puerto LAN: </strong>RJ45, 10M/100M</p>\r\n\r\n			<p><strong>Puerto USB: </strong>4 USB 2.0, 1 miniUSB (OTG)</p>\r\n\r\n			<p><strong>Puerto para gaveta: </strong>1 RJ11, DC-24V</p>\r\n\r\n			<p><strong>Puerto serial: </strong>RS232</p>\r\n\r\n			<p><strong>Ranura tarjeta SD: </strong>1 Tarjeta SD</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Impresor</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Impresor: </strong>T&eacute;rmica</p>\r\n\r\n			<p><strong>Papel t&eacute;rmico: </strong>57mm / 2&quot;</p>\r\n\r\n			<p><strong>Velocidad de impresi&oacute;n: </strong>m&aacute;x. 150 mm/s</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Opciones</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>MSR: </strong>3 V&iacute;as para lectura magn&eacute;tica</p>\r\n\r\n			<p><strong>RFID: </strong>M&oacute;dulo lector Mifare</p>\r\n\r\n			<p><strong>I-Button: </strong>Control de acceso</p>\r\n\r\n			<p><strong>Wireless LAN: </strong>802.11 b / g / n</p>\r\n\r\n			<p><strong>GPRS: </strong>850 / 900 / 21800 / 1900 MHz</p>\r\n\r\n			<p><strong>WCDMA: </strong>850 / 900 / 2100 MHz</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Balanza</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Capacidad m&aacute;xima: </strong>30Kg</p>\r\n\r\n			<p><strong>Valor de divisi&oacute;n: </strong>5/10Kg</p>\r\n\r\n			<p><strong>Tara: </strong>-14,995Kg</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Dimensiones</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Balanza: </strong>344x262mm</p>\r\n\r\n			<p><strong>General: </strong>345x471x673mm</p>\r\n\r\n			<p><strong>Peso: </strong>6,6Kg</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 59),
(258, 'Descripcion LH-30', '<p>Balanza <strong>ACLAS LH-30</strong> es un producto de alta competitividad idel para supermercados gracias a su instalaci&oacute;n a&eacute;rea. Su dise&ntilde;o de larga durabilidad permite el uso constante evitando fallas por desgaste. Teclado totalmente lavable a prueba de l&iacute;quidos. Interfaz de red con varias balanzas ACLAS LS2x, LH-30 y software m&aacute;nager LINK32, que permite administrar la seguridad de la balanza, transmitir datos a alta velocidad, administrar la configuraci&oacute;n del PLU, tipo de papel, bloqueo y desbloqueo de teclas, c&oacute;digo de barras personalizadas entre otras caracter&iacute;sticas.</p>\r\n', 'product', 1, 'es', 'co', 60),
(259, 'Caracteristicas LH-30', '<table border="0" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Hardware</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Visor: </strong>2 LCD con retroiluminaci&oacute;n autom&aacute;tica, 4 l&iacute;neas</p>\r\n\r\n			<p><strong>Impresora: </strong>T&eacute;rmica 56mm x 206ppp</p>\r\n\r\n			<p><strong>Velocidad de impresi&oacute;n: </strong>80mm/s</p>\r\n\r\n			<p><strong>Teclado: </strong>Num&eacute;rico y Funciones / Teclado plano de acceso directo de 112 teclas doble nivel</p>\r\n\r\n			<p><strong>Bandeja: </strong>Acero inoxidable 367mm</p>\r\n\r\n			<p><strong>Peso: </strong>7.5Kg</p>\r\n\r\n			<p><strong>Interfaces Ethernet: </strong>Rj45, TCP-IP est&aacute;ndar</p>\r\n\r\n			<p><strong>Papel: </strong>T&eacute;rmico de 57mm / Etiqueta de 57mm x 40mm</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Caracter&iacute;cas</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Rango de peso : </strong>100g - 30Kg</p>\r\n\r\n			<p><strong>Exactitud : </strong>1/30000</p>\r\n\r\n			<p><strong>Consumo de energ6iacute;a : </strong>Modo de bajo consumo 3W, en Impresi6oacute;n 30W</p>\r\n\r\n			<p><strong>Alerta de sobrecarga : </strong>Alarma cuando se sobrepasa el 100.015% del peso m&aacute;ximo</p>\r\n\r\n			<p><strong>Protecci&oacute;n de sobrecarga : </strong>Protecci&oacute;n mec&aacute;nica cuando sobre pasa 120%</p>\r\n\r\n			<p><strong>Capacidad de PLU : </strong>12.000 Productos</p>\r\n\r\n			<p><strong>C&oacute;digo de barras: </strong>Ean13, Int25</p>\r\n\r\n			<p><strong>Precisi&oacute;n Clase : </strong>III</p>\r\n\r\n			<p><strong>Tara: </strong>-14.998Kg</p>\r\n\r\n			<p><strong>Intervalo de verificaci&oacute;n: </strong>e=5/10g</p>\r\n\r\n			<p><strong>Dise&ntilde;o etiquetas : </strong>Personalizado por software</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Otros</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Humedad : </strong>5%-84% H.R</p>\r\n\r\n			<p><strong>Temperatura de trabajo: </strong>0&deg;C - 40&deg;C</p>\r\n\r\n			<p><strong>Dimensiones : </strong>364mm x 429mm x 478,3mm</p>\r\n\r\n			<p><strong>Especificaci&oacute;n de la energ&iacute;a : </strong>120 Vca, 50Hz / 60Hz 1A</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 'co', 60),
(260, 'Caracteristicas P2506W ', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="width:200px">\r\n			<p>Velocidad de Impresi&oacute;n &nbsp;</p>\r\n			</th>\r\n			<td>22 ppm(A4) / 23 ppm(Carta)</td>\r\n		</tr>\r\n		<tr>\r\n			<th>M&eacute;todo de Impresi&oacute;n</th>\r\n			<td>Electrofotogr&aacute;fico l&aacute;ser monocrom&aacute;tica</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Resoluci&oacute;n</th>\r\n			<td>Max 1200x1200 DPI</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Retardo desde el comando de impresi&oacute;n</th>\r\n			<td>7.8 seg</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Consumible</th>\r\n			<td>Cartucho de t&oacute;ner PB-260</td>\r\n		</tr>\r\n		<tr>\r\n			<th>B&uacute;fer</th>\r\n			<td>128 MB</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Conectividad</th>\r\n			<td>Hi-Speed USB 2.0 -WIFI 802.11 b/g/n</td>\r\n		</tr>\r\n		<tr>\r\n			<th>P&aacute;ginas por cartucho</th>\r\n			<td>1600 p&aacute;ginas aproximadamente</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso</th>\r\n			<td>4,75 kg con el cartucho de t&oacute;ner</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Gramaje</th>\r\n			<td>60-163 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Dimensiones</th>\r\n			<td>34 x 22 x 18 cm</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Nivel de ruido</th>\r\n			<td>52 dB</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Tipo de papel</th>\r\n			<td>Sobres, etiquetas, trasparencias, tarjetas postales, y cartulina.</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Peso del Papel</th>\r\n			<td>52-100 g/m2</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Sistema Operativo</th>\r\n			<td>Windows Xp, Server, 7, 8, 10. Linux Debian, y Mac OSX</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 've', 35),
(261, 'Descripcion P2506W CO', '<p>Impresora L&aacute;ser de Monocromo Inal&aacute;mbrica que destaca en Velocidad, Memoria y Capacidad. Puede ser usada tanto en casa como en oficina. Ahorre en costos y energ&iacute;a. Instalela f&aacute;cil y conectela como quiera, por USB o WiFi. Su alta velocidad de impresi&oacute;n le permite tener el trabajo listo a tiempo. Imprima hasta 1.600 p&aacute;ginas por toner y maneje un volumen mensual de hasta 15.000 p&aacute;ginas. Aproveche ahora!</p>\r\n', 'product', 1, 'es', 've', 35),
(262, 'Texto', 'A nivel mundial contamos con un equipo de trabajo multidiciplinario conformado por profesionales con amplia experiencia; dedicados al desarrollo, ventas, ensamble, despacho, soporte, integracion, servicio técnico y atención al cliente. ', 'nosotros', 1, 'es', 'co', NULL),
(263, 'Texto', 'Nuestra oferta de productos abarca desde rollos de papel para impresión, (facturas fiscales, tickets de estacionaminetos, recibos en puntos de ventas (POS), extractos bancarios (ATM) y fax); facturación electrónica, impresoras fiscales, impresoras de punto de venta, impresoras láser, cajas registradoras, balanzas, contadora de billetes, ordenadores para punto de venta, consumibles, herramientas de software para integración con nuestros productos, hasta luminarias LED.   ', 'nosotros', 1, 'es', 'co', NULL),
(264, 'Descripcion DT-230 VE', '<p><strong>Impresora Fiscal Dascom DT-230</strong>&nbsp;<br />\r\n&nbsp;</p>\r\n\r\n<p>La Impresora fiscal DT-230 re&uacute;ne la robustez y fiabilidad de sus predecesoras, presenta nuevas funcionalidades como la emisi&oacute;n de notas de d&eacute;bitos, la impresi&oacute;n de c&oacute;digos QR y cuenta con puerto USB Tipo B lo que permite mayor velocidad de desempe&ntilde;o.</p>\r\n\r\n<p>Esta impresora se ajusta a las regulaciones de ley establecidas por el SENIAT para documentos y m&aacute;quinas fiscales.</p>\r\n', 'product', 1, 'es', 've', 25),
(265, 'Caracteristicas DT-230 VE', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th style="text-align:left">Velocidad de impresi&oacute;n</th>\r\n			<td>260 mm / seg, 10,2 cent&iacute;metros por segundo</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Ancho de impresi&oacute;n</th>\r\n			<td>72 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">M&eacute;todo de impresi&oacute;n</th>\r\n			<td>Directo, Impresi&oacute;n t&eacute;rmica por l&iacute;nea</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Ancho del papel</th>\r\n			<td>80mm o 58mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Resoluci&oacute;n</th>\r\n			<td>203 dpi, 8pts/mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Vida del cabezal de impresi&oacute;n</th>\r\n			<td>150 kilometros de papel</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Memoria</th>\r\n			<td>RAM: 1Mbytes; Flash: 4Mbytes</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Conectividad est&aacute;ndar</th>\r\n			<td>USB y gaveta de dinero</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Conectividad opcional</th>\r\n			<td>Serie, Paralelo, Ethernet y WiFi</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Emulaci&oacute;n</th>\r\n			<td>ESC / POS</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Di&aacute;metro m&aacute;ximo del rollo externo</th>\r\n			<td>83 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Di&aacute;metro n&uacute;cleo del rollo</th>\r\n			<td>13 mm</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Peso f&iacute;sico</th>\r\n			<td>Aproximadamente 1,8 kg</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Tama&ntilde;o f&iacute;sico L x W x H</th>\r\n			<td>147 &times; 198 &times; 146mm, 5.8x7.8x5.7 pulgadas</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Voltaje</th>\r\n			<td>AC 100-240V / 50-60Hz DC 24V &plusmn; 5%, 2,1 A</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Temperatura de funcionamiento</th>\r\n			<td>5-45 &deg; C, 41 a 113 &deg; F</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Humedad</th>\r\n			<td>10-95% sin condensaci&oacute;n</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Cortador</th>\r\n			<td>2.000.000 recortes</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Opciones</th>\r\n			<td>Cubierta Splash, placa de pared para colgar</td>\r\n		</tr>\r\n		<tr>\r\n			<th style="text-align:left">Garant&iacute;a</th>\r\n			<td>2 a&ntilde;os</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'product', 1, 'es', 've', 25);
INSERT INTO `texts` (`id`, `title`, `text`, `section`, `active`, `language_id`, `country_id`, `product_id`) VALUES
(266, 'Descripcion HKA80 Venezuela ', '<p>La&nbsp;<strong>Impresora Fiscal HKA80</strong>&nbsp;se caracteriza por su alta rapidez de impresi&oacute;n de tickets, cuenta con un dise&ntilde;o robusto y seguro cumpliendo as&iacute; las normativas fiscales del pa&iacute;s. La impresora posee un cortador autom&aacute;tico de papel, impresi&oacute;n de c&oacute;digos de barra en 1D y 2D, comunicaci&oacute;n v&iacute;a Serial o USB tipo B para la PC, puerto para Gaveta y Display externo. Por &uacute;ltimo, su sistema integrado Anti-Tamper es capaz de detectar al momento que se abre el equipo y graba esa informaci&oacute;n en la memoria de auditoria, garantizando la seguridad y robustez que se requiere en una m&aacute;quina fiscal.</p>\r\n\r\n<p>Dise&ntilde;o compacto y elegante, mayor rapidez de comunicaci&oacute;n y altas prestaciones en rendimiento.</p>\r\n\r\n<p>Esta impresora se ajusta a las regulaciones de ley establecidas por el SENIAT para m&aacute;quinas fiscales bajo la providencia SENIAT/GF/00064 de fecha 17 febrero del 2016</p>\r\n', 'product', 1, 'es', 've', 27),
(267, 'Caracteristicas HKA80 Venezuela ', '<ul>\r\n	<li><strong>M&eacute;todo de Impresi&oacute;n:</strong>&nbsp;Impresora lineal t&eacute;rmica directa.</li>\r\n	<li><strong>Velocidad de Impresi&oacute;n:</strong>&nbsp;230 mm/s&nbsp;<strong>(1)</strong>.</li>\r\n	<li><strong>Resoluci&oacute;n:</strong>&nbsp;203ppp&nbsp;<strong>(2)</strong>.</li>\r\n	<li><strong>Peso:</strong>&nbsp;1.7Kg (sin rollo).</li>\r\n	<li><strong>Dimensiones:</strong>&nbsp;195 x 177 x 147 mm.</li>\r\n	<li><strong>Papel t&eacute;mico:</strong>&nbsp;80mm de ancho.</li>\r\n	<li><strong>Alimentaci&oacute;n Externa:</strong>&nbsp;24Vcc 2A</li>\r\n	<li><strong>Memoria Fiscal:</strong>&nbsp;4000 reportes Z.</li>\r\n	<li><strong>Memoria de Auditor&iacute;a:</strong>&nbsp;2GB.</li>\r\n	<li><strong>Memoria de Trabajo:</strong>&nbsp;256KB.</li>\r\n	<li><strong>Puertos:</strong>&nbsp;3 Conectores RJ11 y 1 USB Tipo B\r\n	<ul>\r\n		<li>Gaveta.</li>\r\n		<li>Display.</li>\r\n		<li>PC (RJ11 / USB Tipo B).</li>\r\n	</ul>\r\n	</li>\r\n	<li>Cortadora autom&aacute;tica de papel (auto-cutter).</li>\r\n	<li>Panel de frontal con tres LEDs indicadores de estado y un bot&oacute;n de alimentaci&oacute;n de papel.</li>\r\n	<li>Generaci&oacute;n de facturas, notas de cr&eacute;dito, notas de d&eacute;bito y documentos no fiscales.</li>\r\n	<li>Impresi&oacute;n de logos gr&aacute;ficos, c&oacute;digos de Barra (1D y 2D).</li>\r\n	<li>Manejo de 30 cajeros</li>\r\n	<li>Maneja 24 medios de pagos.</li>\r\n	<li>8 l&iacute;neas de encabezado</li>\r\n	<li>8 l&iacute;neas de pi&eacute; de p&aacute;gina.</li>\r\n	<li>64 caracteres por l&iacute;nea.</li>\r\n	<li>Tres (3) tasas de impuesto m&aacute;s un (1) exento.</li>\r\n	<li>Funciones para cambio de formato de letra fuente: tama&ntilde;o, negrita, subrayado y fondo invertido.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>(1)</strong>&nbsp;mm/s = mil&iacute;metros por segundo&nbsp;<br />\r\n<strong>(2)</strong>&nbsp;ppp = puntos por pulgadas</p>\r\n', 'product', 1, 'es', 've', 27),
(268, 'Descripcion Tally 1125 Venezuela', '<p><strong>IMPRESORA DE MATRIZ DE PUNTO TALLY DASCOM 1125</strong><br />\r\n&nbsp;</p>\r\n\r\n<p>La DASCOM Tally 1125, es una Impresora Fiscal fabricada bajo estrictos est&aacute;ndares de calidad, que dan a su moderno dise&ntilde;o, la seguridad y la robustez necesaria para cumplir con las normativas fiscales vigentes en el pa&iacute;s.</p>\r\n\r\n<p>Es un equipo dise&ntilde;ado y adaptado a las nuevas exigencias tecnol&oacute;gicas, lo cual brinda una mayor facilidad de manejo y programaci&oacute;n.</p>\r\n\r\n<p>Es un equipo de dise&ntilde;o compacto y de altas prestaciones en rendimiento, capacidad de almacenamiento e interconexi&oacute;n con otros dispositivos.</p>\r\n', 'product', 1, 'es', 've', 28),
(269, 'Caracteristicas Tally 1125 Venezuela', '<ul>\r\n	<li><strong>M&eacute;todo de Impresi&oacute;n:</strong>&nbsp;Impresora matriz de punto.</li>\r\n	<li><strong>Velocidad de Impresi&oacute;n:</strong>&nbsp;12.3 cm/s&nbsp;<strong>(1)</strong>.</li>\r\n	<li><strong>Resoluci&oacute;n:</strong>&nbsp;360 x 360 ppp&nbsp;<strong>(2)</strong>.</li>\r\n	<li><strong>Dimensiones:</strong>&nbsp;295 x 350 x 155 mm.</li>\r\n	<li><strong>Papel:</strong>&nbsp;carta, forma cont&iacute;nua carta y media carta.</li>\r\n	<li><strong>Memoria Fiscal:</strong>&nbsp;2000 reportes Z.</li>\r\n	<li><strong>Memoria de Auditor&iacute;a:</strong>&nbsp;2GB.</li>\r\n	<li><strong>Memoria de Trabajo:</strong>&nbsp;256KB.</li>\r\n	<li><strong>Puertos:</strong>&nbsp;2 Conectores RJ11\r\n	<ul>\r\n		<li>Display.</li>\r\n		<li>PC.</li>\r\n	</ul>\r\n	</li>\r\n	<li>Cortadora de papel manual</li>\r\n	<li>Tres (3) tasas de impuesto m&aacute;s un (1) exento.</li>\r\n	<li>Generaci&oacute;n de facturas, notas de cr&eacute;dito y documentos no fiscales.</li>\r\n	<li>Impresi&oacute;n de logos gr&aacute;ficos.</li>\r\n	<li>10 l&iacute;neas de encabezado.</li>\r\n	<li>8 l&iacute;neas de pi&eacute; de p&aacute;gina.</li>\r\n	<li>80 - 136 caracteres por linea.</li>\r\n	<li>Manejo de 30 cajeros</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>(1)</strong>&nbsp;cm/s = cent&iacute;metros por segundo&nbsp;<br />\r\n<strong>(2)</strong>&nbsp;ppp = puntos por pulgadas</p>\r\n', 'product', 1, 'es', 've', 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `language_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_types_languages1_idx` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `types`
--

INSERT INTO `types` (`id`, `name`, `active`, `language_id`, `parent_id`) VALUES
(1, 'Impresoras Fiscales', 1, 'es', 7),
(2, 'Cajas Registradoras', 1, 'es', NULL),
(3, 'Impresoras Estandar', 1, 'es', 7),
(4, 'Balanzas', 1, 'es', NULL),
(5, 'Papel Térmico', 1, 'es', NULL),
(6, 'Perifericos y Otros', 1, 'es', NULL),
(7, 'Impresoras', 1, 'es', NULL),
(8, 'Impresoras  Punto de Venta', 1, 'es', 7),
(9, 'Impresoras Matriz de Puntos', 1, 'es', 7),
(10, 'Impresoras Láser', 1, 'es', 7),
(11, 'Impresoras de Especialidad', 1, 'es', 7),
(12, 'Impresoras Móviles', 1, 'es', 7),
(13, 'Impresoras Térmicas', 1, 'es', 7),
(14, 'Terminales AIO Touch', 1, 'es', NULL),
(15, 'Soluciones Móviles', 0, 'es', 7),
(16, 'Colectores de Datos', 1, 'es', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `types_countries`
--

CREATE TABLE IF NOT EXISTS `types_countries` (
  `country_id` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `product_type_text_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `url` tinytext COLLATE utf8_unicode_ci,
  `active` tinyint(4) NOT NULL,
  KEY `fk_product_type_country_countries1_idx` (`country_id`),
  KEY `fk_product_type_country_product_types1_idx` (`product_type_id`),
  KEY `fk_types_countries_product_type_texts1_idx` (`product_type_text_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `types_countries`
--

INSERT INTO `types_countries` (`country_id`, `product_type_id`, `product_type_text_id`, `order`, `url`, `active`) VALUES
('ve', 1, 1, 1, 'impresoras-fiscales', 1),
('pa', 2, 2, 2, 'cajas-registradoras', 1),
('ve', 2, 2, 2, 'cajas-registradoras', 1),
('ve', 3, 3, 3, 'impresoras-estandar', 1),
('ve', 4, 4, 4, 'balanzas', 1),
('ve', 5, 5, 5, 'papel-termico', 1),
('ve', 6, 6, 6, 'perifericos-y-otros', 1),
('bo', 8, 7, 7, 'puntoventa', 1),
('co', 8, 7, 7, 'puntoventa', 1),
('mx', 8, 7, 7, 'puntoventa', 1),
('pe', 8, 7, 7, 'puntoventa', 1),
('bo', 9, 8, 8, 'matrizpunto', 1),
('co', 9, 8, 8, 'matrizpunto', 1),
('mx', 9, 8, 8, 'matrizpunto', 1),
('pe', 9, 8, 8, 'matrizpunto', 1),
('co', 10, 9, 9, 'impresoralaser', 1),
('bo', 11, 10, 10, 'especialidad', 1),
('co', 11, 10, 10, 'especialidad', 1),
('mx', 11, 10, 10, 'especialidad', 1),
('pe', 11, 10, 10, 'especialidad', 1),
('bo', 12, 11, 4, 'moviles', 1),
('co', 12, 11, 4, 'moviles', 1),
('mx', 12, 11, 4, 'moviles', 1),
('pe', 12, 11, 4, 'moviles', 1),
('bo', 13, 12, 1, 'termicas', 1),
('co', 13, 12, 1, 'termicas', 1),
('mx', 13, 12, 1, 'termicas', 1),
('pe', 13, 12, 1, 'termicas', 1),
('bo', 14, 13, 1, 'terminales-pos', 1),
('pa', 1, 14, 2, 'impresorasfiscales', 1),
('do', 1, 15, 1, 'impresoras-fiscales', 1),
('do', 2, 16, 1, 'cajas-registradoras', 1),
('do', 5, 17, 1, 'papel-termico', 1),
('us', 8, 18, 1, 'point-of-sale-printers', 1),
('us', 9, 19, 2, 'dot-matrix-printers', 1),
('us', 13, 20, 3, 'thermal-printers', 1),
('us', 11, 21, 4, 'specialty-printers', 1),
('us', 12, 22, 5, 'movil-printers', 1),
('co', 4, 4, 4, 'balanzas', 1),
('co', 7, 23, 5, 'impresoras-general', 1),
('co', 15, 24, 10, 'solucionesmoviles', 1),
('co', 16, 25, 9, 'colectoresdedatos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `url_types`
--

CREATE TABLE IF NOT EXISTS `url_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `url_types`
--

INSERT INTO `url_types` (`id`, `name`, `active`) VALUES
(1, 'interno', 1),
(2, 'externo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(25) COLLATE utf8_spanish_ci NOT NULL,
  `last_name` char(25) COLLATE utf8_spanish_ci NOT NULL,
  `user_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 NOT NULL,
  `sexo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `phone_number` bigint(30) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_roles_id_idx` (`rol_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `user_name`, `password`, `sexo`, `email`, `phone_number`, `rol_id`, `status`) VALUES
(1, 'Anderson', 'Bracho', 'andersonj31', '2891baceeef1652ee698294da0e71ba78a2a4064', 'M', 'abracho@thefactoryhka.com', 4160111506, 1, 1),
(2, 'Johan', 'Zerpa', 'jzerpa', '1234', 'M', 'azerpa@thefactoryhka.com', 2147483647, 2, 1),
(3, 'Gabriel', 'Gomez', 'ggomez', '2891baceeef1652ee698294da0e71ba78a2a4064', 'M', 'ggomez@thefactoryhka.com', 4142023962, 2, 1),
(4, 'Jenny', 'Ramirez', 'jramirez', '2891baceeef1652ee698294da0e71ba78a2a4064', 'F', 'jramirez@thefactoryhka.com', 4241111111, 1, 1),
(7, 'Liz', 'Vielma', 'lvielma', '2891baceeef1652ee698294da0e71ba78a2a4064', 'F', 'lvielam@thefactoryhka.com', 4161111111, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles_items`
--

CREATE TABLE IF NOT EXISTS `users_roles_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rol_item_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `rol_item_id` (`rol_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=114 ;

--
-- Volcado de datos para la tabla `users_roles_items`
--

INSERT INTO `users_roles_items` (`id`, `user_id`, `rol_item_id`, `active`) VALUES
(6, 2, 8, 1),
(20, 7, 3, 1),
(21, 7, 4, 1),
(22, 7, 6, 1),
(23, 7, 2, 1),
(100, 1, 1, 1),
(101, 1, 3, 1),
(102, 1, 4, 1),
(103, 1, 5, 1),
(104, 1, 6, 1),
(105, 1, 15, 1),
(106, 1, 2, 1),
(107, 4, 1, 1),
(108, 4, 3, 1),
(109, 4, 4, 1),
(110, 4, 5, 1),
(111, 4, 6, 1),
(112, 4, 15, 1),
(113, 4, 2, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `brands_country`
--
ALTER TABLE `brands_country`
  ADD CONSTRAINT `fk_brands_country_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_brands_country_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contact_info`
--
ALTER TABLE `contact_info`
  ADD CONSTRAINT `fk_contact_info_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contact_info_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `edit_content`
--
ALTER TABLE `edit_content`
  ADD CONSTRAINT `products_id` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `featured_products_country`
--
ALTER TABLE `featured_products_country`
  ADD CONSTRAINT `fk_featured_products_country_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_featured_products_country_featured_products_type1` FOREIGN KEY (`featured_product_type_id`) REFERENCES `featured_products_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_featured_products_country_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_featured_products_country_url_types1` FOREIGN KEY (`url_type_id`) REFERENCES `url_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `fk_files_file_types1` FOREIGN KEY (`file_type_id`) REFERENCES `file_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_files_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`);

--
-- Filtros para la tabla `mainmenu`
--
ALTER TABLE `mainmenu`
  ADD CONSTRAINT `fk_mainmenu_languages` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mainmenu_mainmenu1` FOREIGN KEY (`parent_id`) REFERENCES `mainmenu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mainmenu_countries`
--
ALTER TABLE `mainmenu_countries`
  ADD CONSTRAINT `fk_mainmenu_countries_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mainmenu_countries_mainmenu1` FOREIGN KEY (`mainmenu_id`) REFERENCES `mainmenu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mainmenu_countries_type_urls1` FOREIGN KEY (`url_type_id`) REFERENCES `url_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `fk_news_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `news_country`
--
ALTER TABLE `news_country`
  ADD CONSTRAINT `fk_news_country_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_news_country_news1` FOREIGN KEY (`new_id`) REFERENCES `news` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_news_country_type_urls1` FOREIGN KEY (`url_type_id`) REFERENCES `url_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `products_files_countries`
--
ALTER TABLE `products_files_countries`
  ADD CONSTRAINT `fk_product_file_country_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_file_country_files1` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`),
  ADD CONSTRAINT `fk_product_file_country_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `products_types_countries`
--
ALTER TABLE `products_types_countries`
  ADD CONSTRAINT `fk_products_country_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_products_country_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_products_types_country_types1` FOREIGN KEY (`product_type_id`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `product_aplication`
--
ALTER TABLE `product_aplication`
  ADD CONSTRAINT `fk_apli_img` FOREIGN KEY (`aplication_images_id`) REFERENCES `aplication_images` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `product_type_texts`
--
ALTER TABLE `product_type_texts`
  ADD CONSTRAINT `fk_product_type_texts_country_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_type_texts_country_types1` FOREIGN KEY (`product_type_id`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `roles_items`
--
ALTER TABLE `roles_items`
  ADD CONSTRAINT `roles_items_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roles_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `service_country`
--
ALTER TABLE `service_country`
  ADD CONSTRAINT `service_country_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  ADD CONSTRAINT `service_country_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Filtros para la tabla `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `fk_slider_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sliders_countries`
--
ALTER TABLE `sliders_countries`
  ADD CONSTRAINT `fk_slider_country_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_slider_country_slider1` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sliders_countries_url_types1` FOREIGN KEY (`url_type_id`) REFERENCES `url_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `texts`
--
ALTER TABLE `texts`
  ADD CONSTRAINT `fk_texts_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_texts_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_texts_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `types`
--
ALTER TABLE `types`
  ADD CONSTRAINT `fk_types_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `types_countries`
--
ALTER TABLE `types_countries`
  ADD CONSTRAINT `fk_product_type_country_countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_type_country_product_types1` FOREIGN KEY (`product_type_id`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_types_countries_product_type_texts1` FOREIGN KEY (`product_type_text_id`) REFERENCES `product_type_texts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `users_roles_items`
--
ALTER TABLE `users_roles_items`
  ADD CONSTRAINT `fk_roles_item_id` FOREIGN KEY (`rol_item_id`) REFERENCES `roles_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
