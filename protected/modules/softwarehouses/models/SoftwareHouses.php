<?php

/**
 * This is the model class for table "dsi_software_houses".
 *
 * The followings are the available columns in table 'dsi_software_houses':
 * @property integer $id
 * @property string $ruta
 * @property integer $orden
 * @property string $link
 * @property integer $active
 * @property string $language
 */
class SoftwareHouses extends CActiveRecord
{
	
	public function tableName()
	{
		return 'dsi_software_houses';
	}
        
         public function behaviors(){
            return array(
                // Classname => path to Class
                'LogsableBehavior'=>'application.behaviors.LogsableBehavior',

            );
        }
	
	public function rules()
	{
		
		return array(
			array('name, orden, active, language, info', 'required'),

// ---------------------------------------------------------------------------			
			array(
					'ruta',
					'file',
					'allowEmpty' => false,
					'types' => 'jpg, gif, png',
					'on' => 'insert',
					'except' => 'update',
					'message' => 'Es necesario cargar el logo.',
					'wrongType' => 'Tipo de archivo invalido debe ser jpg, png o gif.',
					'maxSize' => 1000024,
					'maxFiles' => 1,
					'tooSmall' => 'Tamaño del archivo muy bajo debe ser mayor a 100mb.',
					'tooMany' => 'Solo puede subir un archivo.'),
// --------------------------------------------------------------------------------
			
			array('orden, active', 'numerical', 'integerOnly'=>true),
			array('name ,ruta, link', 'length', 'max'=>100),
			array('language', 'length', 'max'=>2),
		
			array('id, ruta, orden, link, active, language, info', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'ruta' => Yii::t('lang','Ruta'),
			'orden' => Yii::t('lang','Orden'),
			'link' => Yii::t('lang','Link'),
			'active' => Yii::t('lang','Active'),
			'language' => Yii::t('lang','Language'),
                        'info' => Yii::t('lang','Info'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ruta',$this->ruta,true);
		$criteria->compare('orden',$this->orden);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('language',$this->language,true);
                $criteria->compare('info',$this->info,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("active <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
