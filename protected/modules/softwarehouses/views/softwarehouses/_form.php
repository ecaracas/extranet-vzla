<?php
/* @var $this SoftwarehousesController */
/* @var $model SoftwareHouses */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'software-houses-form',
  'enableAjaxValidation'=>false,
     'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form',
        'enctype' => 'multipart/form-data',
    ),
)); ?>



        <?php //echo $form->errorSummary($model); ?>

            <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'name'); ?>
                 </div>
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>500)); ?>
                     <?php echo $form->error($model,'name'); ?>
                 </div>
                      
            </div>    


<!-- --------------------------------------------------------------------- -->

            <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'ruta'); ?>
                 </div>
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->fileField($model,'ruta',array('size'=>60,'maxlength'=>500)); ?>
                     <?php echo $form->error($model,'ruta'); ?>
                 </div>
                      
            </div>
<!-- ---------------------------------------------------------------------- -->


            <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'orden'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'orden'); ?>
    
                     <?php echo $form->error($model,'orden'); ?>
                 </div>
                      
            </div>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'info'); ?>
                 </div>
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'info'); ?>
    
                     <?php echo $form->error($model,'info'); ?>
                 </div>  
                    </div> 
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'web site'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'link',array('size'=>60,'maxlength'=>500)); ?>
    
                     <?php echo $form->error($model,'link'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php // echo $form->labelEx($model,'active'); ?>
                 </div>
                   
<!--                 <div class="col-xs-12 col-sm-9">
                    <?php //echo $form->textField($model,'active'); ?>
                       <div style="float: left;">
                        <?php
//                        $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
//                            'id' => @$model['active'],
//                            'state' => @$model['value'],
//                            'type' => 'item',
//                            'coloron' => 'color1',
//                        ));
                        ?>   
                    </div>
                     <?php echo $form->error($model,'active'); ?>
                 </div>-->
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'language'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo CHtml::dropDownList('SoftwareHouses[language]', '1', array('es' => 'es', 'en' => 'en')); ?>
                     <?php echo $form->error($model,'language'); ?>
                 </div>
                      
            </div>
                    <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                        <p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>
</p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?>
</li>
                    <li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?>
</li>
                </ul>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>
