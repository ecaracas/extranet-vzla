<?php
/* @var $this SoftwarehousesController */
/* @var $data SoftwareHouses */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('ruta')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->ruta); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('orden')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->orden); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('link')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->link); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('active')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->active); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('language')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->language); ?></div>
	
</div>

<hr />