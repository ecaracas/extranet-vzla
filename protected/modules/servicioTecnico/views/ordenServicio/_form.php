
<?php 

if (empty($rif)) { ?>
    <script>
        $(function () {
            $(".hidden_fields").hide();
            $("#registra_orden_").hide();
            $("#btn_veri_serial_").show();
        });
    </script>

<?php }else{?>
    <script>
        $(function () {
          $("#btn_veri_serial_").hide();
          $(".hidden_fields").show();
            $("#registra_orden_").show();
                  });
    </script>
<?php }?>
    
    <?php
    /* @var $this OrdenServicioController */
    /* @var $model StOrdenServicio */
    /* @var $form CActiveForm */
    ?>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'st-orden-servicio-form',
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'role' => 'form',
        ),
    ));
    ?>

<style>
    .c div{
        float: left;
    }   
</style>

    <div class="hidden_fields">

        <div>
        <table class=" table table-reflow" id="table_ordenserv_" border="0">

            <caption><strong>Cliente Final</strong></caption>
            <tbody>
                <tr>
                    <th scope="row">Razon Social:</th>
                    <!-- Si doy F5 se vacia -->
                    <td  id='razon_social'><?= ($a)?$a:'Si doy F5 se vacia =/' ?></td>
                </tr>
                <tr>
                    <th scope="row">Rif : </th>
                    <!-- se trae un valor $dv = 0 -->
                    <td  id='rif_usuario'> <?= ($dv==0)?'J':'no hola'  ?> - <?= $rif ?></td>
                </tr>

                <tr>
                    <th scope="row">Marca: </th>
                    <td  id='marca_maquina'><?= $marca ?></td>
                </tr>

                <tr>
                    <th scope="row">Modelo: </th>
                    <td  id='modelo_maquina'><?= $modelo ?></td>
                </tr>
                <tr>
                    <th scope="row">Tipo Garantia: </th>
                    <!-- no esta mostrando el tipo de garantia correcto -->
                    <td  id='modelo_maquina'><?= ($garantia)?$garantia['descripcion']:'' ?></td>
                </tr>
            </tbody>
        </table>
            
        </div>
        <div>
            <table class=" table table-reflow" id="table_ordenserv_" border="0">

            <caption><strong>Distribuidor</strong></caption>
            <tbody>
                <tr>
                    <th scope="row">Razon Social:</th>
                    <td  id='rif_usuario'></td>
                </tr>
                <tr>
                    <th scope="row">Rif : </th>
                    <td  id='rif_usuario'></td>
                </tr>

                <tr>
                    <th scope="row">Marca: </th>
                    <td  id='marca_maquina'></td>
                </tr>

                <tr>
                    <th scope="row">Modelo: </th>
                    <td  id='modelo_maquina'></td>
                </tr>
            </tbody>
        </table>
        </div>


    </div>




<br>

    <div class="form-group ">
        <div class="col-xs-12 col-sm-3">
            <?php echo $form->labelEx($model, 'serial'); ?>
        </div>

        <div class="col-xs-12 col-sm-9">


 

    
            <?php echo $form->dropDownList($model, 'serial', CHtml::listData(Operations::model()->findAll('operation_type_id=:operation_type_id', array(':operation_type_id' => '1')), 'serial', 'serial'), array('multiple' => false, 'style' => 'width:777px;', 'size' => '20', 'class' => ' js-example-basic-multiple ')); ?>

            <?php echo $form->error($model, 'serial'); ?>
            <div id="serial_error" class="errorMessage"></div>
        </div>

    </div>

    

    <div id="registra_orden_">

        <div class="form-group">
            <div class="col-xs-12 col-sm-3">
                <?php echo $form->labelEx($model, 'accesorios'); ?>
            </div>

            <div class="col-xs-12 col-sm-9">
                <?php echo $form->dropDownList($model, 'accesorios', CHtml::listData(StAccesorios::model()->findAll('estatus=:estatus', array(':estatus' => '1')), 'id', 'nombre'), array('multiple' => true, 'style' => 'width:777px;', 'size' => '20', 'class' => ' js-example-basic-multiple ')); ?>

                <?php echo $form->error($model, 'accesorios'); ?>
            </div>

        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-3">
                <?php echo $form->labelEx($model, 'descripcion_falla'); ?>
            </div>

            <div class="col-xs-12 col-sm-9">
                <?php echo $form->textArea($model, 'descripcion_falla', array('rows' => 4,)); ?>

                <?php echo $form->error($model, 'descripcion_falla'); ?>
            </div>

        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-3">
                <?php echo $form->labelEx($model, 'revision_previa'); ?>
            </div>

            <div class="col-xs-12 col-sm-9">
                <?php echo $form->textArea($model, 'revision_previa', array('rows' => 4,)); ?>

                <?php echo $form->error($model, 'revision_previa'); ?>
            </div>

        </div>

        <?php if ($rol == 2 || $rol == 9 || $rol == 4) { ?>
            <div class="form-group">
                <div class="col-xs-12 col-sm-3">
                    <?php echo $form->labelEx($model, 'fallas'); ?>
                </div>

                <div class="col-xs-12 col-sm-9">
                    <?php echo $form->dropDownList($model, 'fallas', CHtml::listData(StFallas::model()->findAll('estatus=:estatus', array(':estatus' => '1')), 'id', 'nombre_falla'), array('multiple' => true, 'style' => 'width:777px;', 'size' => '20', 'class' => 'js-example-basic-multiple')); ?>

                    <?php echo $form->error($model, 'fallas'); ?>
                </div>

            </div>
        <?php } ?>

            <!-- repuestos -->

        <?php if ($rol == 2 || $rol == 9 || $rol == 4) { ?>
            <div class="form-group">
                <div class="col-xs-12 col-sm-3">
                    <?php echo $form->labelEx($model, 'repuestos'); ?>
                </div>

                <div class="col-xs-12 col-sm-9">
                    <?php echo $form->dropDownList($model, 'repuestos', CHtml::listData(StRepuestos::model()->findAll('estatus=:estatus', array(':estatus' => '1')), 'id', 'nombre_repuestos'), array('multiple' => true, 'style' => 'width:777px;', 'size' => '20', 'class' => 'js-example-basic-multiple')); ?>

                    <?php echo $form->error($model, 'repuestos'); ?>
                </div>

            </div>
        <?php } ?>






        <div class="form-group">
            <div class="col-xs-12 col-sm-3">
                <?php echo $form->labelEx($model, 'contacto'); ?>
            </div>

            <div class="col-xs-12 col-sm-9">
                <?php echo $form->textField($model, 'contacto', array('size' => 45, 'maxlength' => 45)); ?>

                <?php echo $form->error($model, 'contacto'); ?>
            </div>

        </div>
        <div class="form-group">
            <div class="col-xs-12 col-sm-3">
                <?php echo $form->labelEx($model, 'telefono'); ?>
            </div>

            <div class="col-xs-12 col-sm-9">
                <?php echo $form->textField($model, 'telefono', array('size' => 45, 'maxlength' => 11)); ?>

                <?php echo $form->error($model, 'telefono'); ?>
            </div>

        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-3">
                <?php echo $form->labelEx($model, 'email'); ?>
            </div>

            <div class="col-xs-12 col-sm-9">
                <?php echo $form->textField($model, 'email', array('size' => 45, 'maxlength' => 45, 'id' => 'telefono_')); ?>

                <?php echo $form->error($model, 'email'); ?>
            </div>

        </div>

<!-- 
        <?php //if ($rol == 2 || $rol == 3  || $rol == 9 || $rol == 4) {
            
           // $disable = ($rol==3) ? 'disable' : '';
            //?>        

            <div class="form-group">
                <div class="col-xs-12 col-sm-3">
                    <?php // echo $form->labelEx($model, 'id_garantia'); ?>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <?php
                    // echo $form->dropDownList($model, 'id_garantia', CHtml::listData(StGarantia::model()->findAll('estatus=:estatus', array(':estatus' => '1')), 'id', 'descripcion'), array('disabled'=> $disable));

                   // ?>
                    <?php //echo $form->error($model, 'id_garantia'); ?>
                </div>
            </div>

        <?php //} ?> -->

        <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <p class="text-left"><?php echo Yii::t('lang', Yii::app()->params['camposrequeridos']); ?>
                </p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang', Yii::app()->params['save-text']), array('class' => Yii::app()->params['save-btn'], 'id' => "button_sub_")); ?>
                    </li>
                    <li class="bottom"><?php echo CHtml::link(Yii::t('lang', Yii::app()->params['cancel-text']), Yii::app()->controller->createUrl('index'), array('class' => Yii::app()->params['cancel-btn'])); ?>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="row" id="btn_veri_serial_">
        <div class="col-xs-12 col-sm-6">
            <p class="text-left"><?php echo Yii::t('lang', Yii::app()->params['camposrequeridos']); ?>
            </p>
        </div>
        <div class="col-xs-12 col-sm-6">                
            <ul class="cmenuhorizontal" id="yw0">
                <li class="bottom">
                    <?php echo CHtml::submitButton(Yii::t('lang', Yii::app()->params['serial-text']), array('class' => Yii::app()->params['cancel-btn'], 'id' => "button_change_")); ?>

                </li>
            </ul>
        </div>

    </div>


    <?php $this->endWidget(); ?>


