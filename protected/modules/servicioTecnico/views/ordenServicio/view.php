<?php
/* @var $this OrdenServicioController */
/* @var $model StOrdenServicio */

$this->breadcrumbs = array(
    'St Orden Servicios' => array('index'),
    $model->id,
);

$this->menu = array(
    array(
        'label' => Yii::t('lang', Yii::app()->params['create-text']),
        'url' => array('create'),
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    /* array(
      'label'=>Yii::t('lang',Yii::app()->params['update-text']),
      'url'=>array('update',
      'id'=>$model->id),
      'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
      'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')), */
    array(
        'label' => Yii::t('lang', Yii::app()->params['index-text']),
        'url' => array('index'),
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);

//if ($model->garantia == 0) {
//
//    $var = 'Sin garantia';
//} elseif ($model->garantia == 1) {
//    $var = 'Con garantia';
//}
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo Yii::t('lang', Yii::app()->params['view-text']) . ' ' . Yii::t('lang', 'StOrdenServicio'); ?></h3>

                <div class="menu-tool">
<?php
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'encodeLabel' => FALSE,
    'htmlOptions' => array('class' => 'cmenuhorizontal'),
));
?>
                </div>
                    <?php $this->ToolActionsRight(); ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('id_distribuidor')); ?></th>
                                <td><?php echo CHtml::encode($model->idDistribuidor->nombreusuario); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('id_tecnico')); ?></th>
                                <td><?php echo CHtml::encode($model->idTecnico->nombreusuario); ?></td>
                            </tr>

                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('marca')); ?></th>
                                <td><?php echo CHtml::encode($model->marca); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('modelo')); ?></th>
                                <td><?php echo CHtml::encode($model->modelo); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('serial')); ?></th>
                                <td><?php echo CHtml::encode($model->serial); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('fecha_enajenacion')); ?></th>
                                <td><?php echo CHtml::encode($model->fecha_enajenacion); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('descripcion_falla')); ?></th>
                                <td><?php echo CHtml::encode($model->descripcion_falla); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('revision_previa')); ?></th>
                                <td><?php echo CHtml::encode($model->revision_previa); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('contacto')); ?></th>
                                <td><?php echo CHtml::encode($model->contacto); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('telefono')); ?></th>
                                <td><?php echo CHtml::encode($model->telefono); ?></td>
                            </tr>

                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></th>
                                <td><?php echo CHtml::encode($model->email); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('fecha_registro')); ?></th>
                                <td><?php echo CHtml::encode($model->fecha_registro); ?></td>
                            </tr>

                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('garantia')); ?></th>
                                <td><?php echo CHtml::encode($model->stGarantia->descripcion); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>