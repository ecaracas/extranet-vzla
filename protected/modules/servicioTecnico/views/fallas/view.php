<?php
/* @var $this FallasController */
/* @var $model StFallas */

$this->breadcrumbs = array(
    'St Fallases' => array('index'),
    $model->id,
);

$this->menu = array(
    array(
        'label' => Yii::t('lang', Yii::app()->params['create-text']),
        'url' => array('create'),
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label' => Yii::t('lang', Yii::app()->params['update-text']),
        'url' => array('update',
            'id' => $model->id),
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label' => Yii::t('lang', Yii::app()->params['index-text']),
        'url' => array('index'),
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo Yii::t('lang', Yii::app()->params['view-text']) . ' ' . Yii::t('lang', 'Fallas'); ?></h3>

                <div class="menu-tool">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items' => $this->menu,
                        'encodeLabel' => FALSE,
                        'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
                <?php $this->ToolActionsRight(); ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('nombre_falla')); ?></th>
                                <td><?php echo CHtml::encode($model->nombre_falla); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('descripcion')); ?></th>
                                <td><?php echo CHtml::encode($model->descripcion); ?></td>
                            </tr>
                            <tr>
                                <th><?php
                                    if ($model->estatus == 0) {
                                        $var = 'Inactivo';
                                    } elseif ($model->estatus == 1) {
                                        $var = 'Activo';
                                    } else {
                                        $var = 'Eliminado';
                                    }

                                    echo CHtml::encode($model->getAttributeLabel('estatus'));
                                    ?></th>
                                <td><?php echo CHtml::encode($var);
                                     ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('fecha_creacion')); ?></th>
                                <td><?php echo CHtml::encode($model->fecha_creacion); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('hora_creacion')); ?></th>
                                <td><?php echo CHtml::encode($model->hora_creacion); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('usuario_crea')); ?></th>
                                <td><?php echo CHtml::encode($model->usuarioCrea->nombreusuario); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('fecha_modificacion')); ?></th>
                                <td><?php echo CHtml::encode($model->fecha_modificacion); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('hora_modificacion')); ?></th>
                                <td><?php echo CHtml::encode($model->hora_modificacion); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('usuario_modifica')); ?></th>
                                <td><?php echo CHtml::encode($model->usuarioModifica->nombreusuario); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>