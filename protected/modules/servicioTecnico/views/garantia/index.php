<?php
/* @var $this StGarantiaController */
/* @var $model StGarantia */

$this->breadcrumbs=array(
    'Garantias'=>array('index'),
    Yii::app('lang','Manage'),
);
Yii::app()->params['IDGridview'] = 'st-garantia-grid';
$this->menu=array(     
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
);

?>
<div class="row">
    <div class="col-xs-12">
<div class="panel panel-default">
    <div class="panel-heading navbar-tool">
        
        <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['index-text']) .' '. Yii::t('lang','Garantias'); ?></h3>
        
       <div class="menu-tool">
            
            <div class="pagesize"> 
             <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
             </div>         
            
            <div class="menu-items">
                 <?php 
        $this->widget('zii.widgets.CMenu', array(
            'items' => $this->menu,
            'encodeLabel' => FALSE,
            'htmlOptions' => array('class' => 'cmenuhorizontal'),
        ));
        ?>
            </div>
        </div>        
        <?php $this->ToolActionsRight(); ?>         
        
    </div>
    <div class="panel-body"> 

<div class="table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>Yii::app()->params['IDGridview'],
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
      'afterAjaxUpdate'=>'js:function(id, data){
        GridviewBoxSwitch();
        reinstallDatePicker();
}',
    'columns'=>array(
		
		 array('name'=>'descripcion', 'filter' => Active::getListActiveP(),'value'=>'$data->descripcion'),
		 array('name'=>'estatus','type' => 'raw','value' => 'Active::checkSwicth($data->estatus,$data->id,$data->id,"",Yii::app()->controller->createUrl("UpdateActive"))', 'filter' => Active::getListActive(),'htmlOptions' => array('class' => 'switchactive')),
                 array('name'=>'fecha_creacion',
                     'value'=>'Yii::app()->dateFormatter->format("y-MM-dd",strtotime($data->fecha_creacion))', 
                     'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker',
                             array('model' => $model, 'attribute' => 'fecha_creacion', 'language' => 'es',
                                    'htmlOptions' => array(
                                        'id' => 'datepicker_for_due_date',
                                        'size' => '10',
                                    ),
                                    'options' => array('dateFormat' => 'yy-mm-dd'), 'defaultOptions' => array(
                                        'showOn' => 'focus',
                                        'dateFormat' => 'yy-mm-dd',
                                        'showOtherMonths' => true,
                                        'selectOtherMonths' => true,
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'language' => 'es'
                                       )), true)), 
               
		
		/*
		 array('name'=>'fecha_eliminado','value'=>'$data->fecha_eliminado'),
		 array('name'=>'eliminado_por','value'=>'$data->eliminado_por'),
		 array('name'=>'estatus','value'=>'$data->estatus'),
		*/
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['view-text']),            
            'label' => Yii::app()->params['view-icon'],
            'linkHtmlOptions' => array('class' => 'view '.Yii::app()->params['view-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))', 
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
        ),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['update-text']),            
            'label' => Yii::app()->params['update-icon'],
            'linkHtmlOptions' => array('class' => 'edit '.Yii::app()->params['update-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
        ),
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['delete-text']),           
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
        ),
    ),
)); ?>
</div>
    </div>
    </div>
 </div>
     </div>
<?php 
 
    $url=Yii::app()->controller->createUrl("index");
    if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    $this->renderPartial('../../../../views/site/modal-delete', array('url'=>$url));    

    }  
//    if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {
//
//    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);
//
//   $this->renderPartial('../../../../views/site/modal-delete', array());    
//
//    } 
?>