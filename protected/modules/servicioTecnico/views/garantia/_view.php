<?php
/* @var $this StGarantiaController */
/* @var $data StGarantia */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->descripcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('fecha_creado')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->fecha_creado); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('creado_por')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->creado_por); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('fecha_editado')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->fecha_editado); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('editado_por')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->editado_por); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('fecha_eliminado')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->fecha_eliminado); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('eliminado_por')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->eliminado_por); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->estatus); ?></div>
	
</div>

<hr />