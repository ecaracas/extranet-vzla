<?php
/* @var $this HistorialOrdenesController */
/* @var $model STHistorialOrdenes */

$this->breadcrumbs = array(
    Yii::t('lang', 'Sthistorial Ordenes') => array('index'),
    Yii::app('lang', 'Manage'),
);
Yii::app()->params['IDGridview'] = 'sthistorial-ordenes-grid';
$this->menu = array(/*
      array(
      'label' => Yii::t('lang', Yii::app()->params['create-text']),
      'url' => array('create'),
      'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
      'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')), */

    array(
        'label' => Yii::t('lang', Yii::app()->params['update-text2']),
        'url' => array('update', 'id_orden' => $model_orden["id"]),
        'linkOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => $btn_activo),
    array(
        'label' => Yii::t('lang', Yii::app()->params['return-text']),
        'url' => array("OrdenServicio/index"),
        'linkOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
);
?>

<div class="col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading navbar-tool">

            <h3 class="panel-title"><?php echo Yii::t('lang', 'Historial De Ordenes'); ?></h3>

            <div class="menu-tool">

                <div class="pagesize"> 
                    <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
                </div>         

                <div class="menu-items">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items' => $this->menu,
                        'encodeLabel' => FALSE,
                        'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
            </div>        
            <?php $this->ToolActionsRight(); ?>         

        </div>
        <div class="panel-body"> 

            <div class="data_orden">

                <table class=" table table-reflow" id="table_info_">

                    <tbody>
                        <tr>
                            <th scope="row"class="inicial_" >Orden número:</th>
                            <td><?php echo $model_orden["id_num_orden"]; ?></td>

                        </tr>

                        <tr>
                            <th scope="row" colspan="2" class="principal_">Datos De Enajenacion </th>
                        </tr>

                        <tr>
                            <th scope="row" class="secundarios_">Fecha Enajenación:</th>
                            <td><?php echo $model_orden["fecha_enajenacion"]; ?></td>
                        </tr>
                        <tr>
                            <th scope="row" class="secundarios_">Distribuidor / Técnico:</th>
                            <td><?php echo $model_orden["distribuidor"]; ?><strong> / </strong><?php echo $model_orden["tecnico"]; ?>  </td>

                        </tr>
                        <tr>
                            <th scope="row" class="secundarios_">Serial:</th>
                            <td><?php echo $model_orden["serial"]; ?></td>
                        </tr>

                        <tr>
                            <th scope="row" colspan="2" class="principal_">Datos De La Orden</th>
                        </tr>

                        <tr>
                            <th scope="row" class="secundarios_">Marca / Modelo:</th>
                            <td><?php echo $model_orden["marca"]; ?> <strong> / </strong><?php echo $model_orden["modelo"]; ?></td>

                        </tr>
                        <tr>
                            <th scope="row" class="secundarios_">Descripción Falla:</th>
                            <td><?php echo $model_orden["descripcion_falla"]; ?></td>
                        </tr>

                        <tr>
                            <th scope="row" class="secundarios_">Revision:</th>
                            <td><?php echo $model_orden["revision_previa"]; ?></td>
                        </tr>

                        <tr>
                            <th scope="row" class="secundarios_">Garantia:</th>
                            <td> <?php echo $model_orden["descripcion"]; ?><strong> </td>

                        </tr>

                        <tr>
                            <th scope="row" class="secundarios_">Contacto:</th>
                            <td> <?php echo $model_orden["contacto"]; ?><strong> /  </strong> <?php echo $model_orden["telefono"]; ?> <strong> /  </strong><?php echo $model_orden["email"]; ?></td>

                        </tr>



                    </tbody>
                </table>

                <?php
                // var_dump($model_fallas);exit();

                $arreglo = array(
                    'Accesorios' => $this->renderPartial('info_accordion', array('model_accesorios' => $model_accesorios, 'op' => 1), true),
                );

                if (!empty($model_fallas))
                    $arreglo['Fallas'] = $this->renderPartial('info_accordion', array('model_fallas' => $model_fallas, 'op' => 2), true);

                if (!empty($model_repuestos))
                    $arreglo['Repuestos'] = $this->renderPartial('info_accordion', array('model_repuestos' => $model_repuestos, 'op' => 3), true);





                $this->widget('zii.widgets.jui.CJuiAccordion', array(
                    'panels' => $arreglo,
                    // additional javascript options for the accordion plugin
                    'options' => array(
                        'collapsible' => true,
                        'animated' => 'bounceslide',
                        'autoHeight' => false,
                        'active' => 2,
                        'icons' => array(
                            "header" => "ui-icon-plus", //ui-icon-circle-arrow-e
                            "headerSelected" => "ui-icon-circle-arrow-s", //ui-icon-circle-arrow-s, ui-icon-minus
                        ),
                    ),
                    'htmlOptions' => array(
                        'class' => 'accordion_historia'
                    )
                ));
                ?>
                <br>
            </div>

            <div class="table-responsive">
                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => Yii::app()->params['IDGridview'],
                    'dataProvider' => $dataProvider,
                    'filter' => $model,
                    'afterAjaxUpdate' => 'js:function(id, data){
                                                
                                                 reinstallDatePicker();
                                    }',
                    'columns' => array(
                        // array('name'=>'id','value'=>'$data->id'),
                        //array('name'=>'id_orden','value'=>'$data->id_orden'),
                        array('name' => 'id_estatus', 'value' => '$data->stEstatus->nombre'),
                        array('name' => 'descripcion', 'value' => '$data->descripcion'),
                        array('name' => 'usuario_r', 'value' => '$data->idUsuarioRevision->nombre. " " .$data->idUsuarioRevision->apellido'),
                        array('name' => 'fecha', 'value' => '$data->fecha', 'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model, 'attribute' => 'fecha', 'language' => 'es',
                                'htmlOptions' => array(
                                    'id' => 'datepicker_for_due_date',
                                    'size' => '10',
                                ),
                                'options' => array('dateFormat' => 'yy-mm-dd'), 'defaultOptions' => array(
                                    'showOn' => 'focus',
                                    'dateFormat' => 'yy-mm-dd',
                                    'showOtherMonths' => true,
                                    'selectOtherMonths' => true,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'language' => 'es'
//                    'showButtonPanel' => true,
                                )), true)),
                    /*   array(
                      'class' => 'CLinkColumn',
                      'header' => Yii::t('lang', Yii::app()->params['view-text']),
                      'label' => Yii::app()->params['view-icon'],
                      'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                      'urlExpression' => 'Yii::app()->controller->createUrl("view", array("id" => $data->id))',
                      'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                      ), */
                    /* array(
                      'class' => 'CLinkColumn',
                      'header' => Yii::t('lang', Yii::app()->params['update-text2']),
                      'label' => Yii::app()->params['update-icon'],
                      'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn']),
                      'urlExpression' => 'Yii::app()->controller->createUrl("update", array("id" => $data->id))',
                      'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
                      ),
                      array(
                      'class' => 'CLinkColumn',
                      'header' => Yii::t('lang', Yii::app()->params['delete-text']),
                      'label' => Yii::app()->params['delete-icon'],
                      'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
                      'urlExpression' => 'Yii::app()->controller->createUrl("delete", array("id" => $data->id))',
                      'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
                      ), */
                    ),
                ));
                ?>
                <br>
                <br>

                <div id="owl-demo">
                    <?php
                    $count = 1;
// var_dump($model_imagenes);
                    foreach ($model_imagenes as $valor) {
                        ?>
                        <div class="item item_click" id="<?php echo $count; ?>">
                            <img class="lazyOwl" data-src="<?php echo Yii::app()->baseUrl . "/img/uploads/" . $valor->path_imagen; ?>" alt="" >
                        </div>
                        <?php
                        $count++;
                    }
                    ?>
                </div>


            </div>

            <div id="owl-demo">
                <?php
                $count = 1;
// var_dump($model_imagenes);
                foreach ($model_imagenes as $valor) {
                    ?>
                    <div class="item item_click" id="<?php echo $count; ?>">
                        <img class="lazyOwl" data-src="<?php echo Yii::app()->baseUrl . "/img/uploads/" . $valor->path_imagen; ?>" alt="" >
                    </div>
                    <?php
                    $count++;
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete ', array());    
}
?>


<style>
    #owl-demo .item{
        margin: 3px;
    }
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: auto;
        max-width: 284px !important;
        max-height: 260px !important;
    }
</style>
<script>
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            //                    autoPlay: 3000,
            items: 3,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            lazyLoad: true,
            navigation: true
        });

    });
</script>