<?php

class RepuestosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', /* Acciones Permitidas */
                'actions' => array('index', 'view', 'create', 'update', 'delete', 'updateactive'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public $modulo = "serviciotecnico";

    public function __construct($id, $module = null) {

        parent::__construct($id, $module);

        Yii::app()->params['title'] = '';
    }

    public function actionView($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {

            $model = $this->loadModel($id);

            $this->render('view', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionCreate() {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {

            $model = new StRepuestos;
            $estatus = 0;

            if (isset($_POST['StRepuestos'])) {
                $model->estatus = $estatus;
                $model->attributes = $_POST['StRepuestos'];
                $model->fecha_creacion = date('Y-m-d');
                $model->hora_creacion = date('H:m:s');
                $model->usuario_crea = Yii::app()->user->id;
                $model->fecha_modificacion = date('Y-m-d');
                $model->hora_modificacion = date('H:m:s');
                $model->usuario_modifica = Yii::app()->user->id;

                if ($model->validate()) {
                    if ($model->save()) {

                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionUpdate($id) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {

            $model = $this->loadModel($id);
            $estatus = 0;


            if (isset($_POST['StRepuestos'])) {

                $model->estatus = $estatus;
                $model->attributes = $_POST['StRepuestos'];
                $model->fecha_modificacion = date('Y-m-d');
                $model->hora_modificacion = date('H:m:s');
                $model->usuario_modifica = Yii::app()->user->id;

                if ($model->validate()) {
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionIndex() {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {

            $model = new StRepuestos('search');

            $model->unsetAttributes();  // clear any default values

            if (isset($_GET['StRepuestos'])) {
                $model->attributes = $_GET['StRepuestos'];
            }

            if (isset($_GET['StRepuestos_sort'])) {

                Yii::app()->params['GridViewOrder'] = TRUE;
            }

            $this->render('index', array('model' => $model,));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }
     public function actionDelete($id) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {
            $estatus = 9;
            $model = $this->loadModel($id);

            $model->estatus = $estatus;
            $model->fecha_modificacion = date('Y-m-d');
            $model->hora_modificacion = date('H:m:s');
            $model->usuario_modifica = Yii::app()->user->id;

            if ($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                    $this->redirect(array('index'));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            } else {
                Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
            }
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }
     public function actionUpdateActive($it = NULL, $s = NULL) {
         
        $model = $this->loadModel($it);
        if ($s == 1) {
            $model->estatus = 1;
        } elseif ($s == 0) {
            $model->estatus = 0;
        }
        if ($model->save()) {
            $correosInternos = array('ecaracas@thefactoryhka.com');
            if ($model->estatus == 1)
                $this->envioEmail($model->email, $model->nombre . ' su usuario ha sido activado, ya puede ingresar al sistema.', 'Activación de usuario The Factory Corp', 'Activación de usuario');
            echo CJSON::encode(array('message' => 'exito', 'success' => 'true'));
        } else
            echo CJSON::encode(array('message' => $model->getErrors(), 'success' => 'true'));
    }

    public function loadModel($id) {
        $sql = new CDbCriteria;
        $sql->params = array(':id' => intval($id));
        $sql->condition = "id = :id";
       //$sql->addcondition("estatus <> 9");
        $model = StRepuestos::model()->find($sql);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }
        return $model;
    }

}
