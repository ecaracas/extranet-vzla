<?php

class OrdenServicioController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', /* Acciones Permitidas */
                'actions' => array('index', 'view', 'create', 'update', 'delete', 'test', 'updateactive'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public $modulo = "serviciotecnico";

    public function __construct($id, $module = null) {

        parent::__construct($id, $module);

        Yii::app()->params['title'] = '';
    }

    public function actionView($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {



            $model = $this->loadModel($id);

            $this->render('view', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionCreate() {
        if (!isset($_POST['StOrdenServicio']) && isset($_SESSION['validado']))
            unset($_SESSION['validado']);

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {

            $model = new STOrdenServicio();

            //se busca el rol para poder determinar si el usuario puede agregar las fallas
            $rol = $this->actionGetRol();
            list($es_valido, $rif,$dv, $marca, $garantia, $modelo, $a) = array('','', '', '', '', '','');

            if (isset($_POST['StOrdenServicio'])) {

                list($es_valido, $rif, $dv, $marca, $modelo, $garantia, $a, $model) = $this->actionTest();
                    // print_r($a);die;
                if ($es_valido) {
                    $model->scenario = 'insert';
                    if (!isset($_SESSION['validado'])) {
                        Yii::app()->session['validado'] = true;
                        $model->id_garantia = $garantia;

$this->render('create', array('model' => $model, 'rol' => $rol, 'rif' => $rif, 'dv'=>$dv, 'marca' => $marca, 'modelo' => $modelo, 'a' => $a,'garantia'=>$garantia));

                        return;
                    }


                    $model->attributes = $_POST['StOrdenServicio'];

                    $model->id_garantia = ($rol == 3) ? $garantia->id : $model->id_garantia;
                    if ($model->validate()) {


                        //se busca la información asociada al serial
                        $siglas = substr($model->serial, 0, 3);
                        $maquina = $this->actionGetMaquina($siglas);
                        $model->marca = $maquina['name'];
                        $model->modelo = $maquina['model'];
                        // se busca el distribuidor y tecnico asociados al serial
                        $operacion = Operations::model()->find('serial=:serial', array(':serial' => $model->serial));
                        //Se busca el rol del usuario
                        $perfil = RBACUsuarios::model()->find('id=:id', array(':id' => Yii::app()->user->id));

                        //si el usuario es tecnico se almacena el id y se asigna el distribuidor correspondiente al serial
                        if ($perfil->rol_id == 8) {
                            $model->id_tecnico = $perfil->id;
                            $model->id_distribuidor = $operacion->dealer_id;
                        }//si el usuario es distribuidor se almacena el id y se asigna el tecnico correspondiente al serial 
                        elseif ($perfil->rol_id == 3) {
                            $model->id_distribuidor = $perfil->id;
                            $model->id_tecnico = $operacion->techinician_id;
                        }// si no se cumple ninguno de los casos anteriores de cargan el tecnico y distribuidor asociado al serial 
                        else {
                            $model->id_tecnico = $operacion->techinician_id;
                            $model->id_distribuidor = $operacion->dealer_id;
                        }
                        $model->fecha_enajenacion = $operacion->date;
                        $model->fecha_registro = date('Y-m-d');
                        //se Crea el codigo de la orden de servicio
                        $id_orden = Yii::app()->db->createCommand()
                                ->select('max(id)as Maxid')
                                ->from('tfhka_extranet_patest.st_orden_servicio')
                                ->queryRow();
                        if ($id_orden['Maxid'] == null) {
                            $cod_ord = str_pad('1', 10, "0", STR_PAD_LEFT);
                            $cod_ord = 'ST' . $cod_ord;
                        } else {

                            $cod_ord = str_pad(($id_orden['Maxid'] + 1), 10, "0", STR_PAD_LEFT);
                            $cod_ord = 'ST' . $cod_ord;
                        }
                        $model->id_num_orden = $cod_ord;


                        if ($model->save()) {

                            $id_orden = $model->id;
                            //se almacenan los accesorios seleccionado por el usuario
                            foreach ($model->accesorios as $valor) {

                                $model_accesorios_orden = new STAccesoriosOrden;
                                $model_accesorios_orden->id_orden = $id_orden;
                                $model_accesorios_orden->id_accesorio = $valor;
                                $model_accesorios_orden->fecha = date('Y-m-d');

                                $model_accesorios_orden->save();
                            }

                            //Se almacenan las fallas seleccionadas por el usuario

                          
                             foreach ($model->fallas as $valor) {

//                      
                              $model_fallas_orden = new StFallasOrden;

                               $model_fallas_orden->id_falla = $valor;
                               $model_fallas_orden->id_orden = $id_orden;
                                $model_fallas_orden->fecha = date('Y-m-d');
//
                                $model_fallas_orden->save();
                            }


                    //Se almacenan los REPUESTOS seleccionadas por el usuario
                                // print_r($model->repuestos); die("hola");
                            foreach ($_POST['StOrdenServicio']['repuestos'] as $valor) {
// //$_POST['StOrdenServicio']['repuestos']
                              $model_repuestos_orden = new StRepuestosOrden;
                               $model_repuestos_orden->id_repuesto = $valor;
                               $model_repuestos_orden->id_orden = $id_orden;
                             $model_repuestos_orden->fecha = date('Y-m-d');
// //
                                $model_repuestos_orden->save();
                            }



                            //se crea la historia con estatus por recibir 
                            $model_historia = new STHistorialOrdenes;

                            $model_historia->id_orden = $id_orden;
                            $model_historia->id_usuariorevision = Yii::app()->user->id;
                            $model_historia->id_estatus = 1;
                            $model_historia->descripcion = $model->descripcion_falla;
                            $model_historia->fecha = date('Y-m-d');
                            //var_dump($model_historia);
                            // exit();

                            $model_historia->save();

                            $correosInternos = Yii::app()->params['correosInternosOrden'];
                            array_push($correosInternos, Yii::app()->user->getState('emailUser'));
                            $this->envioEmail($correosInternos, 'El usuario ' . Yii::app()->user->getState('nombreusuario') . ' ha creado la orden de servicio técnico N.' . $model->id_num_orden . ' para el equipo ' . $model->serial . ' <br/>', 'The Factory Corp', 'Orden De Servicio Técnico Creada');
                            //Correo de generacion de historia
                            $this->envioEmail($correosInternos, 'Se ha generado la historia en estatus POR RECIBIR correspondiente a la orden N.' . $model->id_num_orden . '<br/>', 'The Factory Corp', 'Historia De Servicio Técnico Creada');
                            Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);


                            $this->redirect(array('view', 'id' => $model->id));
                        } else {


                            Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                        }
                    } else {

                        //  var_dump($model->errors);
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                }
            }
            
            $this->render('create', array('model' => $model, 'rol' => $rol, 'rif' => $rif, 'dv' => $dv, 'marca' => $marca, 'modelo' => $modelo, 'a' =>'','garantia'=>$garantia));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionUpdate($id) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {

            $model = $this->loadModel($id);
            $model_accesorios = STAccesoriosOrden::model()->findAll('id_orden=:id', array(':id' => $id));

            $_array = array();
            foreach ($model_accesorios as $key => $value) {

                array_push($_array, $value->id_accesorio);
            }

            $model->accesorios = $_array;

            if (isset($_POST['STOrdenServicio'])) {
                $model->attributes = $_POST['STOrdenServicio'];

                if ($model->validate()) {
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionIndex() {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {

            $model = new ViewEstatusHistoria('search');



            $model->unsetAttributes();  // clear any default values

            if (isset($_GET['ViewEstatusHistoria'])) {
                $model->attributes = $_GET['ViewEstatusHistoria'];
            }

            if (isset($_GET['ViewEstatusHistoria'])) {

                Yii::app()->params['GridViewOrder'] = TRUE;
            }

            $this->render('index', array('model' => $model,));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionUpdateActive($it = NULL, $s = NULL) {

        $model = $this->loadModel($it);

//        if ($s == 1) {
//            $model->garantia = 1;
//            //echo'si';
//        } elseif ($s == 0) {
//            $model->garantia = 0;
//        }
        if ($model->save()) {
            $correosInternos = array('ecaracas@thefactoryhka.com');
            //if ($model->estatus == 1)
            //   $this->envioEmail($model->email, $model->nombre . ' su usuario ha sido activado, ya puede ingresar al sistema.', 'Activación de usuario The Factory Corp', 'Activación de usuario');
            echo CJSON::encode(array('message' => 'exito', 'success' => 'true'));
        } else {
            echo 'NO';
            echo CJSON::encode(array('message' => $model->getErrors(), 'success' => 'true'));
        }
    }

    public function actionTest() {

        $serial = $_POST['StOrdenServicio']['serial'];

        $siglas = substr($serial, 0, 3);

        $model = new STOrdenServicio;
        $model->scenario = 'verifi';
        $model->serial = $serial;

        if ($model->validate()) {
            $operacion = $this->actionGetRuc($serial);


            $datos_maquina = $this->actionGetMaquina($siglas);


            if (!empty($datos_maquina) && !empty($operacion)) {
                //se busca el id de la maquina para saber la fecha de enajenacion
                $maquina = Machines::model()->find('serial=:serial', array(':serial' => $serial));

                //si existe la maquina discal registrada                
                if (!empty($maquina)) {
                    //se verifica si esta enajenada
                    //Yii::import('application.modules.servioTecnico.models.FicalizedMachines');
                    $maquina_enajenada = FicalizedMachines::model()->find('machine_id=:machine_id', array(':machine_id' => $maquina->id));

                    //si la maquina posee fecha de enajenacion se verifica el tiempo que tiene de garantia
                    if (!empty($maquina_enajenada)) {

                        $dias_venta = $this->actionDias($maquina_enajenada->date);
                       
                        //se verifica si tiene una reparacion previa
                        $fecha_reparacion = Yii::app()->db->createCommand()
                                ->select('h.fecha')
                                ->from('tfhka_extranet_patest.st_historial_ordenes h')
                                ->rightJoin('tfhka_extranet_patest.st_orden_servicio o', ' o.id= h.id_orden')
                                ->where(' o.serial=:serial and h.id_estatus=:id', array(':serial' => $serial, ':id' => 9))
                                ->queryRow();
                       
                       
                        if (!empty($fecha_reparacion)) {
                            $dias_reparacion = $this->actionDias($fecha_reparacion['fecha']);
                        } 
                        

                        // se valida si es un caso reincidente 
                        $fecha_repuesto = Yii::app()->db->createCommand()
                                    ->select('h.fecha')
                                    ->from('tfhka_extranet_patest.st_historial_ordenes h')
                                    ->rightJoin('tfhka_extranet_patest.st_orden_servicio o', ' o.id= h.id_orden')
                                    ->where(' o.serial=:serial and h.id_estatus=:id', array(':serial' => $serial, ':id' => 7 ))
                                    ->queryRow();

                        if(!empty($fecha_repuesto)){
                            $dias_repuesto = $this->actionDias($fecha_repuesto['fecha']);
                        }






                       
                        
                        if ((isset($dias_reparacion) && ($dias_reparacion) <= 183)) {
                            
                            // GARANTIA DE REPARACION 
                            $garantia = $this->actionGarantia(3);
                         
                            
                        }

                         else if ((!empty($dias_venta) && ($dias_venta) <= 365)) {
                            //GARANTIA (12 MESES)
                        
                            $garantia = $this->actionGarantia(1);
                        }
                         else {
                            //SIN GARANTIA
                            $garantia = $this->actionGarantia(4);
                        }
                    } else {
                        $garantia = $this->actionGarantia(5);
                    }
                } else {
                    //si no se consigue la maquina se le coloca estatus por verificar
                    $garantia = $this->actionGarantia(5);
                }
                // print_r($operacion->business_name);die;
                return array(true, $operacion->ruc,$operacion->dv, $datos_maquina['name'], $datos_maquina['model'], $garantia, $operacion->business_name, $model);

            } else {

                return array(false, '', '', '', '', $model);
            }
        } else {

            return array(false, '', '', '', '', $model);
        }
    }

    public function actionGetRuc($serial) {

        $sql = Operations::model()->find('serial=:serial', array(':serial' => $serial));
        return $sql;
    }

    public function actionGetMaquina($siglas) {

        $sql2 = Yii::app()->db->createCommand()
                ->select('p.model, b.name')
                ->from('tfhka_prueba.products p')
                ->join('tfhka_prueba.brands b', 'p.brand_id=b.id')
                ->join('tfhka_prueba.brands_country c', 'b.id=c.brand_id')
                ->where('c.country_id=:country_id and p.acronym=:siglas', array(':country_id' => Yii::app()->params['pais'], ':siglas' => $siglas))
                ->queryRow();

//SELECT products.model,products.brand_id,brands.name,brands_country.country_id,products.acronym FROM products,brands,brands_country WHERE products.brand_id=brands.id and brands_country.country_id='*' and products.acronym='*' and country_id='pa';
        return $sql2;
    }

    public function actionGarantia($id) {

        // $sql3 = StGarantia::model()->find(array('select' => 'id', 'condition' => 'id=:id', 'params' => array(':id' => $id)));
        $sql3= Yii::app()->db->createCommand()
            ->select('s.descripcion')
            ->from('tfhka_extranet_patest.st_garantia s')
            ->where('s.id=:id',array(':id'=>$id))
            ->queryRow();
        return $sql3;
    }

    public function actionDias($fecha) {

        $dia_actual = new DateTime(date('Y-m-d'));
        $dia_venta = new DateTime($fecha);
        $diferencia = date_diff($dia_actual, $dia_venta);

        return $diferencia->format('%a');
        ;
    }

    public function actionGetRol() {

        $criteria = new CDbCriteria;
        $criteria->select = 'rol_id';  // only select the 'title' column
        $criteria->condition = 'id=:id';
        $criteria->params = array(':id' => Yii::app()->user->id);
        $rol_user = RBACUsuarios::model()->find($criteria);
        return $rol_user->rol_id;
    }

    public function loadModel($id) {
        $sql = new CDbCriteria;
        $sql->params = array(':id' => intval($id));
        $sql->condition = "id = :id";
        //$sql->addcondition("estatus <> 9");
        $model = STOrdenServicio::model()->find($sql);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }
        return $model;
    }

}
