<?php

class HistorialOrdenesController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', /* Acciones Permitidas */
                'actions' => array('index', 'view', 'create', 'update', 'delete'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public $modulo = "serviciotecnico";

    public function __construct($id, $module = null) {

        parent::__construct($id, $module);

        Yii::app()->params['title'] = '';
    }

    public function actionView($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {

            //se busca la informacion de la historia
            $model = $this->loadModel($id);
            //se verifica el estatus de la historia y si esta anulado o entregada se pone deshabilitar el noton actualizar
            if ($model->id_estatus == 8 || $model->id_estatus == 9) {
                $btn_activo = false;
            } else {

                $btn_activo = Yii::app()->authRBAC->checkAccess($this->modulo . '_update');
            }

            //Se buscan las fallas asociadas a la orden             
            $model_fallas = StFallasOrden::model()->with('idFalla')->findAll('id_orden=:id_orden', array(':id_orden' => $model->id_orden));
            $model_imagenes = STRegistroFotografico::model()->findAll('id_orden=:id_orden', array(':id_orden' => $model->id_orden));
            $model_repuestos = StRepuestosOrden::model()->with('idRepuesto')->findAll('id_orden=:id_orden', array(':id_orden' => $model->id_orden));



            $this->render('view', array('model' => $model, 'btn_activo' => $btn_activo, 'model_fallas' => $model_fallas, 'model_imagenes' => $model_imagenes,'model_repuestos'=>$model_repuestos));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionCreate() {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {

            $model = new STHistorialOrdenes;


            if (isset($_POST['STHistorialOrdenes'])) {
                $model->attributes = $_POST['STHistorialOrdenes'];
                if ($model->validate()) {
                    if ($model->save()) {

                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionUpdate($id_orden) {
        
        

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {


            $model = $this->loadHistoria($id_orden);
            $estatus_old = $model->id_estatus;

            $model_fallas = StFallasOrden::model()->findAll('id_orden=:id_orden', array(':id_orden' => $model->id_orden));
            ;

            $_array = array();
            foreach ($model_fallas as $value) {

                array_push($_array, $value->id_falla);
            }

            $model->fallas = $_array;

            //se define la ruta donde se almacenaran las imagenes
            $path_picture = Yii::app()->basePath . '/../img/uploads/'; // Yii::app()->baseUrl . "/img/uploads/";



            if (isset($_POST['STHistorialOrdenes'])) {


                $model->attributes = $_POST['STHistorialOrdenes'];
                $model->id = null;
                $model->isNewrecord = true;

                // se captura la información de los archivos
                $imagenes = CUploadedFile::getInstancesByName('reg_imagenes');
                $cont = 0;
                // si el usurio cargo alguna imagen               

                if (isset($imagenes) && count($imagenes) > 0) {
                    //Se recorre el arreglo para validar que los archivos cumplan con las condiciones

                    if (count($imagenes) < 5) {
                        // echo "<br>aqui1";
                        foreach ($imagenes as $value => $carga) {
                            $model->reg_fotografico = $carga;

                            if ($model->validate()) {
                                $nombre_imagen = $carga->name;
                                $carga->saveAs($path_picture . $nombre_imagen);
                                $cont++;
                            } else {
                                Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                            }
                        }
                    } else {
                        $model->id_estatus = $estatus_old;
                        $model->addError('reg_fotografico', 'No se pueden seleccionar mas de 4 imagenes de manera simultanea');
                    }
                }

                // exit();
                // si los archivos no presentan errores
                if (count($imagenes) == $cont) {

                    $model->id_usuariorevision = Yii::app()->user->id;
                    $model->fecha = date("Y-m-d");
                    //Se guarda el historico
                    if ($model->save()) {

                        $id_historia = $model->id;
                        $id_orden = $model->id_orden;
                        
						
						
                        //Si las fallas son diferentes de vacio
                        if (!empty($model->fallas)) {
                            //Se recorre el arreglo para validar que si el usuario selecciono una falla nueva se almacene
                            foreach ($model->fallas as $valor) {

                                $search = StFallasOrden::model()->exists('id_falla=' . $valor . ' AND id_orden=' . $id_orden);

                                if (!$search) {
                                    $model_fallas_orden = new StFallasOrden();
                                    $model_fallas_orden->id_falla = $valor;
                                    $model_fallas_orden->id_orden = $id_orden;
                                    $model_fallas_orden->fecha = date('Y-m-d');
                                    $model_fallas_orden->save();
                                }
                            }
                            //Se recorre el arreglo incial para el caso de que el usuario elimino alguna falla
                            foreach ($_array as $valor) {
                                if (!in_array($valor, $model->fallas)) {

                                    $model_f = StFallasOrden::model()->deleteAll(array('condition' => 'id_falla=:id_falla AND id_orden=:id_orden', 'params' => array(':id_falla' => $valor, ':id_orden' => $id_orden)));
                                }
                            }
                        }

                        
                        
      
                        
                        //Si los repuestos son diferentes de vacio
                        if (!empty($model->repuestos)) {
                            //Se recorre el arreglo para validar que si el usuario selecciono una falla nueva se almacene
                            foreach ($model->repuestos as $valor) {

         
                                
                                $search = StRepuestosOrden::model()->exists('id_repuesto=' . $valor . ' AND id_orden=' . $id_orden);

                                
        
                                
                                if (!$search) {
                                    $model_repuestos_orden = new StRepuestosOrden();
                                    $model_repuestos_orden->id_repuesto = $valor;
                                    $model_repuestos_orden->id_orden = $id_orden;
                                    $model_repuestos_orden->fecha = date('Y-m-d');
                                    $model_repuestos_orden->save();
                                }
                            }
                            //Se recorre el arreglo incial para el caso de que el usuario elimino alguna falla
                            foreach ($_array as $valor) {
                                if (!in_array($valor, $model->repuestos)) {

                                    $model_r = StRepuestosOrden::model()->deleteAll(array('condition' => 'id_repuesto=:id_repuesto AND id_orden=:id_orden', 'params' => array(':id_repuesto' => $valor, ':id_orden' => $id_orden)));
                                }
                            }
                        }


                        //Y se almacenan la relación de las imagenes con la historia
                        if (isset($imagenes) && count($imagenes) > 0) {
                            foreach ($imagenes as $value => $carga) {
                                //Se almacena la información de las imagenes asociadas al historico
                                $model_imagenes = new STRegistroFotografico();
                                $model_imagenes->path_imagen = $carga->name;
                                $model_imagenes->id_orden = $id_orden;
                                $model_imagenes->created_at = date("Y-m-d");
                                $model_imagenes->save();
                            }
                        }


                        $correosInternos = Yii::app()->params['correosInternosOrden'];
                        array_push($correosInternos, Yii::app()->user->getState('emailUser'));
                        $this->envioEmail($correosInternos, 'El usuario ' . Yii::app()->user->getState('nombreusuario') . ' ha actualizado la historia de la orden de servicio técnico N.' . $model->idOrdenes->id_num_orden . ' a estatus ' . $model->stEstatus->nombre . ' <br/>', 'The Factory Corp', 'Historia De Servicio Técnico Actualizada');


                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        $model->id_estatus = $estatus_old;
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    $model->id_estatus = $estatus_old;
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('update', array('model' => $model,));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionIndex($id_orden) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {

            $model = new STHistorialOrdenes('search');

            $model->unsetAttributes();  // clear any default values
            $model->id_orden = $id_orden;
            $model_orden = Yii::app()->db->createCommand()
                    ->select('o.*, d.nombreusuario as distribuidor, t.nombreusuario as tecnico, g.descripcion')
                    ->from('tfhka_extranet_patest.st_orden_servicio o')
                    ->join('tfhka_extranet_patest.rbac_usuarios d', 'o.id_distribuidor=d.id')
                    ->join('tfhka_extranet_patest.rbac_usuarios t', 'o.id_tecnico=t.id')
                    ->join('tfhka_extranet_patest.st_garantia g', 'o.id_garantia=g.id')
                    ->where('o.id=:id', array(':id' => $id_orden))
                    ->queryRow();
            $model_imagenes = STRegistroFotografico::model()->findAll('id_orden=:id_orden', array(':id_orden' => $id_orden));
            $model_fallas = StFallasOrden::model()->with('idFalla')->findAll('id_orden=:id_orden', array(':id_orden' => $model->id_orden));
            $model_repuestos = StRepuestosOrden::model()->with('idRepuesto')->findAll('id_orden=:id_orden', array(':id_orden' => $model->id_orden));
            
            $model_accesorios = STAccesoriosOrden::model()->with('idAccesorio')->findAll('id_orden=:id_orden', array(':id_orden' => $model->id_orden));

            //Se verifica el estatus de la orden en caso de estar entregado o anulado se dashabilita el boton de actualizar
            $model_estatus = $this->loadHistoria($id_orden);
            if ($model_estatus->id_estatus == 8 || $model_estatus->id_estatus == 9) {
                $btn_activo = false;
            } else {

                $btn_activo = Yii::app()->authRBAC->checkAccess($this->modulo . '_update');
            }


            if (isset($_GET['STHistorialOrdenes'])) {
                $model->attributes = $_GET['STHistorialOrdenes'];
            }

            if (isset($_GET['STHistorialOrdenes_sort'])) {

                Yii::app()->params['GridViewOrder'] = TRUE;
            }

            $this->render('index', array('model' => $model, 'model_orden' => $model_orden, 'btn_activo' => $btn_activo, 'model_imagenes' => $model_imagenes, 'model_fallas' => $model_fallas, 'model_accesorios' => $model_accesorios,'model_repuestos'=>$model_repuestos));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function loadModel($id) {
        $sql = new CDbCriteria;
        $sql->params = array(':id' => intval($id));
        $sql->condition = "id = :id";

        // $sql->addcondition("estatus <> 9");
        $model = STHistorialOrdenes::model()->find($sql);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }
        return $model;
    }

    public function loadHistoria($id_orden) {

        $sql2 = new CDbCriteria;
        $sql2->params = array(':id_orden' => intval($id_orden));
        $sql2->condition = "id_orden = :id_orden";
        $sql2->order = 'id DESC';
        $sql2->limit = 1;
        // $sql->addcondition("estatus <> 9");
        $model = STHistorialOrdenes::model()->find($sql2);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }

        return $model;
    }

}
