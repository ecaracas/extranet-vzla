<?php

/**
 * This is the model class for table "st_orden_servicio".
 *
 * The followings are the available columns in table 'st_orden_servicio':
 * @property integer $id
 * @property varchar $id_num_orden
 * @property integer $id_distribuidor
 * @property integer $id_tecnico
 * @property integer $id_producto
 * @property string $marca
 * @property string $modelo
 * @property string $serial
 * @property string $descripcion_falla
 * @property string $revision_previa
 * @property string $contacto
 * @property string $telefono
 * @property string $medio_envio
 * @property string $medio_recepcion
 * @property string $direccion_recepcion
 * @property string $email
 * @property string $fecha_registro
 * @property string $fecha_enajenacion
 * @property integer $id_garantia
 *
 * The followings are the available model relations:
 * @property StAccesoriosOrden[] $stAccesoriosOrdens
 * @property StRepuestosOrden[] $stRepuestosOrdens
 * @property StHistorialOrdenes[] $stHistorialOrdenes
 * @property StOrdenEnvio[] $stOrdenEnvios
 * @property RbacUsuarios $idDistribuidor
 * @property RbacUsuarios $idTecnico
 * @property StRegistroFotografico[] $stRegistroFotograficos
 */
class StOrdenServicio extends CActiveRecord {

    public $rif_usuario;
    public $distribuidor;
    public $tecnico;
    public $accesorios;
    public $fallas;
    public $estatus;
    public $repuestos;
    


    public function tableName() {
        return 'st_orden_servicio';
    }

    public function rules() {

        return array(
            array('serial,descripcion_falla, revision_previa, contacto, telefono, email, accesorios', 'required', 'on'=>'insert'),

            array('serial', 'required', 'on'=>'verifi'),
            // array('serial', 'length', 'is'=>10),
            // array('serial', 'unique'), //  PARA QUE SEA UN SOLO SERIAL POR ORDEN 
             array('serial', 'verificaSerial'),
           // array('serial', 'exist', 'allowEmpty' => true, 'attributeName' => 'serial', 'className' => 'Operations'),
            array('id_distribuidor, id_tecnico, id_producto, id_garantia', 'numerical', 'integerOnly' => true),
            array('email', 'email'),
            array('telefono','length', 'is'=>11),
            array('serial, contacto,  medio_envio, medio_recepcion, direccion_recepcion, email, rif_usuario', 'length', 'max' => 45),
	    array('descripcion_falla, revision_previa', 'length', 'max' => 150),
            array('marca, modelo, fecha_registro, fecha_enajenacion, fallas, estatus,id_garantia', 'safe'),
            array('id, distribuidor,tecnico, id_distribuidor, id_tecnico, id_producto, marca, modelo, serial, descripcion_falla, revision_previa, contacto, telefono, medio_envio, medio_recepcion, direccion_recepcion, email, fecha_registro, fecha_enajenacion, id_garantia, id_num_orden, accesorios, estatus', 'safe', 'on' => 'search'),
        );
    }
    
    public function verificaSerial($atrribute) {
        
        //se valida que las siglas sean correctas
        $siglas = substr($this->serial, 0, 3);
// print_r($siglas);die;
        $datos_maquina = $this->actionGetMaquina($siglas);
            
          //  $this->serial= strtoupper($this->serial);
            // PARA QUE SOLO SE GUARDEN VALORES EN MAYUSCULAS
            // print_r($siglas);die;
        if(empty($datos_maquina)){
            $this->addError('serial','Las siglas del serial son incorrectas');
        }else{
            //Si las siglas existen se valida que el serial este registrado
            $model_machine= Machines::model()->find('serial=:serial', array(':serial'=>$this->serial));
             
             if(is_null($model_machine)){
                //si el serial no estaregistrado en la tbl machine
                $this->addError('serial','El serial no existe registrado');
            }  else {
                $id= $model_machine['id'];
                //si el serial esta registrado se verifica que este enajenado
                $model_enajenado =  FicalizedMachines::model()->find('machine_id=:machine_id', array(':machine_id'=>$id));
                         
                if(is_null($model_enajenado['date'])){
                   //si no esta enajenado
                   $this->addError('serial','El serial no se encuentra enajenado'); 
                }
            }
            
        }
        
        
        
    }

    public function relations() {

        return array(
            'stAccesoriosOrdens' => array(self::HAS_MANY, 'STAccesoriosOrden', 'id_orden'),
             'stRepuestosOrdens' => array(self::HAS_MANY, 'StRepuestosOrden', 'id_orden'),
           // 'stHistorialOrdenes' => array(self::HAS_MANY, 'STHistorialOrdenes', 'id_orden', 'order'=>'stHistorialOrdenes.id DESC'),
            'stOrdenEnvios' => array(self::HAS_MANY, 'STOrdenEnvio', 'id_orden'),
            'viewOrdenEstatus' => array(self::HAS_MANY, 'ViewEstatusHistoria', 'id_orden'),
            'stGarantia' => array(self::BELONGS_TO, 'StGarantia', 'id_garantia'),
            'idDistribuidor' => array(self::BELONGS_TO, 'RBACUsuarios', 'id_distribuidor'),
            'idTecnico' => array(self::BELONGS_TO, 'RBACUsuarios', 'id_tecnico'),
            'stFallasOrdenes' => array(self::HAS_MANY, 'StFallasOrden', 'id_orden'),
            'stRegistroFotograficos' => array(self::HAS_MANY, 'STRegistroFotografico', 'id_orden'),
            
                //'operations' => array(self::HAS_ONE, 'Operations', 'serial'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('lang', 'ID'),
            'id_num_orden' => Yii::t('lang', 'Numero Orden'),
            'tecnico' => Yii::t('lang', 'Tecnico'),
            'distribuidor' => Yii::t('lang', 'Distribuidor'),
            'id_distribuidor' => Yii::t('lang', 'Distribuidor'),
            'id_tecnico' => Yii::t('lang', 'Tecnico'),
            'id_producto' => Yii::t('lang', 'Producto'),
            'marca' => Yii::t('lang', 'Marca'),
            'modelo' => Yii::t('lang', 'Modelo'),
            'serial' => Yii::t('lang', 'Serial'),
            'descripcion_falla' => Yii::t('lang', 'Descripcion Falla'),
            'revision_previa' => Yii::t('lang', 'Revision Previa'),
            'contacto' => Yii::t('lang', 'Contacto'),
            'telefono' => Yii::t('lang', 'Telefono'),
            'medio_envio' => Yii::t('lang', 'Medio Envio'),
            'medio_recepcion' => Yii::t('lang', 'Medio Recepcion'),
            'direccion_recepcion' => Yii::t('lang', 'Direccion Recepcion'),
            'email' => Yii::t('lang', 'Email'),
            'fecha_registro' => Yii::t('lang', 'Fecha Creación'),
            'fecha_enajenacion' => Yii::t('lang', 'Fecha Enajenacion'),
            'id_garantia' => Yii::t('lang', 'Garantia'),
            'estatus' => Yii::t('lang', 'Estatus'),
        );
    }

    public function search() {

        $criteria = new CDbCriteria;
        $criteria->with = array('viewOrdenEstatus');

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.id_num_orden', $this->id_num_orden, true);
        $criteria->compare('t.id_distribuidor', $this->id_distribuidor);
        $criteria->compare('t.id_tecnico', $this->id_tecnico);
        $criteria->compare('t.id_producto', $this->id_producto);
        $criteria->compare('t.marca', $this->marca, true);
        $criteria->compare('t.modelo', $this->modelo, true);
        $criteria->compare('t.serial', $this->serial, true);
        $criteria->compare('t.descripcion_falla', $this->descripcion_falla, true);
        $criteria->compare('t.revision_previa', $this->revision_previa, true);
        $criteria->compare('t.contacto', $this->contacto, true);
        $criteria->compare('t.telefono', $this->telefono, true);
        $criteria->compare('t.medio_envio', $this->medio_envio, true);
        $criteria->compare('t.medio_recepcion', $this->medio_recepcion, true);
        $criteria->compare('t.direccion_recepcion', $this->direccion_recepcion, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.fecha_registro', $this->fecha_registro, true);
        $criteria->compare('t.fecha_enajenacion', $this->fecha_enajenacion, true);
        $criteria->compare('t.id_garantia', $this->id_garantia);
//        $criteria->compare('idDistribuidor.nombreusuario', $this->distribuidor, true);
//        $criteria->compare('idTecnico.nombreusuario', $this->tecnico, true);
  //     $criteria->compare('viewOrdenEstatus.nombre', $this->estatus, true);

        /* Condicion para no Mostrar los Eliminados en Gridview */
        //$criteria->addcondition("estatus <> 9");

        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }
   
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
       public function actionGetMaquina($siglas) {

        // $sql2 = Yii::app()->db->createCommand()
        //         ->select('p.model, b.name')
        //         ->from('factoco_site.products p')
        //         ->join('factoco_site.brands b', 'p.brand_id=b.id')
        //         ->join('factoco_site.brands_country c', 'b.id=c.brand_id')
        //         ->where('c.country_id=:country_id and p.acronym=:siglas', array(':country_id' => 'pa', ':siglas' => $siglas))
        //         ->queryRow();

            $sql2 = Yii::app()->db->createCommand()
                ->select('p.model, b.name')
                ->from('tfhka_prueba.products p')
                ->join('tfhka_prueba.brands b', 'p.brand_id=b.id')
                ->join('tfhka_prueba.brands_country c', 'b.id=c.brand_id')
                ->where('c.country_id=:country_id and p.acronym=:siglas', array(':country_id' => Yii::app()->params['pais'], ':siglas' => $siglas))
                ->queryRow();
        
               

        return $sql2;
    }

}
