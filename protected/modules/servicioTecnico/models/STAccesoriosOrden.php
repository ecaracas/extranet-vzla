<?php

/**
 * This is the model class for table "st_accesorios_orden".
 *
 * The followings are the available columns in table 'st_accesorios_orden':
 * @property integer $id
 * @property integer $id_orden
 * @property integer $id_accesorio
 * @property string $fecha
 */
class STAccesoriosOrden extends CActiveRecord
{
	
	public function tableName()
	{
		return 'st_accesorios_orden';
	}

	
	public function rules()
	{
		
		return array(
			//array('id', 'required'),
			//array('id, id_orden, id_accesorio', 'numerical', 'integerOnly'=>true),
			array('fecha', 'safe'),
		
			array('id, id_orden, id_accesorio, fecha', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
                    
                    'idAccesorio' => array(self::BELONGS_TO, 'StAccesorios', 'id_accesorio'),
                    'idOrdenA' => array(self::BELONGS_TO, 'STOrdenServicio', 'id_orden'),
                    
                    
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'id_orden' => Yii::t('lang','Id Orden'),
			'id_accesorio' => Yii::t('lang','Id Accesorio'),
			'fecha' => Yii::t('lang','Fecha'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_orden',$this->id_orden);
		$criteria->compare('id_accesorio',$this->id_accesorio);
		$criteria->compare('fecha',$this->fecha,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
