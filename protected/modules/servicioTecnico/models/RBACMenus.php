<?php

/**
 * This is the model class for table "rbac_menus".
 *
 * The followings are the available columns in table 'rbac_menus':
 * 
 * @property integer $id
 * @property string $descripcion
 * @property string $url
 * @property integer $url_tipo
 * @property string $categoria
 * @property integer $estatus
 * @property integer $orden
 * @property string $icono
 * @property string $modulo
 * @property integer $jerarquia
 * @property string $registro_fecha
 * @property string $registro_hora
 * @property string $modificado_fecha
 * @property string $modificado_hora
 * @property integer $secuencia The followings are the available model relations:
 * @property RbacMenuOpciones[] $rbacMenuOpciones
 */
class RBACMenus extends CActiveRecord
{
	public function tableName()
	{
		return 'rbac_menus';
	}
        
            public function behaviors(){
    return array(
        // Classname => path to Class
        'LogsableBehavior'=>'application.behaviors.LogsableBehavior',
              
    );
}
	
	public function rules()
	{
		return array(
				array(
						'descripcion, orden, modulo, registro_fecha, modificado_fecha',
						'required' ),
				array(
						'url_tipo, estatus, orden, jerarquia, secuencia',
						'numerical',
						'integerOnly' => true ),
				array(
						'descripcion',
						'length',
						'max' => 100 ),
				array(
						'url',
						'length',
						'max' => 255 ),
				array(
						'categoria',
						'length',
						'max' => 20 ),
				array(
						'icono',
						'length',
						'max' => 50 ),
				array(
						'modulo',
						'length',
						'max' => 30 ),
				array(
						'registro_hora, modificado_hora',
						'safe' ),
				
				array(
						'id, descripcion, url, url_tipo, categoria, estatus, orden, icono, modulo, jerarquia, registro_fecha, registro_hora, modificado_fecha, modificado_hora, secuencia',
						'safe',
						'on' => 'search' ) );
	}
	
	public function relations()
	{
		return array(
				'rbacMenuOpciones' => array(
						self :: HAS_MANY,
						'RBACMenuOpciones',
						'menu_id' ) );
	}
	
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'descripcion' => 'Descripcion',
				'url' => 'Url',
				'url_tipo' => 'Url Tipo',
				'categoria' => 'Categoria',
				'estatus' => 'Estatus',
				'orden' => 'Orden',
				'icono' => 'Icono',
				'modulo' => 'Modulo',
				'jerarquia' => 'Jerarquia',
				'registro_fecha' => 'Registro Fecha',
				'registro_hora' => 'Registro Hora',
				'modificado_fecha' => 'Modificado Fecha',
				'modificado_hora' => 'Modificado Hora',
				'secuencia' => 'Secuencia' );
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		
		$criteria->compare('id', $this -> id);
		$criteria->compare('descripcion', $this -> descripcion, true);
		$criteria->compare('url', $this -> url, true);
		$criteria->compare('url_tipo', $this -> url_tipo);
		$criteria->compare('categoria', $this -> categoria, true);
		$criteria->compare('estatus', $this -> estatus);
		$criteria->compare('orden', $this -> orden);
		$criteria->compare('icono', $this -> icono, true);
		$criteria->compare('modulo', $this -> modulo, true);
		$criteria->compare('jerarquia', $this -> jerarquia);
		$criteria->compare('registro_fecha', $this -> registro_fecha, true);
		$criteria->compare('registro_hora', $this -> registro_hora, true);
		$criteria->compare('modificado_fecha', $this -> modificado_fecha, true);
		$criteria->compare('modificado_hora', $this -> modificado_hora, true);
		$criteria->compare('secuencia', $this -> secuencia);
		
		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");
		
		if(Yii::app() -> params['GridViewOrder'] == FALSE)
		{
			$criteria -> order = "id DESC";
		}
		
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria ));
	}
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
