<?php

/**
 * This is the model class for table "st_fallas_orden".
 *
 * The followings are the available columns in table 'st_fallas_orden':
 * @property integer $id
 * @property integer $id_falla
 * @property integer $id_orden
 * @property string $fecha
 *
 * The followings are the available model relations:
 * @property StFallas $idFalla
 */
class StFallasOrden extends CActiveRecord
{
	
	public function tableName()
	{
		return 'st_fallas_orden';
	}

	
	public function rules()
	{
		
		return array(
			array('id_falla, id_orden, fecha', 'required'),
			array('id_falla, id_orden', 'numerical', 'integerOnly'=>true),
		
			array('id, id_falla, id_orden, fecha', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'idFalla' => array(self::BELONGS_TO, 'StFallas', 'id_falla'),
                        'idOrden' => array(self::BELONGS_TO, 'STOrdenServicio', 'id_orden'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'id_falla' => Yii::t('lang','Id Falla'),
			'id_orden' => Yii::t('lang','Id Orden'),
			'fecha' => Yii::t('lang','Fecha'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_falla',$this->id_falla);
		$criteria->compare('id_orden',$this->id_orden);
		$criteria->compare('fecha',$this->fecha,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
