<?php

/**
 * This is the model class for table "st_registro_fotografico".
 *
 * The followings are the available columns in table 'st_registro_fotografico':
 * @property integer $id
 * @property string $path_imagen
 * @property integer $id_orden
 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property StOrdenServicio $idOrden
 */
class StRegistroFotografico extends CActiveRecord
{
	
	public function tableName()
	{
		return 'st_registro_fotografico';
	}

	
	public function rules()
	{
		
		return array(
			array('id_orden', 'required'),
			array('id_orden', 'numerical', 'integerOnly'=>true),
			array('path_imagen', 'length', 'max'=>100),
			array('created_at', 'safe'),
		
			array('id, path_imagen, id_orden, created_at', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'idOrden' => array(self::BELONGS_TO, 'StOrdenServicio', 'id_orden'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'path_imagen' => Yii::t('lang','Path Imagen'),
			'id_orden' => Yii::t('lang','Id Orden'),
			'created_at' => Yii::t('lang','Created At'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('path_imagen',$this->path_imagen,true);
		$criteria->compare('id_orden',$this->id_orden);
		$criteria->compare('created_at',$this->created_at,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
