<?php

/**
 * This is the model class for table "st_garantia".
 *
 * The followings are the available columns in table 'st_garantia':
 * @property integer $id
 * @property string $descripcion
 * @property string $fecha_creacion
 * @property integer $creado_por
 * @property string $fecha_editado
 * @property integer $editado_por
 * @property string $fecha_eliminado
 * @property integer $eliminado_por
 * @property integer $estatus
 */
class StGarantia extends CActiveRecord {

    public function tableName() {
        return 'st_garantia';
    }

    public function rules() {

        return array(
            array('descripcion, fecha_creacion, usuario_crea, estatus', 'required'),
            array('usuario_crea, usuario_modifica, estatus', 'numerical', 'integerOnly' => true),
            array('descripcion', 'length', 'max' => 100),
            array('fecha_editado, fecha_eliminado', 'safe'),
            array('id, descripcion, fecha_creacion, hora_creacion, usuario_crea, fecha_modificacion, hora_modificacion, usuario_modifica, estatus', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {

        return array(
            'usuarioCrea' => array(self::BELONGS_TO, 'RBACUsuarios', 'usuario_crea'),
            'usuarioModifica' => array(self::BELONGS_TO, 'RBACUsuarios', 'usuario_modifica'),
            'stFallasO' => array(self::HAS_MANY, 'StFallasOrden', 'id_falla'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('lang', 'ID'),
            'descripcion' => Yii::t('lang', 'Nombre Garantia'),
            'fecha_creacion' => Yii::t('lang', 'Fecha Creacion'),
            'hora_creacion' => Yii::t('lang', 'Hora Creacion'),
            'usuario_crea' => Yii::t('lang', 'Usuario Crea'),
            'fecha_modificacion' => Yii::t('lang', 'Fecha Modificacion'),
            'hora_modificacion' => Yii::t('lang', 'Hora Modificacion'),
            'usuario_modifica' => Yii::t('lang', 'Usuario Modifica'),
            'estatus' => Yii::t('lang', 'Estatus'),
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('descripcion', $this->descripcion, true);
        $criteria->compare('fecha_creacion', $this->fecha_creacion, true);
        $criteria->compare('hora_creacion', $this->hora_creacion, true);
        $criteria->compare('usuario_crea', $this->usuario_crea);
        $criteria->compare('fecha_modificacion', $this->fecha_modificacion, true);
        $criteria->compare('hora_modificacion', $this->hora_modificacion, true);
        $criteria->compare('usuario_modifica', $this->usuario_modifica);
        $criteria->compare('estatus', $this->estatus);

        /* Condicion para no Mostrar los Eliminados en Gridview */
        $rol = RBACUsuarios::model()->find('id=:id', array(':id' => Yii::app()->user->id));
        if ($rol->rol_id != 2) {
            $criteria->addcondition("estatus <> 9");
        }

        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "id DESC";
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
