<?php

/**
 * This is the model class for table "view_estatus_historia".
 *
 * The followings are the available columns in table 'view_estatus_historia':
 * @property integer $id
 * @property string $id_num_orden
 * @property string $serial
 * @property string $estatus
 * @property integer $id_historia
 * @property string $fecha_enajenacion
 * @property string $fecha_registro
 * @property integer $garantia
 */
class ViewEstatusHistoria extends CActiveRecord
{
	
	public function tableName()
	{
		return 'view_estatus_historia';
	}

	
	public function rules()
	{
		
		return array(
			array('estatus', 'required'),
			array('id, id_historia, garantia', 'numerical', 'integerOnly'=>true),
			array('id_num_orden, serial, estatus', 'length', 'max'=>45),
			array('fecha_enajenacion, fecha_registro', 'safe'),
		
			array('id, id_num_orden, serial, estatus, id_historia, fecha_enajenacion, fecha_registro, garantia', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'id_num_orden' => Yii::t('lang','Numero De Orden'),
			'serial' => Yii::t('lang','Serial'),
			'estatus' => Yii::t('lang','Estatus'),
			'id_historia' => Yii::t('lang','Id Historia'),
			'fecha_enajenacion' => Yii::t('lang','Fecha Enajenacion'),
			'fecha_registro' => Yii::t('lang','Fecha Registro'),
			'garantia' => Yii::t('lang','Garantia'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_num_orden',$this->id_num_orden,true);
		$criteria->compare('serial',$this->serial,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('id_historia',$this->id_historia);
		$criteria->compare('fecha_enajenacion',$this->fecha_enajenacion,true);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('garantia',$this->garantia);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		//$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
