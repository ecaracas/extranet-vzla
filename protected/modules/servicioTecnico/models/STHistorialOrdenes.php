<?php

/**
 * This is the model class for table "st_historial_ordenes".
 *
 * The followings are the available columns in table 'st_historial_ordenes':
 * @property integer $id
 * @property integer $id_orden
 * @property integer $id_usuariorevision
 * @property integer $id_estatus
 * @property string $descripcion
 * @property string $fecha
 */
class STHistorialOrdenes extends CActiveRecord {

    public $usuario_r;
    public $reg_fotografico;
    public $fallas;
    public $repuestos;
    public $tt;

    public function tableName() {
        return 'st_historial_ordenes';
    }

    public function rules() {

        return array(
            //array('id', 'required'),
            array('id_orden, id_usuariorevision, id_estatus', 'numerical', 'integerOnly' => true),
           array('reg_fotografico', 'file', 'allowEmpty'=>true,  'types' => 'jpg,png', ),
            array('descripcion', 'length', 'max' => 150),
            array('id_estatus, descripcion', 'required'),
            array('fecha, fallas,repuestos', 'safe'),
            array('id, id_orden, id_usuariorevision, id_estatus, descripcion, fecha, usuario_r, reg_fotografico, fallas,repuestos', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {

        return array(
            //'stFallasHistorias' => array(self::HAS_MANY, 'StFallasHistoria', 'id_historia'),
            'stEstatus' => array(self::BELONGS_TO, 'StEstatus', 'id_estatus'),
            'idOrdenes' => array(self::BELONGS_TO, 'STOrdenServicio', 'id_orden'),
            'idUsuarioRevision' => array(self::BELONGS_TO, 'RBACUsuarios', 'id_usuariorevision'),
            //'stRegistroFotograficos' => array(self::HAS_MANY, 'STRegistroFotografico', 'id_historia'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('lang', 'ID'),
            'id_orden' => Yii::t('lang', 'Numero de orden'),
            'id_usuariorevision' => Yii::t('lang', 'Usario revisa'),
            'id_estatus' => Yii::t('lang', 'Estatus'),
            'descripcion' => Yii::t('lang', 'Observaciones'),
            'fecha' => Yii::t('lang', 'Fecha'),
            'usuario_r' => Yii::t('lang', 'Registrado por'),
            'reg_fotografico' => Yii::t('lang', 'Registro Fotografico'),
            'fallas' => Yii::t('lang', 'Fallas'),
             'repuestos' => Yii::t('lang', 'Repuestos')
            
        );
    }

    public function search($id_orden = NULL) {

        $criteria = new CDbCriteria;
        $criteria->with = array('idUsuarioRevision','stEstatus');

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.id_orden', $this->id_orden);
        $criteria->compare('t.id_usuariorevision', $this->id_usuariorevision);
        $criteria->compare('stEstatus.nombre', $this->id_estatus, true);
        $criteria->compare('t.descripcion', $this->descripcion, true);
        $criteria->compare('t.fecha', $this->fecha, true);
        $criteria->compare('concat(idUsuarioRevision.nombre, " ", idUsuarioRevision.apellido)', $this->usuario_r, true);

        /* Condicion para no Mostrar los Eliminados en Gridview */
        //$criteria->addcondition("estatus <> 9");

        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
