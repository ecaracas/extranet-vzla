<?php

/**
 * This is the model class for table "machines".
 *
 * The followings are the available columns in table 'machines':
 * @property integer $id
 * @property string $serial
 * @property integer $user_id
 * @property integer $user_associated
 * @property integer $social_reason_id
 * @property string $date
 * @property string $date_fiscal
 * @property string $observation
 * @property integer $status
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property RbacUsuarios $user
 */
class Machines extends CActiveRecord
{
	
	public function tableName()
	{
		return 'machines';
	}

	
	public function rules()
	{
		
		return array(
			array('user_associated, social_reason_id, status', 'required'),
			array('user_id, user_associated, social_reason_id, status', 'numerical', 'integerOnly'=>true),
			array('serial', 'length', 'max'=>6),
			array('date, date_fiscal, observation, created, modified', 'safe'),
                        array('modified','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
                        array('created,modified','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			array('id, serial, user_id, user_associated, social_reason_id, date, date_fiscal, observation, status, created, modified', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'user' => array(self::BELONGS_TO, 'RbacUsuarios', 'user_id'),
			'socialReason' => array(self::BELONGS_TO, 'SocialReasons', 'social_reason_id'),
                        'fiscalizedMachines' => array(self::HAS_MANY, 'FiscalizedMachines', 'machine_id'),
            
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'serial' => Yii::t('lang','Serial'),
			'user_id' => Yii::t('lang','User'),
			'user_associated' => Yii::t('lang','User Associated'),
			'social_reason_id' => Yii::t('lang','Social Reason'),
			'date' => Yii::t('lang','Date'),
			'date_fiscal' => Yii::t('lang','Date Fiscal'),
			'observation' => Yii::t('lang','Observation'),
			'status' => Yii::t('lang','Status machine'),
			'created' => Yii::t('lang','Created'),
			'modified' => Yii::t('lang','Modified'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('serial',$this->serial,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_associated',$this->user_associated);
		$criteria->compare('social_reason_id',$this->social_reason_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('date_fiscal',$this->date_fiscal,true);
		$criteria->compare('observation',$this->observation,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		//$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}
                
               

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function mydevice()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('serial',$this->serial,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_associated',$this->user_associated);
		$criteria->compare('social_reason_id',$this->social_reason_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('date_fiscal',$this->date_fiscal,true);
		$criteria->compare('observation',$this->observation,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		//$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}
               

                $criteria->with = array('user' => array('with' => 'rol'));
                
               

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}



	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
