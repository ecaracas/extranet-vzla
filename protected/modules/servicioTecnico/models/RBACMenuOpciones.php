<?php

/**
 * This is the model class for table "rbac_menu_opciones".
 *
 * The followings are the available columns in table 'rbac_menu_opciones':
 * 
 * @property integer $id
 * @property integer $menu_id
 * @property string $descripcion
 * @property string $opcion
 * @property string $url
 * @property integer $url_tipo
 * @property string $categoria
 * @property integer $estatus
 * @property integer $orden
 * @property string $icono
 * @property integer $jerarquia
 * @property string $registro_fecha
 * @property string $registro_hora
 * @property string $modificado_fecha
 * @property string $modificado_hora
 * @property integer $secuencia The followings are the available model relations:
 * @property RbacMenus $menu
 * @property RbacRolesOpciones[] $rbacRolesOpciones
 */
class RBACMenuOpciones extends CActiveRecord
{
	public function tableName()
	{
		return 'rbac_menu_opciones';
	}
        
            public function behaviors(){
    return array(
        // Classname => path to Class
        'LogsableBehavior'=>'application.behaviors.LogsableBehavior',
              
    );
}
	
	public function rules()
	{
		return array(
				array(
						'menu_id, descripcion, opcion, orden, registro_fecha, modificado_fecha',
						'required' ),
				array(
						'menu_id, url_tipo, estatus, orden, jerarquia, secuencia',
						'numerical',
						'integerOnly' => true ),
				array(
						'descripcion, opcion',
						'length',
						'max' => 100 ),
				array(
						'url',
						'length',
						'max' => 255 ),
				array(
						'categoria',
						'length',
						'max' => 20 ),
				array(
						'icono',
						'length',
						'max' => 50 ),
				array(
						'registro_hora, modificado_hora',
						'safe' ),
				
				array(
						'id, menu_id, descripcion, opcion, url, url_tipo, categoria, estatus, orden, icono, jerarquia, registro_fecha, registro_hora, modificado_fecha, modificado_hora, secuencia',
						'safe',
						'on' => 'search' ) );
	}
	
	public function relations()
	{
		return array(
				'menu' => array(
						self :: BELONGS_TO,
						'RBACMenus',
						'menu_id' ),
				'rbacRolesOpciones' => array(
						self :: HAS_MANY,
						'RBACRolesOpciones',
						'menu_opcion_id' ) );
	}
	
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'menu_id' => 'Menu',
				'descripcion' => 'Descripcion',
				'opcion' => 'Opcion',
				'url' => 'Url',
				'url_tipo' => 'Url Tipo',
				'categoria' => 'Categoria',
				'estatus' => 'Estatus',
				'orden' => 'Orden',
				'icono' => 'Icono',
				'jerarquia' => 'Jerarquia',
				'registro_fecha' => 'Registro Fecha',
				'registro_hora' => 'Registro Hora',
				'modificado_fecha' => 'Modificado Fecha',
				'modificado_hora' => 'Modificado Hora',
				'secuencia' => 'Secuencia' );
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		
		$criteria->compare('id', $this -> id);
		$criteria->compare('menu_id', $this -> menu_id);
		$criteria->compare('descripcion', $this -> descripcion, true);
		$criteria->compare('opcion', $this -> opcion, true);
		$criteria->compare('url', $this -> url, true);
		$criteria->compare('url_tipo', $this -> url_tipo);
		$criteria->compare('categoria', $this -> categoria, true);
		$criteria->compare('estatus', $this -> estatus);
		$criteria->compare('orden', $this -> orden);
		$criteria->compare('icono', $this -> icono, true);
		$criteria->compare('jerarquia', $this -> jerarquia);
		$criteria->compare('registro_fecha', $this -> registro_fecha, true);
		$criteria->compare('registro_hora', $this -> registro_hora, true);
		$criteria->compare('modificado_fecha', $this -> modificado_fecha, true);
		$criteria->compare('modificado_hora', $this -> modificado_hora, true);
		$criteria->compare('secuencia', $this -> secuencia);
		
		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");
		
		if(Yii::app() -> params['GridViewOrder'] == FALSE)
		{
			$criteria -> order = "id DESC";
		}
		
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria ));
	}
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
