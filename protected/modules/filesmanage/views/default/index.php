<?php
/* @var $this FilesController */
/* @var $model Files */
$this->pageTitle = Yii::app()->name;
$active = array(
    '0' => 'Inactivos',
    '1' => 'Activos',
);
foreach ($active as $key => $value) {
    $listData[$key] = $value;
}

?>
  <p align="right"><a href="filesmanage/default/descargas" class="bottomLeft "><i class="btn btn-small btn-info">SUBIR ARCHIVOS</i></a></p>
<div style="padding-left: 20px;">
    <h2 style="text-align: center;"><?php echo Yii::t('pages', 'Manejador de Archivos '); ?></h2>
    <hr style="width: 30%;"/>
    <p>
<?php echo Yii::t('pages', 'You may optionally enter a comparison operator'); ?> (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
<?php echo Yii::t('pages', 'or'); ?> <b>=</b>) 
<?php echo Yii::t('pages', 'at the beginning of each of your search values to specify how the comparison should be done.'); ?>
    </p>
</div>
<div style="padding: 5px; ">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'files-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'name',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->description)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
        
          array(
            'name' => 'version',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->version)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
        
           array(
            'name' => 'date_publishing',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->date_publishing)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
          array(
            'name' => 'type',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->type)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
          array(
            'name' => 'size',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->size)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::app()->params['delete-text'],           
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            
        ),
        
//        array(
//            'class' => 'CButtonColumn',
//            'header' => Yii::t('modules', "Actions"),
//            'template' => '{delete}',
//            'buttons' => array(
//
//                'delete' => array(
//                    'imageUrl' => Yii::app()->request->baseUrl . '/img/icn_trash.png',
//                ),
//            )
//        ),
//        
        
    ),
));
?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.make-switch').on('switch-change', function(e, data) {
            div = $(this);
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->request->baseUrl; ?>/filesmanage/updateActive",
                cache: false,
                data: {active: data.value, id: $(this).attr("id")}
            }).done(function(response) {
                if (response !== '')
                {
                    alert(response);
                    div.bootstrapSwitch('setState', false, true);

                }
            });
        });
    });
    //CUANDO SE EJECUTA UNA FUNCION AJAX POR EL CGRIDVIEW EL ANTERIOR SE PIERDE POR ESO SE HACE LA FUNCION DE ABAJO
    $(document).ajaxComplete(function(event, xhr, settings) {
        if (settings.type === "GET")
        {
            $('.make-switch').on('switch-change', function(e, data) {
                div = $(this);
                $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/filesmanage/updateActive",
                    cache: false,
                    data: {active: data.value, id: $(this).attr("id")}
                }).done(function(response) {
                    if (response !== '')
                    {
                        alert(response);
                        div.bootstrapSwitch('setState', false, true);

                    }
                });
            });
        }
    });
</script>