<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse"  style="color:#000;">
                        Descargas
                    </a>
                </h4>
            </div>
            <?php //$in = (Yii::app()->user->type != "a") ? 'in' : ''; ?>
            <div  <?php //echo $in ?>">
                <div class="panel-body">

                    <div class="list-group">
                        <?php
                        $downloads = filesDocument::model()->findall();

                        foreach ($downloads as $d) {
                            ?> 

                            <div href="#" class="list-group-item lista row" style="width:100%; padding: 0;">
                                <div class="col-lg-5 filedownload" style="font-size:14px;margin-top:6px; border-right:solid 1px #ccc; "><?php echo $d->name; ?></div>
                                <div class="col-lg-4 filedownload" style="font-size:11px;margin-top:6px;">
                                    <?php echo $d->description; ?>
                                </div>

                                <div class="col-lg-2 filedownload" style="">

                                    <a href="<?php echo Yii::app()->request->baseUrl . $d->path; ?>" target="_blank">
                                        <span class="glyphicon glyphicon-download-alt btn btn-success" style="float:right;  "></span>
                                    </a>
                                </div>

                                
                                    <div class="col-lg-1 filedownload" style="">

                                        <a href="<?php echo Yii::app()->request->baseUrl . '/filesmanage/default/delete/'. $d->id; ?>">
                                            <span class="btn btn-danger" style="float:right;  ">
                                                <span class="glyphicon glyphicon-trash" style="  "></span></span>
                                        </a>
                                    </div> 
                             
                            </div>

                        <?php } ?>
                        
                       
                    </div>

                </div>
            </div>


        </div>