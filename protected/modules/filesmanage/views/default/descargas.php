    <?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<style>
    .filedownload{
        padding: 10px;

    }
</style>
<div class="panel-group" id="accordion">
    

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse"  style="color: #000;">
                       <strong> Subir archivos</strong>
                    </a>
                </h4>
            </div>

            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body" style="margin: 10px;  ">

                    <div style="margin-top:30px; ">
                        <form method="post" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                
                                 <div class="panel-body">
                                     <input type="file" name="file" style="margin-right: 60px;"><br>
                                    <b>Tipo de archivos</b><br/><br/>
                                    <label>
                                        <select  name="type_a" id="type_a">
                                            <option>Desarrollador  (SDK)</option>
                                            <option>Aplicación</option>
                                            <option>Controlador Desarrollador </option>
                                            <option>Firmware</option>
                                            <option>Distribuidor kit </option>
                                            <option>Controlador Desarrollador </option>
                                            <option>Guía </option>
                                            <option>Manual</option>
                                            <option>Utilitarios</option>
                                        </select>
                                    </label>      
                                    
                                    <br/>
                                </div>
                                
                                
                                <div class="panel-body">
                                    <b>Marcas</b><br/>
                                    <label>
                                        <select  name="marcas" id="marcas">
                                            <option>Genericos</option>
                                            <option>ACLAS</option>
                                            <option>BIXOLON</option>
                                            <option>CUSTON</option>
                                            <option>OKI</option>
                                            <option>STAR</option>
                                            <option>UNIWEL</option>
                                            <option>FISCAT</option>
                                            <option>BIXOLON</option>
                                            <option>DASCOM</option>
                                        </select>
                                    </label>      
                                </div>
<!--                                <div class="panel-body">
                                    <b>Productos</b><br/><br/>
                                    <label>
                                        <select name="select" name="type" id="type">
                                            <option>PP9</option>
                                            <option>CR2300</option>
                                            <option>PS1X</option>
                                        </select>
                                    </label>      
                                </div>-->
                                
                                
                                <div class="panel-body">
                                    <b>Versión</b>
                                    <input type="text" name="version" id="version" class="form-control" style="width:10%;">
                                           <br/>
                                </div>
                                  <div class="panel-body">
                                    
                                    <b>Descripción</b>
                                        <input type="text" name="description" id="description_file" class="form-control" style="width:50%;"> <br/>
                                </div>
                                <div class="panel-footer"> <input type="submit" class="btn btn-primary glyphicon glyphicon-open" ></div>
                              
                            
                            </div>
                        </form> 

                        <br/> 



                    </div>


                </div>
            </div>
    
    </div>
</div>

<div id="detalle"></div>


<?php 

Yii::app()->clientScript->registerScript('search', "    

        //Detalle de usuario
        jQuery('body').on('click', '.detailuser', function() {
            jQuery.ajax({'beforeSend': function() {
                    $('#detalle').html('');
                    $('#detalle').dialog({width: 600, height: 400});
                },  'success': function(data) {
                    jQuery('#detalle').html(data);
                }, 'type': 'POST', 'url': $(this).attr('href'), 'cache': false, 'data': jQuery(this).parents('form').serialize()});
            return false;
        });
        
         //Cambio de Pass Grid
        jQuery('body').on('click', '.changepassgrid', function() {
            jQuery.ajax({'beforeSend': function() {
                    $('#detalle').html('');
                    $('#detalle').dialog({width: 600, height: 400});
                },  'success': function(data) {
                    jQuery('#detalle').html(data);
                }, 'type': 'POST', 'url': $(this).attr('href'), 'cache': false, 'data': jQuery(this).parents('form').serialize()});
            return false;
        });
        
    //Boton Cambiar pass
        jQuery('body').on('click', '.updatepass', function() {
            jQuery.ajax({'beforeSend': function() {                   
                   
                },  'success': function(data) {
                    jQuery('#detalle').html(data);
                }, 'type': 'POST', 'url': $(this).attr('href'), 'cache': false, 'data': jQuery(this).parents('form').serialize()});
            return false;
        });
        

        



"); ?>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css'); ?>






<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array('id' => 'Dialoguser',
    'options' => array('title' => 'User Detail ', 'autoOpen' => false, 'modal' => true, 'width' => 1100, 'height' => 450, 'position' => 'top',
        'buttons' => array('Close' => 'js:function(){$(this).dialog("close")}'))));
?>
<div id="userload"> 


</div>
<div id="files_related" >
    <input type="hidden" id="product_id_related" value="as">
</div>
<?php
$this->endWidget();
?>

<script>

    $(document).ready(function() {

        $('.make-switch').on('switch-change', function(e, data) {
            div = $(this);

            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->request->baseUrl; ?>/site/userstatus",
                cache: false,
                data: {active: data.value, id: $(this).attr("id")}
            }).done(function(response) {

                if (response !== '')
                {
                    alert(response);
                    div.bootstrapSwitch('setState', false, true);

                }
            });
        });
    });

    $(document).ajaxComplete(function(event, xhr, settings) {

        if (settings.type === "GET")
        {

            $('.make-switch').on('switch-change', function(e, data) {
                div = $(this);
                $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/site/userstatus",
                    cache: false,
                    data: {active: data.value, id: $(this).attr("id")}
                }).done(function(response) {
                    if (response !== '')
                    {
                        alert(response);
                        div.bootstrapSwitch('setState', false, true);

                    }
                });
            });
        }



    });

    /*$('body').on('click', '.changepass', function(e, data) {

       /* $.post("<?php echo Yii::app()->request->baseUrl ?>/site/passworduser/" + $(this).attr('iduser'), function(data) {
            $("#userload").html(data);

        });
        $('#userload').dialog('open');
        alert('a');

    });*/

    $('body').on('click', '.viewuser', function(e, data) {



        $.post("<?php echo Yii::app()->request->baseUrl ?>/site/getuser/" + $(this).attr('iduser'), function(data) {
            $("#userload").html(data);

        });
        $('#Dialoguser').dialog('open');

    })

    $('body').on('change', '.select_ut', function() {

        $.post("<?php echo Yii::app()->request->baseUrl ?>/site/updatetype/" + $(this).attr('iduser'), function(data) {

            $('#users-grid').yiiGridView('update');

        });


    });

  /*  jQuery(function($) {

        jQuery('body').on('click', '.updatepass', function() {
            jQuery.ajax({'beforeSend': function() {
                    //jQuery('#codigogen').html('');
                    //jQuery('#btngen').html('');
                    //$('#estado').addClass('grid-view-loading');
                }, 'complete': function() {
                    //$('#estado').removeClass('grid-view-loading');
                }, 'success': function(data) {
                    jQuery('#userload').html(data);
                }, 'error': function() {
                    // $('#estado').addClass('loadingerror');
                }, 'type': 'POST', 'url': '<?php echo Yii::app()->request->baseUrl ?>/site/passworduser/' + $(this).attr('href'), 'cache': false, 'data': jQuery(this).parents("form").serialize()});
            return false;
        });
   jQuery('body').on('click', '.changepass_', function() {   
                    $('#userload').dialog('open');                    
   alert('a');
            jQuery.ajax({'beforeSend': function() {
                    $('#userload').dialog('open');                    
                }, 'complete': function() {
                    
                }, 'success': function(data) {
                    jQuery('#userload').html(data);
                }, 'error': function() {                    
                }, 'type': 'POST', 'url': '<?php echo Yii::app()->request->baseUrl ?>/site/passworduser/' + $(this).attr('href'), 'cache': false, 'data': jQuery(this).parents('form').serialize()});
            return false;
        });
    });*/


</script>