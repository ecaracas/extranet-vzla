<?php
/* @var $this FilesController */
/* @var $model Files */
$this->pageTitle = Yii::app()->name;
$active = array(
    '0' => 'Inactivos',
    '1' => 'Activos',
);
foreach ($active as $key => $value) {
    $listData[$key] = $value;
}
?>

  <br/>
<div  class="row">
 <div class="col-xs-12">
     <div class="table-responsive">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'files-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            
            'name' => 'name',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->description)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);',
            ),
        
          array(
            
            'name' => 'version',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->version)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);',
            
        ),
        
           array(
            'name' => 'date_publishing',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->date_publishing)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
          array(
            'name' => 'type',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->type)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
          array(
            'name' => 'size',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->size)),Yii::app()->request->baseUrl.Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::app()->params['delete-text'],           
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            
        ),
        
//        array(
//            'class' => 'CButtonColumn',
//            'header' => Yii::t('modules', "Actions"),
//            'template' => '{delete}',
//            'buttons' => array(
//
//                'delete' => array(
//                    'imageUrl' => Yii::app()->request->baseUrl . '/img/icn_trash.png',
//                ),
//            )
//        ),
//        
         
    ),
    
));
?></div>
 </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.make-switch').on('switch-change', function(e, data) {
            div = $(this);
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->request->baseUrl; ?>/filesmanage/updateActive",
                cache: false,
                data: {active: data.value, id: $(this).attr("id")}
            }).done(function(response) {
                if (response !== '')
                {
                    alert(response);
                    div.bootstrapSwitch('setState', false, true);

                }
            });
        });
    });
    //CUANDO SE EJECUTA UNA FUNCION AJAX POR EL CGRIDVIEW EL ANTERIOR SE PIERDE POR ESO SE HACE LA FUNCION DE ABAJO
    $(document).ajaxComplete(function(event, xhr, settings) {
        if (settings.type === "GET")
        {
            $('.make-switch').on('switch-change', function(e, data) {
                div = $(this);
                $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/filesmanage/updateActive",
                    cache: false,
                    data: {active: data.value, id: $(this).attr("id")}
                }).done(function(response) {
                    if (response !== '')
                    {
                        alert(response);
                        div.bootstrapSwitch('setState', false, true);

                    }
                });
            });
        }
    });
</script>