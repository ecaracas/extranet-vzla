<?php
/* @var $this ArchivosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Archivoses',
);

$this->menu = array(
    array(
        'label' => 'Nuevo Archivos',
        'url' => array('create'),
        'linkOptions' => array('class' => Yii::app()->params['create-btn']),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label' => 'Administrar Archivos',
        'url' => array('index'),
        'linkOptions' => array('class' => Yii::app()->params['index-btn']),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>
<!--<h3 style="">Descarga de Archivos</h3>-->
<ul class="nav nav-tabs" role="tablist">
    <?php
    $i = 1;
    foreach ($marcas as $marca) {
        // echo $marca->name;
        ?>

        <?php
        if ($i == 1) {
            ?>
            <li class="active"><a href="#<?php echo $marca->description ?>" role="tab" data-toggle="tab"><?php echo strtoupper($marca->name) ?></a>
            </li>
        <?php } else {
            ?>
            <li><a href="#<?php echo $marca->description ?>" role="tab" data-toggle="tab"><?php echo strtoupper($marca->name) ?></a></li>
        <?php } ?>


        <?php
        $i++;
    }
    ?> 
</ul> </br>

<!-- Tab panes -->
<div class="tab-content">

    <?php
    $i = 1;
    foreach ($marcas as $marca) {
//        $modelo = BrandsModels::model()->findAll(array(
//            'select' => 't.id, t.name',
//            'condition' => "brand_id='$marca->id'"
//        ));
        $modelo = BrandsModelsFiles::model()->findAll(array(
            'select' => "bm.id,bm.name",
            'join' => "inner join brands_models bm on (bm.id=t.brands_models) 
                        
                    ",
            'condition' => "brand_id='$marca->id'",
            'group' => "name"
        ));
        ?>
        <?php
        if ($i == 1) {
            ?>
            <div class="tab-pane active" id="<?php echo $marca->description ?>">
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                    $a = 1;
                    foreach ($modelo as $mol) {

                        if ($a == 1) {
                            ?>
                            <li class="active"><a href="#<?php echo $mol->id ?>" role="tab" data-toggle="tab"><?php echo $mol->name ?></a>
                            </li>
                        <?php } else {
                            ?>
                            <li><a href="#<?php echo $mol->id ?>" role="tab" data-toggle="tab"><?php echo $mol->name ?></a></li>
                        <?php } ?>


                        <?php
                        $a++;
                    }
                    ?>
                </ul>
                <div class="tab-content">

                    <?php
                    $a = 1;
                    foreach ($modelo as $mol) {

                        if ($a == 1) {
                            ?>
                            <div class="tab-pane active" id="<?php echo $mol->id ?>">
                                <?php
                                $grid->brands_models = $mol->id;
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => $mol->name . '-grid',
                                    'dataProvider' => $grid->search(),
                                    'filter' => $grid,
                                    'columns' => array(
                                        array('name' => 'file', 'value' => '@$data->file0->name'),
                                        array('name' => 'type', 'value' => '@$data->fileType->name'),
                                        array('name' => 'version', 'value' => '@$data->file0->version'),
                                        array('name' => 'dates', 'value' => '@$data->file0->dates'),
                                        array(
                                            'class' => 'CLinkColumn',
                                            'header' => 'Descarga',
                                            'label' => '<i class="fa fa-cloud-download"></i>',
                                            'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                                            //'urlExpression' => 'Yii::app()->params["pageUrl"].@$data->file0->path',
                                            'urlExpression' => 'Yii::app()->baseUrl."/downloads/".@$data->file0->path',
                                          //  'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                                        ),
                                        )
                                        )
                                );
                                ?>





                            </div>
                        <?php } else {
                            ?>
                            <div class="tab-pane " id="<?php echo $mol->id ?>">
                                <?php
                                $grid->brands_models = $mol->id;
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => $mol->name . '-grid',
                                    'dataProvider' => $grid->search(),
                                    'filter' => $grid,
                                    'columns' => array(
                                                array('name' => 'file', 'value' => '@$data->file0->name'),
                                        array('name' => 'type', 'value' => '@$data->fileType->name'),
                                        array('name' => 'version', 'value' => '@$data->file0->version'),
                                        array('name' => 'dates', 'value' => '@$data->file0->dates'),
                                        array(
                                            'class' => 'CLinkColumn',
                                            'header' => 'Descarga',
                                            'label' => '<i class="fa fa-cloud-download"></i>',
                                            'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                                          'urlExpression' => 'Yii::app()->baseUrl."/downloads/".@$data->file0->path',
                                          //  'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                                        ),
                                    )
                                        )
                                );
                                ?>
                            </div>

                        <?php } ?>
                        <?php
                        $a++;
                    }
                    ?>
                </div> 
            </div>
        <?php } else {
            ?>
            <div class="tab-pane " id="<?php echo $marca->description ?>">
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                    $a = 1;
                    foreach ($modelo as $mol) {

                        if ($a == 1) {
                            ?>
                            <li class="active"><a href="#<?php echo $mol->id ?>" role="tab" data-toggle="tab"><?php echo $mol->name ?></a>
                            </li>
                        <?php } else {
                            ?>
                            <li><a href="#<?php echo $mol->id ?>" role="tab" data-toggle="tab"><?php echo $mol->name ?></a></li>
                        <?php } ?>


                        <?php
                        $a++;
                    }
                    ?>
                </ul>
                <div class="tab-content">

                    <?php
                    $a = 1;
                    foreach ($modelo as $mol) {

                        if ($a == 1) {
                            ?>
                            <div class="tab-pane active" id="<?php echo $mol->id ?>">
                                <?php
                                $grid->brands_models = $mol->id;
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => $mol->name . '-grid',
                                    'dataProvider' => $grid->search(),
                                    'filter' => $grid,
                                    'columns' => array(
                                              array('name' => 'file', 'value' => '@$data->file0->name'),
                                        array('name' => 'type', 'value' => '@$data->fileType->name'),
                                        array('name' => 'version', 'value' => '@$data->file0->version'),
                                        array('name' => 'dates', 'value' => '@$data->file0->dates'),
                                        array(
                                            'class' => 'CLinkColumn',
                                            'header' => 'Descarga',
                                            'label' => '<i class="fa fa-cloud-download"></i>',
                                            'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                                           'urlExpression' => 'Yii::app()->baseUrl."/downloads/".@$data->file0->path',
                                         //   'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                                        ),
                                    )
                                        )
                                );
                                ?>
                            </div>
                        <?php } else {
                            ?>
                            <div class="tab-pane " id="<?php echo $mol->id ?>">
                                <?php
                                $grid->brands_models = $mol->id;
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => $mol->name . '-grid',
                                    'dataProvider' => $grid->search(),
                                    'filter' => $grid,
                                    'columns' => array(
                                                array('name' => 'file', 'value' => '@$data->file0->name'),
                                        array('name' => 'type', 'value' => '@$data->fileType->name'),
                                        array('name' => 'version', 'value' => '@$data->file0->version'),
                                        array('name' => 'dates', 'value' => '@$data->file0->dates'),
                                        array(
                                            'class' => 'CLinkColumn',
                                            'header' => 'Descarga',
                                            'label' => '<i class="fa fa-cloud-download"></i>',
                                            'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                                           'urlExpression' => 'Yii::app()->baseUrl."/downloads/".@$data->file0->path',
                                       //     'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                                        ),
                                    )
                                        )
                                );
                                ?>
                            </div>

                        <?php } ?>
                        <?php
                        $a++;
                    }
                    ?>
                </div> 
            </div>


            <?php
        }
        ?>

        <?php
        $i++;
    }
    ?>


</div>

<!-- Nav tabs -->
<?php ?>
<script type="text/javascript">
    $('#myTab a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>

