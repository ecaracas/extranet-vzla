<?php
Yii::app()->params['IDGridview'] = 'filemanager-grid';
Yii::app()->params['rutaUrlGridviewBoxSwitch'] = Yii::app()->controller->createUrl('updateActive');
$active = array('0' => 'Inactivos', '1' => 'Activos');
foreach ($active as $key => $value) {
    $listData[$key] = $value;
}
$cont = 1;
?>
<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            $("#yw0_button").trigger('click');
        }, 1);
    });
</script>
<script>
    showall = false;
    language = '<?php echo Yii::app()->language; ?>';
    $(document).ready(function () {
//        $('.todos').hide();
$('#select_filter').hide();
        $("#english_results").click(function () {
            showall = true;
            $(this).hide();
            filtro();
        })
    });
    $(function () {
        $('#file_type_select').change(function () {
            $(".todos").fadeIn(600);
            filtro();
        })
        $('#product_type_select').change(function ()
        {
            $('#product_select').val('0');
            $(".todos").fadeIn(600);
            $('.product').each(function ()
            {
                if ($(this).val() != 0)
                {
                    $(this).hide();
                }
            });
            $('.filter' + $(this).val()).show();
            if ($(this).val() == 0)
            {
                $('.product').show();
            }
            filtro();
        })
        $('#product_select').change(function ()
        {
            $(".todos").fadeIn(600);
            filtro();
        })
    });

    function filtro() {
        var registrosMostrar = new Array();
        var mostrar = 0;
        var count = 0;
        $('#search_error').hide();
        $('.todos').hide();
        fts = $('#file_type_select').val()
        pts = $('#product_type_select').val()
        ps = $('#product_select').val()
        $('.todos').each(function ()
        {
            $(this).hide();
            if (ps == 0 && pts == 0 && fts == $(this).attr('filter') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }
            if (fts == 0 && pts == 0 && ps == $(this).attr('product') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }
            if (fts == 0 && ps == 0 && pts == $(this).attr('product_type') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }

            if (fts == 0 && ps != 0 && ps == $(this).attr('product') && pts == $(this).attr('product_type') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }

            if (ps == 0 && fts != 0 && fts == $(this).attr('filter') && pts == $(this).attr('product_type') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }

            if (pts == 0 && ps != 0 && ps == $(this).attr('product') && fts == $(this).attr('filter') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }

            if (ps == 0 && pts != 0 && pts == $(this).attr('product_type') && fts == $(this).attr('filter') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }

            if (pts == 0 && fts != 0 && fts == $(this).attr('filter') && ps == $(this).attr('product') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }
            if (fts == 0 && pts != 0 && pts == $(this).attr('product_type') && ps == $(this).attr('product') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }

            if (fts != 0 && pts != 0 && ps != 0 && fts == $(this).attr('filter') && pts == $(this).attr('product_type') && ps == $(this).attr('product') || $(this).attr('product_type') == 6)
            {
                mostrar++;
                $(this).show();
            }
        });

        if (mostrar <= 0)
        {
            $('#select_filter').hide();
            $('#search_error').show();
        }
        if (fts == 0 && pts == 0 && ps == 0)
        {
            $('.todos').show();
            $('#select_filter').hide();
        } else
        {
            $('#select_filter').hide();
        }
    }
</script>
<style>
/*    .todos {
        display: none;
    }*/
    .container {
        padding-right: 0px !important;
        padding-left: 0px !important;
    }
    /* use navbar-wrapper to wrap navigation bar, the purpose is to overlay navigation bar above slider */
    .flotar {
        position: absolute;
        top: 10px;
        left: 0;
        width: 100%;
        height: 51px;
    }

    .navbar-wrapper {
        background-color: #FFFFFF;
        position: absolute;
        left: 0;
        width: 100%;
        height: 51px;
    }

    .navbar-wrapper>.container {
        padding: 0;
    }

    @media all and (max-width: 768px ) {
        .navbar-wrapper {
            position: relative;
            top: 0px;
        }
    }

    .jssorb21 div, .jssorb21 div:hover, .jssorb21 .av {
        background: url(../img/b21.png) no-repeat;
        overflow: hidden;
        cursor: pointer;
    }

    .jssorb21 div {
        background-position: -5px -5px;
    }

    .jssorb21 div:hover, .jssorb21 .av:hover {
        background-position: -35px -5px;
    }

    .jssorb21 .av {
        background-position: -65px -5px;
    }

    .jssorb21 .dn, .jssorb21 .dn:hover {
        background-position: -95px -5px;
    }

    .block {
        height: 300px;
        background: #666666;
        margin-bottom: 20px;
    }

    .menu {
        background: white !important;
        opacity: 0.8 !important;
        color: #000000;
        z-index: 1;
        padding: .5em;
        position: absolute;
        width: 100%;
    }

    .fixed {
        position: fixed;
        top: 0;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">
                <h3 class="panel-title"><?php //echo Yii::t('lang', Yii::app()->params['index-text']) . ' ' . Yii::t('lang', 'subir archivo');
                echo Yii::t('lang', 'ARCHIVOS UTILITARIOS');
                ?></h3>               
            </div>
            <div class="panel-body">

                <div class="table-responsive">
                    
                    <?php if (Yii::app()->authRBAC->checkAccess($this->modulo . '_upload')) { ?>
                        <p align="right">
                            <a href="<?php echo Yii::app()->request->baseUrl ?>/filesmanage/files/create" class="bottomLeft "> <i class="btn btn-small btn-info">SUBIR ARCHIVOS</i></a>
                        </p>
                    <?php } ?>
                </div>

                <div class="col-md-4">
                    <label><?php echo Yii::t('forms', 'TIPOS DE ARCHIVO'); ?></label>
                    <select id="file_type_select">
                        <option value="0" selected="selected"><?php echo Yii::t('forms', 'SELECCIONAR'); ?></option>
                        <?php if (count($tipoArchivos) > 0): ?>
                            <?php foreach ($tipoArchivos as $tipoArchivo): ?>
                                <?php
                                if ($cont == 1) {
                                    foreach ($type_file_rol as $id => $name) {

                                        $selected = ($id == $tipoArchivo['id']) ? '' : '';
                                        echo '<option value="' . $id . '" ' . $selected . '>' . $name . '</option>';
                                    }
                                    $cont++;
                                }
                                ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>

                <div class="col-md-4">
                    <label><?php echo Yii::t('forms', 'MARCAS'); ?></label>
                    <select id="product_type_select">
                        <option value="0" selected="selected"><?php echo Yii::t('forms', 'SELECCIONAR'); ?></option>
                        <?php
                        foreach ($filtros as $filtro) {
                            if ($filtro['visible'] == 1 && $filtro['id'] != 6) {
                                ?>
                                <option value="<?php echo $filtro['id']; ?>"><?php echo Yii::t('forms', $filtro['name']); ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-md-4">
                    <label><?php echo Yii::t('forms', 'MODELOS'); ?></label>
                    <select id="product_select">
                        <option value="0" selected="selected"><?php echo Yii::t('forms', 'SELECCIONAR'); ?></option>
                        <?php
                        foreach ($filtros as $filtro) {
                            foreach ($filtro->products as $prod) {
                                if ($prod['active'] == 1 && $filtro['id'] != 6) {
                                    ?>
                                    <option tipo="<?php echo $filtro['id'] ?>" class="product filter<?php echo $filtro['id']; ?>" value="<?php echo $prod['id']; ?>"><?php echo Yii::t('forms', $prod['name']); ?></option>
                                    <?php
                                }
                            }
                        }
                        ?> 
                    </select>
                </div>
                <br /><br />
                <br /><br />    

                <div id="filemanager-grid" class="grid-view">
                    <table class="table-responsive table table-bordered table-condensed table-hover table-striped download helvetica_neueregular">
                        <tr>
                            <th class="tituloheaderDownload"><?php echo Yii::t('forms', 'Extension '); ?></th>
                            <th class="tituloheaderDownload"><?php echo Yii::t('forms', 'Archivos'); ?></th>
                            <th class="tituloheaderDownload"><?php echo Yii::t('forms', 'Tipos de archivo'); ?></th>
                            <th class="tituloheaderDownload"><?php echo Yii::t('forms', 'Marcas'); ?></th>
                            <th class="tituloheaderDownload"><?php echo Yii::t('forms', 'Fecha'); ?></th>
                            <th class="tituloheaderDownload"><?php echo Yii::t('forms', 'Funciones'); ?></th>
                            <?php
                            if (Yii::app()->authRBAC->checkAccess($this->modulo . '_estatus')) {
                                ?>
                                <th class="tituloheaderDownload"><?php echo Yii::t('forms', 'Estatus'); ?></th>
                                <?php
                            }
                            ?>          
                            <!--<th class="tituloheaderDownload" colspan="1"></th>-->
                        </tr>
                        <?php
                        $total = 1;
//                        var_dump($Files);
                        if (count($Files) > 0) {
                            foreach ($Files as $File):
                                if ($File['pathTypeFile'] == 'Firmware') {
                                    $language = $File['language'];
                                    $image_file2 = Yii::app()->request->baseUrl . "/images/pdf-download.png";
                                } elseif ($File['pathTypeFile'] == 'Manager') {
                                    $language = Yii::app()->language;
                                    $image_file2 = Yii::app()->request->baseUrl . "/images/ies.png";
                                } else {
                                    $language = Yii::app()->language;
                                    $image_file2 = Yii::app()->request->baseUrl . "/images/ies.png";
                                }

                                if ($File['Extension'] == 'image/jpeg') {

                                    $image_file = Yii::app()->request->baseUrl . "/img/jpg.png";
                                }
                                if ($File['Extension'] == 'application/zip') {
                                    // $language=$File['language'];
                                    $image_file = Yii::app()->request->baseUrl . "/img/zip.png";
                                }
                                if ($File['Extension'] == 'application/pdf') {
                                    // $language=$File['language'];
                                    $image_file = Yii::app()->request->baseUrl . "/img/pdf.png";
                                }

                                if ($File['Extension'] != 'application/zip' && $File['Extension'] != 'image/jpeg' && $File['Extension'] != 'application/pdf') {
                                    $image_file = Yii::app()->request->baseUrl . "/img/otros.png";
                                }
                                ?>                            
                                <tr class="todos" fileid="<?php echo $File['fileid']; ?>" language="<?php echo $language ?>" filter='<?php echo $File['type']; ?>'product="<?php echo $File['product']; ?>" product_type='<?php echo $File['Product_Type_id']; ?>'  style="border-bottom: 1px solid #c5c5c5; <?php if ($language != Yii::app()->language) echo 'background-color: #f0f0f0'; ?>">

                                    <td><?php echo "<img src=" . $image_file . "  />"; ?></td>
                                    <td><?php echo $File['filename']; ?></td>
                                    <td><?php echo $File['pathTypeFile']; ?></td>


                                    
                                    <td><?php echo $File['Product_Type']; ?></td>


                                    <td><span style="color: #999999; font-size: 9px;"><?php echo $File['created']; ?></span></td>
                                    <th style="width: 150px;">
                                        <?php
                                        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_download')) {
                                            ?>
                                            <a href="<?php echo Yii::app()->request->baseUrl . $File['path']; ?>" target="_black" class="bottomLeft "><i class="btn btn-small btn-info glyphicon glyphicon-arrow-down"></i></a>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {
                                            ?>
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/filesmanage/files/update/id/<?php echo $File['fileid']; ?>" class="bottomLeft "><i class="btn btn-small btn-info glyphicon glyphicon glyphicon-pencil "></i></a>
                                            <?php
                                        }
                                        ?>
                                    </th>
                                    <?php
                                    if (Yii::app()->authRBAC->checkAccess($this->modulo . '_estatus')) {
                                        ?>
                                        <?php
                                        if ($File['fileactive'] == 1) {
                                            ?>
                                        <input type="hidden" name="<?php echo $File['fileid']; ?>-h" class="_<?php echo $File['fileid']; ?>" id="<?php echo Yii::app()->controller->createUrl('updateActive') ?>" >
                                        <td class="switchactive">
                                            <input name="<?php echo $File['fileid']; ?>" class="onoffswitch-checkbox checkbox-active" id="<?php echo $File['fileid']; ?>" checked type="checkbox" >
                                            <label class="onoffswitch-label" for="<?php echo $File['fileid']; ?>">
                                                <span class="onoffswitch-inner">
                                                    <span class="onoffswitch-active"><span class="onoffswitch-switch">Si</span></span>
                                                    <span class="onoffswitch-inactive"><span class="onoffswitch-switch">NO</span></span>
                                                </span>
                                            </label>
                                        </td>
                                        <?php
                                    } else {
                                        ?>
                                        <input type="hidden" name="<?php echo $File['fileid']; ?>-h" class="_<?php echo $File['fileid']; ?>" id="<?php echo Yii::app()->controller->createUrl('updateActive') ?>" >
                                        <td class="switchactive">
                                            <input name="<?php echo $File['fileid']; ?>" class="onoffswitch-checkbox checkbox-active" id="<?php echo $File['fileid']; ?>"  type="checkbox" >
                                            <label class="onoffswitch-label" for="<?php echo $File['fileid']; ?>">
                                                <span class="onoffswitch-inner">
                                                    <span class="onoffswitch-active">
                                                        <span class="onoffswitch-switch">Si</span>
                                                    </span>
                                                    <span class="onoffswitch-inactive">
                                                        <span class="onoffswitch-switch">NO</span>
                                                    </span>

                                                </span>
                                            </label>
                                        </td>
                                    <?php } ?>
                                    <?php
                                }
                                ?>
                                </tr>
                                <?php $total = $total + 1; ?>
                            <?php endforeach; ?>
                        <?php } ?>
                    </table>
                </div>
                <div id="search_error" style="display: none;" class=" alert alert-danger text-center"><?php echo Yii::t('forms', 'No se encontraron resultados para su búsqueda'); ?></div>
                <div id="select_filter" class=" alert alert-success" style="background-color:"><?php echo Yii::t('forms', 'Seleccione un filtro'); ?></div>
            </div><!--final del panel-->

        </div>
    </div>
</div>