<?php
/* @var $this FilesController */
/* @var $model Files */
$this->pageTitle = Yii::app()->name;
$active = array(
    '0' => 'Inactivos',
    '1' => 'Activos',
);

foreach ($active as $key => $value) {
    $listData[$key] = $value;
}
?>
<div style="padding-left: 20px;">
    <h2 style="text-align: center;"><?php echo Yii::t('pages', 'Files'); ?></h2>
    <hr style="width: 30%;"/>
    <p>
<?php echo Yii::t('pages', 'You may optionally enter a comparison operator'); ?> (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
<?php echo Yii::t('pages', 'or'); ?> <b>=</b>) 
<?php echo Yii::t('pages', 'at the beginning of each of your search values to specify how the comparison should be done.'); ?>
    </p>
</div>
<div style="padding: 5px; ">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'files-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
        
    'columns' => array(
          'id',
        'category',
        array(
            'name' => 'active',
            'type' => 'raw',
            'value' => 'Files::visible($data->active,$data->id);',
            'htmlOptions' => array('style' => 'width: 100px; height: auto; text-align: center;'),
            'filter' => $listData
        ),
        
        'name',
        array(
            'name' => 'path',
            'type' => 'html',
            'value' => 'CHtml::link(end(explode("/",$data->path)),Yii::app()->params["pageUrl"].Yii::app()->params["folder"].$data->path);'
        ),
        array(
            'name' => 'language',
            'value' => '$data->language0->name',
            'type' => 'text',
            'filter' => CHtml::listData(Languages::model()->findAll(), "code", "name")
        ),
        'version',
        //		'pathTypeFile',
        array(
            'class' => 'CButtonColumn',
            'header' => Yii::t('modules', "Actions"),
            'template' => '{update}{delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => Yii::app()->request->baseUrl . '/images/icn_edit_article.png',
                    'visible' => 'Yii::app()->authManager->checkAccess("editarArchivos", Yii::app()->user->getState("rolId"))'
                ),
                'delete' => array(
                    'imageUrl' => Yii::app()->request->baseUrl . '/images/icn_trash.png',
                    'visible' => 'Yii::app()->authManager->checkAccess("eliminarArchivos", Yii::app()->user->getState("rolId"))'
                ),
            )
        ),
    ),
));
?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        altura();
        $('.make-switch').on('switch-change', function(e, data) {
            div = $(this);
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->request->baseUrl; ?>/files/updateActive",
                cache: false,
                data: {active: data.value, id: $(this).attr("id")}
            }).done(function(response) {
                if (response !== '')
                {
                    alert(response);
                    div.bootstrapSwitch('setState', false, true);

                }
            });
        });
    });
    //CUANDO SE EJECUTA UNA FUNCION AJAX POR EL CGRIDVIEW EL ANTERIOR SE PIERDE POR ESO SE HACE LA FUNCION DE ABAJO
    $(document).ajaxComplete(function(event, xhr, settings) {
        if (settings.type === "GET")
        {
            altura();
            $('.make-switch').on('switch-change', function(e, data) {
                div = $(this);
                $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/files/updateActive",
                    cache: false,
                    data: {active: data.value, id: $(this).attr("id")}
                }).done(function(response) {
                    if (response !== '')
                    {
                        alert(response);
                        div.bootstrapSwitch('setState', false, true);

                    }
                });
            });
        }
    });
</script>