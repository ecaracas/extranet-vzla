<?php
/* @var $this FilesController */
/* @var $model Files */
$this->pageTitle=Yii::app()->name;
?>
<div style="padding-left: 20px;">
    <h2 style="text-align: center;"><?php echo Yii::t('titles', 'Update File'); ?>:  <b><?php echo $model->name; ?></b></h2>
    <hr style="width: 30%;"/>
    
    <?php echo $this->renderPartial('_form', array('model'=>$model, 'type_file_rol'=>$type_file_rol)); ?>
</div>