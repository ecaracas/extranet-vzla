<?php
/* @var $this FilesController */
/* @var $model Files */
/* @var $form CActiveForm */
?>
<div class="form" style="padding-left: 150px;">
<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'files-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data')
	));?>
	
	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
	<?php echo $form->labelEx($model,'path'); ?>
	<?php //echo $form->fileField($model,'path'); ?>
	<input type="file" name="uploaded_file" id="uploaded-file">
<?php
	echo $form->error($model, 'path');
?>
	</div>
	<br />
	




	<div class="row">
		<label class="required" for="Files_product"><?php echo Yii::t('pages', 'Tipo de arhivos'); ?>
			<span class="required">*</span>
		</label>
		<select id="type" name="Files[file_type]" class="form-control" style="width: 50%;">
			<option value="">SELECCIONAR UN TIPO DE ARCHIVOS</option>
			<?php
				foreach($type_file_rol as $id => $name)
				{
					$selected = ($id == $model->pathTypeFile) ? 'selected="selected"' : '';
					echo '<option value="' . $id . '" ' . $selected . '>' . $name . '</option>';
				}
			?>
		</select>
	</div>
	

	<br />
	



	<div class="row" style="width: 50%">
		<?php echo $form->labelEx($model, Yii::t('utilities', 'brands_models')); ?>
		<?php
		// var_dump($model);
		echo $form->dropDownlist($model, 'brands_models', CHtml::listData(ProductsTypes::model()->findAll(), 'id', 'name'), array(
				'class' => 'form-control',
				'ajax' => array(
						'type' => 'POST',
						'url' => CController::createUrl('Files/Selectproduct'),
						'update' => '#' . CHtml::activeId($model, 'productos'),
						
						'beforeSend' => 'function(){
							$("#Registro_id_level_city").find("option").remove();
							}'),
				'prompt' => 'SELECCIONAR UNA MARCA ')
		)?>
		<?php echo $form->error($model,'id_level_country'); ?>
	</div>


	<br />
	


	<div class="row" style="width: 50%">
	<?php
		$listasCity = array();
	?>
	<?php echo $form->labelEx($model, Yii::t('utilities', 'productos')); ?>
	<?php echo $form->dropDownlist($model,'productos',$listasCity,array("class" => 'form-control','prompt'=>'SELECCIONAR UN PRODUCTO') ); ?>
	<?php echo $form->error($model,'productos'); ?>
	</div>


	<!--        <div class="row">
		<label class="required" for="Files_product"><?php echo Yii::t('pages', 'Modelos'); ?><span class="required">*</span></label>
			<select id="products" name="Files[products]" class="form-control" style="width: 50%;">
				<option>SELECCIONAR UN MODELO </option>
	<?php
// $types = CHtml::listData(Products::model()->findAll(), 'id', 'name');
		      // foreach ($types as $id=>$name){
		      // $selected=($id==$model->pathTypeFile)?'selected="selected"':'';
		      // echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';
		      // }
	?>
	</select>
	</div>-->
	<br />
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?> 
		<?php echo $form->textField($model,'name',array('class'=>'form-control','style'=>'width: 50%;')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<br />
	<div class="row">
	
		<?php echo $form->labelEx($model,'version'); ?>
		<?php echo $form->textField($model,'version',array('class'=>'form-control','style'=>'width: 50%;')); ?>
		<?php echo $form->error($model,'version'); ?>
	</div>
	<div class="row">
	<br/>
		<div class="col-md-3">
			<?php echo $form->labelEx($model,'publicSitePa', array('style' => 'margin-top:7%')); ?>
		</div>
		<div class="col-md-1">
			<?php echo $form->checkbox($model,'publicSitePa',array('class'=>'form-control', 'style' => 'width:50%')); ?>
			<?php echo $form->error($model,'publicSitePa'); ?>
		</div>
	</div>
	<br /> <br /> <br />
	<div class="btn-group">
		<?php echo CHtml::Button(Yii::t('pages','Regresar'),array('class'=>'btn btn-primary','onclick'=>'history.back(-1)')); ?>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn - btn-primary')); ?>
	</div>
	
<?php $this->endWidget(); ?>
</div>
<!-- form -->