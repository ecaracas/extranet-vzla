<!--     <?php
/* @var $this FilesController */
/* @var $model Files */
/* @var $form CActiveForm */
?>
<div class="form" style=" background-color: #F6f6f6; padding-left: 150px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'files-form',
	'enableAjaxValidation'=>true,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
          'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>
        
        
        <div class="row">
		<?php echo $form->labelEx($model,'path'); ?>
		<?php //echo $form->fileField($model,'path'); ?>
            <input type="file" name="uploaded_file" id="uploaded-file">
		<?php echo $form->error($model,'path'); 
                ?>
	</div>
              <br/>
        <div class="row">
		<label class="required" for="Files_product"><?php echo Yii::t('pages', 'Tipo de arhivos'); ?><span class="required">*</span></label>
                <select id="type" name="Files[type]" class="form-control" style="width: 50%;">
				 <option>SELECCIONAR UN TIPO DE ARCHIVOS</option>
       
                                 
                    <?php 
                        
                    
                        foreach ($type_file_rol as $id=>$name){  
                            
                           $selected=($id==$model->pathTypeFile)?'selected="selected"':'';
                            echo '<option value="'.$id.'" '.$selected.'>'.$name.'</option>';

                            }
                            
                            
                     
                   ?>
                </select>
	</div>
        <br/>
        
        <div class="row" style="width: 50%">
		<?php echo $form->labelEx($model, Yii::t('utilities', 'MARCA')); ?>
		<?php echo $form->dropDownlist($model,'id_level_country', 
                        CHtml::listData(LevelCountry::model()->findAll(),'id_level_country','name'),
                          array(
                            'class' => 'form-control',
                            'ajax'=>array(
                              'type'=>'POST',
                              'url'=>CController::createUrl('Files/Selectcity'),
                              'update'=>'#'.CHtml::activeId($model,'id_level_city'),
                              'beforeSend' => 'function(){
                               $("#Registro_id_level_city").find("option").remove();
                               }',  
                               ),'prompt'=>'Selections / Selecciones'
                                )      
                        )
                        ?>
		<?php echo $form->error($model,'id_level_country'); ?>
                    </div>
                    <br/>
                    <div class="row">
                      <?php
                    if ($a =='USA') {
                        $lista = LevelCity::model()->findAll('id_level_country = :id_level_country', array(':id_level_country' => $a));
                        $listasCity = CHtml::listData($lista, 'id_level_city', 'name_city');
                        } else {
                        $listasCity = array();
                        }
                    ?>    
                     
                    <?php
                        $listasCity = array();
                        
                    ?> 
                    <?php echo $form->labelEx($model, Yii::t('utilities', 'PRODUCTO')); ?>
                    <?php echo $form->dropDownlist($model,'id_level_city',$listasCity,array("class" => 'form-control','prompt'=>'Seleccione') ); ?>
                    <?php echo $form->error($model,'id_level_city'); ?>
                </div>
         
            <br/>
   
        
            <br/>
        <div class="row">
		<?php echo $form->labelEx($model,'name'); ?> 
		<?php echo $form->textField($model,'name',array('class'=>'form-control','style'=>'width: 50%;')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
            <br/>
	<div class="row">
		<?php echo $form->labelEx($model,'version'); ?>
		<?php echo $form->textField($model,'version',array('class'=>'form-control','style'=>'width: 50%;')); ?>
		<?php echo $form->error($model,'version'); ?>
	</div>
              
          <br/>

            <br/>
	 <br />
	<div class="btn-group">
                <?php echo CHtml::Button(Yii::t('pages','Go Back'),array('class'=>'btn btn-primary','onclick'=>'history.back(-1)')); ?>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn - btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form --> -->