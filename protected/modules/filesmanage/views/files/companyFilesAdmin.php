
<style>
    .divlang{
        display: none;
        margin-top: 40px;
    }
</style>
<script type="text/javascript">
    $(function(){
        $('#country_select').change(function (){
            $('.divlang').hide(500);
          $('#div'+$(this).val()).show(500);  
        })
        $('#flash_message').delay(3000).hide(500);
    })
    
</script>

<div style="padding-left: 20px;">
<h2 style="text-align: center;">Company Files</h2>
<hr style="width: 30%;">
</div>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
   
        echo '<div style="padding-left:200px;" id="flash_message"><div class="alert alert-'.$key.' col-md-9 col-lg-9">' . $message . "</div></div>\n";
    }
?>
<?php
$files_name=array('is'=>'LED LIGHTING FOR INDUSTRIAL USE','cs'=>'LED LIGHTING FOR COMMERCIAL USE','cp'=>'COMPANY PROFILE');

 $lang = CHtml::listData(Languages::model()->findAll(), 'code', 'name'); 
 ?>
<div class="text-center">
    <select style="width: 50%; font-size: 16px; text-align: center;" id="country_select">
    <option value="0">Select language</option>
 <?php 
  foreach($lang as $code=>$lang)
    {
     echo '<option value="'.$code.'">'.$lang.'</option>';
    }
?>
</select>
</div>
<?php 
$lang = CHtml::listData(Languages::model()->findAll(), 'code', 'name'); 
 foreach($lang as $code=>$lang)
    {
     ?>

<div id="div<?php echo $code; ?>" class="divlang">
      <?php foreach($files_name as $key=>$name){
 ?>
<div class="col-lg-4 col-md-4 " >
    <form action="createcompany" method="post" enctype="multipart/form-data">
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title" style="height: 30px; font-size: 12px; font-weight: bold;"><?php echo $name; ?></h3>
      </div>
    
    <div class="panel-body" style="height: 150px;">
      <b>File:</b> 
       <?php 
       $i=0;
        foreach ($files as $file){
            if($code==$file->language && $name==$file->name){
                echo '<a target="blank_" href="'.Yii::app()->params['folder'].'/downloads/company_files/'.end(explode('/',$file->path)).'">'.end(explode('/',$file->path)).'</a>'; 
                $i++;
            }
       }
    
        if ($i==0){
           echo 'None';
        }
               ?>
      
      <input type="hidden" name="filename" value="<?php echo $key;   ?>" >
          <input type="hidden" name="language" value="<?php echo $code;   ?>" >
          <input type="file" name="file" style="width: 270px; margin-top: 40px;">
  </div>
  <div class="panel-footer text-center">
      
          
      <input type="submit" class="btn btn-primary" value="submit">
      
      
      
  </div>
</div>
    </form>
    </div>
      <?php } ?>
      </div>
      
     <?php } ?>
