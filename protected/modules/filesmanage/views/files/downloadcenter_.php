<style>
    .btn{
        padding: 2px;

    }
    .glyphicon{
        font-size: 15px;
    }
    .foldericon{
        margin: 10px;
        color:#ffc300;
    }

    .file{
        margin: 10px;
        color:#484848;
    }

    .cursor{
        cursor: pointer;
    }
    .afterfolder{
        margin-left: 45px;

    }

    #foldercontainer{
        padding:50px; 
        padding-top: 10px;
    }
    .upload{
        display: none;

    }

    .foldermenu{
        float: right;
        margin-top: 20px;
        margin-right: 100px;
        display: none;

    }
    .filemenu{
        float: right;
        margin-top: 10px;
        margin-right: 100px;
        display: none;

    }
    .folder{
        display: block;
    }
    .file{
        padding: 5px;
    }

    .archivo{
        float:left;
    }

    .uploaddiv{
        background-color: #f1cb43;
    }

    .directorydiv{
        display: none;

    }
</style>

<script>
    start = 0;
    contador=1;
    base = 'download-center';
    $(function() { 

        $('body').on('click', '.directorydiv, .uploaddiv, deletedir, .submitnewd, .adddirectory, .uploadbtn, .deletefile', function(event) {
            event.stopPropagation();
        });
        if (start == 0) {
            alert('entrando');
            loadfolder(base, varvalues = '', first = true);
        }
        $('body').on('click', '.deletedir', function(event) {

            i = $(this);
            div=i.attr('iddiv');
            if (confirm("are you sure that you want to permanently delete this folder, and all the files and subfolders inside of it ?")) {


                $.post("<?php echo Yii::app()->controller->createUrl('dcdeletedirectory'); ?>", {folder: $(this).attr('dir')}, function(data) {

                }).done(function() {
                    
                    $("#" + div).hide('1000')

                }).fail(function() {

                    alert('unexpected mistake ');
                })
            }
        })
        
        
        
        $('body').on('click', '.submitnewd', function(event) {

            i = $(this);
           
            $.post($('#form' + $(this).attr('md5')).attr('action'), $('#form' + $(this).attr('md5')).serialize(), function(data) {

            }).done(function() {
                $('#' + i.attr('md5')).attr('click', 'no').attr('is', 'close').trigger('click');

                $('.directorydiv').hide();
                $('.inputnewdir').val('');

            })

        })
        
        
         $('body').on('click', '.adddirectory', function(event) {

            $('.formopen').hide();
            $('#directory' + $(this).attr('iddirecotry')).show(500)
            $('#dhidden' + $(this).attr('iddirecotry')).show(500).val($(this).attr('folder'))

        })
        
             $('body').on('click', '.uploadbtn', function(event) {

            // alert('#'+$(this).attr('uploaddiv'));
            $('.formopen').hide();
            $('#' + $(this).attr('uploaddiv')).toggle(1000);
            $('#' + $(this).attr('folderinput')).val($(this).attr('folder'));


        })
        
        $('body').on('click', '.deletefile', function(event) {

        div=$(this).attr('iddiv');
            if (confirm("Are you sure you want to  delete this item ?")) {

                //partialfolder
                //
                $.post("<?php echo Yii::app()->controller->createUrl('dcdeletefile'); ?>", {partial: $(this).attr('partialfolder')}).done(function() {
                    $('#' + div).slideUp(1000);
                }).fail(function() {
                    alert("Unexpected Mistake");
                })
            }
        })
        
        

         function loadfolder(object){
          
          //Switch
          varvalues = "";
          if(contador==1){
          first=true;
          contador++;
          }
           
           if (first == true) {
                first=false
               alert('entrando 2');
                $.post("<?php echo Yii::app()->controller->createUrl('returnfiles'); ?>?folder=&only=true&base=" + base, function(data) {
                    $("#foldercontainer").html(data);
                first=false
                //loadfolder($(this));

                 alert('entrando 3');
               
                
                })
                
                }else {
   
                alert('entrando 3');
                $('.filemenu').hide();
                $('.uploaddiv').hide();
                $('.file').attr('click', 'no');
                $('.file').css('background', 'none');
                i = object;
                
                
                folder = i.attr('folder'); 
                
                folderdir = i.attr('folderdir');
                menu = i.attr('menu');
                

                /*abrir y cerrar carpetas*/
                if ($("#foldericon" + folder).attr('is') == 'close') {
                    $("#foldericon" + folder).removeClass('glyphicon-folder-close');
                    $("#foldericon" + folder).addClass('glyphicon-folder-open');
                    $("#foldericon" + folder).attr('is', 'open')
                    i.css('background', "#eaeaea");

                } else {
                    if (i.attr('click') != 'no') {
                        $("#foldericon" + folder).removeClass('glyphicon-folder-open');
                        $("#foldericon" + folder).addClass('glyphicon-folder-close');
                        $("#foldericon" + folder).attr('is', 'close')
                        i.css('background', "none");
                    } else {

                    }
                }
                /*abrir y cerrar carpetas*/


                /*si la carpeta esta cerrada*/
                if (i.attr('is') == 'close') {
                    $("#after" + folder).empty();
                    $("#upload" + folder).show();
                    $.post("<?php echo Yii::app()->controller->createUrl('returnfiles'); ?>?folder=" + folderdir, function(data) {

                        $("#after" + folder).html(data);
                    })//post
                    i.attr('is', 'open')
                } else {

                    /*si la carpeta esta abierta*/

                    if (i.attr('click') != 'no') {
                        $("#after" + folder).empty();
                        $("#upload" + folder).hide();
                        i.attr('is', 'close')
                    } else {
                        $('#' + menu).show();
                    }
                }

                /*carpetas fin*/
                $('.foldermenu').hide();
                click = i.attr('click');
                $('.foldermenu').attr('click', 'no');
                $('.folder').attr('click', 'no');

                if (click == "no") {

                    i.attr('click', 'yes')

                    $('#' + menu).show();
                } else {

                    i.attr('click', 'no');
                    $('#' + menu).hide();
                }
            }
        }
        $('body').on('click', '.folder', function() {
            loadfolder($(this));

        })

             $('body').on('click', '.file', function() {

            $('.foldermenu').hide();
            $('.folder').attr('click', 'no');
            i = $(this);
            click = $(this).attr('click');
            menu = $(this).attr('menu');
            $('.filemenu').hide();
            $('.file').css('background', '#FFF');
            if (click == "no") {

                i.css('background', '#fefaad');
                $(this).attr('click', 'yes')
                $('#' + menu).show();
            } else {

                i.css('background', '#FFF');
                $(this).attr('click', 'no');
                $('#' + menu).hide();
            }
            
        })
                
    })

</script>
<div id="foldercontainer" class="popup-gallery">


</div>

<div id='ejemplo'></div>
<a href="http://www.artviper.net/test/artvipermedia.flv" class="bx2" rel="800-452" title="Playing now...">FLV Video  &raquo;</a><br />
          <a href="http://www.artviper.net/index.mp3" class="bx2" rel="400-93">MP3 Audio &raquo;</a> <br />
          <a href="http://www.artviper.net/pc.swf" class="bx2" rel="480-480">SWF Video &raquo;</a><br />
          <a href="http://www.artviper.net/test.pdf" class="bx2" rel="800-350">PDF File &raquo;</a><br />
          <a href="http://www.artviper.net/website-tools/abc.jpg" class="bx2" rel="357-500">Image File &raquo;</a><br />
          <a href="http://www.artviper.net" class="bx2" rel="1040-600">HTML File &raquo;</a><br />
          <a href="^inlineHTML" class="bx2" rel="450-250">Inline HTML &raquo;</a><br />
          <a href="http://www.youtube.com/embed/XpF32DRBPh0" class="bx2" rel="640-390">Youtube Video &raquo;</a><br />
          <a href="http://player.vimeo.com/video/12939579" class="bx2" rel="400-300">Vimeo Video &raquo;</a>
<div id="inlineHTML">
</div>

<script type="text/javascript">



    
    
    
//names,inSpeed,outSpeed,boxColor,backColor,bgOpacity,bRadius,borderWeight,borderColor,boxShadowSize,boxShadowColor,iconSet,effectsIn,effectsOut
doBump('.gallery',400,450,'000','35435a',0.5,7,2,'444',15,'000',4);
doBump('.bump');
doBump('.bx',1200, 500, '34373c', '34373c', 0.7, 3, 1 ,'333', 9,'000', 1, Fx.Transitions.Bounce.easeOut, Fx.Transitions.Bounce.easeOut);
doBump('.bx2',850, 500, '000', '6b7477', 0.7, 7, 2 ,'333', 15,'000', 2, Fx.Transitions.Back.easeOut, Fx.Transitions.linear);
doBump('.bx3',500,250,'474747','575338',0.8,14,1,'232323',10,'111',3);
doBump(names='.bx4',500,250,'181818','444',0.4,15,1,'111',12,'000',4,Fx.Transitions.Back.easeOut, Fx.Transitions.linear,'images/demo_bg_bumpbox.png','top left','repeat-x');
</script>

