<?php
class DefaultController extends Controller
{
	public function accessRules()
	{

		return array(
				array(
						'allow', /* Acciones Permitidas*/
				'actions' => array(
								'index',
								'view',
								'create',
								'update',
								'delete' ),
						'users' => array(
								'*' ) ),
				array(
						'deny',
						'users' => array(
								'*' ) ) );
	
	}
	public function __construct($id, $module = null)
	{

		parent::__construct($id, $module);
		
		Yii::app()->params['title'] = 'Manejador de Archivos';
	
	}
	public function actionIndex()
	{

		$model = new filesDocument('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Files'])) $model->attributes = $_GET['Files'];
		
		$this->render('index', array(
				'model' => $model ));
		
		// $this->render('index');
	}
	public function actionDevelopers()
	{
		$model = new filesDocument('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Files'])) $model->attributes = $_GET['Files'];
		$this->render('index_developers', array('model' => $model ));
	}
	public function actionUtilitario()
	{

		$model = new filesDocument('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Files'])) $model->attributes = $_GET['Files'];
		
		$this->renderPartial('utilitario', array(
				'model' => $model ));
		
		// $this->render('index');
	}
	public function actionUtilitario_container()
	{

		$model = new filesDocument('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Files'])) $model->attributes = $_GET['Files'];
		
		$this->render('utilitario_container', array(
				'model' => $model ));
		
		// $this->render('index');
	}
	public function actionDescargas()
	{

		if(isset($_FILES["file"]))
		{
			
			$file = $_FILES["file"];
			$filedir = getcwd() . "/downloads/" . $file['name'];
			move_uploaded_file($file["tmp_name"], $filedir);
			$filedb = new Files();
			$desc = (!isset($_POST['description']) || $_POST['description'] == '') ? '&nbsp; ' : $_POST['description'];
			$filedb->estatus = '1';
			$filedb->description = $desc;
			$filedb->name = strtoupper($file['name']);
			$filedb->type = $_POST['type_a'];
			
			$filedb->size = $_FILES["file"]["size"] . " Bytes";
			$filedb->path = "/downloads/" . $file['name'];
			
			$filedb->version = $_POST['version'];
			$filedb->date_publishing = Date('H:i:s') . " , " . Date('Y-m-d');
			;
			
			$filedb->language = 'es';
			$filedb->pathTypeFile = 'images/pdf-download.jpg';
			$filedb->save();
		}
		$this->render('descargas');
	
	}
	public function actiondelete($id)
	{

		$this->loadModel($id)->delete();
		$model = filesDocument::model()->findByPk($id);
		$model = new filesDocument('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Files'])) $model->attributes = $_GET['Files'];
		
		$this->render('index', array(
				'model' => $model ));
	
	}
	public function loadModel($id)
	{

		$model = filesDocument::model()->findByPk($id);
		// if ($model === null)
		// throw new CHttpException(404, 'La página solicitada no existe.');
		return $model;
	
	}
	public function actionCreate()
	{

		$this->render('descargas');
	
	}

}