<?php

/* Importo Modelos */
Yii::import('application.modules.roles.models.RBACRoles');
Yii::import('application.modules.roles.models.RBACUsuarios');

class FilesController extends Controller {
    /**
     *
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     *      using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    // public $layout = '//layouts/column2';

    /**
     *
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete') // we only allow deletion via POST request
        ;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * 
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'create',
                    'update',
                    'dcdeletedirectory',
                    'dcnewdirectory',
                    'admin',
                    'dcupload',
                    'delete',
                    'dcDeleteFile',
                    'CompanyFiles',
                    'createcompany',
                    'updateActive',
                    'DownloadCenter',
                    'returnfiles',
                    'Selectproduct'),
                'users' => array(
                    '@')),
            array(
                'deny', // deny all users
                'users' => array(
                    '*')));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        // if (Yii::app()->authManager->checkAccess('crearArchivos', Yii::app()->user->getState("rolId"))) {
        $model = new Files();

        if (isset($_POST['Files'])) {
            //var_dump($_POST['Files']); die();
            if ($_POST['Files']['name'] != '' && $_POST['Files']['version'] != '') {
                $model->active = 1; // ASIGNAR AL MODELO
                $model->name = $_POST['Files']['name'];
                $model->version = $_POST['Files']['version'];
                $model->language = "es";
                $model->typefile = $_FILES['uploaded_file']['type'];
                $model->pathTypeFile = $_POST['Files']["file_type"];
                $model->publicSitePa = $_POST['Files']["publicSitePa"];
                $model->created = Date('H:i:s') . " , " . Date('Y-m-d');
                $model->category = 15;
                $path = $_FILES["uploaded_file"]['name'];
                if ($_POST['Files']["file_type"] != '' && $_POST['Files']["brands_models"] != '' && $_POST['Files']["productos"] != '') {
                    if (!file_exists(Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . 'downloads/' . $path)) {
                        $model->path = '/downloads/' . $path;
                        move_uploaded_file($_FILES["uploaded_file"]['tmp_name'], Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . 'downloads/' . $path);
                        if ($model->validate()) {
//                                                    Se comenta motivado a que no se encontro un formulario que se llame webpublico
//							if(isset($_POST['webpublico']))
//								$model->publicSitePa = 1;
//							else
//								$model->publicSitePa = 0;

                            $model->save();
                            $productFile = new ProductFiles();
                            $productFile->file = $model->id;
                            $productFile->product = $_POST['Files']["productos"];
                            $productFile->file_type = $_POST['Files']["file_type"];
                            $productFile->save();
                            $ProductsProductsType = new ProductsProductsType();
                            $ProductsProductsType->product = $_POST['Files']["productos"];
                            $ProductsProductsType->product_type = $_POST['Files']["brands_models"];
                            $ProductsProductsType->save();
                            $ocultarFiles = false;
                            unset($_POST['Files']);
                            // $this->render('../Downloads/index');
                            $this->redirect('../Downloads/index');
                        }
                    } else {
                        $model->addError('path', '¡ Este archivo ya está guardado !');
                    }
                } else {

                    $model->addError('path', '¡ TODO LOS CAMPOS CON * SON REQUERIDOS !');
                }
            }
        }

        $criteria = new CDbCriteria();
        $criteria->select = 'id, name';
        $types = CHtml::listData(FilesTypes::model()->findAll($criteria), 'id', 'name');

        $criteria_rol = new CDbCriteria();
        $criteria_rol->select = 'rol_id';
        $criteria_rol->condition = 'id=:idrol';
        $criteria_rol->params = array(
            ':idrol' => Yii::app()->user->id);
        // var_dump(RBACUsuarios::model()->find($criteria_rol)->rol_id ); exit();
        $user_rol = RBACUsuarios::model()->find($criteria_rol)->rol_id;

        foreach ($types as $id => $name) {

            $criteria_rol_typeFile = new CDbCriteria();
            $criteria_rol_typeFile->select = 'id_rol';
            $criteria_rol_typeFile->condition = 'id_filetype=:idtypefile && id_rol=:idrol';
            $criteria_rol_typeFile->params = array(
                ':idtypefile' => $id,
                ':idrol' => $user_rol);
            $rol_type = RolesTypeFiles::model()->find($criteria_rol_typeFile);

            if (!is_null($rol_type)) {
                if ($user_rol == $rol_type->id_rol) {
                    $type_file_rol[$id] = $name;
                }
            }
        }
        unset($_POST['Files']);
        $types = FilesTypes::model()->findAll();

        $ocultarFiles = false;
        $this->render('create', array(
            'model' => $model,
            'ocultarFiles' => $ocultarFiles,
            'type_file_rol' => $type_file_rol));
        // }
    }

    public function actionDownloadCenter() {
        $this->render('downloadcenter');
    }

    // dc ---> download Center
    public function actiondcDeleteFile() {
        echo $dir = Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . $_POST['partial'];

        unlink($dir);
    }

    public function actiondcnewdirectory() {
        $folder = $_POST["folder"];
        $name = $_POST["dirname"];
        $dir = Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . '/downloads/';

        $fulldir = $dir . $folder . '/';

        mkdir($fulldir . $name, 0777);
    }

    public function actiondcdeletedirectory() {
        $folder = $_POST["folder"];

        $dir = Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . '/downloads/';

        $fulldir = $dir . $folder;

        rmdir($fulldir);
    }

    public function actiondcupload() {
        $folder = $_POST["folder"];

        $dir = Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . '/downloads/';
        // var_dump($dir .$folder.$_FILES["file_upload"]['name']);
        $fulldir = $dir . $folder . '/';
        move_uploaded_file($_FILES["file_upload"]['tmp_name'], $fulldir . $_FILES["file_upload"]['name']);

        Yii::app()->request->redirect(Yii::app()->controller->createUrl('downloadcenter'));
    }

    public function actionreturnfiles($folder = '', $only = false, $base = '') {
        if (Yii::app()->authManager->checkAccess("dviewfile", Yii::app()->user->getState("rolId"))) {
            $cantidad = 0;
            $carpeta = Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . '/downloads/' . $folder;
            // $carpeta = '../' . Yii::app()->params['folder'] . '/downloads/' . $folder;
            $relativefolder = Yii::app()->params['folder'] . '/downloads/' . $folder;
            $partialfolder = '/downloads/' . $folder;
            if (is_dir($carpeta)) {
                if ($dir = opendir($carpeta)) {
                    $cantidad = 0;
                    while (($archivo = readdir($dir)) !== false) {
                        $directorio = FALSE;
                        $explode = explode('.', $archivo);
                        $ext = end($explode);
                        if (count($explode) <= 1)
                            $directorio = true;
                        if (!is_dir($archivo)) {

                            if (($archivo != '.' && $archivo != '..' && $only == false) || ($archivo != '.' && $archivo != '..' && ($only == 'true' && $archivo == $base))) {

                                if ($directorio == true) {
                                    $class = 'glyphicon glyphicon-folder-close foldericon ';
                                } else {
                                    if ($ext == 'jpg' || $ext == 'png' || $ext == 'gif') {
                                        $class = "glyphicon glyphicon-picture fileicon ";
                                    } elseif ($ext == 'pdf' || $ext == 'doc' || $ext == 'ppt' || $ext == 'pptx' || $ext == 'docx' || $ext == 'xls' || $ext == 'xlsx') {
                                        $class = "glyphicon glyphicon-book fileicon ";
                                    } else {
                                        $class = "glyphicon glyphicon-file fileicon ";
                                    }
                                }
                                if ($directorio == true) {
                                    $md5 = md5($folder . "/" . $archivo);
                                    if ($folder == '') {
                                        $folderdir = $archivo;
                                    } else {
                                        $folderdir = $folder . "/" . $archivo;
                                    }

                                    echo "<div id='container" . $md5 . "'><div class='text-left cursor folder' id='" . $md5 . "' click='no' menu='menu" . $md5 . "' folderdir='" . $folderdir . "' folder='" . $archivo . "' is='close'><span class=' " . $class . "' id='foldericon" . $archivo . "' is='close' style='font-size:35px;'></span> &nbsp;<span >" . ucwords($archivo) . "</span>  
                                   <div class='foldermenu text-left' id='menu" . $md5 . "' ><span style='float:left; margin-right:20px;' class='uploaddiv formopen' id='upload" . $md5 . "'><form method='post' enctype='multipart/form-data'  action='" . Yii::app()->controller->createUrl('dcupload') . "'><input type='hidden' name='folder' id='folder" . $md5 . "'> <input type='file' name='file_upload' class='archivo'> <input type='submit' class='btn btn-default' value='Submit' > </form></span>   <span style='float:left; margin-right:20px;' class='directorydiv formopen' id='directory" . $md5 . "'><form method='post' id='form" . $md5 . "' action='" . Yii::app()->controller->createUrl('dcnewdirectory') . "'> <span style='float:left'>Name:  <input type='text' name='dirname' class='inputnewdir'/><input type='hidden' id='dhidden" . $md5 . "' name='folder'><span  class='submitnewd btn btn-default' id='submit" . $md5 . "' md5='" . $md5 . "'>Submit</span></span></form></span>";
                                    if (Yii::app()->authManager->checkAccess("duploadfile", Yii::app()->user->getState("rolId"))) {
                                        echo " <span class='btn btn-success uploadbtn' uploaddiv='upload" . $md5 . "' folderinput='folder" . $md5 . "' folder='" . $folderdir . "' ><span class='glyphicon glyphicon-open'></span></span> ";
                                    }
                                    if (Yii::app()->authManager->checkAccess("dcreatefile", Yii::app()->user->getState("rolId"))) {
                                        echo " <span class='btn btn-primary adddirectory' folder='" . $folderdir . "' iddirecotry='" . $md5 . "'><span class='glyphicon glyphicon-folder-close'></span></span> ";
                                    }
                                    if (Yii::app()->authManager->checkAccess("ddeletefile", Yii::app()->user->getState("rolId")) && $base == '') {
                                        echo " <span class='btn btn-danger deletedir' iddiv='" . $md5 . "' dir='" . $folderdir . "'><span class='glyphicon glyphicon-trash'></span></span>  ";
                                    }

                                    echo "</div></div><div id='after" . $archivo . "' class='afterfolder'></div></div>";
                                } else {
                                    $rand = rand(5, 15);
                                    $md5 = md5($rand . $archivo);
                                    echo "<div id='" . $md5 . "' class='text-left file cursor' menu='menu" . $md5 . "' click='no'><span class='cursor " . $class . "' style='font-size:35px;'></span> &nbsp;<span > <a href='" . $relativefolder . "/" . $archivo . "' style='color:#000;' target='_blank'>" . ucwords($archivo) . "</a></span>" . "<div class='filemenu' id='menu" . $md5 . "'> <a href='" . $relativefolder . "/" . $archivo . "' style='color:#000;'><span class='btn btn-success'><span class='glyphicon glyphicon-save'></span></span></a>";
                                    if (Yii::app()->authManager->checkAccess("ddeletefile", Yii::app()->user->getState("rolId"))) {
                                        echo "<span class='btn btn-danger'><span class='glyphicon glyphicon-trash deletefile' partialfolder='" . $partialfolder . "/" . $archivo . "' iddiv='" . $md5 . "'></span></span>";
                                    }
                                    echo " </div> </div>";
                                }
                                $cantidad++;
                            }
                        }
                    }

                    closedir($dir);
                }
            }
            if ($cantidad == 0) {
                echo '<span href="#" class="list-group-item text-center" style="padding:20px;"><h2>0 files for download</h2></span>';
            }
        } else {
            echo "<span href='#' class='list-group-item text-center' style='padding:20px;'><h2>you don't have enough access permission</h2></span>";
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     *        	the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['Files'])) {
            $redirect = true;
            // var_dump($_POST['Files']); echo '<br/> <br/>';
            // var_dump($_FILES["uploaded_file"]['name']);
            $_POST['Files']['language'] = 'es';
            if ($_POST['Files']['name'] != '' && $_POST['Files']['version'] != '' && $_POST['Files']['language'] != '') {
                $model->attributes = $_POST['Files'];
                $model->category = 15;
                $path = $_FILES["uploaded_file"]['name'];
                $productFile = new ProductFiles();
                $productFile->file = $model->id;
                $productFile->product = $_POST['Files']["productos"];
                $productFile->file_type = $_POST['Files']["file_type"];
                $productFile->save();
                $ProductsProductsType = new ProductsProductsType();
                $ProductsProductsType->product = $_POST['Files']["productos"];
                $ProductsProductsType->product_type = $_POST['Files']["brands_models"];
                $ProductsProductsType->save();
                if ($_FILES["uploaded_file"]['name'] != '') {
                    if (is_file(Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . $model->path))
                        unlink(Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . $model->path);

                    $model->path = '/downloads/' . $path;
                    move_uploaded_file($_FILES["uploaded_file"]['tmp_name'], Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . 'downloads/' . $path);
                }
                $model->path = $_FILES["uploaded_file"]['name'] != '' ? '/downloads/' . $_FILES["uploaded_file"]['name'] : $model->path;
                $model->save();
                //$this->redirect(Yii::app()->baseUrl . '/filesmanage/Downloads/index');
            }
            //if($redirect)
            //$this->redirect(Yii::app()->baseUrl . '/filesmanage/Downloads/index');
        }
        $criteria = new CDbCriteria();
        $criteria->select = 'id, name';
        $types = CHtml::listData(FilesTypes::model()->findAll($criteria), 'id', 'name');

        $criteria_rol = new CDbCriteria();
        $criteria_rol->select = 'rol_id';
        $criteria_rol->condition = 'id=:idrol';
        $criteria_rol->params = array(
            ':idrol' => Yii::app()->user->id);
        // var_dump(RBACUsuarios::model()->find($criteria_rol)->rol_id ); exit();

        $user_rol = RBACUsuarios::model()->find($criteria_rol)->rol_id;

        foreach ($types as $id => $name) {

            $criteria_rol_typeFile = new CDbCriteria();
            $criteria_rol_typeFile->select = 'id_rol';
            $criteria_rol_typeFile->condition = 'id_filetype=:idtypefile AND id_rol=:user_rol AND active=1';
            $criteria_rol_typeFile->params = array(
                ':idtypefile' => $id,
                ':user_rol' => $user_rol);
            $rol_type = RolesTypeFiles::model()->find($criteria_rol_typeFile);
            if (!is_null($rol_type)) {
                if ($user_rol == $rol_type->id_rol) {
                    $type_file_rol[$id] = $name;
                }
            }
        }
        unset($_POST['Files']);
        $types = FilesTypes::model()->findAll();

        $this->render('update', array(
            'model' => $model,
            'type_file_rol' => $type_file_rol));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * 
     * @param integer $id
     *        	the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)
                ->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array(
                        'admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Files('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Files']))
            $model->attributes = $_GET['Files'];

        $this->render('admin', array(
            'model' => $model));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * 
     * @param integer $id
     *        	the ID of the model to be loaded
     * @return Files the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Files::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'La página solicitada no existe.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * 
     * @param Files $model
     *        	the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'files-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionUpdateActive() {
        $value = $_POST['active'];
        $id = $_POST['id'];

        if ($value == "true")
            $activo = 1;
        else
            $activo = 0;

        $file = $this->loadModel($id);
        $file->active = $activo;
        $file->save();
        $sql = "SELECT PT.id FROM files F, product_files PF, products_texts PT WHERE F.id = " . $id . " AND F.id = PF.file AND PF.product = PT.product AND PT.category = 8";
        $texts = Yii::app()->db->createCommand($sql)
                ->queryAll();
        foreach ($texts as $text) {
            $activate = ProductsTexts::model()->findByPk($text['id']);
            $activate->visible = $activo;
            $activate->save();
        }
        $log = new Logs();
        $log->description = "Modulo: <b>Archivos</b>. <br/>
                                         Accion: <b>Activar</b>.<br/>
                                         ID: <b>" . $id . "</b>.";
        $log->ip = $_SERVER['REMOTE_ADDR'];
        $log->date = date('d/m/Y h:i:s a');
        $log->user = Yii::app()->user->getState("id");
        $log->language = "es";
        $log->save();

        exit();
    }

    public function actionCompanyFiles() {
        $files = Files::model()->findall('category=22');
        $this->render('companyFilesAdmin', array(
            'files' => $files));
    }

    public function actionSelectproduct() {
        $id_marca = $_POST['Files']['brands_models'];

        $lista = ProductsBrand::model()->findAll('product_type = :id_marca', array(
            ':id_marca' => $id_marca));
        // $lista_name = Products::model()->findAll('id = :id_marca' , array(':id_marca'=>$id_marca));
        // var_dump($lista); exit();
        foreach ($lista as $key => $valor) {
            $id = $valor->product;
            $Productos[$key] = Products::model()->find('id = ?', array(
                $id));
        }
        // var_dump($Productos);
        // $lista = CHtml::listData($lista,'product', 'product_type');
        // $lista = CHtml::listData($lista,'product_type', Products::model()->findAll('id = :id_marca' , array(':id_marca'=>$id_marca)));

        echo CHtml::tag('option', array(
            'value' => ''), 'SELECCIONAR UN PRODUCTO ', true);

        foreach ($Productos as $valor) {

            echo CHtml::tag('option', array(
                'value' => $valor->id), CHtml::encode($valor->name), true);
        }
    }

    public function actioncreatecompany() {
        if ($_FILES['file']['name'] != '') {
            $files_name = array(
                'is' => 'LED LIGHTING FOR INDUSTRIAL USE',
                'cs' => 'LED LIGHTING FOR COMMERCIAL USE',
                'cp' => 'COMPANY PROFILE');
            $f = Files::model()->find("name='" . $files_name[$_POST["filename"]] . "' and language='" . $_POST["language"] . "' ");
            $path = '/downloads/company_files/';
            $dir = Yii::app()->params['serverRoot'] . Yii::app()->params['folder'];
            $exists = true;
            if ($f == NULL) {
                $exists = false;
                $f = new Files();
            }
            if ($exists) {
                unlink($dir . '/' . $f->path);
            }
            $f->name = $files_name[$_POST["filename"]];
            $f->category = 22;
            $f->active = 1;
            if (!is_dir($dir . $path))
                mkdir($dir . $path, 0777, true);
            $f->path = $path . $_FILES['file']['name'];
            $f->version = 1;
            $f->language = $_POST["language"];
            $f->pathTypeFile = 1;

            move_uploaded_file($_FILES["file"]['tmp_name'], $dir . $f->path);
            if ($f->save()) {

                Yii::app()->user->setFlash('success', "Data saved!");
            }
        } else {
            Yii::app()->user->setFlash('danger', "you must select a file!");
        }
        $this->redirect(array(
            'CompanyFiles'));
    }

}
