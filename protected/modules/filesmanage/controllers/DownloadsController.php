<?php
/* Importo Modelos */
Yii::import('application.modules.roles.models.RBACRoles');
Yii::import('application.modules.roles.models.RBACUsuarios');
class DownloadsController extends Controller
{
	public $modulo = "filesmanage";
	public function actionIndex()
	{
//echo $this->modulo; die();
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_index'))
		{
			$criteria = new CDbCriteria();
			$criteria->select = 'id, name';
			$types = CHtml::listData(FilesTypes::model()->findAll($criteria), 'id', 'name');
			$criteria_rol = new CDbCriteria();
			$criteria_rol->select = 'rol_id';
			$criteria_rol->condition = 'id=:idrol';
			$criteria_rol->params = array(
					':idrol' => Yii::app()->user->id);
			$user_rol = RBACUsuarios::model()->find($criteria_rol)->rol_id;
			/* ESTA CONSULTA BUSCA TODOS LOS FILES DONDE CATEGORIA SEA IGUAL A 15 QUE ES BROCHURE o 24 ies */
			$sql = "SELECT distinct 
					FileType.id, ProductProductType.product, FileType.name pathTypeFile ,File.id fileid, File.id fileid,File.active fileactive, File.name filename,ProductType.name Product_Type,ProductType.id Product_Type_id,File.path,File.pathTypeFile type, File.language, File.created created, File.typefile Extension
					FROM files File 
					INNER JOIN product_files ProductFile ON (File.id=ProductFile.file) 
					INNER JOIN products Product ON(ProductFile.product = Product.id) 
					INNER JOIN files_types FileType ON(ProductFile.file_type = FileType.id) 
					INNER JOIN products_products_type ProductProductType on (ProductFile.product= ProductProductType.product) 
					INNER JOIN products_types ProductType on (ProductProductType.product_type = ProductType.id)
					INNER JOIN roles_type_files RolesType on (RolesType.id_filetype = FileType.id and RolesType.id_rol=" . $user_rol . " )
					WHERE File.category = 15 and File.language= 'es' AND Product.active=1";
			$Files = Yii::app()->db->createCommand($sql)
				->queryAll();
                       
			if(Yii::app()->language == 'en')
			{
				$and = "and File.language= 'es'";
			}
			else
			{
				$and = "and (File.language= '" . Yii::app()->language . "' or  File.language= 'en')";
			}
			
			$sql2 = "SELECT File.name, File.path, File.language FROM files File INNER JOIN categories Category ON (File.category = Category.code) WHERE Category.name = 'PDFDownload' AND File.active = 1 " . $and;
			$guias = Yii::app()->db->createCommand($sql2)
				->queryAll();
			
			$sql3 = "SELECT FileType.id, FileType.name FileTypeName FROM files_types FileType where name !='PDF' and name !='IMAGEN' ";
			$tipoArchivos = Yii::app()->db->createCommand($sql3)->queryAll();
			// muestra los tipos de productos y los productos para seleccionar
			
			$filtros = ProductsTypes::model()->with('products')->findAll();
			
			foreach($types as $id => $name)
			{
				
				// echo $id;
				// echo"<br>";
				// echo $user_rol;
				// echo"<br>";
				//
				$criteria_rol_typeFile = new CDbCriteria();
				$criteria_rol_typeFile->select = 'id_rol';
				$criteria_rol_typeFile->condition = 'id_filetype=:idtypefile AND id_rol=:user_rol AND active=1';
				$criteria_rol_typeFile->params = array(
						':idtypefile' => $id,
						':user_rol' => $user_rol);
				$rol_type = RolesTypeFiles::model()->find($criteria_rol_typeFile);
				
				// echo"rol_type->id_rol :";
				// echo $rol_type->id_rol;
				// echo"<br>";
				// echo"user_rol :";
				// echo $user_rol;
				// echo"<br>";
				
				if(!is_null($rol_type))
				{
					
					// echo "file";
					// echo $rol_type->id_rol;
					
					if($user_rol == $rol_type->id_rol)
					{
						$type_file_rol[$id] = $name;
					}
				}
			}
			// exit();
			
			// var_dump($filtros);
			$this->render('index', array(
					'Files' => $Files,
					'guias' => @$guias,
					'tipoArchivos' => $tipoArchivos,
					'filtros' => @$filtros,
					'type_file_rol' => @$type_file_rol));
		}
	}
	public function loadModel($id)
	{
		$model = Files::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
	public function actionUpdateActive($it = NULL, $s = NULL)
	{
		$model = $this->loadModel($it);
		
		if($s == 1)
		{
			
			$model->active = 1;
			$respuesta['dw'] = 1;
		}
		elseif($s == 0)
		{
			
			$model->active = 0;
			$respuesta['dw'] = 0;
		}
		if($model->validate())
		{
			
			if($model->save()){
                            echo CJSON::encode(array('message' => 'exito', 'success' => 'true'));
                            //print CJSON::encode($respuesta);
                        }else{
                           
                            echo CJSON::encode(array('message' => $model->getErrors(), 'success' => 'false'));
                        }
			
		}
			
	}
}
