<?php

class FilesController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', /* Acciones Permitidas */
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'descargas', 'UpdateActive', 'active'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public $modulo = "files";

    /**
     * 
     * 
     */
    public function actionView($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {
            $model = new Files;

            if (isset($_POST['Files'])) {
              
                $model->attributes = $_POST['Files'];
                
                $path = $_FILES["uploaded_file"]['name'];
                date_default_timezone_set('UTC');
                $model->dates = date("Y-m-d");
                //echo date("Y-m-d"); exit();
                if (!file_exists(Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . 'downloads/' . $path)) {
                             $model->path = $path;
                    move_uploaded_file($_FILES["uploaded_file"]['tmp_name'], Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . 'downloads/' . $path);
                    $model->model = $_POST['Files']['brands_models'];

                    if ($model->save()) {
                        
                        $brandsModelsFiles = new BrandsModelsFiles;
                        $brandsModelsFiles->file = $model->id;
                        $brandsModelsFiles->file_type = $model->typefile;
                        $brandsModelsFiles->brands_models = $_POST['Files']['brands_models'];


                        if ($brandsModelsFiles->save()) {

                            $this->redirect(array('view', 'id' => $model->id));
                        }
                    }
                } else {

                    $model->addError('path', 'this file is already saved!');
                }
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {

            $model = $this->loadModel($id);
            // $model2 = BrandsModelsFiles::model()->find("file = '$model->id'");
            $model2 = BrandsModelsFiles::model()->find(array('select' => 'brands_models', 'condition' => "file = '$model->id'"));
            $model3 = BrandsModelsFiles::model()->find(array('select' => 'file_type', 'condition' => "file = '$model->id'"));

            $model->brands_models = @$model2->brands_models;

            if (isset($_POST['Files'])) {


                $model->attributes = $_POST['Files'];
                if ($_FILES["uploaded_file"]['name'] != '') {

                    $path = $_FILES["uploaded_file"]['name'];

                    if (!file_exists(Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . 'downloads/' . $path)) {

                        $model->path = $path;

                        move_uploaded_file($_FILES["uploaded_file"]['tmp_name'], Yii::app()->params['serverRoot'] . Yii::app()->params['folder'] . 'downloads/' . $path);
                    } else {

                        $model->addError('path', 'this file is already saved!');
                    }
                }
                if ($model->save()) {
                    BrandsModelsFiles::model()->updateAll(array('brands_models' => $_POST['Files']['brands_models'], 'file_type' => $_POST['Files']['typefile']), "file = '$model->id'");
                    if ($model2 == null || $model2 == "" || $model3 == null || $model3 == "") {

                        $brandsModelsFiles = new BrandsModelsFiles;
                        $brandsModelsFiles->file = $model->id;
                        $brandsModelsFiles->file_type = $model->typefile;
                        $brandsModelsFiles->brands_models = $_POST['Files']['brands_models'];
                        $brandsModelsFiles->save();
                    }
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }


            $this->render('update', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
  

//        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {
            $model = new Files('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Files'])) {
                $model->attributes = $_GET['Files'];
            }
            $this->render('index', array(
                'model' => $model,
            ));
//        } else {
//            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
//        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_admin')) {
            $dataProvider = new CActiveDataProvider('Files');
            $this->render('admin', array(
                'dataProvider' => $dataProvider,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionDescargas() {
//        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {
//            $marcas = Brands::model()->findAll(array(
//            ));
            $marcas = BrandsModelsFiles::model()->findAll(array(
                'select' => "b.id,b.name,b.description",
                'join' => "inner join brands_models bm on (bm.id=t.brands_models) 
                           inner join brands b on (bm.brand_id=b.id) 
                    ",
                'group' => "name"
            ));
            

            $grid = new BrandsModelsFiles('search');
            $grid->unsetAttributes();
            if (isset($_GET['BrandsModelsFiles'])) {
                $grid->attributes = $_GET['BrandsModelsFiles'];
            }





            $this->render('descargas', array(
                'marcas' => $marcas,
                'grid' => $grid,
            ));
//        } else {
//            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
//        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Files the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Files::model()->find(array('condition' => 'id = :id', 'params' => array(':id' => $id)));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionUpdateActive() {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_descargas')) {

            $value = $_POST['active'];
            $id = $_POST['id'];

            if ($value == "true")
                $activo = 1;
            else
                $activo = 0;

            $file = $this->loadModel($id);
            $file->active = $activo;
            $file->save();
            $sql = "SELECT PT.id FROM files F, product_files PF, products_texts PT WHERE F.id = " . $id . " AND F.id = PF.file AND PF.product = PT.product AND PT.category = 8";
            $texts = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($texts as $text) {
                $activate = ProductsTexts::model()->findByPk($text['id']);
                $activate->visible = $activo;
                $activate->save();
            }

            exit();
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Files $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'files-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionActive($id = NULL, $s = NULL) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_active')) {

            if (Yii::app()->request->isPostRequest) {
                $model = $this->loadModel($id);
                $before = $model->attributes;
                if ($s == 1) {
                    $model->active = 1;
                } elseif ($s == 0) {
                    $model->active = 0;
                }
                if ($model->validate()) {
                    if ($model->save()) {
                        $after = $model->attributes;
                        //$this->register_transactions($model->tableName(), $before, $after, Yii::app()->params['t_edit'], $this->modulo);
                    }
                }

                if (!isset($_GET['ajax'])) {
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
                }
            } else {
                throw new CHttpException(404, Yii::app()->params['Error404']);
            }
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

}
