<?php

/**
 * This is the model class for table "products_types".
 *
 * The followings are the available columns in table 'products_types':
 * @property string $id
 * @property string $name
 * @property integer $visible
 * @property integer $parent_id
 *
 * The followings are the available model relations:
 * @property ProductsTypesTexts[] $productsTypesTexts
 */
class ProductsTypes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductsTypes the static model class
	 */
        public $position; 
         public $sort_product;
          public $add_series;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_types';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, visible', 'required'),
			array('visible, parent_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, visible, parent_id', 'safe', 'on'=>'search'),
		);
	}
        
        public function  getposition($id){
            
           $pt= ProductsTypes::model()->findall();
           $count=  count($pt);
           foreach ($pt as $pt){
               if($pt->id==$id){
                    if($pt->score==$count){
                         $text= $pt->score.'&nbsp;&nbsp;&nbsp;&nbsp;<div style="float:right;">';
                    }else{
        $text= $pt->score.'<div style="float:right;">';
                    }
        if($pt->score<$count){
        $text=$text.'<span class="glyphicon glyphicon-arrow-down pointer positionUP" product="'.$id.'" position="'.$pt->score.'" maxposition="'.$count.'"></span>';
        }else {
      
        }    
         if($pt->score>1){
        $text=$text.'<span class="glyphicon glyphicon-arrow-up pointer positionDown" product="'.$id.'" position="'.$pt->score.'"  ></span>';
          }else {
           $text=$text.'&nbsp;&nbsp;&nbsp;&nbsp;'; 
        }
    
        $text= $text.'</div>';
         $text=$text.'&nbsp;'; 
        }  
       
               }
        return $text;
        
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productsTypesTexts' => array(self::HAS_MANY, 'ProductsTypesTexts', 'product_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('modules', "Name"),
			'visible' => Yii::t('modules', "Active"),
			'parent_id' => Yii::t('modules', "Parent"),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($ordenar = FALSE)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('parent_id',$this->parent_id);
                if($ordenar == FALSE){
                $criteria->order = "score asc";
                }
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function visible($visible,$id)
        {
            if($visible == 1)
            {   
                $cadena = '<div id="'.$id.'" class="make-switch" style="z-index: 1;" data-on="success" data-off="danger" data-on-label="'.Yii::t('pages', 'YES').'" data-off-label="NO">
                                <input type="checkbox" checked="checked" value="" />
                           </div>';
                return $cadena;
            }
            else
            {
                $cadena = '<div id="'.$id.'" class="make-switch" style="z-index: 1;" data-on="success" data-off="danger" data-on-label="'.Yii::t('pages', 'YES').'" data-off-label="NO">
                                <input type="checkbox" value="" />
                           </div>';
                return $cadena;
            }
        }
         public function getCountries($id, $showlist = 0) {
        //consuta que selecciona paises que tienen un producto en especifico
        $sql = ($showlist == 0) ? "SELECT c.*,p.id prod FROM `countries` c
                left join products_countries pc on pc.country_id=c.code
                left join products p on p.id=pc.product_id
                where p.id=" . $id . " or p.id is null" :
                ' 
                SELECT  c.*, if(p.id<>' . $id . ' or p.id is null,null,' . $id . ') prod FROM `countries` 
                    c left join  products_countries pc on pc.country_id=c.code left join products p on
                    p.id=pc.product_id  ';
        $countries = Yii::app()->db->createCommand($sql)->queryAll();
        $countrytext = ''; // para almacenar lo que se va a retornar
        $i = 0;
        $count = count($countries); // cantidad de paises
        if ($showlist == 0) { // 0 para mostrar solo la lista de paises activos
            foreach ($countries as $country):


                if ($country["prod"] != null) { // si el pais tiene un producto los muesta si no nada
                    $countrytext = $countrytext . '<abbr title="' . $country["name"] . '"><b>' . $country["code"] . '</b></abbr>,    ';
                    $i++;
                }
            endforeach;
            $countrytext = ($i == 0) ? 'None' : $countrytext;
            $countrytext = $countrytext . ' <a href="#" title="edit countries where the product is available " class="edit_countries " id_product="' . $id . '" style="float:right"><div class="glyphicon glyphicon-flag " style="font-size: 20px; color: #424242" ></div>          </a>';
        } else {// para mostrar la lista de todos los paises seleccionados o no, es para la ventana emergente
            // la consulta de la base de datos trae resultados de mas y hay que filtrarlos            
            $i = 0; // para la posicion del recorrido de los paises
            $prodarray = array(); // almacena productos mostrados
            foreach ($countries as $country):

                $id_country = $country['id']; // el id del producto
                $pos = $i; // para guardar la posicion
                $j = 0; // para guardar la posicion de comparaci�n                       
                if (!in_array($id_country, $prodarray)) { // si el producto no ha sido agregado
                    foreach ($countries as $country1): // se compara el producto actual con todos los otros productos 

                        if ($country1['id'] == $id_country && $j != $pos) {// si el producto de comparacion es diferente del actual pero tiene el mismo id 
                            if ($country1['prod'] != NULL) { // si el producto de comparaci�n es diferente de nulo
                                $country["prod"] = $country1['prod']; // se asigna el valor de el producto de comparaci�n al actual
                                // si no no se hace nada y se agrega a la lista
                            }
                        }
                        $j++; // incrementar en 1 la posicion de comparacion
                    endforeach;
                }
                if (!in_array($id_country, $prodarray)) { // si el producto no ha sido agregado se muestra
                    $checked = ($country["prod"] != null) ? 'checked="checked"' : '';
                    $countrytext = $countrytext . ' <input type="checkbox" name="countrychk[]"  value="' . $country["code"] . '" ' . $checked . '/>' . $country["name"] . ' <br />';
                    $prodarray[] = $id_country; // se agrega el producto al arreglo que los almacena
                }
                $i++;
            endforeach;
        }
        $countrytext = $countrytext . '<input type="hidden" name="product_id" id="id_product_c" value="' . $id . '"/>';
        return $countrytext;
    }
    public function getCountries_alter($id, $showlist = 0) {
        //consuta que selecciona paises que tienen un producto en especifico
		
        $sql = ($showlist == 0) ? "SELECT c.*,p.id prod FROM `countries` c
                left join categories_countries pc on pc.country_id=c.code
                left join products_types p on p.id=pc.categories_id
                where p.id=" . $id . " or p.id is null" :
                ' 
                SELECT  c.*, if(p.id<>' . $id . ' or p.id is null,null,' . $id . ') prod FROM `countries` 
                    c left join  products_countries pc on pc.country_id=c.code left join products p on
                    p.id=pc.product_id  ';

        

        $countries = Yii::app()->db->createCommand($sql)->queryAll();
        $countrytext = ''; // para almacenar lo que se va a retornar
        $i = 0;
        $count = count($countries); // cantidad de paises
        if ($showlist == 0) { // 0 para mostrar solo la lista de paises activos
            foreach ($countries as $country):


                if ($country["prod"] != null) { // si el pais tiene un producto los muesta si no nada
                    $countrytext = $countrytext . '<abbr title="' . $country["name"] . '"><b>' . $country["code"] . '</b></abbr>,    ';
                    $i++;
                }
            endforeach;
            $countrytext = ($i == 0) ? 'None' : $countrytext;
            
            
            $countrytext = $countrytext . ' <a href="#" title="edit countries where the product is available " class="edit_countries " id_product="' . $id . '" style="float:right"><div class="glyphicon glyphicon-flag " style="font-size: 20px; color: #424242" ></div></a>';
        } else {// para mostrar la lista de todos los paises seleccionados o no, es para la ventana emergente
            // la consulta de la base de datos trae resultados de mas y hay que filtrarlos            
            $i = 0; // para la posicion del recorrido de los paises
            $prodarray = array(); // almacena productos mostrados
            foreach ($countries as $country):

                $id_country = $country['id']; // el id del producto
                $pos = $i; // para guardar la posicion
                $j = 0; // para guardar la posicion de comparaci�n                       
                if (!in_array($id_country, $prodarray)) { // si el producto no ha sido agregado
                    foreach ($countries as $country1): // se compara el producto actual con todos los otros productos 

                        if ($country1['id'] == $id_country && $j != $pos) {// si el producto de comparacion es diferente del actual pero tiene el mismo id 
                            if ($country1['prod'] != NULL) { // si el producto de comparaci�n es diferente de nulo
                                $country["prod"] = $country1['prod']; // se asigna el valor de el producto de comparaci�n al actual
                                // si no no se hace nada y se agrega a la lista
                            }
                        }
                        $j++; // incrementar en 1 la posicion de comparacion
                    endforeach;
                }
                if (!in_array($id_country, $prodarray)) { // si el producto no ha sido agregado se muestra
                    $checked = ($country["prod"] != null) ? 'checked="checked"' : '';
               //     $countrytext = $countrytext . ' <input type="checkbox" name="countrychk[]"  value="' . $country["code"] . '" ' . $checked . '/>' . $country["name"] . ' <br />';
                    $prodarray[] = $id_country; // se agrega el producto al arreglo que los almacena
                }
                $i++;
            endforeach;
        }
        $countrytext = $countrytext . '<input type="hidden" name="product_id" id="id_product_c" value="' . $id . '"/>';
        return $countrytext;
    }
    
    
    
    public function getFeatured($id){
        $model=  Products::model()->findByPk($id);
        if($model->featured_product!=NULL  and $model->featured_product!=0){
            $retorno='<div><div class="glyphicon glyphicon-star" style="font-size: 20px; color: #fddc02"></div></div>';
        }else {
            $retorno='<div  ><div class="glyphicon glyphicon-star addfeatured"  id_product="'.$id.'" style="font-size: 20px; color: #000"></div></div>';
        }
        return $retorno;
    }

        public function getsortlist($id){
                $sql="SELECT distinct p.id, p.name,p.score, i.path FROM products p 
                    inner join products_products_type ppt on p.id=ppt.product 
                    inner join products_types pt on ppt.product_type=pt.id 
                    inner join products_images i on i.product=p.id 
                    where pt.id=".$id." and i.category=0 and i.language='en' and p.active=1 order by p.score asc";
             $products= Yii::app()->db->createCommand($sql)->queryAll();
            echo '
<script>
$(function() {
$( "#sortable" ).sortable();
})
</script>
<ul id="sortable">';
            
$i=0;
            foreach ($products as $p){
                echo '<li class="ui-state-default sortable" score="'.$p["score"].'" pos="'.$i.'" id_product="'.$p["id"].'" id="product_'.$p["id"].'">';
                    if(is_file(Yii::app()->params['serverRoot'].Yii::app()->params['folder'] .$p["path"])){
                        echo '<br/><img src="'. Yii::app()->params['folder'] .$p["path"].'"/>';
                    }else {
                       echo  $p["name"];
                    }
                   echo' </li>';
           $i++; }

echo '</ul>';
        }
}