<?php

/**
 * This is the model class for table "files_document".
 *
 * The followings are the available columns in table 'files_document':
 * @property string $id
 * @property integer $active
 * @property string $name
 * @property string $path
 * @property string $version
 * @property string $language
 * @property string $pathTypeFile
 * @property string $date_publishing
 */
class filesDocument extends CActiveRecord
{
	
	public function tableName()
	{
		return 'files_document';
	}

	
	public function rules()
	{
		
		return array(
			array('estatus, name, path, language, pathTypeFile, description,size, date_publishing', 'required'),
			array('estatus', 'numerical', 'integerOnly'=>true),
			array('language', 'length', 'max'=>2),
			array('pathTypeFile', 'length', 'max'=>100),
			array('version', 'safe'),
		
			array('id, estatus, name,description, path, version, language, pathTypeFile, date_publishing', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
                        'type' => Yii::t('lang','type'),
                        'estatus' => Yii::t('lang','estatus'),
                        'size' => Yii::t('lang','size'),
                        'name' => Yii::t('lang','description'),
			'description' => Yii::t('lang','description'),
			'path' => Yii::t('lang','Path'),
			'version' => Yii::t('lang','Version'),
			'language' => Yii::t('lang','Language'),
			'pathTypeFile' => Yii::t('lang','Path Type File'),
                        'date_publishing' => Yii::t('lang','Fecha de publicación'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
                $criteria->compare('type',$this->type,true);
		$criteria->compare('estatus',$this->estatus);
                $criteria->compare('size',$this->size,true);
		$criteria->compare('name',$this->name,true);
                $criteria->compare('description',$this->description,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('pathTypeFile',$this->pathTypeFile,true);
                $criteria->compare('date_publishing',$this->date_publishing,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
         public function visible($visible,$id)
        {
            if($visible == 1)
            {   
                $cadena = '<div id="'.$id.'" class="make-switch" style="z-index: 1;" data-on="success" data-off="danger" data-on-label="'.Yii::t('pages', 'YES').'" data-off-label="NO">
                                <input type="checkbox" checked="checked" value="" />
                           </div>';
                return $cadena;
            }
            else
            {
                $cadena = '<div id="'.$id.'" class="make-switch" style="z-index: 1;" data-on="success" data-off="danger" data-on-label="'.Yii::t('pages', 'YES').'" data-off-label="NO">
                                <input type="checkbox" value="" />
                           </div>';
                return $cadena;
            }
        }


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
