<?php

/**
 * This is the model class for table "products_products_type".
 *
 * The followings are the available columns in table 'products_products_type':
 * @property string $product_type
 * @property string $product
 *
 * The followings are the available model relations:
 * @property Products $product0
 * @property ProductsTypes $productType
 */
class ProductsProductsType extends CActiveRecord
{
	
	public function tableName()
	{
		return 'products_products_type';
	}

	
	public function rules()
	{
		
		return array(
			array('product_type, product', 'required'),
			array('product_type, product', 'length', 'max'=>10),
		
			array('product_type, product', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'product0' => array(self::BELONGS_TO, 'Products', 'product'),
			'productType' => array(self::BELONGS_TO, 'ProductsTypes', 'product_type'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'product_type' => Yii::t('lang','Product Type'),
			'product' => Yii::t('lang','Product'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('product_type',$this->product_type,true);
		$criteria->compare('product',$this->product,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
