<?php

/**
 * This is the model class for table "brands_models_files".
 *
 * The followings are the available columns in table 'brands_models_files':
 * @property integer $id
 * @property integer $file
 * @property integer $brands_models
 */
class BrandsModelsFiles extends CActiveRecordExt
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'brands_models_files';
	}
        public $type;
        public $dates;
        public $version;
        public $desca;
        public $name;   
        public $description;
        public $id;
        public $file;
        public $file_type;
        /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file, brands_models', 'required'),
			array('file, brands_models', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, file,,name, brands_models,type,dates,version,desca', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                     'brandsModels' => array(self::BELONGS_TO, 'BrandsModels', 'brands_models'),
                     'file0' => array(self::BELONGS_TO, 'Files', 'file'),
                     'fileType' => array(self::BELONGS_TO, 'FilesTypes', 'file_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
                    'id' => 'ID',
                    'file' => 'Nombre de Archivo',
                    'type' => 'Tipo de Archivo',
                    'version' => 'Versión',
                    'dates' => 'Fecha de Publicación',
                    'brands_models' => 'Brands Models',
                    'desca'=>'Descargar'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('file0.name',$this->file,true);
                $criteria->compare('fileType.name',$this->type,true);
                $criteria->compare('file0.version',$this->version,true);
                $criteria->compare('file0.desca',$this->desca,true);
           
		$criteria->compare('brands_models',$this->brands_models);
                $criteria->with=array('file0','fileType');
               
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BrandsModelsFiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
