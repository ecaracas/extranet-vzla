<?php

/**
 * This is the model class for table "brands_models".
 *
 * The followings are the available columns in table 'brands_models':
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $brand_id
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Brands $brand
 */
class BrandsModels extends CActiveRecordExt {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'brands_models';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, code, brand_id, active', 'required'),
            array('brand_id, active', 'numerical', 'integerOnly' => true),
            array('code', 'length', 'max' => 2),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, code, brand_id, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'brand' => array(self::BELONGS_TO, 'Brands', 'brand_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'brand_id' => 'Brand',
            'active' => 'Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('brand_id', $this->brand_id);
        $criteria->compare('active', $this->active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BrandsModels the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getListBrandsModel() {

        $data = BrandsModels::model()->findAll(array(
            'select' => 'id, name',
            'condition' => "active = 1 AND code <> ''",
            'order' => 'name ASC'
        ));

        return CHtml::listData($data, 'id', 'name');
    }
    
        public static function getListBrandsModelsCode($id = NULL) {

        $data = BrandsModels::model()->findAll(array(
            'select' => 'id, name',
            'condition' => "active = 1 AND brand_id = :id  AND code <> ''",
            'params'=>array(':id'=>intval($id)),
            'order' => 'name ASC'
        ));

        return CHtml::listData($data, 'id', 'name');
    }

}
