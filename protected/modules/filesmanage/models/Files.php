<?php

/**
 * This is the model class for table "files".
 *
 * The followings are the available columns in table 'files':
 * @property integer $id
 * @property integer $active
 * @property string $name
 * @property string $path
 * @property string $version
 
 * @property integer $typefile
 * @property string $dates
 */
class Files extends CActiveRecordExt
{
	/**
	 *
	 * @return string the associated database table name
	 */
	public $brands_models;
	public $productos;
	public function tableName()
	{
		return 'files';
	}
	public function behaviors()
	{
		return array(
				// Classname => path to Class
				'LogsableBehavior' => 'application.behaviors.LogsableBehavior')

		;
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('name, path, version','required'),
				array('active, pathTypeFile, publicSitePa','numerical','integerOnly' => true),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array('id, active, name, path, version,  pathTypeFile, created, publicSitePa','safe','on' => 'search'));
	}
	
	/**
	 *
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}
	
	/**
	 *
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'active' => 'active',
				'name' => 'Nombre',
				'path' => 'Ruta',
				'version' => 'Versión',
				'pathTypeFile' => 'Tipo de Archivo',
				'created' => 'Fecha',
				'brands_models' => 'Modelos',
				'publicSitePa' => 'Pública en página WEB'
		)

		;
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 *         based on the search/filter conditions.
	 */
	public function search()
	{
		
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria();
		
		$criteria->compare('id', $this->id);
		$criteria->compare('active', $this->active);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('path', $this->path, true);
		$criteria->compare('version', $this->version, true);
		$criteria->compare('pathTypeFile', $this->pathTypeFile);
		$criteria->compare('publicSitePa', $this->publicSitePa);
		$criteria->compare('created', $this->created, true);
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * 
	 * @param string $className
	 *        	active record class name.
	 * @return Files the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	public static function fecha($date)
	{
		if($date == '' || empty($date))
			return '';
		
		$meses = array(
				"Enero",
				"Febrero",
				"Marzo",
				"Abril",
				"Mayo",
				"Junio",
				"Julio",
				"Agosto",
				"Septiembre",
				"Octubre",
				"Noviembre",
				"Diciembre");
		
		list($anio, $mes, $dia) = preg_split('/-/', $date);
		
		$month = $meses[((int)$mes) - 1];
		
		$fechaLegible = $dia[0] . $dia[1] . " de " . $month . " del " . $anio; // Spanish
		
		/* $fechaLegible = $month.' '.$dia.", ".$anio; */
		// English
		
		return $fechaLegible;
	}
	public function visible($visible, $id)
	{
		// echo $visible.'aaa'.$id;exit();
		if($visible == 1)
		{
			$cadena = '<div id="' . $id . '" class="make-switch" style="z-index: 1;" data-on="success" data-off="danger" data-on-label="' . Yii::t('pages', 'YES') . '" data-off-label="NO">
                                <input type="checkbox" checked="checked" value="" />
                           </div>';
			return $cadena;
		}
		else
		{
			$cadena = '<div id="' . $id . '" class="make-switch" style="z-index: 1;" data-on="success" data-off="danger" data-on-label="' . Yii::t('pages', 'YES') . '" data-off-label="NO">
                                <input type="checkbox" value="" />
                           </div>';
			return $cadena;
		}
	}
}
