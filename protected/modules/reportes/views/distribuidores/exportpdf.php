<style>

.demo {
		width:100%;
		height:100%;
		border:1px solid #C0C0C0;
		border-collapse:collapse;
		padding:2px;
	}
	.demo caption {
		text-align:center !important;
	}
	.demo th {
		border:1px solid #C0C0C0;
		padding:2px;
		background:#F0F0F0;
        color :#556B8D;
	}
	.demo td {           
		border:1px solid #C0C0C0;
		text-align:center !important;
		padding:2px;
	}
	.logo {
		width:100%;
		height:100%;
		border-collapse:collapse;
		padding:2px;
	}	
	.logo th {
		padding:2px;        
	}
	.logo td {  
		text-align:center !important;	
	}
	strong{
		font-size: 12px;
	}
</style>
<div style="clear:both; float:left">
	<strong>THE FACTORY HKA PANAMA</strong><BR/>
	<strong>RUC: 155596713-2-2015 DV: 59</strong><BR/>
	<strong>LISTA DE DISTRIBUIDORES</strong><BR/>
</div>
<table class="demo">
	<caption><br></caption>
	<thead>
	<tr>
		<th><strong>N°</strong></th>
		<th><strong>EMPRESA</strong></th>
		<th><strong>DIRECCION</strong></th>
		<th><strong>N° DE RUC</strong></th>
		<th><strong>D.V.</strong></th>
	</tr>
	</thead>
	<tbody>
	<?php $i=1;?>
	<?php foreach ($model as $distribuidor):?>
	<tr>
		<td>&nbsp;<?php echo $i;?></td>
		<td align="left">&nbsp;<?php echo $distribuidor['razon_social']; ?></td>
		<td align="left">&nbsp;<?php echo $distribuidor['address']; ?></td>
		<td>&nbsp;<?php echo $distribuidor['dni_numero']; ?></td>
		<td>&nbsp;<?php echo $distribuidor['ruc_dv'];?></td>
	</tr>
	<?php $i++; endforeach;?>
	</tbody>
</table>

