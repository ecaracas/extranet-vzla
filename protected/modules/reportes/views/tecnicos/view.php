<?php
/* @var $this UsuariosController */
/* @var $model RBACUsuarios */
//var_dump($model);
$this->breadcrumbs = array(
    'Rbacusuarioses' => array('index'),
    $model->id,
);

$this->menu = array(
    array(
        'label' => Yii::app()->params['create-text'],
        'url' => array('create'),
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label' => Yii::app()->params['update-text'],
        'url' => array('update',
            'id' => $model->id),
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label' => Yii::app()->params['index-text'],
        'url' => array('index'),
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title">Listado de Técnicos.</h3>

                <div class="menu-tool">
                    <?php
//                    $this->widget('zii.widgets.CMenu', array(
//                    'items' => $this->menu,
//                    'encodeLabel' => FALSE,
//                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
//                    ));
                    ?>
                    <a href="<?php echo Yii::app()->baseUrl ?>/reportes/tecnicos"  type="button" class="btn btn-info">Atras</a>
                    <br>
                </div>
                <?php $this->ToolActionsRight(); ?>       
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('rol_id')); ?></th>
                                <td><?php echo CHtml::encode($model->rol->descripcion); ?></td>
                            </tr>
                            <?php if ($model->distribuidor_id != ''): ?>
                                <tr>
                                    <th><?php echo CHtml::encode($model->getAttributeLabel('distribuidor_id')); ?></th>
                                    <td><?php echo CHtml::encode(RBACUsuarios::model()->find('id = ' . $model->distribuidor_id)->nombre); ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo CHtml::encode($model->getAttributeLabel('razon_social')); ?></th>
                                    <td><?php echo CHtml::encode($model->razon_social); ?></td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('nombreusuario')); ?></th>
                                <td><?php echo CHtml::encode($model->nombreusuario); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></th>
                                <td><?php echo CHtml::encode($model->email); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('nombre')); ?></th>
                                <td><?php echo CHtml::encode($model->nombre); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('apellido')); ?></th>
                                <td><?php echo CHtml::encode($model->apellido); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('estatus')); ?></th>
                                <td><?php echo CHtml::encode($model->estatus == 0 ? 'Inactivo' : 'Activo'); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('RUC')); ?></th>
                                <td><?php echo CHtml::encode($model->dni_numero); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('telf_local_tipo')); ?></th>
                                <td><?php echo CHtml::encode($model->telf_local_tipo); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('telf_local_numero')); ?></th>
                                <td><?php echo CHtml::encode($model->telf_local_numero); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('registro_fecha')); ?></th>
                                <td><?php echo CHtml::encode($model->registro_fecha); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('registro_hora')); ?></th>
                                <td><?php echo CHtml::encode($model->registro_hora); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('modificado_fecha')); ?></th>
                                <td><?php echo CHtml::encode($model->modificado_fecha); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('modificado_hora')); ?></th>
                                <td><?php echo CHtml::encode($model->modificado_hora); ?></td>
                            </tr>
                            <?php if($model->rol_id == 3): ?>
                            <tr>
                                <th>Descargar en Excel</th>
                                <td>  <?php echo CHtml::link('Descargar Excel', array('distribuidores/exportexcel/id/'.$model->id), array('class' => 'btn btn-primary btn-3d')); ?>         </td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
Yii::app()->params['IDGridview'] = 'users-grid';
Yii::app()->params['rutaUrlGridviewBoxSwitch'] = Yii::app()->controller->createUrl('UpdateActive');
$active = array('0'=>'Inactivos','1'=>'Activos');

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#users-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<?php if($model->rol_id == 3): ?>

<input type="hidden" id="TOKEN" value="<?php echo Yii::app()->request->csrfToken ?>">
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title">Tecnicos del distribuidor <b><?php echo RBACUsuarios::model()->find('id = ' . $model->distribuidor_id)->nombre?></b> </h3>

                <div class="menu-tool">

                    <div class="pagesize"> 
                        <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model2->tecnico($model->id)); ?>
                    </div>                     

                    <div class="menu-items">
                        <?php  //$this->widget('zii.widgets.CMenu', array('items' => $this->menu,'encodeLabel' => FALSE,'htmlOptions' => array('class' => 'cmenuhorizontal'),)); ?>
                    </div>
                </div>        
                <?php $this->ToolActionsRight(); ?>         

            </div>
            <div class="panel-body"> 
               
                <div class="table-responsive">
                    <?php                   
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>Yii::app()->params['IDGridview'],       
                        'dataProvider' => $dataProvider,
                        'filter' => $model2,
                        'columns' => array(
                            array('name' => 'rol_id', 'value' => '$data->rol->descripcion', 'type' => 'text', 'filter' => CHtml::listData(RBACRoles::model()->findAll(), "id", "descripcion")),
                            array('name' => 'nombreusuario', 'value' => '$data->nombreusuario'),                          
                            array('name' => 'email', 'value' => '$data->email'),
                            array('name' => 'nombre', 'value' => '$data->nombre'),
//                            array('name' => 'distribuidor_id', 'value' => '$data->nombreusuario'),
                            array('name' => 'estatus','type' => 'raw','value' => 'Active::checkSwicth($data->estatus,$data->id)', 'filter' => Active::getListActive(),'htmlOptions' => array('class' => 'switchactive')
                            ),
                          
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::app()->params['view-text'],
                                'label' => Yii::app()->params['view-icon'],
                                'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                            ),
                            
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
