<?php
if(Yii::app()->user->getState('rol') == 3)
{
	$this->widget('zii.widgets.jui.CJuiTabs',array(
			'tabs'=>array(
					'Técnicos'=>array('id'=>'Activos','content'=>$this->renderPartial('_activos',array('model'=>$model),TRUE)),
			),
			'options'=>array('collapsible'=>true,),
			'id'=>'Usuarios',
	));
}
else{
	$this->widget('zii.widgets.jui.CJuiTabs',array(
			'tabs'=>array(
					'Técnicos'=>array('id'=>'tecnicos','content'=>$this->renderPartial('_tecnicos',array('model'=>$model),TRUE)),
			),
			'options'=>array('collapsible'=>true,),
			'id'=>'Usuarios',
	));
}


?>