<?php
Yii::app()->params['IDGridview'] = 'users-pendiente-grid';
Yii::app()->params['rutaUrlGridviewBoxSwitch'] = Yii::app()->controller->createUrl('UpdateActive');
$active = array(
		'0' => 'Inactivos',
		'1' => 'Activos');

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#users-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<input type="hidden" id="TOKEN"
	value="<?php echo Yii::app()->request->csrfToken ?>">
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading navbar-tool">
				<h3 class="panel-title">Listado de Técnicos.</h3>
				<div class="menu-tool">
					<div class="pagesize"> 
					<?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->tecnico()); ?>
					</div>
					<div class="menu-items">
						<?php  $this->widget('zii.widgets.CMenu', array('items' => $this->menu,'encodeLabel' => FALSE,'htmlOptions' => array('class' => 'cmenuhorizontal'),)); ?>
					</div>
				</div>
				<?php $this->ToolActionsRight(); ?>
			</div>
			<div class="panel-heading navbar-tool">
				<?php if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) { ?>
				<?php $form=$this->beginWidget('CActiveForm', array('id'=>'tecnicos-form','action'=>Yii::app()->createUrl('/reportes/tecnicos/exportpdf'),'enableAjaxValidation'=>false,'htmlOptions'=>array('target'=>'_blank','class'=>'form-horizontal','role'=>'form'),)); ?>
					<?php echo CHtml::submitButton(Yii::t('lang','Exportar en pdf'),array('class'=>'btn btn-primary', 'style' => 'float:left; height: 40px')); ?>
					<?php echo CHtml::link('Descargar Excel', array('tecnicos/exportexcel/id/'.$model->id), array('class' => 'btn btn-primary', 'style' => 'text-transform: none; height: 40px; margin-left: 2px')); ?>                           
				<?php $this->endWidget(); ?>
			<?php } ?>
			</div>
			<div class="panel-body"> 
			<div class="table-responsive">
				<?php
				$filtro = '';
				if(Yii::app()->user->getState('rol') == 4)
				{
					$filtro = array(
							'condition' => 'estatus = 1 AND id in (3, 4, 5, 6)');
				}
				$this->widget('zii.widgets.grid.CGridView', array(
						'id' => Yii::app()->params['IDGridview'],
						'dataProvider' => $dataProvider,
						'filter' => $model,
						'columns' => array(
								array(
										'name' => 'nombreusuario',
										'value' => '$data->nombreusuario'),
								array(
										'name' => 'nombreapellido',
										'value' => '$data->nombre . " " . $data->apellido'),
								array(
										'name' => 'dni_numero',
										'value' => '$data->dni_numero'),
								array(
										'name' => 'distribuidor_b',
										'value' => '$data->distribuidor->razon_social'),
								array(
										'name' => 'distribuidor_dni',
										'value' => '$data->distribuidor->dni_numero'),
								array(
										'name' => 'email',
										'value' => '$data->email'),
								array(
										'class' => 'CLinkColumn',
										'header' => 'Detalle',
										'label' => Yii::app()->params['view-icon'],
										'linkHtmlOptions' => array(
												'class' => 'view ' . Yii::app()->params['view-btn']),
										'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
										'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')))));
				?>
				</div>
			</div>
		</div>
	</div>
</div>