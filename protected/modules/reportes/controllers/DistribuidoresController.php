<?php
class DistribuidoresController extends Controller
{
	
	/**
	 *
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 *      using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';
	
	public function behaviors()
	{
		return array(
				'eexcelview' => array(
						'class' => 'application.extensions.eexcelview.EExcelBehavior'));
	}
	
	/**
	 *
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete') // we only allow deletion via POST request
;
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * 
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array(
						'allow', /* Acciones Permitidas */
						'actions' => array(
								'index',
								'view',
								'create',
								'update',
								'delete',
								'exportpdf',
								'exportcsv',
								'exportexcel',
                                                                'UpdateActive'),
						'users' => array(
								'*')),
				array(
						'deny',
						'users' => array(
								'*')));
	}
	
	public $modulo = "reporteDistribuidores";
	
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		
		Yii::app()->params['title'] = '';
	}
	
	public function actionView($id)
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_view'))
		{
			$model = $this->loadModel($id);
			$model2 = new RBACUsuarios('tecnico');
                        $model2->unsetAttributes();
                        if(isset($_GET['RBACUsuarios'])){
                            
                            $model2->attributes=$_GET['RBACUsuarios'];
                        }
			$this->render('view', array('model' => $model, 'model2' => $model2));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionCreate()
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_create'))
		{
			
			$model = new FicalizedMachines();
			
			if(isset($_POST['FicalizedMachines']))
			{
				$model->attributes = $_POST['FicalizedMachines'];
				if($model->validate())
				{
					if($model->save())
					{
						
						Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model->id));
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
				{
					Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
				}
			}
			
			$this->render('create', array(
					'model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionUpdate($id)
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_update'))
		{
			
			$model = $this->loadModel($id);
			
			if(isset($_POST['FicalizedMachines']))
			{
				$model->attributes = $_POST['FicalizedMachines'];
				
				if($model->validate())
				{
					if($model->save())
					{
						Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model->id));
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
				{
					Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
				}
			}
			
			$this->render('update', array(
					'model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionexportcsv()
	{
		@$desde = Yii::app()->request->cookies['desde']->value;
		@$hasta = Yii::app()->request->cookies['hasta']->value;
		$criteria = new CDbCriteria();
		if($desde != null)
		{
			$criteria->condition = "t.date BETWEEN '" . $desde . "' and '" . $hasta . "' ";
		}
		$data = FicalizedMachines::model()->findAll($criteria);
		$xx = array();
		
		foreach($data as $dat)
		{
			
			$xx[0] = $dat['id'];
			$xx[1] = $dat['final_clients_id'];
			$xx[2] = $dat['date'];
			$xx[3] = $dat['ruc_technician'];
			$xx[4] = $dat['ruc_dealer'];
			$xx[5] = $dat['observations'];
			$xx[6] = $dat['machine_id'];
		}
		// $xx[$a['id']] = $a['id'];
		$hoy = date("Y-m-d");
		$handle = fopen($hoy . '.csv', "a+");
		for($j = 0; $j <= 6; $j++)
		{
			fwrite($handle, $xx[$j] . PHP_EOL);
		}
		fclose($handle);
	}
	
	public function actionexportpdf()
	{
		// if (isset($_POST['desde'])):
		$mPDF1 = Yii::app()->ePdf->mpdf();
		Yii::app()->theme = "pdf";
		$model = RBACUsuarios::model()->findAll(array('select' => 'razon_social, address, dni_numero, ruc_dv', 'condition' => 'rol_id = 3'));
		$mPDF1->WriteHTML($this->render('exportpdf', array(
				'model' => $model), true));
		$mPDF1->Output();
		
		// endif;
	}
	
	public function actionIndex()
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_index'))
		{
                    
			$model = new RBACUsuarios('search');
			$model->unsetAttributes(); // clear any default values
			if(isset($_GET['RBACUsuarios']))
			{
				$model->attributes = $_GET['RBACUsuarios'];
			}
			if(isset($_GET['RBACUsuarios_sort']))
			{
				Yii::app()->params['GridViewOrder'] = TRUE;
			}
			$this->render('index', array('model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
        
        //KATHE
        public function actionUpdateActive($it = NULL, $s = NULL)
	{
		$model = $this->loadModel($it);
		if($s == 1)
		{
			$model->estatus = 1;
		}
		elseif($s == 0)
		{
			$model->estatus = 0;
		}
		if($model->save())
		{
			//$correosInternos = array('ggomez@thefactoryhka.com', 'jramirez@thefactoryhka.com');
                        $correosInternos = array('ecaracas@thefactoryhka.com');
			if($model->estatus == 1)
			//$this->envioEmail($model->email, $model->nombre . ' su usuario ha sido activado, ya puede ingresar al sistema.', 'Activación de usuario The Factory Corp', 'Activación de usuario');
			echo CJSON::encode(array('message' => 'exito', 'success' => 'true'));
		}
		else
			echo CJSON::encode(array('message' => $model->getErrors(), 'success' => 'true'));
	}
	
	public function loadModel($id)
	{
		$sql = new CDbCriteria();
		$sql->params = array(
				':id' => intval($id));
		$sql->condition = "id = :id";
		$sql->addcondition("estatus <> 9");
		$model = RBACUsuarios::model()->find($sql);
		if($model === null)
		{
			throw new CHttpException(404, Yii::app()->params['Error404']);
		}
		return $model;
	}
	
	public function actionExportexcel($id = null)
	{
		if(isset($_SESSION['promotion.excel']))
		{
			$data = $_SESSION['promotion.excel'];
			$hoy = date("Y-m-d H:i:s");
			$this->toExcel($data, array(
					'rol.descripcion',
					'razon_social',
					'nombreapellido::Persona de Contacto',
					'dni_numero::RUC',
					'email',
					'telf_local_numero'), $hoy);
		}
	}
}
