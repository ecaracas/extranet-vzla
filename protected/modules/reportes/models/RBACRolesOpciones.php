<?php

/**
 * This is the model class for table "rbac_roles_opciones".
 *
 * The followings are the available columns in table 'rbac_roles_opciones':
 * 
 * @property integer $id
 * @property integer $rol_id
 * @property integer $menu_opcion_id
 * @property integer $estatus
 * @property string $registro_fecha
 * @property string $registro_hora
 * @property integer $registro_usuario_id
 * @property string $modificado_fecha
 * @property string $modificado_hora
 * @property integer $modificado_usuario_id
 * @property integer $secuencia The followings are the available model relations:
 * @property RbacRolesOpcionesUsuarios[] $rbacRolesOpcionesUsuarioses
 * @property RbacMenuOpciones $menuOpcion
 * @property RbacRoles $rol
 */
class RBACRolesOpciones extends CActiveRecord
{
	public function tableName()
	{
		return 'rbac_roles_opciones';
	}
        
//        public function behaviors(){
//            return array(
//                // Classname => path to Class
//                'LogsableBehavior'=>'application.behaviors.LogsableBehavior',
//
//            );
//        }
        
	
	public function rules()
	{
		return array(
				array(
						'rol_id, menu_opcion_id, registro_fecha, registro_usuario_id, modificado_fecha, modificado_usuario_id',
						'required' ),
				array(
						'rol_id, menu_opcion_id, estatus, registro_usuario_id, modificado_usuario_id, secuencia',
						'numerical',
						'integerOnly' => true ),
				array(
						'registro_hora, modificado_hora',
						'safe' ),
				
				array(
						'id, rol_id, menu_opcion_id, estatus, registro_fecha, registro_hora, registro_usuario_id, modificado_fecha, modificado_hora, modificado_usuario_id, secuencia',
						'safe',
						'on' => 'search' ) );
	}
	
	public function relations()
	{
		return array(
				'rbacRolesOpcionesUsuarioses' => array(
						self :: HAS_MANY,
						'RBACRolesOpcionesUsuarios',
						'rol_opcion_id' ),
				'menuopcion' => array(
						self :: BELONGS_TO,
						'RBACMenuOpciones',
						'menu_opcion_id' ),
				'rol' => array(
						self :: BELONGS_TO,
						'RBACRoles',
						'rol_id' ) );
	}
	
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'rol_id' => 'Rol',
				'menu_opcion_id' => 'Menu Opcion',
				'estatus' => 'Estatus',
				'registro_fecha' => 'Registro Fecha',
				'registro_hora' => 'Registro Hora',
				'registro_usuario_id' => 'Registro Usuario',
				'modificado_fecha' => 'Modificado Fecha',
				'modificado_hora' => 'Modificado Hora',
				'modificado_usuario_id' => 'Modificado Usuario',
				'secuencia' => 'Secuencia' );
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		
		$criteria->compare('id', $this -> id);
		$criteria->compare('rol_id', $this -> rol_id);
		$criteria->compare('menu_opcion_id', $this -> menu_opcion_id);
		$criteria->compare('estatus', $this -> estatus);
		$criteria->compare('registro_fecha', $this -> registro_fecha, true);
		$criteria->compare('registro_hora', $this -> registro_hora, true);
		$criteria->compare('registro_usuario_id', $this -> registro_usuario_id);
		$criteria->compare('modificado_fecha', $this -> modificado_fecha, true);
		$criteria->compare('modificado_hora', $this -> modificado_hora, true);
		$criteria->compare('modificado_usuario_id', $this -> modificado_usuario_id);
		$criteria->compare('secuencia', $this -> secuencia);
		
		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");
		
		if(Yii::app() -> params['GridViewOrder'] == FALSE)
		{
			$criteria -> order = "id DESC";
		}
		
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria ));
	}
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
