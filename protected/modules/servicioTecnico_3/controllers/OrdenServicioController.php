<?php

class OrdenServicioController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', /* Acciones Permitidas */
                'actions' => array('index', 'view', 'create', 'update', 'delete', 'test'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public $modulo = "serviciotecnico";

    public function __construct($id, $module = null) {

        parent::__construct($id, $module);

        Yii::app()->params['title'] = '';
    }

    public function actionView($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {

            $model = $this->loadModel($id);

            $this->render('view', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionCreate() {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {

            $model = new StOrdenServicio;
            $rol = $this->actionGetRol();


            if (isset($_POST['StOrdenServicio'])) {
                $model->attributes = $_POST['StOrdenServicio'];


                if ($model->validate()) {


                    $siglas = substr($model->serial, 0, 6);
                    $maquina = $this->actionGetMaquina($siglas);
                    $model->marca = $maquina['name'];
                    $model->modelo = $maquina['model'];
                    $operacion = Operations::model()->find('serial=:serial', array(':serial' => $model->serial));
                    $perfil = RBACUsuarios::model()->find('id=:id', array(':id' => Yii::app()->user->id));


                    if ($perfil->rol_id == 8) {
                        $model->id_tecnico = $perfil->id;
                        $model->id_distribuidor = $operacion->dealer_id;
                    } elseif ($perfil->rol_id == 3) {
                        $model->id_distribuidor = $perfil->id;
                        $model->id_tecnico = $operacion->techinician_id;
                    } else {
                        $model->id_tecnico = $operacion->techinician_id;
                        $model->id_distribuidor = $operacion->dealer_id;
                    }
                    $model->fecha_enajenacion = $operacion->date;
                    $model->fecha_registro = date('Y-m-d');

                    if ($model->save()) {

                        $id_orden = $model->id;

                        foreach ($model->accesorios as $valor) {

                            $model_accesorios_orden = new STAccesoriosOrden;
                            $model_accesorios_orden->id_orden = $id_orden;
                            $model_accesorios_orden->id_accesorio = $valor;
                            $model_accesorios_orden->fecha = date('Y-m-d');

                            $model_accesorios_orden->save();
                        }

                        $model_historia = new STHistorialOrdenes;

                        $model_historia->id_orden = $id_orden;
                        $model_historia->id_usuariorevision=Yii::app()->user->id;
                        $model_historia->id_estatus = 1;
                        $model_historia->descripcion = $model->descripcion_falla;
                        $model_historia->fecha = date('Y-m-d');

                        if ($model_historia->save()) {
                            
                            $id_historia = $model_historia->id;
                            
                           // var_dump($model->fallas);
                           
                            foreach ($model->fallas as $valor) {

                                $model_fallas_historia = new StFallasHistoria;
                                $model_fallas_historia->id_falla = $valor;
                                $model_fallas_historia->id_historia = $id_historia;
                                $model_fallas_historia->fecha = date('Y-m-d');

                                $model_fallas_historia->save();
                            }
                        }

                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                   
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('create', array('model' => $model, 'rol' => $rol));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionUpdate($id) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {

            $model = $this->loadModel($id);
            $model_accesorios = STAccesoriosOrden::model()->findAll('id_orden=:id', array(':id' => $id));

            $_array = array();
            foreach ($model_accesorios as $key => $value) {

                array_push($_array, $value->id_accesorio);
            }

            $model->accesorios = $_array;

            if (isset($_POST['StOrdenServicio'])) {
                $model->attributes = $_POST['StOrdenServicio'];

                if ($model->validate()) {
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('update', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionIndex() {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {

            $model = new StOrdenServicio('search');

            $model->unsetAttributes();  // clear any default values

            if (isset($_GET['StOrdenServicio'])) {
                $model->attributes = $_GET['StOrdenServicio'];
            }

            if (isset($_GET['StOrdenServicio_sort'])) {

                Yii::app()->params['GridViewOrder'] = TRUE;
            }

            $this->render('index', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionTest() {

        $serial = $_POST['serial'];
        $siglas = substr($serial, 0, 6);
        $operacion = $this->actionGetRuc($serial);

        $datos_maquina = $this->actionGetMaquina($siglas);


        if (!empty($datos_maquina) && !empty($operacion)) {
            echo json_encode(array(
                'rif' => $operacion->ruc,
                'marca' => $datos_maquina['name'],
                'modelo' => $datos_maquina['model']
            ));
            Yii::app()->end();
        } else {

            return false;
        }
        var_dump($datos_maquina);
    }

    public function actionGetRuc($serial) {

        $sql = Operations::model()->find('serial=:serial', array(':serial' => $serial));
        return $sql;
    }

    public function actionGetMaquina($siglas) {

        $sql2 = Yii::app()->db->createCommand()
                ->select('p.model, b.name')
                ->from(' tfhka_site.products p')
                ->join('tfhka_site.brands b', 'p.brand_id=b.id')
                ->join('tfhka_site.brands_country c', 'b.id=c.brand_id')
                ->where('c.country_id=:country_id and p.siglas=:siglas', array(':country_id' => 'pa', ':siglas' => $siglas))
                ->queryRow();

        return $sql2;
    }

    public function actionGetRol() {

        $criteria = new CDbCriteria;
        $criteria->select = 'rol_id';  // only select the 'title' column
        $criteria->condition = 'id=:id';
        $criteria->params = array(':id' => Yii::app()->user->id);
        $rol_user = RBACUsuarios::model()->find($criteria);
        return $rol_user->rol_id;
    }

    public function loadModel($id) {
        $sql = new CDbCriteria;
        $sql->params = array(':id' => intval($id));
        $sql->condition = "id = :id";
        //$sql->addcondition("estatus <> 9");
        $model = StOrdenServicio::model()->find($sql);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }
        return $model;
    }

}
