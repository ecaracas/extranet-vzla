<?php

class HistorialOrdenesController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', /* Acciones Permitidas */
                'actions' => array('index', 'view', 'create', 'update', 'delete'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public $modulo = "serviciotecnico";

    public function __construct($id, $module = null) {

        parent::__construct($id, $module);

        Yii::app()->params['title'] = '';
    }

    public function actionView($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {

            $model = $this->loadModel($id);
           

            //se verifica el estatus de la historia y si esta anulado o entregada se pone deshabilitar el noton actualizar
            if ($model->id_estatus == 8 || $model->id_estatus == 9) {
                $btn_activo = false;
            } else {

                $btn_activo = Yii::app()->authRBAC->checkAccess($this->modulo . '_update');
            }



            $model_fallas = StFallasHistoria::model()->findAll('id_historia=:id', array(':id' => $id));

            $_array = array();
            foreach ($model_fallas as $key => $value) {

                array_push($_array, $value->id_falla);
            }

            $model->fallas = $_array;


            $this->render('view', array('model' => $model, 'btn_activo'=> $btn_activo ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionCreate() {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {

            $model = new STHistorialOrdenes;


            if (isset($_POST['STHistorialOrdenes'])) {
                $model->attributes = $_POST['STHistorialOrdenes'];
                if ($model->validate()) {
                    if ($model->save()) {

                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('create', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionUpdate($id_orden) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {

            $model = $this->loadHistoria($id_orden);

            //se define la ruta donde se almacenaran las imagenes
            $path_picture = Yii::app()->basePath . '/../img/uploads/'; // Yii::app()->baseUrl . "/img/uploads/";



            if (isset($_POST['STHistorialOrdenes'])) {


                $model->attributes = $_POST['STHistorialOrdenes'];
                $model->id = null;
                $model->isNewrecord = true;

                // se captura la información de los archivos
                $imagenes = CUploadedFile::getInstancesByName('reg_imagenes');
                $cont = 0;
                // si el usurio cargo alguna imagen               

                if (isset($imagenes) && count($imagenes) > 0) {
                    //Se recorre el arreglo para validar que los archivos cumplan con las condiciones

                    if (count($imagenes) < 5) {
                        // echo "<br>aqui1";
                        foreach ($imagenes as $value => $carga) {
                            $rnd = rand(0, 9999);
                            $model->reg_fotografico = $carga;

                            if ($model->validate()) {
                                $nombre_imagen = $carga->name;
                                $carga->saveAs($path_picture . '_' . $rnd . '_' . $nombre_imagen);
                                $cont++;
                            } else {
                                Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                            }
                        }
                    } else {

                        $model->addError('reg_fotografico', 'No se pueden seleccionar mas de 4 imagenes de manera simultanea');
                    }
                }

                // exit();
                // si los archivos no presentan errores
                if (count($imagenes) == $cont) {

                    $model->id_usuariorevision = Yii::app()->user->id;
                    $model->fecha = date("Y-m-d");
                    //Se guarda el historico
                    if ($model->save()) {

                        $id_historia = $model->id;

                        if (!empty($model->fallas)) {
                            foreach ($model->fallas as $valor) {

                                $model_fallas_historia = new StFallasHistoria;
                                $model_fallas_historia->id_falla = $valor;
                                $model_fallas_historia->id_historia = $id_historia;
                                $model_fallas_historia->fecha = date('Y-m-d');

                                $model_fallas_historia->save();
                            }
                        }


                        //Y se almacenan la relación de las imagenes con la historia
                        if (isset($imagenes) && count($imagenes) > 0) {
                            foreach ($imagenes as $value => $carga) {
                                //Se almacena la información de las imagenes asociadas al historico
                                $model_imagenes = new StRegistroFotografico;
                                $model_imagenes->path_imagen = $carga->name;
                                $model_imagenes->id_historia = $model->id;
                                $model_imagenes->created_at = date("Y-m-d");
                                $model_imagenes->save();
                            }
                        }

                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {

                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    //var_dump($model->errors);
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('update', array('model' => $model,));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionIndex($id_orden) {

        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {

            $model = new STHistorialOrdenes('search');

            $model->unsetAttributes();  // clear any default values
            $model->id_orden = $id_orden;
            //Se buscan los datos de la orden a la cual pertenese la historia
            $model_orden = Yii::app()->db->createCommand()
                    ->select('o.id, d.nombreusuario as distribuidor, t.nombreusuario as tecnico,o.serial, o.marca, o.modelo')
                    ->from('tfhka_extranet_patest.st_orden_servicio o')
                    ->join('tfhka_extranet_patest.rbac_usuarios d', 'o.id_distribuidor=d.id')
                    ->join('tfhka_extranet_patest.rbac_usuarios t', 'o.id_tecnico=t.id')
                    ->where('o.id=:id', array(':id' => $id_orden))
                    ->queryRow();

            //Se verifica el estatus de la orden en caso de estar entregado o anulado se dashabilita el boton de actualizar
            $model_estatus = $this->loadHistoria($id_orden);
            if ($model_estatus->id_estatus == 8 || $model_estatus->id_estatus == 9) {
                $btn_activo = false;
            } else {

                $btn_activo = Yii::app()->authRBAC->checkAccess($this->modulo . '_update');
            }

            if (isset($_GET['STHistorialOrdenes'])) {
                $model->attributes = $_GET['STHistorialOrdenes'];
            }

            if (isset($_GET['STHistorialOrdenes_sort'])) {

                Yii::app()->params['GridViewOrder'] = TRUE;
            }

            $this->render('index', array('model' => $model, 'model_orden' => $model_orden, 'btn_activo' => $btn_activo));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function loadModel($id) {
        $sql = new CDbCriteria;
        $sql->params = array(':id' => intval($id));
        $sql->condition = "id = :id";

        // $sql->addcondition("estatus <> 9");
        $model = STHistorialOrdenes::model()->find($sql);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }
        return $model;
    }

    public function loadHistoria($id_orden) {

        $sql2 = new CDbCriteria;
        $sql2->params = array(':id_orden' => intval($id_orden));
        $sql2->condition = "id_orden = :id_orden";
        $sql2->order = 'id DESC';
        $sql2->limit = 1;
        // $sql->addcondition("estatus <> 9");
        $model = STHistorialOrdenes::model()->find($sql2);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }

        return $model;
    }

}
