<?php

/**
 * This is the model class for table "st_fallas_historia".
 *
 * The followings are the available columns in table 'st_fallas_historia':
 * @property integer $id
 * @property integer $id_falla
 * @property integer $id_historia
 * @property string $fecha
 *
 * The followings are the available model relations:
 * @property StFallas $idFalla
 * @property StHistorialOrdenes $idHistoria
 */
class StFallasHistoria extends CActiveRecord
{
	
	public function tableName()
	{
		return 'st_fallas_historia';
	}

	
	public function rules()
	{
		
		return array(
			array('id_falla, id_historia, fecha', 'required'),
			array('id_falla, id_historia', 'numerical', 'integerOnly'=>true),
		
			array('id_falla, id_historia, fecha', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'idFalla' => array(self::BELONGS_TO, 'StFallas', 'id_falla'),
			'idHistoria' => array(self::BELONGS_TO, 'StHistorialOrdenes', 'id_historia'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'id_falla' => Yii::t('lang','Id Falla'),
			'id_historia' => Yii::t('lang','Id Historia'),
			'fecha' => Yii::t('lang','Fecha'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_falla',$this->id_falla);
		$criteria->compare('id_historia',$this->id_historia);
		$criteria->compare('fecha',$this->fecha,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
