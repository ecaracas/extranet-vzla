<?php

/**
 * This is the model class for table "st_orden_servicio".
 *
 * The followings are the available columns in table 'st_orden_servicio':
 * @property integer $id
 * @property integer $id_distribuidor
 * @property integer $id_tecnico
 * @property integer $id_producto
 * @property string $marca
 * @property string $modelo
 * @property string $serial
 * @property string $descripcion_falla
 * @property string $revision_previa
 * @property string $contacto
 * @property string $telefono
 * @property string $medio_envio
 * @property string $medio_recepcion
 * @property string $direccion_recepcion
 * @property string $email
 * @property string $fecha_registro
 * @property string $fecha_enajenacion
 * @property integer $garantia
 *
 * The followings are the available model relations:
 * @property StAccesoriosOrden[] $stAccesoriosOrdens
 * @property StHistorialOrdenes[] $stHistorialOrdenes
 * @property StOrdenEnvio[] $stOrdenEnvios
 * @property RbacUsuarios $idDistribuidor
 * @property RbacUsuarios $idTecnico
 * @property StRegistroFotografico[] $stRegistroFotograficos
 */
class StOrdenServicio extends CActiveRecord {

    public $rif_usuario;
    public $distribuidor;
    public $tecnico;
    public $accesorios;
    public $fallas;


    public function tableName() {
        return 'st_orden_servicio';
    }

    public function rules() {

        return array(
            array('serial,descripcion_falla, revision_previa, contacto, telefono, email, accesorios, fallas', 'required'),
            array('serial', 'exist', 'allowEmpty' => true, 'attributeName' => 'serial', 'className' => 'Operations'),
            array('id_distribuidor, id_tecnico, id_producto, garantia', 'numerical', 'integerOnly' => true),
            array('email', 'email'),
            array('telefono','length', 'is'=>11),
            
            array('serial, descripcion_falla, revision_previa, contacto,  medio_envio, medio_recepcion, direccion_recepcion, email, rif_usuario', 'length', 'max' => 45),
            array('marca, modelo, fecha_registro, fecha_enajenacion', 'safe'),
            array('id, distribuidor,tecnico, id_distribuidor, id_tecnico, id_producto, marca, modelo, serial, descripcion_falla, revision_previa, contacto, telefono, medio_envio, medio_recepcion, direccion_recepcion, email, fecha_registro, fecha_enajenacion, garantia', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {

        return array(
            'stAccesoriosOrdens' => array(self::HAS_MANY, 'StAccesoriosOrden', 'id_orden'),
            'stHistorialOrdenes' => array(self::HAS_MANY, 'StHistorialOrdenes', 'id_orden'),
            'stOrdenEnvios' => array(self::HAS_MANY, 'StOrdenEnvio', 'id_orden'),
            'idDistribuidor' => array(self::BELONGS_TO, 'RbacUsuarios', 'id_distribuidor'),
            'idTecnico' => array(self::BELONGS_TO, 'RbacUsuarios', 'id_tecnico'),
            
                //'operations' => array(self::HAS_ONE, 'Operations', 'serial'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('lang', 'ID'),
            'tecnico' => Yii::t('lang', 'Tecnico'),
            'distribuidor' => Yii::t('lang', 'Distribuidor'),
            'id_distribuidor' => Yii::t('lang', 'Distribuidor'),
            'id_tecnico' => Yii::t('lang', 'Tecnico'),
            'id_producto' => Yii::t('lang', 'Producto'),
            'marca' => Yii::t('lang', 'Marca'),
            'modelo' => Yii::t('lang', 'Modelo'),
            'serial' => Yii::t('lang', 'Serial'),
            'descripcion_falla' => Yii::t('lang', 'Descripcion Falla'),
            'revision_previa' => Yii::t('lang', 'Revision Previa'),
            'contacto' => Yii::t('lang', 'Contacto'),
            'telefono' => Yii::t('lang', 'Telefono'),
            'medio_envio' => Yii::t('lang', 'Medio Envio'),
            'medio_recepcion' => Yii::t('lang', 'Medio Recepcion'),
            'direccion_recepcion' => Yii::t('lang', 'Direccion Recepcion'),
            'email' => Yii::t('lang', 'Email'),
            'fecha_registro' => Yii::t('lang', 'Fecha Registro'),
            'fecha_enajenacion' => Yii::t('lang', 'Fecha Enajenacion'),
            'garantia' => Yii::t('lang', 'Garantia'),
        );
    }

    public function search() {

        $criteria = new CDbCriteria;
        $criteria->with = array('idDistribuidor','idTecnico');

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.id_distribuidor', $this->id_distribuidor);
        $criteria->compare('t.id_tecnico', $this->id_tecnico);
        $criteria->compare('t.id_producto', $this->id_producto);
        $criteria->compare('t.marca', $this->marca, true);
        $criteria->compare('t.modelo', $this->modelo, true);
        $criteria->compare('t.serial', $this->serial, true);
        $criteria->compare('t.descripcion_falla', $this->descripcion_falla, true);
        $criteria->compare('t.revision_previa', $this->revision_previa, true);
        $criteria->compare('t.contacto', $this->contacto, true);
        $criteria->compare('t.telefono', $this->telefono, true);
        $criteria->compare('t.medio_envio', $this->medio_envio, true);
        $criteria->compare('t.medio_recepcion', $this->medio_recepcion, true);
        $criteria->compare('t.direccion_recepcion', $this->direccion_recepcion, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.fecha_registro', $this->fecha_registro, true);
        $criteria->compare('t.fecha_enajenacion', $this->fecha_enajenacion, true);
        $criteria->compare('t.garantia', $this->garantia);
        $criteria->compare('idDistribuidor.nombreusuario', $this->distribuidor, true);
        $criteria->compare('idTecnico.nombreusuario', $this->tecnico, true);

        /* Condicion para no Mostrar los Eliminados en Gridview */
        //$criteria->addcondition("estatus <> 9");

        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
