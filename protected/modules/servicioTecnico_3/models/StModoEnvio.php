<?php

/**
 * This is the model class for table "st_modo_envio".
 *
 * The followings are the available columns in table 'st_modo_envio':
 * @property integer $id
 * @property string $nombre_envio
 * @property string $descripcion
 * @property integer $estatus
 * @property string $fecha_creacion
 * @property string $hora_creacion
 * @property integer $usuario_crea
 * @property string $fecha_modificacion
 * @property string $hora_modificacion
 * @property integer $usuario_modifica
 *
 * The followings are the available model relations:
 * @property RbacUsuarios $usuarioCrea
 * @property RbacUsuarios $usuarioModifica
 */
class StModoEnvio extends CActiveRecord {

    public function tableName() {
        return 'st_modo_envio';
    }

    public function rules() {

        return array(
            array('nombre_envio, estatus, usuario_crea, usuario_modifica', 'required'),
            array('estatus, usuario_crea, usuario_modifica', 'numerical', 'integerOnly' => true),
            array('nombre_envio', 'length', 'max' => 45),
            array('descripcion, fecha_creacion, hora_creacion, fecha_modificacion, hora_modificacion', 'safe'),
            array('id, nombre_envio, descripcion, estatus, fecha_creacion, hora_creacion, usuario_crea, fecha_modificacion, hora_modificacion, usuario_modifica', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {

        return array(
            'usuarioCrea' => array(self::BELONGS_TO, 'RbacUsuarios', 'usuario_crea'),
            'usuarioModifica' => array(self::BELONGS_TO, 'RbacUsuarios', 'usuario_modifica'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('lang', 'ID'),
            'nombre_envio' => Yii::t('lang', 'Nombre Envio'),
            'descripcion' => Yii::t('lang', 'Descripcion'),
            'estatus' => Yii::t('lang', 'Estatus'),
            'fecha_creacion' => Yii::t('lang', 'Fecha Creacion'),
            'hora_creacion' => Yii::t('lang', 'Hora Creacion'),
            'usuario_crea' => Yii::t('lang', 'Usuario Crea'),
            'fecha_modificacion' => Yii::t('lang', 'Fecha Modificacion'),
            'hora_modificacion' => Yii::t('lang', 'Hora Modificacion'),
            'usuario_modifica' => Yii::t('lang', 'Usuario Modifica'),
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nombre_envio', $this->nombre_envio, true);
        $criteria->compare('descripcion', $this->descripcion, true);
        $criteria->compare('estatus', $this->estatus);
        $criteria->compare('fecha_creacion', $this->fecha_creacion, true);
        $criteria->compare('hora_creacion', $this->hora_creacion, true);
        $criteria->compare('usuario_crea', $this->usuario_crea);
        $criteria->compare('fecha_modificacion', $this->fecha_modificacion, true);
        $criteria->compare('hora_modificacion', $this->hora_modificacion, true);
        $criteria->compare('usuario_modifica', $this->usuario_modifica);

        /* Condicion para no Mostrar los Eliminados en Gridview */
        $rol = RBACUsuarios::model()->find('id=:id', array(':id' => Yii::app()->user->id));

        if ($rol->rol_id != 2) {
            $criteria->addcondition("estatus <> 9");
        }


        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "id DESC";
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
