<?php

/**
 * This is the model class for table "rbac_roles_opciones_usuarios".
 *
 * The followings are the available columns in table 'rbac_roles_opciones_usuarios':
 * 
 * @property integer $id
 * @property integer $usuario_id
 * @property integer $rol_opcion_id
 * @property integer $estatus
 * @property string $registro_fecha
 * @property string $registro_hora
 * @property integer $registro_usuario_id
 * @property string $modificado_fecha
 * @property string $modificado_hora
 * @property integer $modificado_usuario_id
 * @property integer $secuencia The followings are the available model relations:
 * @property RbacRolesOpciones $rolOpcion
 * @property RbacUsuarios $usuario
 */
class RBACRolesOpcionesUsuarios extends CActiveRecord
{
	public function tableName()
	{
		return 'rbac_roles_opciones_usuarios';
	}
        
        public function behaviors(){
            return array(
                // Classname => path to Class
                'LogsableBehavior'=>'application.behaviors.LogsableBehavior',

            );
        }

	
	public function rules()
	{
		return array(
				array(
						'usuario_id, rol_opcion_id, registro_fecha, registro_usuario_id, modificado_fecha, modificado_usuario_id',
						'required' ),
				array(
						'usuario_id, rol_opcion_id, estatus, registro_usuario_id, modificado_usuario_id, secuencia',
						'numerical',
						'integerOnly' => true ),
				array(
						'registro_hora, modificado_hora',
						'safe' ),
				
				array(
						'id, usuario_id, rol_opcion_id, estatus, registro_fecha, registro_hora, registro_usuario_id, modificado_fecha, modificado_hora, modificado_usuario_id, secuencia',
						'safe',
						'on' => 'search' ) );
	}
	
	public function relations()
	{
		return array(
				'rolopcion' => array(
						self :: BELONGS_TO,
						'RBACRolesOpciones',
						'rol_opcion_id' ),
				'usuario' => array(
						self :: BELONGS_TO,
						'RBACUsuarios',
						'usuario_id' ) );
	}
	
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'usuario_id' => 'Usuario',
				'rol_opcion_id' => 'Rol Opcion',
				'estatus' => 'Estatus',
				'registro_fecha' => 'Registro Fecha',
				'registro_hora' => 'Registro Hora',
				'registro_usuario_id' => 'Registro Usuario',
				'modificado_fecha' => 'Modificado Fecha',
				'modificado_hora' => 'Modificado Hora',
				'modificado_usuario_id' => 'Modificado Usuario',
				'secuencia' => 'Secuencia' );
	}
	
	public function search()
	{
		$criteria = new CDbCriteria();
		
		$criteria->compare('id', $this -> id);
		$criteria->compare('usuario_id', $this -> usuario_id);
		$criteria->compare('rol_opcion_id', $this -> rol_opcion_id);
		$criteria->compare('estatus', $this -> estatus);
		$criteria->compare('registro_fecha', $this -> registro_fecha, true);
		$criteria->compare('registro_hora', $this -> registro_hora, true);
		$criteria->compare('registro_usuario_id', $this -> registro_usuario_id);
		$criteria->compare('modificado_fecha', $this -> modificado_fecha, true);
		$criteria->compare('modificado_hora', $this -> modificado_hora, true);
		$criteria->compare('modificado_usuario_id', $this -> modificado_usuario_id);
		$criteria->compare('secuencia', $this -> secuencia);
		
		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");
		
		if(Yii::app() -> params['GridViewOrder'] == FALSE)
		{
			$criteria -> order = "id DESC";
		}
		
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria ));
	}
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
