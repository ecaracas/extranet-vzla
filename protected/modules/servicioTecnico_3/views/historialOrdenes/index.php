<?php
/* @var $this HistorialOrdenesController */
/* @var $model STHistorialOrdenes */

$this->breadcrumbs = array(
    Yii::t('lang', 'Sthistorial Ordenes') => array('index'),
    Yii::app('lang', 'Manage'),
);
Yii::app()->params['IDGridview'] = 'sthistorial-ordenes-grid';
$this->menu = array(/*
      array(
      'label' => Yii::t('lang', Yii::app()->params['create-text']),
      'url' => array('create'),
      'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
      'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')), */

    array(
        'label' => Yii::t('lang', Yii::app()->params['update-text2']),
        'url' => array('update', 'id_orden' => $model_orden["id"]),
        'linkOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => $btn_activo),
   /* array(
        'label' => Yii::t('lang', Yii::app()->params['return-text']),
        'url' => array("ordenservicio/index"),
        'linkOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),*/
);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo Yii::t('lang', Yii::app()->params['index-text']) . ' ' . Yii::t('lang', 'Sthistorial Ordenes'); ?></h3>

                <div class="menu-tool">

                    <div class="pagesize"> 
<?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
                    </div>         

                    <div class="menu-items">
<?php
$this->widget('zii.widgets.CMenu', array(
    'items' => $this->menu,
    'encodeLabel' => FALSE,
    'htmlOptions' => array('class' => 'cmenuhorizontal'),
));
?>
                    </div>
                </div>        
                        <?php $this->ToolActionsRight(); ?>         

            </div>
            <div class="panel-body"> 

                <div class="data_orden">
                    <h4><?php echo 'Orden número' ?>:<?php echo $model_orden["id"]; ?> <h4>
                            <h4><?php echo 'Distribuidor'; ?>: <label> <?php echo $model_orden["distribuidor"]; ?>  </label>/ <?php echo 'Técnico'; ?>: <label> <?php echo $model_orden["tecnico"]; ?>  </label></h4>
                            <h4><?php echo 'Serial '; ?>: <label> <?php echo $model_orden["serial"]; ?> </label></h4>
                            <h4><?php echo 'Marca '; ?>: <label> <?php echo $model_orden["marca"]; ?> </label></h4>
                            <h4><?php echo 'Modelo '; ?>: <label> <?php echo $model_orden["modelo"]; ?></label></h4>
                            <br>
                            </div>

                            <div class="table-responsive">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => Yii::app()->params['IDGridview'],
    'dataProvider' => $dataProvider,
    'filter' => $model,
    'columns' => array(
        // array('name'=>'id','value'=>'$data->id'),
        //array('name'=>'id_orden','value'=>'$data->id_orden'),
        array('name' => 'usuario_r', 'value' => '$data->idUsuarioRevision->nombreusuario'),
        array('name' => 'id_estatus', 'value' => '$data->stEstatus->nombre'),
        array('name' => 'descripcion', 'value' => '$data->descripcion'),
        array('name' => 'fecha', 'value' => '$data->fecha'),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang', Yii::app()->params['view-text']),
            'label' => Yii::app()->params['view-icon'],
            'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
            'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
        ),
        /* array(
          'class' => 'CLinkColumn',
          'header' => Yii::t('lang', Yii::app()->params['update-text2']),
          'label' => Yii::app()->params['update-icon'],
          'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn']),
          'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
          'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
          ), 
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang', Yii::app()->params['delete-text']),
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
        ),*/
    ),
   
));
?>

                                <div class='boton_back_'>
                                <?php
                                echo CHtml::link(Yii::t('lang', Yii::app()->params['return-text']), array('ordenservicio/index'), array('class' => 'edit ' . Yii::app()->params['update-btn'] . ' btn-sm'));
                                ?>
                                </div>
                            </div>
                            </div>
                            </div>
                            </div>
<?php
if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete', array());    
}
?>