<?php
/* @var $this HistorialOrdenesController */
/* @var $model STHistorialOrdenes */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'sthistorial-ordenes-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data'
    ),
        ));
?>



<?php //echo $form->errorSummary($model); ?>




<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'id_estatus'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->dropDownList($model, 'id_estatus', CHtml::listData(StEstatus::model()->findAll(array('condition'=>'id>:id AND estatus=:estatus',  'params'=>array(':id' => $model->id_estatus, ':estatus'=>'1'))), 'id', 'nombre'), array('class'=>'visualiza_')); ?>

        <?php echo $form->error($model, 'id_estatus'); ?>
    </div>

</div>


<div class="form-group visualiza_falla_">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'fallas'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->dropDownList($model, 'fallas', CHtml::listData(StFallas::model()->findAll('estatus=:estatus', array(':estatus'=>'1')), 'id', 'nombre_falla'),array('multiple'=>true ,'style'=>'width:777px;','size'=>'20','class'=>'js-example-basic-multiple altura' )); ?>

<?php echo $form->error($model, 'fallas'); ?>
    </div>

</div>



<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'descripcion'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'descripcion', array('size' => 45, 'maxlength' => 45)); ?>

        <?php echo $form->error($model, 'descripcion'); ?>
    </div>

</div>



<?php //if($model->id_estatus==2){?>
<div class="form-group visualiza_imag_">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'reg_fotografico'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <!--Se crear una division donde se agregaran los nuevos campos -->
        <div class='image_add'>
            <div> <?php echo CHtml::activeFileField($model, 'reg_fotografico', array('multiple' => 'multiple', 'id' => 'filer_input', 'name' => 'reg_imagenes')); ?></div>


        </div>

   
        <?php echo $form->error($model, 'reg_fotografico'); ?>
     
    </div>

</div>
<?php// }?>





<br />
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <p class="text-left"><?php echo Yii::t('lang', Yii::app()->params['camposrequeridos']); ?>
        </p>
    </div>
    <div class="col-xs-12 col-sm-6">                
        <ul class="cmenuhorizontal" id="yw0">

            <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang', Yii::app()->params['save-text']), array('class' => Yii::app()->params['save-btn'])); ?>
            </li>
            <li class="bottom"><?php echo CHtml::link(Yii::t('lang', Yii::app()->params['cancel-text']), Yii::app()->controller->createUrl('index', array('id_orden' => $model->id_orden)), array('class' => Yii::app()->params['cancel-btn'])); ?>
            </li>
            <!--            <li class="bottom"><?php echo CHtml::button(Yii::t('lang', Yii::app()->params['add-picture']), array('id' => 'add-picture_', 'class' => Yii::app()->params['add-btn'])); ?>
                        </li>-->
        </ul>
    </div>

</div>

<?php $this->endWidget(); ?>
