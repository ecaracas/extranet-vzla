<?php
/* @var $this HistorialOrdenesController */
/* @var $model STHistorialOrdenes */

$this->breadcrumbs = array(
    'Sthistorial Ordenes' => array('index'),
    $model->id,
);

$this->menu = array(
    /* array(
      'label'=>Yii::t('lang',Yii::app()->params['create-text']),
      'url'=>array('create'),
      'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
      'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')), */
    array(
        'label' => Yii::t('lang', Yii::app()->params['update-text2']),
        'url' => array('update', 'id_orden' => $model->id_orden),
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => $btn_activo),
    array(
        'label' => Yii::t('lang', Yii::app()->params['index-text']),
        'url' => array('index', 'id_orden' => $model->id_orden),
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo Yii::t('lang', Yii::app()->params['update-text']) . ' ' . Yii::t('lang', 'STHistorialOrdenes'); ?></h3>

                <div class="menu-tool">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items' => $this->menu,
                        'encodeLabel' => FALSE,
                        'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
                <?php $this->ToolActionsRight(); ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('id_orden')); ?></th>
                                <td><?php echo CHtml::encode($model->idOrdenes->id); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('id_usuariorevision')); ?></th>
                                <td><?php echo CHtml::encode($model->idUsuarioRevision->nombreusuario); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('id_estatus')); ?></th>
                                <td><?php echo CHtml::encode($model->stEstatus->nombre); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('fallas')); ?></th>
                                <td>
                                    <?php
                                    //var_dump($model->stFallasHistorias[0]->idFalla->nombre_falla);

                                    foreach ($model->stFallasHistorias as $valor) { //echo  ': '.$valor->idFalla->nombre_falla;
                                        ?>
                                        <?php echo CHtml::encode($valor->idFalla->nombre_falla) . '<br>'; ?>


                                    <?php } ?>

                                </td>

                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('descripcion')); ?></th>
                                <td><?php echo CHtml::encode($model->descripcion); ?></td>
                            </tr>
                            <tr>
                                <th><?php echo CHtml::encode($model->getAttributeLabel('fecha')); ?></th>
                                <td><?php echo CHtml::encode($model->fecha); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>


                <div id="owl-demo">
                    <?php
                    $count = 1;
                    foreach ($model->stRegistroFotograficos as $valor) {
                        ?>
                        <div class="item item_click" id="<?php echo $count; ?>"><img class="lazyOwl" data-src="<?php echo Yii::app()->baseUrl . "/img/uploads/" . $valor->path_imagen; ?>" alt="" ></div>
                        <?php $count++;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
  <?php ?>
    <style>
        #owl-demo .item{
            margin: 3px;
        }
        #owl-demo .item img{
            display: block;
            width: 100%;
            height: auto;
			    max-width: 284px !important;
    max-height: 260px !important;
}
            </style>
    <script>
        $(document).ready(function () {
            $("#owl-demo").owlCarousel({
//                    autoPlay: 3000,
                items: 3,
                itemsDesktop: [1199, 3],
                itemsDesktopSmall: [979, 3],
                lazyLoad: true,
                navigation: true
            });

        });
    </script>