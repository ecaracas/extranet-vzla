<?php
/* @var $this EstatusController */
/* @var $model StEstatus */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'st-estatus-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form'
    ),
        ));
?>



<?php //echo $form->errorSummary($model);  ?>

<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'nombre'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'nombre', array('size' => 45, 'maxlength' => 45)); ?>

<?php echo $form->error($model, 'nombre'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'descripcion'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textArea($model, 'descripcion', array('rows' => 6, 'cols' => 50)); ?>

<?php echo $form->error($model, 'descripcion'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'estatus'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php
        $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
            'id' => 'StEstatus_estatus',
            'attribute' => 'StEstatus[estatus]',
            'state' => $model->estatus,
            'type' => 'item',
            'coloron' => 'color1'));
        ?>
<?php echo $form->error($model, 'estatus'); ?>
    </div>
</div>





<br />
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <p class="text-left"><?php echo Yii::t('lang', Yii::app()->params['camposrequeridos']); ?>
        </p>
    </div>
    <div class="col-xs-12 col-sm-6">                
        <ul class="cmenuhorizontal" id="yw0">
            <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang', Yii::app()->params['save-text']), array('class' => Yii::app()->params['save-btn'])); ?>
            </li>
            <li class="bottom"><?php echo CHtml::link(Yii::t('lang', Yii::app()->params['cancel-text']), Yii::app()->controller->createUrl('index'), array('class' => Yii::app()->params['cancel-btn'])); ?>
            </li>
        </ul>
    </div>

</div>

<?php $this->endWidget(); ?>
