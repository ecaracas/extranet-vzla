<?php
/* @var $this OrdenServicioController */
/* @var $model StOrdenServicio */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'st-orden-servicio-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form'
    ),
        ));
?>



<?php //echo $form->errorSummary($model);  ?>

<div class="hidden_fields">
<h4><?php echo  'Ruc Usuario: '; ?>: <label id='rif_usuario'>  </label></h4>
<h4><?php echo  'Marca '; ?>: <label id='marca_maquina'>  </label></h4>
<h4><?php echo  'Modelo '; ?>: <label id='modelo_maquina'> </label></h4>
<br>
</div>




<div class="form-group ">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'serial'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'serial', array('size' => 45, 'maxlength' => 45, 'ajax' => array(
      'type'=>'POST', //request type
      'url'=>$this->createUrl('OrdenServicio/test'),
      'data'=>array('serial'=>'js:this.value'),
      'dataType'=> 'json',
      'success'=>'js:function(data) { '
            . '$("#rif_usuario").html(data["rif"]);'
            . '$("#marca_maquina").html(data["marca"]);'
            . '$("#modelo_maquina").html(data["modelo"]);'
            . '$(".hidden_fields").show();}',
      'error'=> 'js:function (){'
            . '$(".hidden_fields").hide();}',
    ))); ?>

<?php echo $form->error($model, 'serial'); ?>
    </div>

</div>



<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'accesorios'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->dropDownList($model, 'accesorios', CHtml::listData(StAccesorios::model()->findAll('estatus=:estatus', array(':estatus'=>'1')), 'id', 'nombre'),array('multiple'=>true ,'style'=>'width:777px;','size'=>'20','class'=>'js-example-basic-multiple altura' )); ?>

<?php echo $form->error($model, 'accesorios'); ?>
    </div>

</div>

<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'descripcion_falla'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'descripcion_falla', array('size' => 45, 'maxlength' => 45)); ?>

<?php echo $form->error($model, 'descripcion_falla'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'revision_previa'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'revision_previa', array('size' => 45, 'maxlength' => 45)); ?>

<?php echo $form->error($model, 'revision_previa'); ?>
    </div>

</div>

<?php if($rol==2 || $rol==9){?>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'fallas'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->dropDownList($model, 'fallas', CHtml::listData(StFallas::model()->findAll('estatus=:estatus', array(':estatus'=>'1')), 'id', 'nombre_falla'),array('multiple'=>true ,'style'=>'width:777px;','size'=>'20','class'=>'js-example-basic-multiple altura' )); ?>

<?php echo $form->error($model, 'fallas'); ?>
    </div>

</div>
<?php }?>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'contacto'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'contacto', array('size' => 45, 'maxlength' => 45)); ?>

<?php echo $form->error($model, 'contacto'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'telefono'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'telefono', array('size' => 45, 'maxlength' => 11)); ?>

<?php echo $form->error($model, 'telefono'); ?>
    </div>

</div>

<div class="form-group">
    <div class="col-xs-12 col-sm-3">
<?php echo $form->labelEx($model, 'email'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'email', array('size' => 45, 'maxlength' => 45, 'id'=>'telefono_')); ?>

<?php echo $form->error($model, 'email'); ?>
    </div>

</div>



<br />
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <p class="text-left"><?php echo Yii::t('lang', Yii::app()->params['camposrequeridos']); ?>
        </p>
    </div>
    <div class="col-xs-12 col-sm-6">                
        <ul class="cmenuhorizontal" id="yw0">
            <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang', Yii::app()->params['save-text']), array('class' => Yii::app()->params['save-btn'])); ?>
            </li>
            <li class="bottom"><?php echo CHtml::link(Yii::t('lang', Yii::app()->params['cancel-text']), Yii::app()->controller->createUrl('index'), array('class' => Yii::app()->params['cancel-btn'])); ?>
            </li>
        </ul>
    </div>

</div>

<?php $this->endWidget(); ?>
