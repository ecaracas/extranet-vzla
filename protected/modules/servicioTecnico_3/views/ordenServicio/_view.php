<?php
/* @var $this OrdenServicioController */
/* @var $data StOrdenServicio */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id_distribuidor')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->id_distribuidor); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id_tecnico')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->id_tecnico); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id_producto')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->id_producto); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('marca')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->marca); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modelo')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modelo); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('serial')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->serial); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('descripcion_falla')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->descripcion_falla); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('revision_previa')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->revision_previa); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('contacto')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->contacto); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->telefono); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('medio_envio')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->medio_envio); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('medio_recepcion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->medio_recepcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('direccion_recepcion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->direccion_recepcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('email')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->email); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('fecha_registro')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->fecha_registro); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('fecha_enajenacion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->fecha_enajenacion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('garantia')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->garantia); ?></div>
	
</div>

<hr />