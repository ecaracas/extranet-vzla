<?php
/* @var $this OrdenServicioController */
/* @var $model StOrdenServicio */

$this->breadcrumbs = array(
    'St Orden Servicios' => array('index'),
    Yii::app('lang', 'Manage'),
);
Yii::app()->params['IDGridview'] = 'st-orden-servicio-grid';
$this->menu = array(
    array(
        'label' => Yii::t('lang', Yii::app()->params['create-text']),
        'url' => array('create'),
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo Yii::t('lang', Yii::app()->params['index-text']) . ' ' . Yii::t('lang', 'St Orden Servicios'); ?></h3>

                <div class="menu-tool">

                    <div class="pagesize"> 
                        <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
                    </div>         

                    <div class="menu-items">
                        <?php
                        $this->widget('zii.widgets.CMenu', array(
                            'items' => $this->menu,
                            'encodeLabel' => FALSE,
                            'htmlOptions' => array('class' => 'cmenuhorizontal'),
                        ));
                        ?>
                    </div>
                </div>        
                <?php $this->ToolActionsRight(); ?>         

            </div>
            <div class="panel-body"> 

                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => Yii::app()->params['IDGridview'],
                        'dataProvider' => $dataProvider,
                        'filter' => $model,
                        'columns' => array(
                            array('name' => 'distribuidor', 'value' => '$data->idDistribuidor["nombreusuario"]'),
                            array('name' => 'tecnico', 'value' => '$data->idTecnico["nombreusuario"]'),
                            array('name' => 'serial', 'value' => '$data->serial'),
                            array('name' => 'marca', 'value' => '$data->marca'),
                            array('name' => 'modelo', 'value' => '$data->modelo'),
                            array('name' => 'descripcion_falla', 'value' => '$data->descripcion_falla'),
                            array('name' => 'revision_previa', 'value' => '$data->revision_previa'),
                            array('name' => 'contacto', 'value' => '$data->contacto'),
                            array('name' => 'telefono', 'value' => '$data->telefono'),
                            array('name' => 'email', 'value' => '$data->email'),
                            array('name'=>'fecha_enajenacion','value'=>'$data->fecha_enajenacion'),
                            array('name'=>'garantia','value'=>'$data->garantia'),
                            /*

                              array('name'=>'descripcion_falla','value'=>'$data->descripcion_falla'),
                              array('name'=>'revision_previa','value'=>'$data->revision_previa'),
                              array('name'=>'contacto','value'=>'$data->contacto'),
                              array('name'=>'telefono','value'=>'$data->telefono'),
                              array('name'=>'medio_envio','value'=>'$data->medio_envio'),
                              array('name'=>'medio_recepcion','value'=>'$data->medio_recepcion'),
                              array('name'=>'direccion_recepcion','value'=>'$data->direccion_recepcion'),
                              array('name'=>'email','value'=>'$data->email'),
                              array('name'=>'fecha_registro','value'=>'$data->fecha_registro'),
                              
                             
                             */
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang', 'Historia'),
                                'label' => Yii::app()->params['diario-icon'],
                                'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['history-btn']),
                                //'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                                'urlExpression' => 'Yii::app()->createUrl("servicioTecnico/historialordenes/index",array("id_orden"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                            ),
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang', Yii::app()->params['view-text']),
                                'label' => Yii::app()->params['view-icon'],
                                'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                            ),
                            /*array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang', Yii::app()->params['update-text']),
                                'label' => Yii::app()->params['update-icon'],
                                'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
                            ),*/
                            /*array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang', Yii::app()->params['delete-text']),
                                'label' => Yii::app()->params['delete-icon'],
                                'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
                            ),*/
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete', array());    
}
?>