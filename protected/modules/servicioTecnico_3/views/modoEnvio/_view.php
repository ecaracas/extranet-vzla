<?php
/* @var $this ModoEnvioController */
/* @var $data StModoEnvio */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('nombre_envio')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->nombre_envio); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->descripcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->estatus); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('fecha_creacion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->fecha_creacion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('hora_creacion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->hora_creacion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('usuario_crea')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->usuario_crea); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('fecha_modificacion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->fecha_modificacion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('hora_modificacion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->hora_modificacion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('usuario_modifica')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->usuario_modifica); ?></div>
	
</div>

<hr />