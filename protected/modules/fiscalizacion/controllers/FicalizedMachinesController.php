<?php

class FicalizedMachinesController extends Controller {

    /**
     *
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     *      using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public function behaviors() {
        return array(
            'eexcelview' => array(
                'class' => 'application.extensions.eexcelview.EExcelBehavior'));
    }

    /**
     *
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete') // we only allow deletion via POST request
        ;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * 
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array(
                'allow', /* Acciones Permitidas */
                'actions' => array(
                    'index',
                    'view',
                    'create',
                    'update',
                    'delete',
                    'exportpdf',
                    'exportcsv',
                    'exportexcel'),
                'users' => array(
                    '*')),
            array(
                'deny',
                'users' => array(
                    '*')));
    }

    public $modulo = "reporteEnajenacion";

    public function __construct($id, $module = null) {
        parent::__construct($id, $module);

        Yii::app()->params['title'] = '';
    }

    public function actionView($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {

            $model = $this->loadModel($id);

            $this->render('view', array(
                'model' => $model));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionCreate() {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {

            $model = new FicalizedMachines();

            if (isset($_POST['FicalizedMachines'])) {
                $model->attributes = $_POST['FicalizedMachines'];
                if ($model->validate()) {
                    if ($model->save()) {

                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array(
                            'view',
                            'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('create', array(
                'model' => $model));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionUpdate($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {

            $model = $this->loadModel($id);

            if (isset($_POST['FicalizedMachines'])) {
                $model->attributes = $_POST['FicalizedMachines'];

                if ($model->validate()) {
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array(
                            'view',
                            'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('update', array(
                'model' => $model));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionexportcsv() {
        @$desde = Yii::app()->request->cookies['desde']->value;
        @$hasta = Yii::app()->request->cookies['hasta']->value;
        $criteria = new CDbCriteria();
        if ($desde != null) {
            $criteria->condition = "t.date BETWEEN '" . $desde . "' and '" . $hasta . "' ";
        }
        $data = FicalizedMachines::model()->findAll($criteria);
        $xx = array();

        foreach ($data as $dat) {

            $xx[0] = $dat['id'];
            $xx[1] = $dat['final_clients_id'];
            $xx[2] = $dat['date'];
            $xx[3] = $dat['ruc_technician'];
            $xx[4] = $dat['ruc_dealer'];
            $xx[5] = $dat['observations'];
            $xx[6] = $dat['machine_id'];
        }
        // $xx[$a['id']] = $a['id'];
        $hoy = date("Y-m-d");
        $handle = fopen($hoy . '.csv', "a+");
        for ($j = 0; $j <= 6; $j++) {
            fwrite($handle, $xx[$j] . PHP_EOL);
        }
        fclose($handle);
    }

    public function actionexportpdf() {

        // if (isset($_POST['desde'])):
        $mPDF1 = Yii::app()->ePdf->mpdf();
        Yii::app()->theme = "pdf";
        $desde = Yii::app()->request->cookies['desde']->value;
        $hasta = Yii::app()->request->cookies['hasta']->value;
        $hoy = date("Y-m-d");

        $criteria = new CDbCriteria();
        if ($desde != null) {
            $criteria->condition = "t.date BETWEEN '" . $desde . "' and '" . $hasta . "' ";
        } else {
            $desde = '--';
            $hasta = $hoy;
        }
        $model = FicalizedMachines::model()->findAll($criteria);
        $header = ' 
			<table class="logo">
			<tr>
				<td>
				<img width="40%" alt="logo The factory hka" src="http://thefactoryhka.com/extranet-pa/img/logos/logo.jpg">
            									<td>                                          
                                            </tr>
                                        </table>
            							<br />
                                        <table class="demo">                       
                                        <thead>
                                        <tr>
                                                <th>Fecha Actual<br></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                                <td>&nbsp;' . $hoy . '</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        <table class="demo">
                                        <thead>
                                        <tr>
                                                <th>Desde<br></th>
                                                <th>Hasta</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                                <td>&nbsp;' . $desde . '</td>
                                                <td>&nbsp;' . $hasta . '</td>
                                        </tr>
                                        </tbody>
                                </table>         
                                ';
        $mPDF1->SetHTMLHeader($header);

        $mPDF1->WriteHTML($this->render('exportpdf', array(
                    'model' => $model), true));
        $mPDF1->Output();

        // endif;
    }

    public function actionIndex() {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {
            $model = new Operations('search');
            $model->unsetAttributes();
            unset(Yii::app()->request->cookies['desde']);
            unset(Yii::app()->request->cookies['hasta']);



            if (isset($_GET['Operations'])) {

                $model->attributes = $_GET['Operations'];
            }

            if (isset($_POST['FicalizedMachines'])) {

                $model->desde = $_POST['FicalizedMachines']['desde'];
                $model->desde = stripslashes($model->desde);
                $model->hasta = $_POST['FicalizedMachines']['hasta'];
                $model->hasta = stripslashes($model->hasta);
                Yii::app()->request->cookies['desde'] = new CHttpCookie('desde', $model->desde);
                Yii::app()->request->cookies['hasta'] = new CHttpCookie('hasta', $model->hasta);
            }
            $this->render('index', array('model' => $model));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function loadModel($id) {
        $sql = new CDbCriteria();
        $sql->params = array(
            ':id' => intval($id));
        $sql->condition = "id = :id";
        $sql->addcondition("estatus <> 9");
        $model = FicalizedMachines::model()->find($sql);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }
        return $model;
    }

    public function actionExportexcel($id = null) {
        if (isset($_SESSION['promotion.excel'])) {
            $data = $_SESSION['promotion.excel'];
            $hoy = date("Y-m-d H:i:s");
            //var_dump($data); die();
            $this->toExcel($data, array(
                'date',
                'rbacusuario.razon_social::Distribuidor',
                'nombreapellido::Técnico',
                'serial',
                'finalClient.social_reason::Cliente Final',
                'ruc',
                'finalClient.dv::DV',
                'address',
                'final_cliente_phone',
                'email_final_client',
                'install_address',
                'seal'
                    ), $hoy);
        }
    }

}
