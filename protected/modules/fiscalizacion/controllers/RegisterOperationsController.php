<?php

class RegisterOperationsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', /* Acciones Permitidas*/
				'actions'=>array('index','view','create','update','delete','verificartecnico','VerificarRncCliente','serial','IfFiscalizacion','SocialReason'),
				'users'=>array('@'),
			),			
			array('deny',
				'users'=>array('@'),
			),
		);
	}
        
         public $modulo = "";

         
        public function __construct($id, $module = null) {
            
        parent::__construct($id, $module);
        
         Yii::app()->params['title'] = '';
        
        }
         
	public function actionView($id)
	{
         if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {
         
                $model = $this->loadModel($id);

		$this->render('view',array(
			'model'=>$model,
		));
        
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
	}
        
        public function calc_hash($cBuf) {
        $max = 0;
        $i = 0;
        $lAux = 0;
        $lAux2 = 0;
        $lAux3 = 0;
        $max = strlen($cBuf);
        for ($i = 0; $i < $max; $i++) {
            $lAux2 = $cBuf[$i];
            $lAux2 = ord($lAux2);
            $lAux3 = ord($cBuf[$max - $i - 1]);
            $lAux = ($lAux << 3) ^ $lAux2;
            $lAux = ($lAux << 4) ^ $lAux3;
            $lAux = ($lAux % 999999);
        }
        return $lAux;
    }
    
    public function select($type_equipment_id = NULL, $operation_id = NULL){
        /* EQUIPO
         * Impresora fiscal : 1
         * Caja registradora : 2
         * 
         * OPERACION
         * Fiscalizacion : 1
         * Desbloqueo: 2
         */
         $tipo = '';
         if ($type_equipment_id == 2) {
                    if ($operation_id == 1) {
                       $tipo = "FISCALIZACION";
                    } else {
                        $tipo = "DESBLOQUEO";
                    }
                } else {
                    if ($operation_id == 1) {
                        $tipo = "FISCALIZACIONPANAMA20015";
                    } else {
                        $tipo = "DESBLOQUEOPANAMA2015";
                    }
                }
                return $tipo;
    }
    
    public function codend($code_printer = NULL,$serialprinter = NULL,$tipo = NULL,$rncFinal = NULL, $rncTech = NULL,$address = NULL,$precinto_ini = NULL, $precinto_fin = NULL ){
            require_once('lib/nusoap.php');         
            $consumidor = new nusoap_client('http://www.thefactoryhka.com/tf_cw_swfiscal/index.php?wsdl', true);    
            $param = array('precode' => $code_printer,
                    'serial' => $serialprinter,
                    'tipo' => $tipo,
                    'rncFinal' => $rncFinal,
                    'rncTecnico' => $rncTech,
                    'address' => $address,
                    'initPre' => $precinto_ini,
                    'finPre' => $precinto_fin,
                    'user' => null,
                    'clave' => null,
                    'mean' => 'AUXILIARY');
                 
                $codeFin = $consumidor->call('getCode', $param);
                if(strlen($codeFin) == 9 ) {  //Si se obtiene el codigo se establece resultado exitosa de la operacion
		  $param = array('valor' => TRUE, 'serial' => $serialprinter ,  'mean'  => "AUXILIARY");
                  $consumidor->call('setResult',$param);
		}
                  
                $codigo_desbloqueo1 = $code_printer;
                $serial = $serialprinter;
                $lAux = $this->calc_hash(strtoupper($codigo_desbloqueo1));
                $lAux = ($lAux << 3) ^ $this->calc_hash($serial);
                $lAux = ($lAux << 7) ^ $this->calc_hash($tipo);
                $lAux = ($lAux % 999999999);
                $codeFin = (string) $lAux;
                
                while (strlen($codeFin) < 9) {
                    $codeFin = "0" . $codeFin;
                }
                return $codeFin;
    }

	public function actionCreate() {

//        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {

        $model = new RegisterOperations;
        $modelSocialReasons = new SocialReasons();

        if (isset($_POST['RegisterOperations'])) {
            
                   
            $model->attributes = $_POST['RegisterOperations'];

            if ($model->validate()) {
                
                $code_printer = $model->code_printer;
                $serialprinter = $model->serial_printer;               
                $rncFinal =  $model->rnc_client;
                $rncTech =  $model->rnc_tecnical;
                $address = "Calle Gutierez, Caracas.";
                $precinto_ini =  $model->ini_sel;
                $precinto_fin = $model->end_sel;
                
                $type_equipment_id = $model->device_type_id;
                $operation_id = $model->operation_id;
                $tipo = $this->select($type_equipment_id,$operation_id);
                $codeFin = $this->codend($code_printer,$serialprinter,$tipo,$rncFinal,$rncTech,$address,$precinto_ini,$precinto_fin);
                
                $model->created = date("Y-m-d H:i:s");
                $model->user_id = Yii::app()->user->id;
                $model->mode = 'AUXILIAR';
                $model->result = 1;
                $model->status = 0;

                if ($model->save()) {                    
                    $this->machinestatus($serialprinter);
                    Yii::app()->user->setFlash('codigo', $codeFin);
                    $this->redirect(array('create'));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
            }
        }

        $this->render('create', array(
            'model' => $model,
            'modelSocialReasons'=>$modelSocialReasons,
        ));

//        } else {
//            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
//        }
    }
    
    public function machinestatus($serial = NULL){
    
        $modelMachines = Machines::model()->find('serial = "'.$serial.'"');
        $modelMachines->status = 1;
        $modelMachines->update();
    }
       
    
     public function actionSerial($rnc = NULL) {
        $serial = $_POST['rnc'];
        $criteria = new CDbCriteria;
        $criteria->alias = 'Machine';
        $criteria->select = 'Machine.serial';
        $criteria->condition = 'Machine.serial =  "'.$serial.'"';    
        $Machines = Machines::model()->findAll($criteria);    
       
        if ($Machines) {
            $respuesta['valido'] = 1;
        } else {
            $respuesta['valido'] = 0;
        }
        print CJSON::encode($respuesta);
      
    }
    
     public function actionVerificartecnico($rnc = NULL) {
        Yii::import('application.modules.roles.models.RBACUsuarios');
        
        $serial = $_POST['rnc'];
        $criteria = new CDbCriteria;
        $criteria->alias = 'Usuarios';
        $criteria->select = 'Usuarios.nombre,Usuarios.apellido';
        $criteria->condition = 'Usuarios.rol_id = 4 and Usuarios.estatus = 1 AND  Usuarios.dni_numero =  "'.$serial.'"';    
        $RBACUsuarios = RBACUsuarios::model()->find($criteria);
 
       if ($RBACUsuarios) {
            $respuesta['valido'] = 1;  
            $respuesta['usuario'] = $RBACUsuarios;
            $respuesta['usuario'] = $RBACUsuarios;
        } else {
            $respuesta['valido'] = 0;
        }
        print CJSON::encode($respuesta);
    }
    

    public function actionVerificarRncCliente($serial = NULL) {
        $criteria = new CDbCriteria;
        $serial = $_POST['serial'];
        $criteria->alias = 'SocialReasons';
        $criteria->select = 'SocialReasons.name,SocialReasons.social_reason';
        $criteria->condition = 'SocialReasons.active = 1 AND SocialReasons.rnc =  "'.$serial.'"';    
        $SocialReasons = SocialReasons::model()->find($criteria);

        if ($SocialReasons) {

            $respuesta['valido'] = 1;    
            $respuesta['cliente'] = $SocialReasons;
            $respuesta['cliente'] = $SocialReasons;
          
        } else {
            $respuesta['valido'] = 0;
        }

        print CJSON::encode($respuesta);
     
    }
    
     public function actionIfFiscalizacion($rnc = NULL) {  

        $criteria = new CDbCriteria;
        $serial = $_POST['rnc'];
        $criteria->alias = 'Machine';
        $criteria->select = 'Machine.status';
        $criteria->condition = 'Machine.serial =   "'.$serial.'"';    
        $SocialReasons = Machines::model()->findAll($criteria);
        foreach ($SocialReasons as $option):
               if ($option['status'] == 1) {
                   $respuesta['valido'] = 1;
               } else {
                  $respuesta['valido'] = 0;
               }
       endforeach;
       print CJSON::encode($respuesta);
        
    }
    
     public function actionSocialReason(){
       
       $model = new SocialReasons(); 
       
           
           $SocialReasonName = $_POST['SocialReasonName'];
           $SocialReasonSocialReason = $_POST['SocialReasonSocialReason'];     
           $SocialReasonRnc = $_POST['SocialReasonRnc'];
           $SocialReasonPhone = $_POST['SocialReasonPhone'];
           $SocialReasonAddress = $_POST['SocialReasonAddress'];
           
           $model->user_id =  Yii::app()->user->id;
           $model->name = $SocialReasonName;
           $model->social_reason = $SocialReasonSocialReason;
           $model->rnc =  $SocialReasonRnc;
           $model->phone = $SocialReasonPhone;
           $model->address = $SocialReasonAddress;         
           $model->active = 1;         
           $model->save();
               
           
      
        
    }

   

    public function actionIndex() {

//        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {
         
            $model = new RegisterOperations('search');

            $model->unsetAttributes();  // clear any default values

            if (isset($_GET['RegisterOperations'])) {
                $model->attributes = $_GET['RegisterOperations'];
            }

            if (isset($_GET['RegisterOperations_sort'])) {

                Yii::app()->params['GridViewOrder'] = TRUE;
            }

            $this->render('index', array(
                'model' => $model,
            ));
//        } else {
//            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
//        }
    }

    public function loadModel($id)
	{		
                $sql = new CDbCriteria;
                $sql->params = array(':id' => intval($id));
                $sql->condition = "id = :id";
                $sql->addcondition("estatus <> 9");
                $model = RegisterOperations::model()->find($sql);
		if($model===null){
			throw new CHttpException(404,Yii::app()->params['Error404']);
                }
		return $model;
	}

}
