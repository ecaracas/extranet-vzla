<?php
class OperationsController extends Controller
{
	
	public $modulo = "operations";
	/**
	 *
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 *      using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';
	
	/**
	 *
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete') // we only allow deletion via POST request
;
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * 
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array(
						'allow', /* Acciones Permitidas*/
				'actions' => array(
								'index',
								'view',
								'create',
								'update',
								'delete',
								'tecnicos',
								'calculadvruc'),
						'users' => array(
								'*')),
				array(
						'deny',
						'users' => array(
								'*')));
	}
	
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		
		Yii::app()->params['title'] = '';
	}
	
	public function actionView($id)
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_view'))
		{
			$model = $this->loadModel($id);
			$this->render('view', array('model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	/**
	 * METODO para registrar la enajenacion manualmente.
	 * @throws CHttpException
	 */
	public function actionCreate()
	{
		////////////////////////////////////////////////////
		//BLOQUE PARA LA CARGA MASIVA DE ENAJENACIONES
		////////////////////////////////////////////////////
		if(Yii::app()->request->isAjaxRequest)
		{
			$header = '';
			$dataArray = array();
			$estatus = 'procesado';
			$rutaResponse = '';
			$nombre_archivo = $_FILES['archivo']['name'];
			$tipo_archivo = $_FILES['archivo']['type'];
			$tamano_archivo = $_FILES['archivo']['size'];
			$filename = $_FILES['archivo']['tmp_name'];
			$handle = fopen($filename, "r");
			$i = 0;
			$path = dirname(__FILE__) . '/../../../../downloads/cargaMasivaEnajenaciones/';
			
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
			{
				if($i > 0)
				{
					$model = new Operations();
					$modelFicalized = new FicalizedMachines();
					$modelFinalClient = new FinalClient();
					$modelMachine = new Machines();
					$transaction = Yii::app()->db->beginTransaction();
					$model->serial = $data[0];
					$model->dealer_id = RBACUsuarios::model()->find('dni_numero=:registro', array('registro' => $data[1]))->id;
					$model->techinician_id = RBACUsuarios::model()->find('dni_numero=:registro', array('registro' => $data[2]))->id;
					$model->date = date('Y-m-d',strtotime($data[12]));
					$model->operation_type_id = 1;
					$model->address = strtolower($data[6]);
					$model->install_address = strtolower($data[7]);
					$model->observation = strtolower($data[11]);
					$model->business_name = strtolower($data[5]);
					$model->final_cliente_phone = strtolower($data[8]);
					$model->email_final_client = strtolower($data[9]);
					$model->seal = strtolower($data[10]);
					$model->ruc = $data[3];
					$model->dv = $data[4];
					$model->created_at = date('Y-m-d');
					$modelFinalClient->ruc = $model->ruc;
					$modelFinalClient->dv = $model->dv;
					$modelFinalClient->name = $model->business_name;
					$modelFinalClient->social_reason = $model->business_name;
					$modelFinalClient->address = $model->address;
					$modelFinalClient->created = date('Y-m-d h:m:s');
					$modelFinalClient->modified = date('Y-m-d h:m:s');
					$modelFinalClient->active = 1;
					if($model->validate())
					{
						$validateFinalClient = $modelFinalClient->find('ruc=:rucFinal', array(':rucFinal' => trim($modelFinalClient->ruc)));
						if(is_null($validateFinalClient))
						{
							$modelFinalClient->save();
							$model->final_client_id = $modelFinalClient->id;
						}
						else
						{
							$model->final_client_id = $validateFinalClient->id;
						}
						$maquina = $modelMachine->model()->find("serial = '" . $model->serial . "'");
						$modelFicalized->final_clients_id = $model->final_client_id;
						$modelFicalized->date = date('Y-m-d h:m:s');
						$modelFicalized->ruc_technician = RBACUsuarios::model()->find('id=:id', array('id' => $model->techinician_id))->dni_numero;
						$modelFicalized->ruc_dealer = RBACUsuarios::model()->find('id=:id', array('id' => $model->dealer_id))->dni_numero;
						$modelFicalized->observations = $model->observation;
						$modelFicalized->machine_id = $maquina->id;
						if($modelFicalized->validate() && $modelFicalized->save())
						{
							$model->save();
							$transaction->commit();
							$archivo = fopen($path . 'enajenacionMasiva_' . date("d m Y") . '_' . $estatus . '.csv', "a");
							fwrite($archivo, $data[0] . ',' . $data[1] . ','. $data[2] . ','. $data[3] . ','. $data[4] . ','. $data[5] . ','. $data[6] . ','. $data[7] . ','. $data[8] . ','. $data[9] . ','. $data[10] . ','. $data[11] . ','. $data[12] . ',' . 'PROCESADA' . "\n");
							fclose($archivo);
							$rutaResponse = 'enajenacionMasiva_' . date("d m Y") . '_' . $estatus . '.csv';
						}
						else
						{
							$transaction->rollBack();
							$estatus = 'FALLIDO';
							$archivo = fopen($path . 'enajenacionMasiva_' . date("d m Y") . '_' . $estatus . '.csv', "a");
							fwrite($archivo, $data[0] . ',' . $data[1] . ','. $data[2] . ','. $data[3] . ','. $data[4] . ','. $data[5] . ','. $data[6] . ','. $data[7] . ','. $data[8] . ','. $data[9] . ','. $data[10] . ','. $data[11] . ','. $data[12] . ',' . implode(',',array_column($modelFicalized->getErrors(), 0)) . "\n");
							fclose($archivo);
							$rutaResponse = 'enajenacionMasiva_' . date("d m Y") . '_' . $estatus . '.csv';
						}
						
					}
					else
					{
						$transaction->rollBack();
						$estatus = 'FALLIDO';
						$archivo = fopen($path . 'enajenacionMasiva_' . date("d m Y") . '_' . $estatus . '.csv', "a");
						fwrite($archivo, $data[0] . ',' . $data[1] . ','. $data[2] . ','. $data[3] . ','. $data[4] . ','. $data[5] . ','. $data[6] . ','. $data[7] . ','. $data[8] . ','. $data[9] . ','. $data[10] . ','. $data[11] . ','. $data[12] . ',' . implode(',',array_column($model->getErrors(), 0)) . "\n");
						fclose($archivo);
						$rutaResponse = 'enajenacionMasiva_' . date("d m Y") . '_' . $estatus . '.csv';
						
					}
				}
				else
				{
					$archivo = fopen($path . 'enajenacionMasiva_' . date("d m Y") . '_' . $estatus . '.csv', "a");
					fwrite($archivo, $data[0] . ',' . $data[1] . ','. $data[2] . ','. $data[3] . ','. $data[4] . ','. $data[5] . ','. $data[6] . ','. $data[7] . ','. $data[8] . ','. $data[9] . ','. $data[10] . ','. $data[11] . ','. $data[12] . ',' . 'estatus_proceso' . "\n");
					fclose($archivo);
				}
				$i++;
			}
			echo json_encode(array('succes' => $estatus, 'path' => 'downloads/cargaMasivaEnajenaciones/' . $rutaResponse));
			Yii::app()->end();
			//$return = Array('ok' => FALSE, 'msg' => "Ocurrio un error al subir el archivo. No pudo guardarse.", 'status' => 'error');
		}
		//////////////////////////////////////////////////////
		//BLOQUE PARA LA CARGA INDIVIDUAL DE ENAJENACIONES
		//////////////////////////////////////////////////////
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_create'))
		{
			$model = new Operations();
			$modelFicalized = new FicalizedMachines();
			$modelFinalClient = new FinalClient();
			$modelMachine = new Machines();

			// print_r($_POST['Operations']);die;
			if(isset($_POST['Operations']))
			{
				$model->attributes = $_POST['Operations'];

				// print_r($_POST['Operations']);die;

				$date = new DateTime($model->date);
				$model->date = $date->format('Y-m-d');
				if(Yii::app()->user->getState('rol') != 2 && Yii::app()->user->getState('rol') != 4)
				{
					if(Yii::app()->user->getState('rol') != 3)
					$model->techinician_id = Yii::app()->user->getState('id');
					$model->dealer_id = RBACUsuarios::model()->find('id=:id', array('id' => Yii::app()->user->getState('id')))->distribuidor_id;
				}
				$model->operation_type_id = 1;
				$model->address = strtolower($model->address);
				$model->install_address = $model->install_address == '' ? strtolower($model->address) : strtolower($model->install_address);
				$model->observation = strtolower($model->observation);
				$model->created_at = date('Y-m-d H:m:s');
				$modelFinalClient->ruc = $model->ruc;
				$modelFinalClient->dv = $model->dv;
				$modelFinalClient->name = $model->business_name;
				$modelFinalClient->phone = $model->final_cliente_phone;
				$modelFinalClient->social_reason = $model->business_name;
				$modelFinalClient->address = $model->address;
				$modelFinalClient->created = date('Y-m-d h:m:s');
				$modelFinalClient->modified = date('Y-m-d h:m:s');
				$modelFinalClient->active = 1;
                              
				//VERIFICA EL DIGITO VALIDADOR DEL RUC INGRESADO
				//SE DESACTIVA LA VALIDACION DEL DV POR PETICION DE LUIS CABRERA.
				if(true)//Yii::app()->validaDV->calculateDV($_POST['Operations']['ruc']) == $_POST['Operations']['dv'])
				{
					if($model->validate())
					{
						$validateFinalClient = $modelFinalClient->find('ruc=:rucFinal', array(':rucFinal' => trim($modelFinalClient->ruc)));
						if(is_null($validateFinalClient))
						{
							if($modelFinalClient->validate()){
                                                       
                                                        $modelFinalClient->save();
							$model->final_client_id = $modelFinalClient->id;
                                                        
                                                        }else{
                                                          Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);  
                                                            
                                                        }
						}
						else
						{
							$model->final_client_id = $validateFinalClient->id;
                                                        
						}
						
                                               
						if($model->save())
						{
							$maquina = $modelMachine->model()->find("serial = '" . $model->serial . "'");
							$modelFicalized->final_clients_id = $model->final_client_id;
							$modelFicalized->date = $model->date;
							$modelFicalized->ruc_technician = RBACUsuarios::model()->find('id=:id', array('id' => $model->techinician_id))->dni_numero;
							$modelFicalized->ruc_dealer = RBACUsuarios::model()->find('id=:id', array('id' => $model->dealer_id))->dni_numero;
							$modelFicalized->observations = $model->observation;
							$modelFicalized->machine_id = $maquina->id;
							$modelFicalized->created_at = date('Y-m-d h:m:s');
							if($modelFicalized->save())
							{
								$nombreUser = RBACUsuarios::model()->find('id=:id', array('id' => $model->techinician_id));
								//CORREOS INTERNOS QUE RECIBIRAN LA NOTIFICACION DE LAS ENAJENACIONES HECHAS
								$this->envioEmail(Yii::app()->params['correosInternos'], 'El usuario ' . $nombreUser->nombre . ' ' . $nombreUser->apellido . ' con RUC:' . $nombreUser->dni_numero . '.<br/> Realizó la enajenación del equipo con serial: ' . $model->serial. ' para el cliente final con RUC: ' . $model->ruc . '.<br/>', 'The Factory Corp', 'The Factory Corp, Enajenación serial: ' . $model->serial);
								//CORREO DE CONFIRMACION DE REGISTRO ENVIADO AL USUARIO QUE EJECUTA LA ENAJENACIÓN.
								$this->envioEmail(Yii::app()->user->getState('emailUser'), 'La enajenación para el serial: ' . $model->serial . ' fue exitosa. <br/>Cliente enajenado es: ' . $modelFinalClient->name . '<br/>RUC: ' . $model->ruc . '<br/>' . 'Mantenga esta notificación como respaldo de su operación realizada.<br/>', 'The Factory Corp', 'The Factory Corp, Enajenación serial: ' . $model->serial);
								//CORREO DE CONFIRMACION DE REGISTRO ENVIADO AL DISTRIBUIDOR AL CUAL ESTA ASOCIADO EL TECNICO QUE EJECUTA LA ENAJENACIÓN.
								$this->envioEmail(RBACUsuarios::model()->find('id=:id', array('id' => $model->dealer_id))->email, 'El técnico ' . $nombreUser->nombre . ' ' . $nombreUser->apellido . ' con RUC:' . $nombreUser->dni_numero . '.<br/> Realizó la enajenación para el serial: ' . $model->serial . ' exitosamente. <br/>Cliente enajenado es: ' . $modelFinalClient->name . '<br/>RUC: ' . $model->ruc . '<br/>' . 'Mantenga esta notificación como respaldo de la operación realizada.<br/>', 'The Factory Corp', 'The Factory Corp, Enajenación serial: ' . $model->serial);
								Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
								$this->redirect(array('view', 'id' => $model->id));
							}
						}
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
					$model->addError('ruc', 'El dígito de verificación es invalido!');
			}
			$this->render('create', array('model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionUpdate($id)
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_update'))
		{
			
			$model = $this->loadModel($id);
			
			if(isset($_POST['Operations']))
			{
				$model->attributes = $_POST['Operations'];
				
				if($model->validate())
				{
					if($model->save())
					{
						Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model->id));
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
				{
					Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
				}
			}
			
			$this->render('update', array(
					'model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionIndex()
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_index'))
		{
			$model = new Operations('search');
                      
			$model->unsetAttributes(); // clear any default values
			if(isset($_GET['Operations']))
			{
				$model->attributes = $_GET['Operations'];
			}
			
			if(isset($_GET['Operations_sort']))
			{
				
				Yii::app()->params['GridViewOrder'] = TRUE;
			}
			
			$this->render('index', array('model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function loadModel($id)
	{
		$sql = new CDbCriteria();
		$sql->params = array(
				':id' => intval($id));
		$sql->condition = "id = :id";
		$model = Operations::model()->find($sql);
		if($model === null)
		{
			throw new CHttpException(404, Yii::app()->params['Error404']);
		}
		return $model;
	}

	public function actionTecnicos()
	{
		if(isset($_POST['cod']))
		{
			$tecnicos = CHtml::listData(RBACUsuarios::model()->findAll('distribuidor_id = :id', array(':id' => $_POST['cod'])), 'id', 'nombreusuario');
			if(sizeof($tecnicos) > 0)
				echo json_encode($tecnicos);
			else
			{
				echo json_encode(array(''));
			}
		}
	}
	
	public function actionCalculaDVRUC(){
		$valor = isset($_POST['dv']) ? $_POST['dv'] : '';
		$dv = isset($_POST['dv']) ? Yii::app()->validaDV->calculateDV($_POST['dv']) : '';
		$this->render('calculaDV', array('dv' => $dv, 'valor' => $valor));
	}


		public function autocompletadoSerial()
		    {

		        return array(
		          'aclist'=>array(
		            'class'=>'application.extensions.EAutoCompleteAction',
		            'model'=>'serial', //My model's class name
		            'attribute'=>'Serial', //The attribute of the model i will search
		                ),
		            );
		    }
}
