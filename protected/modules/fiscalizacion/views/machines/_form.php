<?php $form=$this->beginWidget('CActiveForm', array('id'=>'machines-form','enableAjaxValidation'=>false,'htmlOptions'=>array('class'=>'form-horizontal','role'=>'form'),)); ?>

        <?php //echo $form->errorSummary($model); ?>

           <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'serial'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'serial',array('size'=>6,'maxlength'=>6)); ?>
    
                     <?php echo $form->error($model,'serial'); ?>
                 </div>
                      
            </div>
                      
                        
            <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'social_reason_id'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'social_reason_id'); ?>
    
                     <?php echo $form->error($model,'social_reason_id'); ?>
                 </div>
                      
            </div>
                        
                        
            <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'observation'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textArea($model,'observation',array('rows'=>6, 'cols'=>50)); ?>
    
                     <?php echo $form->error($model,'observation'); ?>
                 </div>
                      
            </div>
           
                         
                    <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                        <p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>
</p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?>
</li>
                    <li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?>
</li>
                </ul>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>
