<?php
/* @var $this MachinesController */
/* @var $data Machines */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('serial')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->serial); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->user_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('user_associated')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->user_associated); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('social_reason_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->social_reason_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date_fiscal')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date_fiscal); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('observation')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->observation); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('status')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->status); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('created')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->created); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modified); ?></div>
	
</div>

<hr />