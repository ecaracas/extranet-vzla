<?php
/* @var $this SocialReasonsController */
/* @var $model SocialReasons */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'social-reasons-form',
  'enableAjaxValidation'=>false,
     'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form'
    ),
)); ?>



        <?php //echo $form->errorSummary($model); ?>

                  
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'name'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>200)); ?>
    
                     <?php echo $form->error($model,'name'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'social_reason'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'social_reason',array('size'=>60,'maxlength'=>100)); ?>
    
                     <?php echo $form->error($model,'social_reason'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'rnc'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'rnc',array('size'=>10,'maxlength'=>10)); ?>
    
                     <?php echo $form->error($model,'rnc'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'phone'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'phone',array('size'=>13,'maxlength'=>13)); ?>
    
                     <?php echo $form->error($model,'phone'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'address'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50)); ?>
    
                     <?php echo $form->error($model,'address'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'active'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->dropDownList($model,'active',Active::getListActive(),array('empty'=>'Seleccione')); ?>
    
                     <?php echo $form->error($model,'active'); ?>
                 </div>
                      
            </div>
                         
                         
        <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                        <p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>
</p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?>
</li>
                    <li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?>
</li>
                </ul>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>
