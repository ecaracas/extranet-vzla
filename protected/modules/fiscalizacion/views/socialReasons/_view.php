<?php
/* @var $this SocialReasonsController */
/* @var $data SocialReasons */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->user_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->name); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('social_reason')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->social_reason); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('rnc')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->rnc); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->phone); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('address')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->address); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('active')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->active); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('created')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->created); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modified); ?></div>
	
</div>

<hr />