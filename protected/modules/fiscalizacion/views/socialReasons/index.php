<?php
/* @var $this SocialReasonsController */
/* @var $model SocialReasons */

$this->breadcrumbs=array(
    'Social Reasons'=>array('index'),
    Yii::app('lang','Manage'),
);
Yii::app()->params['IDGridview'] = 'social-reasons-grid';
$this->menu=array(     
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
);

?>
<div class="row">
    <div class="col-xs-12">
<div class="panel panel-default">
    <div class="panel-heading navbar-tool">
        
        <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['index-text']) .' '. Yii::t('lang','Social Reasons'); ?></h3>
        
       <div class="menu-tool">
            
            <div class="pagesize"> 
             <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
             </div>         
            
            <div class="menu-items">
                 <?php 
        $this->widget('zii.widgets.CMenu', array(
            'items' => $this->menu,
            'encodeLabel' => FALSE,
            'htmlOptions' => array('class' => 'cmenuhorizontal'),
        ));
        ?>
            </div>
        </div>        
        
        
    </div>
    <div class="panel-body"> 

<div class="table-responsive">
<?php echo CHtml::link(Yii::t('lang', 'Nuevo cliente final'), array('socialReasons/create'), array('class' => 'btn btn-primary')); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>Yii::app()->params['IDGridview'],
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'columns'=>array(
//		 array('name'=>'id','value'=>'$data->id'),
//		 array('name'=>'user_id','value'=>'$data->user_id'),
		 array('name'=>'name','value'=>'$data->name'),
		 array('name'=>'social_reason','value'=>'$data->social_reason'),
		 array('name'=>'rnc','value'=>'$data->rnc'),
		 array('name'=>'phone','value'=>'$data->phone'),
		/*
		 array('name'=>'address','value'=>'$data->address'),
		 array('name'=>'active','value'=>'$data->active'),
		 array('name'=>'created','value'=>'$data->created'),
		 array('name'=>'modified','value'=>'$data->modified'),
		*/
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['view-text']),            
            'label' => Yii::app()->params['view-icon'],
            'linkHtmlOptions' => array('class' => 'view '.Yii::app()->params['view-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))', 
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
        ),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['update-text']),            
            'label' => Yii::app()->params['update-icon'],
            'linkHtmlOptions' => array('class' => 'edit '.Yii::app()->params['update-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
        ),
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['delete-text']),           
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
        ),
    ),
)); ?>
</div>
    </div>
    </div>
 </div>
     </div>
<?php 
 
    
    if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete', array());    

    } 
?>