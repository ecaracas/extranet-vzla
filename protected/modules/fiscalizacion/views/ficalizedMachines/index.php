<?php
/* @var $this FicalizedMachinesController */
/* @var $model FicalizedMachines */

$this->breadcrumbs = array('Operations' => array('index'), Yii::app('lang', 'Manage'),);
Yii::app()->params['IDGridview'] = 'operations-grid';
$this->menu = array(
    array(
        'label' => Yii::t('lang', Yii::app()->params['create-text']),
        'url' => array('create'),
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
);
?>


<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">
                <h3 class="panel-title"><?php echo Yii::t('lang', 'Listado de Enajenaciones.'); ?></h3>
                <div class="menu-tool">
                    <div class="pagesize"> 
                        <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
                    </div>
                    <div class="menu-items">
                        <?php $this->widget('zii.widgets.CMenu', array('items' => $this->menu, 'encodeLabel' => FALSE, 'htmlOptions' => array('class' => 'cmenuhorizontal'),)); ?>
                    </div>
                </div>
                <?php $this->ToolActionsRight(); ?>
                <br />
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => Yii::app()->params['IDGridview'],
                        'dataProvider' => $dataProvider,
                        'filter' => $model,
                        'afterAjaxUpdate' => 'reinstallDatePicker',
                        'columns' => array(
//                            array('name' => 'created_at', 'type' => 'raw', 'value' => 'Yii::app()->dateFormatter->format("y-MM-dd",strtotime($data->created_at))',),
                            array('name' => 'created_at', 'type' => 'raw', 'value' => 'Yii::app()->dateFormatter->format("y-MM-dd",strtotime($data->created_at))', 'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model, 'attribute' => 'created_at', 'language' => 'es',
                                    'htmlOptions' => array(
                                        'id' => 'datepicker_for_due_date',
                                        'size' => '10',
                                    ),
                                    'options' => array('dateFormat' => 'yy-mm-dd'), 'defaultOptions' => array(
                                        'showOn' => 'focus',
                                        'dateFormat' => 'yy-mm-dd',
                                        'showOtherMonths' => true,
                                        'selectOtherMonths' => true,
                                        'changeMonth' => true,
                                        'changeYear' => true,
                                        'language' => 'es'
//                    'showButtonPanel' => true,
                                    )), true)),
                            //array('name'=>'created_at','type'=>'raw','value'=>'$data->date', 'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array('model'=>$model,'attribute'=>'date'),true)),
                            array('name' => 'dealer_b', 'type' => 'raw', 'value' => '$data->rbacusuario->razon_social'),
                            array('name' => 'techinician', 'type' => 'raw', 'value' => '$data->rbactecnico->nombre . " " . $data->rbactecnico->apellido'),
                            array('name' => 'serial', 'type' => 'raw', 'value' => '$data->serial'),
                            array('name' => 'final_client', 'type' => 'raw', 'value' => '$data->finalClient->social_reason'),
                            array('name' => 'ruc_concat', 'type' => 'raw', 'value' => '$data->ruc . " dv " . $data->dv'),
                            array('name' => 'address', 'type' => 'raw', 'value' => '$data->address'),
                            array('name' => 'install_address', 'type' => 'raw', 'value' => '$data->install_address'),
                            array('name' => 'seal', 'type' => 'raw', 'value' => '$data->seal'),
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang', Yii::app()->params['view-text']),
                                'label' => Yii::app()->params['view-icon'],
                                'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                            ),
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang', Yii::app()->params['update-text']),
                                'label' => Yii::app()->params['update-icon'],
                                'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
                            ),
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang', Yii::app()->params['delete-text']),
                                'label' => Yii::app()->params['delete-icon'],
                                'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
                            ),
                        ),
                    ));
                    ?>
                </div>
                <?php $form = $this->beginWidget('CActiveForm', array('id' => 'operations-form', 'action' => Yii::app()->createUrl('/fiscalizacion/FicalizedMachines/exportexcel'), 'enableAjaxValidation' => false, 'htmlOptions' => array('target' => '_blank', 'class' => 'form-horizontal', 'role' => 'form'),)); ?>
                <?php echo CHtml::submitButton(Yii::t('lang', 'Exportar en excel'), array('class' => 'btn btn-primary')); ?>                           
                <?php $this->endWidget(); ?>
                <?php //$form=$this->beginWidget('CActiveForm', array('id'=>'ficalized-machines-form','action'=>Yii::app()->createUrl('/fiscalizacion/FicalizedMachines/exportcsv'),'enableAjaxValidation'=>false,'htmlOptions'=>array('target'=>'_blank','class'=>'form-horizontal','role'=>'form'),)); ?>
                <?php //echo CHtml::submitButton(Yii::t('lang','exportar en CSV'),array('class'=>'btn btn-primary')); ?>
                <?php //$this->endWidget();  ?> 
            </div>
        </div>
    </div>
</div>
<?php



if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete', array());    
}
?>