<?php
/* @var $this FicalizedMachinesController */
/* @var $data FicalizedMachines */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('final_clients_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->final_clients_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('ruc_technician')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->ruc_technician); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('ruc_dealer')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->ruc_dealer); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('observations')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->observations); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('machine_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->machine_id); ?></div>
	
</div>

<hr />