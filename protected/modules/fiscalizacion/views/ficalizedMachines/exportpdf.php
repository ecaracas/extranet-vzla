<style>

.demo {
		width:100%;
		height:100%;
		border:1px solid #C0C0C0;
		border-collapse:collapse;
		padding:2px;
	}
	.demo caption {
		text-align:center !important;
	}
	.demo th {
		border:1px solid #C0C0C0;
		padding:2px;
		background:#F0F0F0;
        color :#556B8D;
	}
	.demo td {           
		border:1px solid #C0C0C0;
		text-align:center !important;
		padding:2px;
	}
	.logo {
		width:100%;
		height:100%;
		border-collapse:collapse;
		padding:2px;
	}	
	.logo th {
		padding:2px;        
	}
	.logo td {  
		text-align:center !important;	
	}
</style>
<br />
<br />
<br />
<br />
<br /><br /><br /><br /><br />

<table class="demo">
	<caption><br></caption>
	<thead>
	<tr>
		<th>Numero de Serial<br></th>
		<th>Distribuidor<br></th>
		<th>Técnico<br></th>
		<th>Fecha Fiscalización<br></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($model as $modelFiscalized):?>
	<tr>
		<td>&nbsp;<?php echo $modelFiscalized['machine']['serial'];?></td>
		<td>&nbsp;<?php echo FicalizedMachines::ruc($modelFiscalized['ruc_dealer']);?></td>
		<td>&nbsp;<?php echo FicalizedMachines::ruc($modelFiscalized['ruc_technician']);?></td>
		<td>&nbsp;<?php echo $modelFiscalized['date'];?></td>
	</tr>
	<?php endforeach; ?>
	</tbody>
</table>

