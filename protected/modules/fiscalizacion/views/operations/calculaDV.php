<style>
.cmenuhorizontal li.bottom{
	float: none !important;
}
</style>
<div class="row">
	<div class="col-xs-12 col-md-10 col-lg-12">
		<h1>Calculo del dígito verificador del RUC.</h1>
	</div>
</div>
<hr>
<form action="<?php echo Yii::app()->request->baseUrl; ?>/fiscalizacion/operations/calculadvruc" method="post">
	<div class="row">
		<div class="col-xs-12 col-md-10 col-lg-12">
			<input type="text" value="<?php echo $valor;?>" name="dv" id="dv">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-10 col-lg-12">
			<ul class="cmenuhorizontal" id="yw0" style="text-align: center">
				<li class="bottom"><?php echo CHtml::submitButton('Calcular'); ?></li>
			</ul>
		</div>
	</div>
</form>
<?php if($dv != ''):?>
<hr>
<div class="row">
	<div class="col-xs-12 col-md-10 col-lg-12" style="text-align: center">
		<h1><?php echo 'El dígito validador es: ' . $dv;?></h1>
	</div>
</div>
<?php endif;?>