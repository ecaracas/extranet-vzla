<?php
/* @var $this OperationsController */
/* @var $model Operations */

$this->breadcrumbs=array(
    'Operations'=>array('index'),
    Yii::app('lang','Manage'),
);
Yii::app()->params['IDGridview'] = 'operations-grid';
$this->menu=array(     
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
);

?>
<div class="row">
    <div class="col-xs-12">
<div class="panel panel-default">
    <div class="panel-heading navbar-tool">
        
        <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['index-text']) .' '. Yii::t('lang','Operacion'); ?></h3>
        
       <div class="menu-tool">
            
            <div class="pagesize"> 
             <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
             </div>         
            
            <div class="menu-items">
                 <?php $this->widget('zii.widgets.CMenu', array('items' => $this->menu,'encodeLabel' => FALSE,'htmlOptions' => array('class' => 'cmenuhorizontal'),)); ?>
            </div>
        </div>        
        <?php $this->ToolActionsRight(); ?>         
        
    </div>
    <div class="panel-body"> 

<div class="table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>Yii::app()->params['IDGridview'],
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'columns'=>array(
//		 array('name'=>'id','value'=>'$data->id'),
		 array('name'=>'serial','value'=>'$data->serial'),
		 array('name'=>'date','value'=>'$data->date'),
		 array('name'=>'nombreusu_b','type'=>'raw','value'=>'CHtml::link($data->rbacusuario->nombreusuario, array("../roles/usuarios/view/id/".$data->rbacusuario->id),array("target"=>"_blank"))'),
		 array('name'=>'nombreusu_t','type'=>'raw','value'=>'CHtml::link($data->rbactecnico->nombreusuario, array("../roles/usuarios/view/id/".$data->techinician_id),array("target"=>"_blank"))'),
		 array('name'=>'operation_type_id','value'=>'$data->operationType->name'),
		/*
		 array('name'=>'final_client_id','value'=>'$data->final_client_id'),
		 array('name'=>'observation','value'=>'$data->observation'),
		 array('name'=>'ruc','value'=>'$data->ruc'),
		 array('name'=>'business_name','value'=>'$data->business_name'),
		 array('name'=>'address','value'=>'$data->address'),
		*/
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['view-text']),            
            'label' => Yii::app()->params['view-icon'],
            'linkHtmlOptions' => array('class' => 'view '.Yii::app()->params['view-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))', 
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
        ),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['update-text']),            
            'label' => Yii::app()->params['update-icon'],
            'linkHtmlOptions' => array('class' => 'edit '.Yii::app()->params['update-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
        ),
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['delete-text']),           
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
        ),
    ),
)); ?>
</div>
    </div>
    </div>
 </div>
     </div>
<?php 
 
    
    if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete', array());    

    } 
?>