<?php
/* @var $this OperationsController */
/* @var $data Operations */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('serial')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->serial); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('dealer_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->dealer_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('techinician_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->techinician_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('operation_type_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->operation_type_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('final_client_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->final_client_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('observation')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->observation); ?></div>
	
</div>

	<div class='row'>
		<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('ruc')); ?></div>
		<div class='col-xs-11'><?php echo CHtml::encode($data->dv); ?>-<?php echo CHtml::encode($data->ruc); ?> </div>	
	</div>

	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('business_name')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->business_name); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('address')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->address); ?></div>
	
</div>

<hr />