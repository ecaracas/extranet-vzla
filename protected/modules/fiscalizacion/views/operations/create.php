<?php
/* @var $this OperationsController */
/* @var $model Operations */
$this->breadcrumbs = array(
		'Operations' => array(
				'index'),
		'Nuevo');

?>
<div class="row">
	<div class="col-xs-12 col-md-10 col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading navbar-tool">
				<h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['create-text']) .' '. Yii::t('lang','Enajenación'); ?></h3>
				<div class="menu-tool">
<?php
$this->widget('zii.widgets.CMenu', array(
		'items' => $this->menu,
		'encodeLabel' => FALSE,
		'htmlOptions' => array(
				'class' => 'cmenuhorizontal')));
?>
				</div>
				<br/>
				<a href="<?php echo Yii::app()->baseUrl ?>/fiscalizacion/operations/index" type="button" id="myButton" class="btn btn-primary" autocomplete="off">Lista de Enajenaciones</a>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">CARGA MASIVA</button>
				<?php $this->ToolActionsRight(); ?>
			</div>
			
			<div class="panel-body">
				
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Carga masiva de enajenaciones</h4>
      </div>
      <div class="modal-body">
        <form enctype="multipart/form-data" id="formuploadajax" method="post">
            <input  type="file" id="enajenaciones" name="enajenaciones"/>
            <br/>
      <div id="mensaje"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <input class="btn btn-primary" type="submit" value="Subir"/>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
$( document ).ready(function() {
	$("#formuploadajax").on("submit", function(e){
        e.preventDefault();
        var f = $(this);
        var inputFileImage = document.getElementById("enajenaciones");
        var formData = new FormData();
        var file = inputFileImage.files[0];
        $('#mensaje').append('<img id="imgCarga" src="<?php echo Yii::app()->request->baseUrl ?>/img/cargando.gif">');
        formData.append('archivo',file);
        $.ajax({
            url: "/extranet-pa/fiscalizacion/operations/create",
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function(res){
                $('#mensaje').empty();
	            if(res.succes == 'procesado')
	            {
		            $('#mensaje').html('Su archivo se cargo correctamente <a href="../../' + res.path  + '"><strong>click aqui</strong></a> y descarga archivo procesado!!!');
	            }
	            if(res.succes == 'FALLIDO')
	            {
	            	$('#mensaje').html('La carga del archivo fallo haga <a href="../../' + res.path  + '"><strong>click aqui</strong></a> y descargue para verificar los errores!!!');
		        }
            });
    });
});
</script>