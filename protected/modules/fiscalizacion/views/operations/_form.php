<style>
.error{
	color:red;
}

#Operations_serial{
	text-transform: uppercase;
}
</style>
<?php
/* @var $this OperationsController */
/* @var $model Operations */
/* @var $form CActiveForm */
?>
<?php

$form = $this->beginWidget('CActiveForm', array(
		'id' => 'operations-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
				'class' => 'form-horizontal',
				'role' => 'form')));
?>

<div class="row">
	<div class="col-xs-12 col-sm-9">
		<strong>
			<h1 style="font-size: 2em">Datos de la operación fiscal.</h1>
		</strong>
	</div>
</div>
<hr>
<?php //echo $form->errorSummary($model); ?>
<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'operation_type_id'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->dropDownList($model,'operation_type_id', CHtml::listData(OperationTypes::model()->findAll(), 'id', 'name'), array('style' => 'height:42px')); ?>
		<?php echo $form->error($model,'operation_type_id'); ?>
	</div>
</div>
<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'date'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->textField($model,'date',array('size'=>10,'maxlength'=>10, 'style' => 'width:30%', 'value' => date('d-m-Y'))); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>
</div>


<!-- ------------------ Seleccionar el serial que este no enajenado --------------------------------- -->
<div class="form-group">

	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'serial'); ?>
	</div>

	<div class="col-xs-12 col-sm-9">

		<!-- para poder seleccionar los seriales con busqueda de autocompletado -->

		 <?php echo $form->textField($model,'serial',array('size'=>45,'maxlength'=>13)); ?> 

		<?php echo $form->error($model,'serial'); ?>
	</div>
</div>
<!-- ----------------------------------------------------------- -->



<?php if(Yii::app()->user->getState('rol') == 2 || Yii::app()->user->getState('rol') == 4):?>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'dealer_id'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->dropDownList($model,'dealer_id', CHtml::listData(RBACUsuarios::model()->findAll('rol_id = 3 '), 'id', 'nombreusuario'), array('empty' => 'Seleccione','style' => 'height:42px')); ?>
			<?php echo $form->error($model,'dealer_id'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'techinician_id'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->dropDownList($model,'techinician_id', array(''), array('style' => 'height:42px')); ?>
			<?php echo $form->error($model,'techinician_id'); ?>
		</div>
	</div>
<?php else:?>
	<?php if(Yii::app()->user->getState('rol') == 3):?>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'techinician_id'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->dropDownList($model,'techinician_id', array(''), array('style' => 'height:42px')); ?>
			<?php echo $form->error($model,'techinician_id'); ?>
		</div>
	</div>
	<?php else:?>
		<?php echo $form->textField($model,'dealer_id',array('size'=>45,'maxlength'=>45, 'style' => 'display:none')); ?>
		<?php echo $form->textField($model,'techinician_id',array('size'=>45,'maxlength'=>45, 'style' => 'display:none')); ?>
	<?php endif;?>
<?php endif;?>
<br/>

<div class="row"><div class="col-xs-12 col-sm-9"><strong><h1 style="font-size: 2em">Datos del cliente final</h1></strong></div></div>
<hr>
		<div class="form-group">
			<div class="col-xs-12 col-sm-3">
				<?php echo $form->labelEx($model,'ruc'); ?>
			</div>

			<div class="col-xs-12 col-sm-9">



				<div style="float:left; width: 5%">
					<?php echo $form->dropDownList($model,'dv',array('0'=>'J','1'=>'E','2'=>'V','3'=>'G','4'=>'P')); ?> 
				</div>

				<div style="float:left; width: 2%; margin-top: 1%; margin-left: 1%">-</div>
			
				<div style="float:left; width: 40%">

					<?php echo $form->textField($model,'ruc',array('size'=>20,'maxlength'=>12, 'style' => 'width:100%', 'autocomplete' => 'on')); ?>

				</div>

		

				<div style="float:left; clear: both; margin: 1.2%; font-family: "Open Sans", Helvetica, Arial, sans-serif;">
					<?php echo $form->error($model,'ruc'); ?>
					<?php echo $form->error($model,'dv'); ?> 

				</div>
				<div style="float:left; clear: both; margin: 1.2%; font-family: "Open Sans", Helvetica, Arial, sans-serif;">
					<strong>El Rif debe tener el siguiente formato : J-123456781</strong></div>
		


				</div>
</div>
<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'business_name'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->textField($model,'business_name',array('size'=>60,'maxlength'=>100,'required')); ?>
		<?php echo $form->error($model,'business_name'); ?>
	</div>
</div>
<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'address'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->textField($model,'address',array('size'=>50,'maxlength'=>200)); ?>
		&nbsp;<strong>Dirección fiscal es la misma que la de instalación</strong>
		<?php echo CHtml::checkBox('Operations[checkAddress]', false, array('style' => 'float:left')); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>
</div>

<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'install_address'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->textField($model,'install_address',array('size'=>200,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'install_address'); ?>
	</div>
</div>

<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'final_cliente_phone'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->textField($model,'final_cliente_phone',array('size'=>200,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'final_cliente_phone'); ?>
	</div>
</div>

<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'email_final_client'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->textField($model,'email_final_client',array('size'=>200,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'email_final_client'); ?>
	</div>
</div>

<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'seal'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->textField($model,'seal',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'seal'); ?>
	</div>
</div>

<div class="form-group">
	<div class="col-xs-12 col-sm-3">
		<?php echo $form->labelEx($model,'observation'); ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<?php echo $form->textArea($model,'observation',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'observation'); ?>
	</div>
</div>
<br />
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>
</p>
	</div>
	<div class="col-xs-12 col-sm-6">
		<ul class="cmenuhorizontal" id="yw0">
			<li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?></li>
			<li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?><li>
		</ul>
	</div>
</div>
<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->getState('rol') == 2 || Yii::app()->user->getState('rol') == 4 || Yii::app()->user->getState('rol') == 3):?>
<script>
$(document).ready(function(){
	$('#Operations_dealer_id').change(function(){
		cargaAjax($('#Operations_dealer_id').val());
	});
	<?php if($model->dealer_id != null):?>
		cargaAjax($('#Operations_dealer_id').val());
	<?php elseif(Yii::app()->user->getState('rol') == 3):?>
		cargaAjax(<?php echo Yii::app()->user->getState('id');?>);
	<?php endif;?>
});
function cargaAjax(idDist)
{
	var dist = idDist ? idDist : '';
	$.ajax({
		type: "POST",
		url : "<?php echo Yii::app()->request->baseUrl; ?>/fiscalizacion/operations/tecnicos",
		cache : false,
		data: {cod : dist,YII_CSRF_TOKEN:'<?php echo Yii::app()->request->csrfToken ?>'},
		dataType: 'json',
		success: function(response){
			if(typeof response.falso == "undefined")
			{
				$('#Operations_techinician_id').empty();
				$.each(response, function(id, nombre){
					$('#Operations_techinician_id').append('<option value="' + id + '">' + nombre + '</option>');
				})
			}			
		}
	})
}


</script>
<?php endif;?>
<script type="text/javascript">
$( document ).ready(function() {
	
	$(function() {
	    $('input[name="Operations[date]"]').daterangepicker({
	        singleDatePicker: true,
	        showDropdowns: true,
	        format: 'DD-MM-YYYY'
	    }, 
	    function(start, end, label) {
	        var years = moment().diff(start, 'years');
	    });
	});
	$.validator.addMethod("onlytextnumber",function(value,element)
			{
			return this.optional(element) || /^[ 0-9a-z]+$/i.test(value);
			},"Sólo se aceptan letras y números.");
	$.validator.addMethod("onlytext",function(value,element)
			{
			return this.optional(element) || /^[ a-zA-Z]+$/.test(value);
			},"Sólo se aceptan letras.");
	$.validator.addMethod("onlynumbers",function(value,element)
			{
			return this.optional(element) || /^[0-9]*$/.test(value);
			},"Sólo se aceptan números.");
	$('#operations-form').validate({
		debug: false,
		rules: {
			"Operations[serial]": {
				required: true,
				minlength: 10,
				maxlength: 13,
				onlytextnumber:true
			},
			"Operations[date]": {
				required: true,
			},
			"Operations[ruc]": {
				required: true,
				maxlength: 12,
			},

			"Operations[dealer_id]":{
				required: true,
			},
			"Operations[techinician_id]":{
				required:true,
			},
			"Operations[business_name]":{
				onlytextnumber:true
			}
		},
		messages: {
			"Operations[serial]": {
				required: "Serial es obligatorio.",
				minlength: "Debe contener al menos  10 dígitos alfanuméricos.",
				maxlength: "Debe contener maximo 13 dígitos alfanuméricos.",
			},
			"Operations[date]": {
				required: "Fecha de enajenación es obligatoria.",
			},
			"Operations[dealer_id]": {
				required: "Debe indicar el distribuidor obligatoriamente.",
			},
			"Operations[ruc]": {
				required: "El RIF es obligatorio.",
				maxlength: "Debe contener 10 dígitos alfanuméricos.",
			}
		}
	});

	$( "#Operations_checkAddress" ).click(function() {
		if($('#' + this.id).is(':checked'))
		{
			$('#Operations_install_address').val($('#Operations_address').val());
		}
		else
			$('#Operations_install_address').val('');
	});
	
});
</script>
