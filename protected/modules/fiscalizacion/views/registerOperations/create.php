<div class="row">
    <div class="col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

            <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['create-text']) .' '. Yii::t('lang','RegisterOperations'); ?></h3>
            <div class="menu-tool">
            <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
            ?>
            </div>
              <?php $this->ToolActionsRight(); ?>           

            </div>
            <div class="panel-body">

            <?php $this->renderPartial('_form', array('model'=>$model,'modelSocialReasons'=>$modelSocialReasons,)); ?>
            </div>
        </div>
    </div>
</div>