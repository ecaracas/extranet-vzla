<script>
    $(document).ready(function () {
        $(".generate").hide();
        $(".precintos").hide();
        $(".validaciones").hide();
        $(".reason_social").hide();
        $('#RegisterOperations_ini_sel').attr('maxlength', 6);
        $('#RegisterOperations_end_sel').attr('maxlength', 6);
        mayuscula("#RegisterOperations_code_printer");
    });

    function showall() {
        $(".rnc_client").toggle();
        $(".serial_printer").toggle();
        $(".generate").toggle();
        $(".precintos").toggle();
        $(".reason_social").toggle();
    }

    function mayuscula(campo) {
        $(campo).keyup(function () {
            $(this).val($(this).val().toUpperCase());
        });
    }
    
    function checkAll(){
        
        verificar_rnc_tecnico();
        verificar_rnc_cliente();
        verificar_serial();
    }

    function equipo() {
      
        var nombre_equipo = $('#RegisterOperations_device_type_id option:selected').val();

        if (nombre_equipo == 2) {
            /*cash register**/
            $('#RegisterOperations_ini_sel').attr('maxlength', 6);
            $('#RegisterOperations_end_sel').attr('maxlength', 6);

            $("#divnumber").removeClass('hidden');
            $("#divletter").addClass('hidden');
            $(".validaciones").show();
            


            $("#divletter .text").removeClass('required');
            $("#divnumber .text").addClass('required');


            $(".cpnumber").attr('name', 'data[RegisterOperation][code_printer]');
            $(".cpletter").removeAttr('name');
            $(".cpnumber").val('');
            $(".cpletter").val('');

            $(".cpnumber").attr('required', 'required');
            $(".cpletter").removeAttr('required');


        } else if (nombre_equipo == 1) {
            /*fiscal printer**/
            $('#RegisterOperations_ini_sel').attr('maxlength', 7);
            $('#RegisterOperations_end_sel').attr('maxlength', 7);

            $("#divnumber").addClass('hidden');
            $("#divletter").removeClass('hidden');
            $(".validaciones").show();


            $("#divnumber .text").removeClass('required');
            $("#divletter .text").addClass('required');

            $(".cpletter").attr('name', 'data[RegisterOperation][code_printer]');
            $(".cpnumber").removeAttr('name');

            $(".cpnumber").val('');
            $(".cpletter").val('');

            $(".cpletter").attr('required', 'required');
            $(".cpnumber").removeAttr('required');
        }

    }

    function verificar_rnc_tecnico() {

        var rnc = $('#RegisterOperations_rnc_tecnical').val();

        if (rnc >= 1000000) {
            $("#verrortecnical").html('');
            $.ajax({
                url: 'http://<?php echo $_SERVER['HTTP_HOST'] .Yii::app()->baseUrl; ?>/fiscalizacion/registerOperations/Verificartecnico/',
                data: 'rnc=' + rnc,           
                type: 'POST',
                dataType: "html",              
                beforeSend: function () {
                    $(".cargando_rnc_tecnical").html("<i class='fa fa-spinner fa-spin'></i>");
                },  
                complete: function () {
                    $(".cargando_rnc_tecnical").html("");
                },
                error: function () {
                    alert("Found an error. Connection problems");
                },
                success: function (data) {
                var respuesta = eval('(' + data + ')');

                   if (respuesta.valido == 1) {
                        $("#verificar_rnc_tecnico").html('Se ha encontrado el tecnico '+respuesta.usuario.nombre+' '+respuesta.usuario.apellido);                       

                    } else {
                        $("#verificar_rnc_tecnico").html('');                        
                        $("#verrortecnical").html('No ha sido encontrado');
                    }
                }

            });
        } else {

            $("#verrortecnical").html('El Ruc debe de tener al menos 10 caracteres');
        }
    }
    function verificar_rnc_cliente() {

        var serial = $('#RegisterOperations_rnc_client').val();
        if (serial >= 1000000) {
            $("#verrorclient").html('');

            $.ajax({
                url: 'http://<?php echo $_SERVER['HTTP_HOST'] .Yii::app()->baseUrl; ?>/fiscalizacion/registerOperations/VerificarRncCliente/',
                data: 'serial=' + serial,           
                type: 'POST',
                dataType: "html",                     
                beforeSend: function () {
                    $(".cargando_rnc_client").html("<i class='fa fa-spinner fa-spin'></i>");
                },
                complete: function () {
                    $(".cargando_rnc_client").html("");
                },
                error: function () {
                    alert("Found an error. Connection problems");
                },
                success: function (data) {
                var respuesta = eval('(' + data + ')');

                    if (respuesta.valido == 1) {
                        $("#verificar_rnc_cliente").html("Se ha encontrado la razon social "+respuesta.cliente.social_reason);
                        $(".precintos").show();
                        $(".generate").show();
                        $(".reason_social").hide();

                    } else {
                        $("#verificar_rnc_cliente").html('No existe razon social con este RUC. Por favor registre la nueva razon social');
                        $('#modal_new_reason_social').modal('show');
                        $("#SocialReasonRnc").val(serial);
                    }


                }

            });

        } else {

            $("#verrorclient").html('CRIB minimum length is 9 digits and numeric');
        }
    }
    function verificar_serial() {

        var rnc = $('#RegisterOperations_serial_printer').val();

        if (rnc.length == 13) {
            $("#verrorprintserial").html('');
            $.ajax({
                url: 'http://<?php echo $_SERVER['HTTP_HOST'] .Yii::app()->baseUrl; ?>/fiscalizacion/registerOperations/serial/',
                data: 'rnc=' + rnc,           
                type: 'POST',
                dataType: "html",
                beforeSend: function () {
                    $(".cargando_verificar_serial").html("<i class='fa fa-spinner fa-spin'></i>");
                },
//                complete: function () {
//                    $(".cargando_verificar_serial").html("");
//                },
                error: function () {
                    alert("Found an error. Connection problems");
                },
                success: function (data) {
                    var respuesta = eval('(' + data + ')');   
                    if (respuesta.valido == 1) {                        
                        $("#verificar_serial").html('Se ha encontrado el serial del equipo');
                        if_fiscalizacion();
                    } else {
                        $("#verificar_rnc_cliente").html('');
                        $("#verrorprintserial").html('No found');
                    }

                }

            });

        } else {

            $("#verrorprintserial").html('El serial deberia de tener 13 digitos');
        }

    }

    function if_fiscalizacion() {
        var rnc = $('#RegisterOperations_serial_printer').val();
        $.ajax({
            url: 'http://<?php echo $_SERVER['HTTP_HOST'] .Yii::app()->baseUrl; ?>/fiscalizacion/RegisterOperations/IfFiscalizacion/',
            data: 'rnc=' + rnc,           
            type: 'POST',
            dataType: "html",
            beforeSend: function () {
                $(".cargando_verificar_serial").html("<i class='fa fa-refresh'></i>");
            },
            complete: function () {
                $(".cargando_verificar_serial").html("");
            },
            error: function () {
                alert("Found an error. Connection problems");
            },
            success: function (data) {
                var respuesta = eval('(' + data + ')');
                if (respuesta.valido == 1) {
                    $("#verificar_fiscalizacion").html("El equipo ya esta fiscalizado");
                } else
                {
                    $("#verificar_fiscalizacion").html("El equipo aun no se ha fiscalizado");

                }

            }

        });

    }

    function verificar_reason() {
        SocialReasonName = $("#SocialReasons_name").val();
        SocialReasonSocialReason = $("#SocialReasons_social_reason").val();
        SocialReasonRnc = $("#SocialReasons_rnc").val();
        SocialReasonPhone = $("#SocialReasons_phone").val();
        SocialReasonAddress = $("#SocialReasons_address").val();


        $.ajax({
            url: 'http://<?php echo $_SERVER['HTTP_HOST'].Yii::app()->baseUrl; ?>/fiscalizacion/RegisterOperations/SocialReason/',                 
            type: 'POST',
            dataType: "html",
            data: 'SocialReasonName='+SocialReasonName+'&SocialReasonSocialReason='+SocialReasonSocialReason+'&SocialReasonRnc='+SocialReasonRnc+'&SocialReasonPhone='+SocialReasonPhone+'&SocialReasonAddress='+SocialReasonAddress,
            beforeSend: function () {
                $(".message-save").html("<i class='fa fa-refresh'></i>");
            },
            complete: function () {
                $(".message-save").html("");
            },
            error: function () {
                alert("Found an error. Connection problems");
            },
            success: function (data) {
                $('#modal_new_reason_social').modal('hide');
                $(".generate").show();
                $(".precintos").show();
            }
        });
    }


</script>
<div style="padding: 20px;" class="form">
    <?php $form = $this->beginWidget('CActiveForm', array('id' => 'register-operations-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),)); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->errorSummary($modelSocialReasons); ?>
     <?php echo CHtml::link('Registro de operaciones', array('registerOperations/index'),array('class'=>'btn btn-primary')); ?>    
    <?php if (Yii::app()->user->hasFlash('codigo')): ?>
        <div class="alert alert-success alert-dismissable"><strong> <?php echo Yii::app()->user->getFlash('codigo'); ?></strong></div>          
    <?php endif; ?>    
    
    <div class="panel panel-primary">
        <div class="panel-heading"><i class="fa fa-cogs"></i> Equipo / Operación</div>
        <div class="panel-body">
            <div class="row">
                <?php echo $form->labelEx($model, 'device_type_id'); ?>
                <?php echo $form->dropDownList($model, 'device_type_id', CHtml::listData(DeviceTypes::model()->findAll('active = 1'), 'id', 'name'), array('onclick'=>'equipo()','class' => 'chosen-select', 'prompt' => 'Seleccione el tipo de equipo')); ?>                  
                <?php echo $form->error($model, 'device_type_id'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'operation_id'); ?>
                <?php echo $form->dropDownList($model, 'operation_id', CHtml::listData(Operations::model()->findAll('active = 1'), 'id', 'name'), array('onclick'=>'equipo()','class' => 'chosen-select', 'prompt' => 'Seleccione el tipo de operacion')); ?>                  
                <?php echo $form->error($model, 'operation_id'); ?>
            </div>
        </div>
    </div>

    <div class="validaciones panel panel-primary">
        <div class="panel-heading"><i class="fa fa-unlock"></i> <?php echo Yii::t('lang','validations'); ?></div>
        <div class="panel-body">
            <div class="row">
                <?php echo $form->labelEx($model, 'serial_printer'); ?>
                <?php //echo $form->textField($model, 'serial_printer', array('size' => 13, 'maxlength' => 13)); ?>
                <?php echo $form->dropDownList($model,'serial_printer',array('AABBCC9999999'=>'AABBCC9999999','AABBCC9999998'=>'AABBCC9999998','AABBCC9999997'=>'AABBCC99999997','AABBCC9999998'=>'AABBCC9999998','AABBCC9999997'=>'AABBCC9999997','AABBCC9999996'=>'AABBCC9999996','AABBCC9999995'=>'AABBCC9999995','AABBCC9999994'=>'AABBCC9999994','AABBCC9999993'=>'AABBCC9999993','AABBCC9999992'=>'AABBCC9999992','AABBCC9999991'=>'AABBCC9999991','AABBCC9999990'=>'AABBCC9999990'),array('empty'=>'Seleccione serial de la impresora')); ?>
                <?php echo $form->error($model, 'serial_printer'); ?>
                <div id="verrorprintserial" class="label label-danger"></div>
                <i  class="label label-info" id="verificar_serial"></i><br />
                <i  class="label label-info" id="verificar_fiscalizacion"></i>
                <i class="cargando_verificar_serial"></i>            
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'rnc_tecnical'); ?>
                <?php echo $form->textField($model, 'rnc_tecnical', array('size' => 10, 'maxlength' => 10)); ?>
                <?php echo $form->error($model, 'rnc_tecnical'); ?>
                <i id="verrortecnical" class="label label-danger"></i>
                <i  class="label label-info" id="verificar_rnc_tecnico"></i>
                <i class="cargando_rnc_tecnical"></i>     
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'rnc_client'); ?>
                <?php echo $form->textField($model, 'rnc_client', array('size' => 10, 'maxlength' => 10)); ?>
                <?php echo $form->error($model, 'rnc_client'); ?>
                <i class="label label-info" id="verificar_rnc_cliente"></i>
                <i class="cargando_rnc_client"></i>  
            </div>
            
        </div>
        <button type="button" class="btn btn-success btn-block"  onclick="checkAll();">Validar</button>
    </div>
    
    <div class=" precintos panel panel-primary">
        <div class="panel-heading"><i class="fa fa-tags"></i> <?php echo Yii::t('lang','Serial safety'); ?></div>
        <div class="panel-body">
            <div class="row">
                <?php echo $form->labelEx($model, 'ini_sel'); ?>
                <?php echo $form->textField($model, 'ini_sel', array('size' => 10, 'maxlength' => 10)); ?>
                <?php echo $form->error($model, 'ini_sel'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'end_sel'); ?>
                <?php echo $form->textField($model, 'end_sel', array('size' => 10, 'maxlength' => 10)); ?>
                <?php echo $form->error($model, 'end_sel'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model, 'code_printer'); ?>
                <?php echo $form->textField($model, 'code_printer', array('size' => 45, 'maxlength' => 45)); ?>
                <?php echo $form->error($model, 'code_printer'); ?>
            </div>
        </div>
    </div>
  <div class="generate btn-group">        
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Generar Codigo' : 'Actualizar ',array('class'=>Yii::app()->params['save-btn'])); ?>
    </div>
    <br />    
           <!-- Modal -->
           <div class="col-md-12">       
                        <div class="modal fade" id="modal_new_reason_social" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"> <?php echo Yii::t('lang','Business name'); ?></h4>
                              </div>
                              <div class="modal-body">
                                  <div>
                                     <?php echo $form->labelEx($modelSocialReasons,'name'); ?>
                                     <?php echo $form->textField($modelSocialReasons,'name',array('size'=>60,'maxlength'=>200)); ?>    
                                     <?php echo $form->error($modelSocialReasons,'name'); ?>
                                  </div>
                                  <div>
                                       <?php echo $form->labelEx($modelSocialReasons,'social_reason'); ?>
                                       <?php echo $form->textField($modelSocialReasons,'social_reason',array('size'=>60,'maxlength'=>100)); ?>    
                                       <?php echo $form->error($modelSocialReasons,'social_reason'); ?>
                                  </div>
                                  <div>
                                      <?php echo $form->labelEx($modelSocialReasons,'rnc'); ?>
                                      <?php echo $form->textField($modelSocialReasons,'rnc',array('size'=>10,'maxlength'=>10)); ?>    
                                      <?php echo $form->error($modelSocialReasons,'rnc'); ?>
                                  </div>
                                  <div>
                                       <?php echo $form->labelEx($modelSocialReasons,'phone'); ?>
                                       <?php echo $form->textField($modelSocialReasons,'phone',array('size'=>13,'maxlength'=>13)); ?>    
                                       <?php echo $form->error($modelSocialReasons,'phone'); ?>
                                  </div>
                                  <div>
                                       <?php echo $form->labelEx($modelSocialReasons,'address'); ?>
                                       <?php echo $form->textArea($modelSocialReasons,'address',array('rows'=>6, 'cols'=>50)); ?>    
                                       <?php echo $form->error($modelSocialReasons,'address'); ?>
                                  </div>
                                <div class="message-save text-info"></div>                  
                              </div>
                              <div class="modal-footer">
                                  <div class="btn-group ">                                      
                                      <button type="button" class="btn btn-success"  onclick="verificar_reason();"><?php echo Yii::t('lang','save'); ?></button> 
                                  </div>

                              </div>
                            </div>
                          </div>
                        </div>
                </div>
    <?php $this->endWidget(); ?>
</div>