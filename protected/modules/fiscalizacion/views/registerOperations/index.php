<?php
Yii::app()->params['IDGridview'] = 'register-operations-grid';
$this->menu=array(     
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
);

?>
<div class="row">
    <div class="col-xs-12">
<div class="panel panel-default">
    <div class="panel-heading navbar-tool">
        
        <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['index-text']) .' '. Yii::t('lang','Register Operations'); ?></h3>
        
       <div class="menu-tool">
            
            <div class="pagesize"> 
             <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
             </div>         
            
            <div class="menu-items">
                <?php 
                        $this->widget('zii.widgets.CMenu', array(
                            'items' => $this->menu,
                            'encodeLabel' => FALSE,
                            'htmlOptions' => array('class' => 'cmenuhorizontal'),
                        ));
                ?>
            </div>
        </div>  
         <?php echo CHtml::link('Generar Codigo', array('registerOperations/create'),array('class'=>'btn btn-primary')); ?>
        <?php $this->ToolActionsRight(); ?>         
        
    </div>
    <div class="panel-body"> 

<div class="table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>Yii::app()->params['IDGridview'],
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'columns'=>array(		
		 //array('name'=>'user_id','value'=>'$data->user->nombreusuario'),
		 array('name'=>'device_type_id','value'=>'$data->deviceType->name'),
		 array('name'=>'operation_id','value'=>'$data->operation->name'),
		 array('name'=>'rnc_tecnical','value'=>'$data->rnc_tecnical'),
		 array('name'=>'rnc_client','value'=>'$data->rnc_client'),		
		 array('name'=>'serial_printer','value'=>'$data->serial_printer'),
		 array('name'=>'mode','value'=>'$data->mode'),
//		 array('name'=>'address_fiscal','value'=>'$data->address_fiscal'),		
		 array('name'=>'code_printer','value'=>'$data->code_printer'),
		 array('name'=>'created','value'=>'$data->created'),
//		 array('name'=>'modified','value'=>'$data->modified'),
		 array('name'=>'result','value'=>'$data->result'),
//		 array('name'=>'observation','value'=>'$data->observation'),
//		 array('name'=>'status','value'=>'$data->status'),
		
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['view-text']),            
            'label' => Yii::app()->params['view-icon'],
            'linkHtmlOptions' => array('class' => 'view '.Yii::app()->params['view-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))', 
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
        ),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['update-text']),            
            'label' => Yii::app()->params['update-icon'],
            'linkHtmlOptions' => array('class' => 'edit '.Yii::app()->params['update-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
        ),
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['delete-text']),           
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
        ),
    ),
)); ?>
</div>
    </div>
    </div>
 </div>
     </div>
<?php if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {$this->ModalConfirmDelete(Yii::app()->params['IDGridview']);  } 
?>