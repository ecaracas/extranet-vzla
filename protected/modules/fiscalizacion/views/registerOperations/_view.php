<?php
/* @var $this RegisterOperationsController */
/* @var $data RegisterOperations */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->user_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('device_type_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->device_type_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('operation_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->operation_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('rnc_tecnical')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->rnc_tecnical); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('rnc_client')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->rnc_client); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('serial_printer')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->serial_printer); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('mode')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->mode); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('address_fiscal')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->address_fiscal); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('ini_sel')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->ini_sel); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('end_sel')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->end_sel); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('code_printer')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->code_printer); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('created')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->created); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modified); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('result')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->result); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('observation')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->observation); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('status')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->status); ?></div>
	
</div>

<hr />