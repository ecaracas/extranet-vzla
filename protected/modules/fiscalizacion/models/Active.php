<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class Active extends CFormModel
{
	public function rules()
	{

		return array();
	
	}
	
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{

		return array();
	
	}
	
	/* Metodos Estatico CheckBox */
	public static function checkSwicth($visible, $name, $id = NULL, $class = NULL)
	{

		if($visible == 1 or $visible == 0)
		{
			
			if($visible)
			{
				$value = ' checked';
			}
			else
			{
				$value = '';
			}
			if($id == NULL)
			{
				$id = $name;
			}
			
			// $id = 'item' . $id;
			
			// $data = '<div class="switch"><input class="cmn-toggle cmn-toggle-round allbox" id="' . $id . '" type="checkbox" value="1" ' . $value . ' name="' . $id . '"><label for="' . $id . '"></label></div>';
			
			$data = '<div class="onoffswitch">
					<input type="checkbox" name="' . $name . '" class="onoffswitch-checkbox ' . $class . '" id="' . $id . '" ' . $value . '>
					<label class="onoffswitch-label" for="' . $id . '">
					<span class="onoffswitch-inner">
					<span class="onoffswitch-active"><span class="onoffswitch-switch">Si</span></span>
					<span class="onoffswitch-inactive"><span class="onoffswitch-switch">NO</span></span>
					</span>
					</label>
					</div> ';
			
			return $data;
		}
		elseif($visible == 2)
		{

			return $visible;}
	
	}
	public static function checkSwicthAll($visible, $id, $class = NULL)
	{

		if($visible == 1 or $visible == 0)
		{
			if($visible)
			{
				$value = ' checked';
			}
			else
			{
				$value = '';
			}
			
			$data = '<div class="onoffswitch">
            <input type="checkbox" name="' . $id . '" class="onoffswitch-checkbox" id="' . $id . '" ' . $value . '>
            <label class="onoffswitch-label" for="' . $id . '">
                <span class="onoffswitch-inner"></span>
                <span class="onoffswitch-switch"></span>
            </label>
            </div>';
			return $data;
		}
		else
		{
			return 'Eliminado';
		}
	
	}
	public static function getListActive()
	{

		return array(
				1 => 'Activo',
				0 => 'Inactivo' );
	
	}
	public static function checkSwicthLabel($visible, $id = NULL)
	{

		if($visible == 1)
		{
			
			$label = '<span class="label label-success"> Activo</span>';
		}
		elseif($visible == 0)
		{
			
			$label = '<span class="label label-danger"> Inactivo</span>';
		}
		elseif($visible == 9)
		{
			
			$label = '<span class="label-del"><i class="glyph-icon flaticon-information19"></i> Eliminado</span>';
		}
		else
		{
			$label = '<span class="btn btn-default btn-xs">' . $visible . '</span>';
		}
		
		return $label;
	
	}
	public static function checkSwicthDocument($estatus)
	{

		if($estatus == 1)
		{
			$label = '<span class="label label-success"><i class="fa fa-check"></i> Verificado</span>';
			// $label = '<img alt="Verificado" title="Verificado" src="'. Yii::app()->baseUrl .'/img/check.gif" height="20" width="20">';
			/*
			 * . ''
			 * . '<span class="label label-success"><i class="fa fa-check-circle"></i> Documento Verificado</span>';
			 */
		}
		elseif($estatus == 0)
		{
			
			$label = '<span class="label label-default"><i class="fa  fa-info-circle"></i> Pendiente por verificar</span>';
		}
		elseif($estatus == 2)
		{
			
			$label = '<span class="label label-danger"><i class="fa fa-times-circle"></i> Documento no válido</span>';
		}
		else
		{
			$label = '<span class="btn btn-default btn-xs">' . $estatus . '</span>';
		}
		
		return $label;
	
	}
	public static function formatoFecha($fecha)
	{

		if($fecha != null)
		{
			$resultado = date('d/m/Y', strtotime($fecha));
		}
		else
		{
			$resultado = '01/01/1969';
		}
		return $resultado;
	
	}
	public static function formatoHora($hora)
	{

		if($hora != null)
		{
			$resultado = date('g:i:s A', strtotime($hora));
		}
		else
		{
			$resultado = '00:00:00';
		}
		return $resultado;
	
	}

}
