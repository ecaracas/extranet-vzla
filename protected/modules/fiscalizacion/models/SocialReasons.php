<?php

/**
 * This is the model class for table "social_reasons".
 *
 * The followings are the available columns in table 'social_reasons':
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $social_reason
 * @property string $rnc
 * @property string $phone
 * @property string $address
 * @property integer $active
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property RbacUsuarios $user
 */
class SocialReasons extends CActiveRecord
{
	
	public function tableName()
	{
		return 'social_reasons';
	}

	
	public function rules()
	{
		
		return array(
			array('name, social_reason, rnc, phone, address', 'required'),
			array('user_id, active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>200),
			array('social_reason', 'length', 'max'=>100),
			array('rnc', 'length', 'max'=>10),
			array('phone', 'length', 'max'=>13),
			array('modified','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
                        array('created,modified','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
		
			array('id, user_id, name, social_reason, rnc, phone, address, active, created, modified', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			 'machines' => array(self::HAS_MANY, 'Machines', 'social_reason_id'),
                         'user' => array(self::BELONGS_TO, 'RbacUsuarios', 'user_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'user_id' => Yii::t('lang','User'),
			'name' => Yii::t('lang','Nombre del cliente final'),
			'social_reason' => Yii::t('lang','Cliente final'),
			'rnc' => Yii::t('lang','RUC'),
			'phone' => Yii::t('lang','Teléfono'),
			'address' => Yii::t('lang','Dirección'),
			'active' => Yii::t('lang','Activo'),
			'created' => Yii::t('lang','Created'),
			'modified' => Yii::t('lang','Modified'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('social_reason',$this->social_reason,true);
		$criteria->compare('rnc',$this->rnc,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
//		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
