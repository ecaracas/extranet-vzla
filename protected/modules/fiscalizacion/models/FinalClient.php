<?php

/**
 * This is the model class for table "final_clients".
 *
 * The followings are the available columns in table 'final_clients':
 * @property integer $id
 * @property string $ruc
 * @property string $name
 * @property string $social_reason
 * @property string $phone
 * @property string $address
 * @property string $created
 * @property string $modified
 * @property integer $active
 */
class FinalClient extends CActiveRecord
{
	
	public function tableName()
	{
		return 'final_clients';
	}

	
	public function rules()
	{
		
		return array(
			array('ruc, name, social_reason, address, active', 'required'),
			array('active, dv', 'numerical', 'integerOnly'=>true),
			array('ruc', 'length', 'max'=>20),
			array('name', 'length', 'max'=>200),
			array('social_reason', 'length', 'max'=>100),
			array('created, modified', 'safe'),
			array('id, name, social_reason, phone, address, created, modified, active', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'fiscalizedMachines' => array(self::HAS_MANY, 'FiscalizedMachines', 'final_clients_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'ruc' => Yii::t('lang','Ruc'),
			'dv' => Yii::t('lang','DV'),
			'name' => Yii::t('lang','Name'),
			'social_reason' => Yii::t('lang','Razon Social Cliente Final'),
			'phone' => Yii::t('lang','Phone'),
			'address' => Yii::t('lang','Address'),
			'created' => Yii::t('lang','Created'),
			'modified' => Yii::t('lang','Modified'),
			'active' => Yii::t('lang','Active'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ruc',$this->ruc,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('social_reason',$this->social_reason,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('active',$this->active);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		//$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
