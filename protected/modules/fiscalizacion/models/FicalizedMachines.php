<?php

/**
 * This is the model class for table "fiscalized_machines".
 *
 * The followings are the available columns in table 'fiscalized_machines':
 * @property integer $id
 * @property integer $final_clients_id
 * @property string $date
 * @property string $ruc_technician
 * @property string $ruc_dealer
 * @property string $observations
 * @property integer $machine_id
 */
class FicalizedMachines extends CActiveRecord
{
	public function behaviors()
	{
		return array(
				// Classname => path to Class
				'LogsableBehavior' => 'application.behaviors.LogsableBehavior');
	}
	public function tableName()
	{
		return 'fiscalized_machines';
	}
        
        public $desde;
        public $hasta;
        public $textRUCTecnico = 'RUC Técnico';

	
	public function rules()
	{
		
		return array(
			array('final_clients_id, date, ruc_technician, ruc_dealer, machine_id', 'required'),
			array('final_clients_id, machine_id', 'numerical', 'integerOnly'=>true),
			array('ruc_technician, ruc_dealer', 'length', 'max'=>45),
			array('observations', 'safe'),
			array('machine_id', 'unique'),
			array('id, final_clients_id, date, ruc_technician, ruc_dealer, observations, machine_id, desde, hasta', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
                    'finalClients' => array(self::BELONGS_TO, 'FinalClient', 'final_clients_id'),
                    'machine' => array(self::BELONGS_TO, 'Machines', 'machine_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'final_clients_id' => Yii::t('lang','Cliente Final'),
			'date' => Yii::t('lang','Fecha fiscalizada'),
			'ruc_technician' => Yii::t('lang','Técnico'),
			'ruc_dealer' => Yii::t('lang','Distribuidor'),
			'observations' => Yii::t('lang','Observaciones'),
			'machine_id' => Yii::t('lang','N° de regisrto'),
			'created_at' => Yii::t('lang','Fecha de creación'),
		);
	}

	public function search($desde = NULL, $hasta = NULL, $dealer = NULL){
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('final_clients_id',$this->final_clients_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('ruc_technician',$this->ruc_technician,true);
		$criteria->compare('ruc_dealer',$this->ruc_dealer,true);
		$criteria->compare('observations',$this->observations,true);
		$criteria->compare('machine_id',$this->machine_id);
		$criteria->compare('created_at',$this->created_at);
        if($desde != NULL AND $hasta != NULL){
            if(Yii::app()->user->getState('rol') == 3 and  Yii::app()->user->getState('rol') == 5){
                
                    $criteria->join = "INNER JOIN rbac_usuarios Usuario ON (Usuario.dni_numero = t.ruc_dealer)";   
                    $criteria->condition = "Usuario.dni_numero = ".Yii::app()->user->getState('dni_numero');
                    $criteria->addBetweenCondition('date', $desde, $hasta, 'AND');
            }else {            
                    $criteria->join = "INNER JOIN rbac_usuarios Usuario ON (Usuario.dni_numero = t.ruc_dealer)";
                    $criteria->addBetweenCondition('date', $desde, $hasta, 'AND');
                  }
        }

		/* Condicion para no Mostrar los Eliminados en Gridview */
		//$criteria->addcondition("estatus <> 9");

		if(Yii::app()->params['GridViewOrder'] == FALSE)
		{
			$criteria->order = "id DESC";
			if(Yii::app()->user->getState('rol') == '3' or  Yii::app()->user->getState('rol') == '5')
			{
				$criteria->condition = "ruc_dealer = '".Yii::app()->user->getState('dni_numero'). "'";
			}
			
		}
		$data = new CActiveDataProvider($this, array('criteria'=>$criteria,));
		$_SESSION['promotion.excel'] = new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => false,
                 ));

		return $data;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
        public function ruc($ruc_technician = NULL, $valor = NULL){
            
           $sql = "SELECT Usuario.nombre, Usuario.apellido, Usuario.razon_social FROM rbac_usuarios Usuario WHERE Usuario.dni_numero = '".$ruc_technician."'";
           $query = Yii::app()->db->createCommand($sql)->queryAll();
           $xx = array();
           foreach ($query as $q):
               $xx[0] = $q['nombre'];                               
               $xx[1] = $q['apellido'];
               $xx[2] = $q['razon_social'];
           endforeach;  
            if($xx !=  null ){                
                $valor = $xx[2];//$xx[0]." ".$xx[1];                
            }else {
                $valor = 'No se encotro';               
            }            
            return $valor;
            
        }
        public function ruc_id($ruc_technician = NULL, $valor = NULL){
        
        	$sql = "SELECT Usuario.id FROM rbac_usuarios Usuario WHERE Usuario.dni_numero = '".$ruc_technician."'";
        	$query = Yii::app()->db->createCommand($sql)->queryAll();
        	$xx = '';
        	foreach ($query as $q):
        	$xx = $q['id'];
        	
        	endforeach;
        	if($xx !=  null ){
        		$valor = $xx;
        	}else {
        		$valor = 'No se encontro';
        	}
        	return $valor;
        
        }
}
