<?php

/**
 * This is the model class for table "device_types".
 *
 * The followings are the available columns in table 'device_types':
 * @property integer $id
 * @property string $name
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property RegisterOperations[] $registerOperations
 */
class DeviceTypes extends CActiveRecord
{
	
	public function tableName()
	{
		return 'device_types';
	}

	
	public function rules()
	{
		
		return array(
			array('name, active', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
		
			array('id, name, active', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'registerOperations' => array(self::HAS_MANY, 'RegisterOperations', 'device_type_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'name' => Yii::t('lang','Name'),
			'active' => Yii::t('lang','Active'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('active',$this->active);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
