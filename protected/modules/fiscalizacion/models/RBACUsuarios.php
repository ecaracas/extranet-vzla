<?php

/**
 * This is the model class for table "rbac_usuarios".
 *
 * The followings are the available columns in table 'rbac_usuarios':
 * 
 * @property integer $id
 * @property integer $rol_id
 * @property string $nombreusuario
 * @property string $contrasena
 * @property string $email
 * @property string $nombre
 * @property string $apellido
 * @property integer $estatus
 * @property string $dni_numero
 * @property integer $telf_local_tipo
 * @property string $telf_local_numero
 * @property integer $principal
 * @property integer $cambio_clave
 * @property string $registro_fecha
 * @property string $registro_hora
 * @property integer $registro_usuario_id
 * @property string $modificado_fecha
 * @property string $modificado_hora
 * @property integer $modificado_usuario_id
 * @property integer $secuencia The followings are the available model relations:
 * @property RbacRoles $rol
 * @property RbacRolesOpcionesUsuarios[] $rbacRolesOpcionesUsuarioses
 */
class RBACUsuarios extends CActiveRecord
{
	public function tableName()
	{
		return 'rbac_usuarios';
	}
	public function rules()
	{
		return array(
				array(
						'rol_id, nombreusuario, contrasena, email, nombre, apellido, dni_numero, registro_fecha, registro_usuario_id, modificado_fecha, modificado_usuario_id',
						'required' ),
				array(
						'rol_id, estatus, telf_local_tipo, principal, cambio_clave, registro_usuario_id, modificado_usuario_id, secuencia',
						'numerical',
						'integerOnly' => true ),
				array(
						'nombreusuario, email, nombre, apellido',
						'length',
						'max' => 100 ),
				array('email', 'email'),
				//array('nombreusuario', 'unique', 'attributeName'=>$this->nombreusuario),
				array(
						'dni_numero',
						'length',
						'max' => 20 ),
				array(
						'telf_local_numero',
						'length',
						'max' => 7 ),
				array(
						'registro_hora, modificado_hora',
						'safe' ),
				array(
						'id, rol_id, nombreusuario, contrasena, email, nombre, apellido, estatus, dni_numero, telf_local_tipo, telf_local_numero, principal, cambio_clave, registro_fecha, registro_hora, registro_usuario_id, modificado_fecha, modificado_hora, modificado_usuario_id, secuencia',
						'safe',
						'on' => 'search' ) );
	}
	public function relations()
	{
		return array(
				'rol' => array(self :: BELONGS_TO, 'RBACRoles', 'rol_id' ),
				'rbacRolesOpcionesUsuarioses' => array(self :: HAS_MANY, 'RBACRolesOpcionesUsuarios', 'usuario_id' ), 
                                'tecnico_id'=>array(self::HAS_MANY, 'Operations','id'),
                                'distribuidor_id'=>array(self::HAS_MANY, 'Operations','id'), );
	}
	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'rol_id' => 'Rol',
				'nombreusuario' => 'Usuario',
				'contrasena' => 'Contrasena',
				'email' => 'Email',
				'nombre' => 'Nombre',
				'apellido' => 'Apellido',
				'razon_social' => 'Razon Social',
				'estatus' => 'Activo?',
				'dni_numero' => 'C.I.',
				'telf_local_tipo' => 'Cod Tlf',
				'telf_local_numero' => 'Numero Tlf',
				'principal' => 'Principal',
				'cambio_clave' => 'Cambio Clave',
				'registro_fecha' => 'Registro Fecha',
				'registro_hora' => 'Registro Hora',
				'registro_usuario_id' => 'Registro Usuario',
				'modificado_fecha' => 'Modificado Fecha',
				'modificado_hora' => 'Modificado Hora',
				'modificado_usuario_id' => 'Modificado Usuario',
				'secuencia' => 'Secuencia',
				'nombreapellido' => 'Nombre',
		);
	}
	public function search()
	{
		$criteria = new CDbCriteria();
		$criteria->compare('id', $this -> id);
		$criteria->compare('rol_id', $this -> rol_id);
		$criteria->compare('nombreusuario', $this -> nombreusuario, true);
		$criteria->compare('contrasena', $this -> contrasena, true);
		$criteria->compare('email', $this -> email, true);
		$criteria->compare('nombre', $this -> nombre, true);
		$criteria->compare('apellido', $this -> apellido, true);
		$criteria->compare('razon_social', $this -> razon_social, true);
		$criteria->compare('estatus', $this -> estatus);
		$criteria->compare('dni_numero', $this -> dni_numero, true);
		$criteria->compare('telf_local_tipo', $this -> telf_local_tipo);
		$criteria->compare('telf_local_numero', $this -> telf_local_numero, true);
		$criteria->compare('principal', $this -> principal);
		$criteria->compare('cambio_clave', $this -> cambio_clave);
		$criteria->compare('registro_fecha', $this -> registro_fecha, true);
		$criteria->compare('registro_hora', $this -> registro_hora, true);
		$criteria->compare('registro_usuario_id', $this -> registro_usuario_id);
		$criteria->compare('modificado_fecha', $this -> modificado_fecha, true);
		$criteria->compare('modificado_hora', $this -> modificado_hora, true);
		$criteria->compare('modificado_usuario_id', $this -> modificado_usuario_id);
		$criteria->compare('secuencia', $this -> secuencia);
		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");
		if(Yii::app() -> params['GridViewOrder'] == FALSE)
		{
			$criteria -> order = "id DESC";
		}
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria ));
	}
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
