<?php

/**
 * This is the model class for table "register_operations".
 *
 * The followings are the available columns in table 'register_operations':
 * @property integer $id
 * @property integer $user_id
 * @property integer $device_type_id
 * @property integer $operation_id
 * @property string $rnc_tecnical
 * @property string $rnc_client
 * @property string $serial_printer
 * @property string $mode
 * @property string $address_fiscal
 * @property string $ini_sel
 * @property string $end_sel
 * @property string $code_printer
 * @property string $created
 * @property string $modified
 * @property integer $result
 * @property string $observation
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property DeviceTypes $deviceType
 * @property RbacUsuarios $user
 * @property Operations $operation
 */
class RegisterOperations extends CActiveRecord
{
	
	public function tableName()
	{
		return 'register_operations';
	}

	
	public function rules()
	{
		
		return array(
			array('device_type_id, operation_id, rnc_tecnical, rnc_client, serial_printer, ini_sel, end_sel, code_printer', 'required'),
			array('user_id, device_type_id, operation_id, result, status', 'numerical', 'integerOnly'=>true),
			array('rnc_tecnical, rnc_client, ini_sel, end_sel', 'length', 'max'=>10),
			array('code_printer', 'length', 'max'=>45),
			array('serial_printer', 'length', 'max'=>13),
			array('mode', 'length', 'max'=>20),
			array('modified','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
                        array('created,modified','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
		
			array('id, user_id, device_type_id, operation_id, rnc_tecnical, rnc_client, serial_printer, mode, address_fiscal, ini_sel, end_sel, code_printer, created, modified, result, observation, status', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'deviceType' => array(self::BELONGS_TO, 'DeviceTypes', 'device_type_id'),
			'user' => array(self::BELONGS_TO, 'RbacUsuarios', 'user_id'),
			'operation' => array(self::BELONGS_TO, 'Operations', 'operation_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'user_id' => Yii::t('lang','User'),
			'device_type_id' => Yii::t('lang','Device Type'),
			'operation_id' => Yii::t('lang','Operation'),
			'rnc_tecnical' => Yii::t('lang','Rnc Tecnical'),
			'rnc_client' => Yii::t('lang','Rnc Client'),
			'serial_printer' => Yii::t('lang','Serial Printer'),
			'mode' => Yii::t('lang','Mode'),
			'address_fiscal' => Yii::t('lang','Address Fiscal'),
			'ini_sel' => Yii::t('lang','Ini Sel'),
			'end_sel' => Yii::t('lang','End Sel'),
			'code_printer' => Yii::t('lang','Code Printer'),
			'created' => Yii::t('lang','Created'),
			'modified' => Yii::t('lang','Modified'),
			'result' => Yii::t('lang','Result'),
			'observation' => Yii::t('lang','Observation'),
			'status' => Yii::t('lang','Status'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('device_type_id',$this->device_type_id);
		$criteria->compare('operation_id',$this->operation_id);
		$criteria->compare('rnc_tecnical',$this->rnc_tecnical,true);
		$criteria->compare('rnc_client',$this->rnc_client,true);
		$criteria->compare('serial_printer',$this->serial_printer,true);
		$criteria->compare('mode',$this->mode,true);
		$criteria->compare('address_fiscal',$this->address_fiscal,true);
		$criteria->compare('ini_sel',$this->ini_sel,true);
		$criteria->compare('end_sel',$this->end_sel,true);
		$criteria->compare('code_printer',$this->code_printer,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('result',$this->result);
		$criteria->compare('observation',$this->observation,true);
		$criteria->compare('status',$this->status);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		//$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
