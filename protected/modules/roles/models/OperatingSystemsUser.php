<?php

/**
 * This is the model class for table "operating_systems_user".
 *
 * The followings are the available columns in table 'operating_systems_user':
 * @property string $id
 * @property integer $user_id
 * @property integer $operatingsystem_id
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property OperatingSystem $operatingsystem
 * @property RbacUsuarios $user
 */
class OperatingSystemsUser extends CActiveRecord
{
	
	public function tableName()
	{
		return 'operating_systems_user';
	}

	
	public function rules()
	{
		
		return array(
			array('user_id, operatingsystem_id, created_at, updated_at', 'required'),
			array('user_id, operatingsystem_id', 'numerical', 'integerOnly'=>true),
		
			array('id, user_id, operatingsystem_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'operatingsystem' => array(self::BELONGS_TO, 'OperatingSystem', 'operatingsystem_id'),
			'user' => array(self::BELONGS_TO, 'RbacUsuarios', 'user_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'user_id' => Yii::t('lang','User'),
			'operatingsystem_id' => Yii::t('lang','Operatingsystem'),
			'created_at' => Yii::t('lang','Created At'),
			'updated_at' => Yii::t('lang','Updated At'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('operatingsystem_id',$this->operatingsystem_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
