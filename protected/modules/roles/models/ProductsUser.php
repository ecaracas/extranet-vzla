<?php

/**
 * This is the model class for table "products_user".
 *
 * The followings are the available columns in table 'products_user':
 * @property string $id
 * @property integer $user_id
 * @property string $product_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductsUser extends CActiveRecord
{
	
	public function tableName()
	{
		return 'products_user';
	}

	
	public function rules()
	{
		
		return array(
			array('user_id, product_id, created_at, updated_at', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('product_id', 'length', 'max'=>10),
		
			array('id, user_id, product_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'user_id' => Yii::t('lang','User'),
			'product_id' => Yii::t('lang','Product'),
			'created_at' => Yii::t('lang','Created At'),
			'updated_at' => Yii::t('lang','Updated At'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
