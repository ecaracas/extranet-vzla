<?php

/**
 * This is the model class for table "programming_languages".
 *
 * The followings are the available columns in table 'programming_languages':
 * @property integer $id
 * @property string $name
 * @property integer $visible
 * @property string $created_at
 * @property string $updated_at
 */
class ProgrammingLanguages extends CActiveRecord
{
	
	public function tableName()
	{
		return 'programming_languages';
	}

	
	public function rules()
	{
		
		return array(
			array('name, visible, created_at, updated_at', 'required'),
			array('visible', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
		
			array('id, name, visible, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'name' => Yii::t('lang','Name'),
			'visible' => Yii::t('lang','Visible'),
			'created_at' => Yii::t('lang','Created At'),
			'updated_at' => Yii::t('lang','Updated At'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
