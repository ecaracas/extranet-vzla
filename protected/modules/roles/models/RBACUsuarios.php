<?php

/**
 * This is the model class for table "rbac_usuarios".
 *
 * The followings are the available columns in table 'rbac_usuarios':
 * @property integer $id
 * @property integer $rol_id
 * @property string $nombreusuario
 * @property string $contrasena
 * @property string confirmaPassword
 * @property string $email
 * @property string $web
 * @property string $softwarehouse
 * @property string $nombre
 * @property string $apellido
 * @property string $razon_social
 * @property integer $estatus
 * @property string $dni_numero
 * @property integer $telf_local_tipo
 * @property string $telf_local_numero
 * @property integer $principal
 * @property integer $cambio_clave
 * @property string $registro_fecha
 * @property string $registro_hora
 * @property integer $registro_usuario_id
 * @property string $modificado_fecha
 * @property string $modificado_hora
 * @property integer $modificado_usuario_id
 * @property integer $secuencia The followings are the available model relations:
 * @property RbacRoles $rol
 * @property RbacRoles $distribuidor_id
 * @property RbacRolesOpcionesUsuarios[] $rbacRolesOpcionesUsuarioses
 */
class RBACUsuarios extends CActiveRecord {

    public $confirmaPassword;

    public function tableName() {
        return 'rbac_usuarios';
    }

    public function behaviors() {
        return array(
            // Classname => path to Class
            'LogsableBehavior' => 'application.behaviors.LogsableBehavior')

        ;
    }

    public function rules() {
        return array(
            array(
                'rol_id, nombreusuario, contrasena, email, nombre, apellido, razon_social, dni_numero, registro_fecha, registro_usuario_id, modificado_fecha, modificado_usuario_id',
                'required'),
            array('cod_saint', 'validaCodSaint'),
            array(
                'file',
                'file',
                'allowEmpty' => true,
                'types' => 'jpg, gif, png',
                'on' => 'insert',
                'except' => 'update',
                'message' => 'Upload Valid Image!',
                'wrongType' => 'File type is Invalid',
                'minSize' => 1024,
                'maxSize' => 1024,
                'maxFiles' => 1,
                'tooLarge' => 'File Size Too Large',
                'tooSmall' => 'File Size Too Small',
                'tooMany' => 'Too Many Files Uploaded'),
            array(
                'rol_id, estatus, telf_local_tipo, principal, cambio_clave, registro_usuario_id, modificado_usuario_id, secuencia, ruc_dv',
                'numerical',
                'integerOnly' => true),
            array(
                'nombreusuario, email, nombre, apellido',
                'length',
                'max' => 100),
            array('nombreusuario', 'match', 'pattern' => "/^[0-9a-z]+$/i", 'message' => 'Sólo se aceptan letras y números.'),
            array('nombre, apellido', 'match', 'pattern' => "/^[a-zA-Z]+$/", 'message' => 'Sólo se aceptan letras.'),
            array('telf_local_tipo, telf_local_numero', 'match', 'pattern' => "/^[0-9]*$/", 'message' => 'Sólo se aceptan números.'),
            array('contrasena', 'length', 'max' => 64, 'min' => 6),
            array(
                'email',
                'email'),
            array(
                'web',
                'url'),
            array(
                'nombreusuario',
                'unique', 'message' => 'Usuario ya existe.'),
            array(
                'email',
                'uniqueEmail'),
            array(
                'dni_numero',
                'unique', 'message' => 'RUC ya existe.'),
            array(
                'dni_numero',
                'length',
                'max' => 20),
            array(
                'telf_local_numero',
                'length',
                'max' => 20),
            array(
                'registro_hora, modificado_hora',
                'safe'),
            array(
                'id, rol_id, nombreusuario, contrasena, email, nombre, apellido, estatus, dni_numero, telf_local_tipo, telf_local_numero, principal, cambio_clave, registro_fecha, registro_hora, registro_usuario_id, modificado_fecha, modificado_hora, modificado_usuario_id, secuencia',
                'safe',
                'on' => 'search'),
            array(
                'id, rol_id, nombreusuario, contrasena, email, nombre, apellido, estatus, dni_numero, telf_local_tipo, telf_local_numero, principal, cambio_clave, registro_fecha, registro_hora, registro_usuario_id, modificado_fecha, modificado_hora, modificado_usuario_id, secuencia',
                'safe',
                'on' => 'pendiente'),
            array(
                'id, rol_id, nombreusuario, contrasena, email, nombre, apellido, estatus, dni_numero, telf_local_tipo, telf_local_numero, principal, cambio_clave, registro_fecha, registro_hora, registro_usuario_id, modificado_fecha, modificado_hora, modificado_usuario_id, secuencia',
                'safe',
                'on' => 'distribuidor'),
            array(
                'id, rol_id, nombreusuario, contrasena, email, nombre, apellido, estatus, dni_numero, telf_local_tipo, telf_local_numero, principal, cambio_clave, registro_fecha, registro_hora, registro_usuario_id, modificado_fecha, modificado_hora, modificado_usuario_id, secuencia',
                'safe',
                'on' => 'tecnico'),
        );
    }

    public function validaCodSaint($attribute, $params) {
        //echo 'aqui: '.$this->rol_id; exit();
        if ($this->rol_id == 3) {
            $this->addError($attribute, 'Codigo Saint es obligatorio');
        }
    }

    public function relations() {
        return array(
            'rol' => array(
                self::BELONGS_TO,
                'RBACRoles',
                'rol_id'),
            'rbacRolesOpcionesUsuarioses' => array(
                self::HAS_MANY,
                'RBACRolesOpcionesUsuarios',
                'usuario_id'),
            'distribuidor' => array(
                self::BELONGS_TO,
                'RBACUsuarios',
                'distribuidor_id'),
        );
    }

    public function attributeLabels() {
        return array(
        'id' => 'ID',
        'rol_id' => 'Rol',
        'nombreusuario' => 'Usuario',
        'contrasena' => 'Contraseña',
        'confirmaPassword' => 'Confirmar Contraseña',
        'email' => 'Email',
        'nombre' => 'Nombre',
        'apellido' => 'Apellido',
        'razon_social' => Yii::t('lang', 'Razon Social'),
        'estatus' => 'Activo',
        'dni_numero' => 'RUC',
        'telf_local_tipo' => 'Celular',
        'telf_local_numero' => 'Teléfono Local',
        'principal' => 'Principal',
        'cambio_clave' => 'Cambio Clave',
        'registro_fecha' => 'Registro Fecha',
        'registro_hora' => 'Registro Hora',
        'registro_usuario_id' => 'Registro Usuario',
        'modificado_fecha' => 'Modificado Fecha',
        'modificado_hora' => 'Modificado Hora',
        'modificado_usuario_id' => 'Modificado Usuario',
        'secuencia' => 'Secuencia',
        'softwarehouse' => 'Casa de software',
        'distribuidor_id' => 'Distribuidor',
        'address' => 'Direccion',
        'ruc_dv' => 'DV',
        'cod_saint' => 'Codigo Saint',
        'file' => 'Foto');
    }

    public function search() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
        $criteria->compare('rol.id', $this->rol_id);
        $criteria->compare('t.nombreusuario', trim($this->nombreusuario), true);
        $criteria->compare('t.contrasena', $this->contrasena, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.nombre', $this->nombre, true);
        $criteria->compare('t.apellido', $this->apellido, true);
        $criteria->compare('razon_social', $this->razon_social, true);
        $criteria->compare('t.estatus', $this->estatus, true);
        $criteria->compare('t.dni_numero', $this->dni_numero, true);
        $criteria->compare('t.telf_local_tipo', $this->telf_local_tipo);
        $criteria->compare('t.telf_local_numero', $this->telf_local_numero, true);
        $criteria->compare('t.principal', $this->principal);
        $criteria->compare('t.cambio_clave', $this->cambio_clave);
        $criteria->compare('t.registro_fecha', $this->registro_fecha, true);
        $criteria->compare('t.softwarehouse', $this->softwarehouse, true);
        $criteria->compare('t.registro_hora', $this->registro_hora, true);
        $criteria->compare('t.registro_usuario_id', $this->registro_usuario_id);
        $criteria->compare('t.modificado_fecha', $this->modificado_fecha, true);
        $criteria->compare('t.modificado_hora', $this->modificado_hora, true);
        $criteria->compare('t.modificado_usuario_id', $this->modificado_usuario_id);
        $criteria->compare('t.secuencia', $this->secuencia);
        $criteria->compare('t.distribuidor_id', $this->distribuidor_id);
        $criteria->compare('t.file', $this->file);
        /* Condicion para no Mostrar los Eliminados en Gridview */
        $criteria->addcondition("t.estatus <> 9");
        $criteria->addcondition("t.estatus <> 3");
        if (Yii::app()->user->getState('rol') == 4) {
            $criteria->addcondition("t.rol_id in (3, 4, 5, 6, 8)");
        } elseif (Yii::app()->user->getState('rol') == 3) {
            $criteria->addcondition("t.rol_id in (8)");
            $criteria->addcondition("t.distribuidor_id = " . Yii::app()->user->getState('id'));
        }
        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }
        $criteria->with = array('rol');
        $data = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));

        $_SESSION['promotion.excel'] = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));

        return $data;
    }

    public function tecnico($id = null) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
        $criteria->compare('rol.id', $this->rol_id);
        $criteria->compare('t.nombreusuario', trim($this->nombreusuario), true);
        $criteria->compare('t.contrasena', $this->contrasena, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.nombre', $this->nombre, true);
        $criteria->compare('t.apellido', $this->apellido, true);
        $criteria->compare('razon_social', $this->razon_social, true);
        $criteria->compare('t.estatus', $this->estatus, true);
        $criteria->compare('t.dni_numero', $this->dni_numero, true);
        $criteria->compare('t.telf_local_tipo', $this->telf_local_tipo);
        $criteria->compare('t.telf_local_numero', $this->telf_local_numero, true);
        $criteria->compare('t.principal', $this->principal);
        $criteria->compare('t.cambio_clave', $this->cambio_clave);
        $criteria->compare('t.registro_fecha', $this->registro_fecha, true);
        $criteria->compare('t.softwarehouse', $this->softwarehouse, true);
        $criteria->compare('t.registro_hora', $this->registro_hora, true);
        $criteria->compare('t.registro_usuario_id', $this->registro_usuario_id);
        $criteria->compare('t.modificado_fecha', $this->modificado_fecha, true);
        $criteria->compare('t.modificado_hora', $this->modificado_hora, true);
        $criteria->compare('t.modificado_usuario_id', $this->modificado_usuario_id);
        $criteria->compare('t.secuencia', $this->secuencia);
        $criteria->compare('t.distribuidor_id', $this->distribuidor_id);
        $criteria->compare('t.file', $this->file);
        /* Condicion para no Mostrar los Eliminados en Gridview */
        $criteria->addcondition("t.estatus <> 9");
        $criteria->addcondition("t.estatus <> 3");
        if (Yii::app()->user->getState('rol') == 2) {
            $criteria->addcondition("t.rol_id = 8");
            $criteria->addcondition("t.distribuidor_id = " . $id);
        } elseif (Yii::app()->user->getState('rol') == 4) {
            $criteria->addcondition("t.rol_id  = 8");
            $criteria->addcondition("t.distribuidor_id = " . $id);
        }
        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }
        $criteria->with = array('rol');
//		return new CActiveDataProvider($this, array('criteria' => $criteria));

        $data = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));

        $_SESSION['promotion.excel'] = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));

        return $data;
    }

    public function pendiente() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.rol_id', $this->rol_id);
        $criteria->compare('t.nombreusuario', $this->nombreusuario, true);
        $criteria->compare('t.contrasena', $this->contrasena, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.nombre', $this->nombre, true);
        $criteria->compare('t.apellido', $this->apellido, true);
        $criteria->compare('razon_social', $this->razon_social, true);
        $criteria->compare('t.estatus', $this->estatus);
        $criteria->compare('t.dni_numero', $this->dni_numero, true);
        $criteria->compare('t.telf_local_tipo', $this->telf_local_tipo);
        $criteria->compare('t.telf_local_numero', $this->telf_local_numero, true);
        $criteria->compare('t.principal', $this->principal);
        $criteria->compare('t.cambio_clave', $this->cambio_clave);
        $criteria->compare('t.registro_fecha', $this->registro_fecha, true);
        $criteria->compare('t.softwarehouse', $this->softwarehouse, true);
        $criteria->compare('t.registro_hora', $this->registro_hora, true);
        $criteria->compare('t.registro_usuario_id', $this->registro_usuario_id);
        $criteria->compare('t.modificado_fecha', $this->modificado_fecha, true);
        $criteria->compare('t.modificado_hora', $this->modificado_hora, true);
        $criteria->compare('t.modificado_usuario_id', $this->modificado_usuario_id);
        $criteria->compare('t.secuencia', $this->secuencia);
        $criteria->compare('t.distribuidor_id', $this->distribuidor_id);
        $criteria->compare('t.file', $this->file);
        /* Condicion para no Mostrar los Eliminados en Gridview */
        $criteria->addcondition("t.estatus <> 9");
        $criteria->addcondition("t.estatus = 3 ");
        if (Yii::app()->user->getState('rol') == 4) {
            $criteria->addcondition("t.rol_id in (3, 4, 5, 6, 8)");
        } elseif (Yii::app()->user->getState('rol') == 3) {
            $criteria->addcondition("t.rol_id in (8)");
            $criteria->addcondition("t.distribuidor_id = " . Yii::app()->user->getState('id'));
        }
        $criteria->with = array(
            'rol');
        // $criteria->with = array('rol');
        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }
        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public function distribuidor() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.rol_id', $this->rol_id);
        $criteria->compare('t.nombreusuario', trim($this->nombreusuario), true);
        $criteria->compare('t.contrasena', $this->contrasena, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.nombre', $this->nombre, true);
        $criteria->compare('t.apellido', $this->apellido, true);
        $criteria->compare('razon_social', $this->razon_social, true);
        $criteria->compare('t.estatus', $this->estatus);
        $criteria->compare('t.dni_numero', $this->dni_numero, true);
        $criteria->compare('t.telf_local_tipo', $this->telf_local_tipo);
        $criteria->compare('t.telf_local_numero', $this->telf_local_numero, true);
        $criteria->compare('t.principal', $this->principal);
        $criteria->compare('t.cambio_clave', $this->cambio_clave);
        $criteria->compare('t.registro_fecha', $this->registro_fecha, true);
        $criteria->compare('t.softwarehouse', $this->softwarehouse, true);
        $criteria->compare('t.registro_hora', $this->registro_hora, true);
        $criteria->compare('t.registro_usuario_id', $this->registro_usuario_id);
        $criteria->compare('t.modificado_fecha', $this->modificado_fecha, true);
        $criteria->compare('t.modificado_hora', $this->modificado_hora, true);
        $criteria->compare('t.modificado_usuario_id', $this->modificado_usuario_id);
        $criteria->compare('t.secuencia', $this->secuencia);
        $criteria->compare('t.distribuidor_id', $this->distribuidor_id);
        $criteria->compare('t.file', $this->file);
        /* Condicion para no Mostrar los Eliminados en Gridview */
        $criteria->addcondition("t.estatus <> 9");
//		$criteria->addcondition("t.estatus = 3 ");
        /* si entra el super admin(2) se le muestra de una vez una lista de los distribuidores */
        if (Yii::app()->user->getState('rol') == 2 or Yii::app()->user->getState('rol') == 4) {
            $criteria->addcondition("t.rol_id = 3");
        } elseif (Yii::app()->user->getState('rol') == 3) {
            $criteria->addcondition("t.rol_id in (8)");
            $criteria->addcondition("t.distribuidor_id = " . Yii::app()->user->getState('id'));
        }
        $criteria->with = array('rol');
        // $criteria->with = array('rol');
        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }
        $data = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));

        $_SESSION['promotion.excel'] = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));

        return $data;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function uniqueEmail($attribute) {
        $existe = false;
        $validaTec = true;
        if (!Yii::app()->request->isAjaxRequest) {
            if (!is_null($this->id)) {
                $user = RBACUsuarios::model()->find('id=:id', array('id' => $this->id));
                if ($user->rol_id != 8 && $user->email == $this->$attribute) {
                    $validaTec = false;
                } elseif ($user->rol_id == 8)
                    $validaTec = false;
            }
            if ($validaTec) {
                $existe = RBACUsuarios::model()->find('email=:email', array('email' => $this->$attribute));
                if (!is_null($existe))
                    $this->addError($attribute, 'Email ya existe.!');
            }
        }
    }

}
