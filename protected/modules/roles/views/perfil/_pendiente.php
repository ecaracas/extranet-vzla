<?php
Yii::app()->params['IDGridview'] = 'users-pendiente-grid';
Yii::app()->params['rutaUrlGridviewBoxSwitch'] = Yii::app()->controller->createUrl('UpdateActive');
$active = array('0'=>'Inactivos','1'=>'Activos');

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#users-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<input type="hidden" id="TOKEN" value="<?php echo Yii::app()->request->csrfToken ?>">
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo Yii::app()->params['index-text']; ?> Usuarios</h3>

                <div class="menu-tool">

                    <div class="pagesize"> 
                        <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->pendiente()); ?>
                    </div>                     

                    <div class="menu-items">
                        <?php  $this->widget('zii.widgets.CMenu', array('items' => $this->menu,'encodeLabel' => FALSE,'htmlOptions' => array('class' => 'cmenuhorizontal'),)); ?>
                    </div>
                </div>        
                <?php $this->ToolActionsRight(); ?>         

            </div>
            <div class="panel-body"> 
                <?php if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) { ?>
                    <?php echo CHtml::link('Nuevo usuario', array('usuarios/create'), array('class' => 'btn btn-3d btn-primary')); ?>
                <?php } ?>
                <div class="table-responsive">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>Yii::app()->params['IDGridview'],       
                        'dataProvider' => $dataProvider,
                        'filter' => $model,
                        'columns' => array(                                                 
                            array('name' => 'rol_id', 'value' => '$data->rol->descripcion', 'type' => 'text', 'filter' => CHtml::listData(RBACRoles::model()->findAll('estatus = 1'), "id", "descripcion")),
                            array('name' => 'nombreusuario', 'value' => '$data->nombreusuario'),                          
                            array('name' => 'email', 'value' => '$data->email'),
                            array('name' => 'nombre', 'value' => '$data->nombre'),
                            array('name' => 'estatus','type' => 'raw','value' => 'Active::checkSwicth($data->estatus,$data->id)', 'filter' => Active::getListActive(),'htmlOptions' => array('class' => 'switchactive')
                            ),
                          
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::app()->params['view-text'],
                                'label' => Yii::app()->params['view-icon'],
                                'linkHtmlOptions' => array('class' => 'view ' . Yii::app()->params['view-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                            ),
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::app()->params['update-text'],
                                'label' => Yii::app()->params['update-icon'],
                                'linkHtmlOptions' => array('class' => 'edit ' . Yii::app()->params['update-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
                            ),
                            array(
                                'class' => 'CLinkColumn',
                                'header' => 'Menu',
                                'label' => '<i class="fa fa-th"></i>',
                                'linkHtmlOptions' => array('class' => 'menu ' . Yii::app()->params['update-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("menu",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
                            ),
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::app()->params['delete-text'],
                                'label' => Yii::app()->params['delete-icon'],
                                'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id,"delete"=>$data->estatus))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
                            ),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
//if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {
//
//    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);
//
//    $this->renderPartial('../../../../views/site/modal-delete', array());    
//}
?>