<?php
/* @var $this UsuariosController */
/* @var $model RBACUsuarios */
/* @var $form CActiveForm */
?>
<?php $form = $this->beginWidget('CActiveForm', array('id' => 'rbacusuarios-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form'),)); ?>


<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'rol_id'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->dropDownList($model, 'rol_id', CHtml::listData(RBACRoles::model()->findAll(), 'id', 'descripcion'), array((Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create') ? '' : "disabled"=>"disabled")); ?>
        <?php echo $form->error($model, 'rol_id'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'nombreusuario'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'nombreusuario', array('size' => 60, 'maxlength' => 100)); ?>

        <?php echo $form->error($model, 'nombreusuario'); ?>
    </div>

</div>

<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'contrasena'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->passwordField($model, 'contrasena', array('rows' => 60, 'cols' => 100)); ?>

        <?php echo $form->error($model, 'contrasena'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'email'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 100)); ?>

        <?php echo $form->error($model, 'email'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'nombre'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'nombre', array('size' => 60, 'maxlength' => 100)); ?>

        <?php echo $form->error($model, 'nombre'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'apellido'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'apellido', array('size' => 60, 'maxlength' => 100)); ?>

        <?php echo $form->error($model, 'apellido'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'dni_numero'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'dni_numero', array('size' => 20, 'maxlength' => 20)); ?>

        <?php echo $form->error($model, 'dni_numero'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'telf_local_tipo'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'telf_local_tipo'); ?>

        <?php echo $form->error($model, 'telf_local_tipo'); ?>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'telf_local_numero'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'telf_local_numero', array('size' => 7, 'maxlength' => 7)); ?>

        <?php echo $form->error($model, 'telf_local_numero'); ?>
    </div>


</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'file'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <div class="col-xs-12 col-sm-9">
            <?php echo $form->FileField($model, 'file', array('size' => 7, 'maxlength' => 7)); ?>

            <?php echo $form->error($model, 'file'); ?>
        </div>
    </div>



</div>
<br />
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'estatus'); ?>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php
        $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
            'id' => 'RBACUsuarios_estatus',
            'attribute' => 'RBACUsuarios[estatus]',
            'state' => $model->estatus,
            'type' => 'item',
            'coloron' => 'color1',
        ));
        ?>  

        <?php echo $form->error($model, 'estatus'); ?>
    </div>

</div>
<br />
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <p class="text-left">Campos con <span class="errorMessage">*</span> son requeridos.</p>
    </div>
    <div class="col-xs-12 col-sm-6">                
        <ul class="cmenuhorizontal" id="yw0">
            <li class="bottom"><?php echo CHtml::submitButton(Yii::app()->params['save-text'], array('class' => Yii::app()->params['save-btn'])); ?>
            </li>
            <li class="bottom"><?php echo CHtml::link(Yii::app()->params['cancel-text'], Yii::app()->controller->createUrl('index'), array('class' => Yii::app()->params['cancel-btn'])); ?>
            </li>
        </ul>
    </div>

</div>

<?php $this->endWidget(); ?>
