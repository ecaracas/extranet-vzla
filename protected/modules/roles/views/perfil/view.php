<?php
/* @var $this UsuariosController */
/* @var $model RBACUsuarios */

$this->breadcrumbs=array(
    'Rbacusuarioses'=>array('index'),
    $model->id,
);

$this->menu=array(
    array(
        'label'=>Yii::app()->params['create-text'], 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label'=>Yii::app()->params['update-text'], 
        'url'=>array('update', 
        'id'=>$model->id), 
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label'=>Yii::app()->params['index-text'], 
        'url'=>array('index'), 
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo  Yii::app()->params['update-text']; ?> Usuarios</h3>

                <div class="menu-tool">
                    <?php 
//                    $this->widget('zii.widgets.CMenu', array(
//                    'items' => $this->menu,
//                    'encodeLabel' => FALSE,
//                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
//                    ));
                    ?>
                    <a href="javascript:history.go(-1);"  type="button" class="btn btn-info">Atras</a>
                    <br>
                </div>
                        <?php $this->ToolActionsRight(); ?>       
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('rol_id')); ?></th>
	<td><?php echo CHtml::encode($model->rol->descripcion); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('nombreusuario')); ?></th>
	<td><?php echo CHtml::encode($model->nombreusuario); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></th>
	<td><?php echo CHtml::encode($model->email); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('nombre')); ?></th>
	<td><?php echo CHtml::encode($model->nombre); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('apellido')); ?></th>
	<td><?php echo CHtml::encode($model->apellido); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('estatus')); ?></th>
	<td><?php echo CHtml::encode($model->estatus == 0 ? 'Inactivo' : 'Activo'); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('RUC')); ?></th>
	<td><?php echo CHtml::encode($model->dni_numero); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('telf_local_tipo')); ?></th>
	<td><?php echo CHtml::encode($model->telf_local_tipo); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('telf_local_numero')); ?></th>
	<td><?php echo CHtml::encode($model->telf_local_numero); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('registro_fecha')); ?></th>
	<td><?php echo CHtml::encode($model->registro_fecha); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('registro_hora')); ?></th>
	<td><?php echo CHtml::encode($model->registro_hora); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('modificado_fecha')); ?></th>
	<td><?php echo CHtml::encode($model->modificado_fecha); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('modificado_hora')); ?></th>
	<td><?php echo CHtml::encode($model->modificado_hora); ?></td>
	</tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>