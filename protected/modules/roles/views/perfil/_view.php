<?php
/* @var $this UsuariosController */
/* @var $data RBACUsuarios */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('rol_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->rol->descripcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('nombreusuario')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->nombreusuario); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('email')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->email); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->nombre); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('apellido')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->apellido); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->estatus); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('dni_numero')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->dni_numero); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('telf_local_tipo')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->telf_local_tipo); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('telf_local_numero')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->telf_local_numero); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('principal')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->principal); ?></div>
	
</div>
	<div class='row'>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_fecha); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_hora')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_hora); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_usuario_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_usuario_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_fecha); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_hora')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_hora); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_usuario_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_usuario_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('secuencia')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->secuencia); ?></div>
	
</div>

<hr />