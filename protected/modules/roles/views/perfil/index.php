<?php
$this->widget('zii.widgets.jui.CJuiTabs',array(
    'tabs'=>array(      
        'Activos'=>array('id'=>'Activos','content'=>$this->renderPartial('_activos',array('model'=>$model),TRUE)),        
        'Pendiente por aprobar'=>array('id'=>'pendiente','content'=>$this->renderPartial('_pendiente',array('model'=>$model),TRUE)),                
    ),  
    'options'=>array('collapsible'=>true,),
    'id'=>'Usuarios',
));
?>