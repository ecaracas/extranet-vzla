<?php
/* @var $this RolesController */
/* @var $data RBACRoles */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->descripcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php
                        $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
                            'id' => 'RBACRoles_estatus',
                            'attribute' => 'RBACRoles[estatus]',
                            'state' => $model->estatus,
                            'type' => 'item',
                            'coloron' => 'color1',
                        ));
                        ?> </div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->estatus); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_fecha); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_hora')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_hora); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_usuario_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_usuario_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_fecha); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_hora')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_hora); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_usuario_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_usuario_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('principal')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->principal); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('secuencia')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->secuencia); ?></div>
	
</div>

<hr />