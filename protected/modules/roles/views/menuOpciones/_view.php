<?php
/* @var $this MenuOpcionesController */
/* @var $data RBACMenuOpciones */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('menu_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->menu_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->descripcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('opcion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->opcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('url')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->url); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('url_tipo')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->url_tipo); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('categoria')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->categoria); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($model->estatus == 0 ? 'Inactivo' : 'Activo'); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('orden')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->orden); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('icono')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->icono); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('jerarquia')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->jerarquia); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_fecha); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_hora')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_hora); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_fecha); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_hora')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_hora); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('secuencia')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->secuencia); ?></div>
	
</div>

<hr />