<?php
/* @var $this RolesOpcionesController */
/* @var $data RBACRolesOpciones */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('rol_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->rol_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('menu_opcion_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->menu_opcion_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($model->estatus == 0 ? 'Inactivo' : 'Activo'); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_fecha); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_hora')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_hora); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('registro_usuario_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->registro_usuario_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_fecha); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_hora')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_hora); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('modificado_usuario_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->modificado_usuario_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('secuencia')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->secuencia); ?></div>
	
</div>

<hr />