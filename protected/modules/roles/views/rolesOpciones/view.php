<?php
/* @var $this RolesOpcionesController */
/* @var $model RBACRolesOpciones */

$this->breadcrumbs=array(
    'Rbacroles Opciones'=>array('index'),
    $model->id,
);

$this->menu=array(
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['update-text']), 
        'url'=>array('update', 
        'id'=>$model->id), 
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['index-text']), 
        'url'=>array('index'), 
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['view-text']) .' '. Yii::t('lang','Roles Opciones'); ?></h3>

                <div class="menu-tool">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
                  <?php $this->ToolActionsRight(); ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('rol_id')); ?></th>
	<td><?php echo CHtml::encode($model->rol_id); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('menu_opcion_id')); ?></th>
	<td><?php echo CHtml::encode($model->menu_opcion_id); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('estatus')); ?></th>
	<td><?php echo CHtml::encode($model->estatus == 0 ? 'Inactivo' : 'Activo'); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('registro_fecha')); ?></th>
	<td><?php echo CHtml::encode($model->registro_fecha); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('registro_hora')); ?></th>
	<td><?php echo CHtml::encode($model->registro_hora); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('registro_usuario_id')); ?></th>
	<td><?php echo CHtml::encode($model->registro_usuario_id); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('modificado_fecha')); ?></th>
	<td><?php echo CHtml::encode($model->modificado_fecha); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('modificado_hora')); ?></th>
	<td><?php echo CHtml::encode($model->modificado_hora); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('modificado_usuario_id')); ?></th>
	<td><?php echo CHtml::encode($model->modificado_usuario_id); ?></td>
	</tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>