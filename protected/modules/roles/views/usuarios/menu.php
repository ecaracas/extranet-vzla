<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'rbacroles-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form'
    ),
        ));
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo Yii::app()->params['index-text']; ?> Menu</h3>

                <div class="menu-tool">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items' => $this->menu,
                        'encodeLabel' => FALSE,
                        'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>

            </div>
            <div class="panel-body">

                <div class="form-group">
                    <div class="col-xs-12 col-sm-2">
                        
                    </div>

                    <div class="col-xs-12 col-sm-4">
                    
                    </div>
                    <div class="col-xs-12 col-sm-2">

                    </div>
                    <?php if ($model->id != NULL) { ?>
                        <div class="col-xs-12 col-sm-4">
                            <ul class="cmenuhorizontal right" id="yw0">
                                <li class="bottom"><?php echo CHtml::htmlButton(Yii::app()->params['save-text'], array('class' => Yii::app()->params['save-btn'], 'name' => 'btnsave', 'Type' => 'submit')); ?>
                                </li>
                                <li class="bottom"><?php echo CHtml::link(Yii::app()->params['cancel-text'], Yii::app()->controller->createUrl('index'), array('class' => Yii::app()->params['cancel-btn'])); ?>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>

                <?php  if ($model->id != NULL) { ?>

                    <?php
                    $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
                        'attribute' => 'rowSelectAll',
                        'type' => 'selectall',
                        'state' => FALSE,
                            )
                    );
                    ?>
                  <p>Seleccionar Todos</p>
                <?php  } ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php
    foreach ($menuList as $menu) {
        ?>    
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading navbar-tool">
                    <h3  style="float: left;" class="panel-title"><?php echo $menu['menuicon']; ?> <?php echo ucfirst($menu['menu']); ?></h3>
                    <div style="float: right;">
                        <?php
                        $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
                            'id' => @$menu['opcion_id'],
                            'attribute' => 'Items[' . @$menu['opcion_id'] . ']',
                            'state' => @$menu['value'],
                            'type' => 'item',
                            'coloron' => 'color1',
                        ));
                        ?>   
                    </div>
                    <div class="clearfix" ></div>
                </div>
                <div class="panel-body">
                    
                    
                    <?php
                    if (count(@$opcionMenu[$menu['menu_id']]) > 0) {
                        ?> 
                        <ul class="list-group">

                            <?php foreach (@$opcionMenu[$menu['menu_id']] as $om) { ?>

                                <li class="list-group-item">
                                    <p  style="float: left;"><?php echo $om['menuicon']; ?> <?php echo ucfirst($om['menu']); ?></p>
                                    <div style="float: right;">
                                        <?php
                                        $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
                                            'id' => @$om['opcion_id'],
                                            'attribute' => 'Items[' . @$om['opcion_id'] . ']',
                                            'state' => @$om['value'],
                                            'type' => 'item',
                                            'coloron' => 'color1',
                                        ));
                                        ?>   
                                    </div>
                                    <div class="clearfix" ></div>
                                </li>

                            <?php } ?> 

                        </ul>                    
                    <?php } ?>
                    
                    <hr />

                    <?php
                    if (count(@$submenuList[$menu['menu_id']]) > 0) {
                        ?> 
                        <?php foreach (@$submenuList[$menu['menu_id']] as $sm) { ?>


                            <div class="panel panel-primary">
                                <div class="panel-heading navbar-tool">
                                    <h3  style="float: left;" class="panel-title"><?php echo $sm['menuicon']; ?> <?php echo ucfirst($sm['menu']); ?></h3>
                                    <div style="float: right;">
                                        <?php
                                        $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
                                            'id' => @$sm['opcion_id'],
                                            'attribute' => 'Items[' . @$sm['opcion_id'] . ']',
                                            'state' => @$sm['value'],
                                            'type' => 'item',
                                            'coloron' => 'color1',
                                        ));
                                        ?>   
                                    </div>
                                    <div class="clearfix" ></div>
                                </div>
                                <div class="panel-body">

                                    <?php
                                    if (count(@$opcionSubmenu[$sm['menu_id']]) > 0) {
                                        ?> 
                                        <ul class="list-group">

                                            <?php foreach (@$opcionSubmenu[$sm['menu_id']] as $om) { ?>

                                                <li class="list-group-item">
                                                    <p  style="float: left;"><?php echo $om['menuicon']; ?> <?php echo ucfirst($om['menu']); ?></p>
                                                    <div style="float: right;">
                                                        <?php
                                                        $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
                                                            'id' => @$om['opcion_id'],
                                                            'attribute' => 'Items[' . @$om['opcion_id'] . ']',
                                                            'state' => @$om['value'],
                                                            'type' => 'item',
                                                            'coloron' => 'color1',
                                                        ));
                                                        ?>   
                                                    </div>
                                                    <div class="clearfix" ></div>
                                                </li>

                                            <?php } ?> 

                                        </ul>                    
                                    <?php } ?>


                                </div>
                            </div>                            
                        <?php } ?>                               
                    <?php } ?>

                </div>                    
            </div>
        </div>
        <?php
    }
    ?> 
</div>
<?php $this->endWidget(); ?>