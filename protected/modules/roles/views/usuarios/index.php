<?php

if (Yii::app()->user->getState('rol') == 3) {
    $this->widget('zii.widgets.jui.CJuiTabs', array(
        'tabs' => array(
            'Técnicos' => array('id' => 'Activos', 'content' => $this->renderPartial('_activos', array('model' => $model), TRUE)),
        ),
        'options' => array('collapsible' => true,),
        'id' => 'Usuarios',
    ));
} else {
    $this->widget('zii.widgets.jui.CJuiTabs', array(
        'tabs' => array(
            'Activos' => array('id' => 'Activos', 'content' => $this->renderPartial('_activos', array('model' => $model), TRUE)),
            'Pendiente por aprobar' => array('id' => 'pendiente', 'content' => $this->renderPartial('_pendiente', array('model' => $model), TRUE)),
            //'Distribuidor' => array('id' => 'distribuidor', 'content' => $this->renderPartial('_distribuidor', array('model' => $model), TRUE)),
        ),
        'options' => array('collapsible' => true,),
        'id' => 'Usuarios',
    ));
}

//Actualiza los tabs en caso de que ocurra una actualización y se observen en los otros
Yii::app()->clientScript->registerScript('actualizar', "
$( '#Usuarios ul li' ).click(function() {
     
      jQuery('#users-distribuidor-grid').yiiGridView('update');
      jQuery('#users-activo-grid').yiiGridView('update');
      jQuery('#users-pendiente-grid').yiiGridView('update');
      
    });
 
");

?>