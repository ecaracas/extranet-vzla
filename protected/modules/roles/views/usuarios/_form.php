<?php
/* @var $this UsuariosController */
/* @var $model RBACUsuarios */
/* @var $form CActiveForm */
$contrasenaOld = $model->contrasena;
$model->contrasena = '';
?>
<style>
    .error{
        color:red;
    }
</style>
<?php $form = $this->beginWidget('CActiveForm', array('id' => 'rbacusuarios-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'role' => 'form'),)); ?>
<input type="hidden" name="oldPass" value="<?php echo $contrasenaOld ?>"/>
<div class="row"><div class="col-xs-12 col-sm-9"><strong><h1 style="font-size: 2em">Datos del usuario</h1></strong></div></div>
<hr>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'rol_id'); ?>
    </div>
    <?php if (Yii::app()->user->getState('rol') == 4): ?>
        <div class="col-xs-12 col-sm-9">
            <?php echo $form->dropDownList($model, 'rol_id', CHtml::listData(RBACRoles::model()->findAll('id in (3,4,5,6,8)'), 'id', 'descripcion'), array('empty' => 'Seleccione', (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create') ? '' : "disabled" => "disabled")); ?>
            <?php echo $form->error($model, 'rol_id'); ?>
        </div>
    <?php elseif (Yii::app()->user->getState('rol') == 3 || Yii::app()->user->getState('rol') == 5): ?>
        <div class="col-xs-12 col-sm-9">
            <?php echo $form->dropDownList($model, 'rol_id', CHtml::listData(RBACRoles::model()->findAll('id = 8'), 'id', 'descripcion'), array('empty' => 'Seleccione', (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create') ? '' : "disabled" => "disabled")); ?>
            <?php echo $form->error($model, 'rol_id'); ?>
        </div>
    <?php else: ?>
        <div class="col-xs-12 col-sm-9">
            <?php echo $form->dropDownList($model, 'rol_id', CHtml::listData(RBACRoles::model()->findAll(), 'id', 'descripcion'), array('empty' => 'Seleccione', (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create') ? '' : "disabled" => "disabled")); ?>
            <?php echo $form->error($model, 'rol_id'); ?>
        </div>
    <?php endif; ?>
</div>
<?php if (Yii::app()->user->getState('rol') == 2 || Yii::app()->user->getState('rol') == 4): ?>

    <div class="form-group"  id="campoDist">
        <div class="col-xs-12 col-sm-3">
            <?php echo $form->labelEx($model, 'distribuidor_id'); ?>
        </div>
        <div class="col-xs-12 col-sm-9">
            <?php echo $form->dropDownList($model, 'distribuidor_id', CHtml::listData(RBACUsuarios::model()->findAll('rol_id = 3'), 'id', 'nombre'), array('empty' => 'Seleccione', (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create') ? '' : "disabled" => "disabled")); ?>
            <?php echo $form->error($model, 'distribuidor_id'); ?>
        </div>
    </div>

    <div class="form-group"  id="conSaint" style="<?php echo ($model->rol_id == 3 || $model->rol_id == 5) ? '' : 'display: none' ?>">
        <div class="col-xs-12 col-sm-3">
            <?php echo $form->labelEx($model, 'cod_saint'); ?>&nbsp;<span class="required" aria-required="true" style="    color: #E20F0F;">*</span> 
        </div>
        <div class="col-xs-12 col-sm-9">
            <?php echo $form->textField($model, 'cod_saint', array('size' => 100, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'cod_saint'); ?>
        </div>
    </div>

    <div class="form-group" id="campoRazon" style="<?php echo ($model->rol_id == 3 || $model->rol_id == 5) ? '' : 'display: none' ?>">
        <div class="col-xs-12 col-sm-3">
            <?php echo $form->labelEx($model, 'razon_social', array('class' => 'label_saint')); ?>
        </div>
        <div class="col-xs-12 col-sm-9">
            <?php echo $form->textField($model, 'razon_social', array('size' => 100, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'razon_social'); ?>
        </div>
    </div>
<?php endif; ?>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'nombreusuario'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'nombreusuario', array('size' => 60, 'maxlength' => 100, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled" => "disabled")); ?>
        <?php echo $form->error($model, 'nombreusuario'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'contrasena'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->passwordField($model, 'contrasena', array('rows' => 60, 'cols' => 100)); ?>
        <?php echo $form->error($model, 'contrasena'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'confirmaPassword'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->passwordField($model, 'confirmaPassword', array('rows' => 60, 'cols' => 100)); ?>
        <?php echo $form->error($model, 'confirmaPassword'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'email'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 100, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled" => "disabled")); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>
</div>





<div class="row"><div class="col-xs-12 col-sm-9"><strong><h1 style="font-size: 2em">Datos de registro</h1></strong></div></div>
<hr>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'nombre'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'nombre', array('size' => 60, 'maxlength' => 100, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled" => "disabled")); ?>
        <?php echo $form->error($model, 'nombre'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'apellido'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'apellido', array('size' => 60, 'maxlength' => 100, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled" => "disabled")); ?>
        <?php echo $form->error($model, 'apellido'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'dni_numero'); ?>
    </div>
    <div class="col-xs-12 col-sm-5">
        <?php echo $form->textField($model, 'dni_numero', array('size' => 20, 'maxlength' => 20, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled" => "disabled")); ?>
        <?php echo $form->error($model, 'dni_numero'); ?>
    </div>

    <?php
    if ($model->rol_id == 3 OR $model->rol_id == 5) {
        $display_dv = 'block';
    } else {
        $display_dv = 'none';
    }
    ?>

    <div class="col-xs-12 col-sm-4 campodv" style="display: <?= $display_dv; ?>">
        <div style="float: left; padding-right: 20px;">
            <?php echo $form->labelEx($model, 'ruc_dv'); ?>
        </div>
        <div class="campodv" style="float: left;" style="display: <?= $display_dv; ?>">
            <?php echo $form->textField($model, 'ruc_dv', array('size' => 20, 'maxlength' => 2, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled" => "disabled")); ?>
            <?php echo $form->error($model, 'ruc_dv'); ?>
        </div>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'telf_local_tipo'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'telf_local_tipo', array('maxlength' => 11, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled" => "disabled")); ?>
        <?php echo $form->error($model, 'telf_local_tipo'); ?>
    </div>
</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'telf_local_numero'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'telf_local_numero', array('maxlength' => 7, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled" => "disabled")); ?>
        <?php echo $form->error($model, 'telf_local_numero'); ?>
    </div>
</div>

<?php
if ($model->rol_id == 3 OR $model->rol_id == 5) {
    $display_address = 'block';
} else {
    $display_address = 'none';
}
?>
<div class="form-group" id="campoDireccion" style="display: <?= $display_address; ?>">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'address'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textField($model, 'address', array('maxlength' => 255)); ?>
        <?php echo $form->error($model, 'address'); ?>
    </div>
</div>

<?php if (Yii::app()->user->getState('id') != $model->id): ?>
    <div class="form-group">
        <div class="col-xs-12 col-sm-3">
            <?php echo $form->labelEx($model, 'file'); ?>
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="col-xs-12 col-sm-9">
                <?php
                //echo $form->FileField($model, 'file', array('size' => 7, 'maxlength' => 100, (Yii::app()->user->getState('rol') == 2 || Yii::app()->controller->action->id == 'create' || Yii::app()->user->getState('rol') == $model->rol_id) ? '' : "disabled"=>"disabled")); 
                echo $form->FileField($model, 'file', array('size' => 7, 'maxlength' => 100,));
                ?>
                <?php echo $form->error($model, 'file'); ?>
            </div>
        </div>
    </div>
    <br />
<?php endif; ?>
<?php if (Yii::app()->user->getState('id') != $model->id): ?>
    <div class="form-group">
        <div class="col-xs-12 col-sm-3">
            <?php echo $form->labelEx($model, 'estatus'); ?>
        </div>
        <div class="col-xs-12 col-sm-9">
            <?php
            $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
                'id' => 'RBACUsuarios_estatus',
                'attribute' => 'RBACUsuarios[estatus]',
                'state' => $model->estatus,
                'type' => 'item',
                'coloron' => 'color1'));
            ?>
            <?php echo $form->error($model, 'estatus'); ?>
        </div>
    </div>
<?php else: ?>
    <div class="form-group">
        <div class="col-xs-12 col-sm-9">
            <input id="RBACUsuarios_estatus" type="hidden" value="<?php echo $model->estatus; ?>" name="RBACUsuarios[estatus]">
        </div>
    </div>
<?php endif; ?>
<br />
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <p class="text-left">
            Campos con <span class="errorMessage">*</span> son requeridos.
        </p>
    </div>
    <div class="col-xs-12 col-sm-6">
        <ul class="cmenuhorizontal" id="yw0">
            <li class="bottom"><?php echo CHtml::submitButton(Yii::app()->params['save-text'], array('class' => Yii::app()->params['save-btn'])); ?></li>
            <li class="bottom"><?php echo CHtml::link(Yii::app()->params['cancel-text'], Yii::app()->controller->createUrl('index'), array('class' => Yii::app()->params['cancel-btn'])); ?></li>
        </ul>
    </div>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $.validator.addMethod("onlytextnumber", function (value, element)
        {
            return this.optional(element) || /^[0-9a-z]+$/i.test(value);
        }, "Sólo se aceptan letras y números.");
        $.validator.addMethod("onlytext", function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z]+$/.test(value);
        }, "Sólo se aceptan letras.");
        $.validator.addMethod("onlynumbers", function (value, element)
        {
            return this.optional(element) || /^[0-9]*$/.test(value);
        }, "Sólo se aceptan números.");
        $('#RBACUsuarios_rol_id').change(function () {
            if (this.value == 8)
            {
                $('#campoDist').fadeIn();
                $('#campoRazon').fadeOut();
                $('#conSaint').fadeOut();
                $('#campoDireccion').fadeOut();
                $('.campodv').fadeOut();
              
            } else
            if (this.value == 3 || this.value==5)
            {
                $('#conSaint').fadeIn();
                $('#campoRazon').fadeIn();
                $('#campoDireccion').fadeIn();
                $('.campodv').fadeIn();
                $('#campoDist').fadeOut();
                
            } else
            {
                $('#campoDist').fadeOut();
                $('#campoRazon').fadeOut();
                $('#conSaint').fadeOut();
                $('#campoDireccion').fadeOut();
                $('.campodv').fadeOut();
              
            }

        });
        $.validator.addMethod("codsaint", function (value, element) {
//            alert($('#RBACUsuarios[cod_saint]').val());
            if (($('#RBACUsuarios_rol_id').val() == 3 || $('#RBACUsuarios_rol_id').val() == 5) && $('#RBACUsuarios_cod_saint').val() =='') {
                return false;
            } else {
                return true;
            }

        }, "Codigo Saint es requerido");
        
         $.validator.addMethod("campoRazon", function (value, element) {
//            alert($('#RBACUsuarios[cod_saint]').val());
            if (($('#RBACUsuarios_rol_id').val() == 3 || $('#RBACUsuarios_rol_id').val() == 5) && $('#RBACUsuarios_razon_social').val() =='') {
                return false;
            } else {
                return true;
            }

        }, "Razon Social es requerido");
        
        $('#rbacusuarios-form').validate({
            debug: false,
            rules: {
                "RBACUsuarios[nombreusuario]": {
                    required: true,
                    onlytextnumber: true,
                },
                "RBACUsuarios[cod_saint]": {
                    codsaint: true
                },
                "RBACUsuarios[razon_social]": {
                    campoRazon: true
                },
                "RBACUsuarios[email]": {
                    required: true,
                    email: true,
                },
                "RBACUsuarios[nombre]": {
                    required: true,
                    onlytext: true,
                },
                "RBACUsuarios[apellido]": {
                    required: true,
                    onlytext: true,
                },
                "RBACUsuarios[contrasena]": {
                    required: false,
                },
                "RBACUsuarios[dni_numero]": {
                    required: true,
                },
                "RBACUsuarios[telf_local_tipo]": {
                    onlynumbers: true,
                },
                "RBACUsuarios[telf_local_numero]": {
                    onlynumbers: true,
                },
                "RBACUsuarios[rol_id]": {
                    required: true,
                },
            },
            messages: {
                "RBACUsuarios[nombreusuario]": {
                    required: "Nombre de usuario es obligatorio.",
                },
                "RBACUsuarios[email]": {
                    required: "Email es obligatorio.",
                    email: "Debe indicar un email válido.",
                },
                "RBACUsuarios[nombre]": {
                    required: "Nombre es obligatorio.",
                },
                "RBACUsuarios[apellido]": {
                    required: "Nombre es obligatorio.",
                },
                "RBACUsuarios[contrasena]": {
                    required: "Contraseña es obligatoria.",
                },
                "RBACUsuarios[dni_numero]": {
                    required: "Debe indicar el RUC.",
                },
                "RBACUsuarios[rol_id]": {
                    required: "Debe seleccionar el rol del usuario.",
                },
            }

        });
    });
</script>