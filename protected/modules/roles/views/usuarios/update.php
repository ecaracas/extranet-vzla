<?php
/* @var $this UsuariosController */
/* @var $model RBACUsuarios */

$this->breadcrumbs=array(
    'Rbacusuarioses'=>array('index'),
    $model->id=>array('view','id'=>$model->id),
    'Editar',
);

$this->menu=array(
    array(
        'label'=>Yii::app()->params['create-text'], 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label'=>Yii::app()->params['view-text'], 
        'url'=>array('view', 'id'=>$model->id), 
        'linkOptions' => array('class' => Yii::app()->params['view-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')),
    array(
        'label'=>Yii::app()->params['index-text'], 
        'url'=>array('index'), 
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>
<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading navbar-tool">

        <h3 class="panel-title"><?php echo  Yii::app()->params['update-text']; ?> Usuarios</h3>

        <div class="menu-tool">
            <?php 
            if(Yii::app()->authRBAC->checkAccess($this->modulo . '_menu'))
            {
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
            }
            ?>
        </div>
               <?php $this->ToolActionsRight(); ?>       
    </div>
    <div class="panel-body">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
            </div>
        </div>
    </div>
</div>