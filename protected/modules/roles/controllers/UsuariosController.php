<?php
class UsuariosController extends Controller
{
	public $modulo = "usuarios";
	
	/**
	 *
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 *      using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';
        
         public function behaviors() {
        return array(
            'eexcelview' => array(
                'class' => 'application.extensions.eexcelview.EExcelBehavior',
            ),
        );
    }
	
	/**
	 *
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete'); // we only allow deletion via POST request
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array(
						'allow', /* Acciones Permitidas */
									'actions' => array(
								'index',
								'view',
								'create',
								'update',
								'delete',
								'menu',
                                                                'exportexcel',
								'UpdateActive'),
						'users' => array(
								'*')),
				array(
						'deny',
						'users' => array(
								'*')));
	}
	
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		
		Yii::app()->params['title'] = '';
	}

	public function actionExportexcel($id = null)
	{
		if (isset($_SESSION['promotion.excel']))
		{
			$data = $_SESSION['promotion.excel'];
			$hoy = date("Y-m-d H:i:s");     
			$this->toExcel($data, array('rol.descripcion','nombreusuario','email','nombre','apellido','razon_social','dni_numero','telf_local_numero'),$hoy);
		}
	}

	public function actionView($id)
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_view'))
		{
			
			$model = $this->loadModel($id);
                        $model2 = new RBACUsuarios('tecnico');
			$model2->unsetAttributes();
                        if(isset($_GET['RBACUsuarios'])){
                            
                            $model2->attributes=$_GET['RBACUsuarios'];
                        }
			$this->render('view', array('model' => $model,'model2'=>$model2));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}

	public function actionCreate()
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_create') || isset($_POST['siteRegistro']))
		{
			$permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
			$asinc = false;
			$validPass = true;
			$fechaActual = date('d-m-Y h:m:s');
			$validateProgrammer = true;
			$estatus = 0;
			$model = new RBACUsuarios('create');
			$modelSO = new OperatingSystemsUser();
			$modelPL = new ProgrammingLanguagesUser();
			$modelPU = new ProductsUser();
			$modelRolesOp = new RBACRolesOpciones();
			// ///////////////////////////////////////////////////////////////////////////////////
			// BLOQUE PARA EL MANEJO DE REGISTRO DE USUARIOS PROVENIENTE DE LA VISTA DE LOGIN
			// SOLO SE REGISTRARAN USUARIOS DISTRIBUIDOR Y PROGRAMADOR
			if(isset($_POST['siteRegistro']))
			{
                           
				$asinc = true;
				$_POST['RBACUsuarios'] = json_decode($_POST['siteRegistro'], true);
				$estatus = 3;
				//$_POST['RBACUsuarios']['file'] = '';
				if($_POST['RBACUsuarios']['sistema_operativo'] != '' && $_POST['RBACUsuarios']['lenguaje_programacion'] != '' && $_POST['RBACUsuarios']['producto_integracion'] != '')
				{
					$modelSO->operatingsystem_id = $_POST['RBACUsuarios']['sistema_operativo'];
					$modelSO->created_at = $fechaActual;
					$modelSO->updated_at = $fechaActual;
					$modelPL->programming_languages_id = $_POST['RBACUsuarios']['lenguaje_programacion'];
					$modelPL->created_at = $fechaActual;
					$modelPL->updated_at = $fechaActual;
					$modelPU->product_id = $_POST['RBACUsuarios']['producto_integracion'];
					$modelPU->created_at = $fechaActual;
					$modelPU->updated_at = $fechaActual;
					$model->rol_id = 6;
				}
				else $validateProgrammer = false;
			}
			if(isset($_POST['RBACUsuarios']))
			{
                           // print_r($_POST); exit();
                                                      
                               	$model->estatus = $estatus;
				$model->attributes = $_POST['RBACUsuarios'];

				if(isset($_POST['RBACUsuarios']['address'])){
				$model->address = $_POST['RBACUsuarios']['address'];	
				}
				if(is_numeric($_POST['RBACUsuarios']['ruc_dv'])){
				$model->ruc_dv = $_POST['RBACUsuarios']['ruc_dv'];	
				} else {
					$model->ruc_dv = intval($_POST['RBACUsuarios']['ruc_dv']);	
				}
                                
                                if(isset($_POST['RBACUsuarios']['cod_saint'] )){
                                    $model->cod_saint=$_POST['RBACUsuarios']['cod_saint'];
                                }
				
				if(isset($_POST['RBACUsuarios']['softwarehouse']) && $_POST['RBACUsuarios']['softwarehouse'] != '')
				$model->$_POST['RBACUsuarios']['softwarehouse'];
				$model->registro_fecha = $this->getFecha();
				$model->registro_hora = $this->getHora();
				$model->registro_usuario_id = $asinc ? 4 : Yii::app()->user->id;
				$model->modificado_fecha = $this->getFecha();
				$model->modificado_hora = $this->getHora();
				$model->modificado_usuario_id = $asinc ? 4 : Yii::app()->user->id;
				$model->distribuidor_id = Yii::app()->user->getState('rol') == 3 ? Yii::app()->user->getState('id') : $_POST['RBACUsuarios']['distribuidor_id'];
				$erroUser = true;
				if($model->contrasena != '' && $model->contrasena != $_POST['RBACUsuarios']['confirmaPassword'])
				{
					$validPass = false;
				}
				$cadena = $model->nombreusuario;
				for ($i=0; $i<strlen($model->nombreusuario); $i++){
					if (strpos($permitidos, substr($cadena,$i,1))===false){
						$erroUser = false;
					}
				}
				if($model->rol_id != 3)
				{
					$model->razon_social = $model->nombre . ' ' . $model->apellido;
				}
                                //echo 'adsds'; exit();
				if($model->validate() && $validateProgrammer && $validPass && $erroUser)
				{
					$model->nombreusuario = strtolower($model->nombreusuario);
					//if(isset($_POST['RBACUsuarios']['file']) && $_POST['RBACUsuarios']['file'] != '')
                                        if(isset($_FILES['RBACUsuarios']) && $_FILES['RBACUsuarios']['name']['file'] != '')
					{
						$file = CUploadedFile::getInstance($model, "file");
                                                //$tmp = explode(".", $file->name);
                                                $tmp = explode(".", $_FILES['RBACUsuarios']['name']['file']);
						$extension = end($tmp);
						$name = $model->nombreusuario;
						$name = str_replace("_", " ", $name);
						$file->saveAs(Yii::getPathOfAlias("webroot") . "/img/profile/" . $name . "." . $extension);
						$model->file = $name . "." . $extension;
					}
					
					$pass = $model->contrasena;
					$model->contrasena = ValidateSafePassword::Hash($model->contrasena);
					if($model->save())
					{
						if(Yii::app()->user->getState('rol') == 3)
						{
							$model->distribuidor_id = Yii::app()->user->getState('id');
							$model->save();
						}
						elseif($model->rol_id != 8)
						{
							$model->distribuidor_id = $model->id;
							$model->save();
						}
						foreach ($modelRolesOp->findAll('rol_id = ' . $model->rol_id) as $value)
						{
							$modelRolesUser = new RBACRolesOpcionesUsuarios();
							$modelRolesUser->usuario_id = $model->id;
							$modelRolesUser->rol_opcion_id = $value['id'];
							$modelRolesUser->estatus = 1;
							$modelRolesUser->registro_fecha = date('Y-m-d');
							$modelRolesUser->registro_hora = date('H:m:s');
							$modelRolesUser->registro_usuario_id = (null !== Yii::app()->user->getState('id')) ? Yii::app()->user->getState('id') : 4;
							$modelRolesUser->modificado_fecha = date('Y-m-d');
							$modelRolesUser->modificado_hora = date('H:m:s');
							$modelRolesUser->modificado_usuario_id = (null !== Yii::app()->user->getState('id')) ? Yii::app()->user->getState('id') : 4;
							$modelRolesUser->secuencia = 1;
							$modelRolesUser->save();
						}
						//SE VALIDA LA DATA ASOCIADA A USUARIO PROGRAMADOR PROVENIENTE DE LA PANTALLA DE LOGIN.
						if($asinc)
						{
							$modelSO->user_id = $model->id;
							$modelPL->user_id = $model->id;
							$modelPU->user_id = $model->id;
							if(isset($_POST['siteRegistro']) && $modelSO->save() && $modelPL->save() && $modelPU->save())
							{
								//CORREOS INTERNOS QUE RECIBIRAN LA NOTIFICACION DE REGISTRO DE USUARIOS PROGRAMADORES
								$this->envioEmail(Yii::app()->params['correosInternos'], 'El usuario ' . Yii::app()->user->getState('nombreusuario') . 'registro a ' . $model->nombreusuario . ' con perfil ' . $model->rol->descripcion . ' para la extranet de Panama, y éste se encuentra a la espera de su activación.<br/>', 'The Factory Corp', 'Usuario Registrado');
								//CORREO DE CONFIRMACION DE REGISTRO ENVIADO AL USUARIO PROGRAMADOR REGISTRADO.
								$this->envioEmail($model->email, 'El registro para el usuario ' . $model->nombreusuario . ' fue exitosa. <br/>Su usuario es: ' . $model->nombreusuario . '<br/> Su contraseña : ' . $pass . '<br/>' . 'Espere a que el equipo de The Factory Corp verifique sus datos y proceda a su activación.<br/>', 'The Factory Corp', 'Usuario Registrado');
								echo CJSON::encode(array('message' => Yii::app()->params['msjsuccess'], 'success' => 'true'));
								return;
							}
							else
							{
								echo CJSON::encode(array('message' => Yii::app()->params['msjdanger'], 'success' => 'false'));
								return;
							}
						}
						else
						{
							//CORREOS INTERNOS QUE RECIBIRAN LA NOTIFICACION DE REGISTRO DE USUARIOS
							$correosInternos = Yii::app()->params['correosInternos'];
							array_push($correosInternos, Yii::app()->user->getState('emailUser'));
							$this->envioEmail($correosInternos, 'El usuario ' . Yii::app()->user->getState('nombreusuario') . ' registro a ' . $model->nombreusuario . ' con perfil ' . $model->rol->descripcion . ' para la extranet de Panama.<br/>', 'The Factory Corp', 'Usuario Registrado');
							//CORREO DE CONFIRMACION DE REGISTRO ENVIADO AL USUARIO PROGRAMADOR REGISTRADO.
							$this->envioEmail($model->email, 'El registro para el usuario fue exitosa. <br/>Su usuario es: ' . $model->nombreusuario . '<br/> Su contraseña : ' . $pass . '<br/>' . 'El equipo de The Factory Corp le da la bienvenida.<br/>', 'The Factory Corp', 'Usuario Registrado');
							Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
							$this->redirect(array('view', 'id' => $model->id));
						}
					}
					else
					{
						//SE DEVUELVE MENSAJE DE EXITO AL FORMULARIO DE REGISTRO DE USUARIO PROGRAMADOR VIA AJAX
						//SIEMPRE Y CUANDO LA PETICION SEA ASINCRONA
						if(!$asinc)
						{
							Yii::app()->user->setFlash('danger', CHtml::errorSummary($model));
						}
						else
						{
							echo CJSON::encode(array('message' => $model->getErrors(), 'success' => 'false'));
							return;
						}
					}
				}
				else
				{
					if(!$asinc)
					{
						if(!$validPass)
						{
							Yii::app()->user->setFlash('danger', 'Contraseñas no coinciden.');
						}
						elseif(!$erroUser)
						{
							Yii::app()->user->setFlash('danger', 'Nombre de usuario solo debe contener letras');
						}
						else
						{
							Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
						}
					}
					else
					{
						if(!$validPass)
						{
							echo CJSON::encode(array('message' => array('Contraseñas no coinciden.'), 'success' => 'false'));
							return;
						}
						else
						{
							echo CJSON::encode(array('message' => $model->getErrors(), 'success' => 'false'));
							return;
						}
					}
				}
			}
			if(!$asinc)
			{
				$this->render('create', array(
						'model' => $model));
			}
			else
			{
				echo CJSON::encode(array(
						'message' => Yii::app()->params['msjdanger'],
						'success' => 'false'));
				return;
			}
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionUpdate($id)
	{
		$pass = '';
		$validPass = true;
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_update'))
		{
			$model = $this->loadModel($id);
			if(isset($_POST['RBACUsuarios']))
			{
				$model->estatus = 0;
				$model->attributes = $_POST['RBACUsuarios'];
				if(isset($_POST['RBACUsuarios']['address']))
				{
					$model->address = $_POST['RBACUsuarios']['address'];
				}
				if(is_numeric($_POST['RBACUsuarios']['ruc_dv']))
				{
					$model->ruc_dv = $_POST['RBACUsuarios']['ruc_dv'];
				}
				else
				{
					$model->ruc_dv = intval($_POST['RBACUsuarios']['ruc_dv']);
				}
                                
                                if(isset($_POST['RBACUsuarios']['cod_saint'] )){
                                    $model->cod_saint=$_POST['RBACUsuarios']['cod_saint'];
                                }
				
				if($_POST['RBACUsuarios']['contrasena'] == '')
				{
					$model->contrasena = $_POST['oldPass'];
				}
				elseif($model->contrasena != $_POST['RBACUsuarios']['confirmaPassword'])
				{
					$validPass = false;
				}
				else 
				{
					$pass = $_POST['RBACUsuarios']['contrasena'];
					$model->contrasena = ValidateSafePassword::Hash($_POST['RBACUsuarios']['contrasena']);
				}
				if($validPass && $model->validate())
				{
					if(isset($_POST['RBACUsuarios']['file']))
					{
						$file = CUploadedFile::getInstance($model, "file");
						if (is_object($file)) 
						{
							$extension = end(explode(".", $file->name));
							$name = $model->nombreusuario;
							$name = str_replace("_", " ", $name);
							$file->saveAs(Yii::getPathOfAlias("webroot") . "/img/profile/" . $name . "." . $extension);
							$model->file = $name . "." . $extension;
						}
					}
					if($model->save())
					{
						//CORREOS INTERNOS QUE RECIBIRAN LA NOTIFICACION DE REGISTRO DE USUARIOS PROGRAMADORES
						$correosInternos = Yii::app()->params['correosInternos'];
						array_push($correosInternos, Yii::app()->user->getState('emailUser'));
						$this->envioEmail(Yii::app()->params['correosInternos'], 'El usuario <strong>' . Yii::app()->user->getState('nombreusuario') . '</strong> cambio la contraseña de ' . $model->nombreusuario . ' con perfil ' . $model->rol->descripcion, 'The Factory Corp', 'Cambio de Contraseña');
						//CORREO DE CONFIRMACION DE REGISTRO ENVIADO AL USUARIO PROGRAMADOR REGISTRADO.
						$this->envioEmail($model->email, 'El cambio de contraseña para ' . $model->nombreusuario . ' fue exitoso. <br/>Su nueva contraseña es: ' . $pass . '<br/>', 'The Factory Corp', 'Cambio de Contraseña');
						Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
						$this->redirect(array('view', 'id' => $model->id));
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
				{
					Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
				}
			}
			$this->render('update', array('model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionIndex()
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_index'))
		{
			$model = new RBACUsuarios('search');
			

			


			$model->unsetAttributes(); // clear any default values
			
			
                        if(isset($_GET['RBACUsuarios']))
			{
                                
				$model->attributes = $_GET['RBACUsuarios'];
			}
			
			if(isset($_GET['RBACUsuarios_sort']))
			{
				
				Yii::app()->params['GridViewOrder'] = TRUE;
			}
			
			$this->render('index', array('model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function loadModel($id)
	{
		$sql = new CDbCriteria();
		$sql->params = array(':id' => intval($id));
		$sql->condition = "id = :id";
		$sql->addcondition("estatus <> 9");
		$model = RBACUsuarios::model()->find($sql);
		if($model === null)
		{
			throw new CHttpException(404, Yii::app()->params['Error404']);
		}
		return $model;
	}
	
	public function actionMenu($id = NULL)
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_menu'))
		{
			
			/* Inicio de la Transaccion */
			$transaccion_sql = Yii::app()->db->beginTransaction();
			




			$model = new RBACUsuarios();
			
			$options = array();
			
			if(isset($id))
			{
				
				if($id != NULL)
				{
					
					$model->id = intval($id);
					
					$sql = new CDbCriteria();
					$sql->params = array(
							':id' => intval($model->id));
					$sql->condition = "id = :id";
					$sql->addcondition("estatus <> 9");
					$sql->addcondition("principal = 0");
					$model = RBACUsuarios::model()->find($sql);
					if($model != null)
					{
						
						if(isset($_POST['btnsave']))
						{
							
							RBACRolesOpcionesUsuarios::model()->updateAll(array(
									'estatus' => 0,
									'modificado_fecha' => $this->getFecha(),
									'modificado_hora' => $this->getHora()), "usuario_id = :usuario_id", array(
									':usuario_id' => $model->id));
							
							if(isset($_POST['Items']))
							{
								
								foreach($_POST['Items'] as $a => $b)
								{
									
									$useropcion = RBACRolesOpcionesUsuarios::model()->find(array(
											'condition' => "rol_opcion_id = :rol_opcion_id AND usuario_id = :usuario_id",
											'params' => array(
													':rol_opcion_id' => intval($a),
													':usuario_id' => $model->id)));
									
									if(count($useropcion) == 1)
									{
										$useropcion->estatus = 1;
									}
									else
									{
										$useropcion = new RBACRolesOpcionesUsuarios();
										$useropcion->rol_opcion_id = $a;
										$useropcion->usuario_id = $model->id;
										$useropcion->estatus = 1;
										$useropcion->registro_fecha = $this->getFecha();
										$useropcion->registro_hora = $this->getHora();
										$useropcion->registro_usuario_id = 1; // Yii::app()->user->id;
										$useropcion->modificado_fecha = $this->getFecha();
										$useropcion->modificado_hora = $this->getHora();
										$useropcion->modificado_usuario_id = 1; // Yii::app()->user->id;

										
									}
									if($useropcion->validate())
									{
										if($useropcion->save())
										{
											//echo 'prueba'; die();
										}
										else
										{
											/* Defino una Variable que mas abajo genere un Rollback */
											Yii::app()->params['rollback'] = TRUE;
										}
									}
									else
									{
										/* Defino una Variable que mas abajo genere un Rollback */
										Yii::app()->params['rollback'] = TRUE;
									}
								}
							}
							
							if(Yii::app()->params['rollback'] == FALSE)
							{
								/* Hago el Commit de todas las Transacciones */
								$transaccion_sql->commit();
								Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
							}
						}
						/* Consulta de Menus */
						$sql_menu = new CDbCriteria();
						
						$sql_menu->condition = "t.rol_id = :rol_id AND menuopcion.estatus = 1 AND menu.estatus = 1 AND t.estatus = 1 AND rol.estatus = 1";
						
						$sql_menu->with = array(
								'menuopcion' => array(
										'with' => 'menu'),
								'rol');
						
						$sql_menu->order = "menu.jerarquia ASC, menu.orden ASC, menuopcion.orden ASC";
						
						$sql_menu->params = array(
								':rol_id' => $model->rol_id);
						
						$MenuData = RBACRolesOpciones::model()->findAll($sql_menu);
						
						/* Organizo los Menu Principales */
						
						$menuList = array();
						
						foreach($MenuData as $row)
						{
							if($row->menuopcion->menu->jerarquia == 0 and $row->menuopcion->jerarquia == 1)
							{
								$menuList[$row->menuopcion->menu->id]['menu_id'] = $row->menuopcion->menu->id;
								$menuList[$row->menuopcion->menu->id]['menu'] = $row->menuopcion->menu->descripcion;
								$menuList[$row->menuopcion->menu->id]['menuicon'] = $row->menuopcion->menu->icono;

								$menuList[$row->menuopcion->menu->id]['opcion_id'] = $row->id;
								$menuList[$row->menuopcion->menu->id]['value'] = FALSE;
								$menuList[$row->menuopcion->menu->id]['jerarquia'] = $row->menuopcion->menu->jerarquia;
							}
						}
						
						/* Organizo los submenu */
						$submenuList = array();
						
						foreach($MenuData as $row)
						{
							if($row->menuopcion->menu->jerarquia > 0 and $row->menuopcion->jerarquia == 1)
							{
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['menu_id'] = $row->menuopcion->menu->id;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['menu'] = $row->menuopcion->menu->descripcion;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['menuicon'] = $row->menuopcion->menu->icono;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['opcion_id'] = $row->id;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['value'] = FALSE;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['jerarquia'] = $row->menuopcion->menu->jerarquia;
							}
						}
						
						/* Organizo las opciones del menu Principal */
						
						$opcionMenu = array();
						
						foreach($MenuData as $row)
						{
							if($row->menuopcion->menu->jerarquia == 0 and $row->menuopcion->jerarquia == 0)
							{
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menu_id'] = $row->menuopcion->id;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menu'] = $row->menuopcion->descripcion;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menuicon'] = $row->menuopcion->icono;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['opcion_id'] = $row->id;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['value'] = FALSE;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['jerarquia'] = $row->menuopcion->jerarquia;
							}
						}
						
						/* Organizo las opciones del submenu */
						
						$opcionSubmenu = array();
						
						foreach($MenuData as $row)
						{
							if($row->menuopcion->menu->jerarquia > 0 and $row->menuopcion->jerarquia == 0)
							{
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menu_id'] = $row->menuopcion->id;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menu'] = $row->menuopcion->descripcion;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menuicon'] = $row->menuopcion->icono;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['opcion_id'] = $row->id;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['value'] = FALSE;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['jerarquia'] = $row->menuopcion->jerarquia;
							}
						}
						
						/* Opciones Registradas */
						
						$sqlrol = new CDbCriteria();
						
						$sqlrol->condition = 't.usuario_id = :usuario_id AND menuopcion.estatus = 1 AND menu.estatus = 1 AND t.estatus = 1 AND rolopcion.estatus = 1';
						
						$sqlrol->params = array(
								':usuario_id' => intval($model->id));
						
						$sqlrol->with = array(
								'rolopcion' => array(
										'with' => array(
												'menuopcion' => array(
														'with' => 'menu'))),
								'usuario');
						
						$sqlrol->order = 'menu.jerarquia ASC, menu.orden ASC, menuopcion.orden ASC';
						
						$rolesitems = RBACRolesOpcionesUsuarios::model()->findAll($sqlrol);
						
						/* Verifico los Items Guardados del Menu */
						foreach($rolesitems as $row)
						{
							if($row->rolopcion->menuopcion->menu->jerarquia == 0 and $row->rolopcion->menuopcion->jerarquia == 1)
							{
								@$menuList[@$row->rolopcion->menuopcion->menu->id]['value'] = TRUE;
							}
						}
						/* Verifico los Items Guardados del SubMenu */
						foreach($rolesitems as $row)
						{
							if($row->rolopcion->menuopcion->menu->jerarquia > 0)
							{
								$submenuList[@$row->rolopcion->menuopcion->menu->jerarquia][@$row->rolopcion->menuopcion->menu->id]['value'] = TRUE;
							}
						}
						
						/* Verifico las opciones de los menu */
						foreach($rolesitems as $row)
						{
							if($row->rolopcion->menuopcion->menu->jerarquia == 0 and $row->rolopcion->menuopcion->jerarquia == 0)
							{
								$opcionMenu[@$row->rolopcion->menuopcion->menu->id][@$row->rolopcion->menuopcion->id]['value'] = TRUE;
							}
						}
						
						/* Verifico las opciones de los submenu */
						foreach($rolesitems as $row)
						{
							if($row->rolopcion->menuopcion->menu->jerarquia > 0 and $row->rolopcion->menuopcion->jerarquia == 0)
							{
								$opcionSubmenu[$row->rolopcion->menuopcion->menu->id][$row->rolopcion->menuopcion->id]['value'] = TRUE;
							}
						}
					}
					else
					{
						throw new CHttpException(404, Yii::app()->params['ErrorAccesoDenegado']);
					}
				}
			}
			$this->render('menu', array(
					'model' => $model,
					'menuList' => @$menuList,
					'submenuList' => @$submenuList,
					'opcionMenu' => @$opcionMenu,
					'opcionSubmenu' => @$opcionSubmenu));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionUpdateActive($it = NULL, $s = NULL)
	{
		$model = $this->loadModel($it);
		if($s == 1)
		{
			$model->estatus = 1;
		}
		elseif($s == 0)
		{
			$model->estatus = 0;
		}
		if($model->save())
		{
			$correosInternos = array('ecaracas@thefactoryhka.com');
			if($model->estatus == 1)
				$this->envioEmail($model->email, $model->nombre . ' su usuario ha sido activado, ya puede ingresar al sistema.', 'Activación de usuario The Factory Corp', 'Activación de usuario');
			echo CJSON::encode(array('message' => 'exito', 'success' => 'true'));
		}
		else
			echo CJSON::encode(array('message' => $model->getErrors(), 'success' => 'true'));
	}
}