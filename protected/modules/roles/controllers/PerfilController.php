<?php
class PerfilController extends Controller
{
	
	/**
	 *
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 *      using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';
	public $modulo = "home";
	/**
	 *
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete'); // we only allow deletion via POST request

	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		
           
            return array(
				array(
						'allow', /* Acciones Permitidas */
                                    'actions' => array(
								'index',
								'view',
								'create',
								'update',
								'delete',
								'menu',
								'UpdateActive'),
						'users' => array(
								'*')),
				array(
						'deny',
						'users' => array(
								'*')));
	}
	
	public function __construct($id, $module = null)
	{
		parent::__construct ( $id, $module );
		
		Yii::app ()->params['title'] = '';
	}
	public function actionView($id)
	{
			$model = $this->loadModel ( $id );
			
			$this->render ( 'view', array(
					'model' => $model) );
	}
	public function actionCreate()
	{
		if (Yii::app ()->authRBAC->checkAccess ( $this->modulo . '_create' ) || isset ( $_POST['siteRegistro'] ))
		{
			$asinc = false;
			// ///////////////////////////////////////////////////////////////////////////////////
			// BLOQUE PARA EL MANEJO DE REGISTRO DE USUARIOS PROVENIENTE DE LA VISTA DE LOGIN
			// SOLO SE REGISTRARAN USUARIOS DISTRIBUIDOR Y PROGRAMADOR
			if (isset($_POST['siteRegistro']))
			{
				$asinc = true;
				$_POST['RBACUsuarios'] = json_decode($_POST['siteRegistro'], true);
			}
			// //////////////////////////////////////////////////////////////////////////////////////////
			// //////////////////////////////////////////////////////////////////////////////////////////
			$model = new RBACUsuarios ();
			if (isset($_POST['RBACUsuarios']))
			{
				$model->estatus = 0;
				$model->attributes = $_POST['RBACUsuarios'];
				$model->registro_fecha = $this->getFecha ();
				$model->registro_hora = $this->getHora ();
				$model->registro_usuario_id = $asinc ? 4 : Yii::app ()->user->id;
				$model->modificado_fecha = $this->getFecha ();
				$model->modificado_hora = $this->getHora ();
				$model->modificado_usuario_id = $asinc ? 4 : Yii::app ()->user->id;
				if ($model->validate ())
				{
					$model->contrasena = ValidateSafePassword::Hash ( $model->contrasena );
					if ($model->save ())
					{
						if ($asinc)
						{
							echo CJSON::encode ( array(
									'message' => Yii::app ()->params['msjsuccess'],
									'success' => 'true') );
							return;
						}
						else
						{
							Yii::app ()->user->setFlash ( 'success', Yii::app ()->params['msjsuccess'] );
							$this->redirect ( array(
									'view',
									'id' => $model->id) );
						}
					}
					else
					{
						if (!$asinc)
						{
							Yii::app ()->user->setFlash ( 'danger', Yii::app ()->params['msjdanger'] );
						}
						else
						{
							echo CJSON::encode ( array(
									'message' => Yii::app ()->params['msjdanger'],
									'success' => 'false') );
							return;
						}
					}
				}
				else
				{
					if (!$asinc)
					{
						Yii::app ()->user->setFlash ( 'danger', Yii::app ()->params['msjdanger'] );
					}
					else
					{
						echo CJSON::encode ( array(
								'message' => Yii::app ()->params['msjdanger'],
								'success' => 'false') );
						return;
					}
				}
			}
			if (!$asinc)
			{
				$this->render ( 'create', array(
						'model' => $model) );
			}
			else
			{
				echo CJSON::encode ( array(
						'message' => Yii::app ()->params['msjdanger'],
						'success' => 'false') );
				return;
			}
		}
		else
		{
			throw new CHttpException ( 403, Yii::app ()->params['ErrorAccesoDenegado'] );
		}
	}
       
	public function actionUpdate($id)
	{
			$model = $this->loadModel ( $id );
			$contraseña = $model->contrasena;
			$model->contrasena = '';
			if (isset ( $_POST['RBACUsuarios'] ))
			{
				$model->estatus = 0;
				$model->attributes = $_POST['RBACUsuarios'];
				if ($_POST['RBACUsuarios']['contrasena'] == '')
				{
					unset ( $_POST['RBACUsuarios']['contrasena'] );
					$model->contrasena = $contraseña;
				}
				else
				{
					$model->contrasena = ValidateSafePassword::Hash ( $model->contrasena );
				}
				if ($model->validate ())
				{
                                          if(isset($_FILES)){
                                                $file = CUploadedFile::getInstance($model, "file");
                                                $extension = end(explode(".", $file->name));
                                                $name = $model->nombreusuario;
                                                $name = str_replace("_", " ", $name);
                                                $file->saveAs(Yii::getPathOfAlias("webroot") . "/img/profile/" . $name . "." . $extension);
                                                $model->file = $name . "." . $extension;

                                            }           
					if ($model->save ())
					{
						Yii::app ()->user->setFlash ( 'success', Yii::app ()->params['msjsuccess'] );
						$this->redirect ( array(
								'view',
								'id' => $model->id) );
					}
					else
					{
						Yii::app ()->user->setFlash ( 'danger', Yii::app ()->params['msjdanger'] );
					}
				}
				else
				{
					Yii::app ()->user->setFlash ( 'danger', Yii::app ()->params['msjdanger'] );
				}
			}
			
			$this->render ( 'update', array(
					'model' => $model) );
	}
	public function actionIndex()
	{
		if (Yii::app ()->authRBAC->checkAccess ( $this->modulo . '_index' ))
		{
			
			$model = new RBACUsuarios ( 'search' );
			
			$model->unsetAttributes (); // clear any default values
			
			if (isset ( $_GET['RBACUsuarios'] ))
			{
				$model->attributes = $_GET['RBACUsuarios'];
			}
			
			if (isset ( $_GET['RBACUsuarios_sort'] ))
			{
				
				Yii::app ()->params['GridViewOrder'] = TRUE;
			}
			
			$this->render ( 'index', array(
					'model' => $model) );
		}
		else
		{
			throw new CHttpException ( 403, Yii::app ()->params['ErrorAccesoDenegado'] );
		}
	}
	public function loadModel($id)
	{
		$sql = new CDbCriteria ();
		$sql->params = array(
				':id' => intval ( $id ));
		$sql->condition = "id = :id";
		$sql->addcondition ( "estatus <> 9" );
		$model = RBACUsuarios::model ()->find ( $sql );
		if ($model === null)
		{
			throw new CHttpException ( 404, Yii::app ()->params['Error404'] );
		}
		return $model;
	}
	public function actionMenu($id = NULL)
	{
		if (Yii::app ()->authRBAC->checkAccess ( $this->modulo . '_menu' ))
		{
			
			/* Inicio de la Transaccion */
			$transaccion_sql = Yii::app ()->db->beginTransaction ();
			
			$model = new RBACUsuarios ();
			
			$options = array();
			
			if (isset ( $id ))
			{
				
				if ($id != NULL)
				{
					
					$model->id = intval ( $id );
					
					$sql = new CDbCriteria ();
					$sql->params = array(
							':id' => intval ( $model->id ));
					$sql->condition = "id = :id";
					$sql->addcondition ( "estatus <> 9" );
					$sql->addcondition ( "principal = 0" );
					$model = RBACUsuarios::model ()->find ( $sql );
					if ($model != null)
					{
						
						if (isset ( $_POST['btnsave'] ))
						{
							
							RBACRolesOpcionesUsuarios::model ()->updateAll ( array(
									'estatus' => 0,
									'modificado_fecha' => $this->getFecha (),
									'modificado_hora' => $this->getHora ()), "usuario_id = :usuario_id", array(
									':usuario_id' => $model->id) );
							
							if (isset ( $_POST['Items'] ))
							{
								
								foreach($_POST['Items'] as $a => $b)
								{
									
									$useropcion = RBACRolesOpcionesUsuarios::model ()->find ( array(
											'condition' => "rol_opcion_id = :rol_opcion_id AND usuario_id = :usuario_id",
											'params' => array(
													':rol_opcion_id' => intval ( $a ),
													':usuario_id' => $model->id)) );
									
									if (count ( $useropcion ) == 1)
									{
										$useropcion->estatus = 1;
									}
									else
									{
										$useropcion = new RBACRolesOpcionesUsuarios ();
										$useropcion->rol_opcion_id = $a;
										$useropcion->usuario_id = $model->id;
										$useropcion->estatus = 1;
										$useropcion->registro_fecha = $this->getFecha ();
										$useropcion->registro_hora = $this->getHora ();
										$useropcion->registro_usuario_id = 1; // Yii::app()->user->id;
										$useropcion->modificado_fecha = $this->getFecha ();
										$useropcion->modificado_hora = $this->getHora ();
										$useropcion->modificado_usuario_id = 1; // Yii::app()->user->id;
									}
									if ($useropcion->validate ())
									{
										if ($useropcion->save ())
										{
										}
										else
										{
											/* Defino una Variable que mas abajo genere un Rollback */
											Yii::app ()->params['rollback'] = TRUE;
										}
									}
									else
									{
										/* Defino una Variable que mas abajo genere un Rollback */
										Yii::app ()->params['rollback'] = TRUE;
									}
								}
							}
							
							if (Yii::app ()->params['rollback'] == FALSE)
							{
								/* Hago el Commit de todas las Transacciones */
								$transaccion_sql->commit ();
								Yii::app ()->user->setFlash ( 'success', Yii::app ()->params['msjsuccess'] );
							}
						}
						/* Consulta de Menus */
						$sql_menu = new CDbCriteria ();
						
						$sql_menu->condition = "t.rol_id = :rol_id AND menuopcion.estatus = 1 AND menu.estatus = 1 AND t.estatus = 1 AND rol.estatus = 1";
						
						$sql_menu->with = array(
								'menuopcion' => array(
										'with' => 'menu'),
								'rol');
						
						$sql_menu->order = "menu.jerarquia ASC, menu.orden ASC, menuopcion.orden ASC";
						
						$sql_menu->params = array(
								':rol_id' => $model->rol_id);
						
						$MenuData = RBACRolesOpciones::model ()->findAll ( $sql_menu );
						
						/* Organizo los Menu Principales */
						
						$menuList = array();
						
						foreach($MenuData as $row)
						{
							if ($row->menuopcion->menu->jerarquia == 0 and $row->menuopcion->jerarquia == 1)
							{
								$menuList[$row->menuopcion->menu->id]['menu_id'] = $row->menuopcion->menu->id;
								$menuList[$row->menuopcion->menu->id]['menu'] = $row->menuopcion->menu->descripcion;
								$menuList[$row->menuopcion->menu->id]['menuicon'] = $row->menuopcion->menu->icono;
								$menuList[$row->menuopcion->menu->id]['opcion_id'] = $row->id;
								$menuList[$row->menuopcion->menu->id]['value'] = FALSE;
								$menuList[$row->menuopcion->menu->id]['jerarquia'] = $row->menuopcion->menu->jerarquia;
							}
						}
						
						/* Organizo los submenu */
						$submenuList = array();
						
						foreach($MenuData as $row)
						{
							if ($row->menuopcion->menu->jerarquia > 0 and $row->menuopcion->jerarquia == 1)
							{
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['menu_id'] = $row->menuopcion->menu->id;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['menu'] = $row->menuopcion->menu->descripcion;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['menuicon'] = $row->menuopcion->menu->icono;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['opcion_id'] = $row->id;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['value'] = FALSE;
								$submenuList[$row->menuopcion->menu->jerarquia][$row->menuopcion->menu->id]['jerarquia'] = $row->menuopcion->menu->jerarquia;
							}
						}
						
						/* Organizo las opciones del menu Principal */
						
						$opcionMenu = array();
						
						foreach($MenuData as $row)
						{
							if ($row->menuopcion->menu->jerarquia == 0 and $row->menuopcion->jerarquia == 0)
							{
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menu_id'] = $row->menuopcion->id;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menu'] = $row->menuopcion->descripcion;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menuicon'] = $row->menuopcion->icono;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['opcion_id'] = $row->id;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['value'] = FALSE;
								$opcionMenu[$row->menuopcion->menu->id][$row->menuopcion->id]['jerarquia'] = $row->menuopcion->jerarquia;
							}
						}
						
						/* Organizo las opciones del submenu */
						
						$opcionSubmenu = array();
						
						foreach($MenuData as $row)
						{
							if ($row->menuopcion->menu->jerarquia > 0 and $row->menuopcion->jerarquia == 0)
							{
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menu_id'] = $row->menuopcion->id;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menu'] = $row->menuopcion->descripcion;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['menuicon'] = $row->menuopcion->icono;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['opcion_id'] = $row->id;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['value'] = FALSE;
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['jerarquia'] = $row->menuopcion->jerarquia;
							}
						}
						
						/* Opciones Registradas */
						
						$sqlrol = new CDbCriteria ();
						
						$sqlrol->condition = 't.usuario_id = :usuario_id AND menuopcion.estatus = 1 AND menu.estatus = 1 AND t.estatus = 1 AND rolopcion.estatus = 1';
						
						$sqlrol->params = array(
								':usuario_id' => intval ( $model->id ));
						
						$sqlrol->with = array(
								'rolopcion' => array(
										'with' => array(
												'menuopcion' => array(
														'with' => 'menu'))),
								'usuario');
						
						$sqlrol->order = 'menu.jerarquia ASC, menu.orden ASC, menuopcion.orden ASC';
						
						$rolesitems = RBACRolesOpcionesUsuarios::model ()->findAll ( $sqlrol );
						
						/* Verifico los Items Guardados del Menu */
						foreach($rolesitems as $row)
						{
							if ($row->rolopcion->menuopcion->menu->jerarquia == 0 and $row->rolopcion->menuopcion->jerarquia == 1)
							{
								@$menuList[@$row->rolopcion->menuopcion->menu->id]['value'] = TRUE;
							}
						}
						/* Verifico los Items Guardados del SubMenu */
						foreach($rolesitems as $row)
						{
							if ($row->rolopcion->menuopcion->menu->jerarquia > 0)
							{
								$submenuList[@$row->rolopcion->menuopcion->menu->jerarquia][@$row->rolopcion->menuopcion->menu->id]['value'] = TRUE;
							}
						}
						
						/* Verifico las opciones de los menu */
						foreach($rolesitems as $row)
						{
							if ($row->rolopcion->menuopcion->menu->jerarquia == 0 and $row->rolopcion->menuopcion->jerarquia == 0)
							{
								$opcionMenu[@$row->rolopcion->menuopcion->menu->id][@$row->rolopcion->menuopcion->id]['value'] = TRUE;
							}
						}
						
						/* Verifico las opciones de los submenu */
						foreach($rolesitems as $row)
						{
							if ($row->rolopcion->menuopcion->menu->jerarquia > 0 and $row->rolopcion->menuopcion->jerarquia == 0)
							{
								$opcionSubmenu[$row->rolopcion->menuopcion->menu->id][$row->rolopcion->menuopcion->id]['value'] = TRUE;
							}
						}
					}
					else
					{
						throw new CHttpException ( 404, Yii::app ()->params['ErrorAccesoDenegado'] );
					}
				}
			}
			$this->render ( 'menu', array(
					'model' => $model,
					'menuList' => @$menuList,
					'submenuList' => @$submenuList,
					'opcionMenu' => @$opcionMenu,
					'opcionSubmenu' => @$opcionSubmenu) );
		}
		else
		{
			throw new CHttpException ( 403, Yii::app ()->params['ErrorAccesoDenegado'] );
		}
	}
	public function actionUpdateActive($it = NULL, $s = NULL)
	{
		$model = $this->loadModel ( $it );
		if ($s == 1){
			$model->estatus = 1;
		}
		elseif ($s == 0){
			$model->estatus = 0;
		}
		if ($model->validate ()){
			if ($model->save()){
			 $this->enviar($model->email, $model->nombre );				
			}
		}
	}
	public function enviar($email = NULL, $nombre = NULL)
	{
		$mailer = Yii::createComponent ( 'application.extensions.mailer.phpmailer', true );
		$mailer->AddAddress ( 'ggomez@thefactoryhka.com' );
		$mailer->AddAddress ( 'jramirez@thefactoryhka.com' );
		$mailer->From = 'contactanos@thefactory.com.ve';
		$mailer->CharSet = 'UTF-8';
		$mailer->FromName = 'Activación de usuario TFHKA Panama';
		$mailer->WordWrap = 50;
		$mailer->IsHTML ( true );
		$mailer->Subject = "Activación de usuario TFHKA Panama";
		$mailer->Body = '
					<style>
					.comentario{width: 100%;word-wrap: break-word;} .container{padding-right:10px;padding-left:10px;margin-right:auto;margin-left:auto}#header,body{margin:0;padding:0}@media (min-width:768px){.container{width:100%}}@media (min-width:992px){.container{width:100%}}@media (min-width:1200px){.container{width:1000px}}body{color:#555;font:400 10pt Arial,Helvetica,sans-serif;background:#F5F5F5}#header{padding-top: 15px;border-top:0 solid #C9E0ED}#page{margin-top:0;margin-bottom:5px;background:#fff;border:0 solid #C9E0ED}#content{padding:20px}.centrado{display:block;margin-left:auto;margin-right:auto}hr{color:#f5f5f5}
						</style>
							<body>
								<div class="container" id="page">
									<div  id="header">
										<p><a  href="" target="_blank"><img class="centrado"  alt="The Factory HKA" src="http://thefactoryhka.com/extranet-pa/img/logos/logo.jpg" style="height:123px; width:287px" /></a></p>
									</div>
									<div id="content">
										<p>"' . $nombre . '" su suario ha sido activado, ya puede ingresar al sistema </p>
										<br />
										<p><b>NO RESPONDER ESTE EMAIL</b></p>
										<p><b>Atentamente</b></p>
										<p><b>The Factory HKA</b></p>
									</div>
								</div>
							</body>';
		$mailer->IsSMTP ();
		$mailer->SMTPAuth = false;
		$mailer->Username = "contacto@thefactory.com.ve";
		$mailer->Mailer = "smtp-relay.gmail.com";
		$mailer->send ();
		return $mailer;
	}
}
