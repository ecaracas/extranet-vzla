<?php
class RolesController extends Controller
{
	public $modulo = "roles";
	
	/**
	 *
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 *      using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';
	
	/**
	 *
	 * @return array action filters
	 */
	public function filters()
	{

		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete' );
	 // we only allow deletion via POST request
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{

		return array(
				array(
						'allow', /* Acciones Permitidas */
                'actions' => array(
								'index',
								'view',
								'create',
								'update',
								'delete',
								'menu' ),
						'users' => array(
								'*' ) ),
				array(
						'deny',
						'users' => array(
								'*' ) ) );
	
	}
	
	public function __construct($id, $module = null)
	{

		parent::__construct($id, $module);
		
		Yii::app()->params['title'] = '';
	
	}
	
	public function actionView($id)
	{
            
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_view'))
		{
			
			$model = $this->loadModel($id);
			
			$this->render('view', array(
					'model' => $model ));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	
	}
	
	public function actionCreate()
	{

		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_create'))
		{
			
			$model = new RBACRoles();
			
			if(isset($_POST['RBACRoles']))
			{
				$model->estatus = 0;
				$model->attributes = $_POST['RBACRoles'];
				$model->registro_fecha = Date('Y-m-d');
				$model->registro_hora = Date('H:i:s');
				$model->registro_usuario_id = Yii::app() -> user -> id;;
				$model->modificado_fecha = Date('Y-m-d');
				$model->modificado_hora = Date('H:i:s');
				$model->modificado_usuario_id = Yii::app() -> user -> id;;
				$model->principal = 1;
				$model->secuencia = 1;
				if($model->validate())
				{
					if($model->save())
					{
						
						Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model->id ));
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
				{
					Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
				}
			}
			
			$this->render('create', array(
					'model' => $model ));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	
	}
	
	public function actionUpdate($id)
	{

		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_update'))
		{
			
			$model = $this->loadModel($id);
			
			if(isset($_POST['RBACRoles']))
			{
				$model->estatus = 0;
				$model->attributes = $_POST['RBACRoles'];
				
				if($model->validate())
				{
					if($model->save())
					{
						Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model->id ));
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
				{
					Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
				}
			}
			
			$this->render('update', array(
					'model' => $model ));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	
	}
	
	public function actionIndex()
	{

		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_index'))
		{
			$model = new RBACRoles('search');
			$model->unsetAttributes(); // clear any default values
			if(isset($_GET['RBACRoles']))
			{
				$model->attributes = $_GET['RBACRoles'];
			}
			if(isset($_GET['RBACRoles_sort']))
			{
				Yii::app()->params['GridViewOrder'] = TRUE;
			}
			$this->render('index', array(
					'model' => $model ));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	
	}
	
	public function loadModel($id)
	{

		$sql = new CDbCriteria();
		$sql->params = array(
				':id' => intval($id) );
		$sql->condition = "id = :id";
		$sql->addcondition("estatus <> 9");
		$model = RBACRoles::model()->find($sql);
		if($model === null)
		{throw new CHttpException(404, Yii::app()->params['Error404']);}
		return $model;
	
	}
	
	public function actionMenu($id = NULL)
	{

		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_menu'))
		{
			
			/* Inicio de la Transaccion */
			$transaccion_sql = Yii::app()->db->beginTransaction();
			
			$model = new RBACRoles();
			
			$options = array();
			
			if(isset($id))
			{
				
				if($id != NULL)
				{
					
					$model->id = intval($id);
					
					$sql = new CDbCriteria();
					$sql->params = array(
							':id' => intval($model->id) );
					$sql->condition = "id = :id";
					$sql->addcondition("estatus <> 9");
					// $sql->addcondition("principal = 0");
					$model = RBACRoles::model()->find($sql);
					if($model != null)
					{
						
						if(isset($_POST['btnsave']))
						{
							
							RBACRolesOpciones::model()->updateAll(array(
									'estatus' => 0,
									'modificado_fecha' => $this->getFecha(),
									'modificado_hora' => $this->getHora() ), "rol_id = :rol_id", array(
									':rol_id' => $model->id ));
							
							if(isset($_POST['Items']))
							{
								
								foreach($_POST['Items'] as $a => $b)
								{
									
									$rol = RBACRolesOpciones::model()->find(array(
											'condition' => "menu_opcion_id = :item_id AND rol_id = :rol_id",
											'params' => array(
													':item_id' => intval($a),
													':rol_id' => $model->id ) ));
									
									if(count($rol) == 1)
									{
										$rol->estatus = 1;
									}
									else
									{
										$rol = new RBACRolesOpciones();
										$rol->menu_opcion_id = $a;
										$rol->rol_id = $model->id;
										$rol->estatus = 1;
										$rol->registro_fecha = $this->getFecha();
										$rol->registro_hora = $this->getHora();
										$rol->registro_usuario_id = 1; // Yii::app()->user->id;
										$rol->modificado_fecha = $this->getFecha();
										$rol->modificado_hora = $this->getHora();
										$rol->modificado_usuario_id = 1; // Yii::app()->user->id;
									}
									if($rol->validate())
									{
										if($rol->save())
										{}
										else
										{
											/* Defino una Variable que mas abajo genere un Rollback */
											Yii::app()->params['rollback'] = TRUE;
										}
									}
									else
									{
										/* Defino una Variable que mas abajo genere un Rollback */
										Yii::app()->params['rollback'] = TRUE;
									}
								}
							}
							
							if(Yii::app()->params['rollback'] == FALSE)
							{
								/* Hago el Commit de todas las Transacciones */
								$transaccion_sql->commit();
								Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
							}
						}
						/* Consulta de Menus */
						$sql_menu = new CDbCriteria();
						
						$sql_menu->condition = "t.estatus = 1 AND menu.estatus = 1";
						
						$sql_menu->with = array(
								'menu' );
						
						$sql_menu->order = "menu.jerarquia ASC, menu.orden ASC, t.orden ASC";
						
						$MenuData = RBACMenuOpciones::model()->findAll($sql_menu);
						
						/* Organizo los Menu Principales */
						
						$menuList = array();
						
						foreach($MenuData as $row)
						{
							if($row->menu->jerarquia == 0 and $row->jerarquia == 1)
							{
								$menuList[$row->menu->id]['menu_id'] = $row->menu->id;
								$menuList[$row->menu->id]['menu'] = $row->menu->descripcion;
								$menuList[$row->menu->id]['menuicon'] = $row->menu->icono;
								$menuList[$row->menu->id]['opcion_id'] = $row->id;
								$menuList[$row->menu->id]['value'] = FALSE;
								$menuList[$row->menu->id]['jerarquia'] = $row->menu->jerarquia;
							}
						}
						
						/* Organizo los submenu */
						$submenuList = array();
						
						foreach($MenuData as $row)
						{
							if($row->menu->jerarquia > 0 and $row->jerarquia == 1)
							{
								$submenuList[$row->menu->jerarquia][$row->menu->id]['menu_id'] = $row->menu->id;
								$submenuList[$row->menu->jerarquia][$row->menu->id]['menu'] = $row->menu->descripcion;
								$submenuList[$row->menu->jerarquia][$row->menu->id]['menuicon'] = $row->menu->icono;
								$submenuList[$row->menu->jerarquia][$row->menu->id]['opcion_id'] = $row->id;
								$submenuList[$row->menu->jerarquia][$row->menu->id]['value'] = FALSE;
								$submenuList[$row->menu->jerarquia][$row->menu->id]['jerarquia'] = $row->menu->jerarquia;
							}
						}
						
						/* Organizo las opciones del menu Principal */
						
						$opcionMenu = array();
						
						foreach($MenuData as $row)
						{
							if($row->menu->jerarquia == 0 and $row->jerarquia == 0)
							{
								$opcionMenu[$row->menu->id][$row->id]['menu_id'] = $row->id;
								$opcionMenu[$row->menu->id][$row->id]['menu'] = $row->descripcion;
								$opcionMenu[$row->menu->id][$row->id]['menuicon'] = $row->icono;
								$opcionMenu[$row->menu->id][$row->id]['opcion_id'] = $row->id;
								$opcionMenu[$row->menu->id][$row->id]['value'] = FALSE;
								$opcionMenu[$row->menu->id][$row->id]['jerarquia'] = $row->jerarquia;
							}
						}
						
						/* Organizo las opciones del submenu */
						
						$opcionSubmenu = array();
						
						foreach($MenuData as $row)
						{
							if($row->menu->jerarquia > 0 and $row->jerarquia == 0)
							{
								$opcionSubmenu[$row->menu->id][$row->id]['menu_id'] = $row->id;
								$opcionSubmenu[$row->menu->id][$row->id]['menu'] = $row->descripcion;
								$opcionSubmenu[$row->menu->id][$row->id]['menuicon'] = $row->icono;
								$opcionSubmenu[$row->menu->id][$row->id]['opcion_id'] = $row->id;
								$opcionSubmenu[$row->menu->id][$row->id]['value'] = FALSE;
								$opcionSubmenu[$row->menu->id][$row->id]['jerarquia'] = $row->jerarquia;
							}
						}
						
						/* Opciones Registradas */
						
						$sqlrol = new CDbCriteria();
						
						$sqlrol->condition = 't.rol_id = :rol_id AND menuopcion.estatus = 1 AND menu.estatus = 1 AND t.estatus = 1 AND rol.estatus = 1';
						
						$sqlrol->params = array(
								':rol_id' => intval($model->id) );
						
						$sqlrol->with = array(
								'menuopcion' => array(
										'with' => 'menu' ),
								'rol' );
						
						$sqlrol->order = 'menu.jerarquia ASC, menu.orden ASC, menuopcion.orden ASC';
						
						$rolesitems = RBACRolesOpciones::model()->findAll($sqlrol);
						
						/* Verifico los Items Guardados del Menu */
						foreach($rolesitems as $row)
						{
							if($row->menuopcion->menu->jerarquia == 0 and $row->menuopcion->jerarquia == 1)
							{
								@$menuList[@$row->menuopcion->menu->id]['value'] = TRUE;
							}
						}
						/* Verifico los Items Guardados del SubMenu */
						foreach($rolesitems as $row)
						{
							if($row->menuopcion->menu->jerarquia > 0)
							{
								$submenuList[@$row->menuopcion->menu->jerarquia][@$row->menuopcion->menu->id]['value'] = TRUE;
							}
						}
						
						/* Verifico las opciones de los menu */
						foreach($rolesitems as $row)
						{
							if($row->menuopcion->menu->jerarquia == 0 and $row->menuopcion->jerarquia == 0)
							{
								$opcionMenu[@$row->menuopcion->menu->id][@$row->menuopcion->id]['value'] = TRUE;
							}
						}
						
						/* Verifico las opciones de los submenu */
						foreach($rolesitems as $row)
						{
							if($row->menuopcion->menu->jerarquia > 0 and $row->menuopcion->jerarquia == 0)
							{
								$opcionSubmenu[$row->menuopcion->menu->id][$row->menuopcion->id]['value'] = TRUE;
							}
						}
					}
					else
					{
						throw new CHttpException(404, Yii::app()->params['ErrorAccesoDenegado']);
					}
				}
			}
			$this->render('menu', array(
					'model' => $model,
					'menuList' => @$menuList,
					'submenuList' => @$submenuList,
					'opcionMenu' => @$opcionMenu,
					'opcionSubmenu' => @$opcionSubmenu ));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	
	}

}
