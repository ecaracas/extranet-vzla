<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property string $id
 * @property string $name
 * @property integer $active
 * @property string $model
 * @property integer $score
 * @property string $trademark
 * @property integer $featured_product
 * @property string $sigla
 *
 * The followings are the available model relations:
 * @property ProductsBrand[] $productsBrands
 * @property ProductsProductsType[] $productsProductsTypes
 */
class Products_OLD extends CActiveRecord
{
	
	public function tableName()
	{
		return 'products';
	}

	
	public function rules()
	{
		
		return array(
			array('active, trademark, sigla', 'required'),
			array('active, score, featured_product', 'numerical', 'integerOnly'=>true),
			array('trademark', 'length', 'max'=>10),
			array('sigla', 'length', 'max'=>6),
			array('name, model', 'safe'),
		
			array('id, name, active, model, score, trademark, featured_product, sigla', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'productsBrands' => array(self::HAS_MANY, 'ProductsBrand', 'product'),
			'productsProductsTypes' => array(self::HAS_MANY, 'ProductsProductsType', 'product'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'name' => Yii::t('lang','NOMBRE'),
			'active' => Yii::t('lang','ESTATUS'),
			'model' => Yii::t('lang','MODELO'),
			'score' => Yii::t('lang','Score'),
			'trademark' => Yii::t('lang','MARCA REGISTRADA'),
			'featured_product' => Yii::t('lang','Featured Product'),
			'sigla' => Yii::t('lang','SIGLAS'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('trademark',$this->trademark,true);
		$criteria->compare('featured_product',$this->featured_product);
		$criteria->compare('sigla',$this->sigla,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		// $criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
