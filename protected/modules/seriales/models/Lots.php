<?php

/**
 * This is the model class for table "lots".
 *
 * The followings are the available columns in table 'lots':
 * @property integer $id
 * @property integer $user_id
 * @property integer $brand_id
 * @property integer $model_id
 * @property integer $initial
 * @property integer $quantity
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Models $model
 * @property RbacUsuarios $user
 * @property Brands $brand
 * @property Machines[] $machines
 */
class Lots extends CActiveRecord
{
	
	public function tableName()
	{
		return 'lots';
	}

	
	public function rules()
	{
		
		return array(
			array('user_id, quantity, date', 'required'),
			array('user_id, initial, quantity', 'numerical', 'integerOnly'=>true),
		
			array('id, user_id, initial, quantity, date', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'model' => array(self::BELONGS_TO, 'Models', 'model_id'),
			'user' => array(self::BELONGS_TO, 'RbacUsuarios', 'user_id'),
			'brand' => array(self::BELONGS_TO, 'Brands', 'brand_id'),
			'machines' => array(self::HAS_MANY, 'Machines', 'lot_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'user_id' => Yii::t('lang','User'),			
			'initial' => Yii::t('lang','Initial'),
			'quantity' => Yii::t('lang','Quantity'),
			'date' => Yii::t('lang','Date'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);		
		$criteria->compare('initial',$this->initial);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('date',$this->date,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
