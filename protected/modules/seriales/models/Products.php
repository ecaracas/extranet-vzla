<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property string $id
 * @property string $name
 * @property integer $active
 * @property string $model
 * @property integer $score
 * @property string $trademark
 * @property string $featured_product
 * @property string $sigla
 *
 * The followings are the available model relations:
 * @property ProductsBrand[] $productsBrands
 * @property ProductsProductsType[] $productsProductsTypes
 */
class Products extends CActiveRecord
{
	public $brands_models;
	public $productos;
	public function tableName()
	{
		return 'products';
	}

	     public function behaviors(){
            return array(
                // Classname => path to Class
                'LogsableBehavior'=>'application.behaviors.LogsableBehavior',

            );
        }

	
	public function rules()
	{
		
		return array(
			array('active, sigla', 'required'),
			array('active, score', 'numerical', 'integerOnly'=>true),
 // para poder subir un archivo
			// array(
			// 	'trademark',
			// 	'file',
			// 	'allowEmpty' => false,
			// 	'types' => 'jpg, gif, png',
			// 	'on'=> 'insert',
			// 	'except' => 'update',
			// 	'message' => 'es necesario cargar el logo',
			// 	'wrongType' => 'Tipo de archivo invalido debe ser jpg,png o gif.',
			// 	'maxSize' => 1000024,
			// 	'maxFiles' => 1,
			// 	'tooSmall' => 'Tamaño del archivo muy bajo debe ser mayor a 100mb',
			// 	'tooMany'=> 'solo puede subir un archivo.'
			// ),
			array('trademark', 'length', 'max'=>10),
// ---------------------------------------
			array('featured_product', 'length', 'max'=>250),
			array('sigla', 'length', 'max'=>6),
			array('name, model', 'safe'),
		
			array('id, name, active, model, score, trademark, featured_product, sigla,brands_models', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'productsBrands' => array(self::HAS_MANY, 'ProductsBrand', 'product'),
			'productsProductsTypes' => array(self::HAS_MANY, 'ProductsProductsType', 'product'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','#'),
			'name' => Yii::t('lang','Nombre'),
			'active' => Yii::t('lang','Estatus'),
			'model' => Yii::t('lang','Modelo'),
			'score' => Yii::t('lang','Posicion'),
			'trademark' => Yii::t('lang','Logo'),
			'featured_product' => Yii::t('lang','Caracteristicas'),
			'sigla' => Yii::t('lang','Siglas'),
			'brands_models'=> Yii::t('lang','Marca')
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;
		// $criteria->with = array('ProductsBrand');

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('trademark',$this->trademark,true);
		$criteria->compare('featured_product',$this->featured_product,true);
		$criteria->compare('sigla',$this->sigla,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		// $criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchWithBrands()
	{
		$criteria=new CDbCriteria;
		// $criteria->with = array('ProductsBrand');

		$criteria->select= array('t.*, products_types.name as brands_models');
        $criteria->join ='LEFT JOIN products_brand on products_brand.product = t.id LEFT JOIN products_types on products_brand.product_type = products_types.id ';
        //$Criteria->condition = "products_brand.product = t.id and products_brand.product_type = products_types.id";
        $criteria->condition = "1 =  :value";

		  $criteria->params= array(":value" => 1);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('trademark',$this->trademark,true);
		$criteria->compare('featured_product',$this->featured_product,true);
		$criteria->compare('sigla',$this->sigla,true);
		$criteria->compare('brands_models',$this->brands_models,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		// $criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id, brands_models DESC";		
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getBrand()
	{
		
	}













	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
