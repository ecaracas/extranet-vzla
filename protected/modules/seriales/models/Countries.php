<?php

/**
 * This is the model class for table "countries".
 *
 * The followings are the available columns in table 'countries':
 * @property string $id
 * @property string $name
 * @property string $logo_path
 * @property string $flag_path
 * @property integer $order
 * @property string $slogan
 * @property string $url
 * @property string $company_name
 * @property string $address
 * @property string $coordinates
 * @property string $phone_number
 * @property string $zipcode
 * @property string $facebook
 * @property string $twitter
 * @property string $youtube
 * @property string $script_seo
 * @property string $script_analytics
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property BrandsCountry[] $brandsCountries
 * @property ContactInfo[] $contactInfos
 * @property FeaturedProductsCountry[] $featuredProductsCountries
 * @property MainmenuCountries[] $mainmenuCountries
 * @property NewsCountry[] $newsCountries
 * @property ProductsFilesCountries[] $productsFilesCountries
 * @property ProductsTypesCountries[] $productsTypesCountries
 * @property SlidersCountries[] $slidersCountries
 * @property Texts[] $texts
 * @property TypesCountries[] $typesCountries
 */
class Countries extends CActiveRecord
{
	 public function getDbConnection() {
            return Yii::app()->dbtfhka;
        }
	public function tableName()
	{
		return 'countries';
	}

	
	public function rules()
	{
		
		return array(
			array('id, name, logo_path, flag_path, order, url, company_name, address, coordinates, phone_number, active', 'required'),
			array('order, active', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>3),
			array('slogan, zipcode, facebook, twitter, youtube, script_seo, script_analytics', 'safe'),
		
			array('id, name, logo_path, flag_path, order, slogan, url, company_name, address, coordinates, phone_number, zipcode, facebook, twitter, youtube, script_seo, script_analytics, active', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'brandsCountries' => array(self::HAS_MANY, 'BrandsCountry', 'country_id'),
			'contactInfos' => array(self::HAS_MANY, 'ContactInfo', 'country_id'),
			'featuredProductsCountries' => array(self::HAS_MANY, 'FeaturedProductsCountry', 'country_id'),
			'mainmenuCountries' => array(self::HAS_MANY, 'MainmenuCountries', 'country_id'),
			'newsCountries' => array(self::HAS_MANY, 'NewsCountry', 'country_id'),
			'productsFilesCountries' => array(self::HAS_MANY, 'ProductsFilesCountries', 'country_id'),
			'productsTypesCountries' => array(self::HAS_MANY, 'ProductsTypesCountries', 'country_id'),
			'slidersCountries' => array(self::HAS_MANY, 'SlidersCountries', 'country_id'),
			'texts' => array(self::HAS_MANY, 'Texts', 'country_id'),
			'typesCountries' => array(self::HAS_MANY, 'TypesCountries', 'country_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'name' => Yii::t('lang','Name'),
			'logo_path' => Yii::t('lang','Logo Path'),
			'flag_path' => Yii::t('lang','Flag Path'),
			'order' => Yii::t('lang','Order'),
			'slogan' => Yii::t('lang','Slogan'),
			'url' => Yii::t('lang','Url'),
			'company_name' => Yii::t('lang','Company Name'),
			'address' => Yii::t('lang','Address'),
			'coordinates' => Yii::t('lang','Coordinates'),
			'phone_number' => Yii::t('lang','Phone Number'),
			'zipcode' => Yii::t('lang','Zipcode'),
			'facebook' => Yii::t('lang','Facebook'),
			'twitter' => Yii::t('lang','Twitter'),
			'youtube' => Yii::t('lang','Youtube'),
			'script_seo' => Yii::t('lang','Script Seo'),
			'script_analytics' => Yii::t('lang','Script Analytics'),
			'active' => Yii::t('lang','Active'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('logo_path',$this->logo_path,true);
		$criteria->compare('flag_path',$this->flag_path,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('slogan',$this->slogan,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('company_name',$this->company_name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('coordinates',$this->coordinates,true);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('twitter',$this->twitter,true);
		$criteria->compare('youtube',$this->youtube,true);
		$criteria->compare('script_seo',$this->script_seo,true);
		$criteria->compare('script_analytics',$this->script_analytics,true);
		$criteria->compare('active',$this->active);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
