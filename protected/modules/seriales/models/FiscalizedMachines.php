<?php

/**
 * This is the model class for table "fiscalized_machines".
 *
 * The followings are the available columns in table 'fiscalized_machines':
 * @property integer $id
 * @property integer $final_clients_id
 * @property string $date
 * @property string $ruc_technician
 * @property string $ruc_dealer
 * @property string $observations
 * @property integer $machine_id
 *
 * The followings are the available model relations:
 * @property FinalClients $finalClients
 * @property Machines $machine
 */
class FiscalizedMachines extends CActiveRecord
{
	public function behaviors()
	{
		return array(
				// Classname => path to Class
				'LogsableBehavior' => 'application.behaviors.LogsableBehavior');
	}
	public function tableName()
	{
		return 'fiscalized_machines';
	}

	
	public function rules()
	{
		
		return array(
			array('final_clients_id, date, ruc_technician, ruc_dealer, machine_id', 'required'),
			array('final_clients_id, machine_id', 'numerical', 'integerOnly'=>true),
			array('ruc_technician, ruc_dealer', 'length', 'max'=>45),
			array('observations', 'safe'),
		
			array('id, final_clients_id, date, ruc_technician, ruc_dealer, observations, machine_id', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'finalClients' => array(self::BELONGS_TO, 'FinalClients', 'final_clients_id'),
			'machine' => array(self::BELONGS_TO, 'Machines', 'machine_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'final_clients_id' => Yii::t('lang','Final Clients'),
			'date' => Yii::t('lang','Date'),
			'ruc_technician' => Yii::t('lang','Ruc Technician'),
			'ruc_dealer' => Yii::t('lang','Ruc Dealer'),
			'observations' => Yii::t('lang','Observations'),
			'machine_id' => Yii::t('lang','Machine'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('final_clients_id',$this->final_clients_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('ruc_technician',$this->ruc_technician,true);
		$criteria->compare('ruc_dealer',$this->ruc_dealer,true);
		$criteria->compare('observations',$this->observations,true);
		$criteria->compare('machine_id',$this->machine_id);

		/* Condicion para no Mostrar los Eliminados en Gridview */
//		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
