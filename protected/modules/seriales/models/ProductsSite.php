<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property string $name
 * @property string $model
 * @property string $sigla
 * @property string $url
 * @property integer $active
 * @property integer $brand_id
 *
 * The followings are the available model relations:
 * @property FeaturedProductsCountry[] $featuredProductsCountries
 * @property Brands $brand
 * @property ProductsFilesCountries[] $productsFilesCountries
 * @property ProductsTypesCountries[] $productsTypesCountries
 * @property Texts[] $texts
 */
class ProductsSite extends CActiveRecord
{
//   public function getDbConnection() {
//            return Yii::app()->dbtfhka;
//        }
    public function tableName()
    {
        return 'products';
    }

    public function rules()
    {
        return array(
                array('model, url, active, brand_id','required'),
                array('active, brand_id','numerical','integerOnly' => true),
                array('name','cod_saint','safe'),
                array('id, name, model, url, active, brand_id','safe','on' => 'search'),
        );
    }
    
    public function getDbConnection()
    {
        return Yii::app()->dbtfhka;
    }

    
    public function relations()
    {
        return array(
                'featuredProductsCountries' => array(
                        self::HAS_MANY,
                        'FeaturedProductsCountry',
                        'product_id'),
                'brand' => array(
                        self::BELONGS_TO,
                        'Brands',
                        'brand_id'),
                'productsFilesCountries' => array(
                        self::HAS_MANY,
                        'ProductsFilesCountries',
                        'product_id'),
                'productsTypesCountries' => array(
                        self::HAS_MANY,
                        'ProductsTypesCountries',
                        'product_id'),
                'texts' => array(
                        self::HAS_MANY,
                        'Texts',
                        'product_id')
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria();
        
        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('model', $this->model, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('active', $this->active);
        $criteria->compare('brand_id', $this->brand_id);
        
        /* Condicion para no Mostrar los Eliminados en Gridview */
        $criteria->addcondition("estatus <> 9");
        
        if(Yii::app()->params['GridViewOrder'] == FALSE)
        {
            $criteria->order = "id DESC";
        }
        
        return new CActiveDataProvider($this, array(
                'criteria' => $criteria));
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}

/*
class ProductsSite extends CActiveRecord
{
	public function tableName()
	{
		return 'products';
	}

	public function rules()
	{
		return array(
				array(
						'model, url, active, brand_id',
						'required'),
				array(
						'active, brand_id',
						'numerical',
						'integerOnly' => true),
				array(
						'name','cod_saint'
						'safe'),
				
				array(
						'id, name, model, url, active, brand_id',
						'safe',
						'on' => 'search')
        );
	}
	public function relations()
	{
		return array(
				'featuredProductsCountries' => array(
						self::HAS_MANY,
						'FeaturedProductsCountry',
						'product_id'),
				'brand' => array(
						self::BELONGS_TO,
						'Brands',
						'brand_id'),
				'productsFilesCountries' => array(
						self::HAS_MANY,
						'ProductsFilesCountries',
						'product_id'),
				'productsTypesCountries' => array(
						self::HAS_MANY,
						'ProductsTypesCountries',
						'product_id'),
				'texts' => array(
						self::HAS_MANY,
						'Texts',
						'product_id')
        );
	}
	public function attributeLabels()
	{
		return array(
				'id' => Yii::t('lang', 'ID'),
				'name' => Yii::t('lang', 'Name'),
				'model' => Yii::t('lang', 'Model'),
				'url' => Yii::t('lang', 'Url'),
				'active' => Yii::t('lang', 'Active'),
				'brand_id' => Yii::t('lang', 'Brand')
        );
	}
    
	public function getDbConnection()
	{
		return Yii::app()->dbtfhka;
	}
    
   
	public function search()
	{
		$criteria = new CDbCriteria();
		
		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('model', $this->model, true);
		$criteria->compare('url', $this->url, true);
		$criteria->compare('active', $this->active);
		$criteria->compare('brand_id', $this->brand_id);
		
		 Condicion para no Mostrar los Eliminados en Gridview 
		$criteria->addcondition("estatus <> 9");
		
		if(Yii::app()->params['GridViewOrder'] == FALSE)
		{
			$criteria->order = "id DESC";
		}
		
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria));
	}
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
*/