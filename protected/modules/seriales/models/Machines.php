<?php

/**
 * This is the model class for table "machines".
 *
 * The followings are the available columns in table 'machines':
 * @property integer $id
 * @property integer $lot_id
 * @property string $serial
 * @property integer $brand_id
 * @property integer $model_id
 * @property integer $dealer_id
 * @property string $observation
 * @property integer $status
 * @property string $created
 *
 * The followings are the available model relations:
 * @property FiscalizedMachines[] $fiscalizedMachines
 * @property Lots $lot
 * @property RbacUsuarios $dealer
 * @property Brands $brand
 * @property Models $model
 */
class Machines extends CActiveRecord {

    public $enajedados;


    public function behaviors() {
        return array(
            // Classname => path to Class
            'LogsableBehavior' => 'application.behaviors.LogsableBehavior');
    }

    public function tableName() {
        return 'machines';
    }

    public function rules() {

        return array(
//			array('serial,status,created', 'required'),
            array('serial', 'unique'),
            array('dealer_id, status', 'numerical', 'integerOnly' => true),
            array('serial', 'length', 'max' => 13),
            array('created', 'default', 'value' => new CDbExpression('NOW()'), 'setOnEmpty' => false, 'on' => 'insert'),
            array('observation', 'safe'),
            array('id, serial, enajedados,dealer_id, observation, status, created', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {

        return array(
            'fiscalizedMachines' => array(self::HAS_MANY, 'FiscalizedMachines', 'machine_id'),
            'dealer' => array(self::BELONGS_TO, 'RbacUsuarios', 'dealer_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('lang', 'ID'),
            'serial' => Yii::t('lang', 'Número de Serial'),
            'dealer_id' => Yii::t('lang', 'Dealer'),
            'observation' => Yii::t('lang', 'Observation'),
            'status' => Yii::t('lang', 'Status'),
            'enajedados'=>Yii::t('lang', 'Status'),
            'created' => Yii::t('lang', 'Created'),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.serial', $this->serial, true);
        $criteria->compare('t.dealer_id', $this->dealer_id);
        $criteria->compare('t.observation', $this->observation, true);
//		$criteria->compare('status',$this->status);
        $criteria->compare('t.created', $this->created, true);
        /* Condicion para no Mostrar los Eliminados en Gridview */
        $criteria->addcondition("t.status <> 9");

        $criteria->with = array('fiscalizedMachines');
        $criteria->together = true;

        if ($this->enajedados == '1') {
            $criteria->addcondition("fiscalizedMachines.date is not null ");
        }
        
         if ($this->enajedados == '0') {
            $criteria->addcondition("fiscalizedMachines.date is null ");
        }

//        if ($this->status == '1') {
//            $criteria->join = 'INNER JOIN fiscalized_machines fm ON(fm.machine_id = t.id)';
//        }
//
//        if ($this->status == '0') {
//            $criteria->join = 'INNER JOIN fiscalized_machines fm ON(fm.machine_id = t.id)';
//        }

        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
