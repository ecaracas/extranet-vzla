<?php

/**
 * This is the model class for table "brands".
 *
 * The followings are the available columns in table 'brands':
 * @property integer $id
 * @property string $name
 * @property string $image_path
 * @property string $website
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Products[] $products
 */
class Brands extends CActiveRecord
{
	  public function getDbConnection() {
            return Yii::app()->dbtfhka;
        }
	public function tableName()
	{
		return 'brands';
	}

	
	public function rules()
	{
		
		return array(
			array('name, active', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('image_path, website', 'safe'),
		
			array('id, name, image_path, website, active', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'products' => array(self::HAS_MANY, 'Products', 'brand_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'name' => Yii::t('lang','Name'),
			'image_path' => Yii::t('lang','Image Path'),
			'website' => Yii::t('lang','Website'),
			'active' => Yii::t('lang','Active'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('image_path',$this->image_path,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('active',$this->active);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
