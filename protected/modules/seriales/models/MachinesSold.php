<?php

/**
 * This is the model class for table "machines_sold".
 *
 * The followings are the available columns in table 'machines_sold':
 * @property integer $id
 * @property string $cod_saint
 * @property string $serial
 * @property integer $id_type_transaction
 * @property string $sales_check
 * @property string $date_sales_check
 * @property integer $last_operation
 * @property string $date_created
 * @property integer $created_by
 * @property string $date_edited
 * @property integer $edited_by
 * @property string $date_removed
 * @property integer $removed_by
 * @property integer $id_distributor
 */

class MachinesSold extends CActiveRecord {

    public function tableName() {
        return 'machines_sold';
    }

    public function rules() {

        return array(
            array('date_created, created_by, id_distributor, serial, sales_check, cod_saint, id_type_transaction, date_sales_check', 'required'),
            array('id_type_transaction, last_operation, created_by, edited_by, removed_by, id_distributor', 'numerical', 'integerOnly' => true),
            array('cod_saint', 'length', 'max' => 100),
            array('serial', 'length', 'max' => 13),
            array('sales_check', 'length', 'max' => 8),
            array('date_sales_check, date_edited, date_removed', 'safe'),
            array('id, cod_saint, serial, id_type_transaction, sales_check, date_sales_check, last_operation, date_created, created_by, date_edited, edited_by, date_removed, removed_by, id_distributor', 'safe', 'on' => 'search'),
            array('cod_saint', 'unique', 'criteria' => array(
                    'condition' => 'serial=:serial and sales_check=:sales_check',
                    'params' => array(':serial' => $this->serial, ':sales_check' => $this->sales_check),
                )),
            array('id_distributor', 'exist', 'attributeName' => 'id', 'className' => 'RBACUsuarios'),
        );
    }

    public function relations() {

        return array(
            'distribuidor' => array(self::BELONGS_TO, 'RBACUsuarios', 'id_distributor'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('lang', 'ID'),
            'cod_saint' => Yii::t('lang', 'Cod Saint'),
            'serial' => Yii::t('lang', 'Serial'),
            'id_type_transaction' => Yii::t('lang', 'Id Type Transaction'),
            'sales_check' => Yii::t('lang', 'Número de Factura'),
            'date_sales_check' => Yii::t('lang', 'Fecha'),
            'last_operation' => Yii::t('lang', 'Last Operation'),
            'date_created' => Yii::t('lang', 'Date Created'),
            'created_by' => Yii::t('lang', 'Created By'),
            'date_edited' => Yii::t('lang', 'Date Edited'),
            'edited_by' => Yii::t('lang', 'Edited By'),
            'date_removed' => Yii::t('lang', 'Date Removed'),
            'removed_by' => Yii::t('lang', 'Removed By'),
            'id_distributor' => Yii::t('lang', 'Cliente'),
        );
    }

    public function search() {

        $criteria = new CDbCriteria;
        $criteria->with = array('distribuidor');

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.cod_saint', $this->cod_saint, true);
        $criteria->compare('t.serial', $this->serial, true);
        $criteria->compare('t.id_type_transaction', $this->id_type_transaction);
        $criteria->compare('t.sales_check', $this->sales_check, true);
        $criteria->compare('t.date_sales_check', $this->date_sales_check, true);
        $criteria->compare('t.last_operation', $this->last_operation);
        $criteria->compare('t.date_created', $this->date_created, true);
        $criteria->compare('t.created_by', $this->created_by);
        $criteria->compare('t.date_edited', $this->date_edited, true);
        $criteria->compare('t.edited_by', $this->edited_by);
        $criteria->compare('t.date_removed', $this->date_removed, true);
        $criteria->compare('t.removed_by', $this->removed_by);
        //$criteria->compare('id_distributor',$this->id_distributor);
        // compare title
        $criteria->compare('distribuidor.razon_social', $this->id_distributor, true);

        /* Condicion para no Mostrar los Eliminados en Gridview */
        //$criteria->addcondition("estatus <> 9");

        if (Yii::app()->params['GridViewOrder'] == FALSE) {
            $criteria->order = "t.id DESC";
        }

        /* $cliente=MachinesSold::find()
          ->select('u.razon_social')
          ->from('machines_sold ms')
          ->innerJoin('RBACUsuarios u','ms.id_distributor = u.id')
          ->where('u.rol_id = :rol_id', [':rol_id'=>3])
          ; */
        $data = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));

        $_SESSION['promotion.excel'] = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => false,
        ));

        return $data;

    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
