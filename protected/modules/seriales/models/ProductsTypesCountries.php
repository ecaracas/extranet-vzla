<?php

/**
 * This is the model class for table "products_types_countries".
 *
 * The followings are the available columns in table 'products_types_countries':
 * @property string $country_id
 * @property integer $product_id
 * @property integer $product_type_id
 * @property integer $order
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Countries $country
 * @property Products $product
 * @property Types $productType
 */
class ProductsTypesCountries extends CActiveRecord
{
	 public function getDbConnection() {
            return Yii::app()->dbtfhka;
        }
	public function tableName()
	{
		return 'products_types_countries';
	}

	
	public function rules()
	{
		
		return array(
			array('country_id, product_id, product_type_id, order, active', 'required'),
			array('product_id, product_type_id, order, active', 'numerical', 'integerOnly'=>true),
			array('country_id', 'length', 'max'=>3),
		
			array('country_id, product_id, product_type_id, order, active', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'country' => array(self::BELONGS_TO, 'Countries', 'country_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'productType' => array(self::BELONGS_TO, 'Types', 'product_type_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'country_id' => Yii::t('lang','Country'),
			'product_id' => Yii::t('lang','Product'),
			'product_type_id' => Yii::t('lang','Product Type'),
			'order' => Yii::t('lang','Order'),
			'active' => Yii::t('lang','Active'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('country_id',$this->country_id,true);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('product_type_id',$this->product_type_id);
		$criteria->compare('order',$this->order);
		$criteria->compare('active',$this->active);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
