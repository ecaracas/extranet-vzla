<?php

/**
 * This is the model class for table "types".
 *
 * The followings are the available columns in table 'types':
 * @property integer $id
 * @property string $name
 * @property integer $active
 * @property string $language_id
 * @property integer $parent_id
 *
 * The followings are the available model relations:
 * @property ProductTypeTexts[] $productTypeTexts
 * @property ProductsTypesCountries[] $productsTypesCountries
 * @property Languages $language
 * @property Types $parent
 * @property Types[] $types
 * @property TypesCountries[] $typesCountries
 */
class Types extends CActiveRecord
{
	 public function getDbConnection() {
            return Yii::app()->dbtfhka;
        }
	public function tableName()
	{
		return 'types';
	}

	
	public function rules()
	{
		
		return array(
			array('name, active, language_id', 'required'),
			array('active, parent_id', 'numerical', 'integerOnly'=>true),
			array('language_id', 'length', 'max'=>2),
		
			array('id, name, active, language_id, parent_id', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'productTypeTexts' => array(self::HAS_MANY, 'ProductTypeTexts', 'product_type_id'),
			'productsTypesCountries' => array(self::HAS_MANY, 'ProductsTypesCountries', 'product_type_id'),
			'language' => array(self::BELONGS_TO, 'Languages', 'language_id'),
			'parent' => array(self::BELONGS_TO, 'Types', 'parent_id'),
			'types' => array(self::HAS_MANY, 'Types', 'parent_id'),
			'typesCountries' => array(self::HAS_MANY, 'TypesCountries', 'product_type_id'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'name' => Yii::t('lang','Name'),
			'active' => Yii::t('lang','Active'),
			'language_id' => Yii::t('lang','Language'),
			'parent_id' => Yii::t('lang','Parent'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('language_id',$this->language_id,true);
		$criteria->compare('parent_id',$this->parent_id);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
