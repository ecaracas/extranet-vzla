<?php

/**
 * This is the model class for table "type_transaction".
 *
 * The followings are the available columns in table 'type_transaction':
 * @property integer $id
 * @property string $description
 * @property string $date_created
 * @property integer $created_by
 * @property string $date_edited
 * @property integer $edited_by
 * @property string $date_removed
 * @property string $removed_by
 *
 * The followings are the available model relations:
 * @property MachinesSold[] $machinesSolds
 */
class TypeTransaction extends CActiveRecord
{
	
	public function tableName()
	{
		return 'type_transaction';
	}

	
	public function rules()
	{
		
		return array(
			array('description, date_created, created_by', 'required'),
			array('created_by, edited_by', 'numerical', 'integerOnly'=>true),
			array('description, removed_by', 'length', 'max'=>100),
			array('date_edited, date_removed', 'safe'),
		
			array('id, description, date_created, created_by, date_edited, edited_by, date_removed, removed_by', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'machinesSolds' => array(self::HAS_MANY, 'MachinesSold', 'id_type_transaction'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'description' => Yii::t('lang','Description'),
			'date_created' => Yii::t('lang','Date Created'),
			'created_by' => Yii::t('lang','Created By'),
			'date_edited' => Yii::t('lang','Date Edited'),
			'edited_by' => Yii::t('lang','Edited By'),
			'date_removed' => Yii::t('lang','Date Removed'),
			'removed_by' => Yii::t('lang','Removed By'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('date_edited',$this->date_edited,true);
		$criteria->compare('edited_by',$this->edited_by);
		$criteria->compare('date_removed',$this->date_removed,true);
		$criteria->compare('removed_by',$this->removed_by,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
