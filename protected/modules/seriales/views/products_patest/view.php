<?php
/* @var $this ProductsController */
/* @var $model Products */

$this->breadcrumbs=array(
    'Products'=>array('index'),
    $model->name,
);

$this->menu=array(
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['update-text']), 
        'url'=>array('update', 
        'id'=>$model->id), 
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['index-text']), 
        'url'=>array('index'), 
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['update-text']) .' '. Yii::t('lang','Products'); ?></h3>

                <div class="menu-tool">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
                  <?php $this->ToolActionsRight(); ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('name')); ?></th>
	<td><?php echo CHtml::encode($model->name); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('active')); ?></th>
	<td><?php echo CHtml::encode($model->active); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('model')); ?></th>
	<td><?php echo CHtml::encode($model->model); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('score')); ?></th>
	<td><?php echo CHtml::encode($model->score); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('trademark')); ?></th>
	<td><?php echo CHtml::encode($model->trademark); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('featured_product')); ?></th>
	<td><?php echo CHtml::encode($model->featured_product); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('sigla')); ?></th>
	<td><?php echo CHtml::encode($model->sigla); ?></td>
	</tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>