<?php
/* @var $this ProductsController */
/* @var $data Products */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->name); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('active')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->active); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('model')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->model); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('score')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->score); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('trademark')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->trademark); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('featured_product')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->featured_product); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('sigla')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->sigla); ?></div>
	
</div>

<hr />