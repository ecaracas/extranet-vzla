<?php
/* @var $this MachinesSoldController */
/* @var $data MachinesSold */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('cod_saint')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->cod_saint); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('serial')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->serial); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id_type_transaction')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->id_type_transaction); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('sales_check')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->sales_check); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date_sales_check')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date_sales_check); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('last_operation')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->last_operation); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date_created); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->created_by); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date_edited')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date_edited); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('edited_by')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->edited_by); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date_removed')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date_removed); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('removed_by')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->removed_by); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id_distributor')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->id_distributor); ?></div>
	
</div>

<hr />