<?php
/* @var $this MachinesSoldController */
/* @var $model MachinesSold */

$this->breadcrumbs = array(
    'Machines Solds' => array('index'),
    Yii::app('lang', 'Manage'),
);
Yii::app()->params['IDGridview'] = 'machines-sold-grid';
$this->menu = array(
    /*array(
        'label' => Yii::t('lang', Yii::app()->params['create-text']),
        'url' => array('create'),
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
*/);
?>
<div class="row">
    <div class="col-xs-12">

        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo Yii::t('lang', Yii::app()->params['index-text']) . ' ' . Yii::t('lang', 'machines_sold'); ?></h3>

                <div class="menu-tool">

                    <div class="pagesize"> 
                        <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
                    </div>         

                    <div class="menu-items">
                        <?php
                        $this->widget('zii.widgets.CMenu', array(
                            'items' => $this->menu,
                            'encodeLabel' => FALSE,
                            'htmlOptions' => array('class' => 'cmenuhorizontal'),
                        ));
                        ?>
                    </div>
                </div>        
                <?php $this->ToolActionsRight(); ?>         

            </div>
            <div class="panel-body"> 
                    <?php if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {?>
                        <div style="margin: 19px 0px 0px 0px;">
                            <?= CHtml::submitButton(Yii::t('lang','Carga Masiva'),array('class'=>'btn btn-primary','data-toggle'=>"modal", 'data-target'=>"#myModal", 'style' => 'float:left; height: 40px')); ?>
                            <?= CHtml::link('Descargar Excel', array('machinesSold/exportexcel/id/'.$model->id), array('class' => 'btn btn-primary', 'style' => 'text-transform: none; height: 40px; margin-left: 2px')); ?>
                        </div>
                    <?php } ?>
                    <div class="table-responsive">
                    <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>Yii::app()->params['IDGridview'],
                        'dataProvider'=>$dataProvider,
                        'filter'=>$model,
                              'afterAjaxUpdate'=>'js:function(id, data){
                                GridviewBoxSwitch();
                                reinstallDatePicker();
                        }',
                        'columns'=>array(
                             array('name'=>'serial','value'=>'$data->serial'),
                             array('name'=>'sales_check','value'=>'$data->sales_check'),
                    		 array('name'=>'id_distributor','value'=>'$data->distribuidor->razon_social'),
                             //array('name'=>'date_sales_check','value'=>'$data->date_sales_check'),
                             array('name'=>'date_sales_check',
                                 'value'=>'Yii::app()->dateFormatter->format("y-MM-dd",strtotime($data->date_sales_check))', 
                                 'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker',
                                         array('model' => $model, 'attribute' => 'date_sales_check', 'language' => 'es',
                                                'htmlOptions' => array(
                                                    'id' => 'datepicker_for_due_date',
                                                    'size' => '10',
                                                    'class'=>'form-control',
                                                ),
                                                'options' => array('dateFormat' => 'yy-mm-dd'), 'defaultOptions' => array(
                                                    'showOn' => 'focus',
                                                    'dateFormat' => 'yy-mm-dd',
                                                    'showOtherMonths' => true,
                                                    'selectOtherMonths' => true,
                                                    'maxDate' => 0,
                                                    'changeMonth' => true,
                                                    'changeYear' => true,
                                                    'language' => 'es'
                                                   )), true)), 
                             
                            /*
                             array('name'=>'id_type_transaction','value'=>'$data->id_type_transaction'),
                             array('name'=>'id','value'=>'$data->id'),
                             array('name'=>'cod_saint','value'=>'$data->cod_saint'),
                             array('name'=>'last_operation','value'=>'$data->last_operation'),
                             array('name'=>'date_created','value'=>'$data->date_created'),
                             array('name'=>'created_by','value'=>'$data->created_by'),
                             array('name'=>'date_edited','value'=>'$data->date_edited'),
                             array('name'=>'edited_by','value'=>'$data->edited_by'),
                             array('name'=>'date_removed','value'=>'$data->date_removed'),
                             array('name'=>'removed_by','value'=>'$data->removed_by'),
                    		*/
                              array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang',Yii::app()->params['view-text']),            
                                'label' => Yii::app()->params['view-icon'],
                                'linkHtmlOptions' => array('class' => 'view '.Yii::app()->params['view-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))', 
                                 'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
                            ),
                            array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang',Yii::app()->params['update-text']),            
                                'label' => Yii::app()->params['update-icon'],
                                'linkHtmlOptions' => array('class' => 'edit '.Yii::app()->params['update-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                                 'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
                            ),
                              array(
                                'class' => 'CLinkColumn',
                                'header' => Yii::t('lang',Yii::app()->params['delete-text']),           
                                'label' => Yii::app()->params['delete-icon'],
                                'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
                                'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                                'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
                            ),
                        ),
                    )); ?>
                    </div>
            </div>
                

    </div>
</div>
<?php
if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete', array());    
}
?>

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Carga masiva</h4>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="formuploadajax" method="post">
                    <input  type="file" id="facturas" name="facturas"/>
                    <br/>
                    <div id="mensaje"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <input class="btn btn-primary" type="submit" value="Subir"/>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#main-content').css({'min-height': '800px'});
        $("#formuploadajax").on("submit", function (e) {
            e.preventDefault();
            var f = $(this);
            var inputFileImage = document.getElementById("facturas");
            var formData = new FormData();
            var file = inputFileImage.files[0];
            $('#mensaje').html('<img id="imgCarga" src="<?php echo Yii::app()->request->baseUrl ?>/img/cargando.gif">');
            formData.append('archivo', file);
            $.ajax({
                url: "create",
                type: "post",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (res) {
                
                $('#mensaje').empty();
                if (res.succes == 'procesado')
                {
                    $('#mensaje').html('Su archivo se cargo correctamente <a href="../../' + res.path + '"><strong>click aqui</strong></a> y descarga archivo procesado!!!');

                    $.fn.yiiGridView.update("machines-sold-grid")
                }
                if (res.succes == 'FALLIDO')
                {
                    $('#mensaje').html('La carga del archivo fallo haga <a href="../../' + res.path + '"><strong>click aqui</strong></a> y descargue para verificar los errores!!!');
                }
                if (res.succes == 'mensaje')
                {
                    $('#mensaje').html(res.text);
                }
            }).fail(function() {
                $('#mensaje').html('Algo salio mal...');
            });
        });
    });
</script>