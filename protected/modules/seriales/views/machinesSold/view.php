<?php
/* @var $this MachinesSoldController */
/* @var $model MachinesSold */

$this->breadcrumbs=array(
    'Machines Solds'=>array('index'),
    $model->id,
);

$this->menu=array(
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['update-text']), 
        'url'=>array('update', 
        'id'=>$model->id), 
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['index-text']), 
        'url'=>array('index'), 
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['update-text']) .' '. Yii::t('lang','MachinesSold'); ?></h3>

                <div class="menu-tool">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
                  <?php $this->ToolActionsRight(); ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('cod_saint')); ?></th>
	<td><?php echo CHtml::encode($model->cod_saint); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('serial')); ?></th>
	<td><?php echo CHtml::encode($model->serial); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('id_type_transaction')); ?></th>
	<td><?php echo CHtml::encode($model->id_type_transaction); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('sales_check')); ?></th>
	<td><?php echo CHtml::encode($model->sales_check); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('date_sales_check')); ?></th>
	<td><?php echo CHtml::encode($model->date_sales_check); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('last_operation')); ?></th>
	<td><?php echo CHtml::encode($model->last_operation); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('date_created')); ?></th>
	<td><?php echo CHtml::encode($model->date_created); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('created_by')); ?></th>
	<td><?php echo CHtml::encode($model->created_by); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('date_edited')); ?></th>
	<td><?php echo CHtml::encode($model->date_edited); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('edited_by')); ?></th>
	<td><?php echo CHtml::encode($model->edited_by); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('date_removed')); ?></th>
	<td><?php echo CHtml::encode($model->date_removed); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('removed_by')); ?></th>
	<td><?php echo CHtml::encode($model->removed_by); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('id_distributor')); ?></th>
	<td><?php echo CHtml::encode($model->id_distributor); ?></td>
	</tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>