<?php
/* @var $this MachinesController */
/* @var $model Machines */

$this->breadcrumbs=array(
    'Machines'=>array('index'),
    $model->id,
);

$this->menu=array(
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['update-text']), 
        'url'=>array('update', 
        'id'=>$model->id), 
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['index-text']), 
        'url'=>array('index'), 
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['update-text']) .' '. Yii::t('lang','Machines'); ?></h3>

                <div class="menu-tool">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
                  <?php $this->ToolActionsRight(); ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('lot_id')); ?></th>
	<td><?php echo CHtml::encode($model->lot_id); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('serial')); ?></th>
	<td><?php echo CHtml::encode($model->serial); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('brand_id')); ?></th>
	<td><?php echo CHtml::encode($model->brand_id); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('model_id')); ?></th>
	<td><?php echo CHtml::encode($model->model_id); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('dealer_id')); ?></th>
	<td><?php echo CHtml::encode($model->dealer_id); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('observation')); ?></th>
	<td><?php echo CHtml::encode($model->observation); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('status')); ?></th>
	<td><?php echo CHtml::encode($model->status); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('created')); ?></th>
	<td><?php echo CHtml::encode($model->created); ?></td>
	</tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>