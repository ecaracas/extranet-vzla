<?php
/* @var $this MachinesController */
/* @var $data Machines */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('lot_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->lot_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('serial')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->serial); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('brand_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->brand_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('model_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->model_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('dealer_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->dealer_id); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('observation')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->observation); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('status')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->status); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('created')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->created); ?></div>
	
</div>

<hr />