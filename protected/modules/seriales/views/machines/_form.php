<style>
.error{
	color:red;
}
</style>
<?php $form = $this->beginWidget('CActiveForm', array('id' => 'machines-form', 'enableAjaxValidation' => false, 'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->errorSummary($modelProducts); ?>


<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <label>Modelo</label>
    </div>

    <div class="col-xs-12 col-sm-9">
        <?php echo $form->dropDownList($modelProducts,'name',CHtml::listData(Products::model()->findAll('active = 1',array('order' => 'id ASC')), 'id', 'model'), array('required'=>'required','empty'=>'Seleccione')); ?>       
        <p class="help-block">Seleccione el modelo del equipo</p>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <label>Serial</label>
    </div>

    <div class="col-xs-12 col-sm-9">
        <input id="Machines_inicial" class="form-control input-integer" type="text" name="Machines[inicial]" minlength="1"  maxlength="7" size="7" autocomplete="off">
        <p class="help-block">Si deja este campo vacio se generara los seriales tomando el ultimo valor</p>
    </div>

</div>
<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <label>Cantidad</label>
    </div>

    <div class="col-xs-12 col-sm-9">
        <input id="Machines_cantidad" class="form-control input-integer" required="" type="text" name="Machines[cantidad]" minlength="1" maxlength="7" size="7" autocomplete="off">
      <p class="help-block">Inique la cantidad de seriales. solo ingresar valores numericos</p>
    </div>

</div>

<div class="form-group">
    <div class="col-xs-12 col-sm-3">
        <?php echo $form->labelEx($model, 'observation'); ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <?php echo $form->textArea($model, 'observation', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'observation'); ?>
         <p class="help-block">indique alguna observacion que tenga en cuenta</p>
    </div>
</div>

<br />
<div class="row">
    <div class="col-xs-12 col-sm-6">
       <p class="text-left"><?php echo Yii::t('lang', Yii::app()->params['camposrequeridos']); ?>
        </p>
    </div>
    <div class="col-xs-12 col-sm-6">                
        <ul class="cmenuhorizontal" id="yw0">
            <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang','Generar seriales'), array('confirm'=>'Desea generar estos seriales ?','class' =>Yii::app()->params['save-btn'])); ?></li>
            <li class="bottom"><?php echo CHtml::link(Yii::t('lang', Yii::app()->params['cancel-text']), Yii::app()->controller->createUrl('index'), array('class' => Yii::app()->params['cancel-btn'])); ?>
            </li>
        </ul>
    </div>
</div>
<?php $this->endWidget(); ?>
<script>

    
    $( document ).ready(function() {
    $('#machines-form').submit(function(e) {
   
    }).validate({     
        rules: {
            "Products[name]": {
                required: true,               
            },
            "Machines[inicial]": {
                required: false,
                minlength: 1,
                maxlength: 7
            },
            "Machines[cantidad]": {
                required: true,
                minlength: 1,
                maxlength: 7
            },
        },
        messages: {
            "Machines[inicial]": {
                required: "Serial es obligatorio.",
                minlength: "Debe contener  1 dígitos numérico y maximo 7 dígitos",
                maxlength: "Debe contener 1 dígitos numérico y maximo 7 dígitos",
            },
            "Machines[cantidad]": {
                required: "El valor de la cantidad es numérico.",
                minlength: "Debe contener  1 dígitos numérico y maximo 6 dígitos",
                maxlength: "Debe contener 7 dígitos numérico y maximo 6 dígitos",
            },            
            "Products[name]": {
                required: "Debe de seleccionar un modelo de impresora.",               
            },            
        }
    });   
});

</script>