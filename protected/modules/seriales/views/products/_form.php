<?php
/* @var $this ProductsController */
/* @var $model Products */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'products-form',
  'enableAjaxValidation'=>false,
     'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form',
        // 'enctype'=>'multipart/form-data',
    ),
)); ?>

    <p class="note">Los campos con <span class="required">*</span> son requeridos.</p>



        <?php //echo $form->errorSummary($model); ?>

            <div class="form-group">
                
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'name'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textArea($model,'name',array('rows'=>1, 'cols'=>3)); ?>
    
                     <?php echo $form->error($model,'name'); ?>
                 </div>
                      
            </div>
            
<!-- seleccionar la marca  -->

            <div class="row" style="width: 50%">
        <?php echo $form->labelEx($model, Yii::t('utilities', 'brands_models')); ?>
        <?php
        // var_dump($model);
        echo $form->dropDownlist($model, 'brands_models', CHtml::listData(ProductsTypes::model()->findAll(), 'id', 'name'), array(
                'class' => 'form-control',
                'ajax' => array(
                        'type' => 'POST',
                        'url' => CController::createUrl('Products/Selectproduct'),
                        'update' => '#' . CHtml::activeId($model, 'productos'),
                        
                        'beforeSend' => 'function(){
                            $("#Registro_id_level_city").find("option").remove();
                            }'),
                'prompt' => 'SELECCIONAR UNA MARCA ')
        )?>
        <?php echo $form->error($model,'id_level_country'); ?>
    </div>
<!-- --------------------------------------- -->

<!-- seleccionar el modelo -->
            
       <div class="row" style="width: 50%">
    <?php
        $listasCity = array();
    ?>
    <?php echo $form->labelEx($model, Yii::t('utilities', 'productos')); ?>
    <?php echo $form->dropDownlist($model,'productos',$listasCity,array("class" => 'form-control','prompt'=>'SELECCIONAR UN PRODUCTO') ); ?>
    <?php echo $form->error($model,'productos'); ?>
    </div>

<!-- ---------------------------------- -->
            
            <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'score'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'score'); ?>
    
                     <?php echo $form->error($model,'score'); ?>
                 </div>
                      
            </div>
<!-- ---------------- para poder cargar la imagen del logo -------- -->
            <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'trademark'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'trademark',array('size'=>60,'maxlength'=>500)); ?>
    
                     <?php echo $form->error($model,'trademark'); ?>
                 </div>
                      
            </div>
<!-- ------------------------------------------------------- -->


                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'featured_product'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'featured_product',array('size'=>60,'maxlength'=>250)); ?>
    
                     <?php echo $form->error($model,'featured_product'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'sigla'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'sigla',array('size'=>6,'maxlength'=>6)); ?>
    
                     <?php echo $form->error($model,'sigla'); ?>
                 </div>    
            </div>
            <div class="form-group">

        <div class="col-xs-12 col-sm-3">
            <?php echo $form->labelEx($model, 'active'); ?>
        </div>
        <div class="col-xs-12 col-sm-9">
            <?php
            $this->widget('application.extensions.SwitchToggleJD.SwitchToggleJD', array(
                'id' => 'Products_active',
                'attribute' => 'Products[active]',
                'state' => $model->active,
                'type' => 'item',
                'coloron' => 'color1'));
            ?>
            <?php echo $form->error($model, 'active'); ?>
        </div>
    </div>

                    <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                        <p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>
</p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?>
</li>
                    <li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?>
</li>
                </ul>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>
