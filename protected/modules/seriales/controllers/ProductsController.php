<?php

class ProductsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', /* Acciones Permitidas*/
				'actions'=>array('index','view','create','update','delete','selectproduct'),
				'users'=>array('*'),
			),			
			array('deny',
				'users'=>array('*'),
			),
		);
	}
        
         public $modulo = "seriales";

         
        public function __construct($id, $module = null) {
            
        parent::__construct($id, $module);
        
         Yii::app()->params['title'] = '';
        
        }



    public function actionSelectproduct() {
        $id_marca = $_POST['Products']['brands_models'];
        $lista = ProductsBrand::model()->findAll('product_type = :id_marca', array(
            ':id_marca' => $id_marca));
        // $lista_name = Products::model()->findAll('id = :id_marca' , array(':id_marca'=>$id_marca));
        // var_dump($lista); exit();
        foreach ($lista as $key => $valor) {
            $id = $valor->product;
            $Productos[$key] = Products::model()->find('id = ?', array(
                $id));
        }
        // var_dump($Productos);
        // $lista = CHtml::listData($lista,'product', 'product_type');
        // $lista = CHtml::listData($lista,'product_type', Products::model()->findAll('id = :id_marca' , array(':id_marca'=>$id_marca)));

        echo CHtml::tag('option', array(
            'value' => ''), 'SELECCIONAR UN PRODUCTO ', true);

        foreach ($Productos as $valor) {

            echo CHtml::tag('option', array(
                'value' => $valor->name), CHtml::encode($valor->name), true);
        }
    }
         
	public function actionView($id)
	{
         // if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {
         
                $model = $this->loadModel($id);

		$this->render('view',array(
			'model'=>$model,
		));
        
        // } else {
        //     throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        // }
	}

	public function actionCreate()
	{
        
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {

		$model=new Products;
// print_r($_POST['Products']);die;
		if(isset($_POST['Products']))
		{
			$model->attributes=$_POST['Products'];
			$model->model = $_POST['Products']['productos'];
			if($model->validate()){
                        if($model->save()){
                        
					Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
					$this->redirect(array('view','id'=>$model->id));
            } else {
                  Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);  
            }
            	} else {
                  Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);  
            	}
            
		}

		$this->render('create',array(
			'model'=>$model,
		));
                
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
	}


	public function actionUpdate($id)
	{
        
         if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {
        
            $model = $this->loadModel($id);

            
		if(isset($_POST['Products']))
		{
			$model->attributes=$_POST['Products'];
                        
		   	if($model->validate()){
			 if($model->save()){
					Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
					$this->redirect(array('view','id'=>$model->id));
			} else {
                  Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);  
            }
            	} else {
                  Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);  
            	}
             
        }

		$this->render('update',array(
			'model'=>$model,
		));
                            
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
	}

	public function actionIndex()
	{
        
        // if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {

		//$model=new Products('search');


        //$criteria->join =' ';

		$model= new Products('searchWithBrands');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Products'])){
			$model->attributes=$_GET['Products'];
        }

        if(isset($_GET['Products_sort'])){

        Yii::app()->params['GridViewOrder'] = TRUE;
    	}

		$this->render('index',array(
			'model'=>$model,
		));
        
        // } else {  
        // 	throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        // }

	}

		public function loadModel($id)
	{		
                $sql = new CDbCriteria;
                $sql->params = array(':id' => intval($id));
                $sql->condition = "id = :id";
                // $sql->addcondition("estatus <> 9");
                $model = Products::model()->find($sql);
		if($model===null){
			throw new CHttpException(404,Yii::app()->params['Error404']);
                }
		return $model;
	}

}
