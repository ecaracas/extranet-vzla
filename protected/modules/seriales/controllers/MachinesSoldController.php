<?php

class MachinesSoldController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    public function behaviors()
    {
        return array(
                'eexcelview' => array(
                        'class' => 'application.extensions.eexcelview.EExcelBehavior'));
    }
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', /* Acciones Permitidas*/
                'actions'=>array('index','view','create','update','delete','exportexcel'),
                'users'=>array('*'),
            ),          
            array('deny',
                'users'=>array('*'),
            ),
        );
    }
        
         public $modulo = "seriales";

         
        public function __construct($id, $module = null) {
            
        parent::__construct($id, $module);
        Yii::app()->getModule('fiscalizacion');
        Yii::app()->params['title'] = '';
        
        }
         
    public function actionView($id)
    {
         if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {
         
                $model = $this->loadModel($id);

        $this->render('view',array(
            'model'=>$model,
        ));
        
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionCreate()
    {
        if(Yii::app()->request->isAjaxRequest)
        {

            if (!$_FILES) {
                echo json_encode(array('succes' => 'mensaje', 'text' => 'Seleccione un archivo...'));
                return true;
            }
            $header = '';
            $dataArray = array();
            $estatus = 'procesado';
            $rutaResponse = '';
            $nombre_archivo = $_FILES['archivo']['name'];
            $tipo_archivo = $_FILES['archivo']['type'];
            $tamano_archivo = $_FILES['archivo']['size'];
            $filename = $_FILES['archivo']['tmp_name'];
            $handle = fopen($filename, "r");
            $i = 0;
            
            $estatus = 'procesado';

            $rutaResponse = 'cargaMasivaFacturas_' . date("d_m_Y_Hms") . '.csv';
            
            if ($tipo_archivo!='text/csv' && $tipo_archivo!= 'application/vnd.ms-excel' ) {
                echo json_encode(array('succes' => 'mensaje', 'text' => 'Formato no valido (Permitido .csv)'));
                return true;
            }

            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)    
            {   
                if ($i == 0) {
                    $i++;
                    continue;
                }
                if (count($data) != 8) {
                    $this->insertArchivo($data,'Error formato',$rutaResponse);
                    $estatus = 'FALLIDO';
                    continue;
                }
                
                $date = $this->check_date($data[5]);

                if (!$date) {
                    $this->insertArchivo($data,'Fecha no valida'.$date,$rutaResponse);
                    $estatus = 'FALLIDO';
                    continue;
                }
                $model = new MachinesSold();
                $model->date_sales_check = $date;
                $transaction = Yii::app()->db->beginTransaction();
                $type = TypeTransaction::model()->find('description=:description', array(':description'=>$data[3]));
                $model->id_type_transaction = $type?$type->id:null;

                $product = ProductsSite::model()->find('cod_saint=:cod_saint', array('cod_saint' => substr($data[2],0,3)));
                
                $model->serial = $product?$product->acronym.substr($data[2], -7):null;
                $model->cod_saint = substr($data[2],0,-7);
                $model->date_created = date('Y-m-d H:m:s');
                $model->created_by = Yii::app()->user->getState('id');
                $distri = explode(" ", $data[7]);
                if (strlen($distri[0])<=3){
                    $dni = false; 
                    $distributor = RBACUsuarios::model()->find('cod_saint=:cod_saint', array('cod_saint' => $distri[0]));        
                } else {
                    $dni = true; 
                    $distributor = RBACUsuarios::model()->find('dni_numero=:dni_numero', array('dni_numero' => $distri[0]));        
                }

                if (!$distributor) {
                    $usuario = new RBACUsuarios();
                    $usuario->rol_id = 3;
                    $usuario->cod_saint = !$dni?$distri[0]:null;
                    $usuario->nombreusuario = $distri[0];
                    $usuario->email = 'S@A.com';
                    $nombre = str_replace($distri[0], "", $data[7]);
                    $usuario->nombre = $nombre;
                    $usuario->apellido = 'S/A';
                    $usuario->dni_numero = $dni?$distri[0]:'S/A';
                    $usuario->registro_fecha = date('Y-m-d H:m:s');
                    $usuario->registro_usuario_id = Yii::app()->user->getState('id');
                    $usuario->modificado_fecha = date('Y-m-d H:m:s');
                    $usuario->modificado_usuario_id = Yii::app()->user->getState('id');
                    $usuario->secuencia = 1;
                    $usuario->estatus = 3;
                    $usuario->razon_social = $nombre;
                    $usuario->contrasena = ValidateSafePassword::Hash('123456');
                    $usuario->save();
                    if ($usuario->errors) {
                        print_r($usuario->errors);
                    }
                    $distributor = $usuario;
                }
                $model->id_distributor = $distributor?$distributor->id:$data[7];
                $model->sales_check = $data[4];
                $model->last_operation = 1;

                if ($model->id_type_transaction == 1) {
                    $model->last_operation = 0;
                }
                
                if($model->validate())
                {
                    $model->save();
                    $transaction->commit();
                    $this->insertArchivo($data,'PROCESADA',$rutaResponse);
                } else {
                    $transaction->rollBack();
                    $estatus = 'FALLIDO';
                    $this->insertArchivo($data,implode(';',array_column($model->getErrors(), 0)),$rutaResponse);
                }
            }

            $facturas = MachinesSold::model()->findAll('id_type_transaction=:id_type_transaction', array(':id_type_transaction'=>1));

            foreach ($facturas as $key => $factura) {
                $model = MachinesSold::model()->find('cod_saint=:cod_saint and serial=:serial and id_type_transaction=:id_type_transaction order by sales_check asc', array(':cod_saint'=>$factura->cod_saint,':serial'=>$factura->serial,':id_type_transaction'=>2));

                if (!$model) {
                    continue;
                }

                $model->last_operation = 0;
                $model->save();
            }
            echo json_encode(array('succes' => $estatus, 'path' => 'downloads'. DIRECTORY_SEPARATOR .'cargaMasivaFacturas' . DIRECTORY_SEPARATOR . $rutaResponse));
        }
    }

    public function insertArchivo($data,$mensaje,$rutaResponse)
    {
        $path = dirname(__FILE__) . '/../../../../downloads/cargaMasivaFacturas/';
        
        $archivo = fopen($path . $rutaResponse, "a");
        fwrite($archivo, 
            @$data[0] . ';' . 
            @$data[1] . ';'. 
            @$data[2] . ';'. 
            @$data[3] . ';'. 
            @$data[4] . ';'. 
            @$data[5] . ';'. 
            @$data[6] . ';'. 
            @$data[7] . ';'. 
            @$mensaje . 
        "\n");
        fclose($archivo);
    }

    public function check_date($str){ 
        trim($str);

        list($year,$month,$day) = explode("/",$str);

        if(!checkdate($month,$day,$year)){
            list($day,$month,$year) = explode("/",$str);
            if(!checkdate($month,$day,$year)){
                return false;
            }
        }        
        return $year.'-'.$month.'-'.$day;
    }

    public function actionUpdate($id)
    {
        
         if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {
        
            $model = $this->loadModel($id);

            
        if(isset($_POST['MachinesSold']))
        {
            $model->attributes=$_POST['MachinesSold'];
                        
            if($model->validate()){
             if($model->save()){
                    Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                    $this->redirect(array('view','id'=>$model->id));
            } else {
                  Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);  
            }
                } else {
                  Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);  
                }
             
        }

        $this->render('update',array(
            'model'=>$model,
        ));
                            
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionIndex()
    {
        
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {

        $model=new MachinesSold('search');
        
        $model->unsetAttributes();  // clear any default values
        
        if(isset($_GET['MachinesSold'])){
            $model->attributes=$_GET['MachinesSold'];
        }

        if(isset($_GET['MachinesSold_sort'])){

        Yii::app()->params['GridViewOrder'] = TRUE;
        }

        $this->render('index',array(
            'model'=>$model,
        ));
        
        } else {  
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }

    }

    public function actionExportexcel($id = null)
    {
        if(isset($_SESSION['promotion.excel']))
        {
            
            $data = $_SESSION['promotion.excel'];
            $hoy = date("Y-m-d H:i:s");
            $this->toExcel($data, array(
                    'serial',
                    'sales_check',
                    'distribuidor.razon_social',
                    'date_sales_check'
                   ), $hoy);
        }
    }

    public function loadModel($id)
    {       
                $sql = new CDbCriteria;
                $sql->params = array(':id' => intval($id));
                $sql->condition = "id = :id";
                //$sql->addcondition("estatus <> 9");
                $model = MachinesSold::model()->find($sql);
        if($model===null){
            throw new CHttpException(404,Yii::app()->params['Error404']);
                }
        return $model;
    }

}
