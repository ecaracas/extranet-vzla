<?php

/**
 * @author thefactoryhka
 *
 */
class MachinesController extends Controller {

    public $modulo = "seriales";

    /**
     *
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     *      using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     *
     * @return array action filters
     */
    public function filters() {
        return array('accessControl');
        // perform access control for CRUD operations
        // 'postOnly + delete', // we only allow deletion via POST request
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * 
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array(
                'allow', /* Acciones Permitidas */
                'actions' => array(
                    'index',
                    'view',
                    'create',
                    'update',
                    'delete',
                    'autoincremento',
                    'codigo'),
                'users' => array(
                    '*')),
            array(
                'deny',
                'users' => array(
                    '*')));
    }

    /**
     * @param unknown $id
     * @param unknown $module
     */
    public function __construct($id, $module = null) {
        parent::__construct($id, $module);

        Yii::app()->params['title'] = '';
    }

    /**
     * @param unknown $id
     * @throws CHttpException
     */
    public function actionView($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_view')) {
            $model = $this->loadModel($id);
            $this->render('view', array('model' => $model));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    /**
     * 
     */
    public function actionCreate() {
        // if (Yii::app()->authRBAC->checkAccess($this->modulo . '_create')) {
        $model = new Machines();
        $modelLots = new Lots();
        $modelProducts = new Products();
        $transaction = $model->dbConnection->beginTransaction();
        try {
            $xx = '';
            if (isset($_POST['Machines'])) {
                $model->attributes = $_POST['Machines'];
                if ($model->validate()) {
                    $cantidad = $_POST['Machines']['cantidad'];
                    $modelLots->user_id = Yii::app()->user->id;
                    $modelLots->initial = $_POST['Machines']['inicial'];

                    $modelLots->quantity = $_POST['Machines']['cantidad'];
                    $modelLots->date = date("Y-m-d H:i:s");
                    $modelLots->save();
                    $serial = is_null($modelLots->initial) ? false : $modelLots->initial;


                    for ($j = 1; $j <= $cantidad; $j++):
                        $model = new Machines();
                        $model->serial = $this->Codigo($_POST['Products']['name'], $serial);
                        $model->dealer_id = Yii::app()->user->id;
                        $model->observation = $_POST['Machines']['observation'];
                        $model->status = 1;

                        $xx = $model->save();

                        if ($serial)
                            $serial++;
                    endfor;
                  // var_dump($modelProducts);die;


                    if ($xx != null) {
                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjvalidate']);
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
        }
        $this->render('create', array('model' => $model, 'modelProducts' => $modelProducts));
        // } else {
        // throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        // }
    }

    /**
     * @param unknown $id
     * @throws CHttpException
     */
    public function actionUpdate($id) {
        if (Yii::app()->authRBAC->checkAccess($this->modulo . '_update')) {
            $model = $this->loadModel($id);
            if (isset($_POST['Machines'])) {
                $model->attributes = $_POST['Machines'];
                if ($model->validate()) {
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
                        $this->redirect(array(
                            'view',
                            'id' => $model->id));
                    } else {
                        Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                    }
                } else {
                    Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
                }
            }

            $this->render('update', array('model' => $model));
        } else {
            throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        }
    }

    public function actionIndex() {
        // if (Yii::app()->authRBAC->checkAccess($this->modulo . '_index')) {
        $model = new Machines('search');
        $model->unsetAttributes(); // clear any default values

        if (isset($_GET['Machines'])) {
            $model->attributes = $_GET['Machines'];
        }

        if (isset($_GET['Machines_sort'])) {
            Yii::app()->params['GridViewOrder'] = TRUE;
        }
        $this->render('index', array('model' => $model));
        // } else {
        // throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
        // }
    }

    public function loadModel($id) {
        $sql = new CDbCriteria();
        $sql->params = array(':id' => intval($id));
        $sql->condition = "id = :id";
        $sql->addcondition("estatus <> 9");
        $model = Machines::model()->find($sql);
        if ($model === null) {
            throw new CHttpException(404, Yii::app()->params['Error404']);
        }
        return $model;
    }

    /**
     * Metodo autoincremento
     */
    public function autoincremento($serial = false, $sigla = false) {
        $digitos = 7;
//                         
        if (!$serial) {
            $criterio = new CDbCriteria();
            $criterio->select = "t.id, t.serial";
            // print $sigla; die;
            $criterio->condition = "t.id IN(SelecT MAX(id) from machines where serial like '%$sigla%')";
            $maquinas = Machines::model()->findAll($criterio);
                 // print_r($maquinas); die;
            if($maquinas){
            foreach ($maquinas as $maquina) {
                $id = $maquina['id'];
                $q = $maquina['serial'];
            }

            //var_dump($maquinas); die;    


            $a = substr($q, strrpos($q, '') + 6);
            $serial = $a + 1;
//agregamos esto para poder generar seriales nuevos para las maquinas que se vayan agregando.
}else{
    $serial = '0000001';
}
        }
        $i = strlen($serial);
        $string = '';
        while ($i < $digitos) {
            $string .= '0';
            $i++;
        }
        $serial_final = $string . $serial;
        return $serial_final;
    }

    public function Codigo($id = null, $serial) {
        $producto = Products::model()->find('active = 1 AND id = ' . $id);
        $sigla = $producto['sigla'];
        $b = $this->autoincremento($serial, $sigla);
        $codigo = $sigla . $b;
        return $codigo;
    }

}
