<?php

/**
 * This is the model class for table "st_orden_servicio".
 *
 * The followings are the available columns in table 'st_orden_servicio':
 * @property integer $id
 * @property string $id_num_orden
 * @property integer $id_distribuidor
 * @property integer $id_tecnico
 * @property integer $id_producto
 * @property string $marca
 * @property string $modelo
 * @property string $serial
 * @property string $descripcion_falla
 * @property string $revision_previa
 * @property string $contacto
 * @property string $telefono
 * @property string $medio_envio
 * @property string $medio_recepcion
 * @property string $direccion_recepcion
 * @property string $email
 * @property string $fecha_registro
 * @property string $fecha_enajenacion
 * @property integer $id_garantia
 *
 * The followings are the available model relations:
 * @property StAccesoriosOrden[] $stAccesoriosOrdens
 * @property StFallasOrden[] $stFallasOrdens
 * @property StHistorialOrdenes[] $stHistorialOrdenes
 * @property StOrdenEnvio[] $stOrdenEnvios
 * @property RbacUsuarios $idDistribuidor
 * @property RbacUsuarios $idTecnico
 * @property StRegistroFotografico[] $stRegistroFotograficos
 * @property StRepuestosOrden[] $stRepuestosOrdens
 */
class StOrdenServicio extends CActiveRecord
{
	
	public function tableName()
	{
		return 'st_orden_servicio';
	}

	
	public function rules()
	{
		
		return array(
			array('id_distribuidor, id_tecnico, id_producto, id_garantia', 'numerical', 'integerOnly'=>true),
			array('id_num_orden, marca, modelo, serial, contacto, telefono, medio_envio, medio_recepcion, direccion_recepcion, email', 'length', 'max'=>45),
			array('descripcion_falla, revision_previa', 'length', 'max'=>150),
			array('fecha_registro, fecha_enajenacion', 'safe'),
		
			array('id, id_num_orden, id_distribuidor, id_tecnico, id_producto, marca, modelo, serial, descripcion_falla, revision_previa, contacto, telefono, medio_envio, medio_recepcion, direccion_recepcion, email, fecha_registro, fecha_enajenacion, id_garantia', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'stAccesoriosOrdens' => array(self::HAS_MANY, 'StAccesoriosOrden', 'id_orden'),
			'stFallasOrdens' => array(self::HAS_MANY, 'StFallasOrden', 'id_orden'),
			'stHistorialOrdenes' => array(self::HAS_MANY, 'StHistorialOrdenes', 'id_orden'),
			'stOrdenEnvios' => array(self::HAS_MANY, 'StOrdenEnvio', 'id_orden'),
			'idDistribuidor' => array(self::BELONGS_TO, 'RbacUsuarios', 'id_distribuidor'),
			'idTecnico' => array(self::BELONGS_TO, 'RbacUsuarios', 'id_tecnico'),
			'stRegistroFotograficos' => array(self::HAS_MANY, 'StRegistroFotografico', 'id_orden'),
			'stRepuestosOrdens' => array(self::HAS_MANY, 'StRepuestosOrden', 'id_orden'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'id_num_orden' => Yii::t('lang','Id Num Orden'),
			'id_distribuidor' => Yii::t('lang','Id Distribuidor'),
			'id_tecnico' => Yii::t('lang','Id Tecnico'),
			'id_producto' => Yii::t('lang','Id Producto'),
			'marca' => Yii::t('lang','Marca'),
			'modelo' => Yii::t('lang','Modelo'),
			'serial' => Yii::t('lang','Serial'),
			'descripcion_falla' => Yii::t('lang','Descripcion Falla'),
			'revision_previa' => Yii::t('lang','Revision Previa'),
			'contacto' => Yii::t('lang','Contacto'),
			'telefono' => Yii::t('lang','Telefono'),
			'medio_envio' => Yii::t('lang','Medio Envio'),
			'medio_recepcion' => Yii::t('lang','Medio Recepcion'),
			'direccion_recepcion' => Yii::t('lang','Direccion Recepcion'),
			'email' => Yii::t('lang','Email'),
			'fecha_registro' => Yii::t('lang','Fecha Registro'),
			'fecha_enajenacion' => Yii::t('lang','Fecha Enajenacion'),
			'id_garantia' => Yii::t('lang','Id Garantia'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_num_orden',$this->id_num_orden,true);
		$criteria->compare('id_distribuidor',$this->id_distribuidor);
		$criteria->compare('id_tecnico',$this->id_tecnico);
		$criteria->compare('id_producto',$this->id_producto);
		$criteria->compare('marca',$this->marca,true);
		$criteria->compare('modelo',$this->modelo,true);
		$criteria->compare('serial',$this->serial,true);
		$criteria->compare('descripcion_falla',$this->descripcion_falla,true);
		$criteria->compare('revision_previa',$this->revision_previa,true);
		$criteria->compare('contacto',$this->contacto,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('medio_envio',$this->medio_envio,true);
		$criteria->compare('medio_recepcion',$this->medio_recepcion,true);
		$criteria->compare('direccion_recepcion',$this->direccion_recepcion,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_enajenacion',$this->fecha_enajenacion,true);
		$criteria->compare('id_garantia',$this->id_garantia);

		/* Condicion para no Mostrar los Eliminados en Gridview */
	

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
