<?php
/* @var $this OrdenServicioController */
/* @var $model STOrdenServicio */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
		'id' => 'storden-servicio-form',
		'htmlOptions' => array(
				'class' => 'form-horizontal',
				'role' => 'form'),
		'focus'=>array($model,'serial'),
));
?>
<?php //echo $form->errorSummary($model); ?>
<div class="row"><div class="col-xs-12 col-sm-9"><strong><h1 style="font-size: 2em">Introduzca Serial del Equipo</h1></strong></div></div>
<hr>
<div class="form-group">
	<div class="col-xs-12 col-sm-10">
		<?php echo $form->textField($model,'serial',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'serial'); ?>
	</div>
	<div class="col-xs-2 col-sm-2">
		<?php echo CHtml::Button(Yii::t('lang','Buscar'),array('class'=>Yii::app()->params['save-btn'], 'id' => 'buscaSerial')); ?>
	</div>
</div>
<div id="mensaje"></div>
<div style="display:none; position: relative" id="contenido">
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'id_distribuidor'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'id_distribuidor'); ?>
			<?php echo $form->error($model,'id_distribuidor'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'id_tecnico'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'id_tecnico'); ?>
			<?php echo $form->error($model,'id_tecnico'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'id_producto'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'id_producto'); ?>
			<?php echo $form->error($model,'id_producto'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'descripcion_falla'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'descripcion_falla',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'descripcion_falla'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'revision_previa'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'revision_previa',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'revision_previa'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'contacto'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'contacto',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'contacto'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'telefono'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'telefono',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'telefono'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'direccion_recepcion'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'direccion_recepcion',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'direccion_recepcion'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'email'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'email',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'fecha_registro'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'fecha_registro'); ?>
			<?php echo $form->error($model,'fecha_registro'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'fecha_enajenacion'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'fecha_enajenacion'); ?>
			<?php echo $form->error($model,'fecha_enajenacion'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12 col-sm-3">
			<?php echo $form->labelEx($model,'garantia'); ?>
		</div>
		<div class="col-xs-12 col-sm-9">
			<?php echo $form->textField($model,'garantia'); ?>
			<?php echo $form->error($model,'garantia'); ?>
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-xs-12 col-sm-6">
		<p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?></p>
		</div>
		<div class="col-xs-12 col-sm-6">
			<ul class="cmenuhorizontal" id="yw0">
				<li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?></li>
				<li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?></li>
			</ul>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
<script>
$( document ).ready(function() {
	$( "#buscaSerial" ).click(function() {
		$('#contenido').hide();
		$('#mensaje').empty();
		$('#mensaje').append('<img id="imgCarga" src="<?php echo Yii::app()->request->baseUrl ?>/img/cargando.gif">');
		$.ajax({
			method: "POST",
			url: "<?php echo Yii::app()->baseUrl;?>/servicioTecnico/OrdenServicio/ajaxComplete",
			data: { serial: $('#STOrdenServicio_serial').val()},
			dataType: "json"
			})
			.done(function( msg ) {
					if(typeof(msg.fallo) == "undefined")
					{
						$('#mensaje').empty();
						$('#contenido').slideDown(2000);
					}
					else
					{
						$('#mensaje').empty();
						$('#mensaje').append('<div>' + msg.fallo + '</div>');
					}
						
					
			});
		});
	});
</script>