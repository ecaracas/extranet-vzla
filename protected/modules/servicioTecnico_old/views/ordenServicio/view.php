<?php
/* @var $this OrdenServicioController */
/* @var $model STOrdenServicio */

$this->breadcrumbs=array(
    'Storden Servicios'=>array('index'),
    $model->id,
);

$this->menu=array(
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['update-text']), 
        'url'=>array('update', 
        'id'=>$model->id), 
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['index-text']), 
        'url'=>array('index'), 
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['update-text']) .' '. Yii::t('lang','STOrdenServicio'); ?></h3>

                <div class="menu-tool">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
                  <?php $this->ToolActionsRight(); ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo Yii::app()->params['ClassTable']; ?>">        
                        <tbody>
                            	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('id_distribuidor')); ?></th>
	<td><?php echo CHtml::encode($model->id_distribuidor); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('id_tecnico')); ?></th>
	<td><?php echo CHtml::encode($model->id_tecnico); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('id_producto')); ?></th>
	<td><?php echo CHtml::encode($model->id_producto); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('serial')); ?></th>
	<td><?php echo CHtml::encode($model->serial); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('descripcion_falla')); ?></th>
	<td><?php echo CHtml::encode($model->descripcion_falla); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('revision_previa')); ?></th>
	<td><?php echo CHtml::encode($model->revision_previa); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('contacto')); ?></th>
	<td><?php echo CHtml::encode($model->contacto); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('telefono')); ?></th>
	<td><?php echo CHtml::encode($model->telefono); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('medio_envio')); ?></th>
	<td><?php echo CHtml::encode($model->medio_envio); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('medio_recepcion')); ?></th>
	<td><?php echo CHtml::encode($model->medio_recepcion); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('direccion_recepcion')); ?></th>
	<td><?php echo CHtml::encode($model->direccion_recepcion); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></th>
	<td><?php echo CHtml::encode($model->email); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('fecha_registro')); ?></th>
	<td><?php echo CHtml::encode($model->fecha_registro); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('fecha_enajenacion')); ?></th>
	<td><?php echo CHtml::encode($model->fecha_enajenacion); ?></td>
	</tr>
	<tr>
	<th><?php echo CHtml::encode($model->getAttributeLabel('garantia')); ?></th>
	<td><?php echo CHtml::encode($model->garantia); ?></td>
	</tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>