<?php
/* @var $this HistorialOrdenesController */
/* @var $data STHistorialOrdenes */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id_orden')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->id_orden); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id_usuariorevision')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->id_usuariorevision); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id_estatus')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->id_estatus); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->descripcion); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->fecha); ?></div>
	
</div>

<hr />