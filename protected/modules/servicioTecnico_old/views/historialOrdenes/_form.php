<?php
/* @var $this HistorialOrdenesController */
/* @var $model STHistorialOrdenes */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'sthistorial-ordenes-form',
  'enableAjaxValidation'=>false,
     'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form'
    ),
)); ?>



        <?php //echo $form->errorSummary($model); ?>

                     <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'id'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'id'); ?>
    
                     <?php echo $form->error($model,'id'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'id_orden'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'id_orden'); ?>
    
                     <?php echo $form->error($model,'id_orden'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'id_usuariorevision'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'id_usuariorevision'); ?>
    
                     <?php echo $form->error($model,'id_usuariorevision'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'id_estatus'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'id_estatus'); ?>
    
                     <?php echo $form->error($model,'id_estatus'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'descripcion'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'descripcion',array('size'=>45,'maxlength'=>45)); ?>
    
                     <?php echo $form->error($model,'descripcion'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'fecha'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'fecha'); ?>
    
                     <?php echo $form->error($model,'fecha'); ?>
                 </div>
                      
            </div>
                    <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                        <p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>
</p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?>
</li>
                    <li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?>
</li>
                </ul>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>
