<?php

/**
 * This is the model class for table "st_historial_ordenes".
 *
 * The followings are the available columns in table 'st_historial_ordenes':
 * @property integer $id
 * @property integer $id_orden
 * @property integer $id_usuariorevision
 * @property integer $id_estatus
 * @property string $descripcion
 * @property string $fecha
 */
class STHistorialOrdenes extends CActiveRecord
{
	
	public function tableName()
	{
		return 'st_historial_ordenes';
	}

	
	public function rules()
	{
		
		return array(
			array('id', 'required'),
			array('id, id_orden, id_usuariorevision, id_estatus', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>45),
			array('fecha', 'safe'),
		
			array('id, id_orden, id_usuariorevision, id_estatus, descripcion, fecha', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'id_orden' => Yii::t('lang','Id Orden'),
			'id_usuariorevision' => Yii::t('lang','Id Usuariorevision'),
			'id_estatus' => Yii::t('lang','Id Estatus'),
			'descripcion' => Yii::t('lang','Descripcion'),
			'fecha' => Yii::t('lang','Fecha'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_orden',$this->id_orden);
		$criteria->compare('id_usuariorevision',$this->id_usuariorevision);
		$criteria->compare('id_estatus',$this->id_estatus);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('fecha',$this->fecha,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
