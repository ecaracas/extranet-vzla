<?php

/**
 * This is the model class for table "st_orden_servicio".
 *
 * The followings are the available columns in table 'st_orden_servicio':
 * @property integer $id
 * @property integer $id_distribuidor
 * @property integer $id_tecnico
 * @property integer $id_producto
 * @property string $serial
 * @property string $descripcion_falla
 * @property string $revision_previa
 * @property string $contacto
 * @property string $telefono
 * @property string $medio_envio
 * @property string $medio_recepcion
 * @property string $direccion_recepcion
 * @property string $email
 * @property string $fecha_registro
 * @property string $fecha_enajenacion
 * @property integer $garantia
 */
class STOrdenServicio extends CActiveRecord
{
	public function tableName()
	{
		return 'st_orden_servicio';
	}
	public function rules()
	{
		return array(
				array(
						'id',
						'required'),
				array(
						'id, id_distribuidor, id_tecnico, id_producto, garantia',
						'numerical',
						'integerOnly' => true),
				array(
						'serial, descripcion_falla, revision_previa, contacto, telefono, medio_envio, medio_recepcion, direccion_recepcion, email',
						'length',
						'max' => 45),
				array(
						'fecha_registro, fecha_enajenacion',
						'safe'),
				
				array(
						'id, id_distribuidor, id_tecnico, id_producto, serial, descripcion_falla, revision_previa, contacto, telefono, medio_envio, medio_recepcion, direccion_recepcion, email, fecha_registro, fecha_enajenacion, garantia',
						'safe',
						'on' => 'search'));
	}
	public function relations()
	{
		return array(
				'usuarioDist' => array(self::BELONGS_TO, 'RBACUsuarios', 'id_distribuidor'),
				'usuarioTec' => array(self::BELONGS_TO, 'RBACUsuarios', 'id_tecnico')
			
		);
	}
	public function attributeLabels()
	{
		return array(
				'id' => Yii::t('lang', 'ID'),
				'id_distribuidor' => Yii::t('lang', 'Distribuidor'),
				'id_tecnico' => Yii::t('lang', 'Tecnico'),
				'id_producto' => Yii::t('lang', 'Producto'),
				'serial' => Yii::t('lang', 'Serial'),
				'descripcion_falla' => Yii::t('lang', 'Descripcion'),
				'revision_previa' => Yii::t('lang', 'Revision Previa'),
				'contacto' => Yii::t('lang', 'Contacto'),
				'telefono' => Yii::t('lang', 'Telefono'),
				'medio_envio' => Yii::t('lang', 'Medio Envio'),
				'medio_recepcion' => Yii::t('lang', 'Medio Recepcion'),
				'direccion_recepcion' => Yii::t('lang', 'Direccion Recepción'),
				'email' => Yii::t('lang', 'Email'),
				'fecha_registro' => Yii::t('lang', 'Fecha Registro'),
				'fecha_enajenacion' => Yii::t('lang', 'Fecha Enajenacion'),
				'garantia' => Yii::t('lang', 'Garantia'));
	}
	public function search()
	{
		$criteria = new CDbCriteria();
		
		$criteria->compare('id', $this->id);
		$criteria->compare('id_distribuidor', $this->id_distribuidor);
		$criteria->compare('id_tecnico', $this->id_tecnico);
		$criteria->compare('id_producto', $this->id_producto);
		$criteria->compare('serial', $this->serial, true);
		$criteria->compare('descripcion_falla', $this->descripcion_falla, true);
		$criteria->compare('revision_previa', $this->revision_previa, true);
		$criteria->compare('contacto', $this->contacto, true);
		$criteria->compare('telefono', $this->telefono, true);
		$criteria->compare('medio_envio', $this->medio_envio, true);
		$criteria->compare('medio_recepcion', $this->medio_recepcion, true);
		$criteria->compare('direccion_recepcion', $this->direccion_recepcion, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('fecha_registro', $this->fecha_registro, true);
		$criteria->compare('fecha_enajenacion', $this->fecha_enajenacion, true);
		$criteria->compare('garantia', $this->garantia);
		if(Yii::app()->params['GridViewOrder'] == FALSE)
		{
			$criteria->order = "id DESC";
		}
		return new CActiveDataProvider($this, array('criteria' => $criteria));
	}
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
