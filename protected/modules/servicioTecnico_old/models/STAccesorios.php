<?php

/**
 * This is the model class for table "st_accesorios".
 *
 * The followings are the available columns in table 'st_accesorios':
 * @property integer $id
 * @property string $nombre
 * @property string $fecha
 */
class STAccesorios extends CActiveRecord
{
	
	public function tableName()
	{
		return 'st_accesorios';
	}

	
	public function rules()
	{
		
		return array(
			array('id', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>45),
			array('fecha', 'safe'),
		
			array('id, nombre, fecha', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'nombre' => Yii::t('lang','Nombre'),
			'fecha' => Yii::t('lang','Fecha'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('fecha',$this->fecha,true);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
