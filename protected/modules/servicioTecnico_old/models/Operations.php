<?php

/**
 * This is the model class for table "operations".
 *
 * The followings are the available columns in table 'operations':
 * @property integer $id
 * @property string $serial
 * @property string $date
 * @property integer $dealer_id
 * @property integer $techinician_id
 * @property integer $operation_type_id
 * @property integer $final_client_id
 * @property string $observation
 * @property string $ruc
 * @property string $business_name
 * @property string $address
 * @property string $install_address
 * @property string $final_cliente_phone
 * @property string $email_final_client
 * @property string $seal
 */
class Operations extends CActiveRecord
{
	public $desde;
	public $hasta;
	public $nombreapellido;
	
	public function behaviors()
	{
		return array(
				// Classname => path to Class
				'LogsableBehavior' => 'application.behaviors.LogsableBehavior');
	}
	
	public function tableName()
	{
		return 'operations';
	}

	
	public function rules()
	{
		
		return array(
			array('serial, date, dealer_id, techinician_id, operation_type_id, ruc, dv', 'required', 'message' => 'Este campo es obligatorio'),
			array('dealer_id, techinician_id, operation_type_id, final_client_id, seal, final_cliente_phone, dv', 'numerical', 'integerOnly'=>true),
			array('serial', 'length', 'max'=>13),
			array('address', 'length', 'max'=>200),
			//array('ruc', 'length', 'max'=>16),
			array('business_name', 'length', 'max'=>60),
			array('observation', 'safe'),
			array('serial', 'serialIntervalExists'),
			array('date', 'menorque'),
			array('email_final_client', 'email'),
			array('id, serial, date, dealer_id, techinician_id, operation_type_id, final_client_id, observation, ruc, business_name, address, install_address, final_cliente_phone, email_final_client, seal', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
					'rbacusuario' => array(self::BELONGS_TO, 'RBACUsuarios', 'dealer_id'),
					'rbactecnico' => array(self::BELONGS_TO, 'RBACUsuarios', 'techinician_id'),
					'operationType' => array(self::BELONGS_TO, 'OperationTypes', 'operation_type_id'),
					'finalClient' => array(self::BELONGS_TO, 'FinalClient', 'final_client_id')
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'serial' => Yii::t('lang','Número de Registro'),
			'date' => Yii::t('lang','Fecha de enajenación'),
			'dealer_id' => Yii::t('lang','Distribuidor'),
			'techinician_id' => Yii::t('lang','Tecnico'),
			'operation_type_id' => Yii::t('lang','Tipo de operación'),
			'final_client_id' => Yii::t('lang','Cliente Final'),
			'observation' => Yii::t('lang','Observaciones'),
			'ruc' => Yii::t('lang','Ruc Cliente'),
			'dv' => Yii::t('lang','DV'),
			'business_name' => Yii::t('lang','Razón Social'),
			'address' => Yii::t('lang','Dirección fiscal'),
			'install_address' => Yii::t('lang','Dirección de instalación'),
			'final_cliente_phone' => Yii::t('lang','Teléfono'),
			'email_final_client' => Yii::t('lang','Correo'),
			'seal' => Yii::t('lang','Número del precinto'),
			'created_at' => Yii::t('lang','Fecha de registro'),
			'nombreapellido' => Yii::t('lang','Nombre Usuario'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->select = 't.id,t.serial,t.date,t.dealer_id,t.techinician_id,t.operation_type_id,t.final_client_id,t.observation,t.ruc,business_name,t.address,t.install_address,t.final_cliente_phone,t.email_final_client,t.seal, CONCAT(us.nombre, " ",us.apellido) as nombreapellido';
		$criteria->compare('id',$this->id);
		$criteria->compare('serial',$this->serial,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('dealer_id',$this->dealer_id);
		$criteria->compare('techinician_id',$this->techinician_id);
		$criteria->compare('operation_type_id',$this->operation_type_id);
		$criteria->compare('final_client_id',$this->final_client_id);
		$criteria->compare('observation',$this->observation,true);
		$criteria->compare('ruc',$this->ruc,true);
		$criteria->compare('business_name',$this->business_name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('install_address',$this->install_address,true);
		$criteria->compare('final_cliente_phone',$this->final_cliente_phone,true);
		$criteria->compare('email_final_client',$this->email_final_client,true);
		$criteria->compare('seal',$this->seal,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->join = 'LEFT JOIN rbac_usuarios us ON (t.techinician_id = us.id)';
		if(Yii::app()->user->getState('rol') == 3)
		{
			$criteria->addcondition("t.dealer_id = " . Yii::app()->user->getState('id'));
		}
		elseif(Yii::app()->user->getState('rol') == 8)
		{
			$criteria->addcondition("t.techinician_id = ".Yii::app()->user->getState('id'));
		}
		$criteria->order = "id DESC";
		/* Condicion para no Mostrar los Eliminados en Gridview */
		//$criteria->addcondition("estatus <> 9");
		if(Yii::app()->params['GridViewOrder'] == FALSE)
		{
			if(Yii::app()->user->getState('rol') == '3' or  Yii::app()->user->getState('rol') == '5'){
				$criteria->condition = "dealer_id = '".Yii::app()->user->getState('id'). "'";
			}
			
		}
		$data = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

		$_SESSION['promotion.excel'] = new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => false,
                 ));
		return $data;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Validación para saber si el serial cumple con la estructura de sigla y
	 * si el serial ya se encuentra registrado o generado.
	 * @param unknown $attribute
	 */
	public function serialIntervalExists($attribute)
	{
		$sigla = strtolower(substr(trim($this->$attribute), 0, 6));
		if(is_null(Products::model()->find("sigla = '" . $sigla . "'")))
		{
			$this->addError($attribute, 'La sigla introducida es incorrecta!');
		}
		if(!is_null(Operations::model()->find("serial = '" . $this->$attribute . "' and operation_type_id = 1")))
		{
			$this->addError($attribute, 'Serial ya se encuentra enajenado!');
		}
		
		if(is_null(Machines::model()->find("serial = '" . $this->$attribute . "'")))
		{
			$this->addError($attribute, 'Serial no se encuentra registrado!');
		}
	}
	
	public function menorque($attribute)
	{
		if (strtotime($this->$attribute) > strtotime(date("Y-m-d")))
			$this->addError($attribute,'La fecha no puede ser mayor del ' . date("d-m-Y") . '.');
	}
}
