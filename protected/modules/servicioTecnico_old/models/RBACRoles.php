<?php

/**
 * This is the model class for table "rbac_roles".
 *
 * The followings are the available columns in table 'rbac_roles':
 * @property integer $id
 * @property string $descripcion
 * @property integer $estatus
 * @property string $registro_fecha
 * @property string $registro_hora
 * @property integer $registro_usuario_id
 * @property string $modificado_fecha
 * @property string $modificado_hora
 * @property integer $modificado_usuario_id
 * @property integer $principal
 * @property integer $secuencia
 *
 * The followings are the available model relations:
 * @property RbacUsuarios $registroUsuario
 * @property RbacRolesOpciones[] $rbacRolesOpciones
 * @property RbacUsuarios[] $rbacUsuarioses
 */
class RBACRoles extends CActiveRecord
{
	
	public function tableName()
	{
		return 'rbac_roles';
	}
        
        public function behaviors(){
            return array(
                // Classname => path to Class
                'LogsableBehavior'=>'application.behaviors.LogsableBehavior',

            );
        }

	
	public function rules()
	{
		
		return array(
			array('descripcion, registro_fecha, registro_usuario_id, modificado_fecha, modificado_usuario_id', 'required'),
			array('estatus, registro_usuario_id, modificado_usuario_id, principal, secuencia', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>100),
			array('registro_hora, modificado_hora', 'safe'),		
			array('id, descripcion, estatus, registro_fecha, registro_hora, registro_usuario_id, modificado_fecha, modificado_hora, modificado_usuario_id, principal, secuencia', 'safe', 'on'=>'search'),
		);
	}	
	public function relations()
	{
		
		return array(
			'registroUsuario' => array(self::BELONGS_TO, 'RBACUsuarios', 'registro_usuario_id'),
			'rbacRolesOpciones' => array(self::HAS_MANY, 'RBACRolesOpciones', 'rol_id'),
			'rbacUsuarioses' => array(self::HAS_MANY, 'RBACUsuarios', 'rol_id'),
		);
	}
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'descripcion' => Yii::t('lang','Descripcion'),
			'estatus' => Yii::t('lang','Estatus'),
			'registro_fecha' => Yii::t('lang','Registro Fecha'),
			'registro_hora' => Yii::t('lang','Registro Hora'),
			'registro_usuario_id' => Yii::t('lang','Registro Usuario'),
			'modificado_fecha' => Yii::t('lang','Modificado Fecha'),
			'modificado_hora' => Yii::t('lang','Modificado Hora'),
			'modificado_usuario_id' => Yii::t('lang','Modificado Usuario'),
			'principal' => Yii::t('lang','Principal'),
			'secuencia' => Yii::t('lang','Secuencia'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.id',$this->descripcion,true);
		$criteria->compare('t.estatus',$this->estatus);
		$criteria->compare('t.registro_fecha',$this->registro_fecha,true);
		$criteria->compare('t.registro_hora',$this->registro_hora,true);
		$criteria->compare('t.registro_usuario_id',$this->registro_usuario_id);
		$criteria->compare('t.modificado_fecha',$this->modificado_fecha,true);
		$criteria->compare('t.modificado_hora',$this->modificado_hora,true);
		$criteria->compare('t.modificado_usuario_id',$this->modificado_usuario_id);
		$criteria->compare('t.principal',$this->principal);
		$criteria->compare('t.secuencia',$this->secuencia);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");
          
                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "t.id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
