<?php

/**
 * This is the model class for table "st_orden_envio".
 *
 * The followings are the available columns in table 'st_orden_envio':
 * @property integer $id
 * @property integer $id_orden
 * @property string $modo_envio
 * @property string $numero_guia
 * @property string $fecha
 * @property string $direccion_envio
 * @property integer $id_usuario
 */
class STOrdenEnvio extends CActiveRecord
{
	
	public function tableName()
	{
		return 'st_orden_envio';
	}

	
	public function rules()
	{
		
		return array(
			array('id', 'required'),
			array('id, id_orden, id_usuario', 'numerical', 'integerOnly'=>true),
			array('modo_envio, direccion_envio', 'length', 'max'=>45),
			array('numero_guia', 'length', 'max'=>10),
			array('fecha', 'safe'),
		
			array('id, id_orden, modo_envio, numero_guia, fecha, direccion_envio, id_usuario', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'id_orden' => Yii::t('lang','Id Orden'),
			'modo_envio' => Yii::t('lang','Modo Envio'),
			'numero_guia' => Yii::t('lang','Numero Guia'),
			'fecha' => Yii::t('lang','Fecha'),
			'direccion_envio' => Yii::t('lang','Direccion Envio'),
			'id_usuario' => Yii::t('lang','Id Usuario'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_orden',$this->id_orden);
		$criteria->compare('modo_envio',$this->modo_envio,true);
		$criteria->compare('numero_guia',$this->numero_guia,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('direccion_envio',$this->direccion_envio,true);
		$criteria->compare('id_usuario',$this->id_usuario);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
