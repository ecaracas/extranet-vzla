<?php
class OrdenServicioController extends Controller
{
	/**
	 *
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 *      using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';
	
	/**
	 *
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete'); // we only allow deletion via POST request
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array(
						'allow', /* Acciones Permitidas*/
				'actions' => array(
								'index',
								'view',
								'create',
								'update',
								'delete',
								'ajaxcomplete'),
						'users' => array(
								'*')),
				array(
						'deny',
						'users' => array(
								'*')));
	}
	public $modulo = "servicioTecnico";
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		
		Yii::app()->params['title'] = '';
	}
	public function actionView($id)
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_view'))
		{
			
			$model = $this->loadModel($id);
			
			$this->render('view', array(
					'model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	public function actionCreate()
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_create'))
		{
			
			$model = new STOrdenServicio();
			if(isset($_POST['STOrdenServicio']))
			{
				$model->attributes = $_POST['STOrdenServicio'];
				if($model->validate())
				{
					if($model->save())
					{
						
						Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model->id));
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
				{
					Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
				}
			}
			
			$this->render('create', array(
					'model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	public function actionUpdate($id)
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_update'))
		{
			
			$model = $this->loadModel($id);
			
			if(isset($_POST['STOrdenServicio']))
			{
				$model->attributes = $_POST['STOrdenServicio'];
				
				if($model->validate())
				{
					if($model->save())
					{
						Yii::app()->user->setFlash('success', Yii::app()->params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model->id));
					}
					else
					{
						Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
					}
				}
				else
				{
					Yii::app()->user->setFlash('danger', Yii::app()->params['msjdanger']);
				}
			}
			
			$this->render('update', array(
					'model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	public function actionIndex()
	{
		if(Yii::app()->authRBAC->checkAccess($this->modulo . '_index'))
		{
			
			$model = new STOrdenServicio('search');
			
			$model->unsetAttributes(); // clear any default values
			
			if(isset($_GET['STOrdenServicio']))
			{
				$model->attributes = $_GET['STOrdenServicio'];
			}
			
			if(isset($_GET['STOrdenServicio_sort']))
			{
				
				Yii::app()->params['GridViewOrder'] = TRUE;
			}
			
			$this->render('index', array(
					'model' => $model));
		}
		else
		{
			throw new CHttpException(403, Yii::app()->params['ErrorAccesoDenegado']);
		}
	}
	public function loadModel($id)
	{
		$sql = new CDbCriteria();
		$sql->params = array(
				':id' => intval($id));
		$sql->condition = "id = :id";
		$sql->addcondition("estatus <> 9");
		$model = STOrdenServicio::model()->find($sql);
		if($model === null)
		{
			throw new CHttpException(404, Yii::app()->params['Error404']);
		}
		return $model;
	}
	
	public function actionAjaxComplete()
	{
		if(!YII_DEBUG && !Yii::app()->request->isAjaxRequest)
		{
			throw new CHttpException('403', 'Forbidden access.');
		}
		if(isset($_POST['serial']))
		{
			$maquina = Machines::model()->find("serial = '" . $_POST['serial'] . "'");
			if(!is_null($maquina))
			{
				$enajenacion = FicalizedMachines::model()->find("machine_id = " . $maquina->id);
				if(!is_null($enajenacion))
				{
					$dataAsocMaquina = 
					$arraymaquina = new ArrayObject($enajenacion->attributes);
					echo json_encode($arraymaquina->getArrayCopy());
				}
				else 
				{
					echo json_encode(array('fallo' => 'Equipo no se encuentra enajenado consulte con su distribuidor.'));
				}
			}
			else
			{
				echo json_encode(array('fallo' => 'No se encuentra registrado el serial.'));
			}
		}
		Yii::app()->end();
	}
}
