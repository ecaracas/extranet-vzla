 <?php

/**
 * This is the model class for table "logs".
 *
 * The followings are the available columns in table 'logs':
 * @property integer $id
 * @property integer $user_id
 * @property string $module
 * @property string $country
 * @property string $description
 * @property string $ip
 * @property string $date
 * @property string $device
 *
 * The followings are the available model relations:
 * @property RbacUsuarios $user
 */
class Logs extends CActiveRecord
{
    
    public function tableName()
    {
        return 'logs';
    }

    
    public function rules()
    {
        
        return array(
            array('user_id', 'numerical', 'integerOnly'=>true),
            array('module, country', 'length', 'max'=>50),
            array('description,change', 'length', 'max'=>500),
            array('ip', 'length', 'max'=>20),
            array('device', 'length', 'max'=>100),
            array('date', 'safe'),
        
            array('id, user_id, module, country, description, ip, date, device', 'safe', 'on'=>'search'),
        );
    }

    
    public function relations()
    {
        
        return array(
            'user' => array(self::BELONGS_TO, 'RbacUsuarios', 'user_id'),
        );
    }

    
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('lang','ID'),
            'user_id' => Yii::t('lang','User'),
            'module' => Yii::t('lang','Module'),
            'country' => Yii::t('lang','Country'),
            'description' => Yii::t('lang','Description'),
            'change' => Yii::t('lang','Cambios'),
            'ip' => Yii::t('lang','Ip'),
            'date' => Yii::t('lang','Date'),
            'device' => Yii::t('lang','Device'),
        );
    }

    public function search()
    {
        
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('module',$this->module,true);
        $criteria->compare('country',$this->country,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('change',$this->change,true);
        $criteria->compare('ip',$this->ip,true);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('device',$this->device,true);

        /* Condicion para no Mostrar los Eliminados en Gridview */
      

        if(Yii::app()->params['GridViewOrder'] == FALSE){
        $criteria->order = "id DESC";        
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}