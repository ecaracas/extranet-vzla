
<?php
/* @var $this LogsController */
/* @var $model Logs */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'logs-form',
  'enableAjaxValidation'=>false,
     'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form'
    ),
)); ?>



        <?php //echo $form->errorSummary($model); ?>

                     <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'user_id'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'user_id'); ?>
    
                     <?php echo $form->error($model,'user_id'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'module'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'module',array('size'=>50,'maxlength'=>50)); ?>
    
                     <?php echo $form->error($model,'module'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'country'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'country',array('size'=>50,'maxlength'=>50)); ?>
    
                     <?php echo $form->error($model,'country'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'description'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>500)); ?>
    
                     <?php echo $form->error($model,'description'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'ip'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'ip',array('size'=>20,'maxlength'=>20)); ?>
    
                     <?php echo $form->error($model,'ip'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'date'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'date'); ?>
    
                     <?php echo $form->error($model,'date'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'device'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'device',array('size'=>60,'maxlength'=>100)); ?>
    
                     <?php echo $form->error($model,'device'); ?>
                 </div>
                      
            </div>
                    <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                        <p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>
</p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?>
</li>
                    <li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?>
</li>
                </ul>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>