<?php
/* @var $this LogsController */
/* @var $data Logs */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('description')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->description); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('ip')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->ip); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('date')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->date); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->user); ?></div>
	
</div>

<hr />