<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
    '$label'=>array('index'),
    \$model->{$nameColumn},
);\n";
?>

$this->menu=array(
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['update-text']), 
        'url'=>array('update', 
        'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>), 
        'linkOptions' => array('class' => Yii::app()->params['update-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')),
    array(
        'label'=>Yii::t('lang',Yii::app()->params['index-text']), 
        'url'=>array('index'), 
        'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>


<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

                <h3 class="panel-title"><?php echo "<?php echo "; ?> Yii::t('lang',Yii::app()->params['update-text']) .' '. Yii::t('lang','<?php echo $this->modelClass; ?>'); ?></h3>

                <div class="menu-tool">
                    <?php echo "<?php"; ?> 
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
                    ?>
                </div>
                  <?php echo "<?php \$this->ToolActionsRight(); ?>"; ?>      
            </div>
            <div class="panel-body">

                <div class="table-responsive">        
                    <table class="<?php echo "<?php echo Yii::app()->params['ClassTable']; ?>"; ?>">        
                        <tbody>
                            <?php
                            /* echo "\t<div class='row'>\n<div class='col-xs-1'><?php echo CHtml::encode(\$data->getAttributeLabel('{$this->tableSchema->primaryKey}')); ?></div>\n";
                              echo "\t<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode(\$data->{$this->tableSchema->primaryKey}), array('view', 'id'=>\$data->{$this->tableSchema->primaryKey})); ?></div>\n\t\n</div>\n"; */
                            $count = 0;
                            foreach ($this->tableSchema->columns as $column) {
                                if ($column->isPrimaryKey)
                                    continue;
                                
                                if($column->name != 'principal' AND $column->name != 'secuencia'){            

                                echo "\t<tr>\n";
                                echo "\t<th><?php echo CHtml::encode(\$model->getAttributeLabel('{$column->name}')); ?></th>\n";
                                echo "\t<td><?php echo CHtml::encode(\$model->{$column->name}); ?></td>\n";
                                echo "\t</tr>\n";
                                
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>