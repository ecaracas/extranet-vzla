<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
    '$label'=>array('index'),
    Yii::app('lang','Manage'),
);\n";
?>
Yii::app()->params['IDGridview'] = '<?php echo $this->class2id($this->modelClass); ?>-grid';
$this->menu=array(     
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
);

?>
<div class="row">
    <div class="col-xs-12">
<div class="panel panel-default">
    <div class="panel-heading navbar-tool">
        
        <h3 class="panel-title"><?php echo "<?php echo "; ?> Yii::t('lang',Yii::app()->params['index-text']) .' '. Yii::t('lang','<?php echo $this->pluralize($this->class2name($this->modelClass)); ?>'); ?></h3>
        
       <div class="menu-tool">
            
            <div class="pagesize"> 
             <?php echo "<?php"; ?> $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
             </div>         
            
            <div class="menu-items">
                 <?php echo "<?php"; ?> 
        $this->widget('zii.widgets.CMenu', array(
            'items' => $this->menu,
            'encodeLabel' => FALSE,
            'htmlOptions' => array('class' => 'cmenuhorizontal'),
        ));
        ?>
            </div>
        </div>        
        <?php echo "<?php \$this->ToolActionsRight(); ?>"; ?>         
        
    </div>
    <div class="panel-body"> 

<div class="table-responsive">
<?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>Yii::app()->params['IDGridview'],
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'columns'=>array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
    if(++$count==7)
        echo "\t\t/*\n";
    echo "\t\t array('name'=>'".$column->name."','value'=>'\$data->".$column->name."'),\n";
}
if($count>=7)
    echo "\t\t*/\n";
?>
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['view-text']),            
            'label' => Yii::app()->params['view-icon'],
            'linkHtmlOptions' => array('class' => 'view '.Yii::app()->params['view-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))', 
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
        ),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['update-text']),            
            'label' => Yii::app()->params['update-icon'],
            'linkHtmlOptions' => array('class' => 'edit '.Yii::app()->params['update-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
        ),
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['delete-text']),           
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
        ),
    ),
)); ?>
</div>
    </div>
    </div>
 </div>
     </div>
<?php echo "<?php \n"; ?> 
    
    if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete', array());    

    } 
?>