<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>
<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
  'id'=>'" . $this->class2id($this->modelClass) . "-form',
  'enableAjaxValidation'=>false,
     'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form'
    ),
)); ?>\n"; ?>



        <?php echo "<?php //echo \$form->errorSummary(\$model); ?>\n"; ?>

        <?php
        foreach ($this->tableSchema->columns as $column) {
            if ($column->autoIncrement)
                continue;
            
            if($column->name != 'registro_fecha' AND $column->name != 'registro_hora' AND $column->name != 'modificado_fecha' AND $column->name != 'modificado_hora' AND $column->name != 'registro_usuario_id' AND $column->name != 'modificado_usuario_id' AND $column->name != 'principal' AND $column->name != 'secuencia'){            
            ?>
             <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo "<?php echo " . $this->generateActiveLabel($this->modelClass, $column) . "; ?>\n"; ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo "<?php echo " . $this->generateActiveField($this->modelClass, $column) . "; ?>\n"; ?>    
                     <?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
                 </div>
                      
            </div>
            <?php
        }
        }
        ?>
        <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                        <p class="text-left"><?php echo "<?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>\n"; ?></p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo "<?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?>\n"; ?></li>
                    <li class="bottom"><?php echo "<?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?>\n"; ?></li>
                </ul>
            </div>
            
        </div>

        <?php echo "<?php \$this->endWidget(); ?>\n"; ?>
