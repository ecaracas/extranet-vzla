<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
    '$label'=>array('index'),
    'Nuevo',
);\n";
?>

$this->menu=array(
array(
'label'=>Yii::t('lang',Yii::app()->params['index-text']), 
'url'=>array('index'), 
'linkOptions' => array('class' => Yii::app()->params['index-btn'] . ' btn-sm'),
'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_index')),
);
?>
<div class="row">
    <div class="col-xs-12 col-md-10 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading navbar-tool">

            <h3 class="panel-title"><?php echo "<?php echo "; ?> Yii::t('lang',Yii::app()->params['create-text']) .' '. Yii::t('lang','<?php echo $this->modelClass; ?>'); ?></h3>
            <div class="menu-tool">
            <?php echo "<?php"; ?> 
                    $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => FALSE,
                    'htmlOptions' => array('class' => 'cmenuhorizontal'),
                    ));
            ?>
            </div>
              <?php echo "<?php \$this->ToolActionsRight(); ?>"; ?>           

            </div>
            <div class="panel-body">

            <?php echo "<?php \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>

            </div>
        </div>
    </div>
</div>