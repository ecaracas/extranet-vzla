<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'menu-form',
  'enableAjaxValidation'=>false,
     'htmlOptions'=>array(
        'class'=>'form-horizontal',
        'role'=>'form'
    ),
)); ?>



        <?php //echo $form->errorSummary($model); ?>

                     <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'descripcion'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>100)); ?>
    
                     <?php echo $form->error($model,'descripcion'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'url'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
    
                     <?php echo $form->error($model,'url'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'url_tipo'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'url_tipo'); ?>
    
                     <?php echo $form->error($model,'url_tipo'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'categoria'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'categoria',array('size'=>20,'maxlength'=>20)); ?>
    
                     <?php echo $form->error($model,'categoria'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'estatus'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'estatus'); ?>
    
                     <?php echo $form->error($model,'estatus'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'orden'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'orden'); ?>
    
                     <?php echo $form->error($model,'orden'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'icono'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'icono',array('size'=>50,'maxlength'=>50)); ?>
    
                     <?php echo $form->error($model,'icono'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'modulo'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'modulo',array('size'=>30,'maxlength'=>30)); ?>
    
                     <?php echo $form->error($model,'modulo'); ?>
                 </div>
                      
            </div>
                         <div class="form-group">
                 <div class="col-xs-12 col-sm-3">
                      <?php echo $form->labelEx($model,'jerarquia'); ?>
                 </div>
                   
                 <div class="col-xs-12 col-sm-9">
                    <?php echo $form->textField($model,'jerarquia'); ?>
    
                     <?php echo $form->error($model,'jerarquia'); ?>
                 </div>
                      
            </div>
                    <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                        <p class="text-left"><?php echo Yii::t('lang',Yii::app()->params['camposrequeridos']); ?>
</p>
            </div>
            <div class="col-xs-12 col-sm-6">                
                <ul class="cmenuhorizontal" id="yw0">
                    <li class="bottom"><?php echo CHtml::submitButton(Yii::t('lang',Yii::app()->params['save-text']),array('class'=>Yii::app()->params['save-btn'])); ?>
</li>
                    <li class="bottom"><?php echo CHtml::link(Yii::t('lang',Yii::app()->params['cancel-text']),Yii::app()->controller->createUrl('index'),array('class'=>Yii::app()->params['cancel-btn'])); ?>
</li>
                </ul>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>
