<?php
/* @var $this MenuController */
/* @var $model Menu */

$this->breadcrumbs=array(
    'Menus'=>array('index'),
    Yii::app('lang','Manage'),
);
Yii::app()->params['IDGridview'] = 'menu-grid';
$this->menu=array(     
    array(
        'label'=>Yii::t('lang',Yii::app()->params['create-text']), 
        'url'=>array('create'), 
        'linkOptions' => array('class' => Yii::app()->params['create-btn'] . ' btn-sm'),
        'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_create')),
);

?>
<div class="row">
    <div class="col-xs-12">
<div class="panel panel-default">
    <div class="panel-heading navbar-tool">
        
        <h3 class="panel-title"><?php echo  Yii::t('lang',Yii::app()->params['index-text']) .' '. Yii::t('lang','Menus'); ?></h3>
        
       <div class="menu-tool">
            
            <div class="pagesize"> 
             <?php $dataProvider = $this->PageSize(Yii::app()->params['IDGridview'], $model->search()); ?>
             </div>         
            
            <div class="menu-items">
                 <?php 
        $this->widget('zii.widgets.CMenu', array(
            'items' => $this->menu,
            'encodeLabel' => FALSE,
            'htmlOptions' => array('class' => 'cmenuhorizontal'),
        ));
        ?>
            </div>
        </div>        
        <?php $this->ToolActionsRight(); ?>         
        
    </div>
    <div class="panel-body"> 

<div class="table-responsive">
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>Yii::app()->params['IDGridview'],
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'columns'=>array(
		 array('name'=>'id','value'=>'$data->id'),
		 array('name'=>'descripcion','value'=>'$data->descripcion'),
		 array('name'=>'url','value'=>'$data->url'),
		 array('name'=>'url_tipo','value'=>'$data->url_tipo'),
		 array('name'=>'categoria','value'=>'$data->categoria'),
		 array('name'=>'estatus','value'=>'$data->estatus'),
		/*
		 array('name'=>'orden','value'=>'$data->orden'),
		 array('name'=>'icono','value'=>'$data->icono'),
		 array('name'=>'modulo','value'=>'$data->modulo'),
		 array('name'=>'jerarquia','value'=>'$data->jerarquia'),
		 array('name'=>'registro_fecha','value'=>'$data->registro_fecha'),
		 array('name'=>'registro_hora','value'=>'$data->registro_hora'),
		 array('name'=>'modificado_fecha','value'=>'$data->modificado_fecha'),
		 array('name'=>'modificado_hora','value'=>'$data->modificado_hora'),
		 array('name'=>'secuencia','value'=>'$data->secuencia'),
		*/
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['view-text']),            
            'label' => Yii::app()->params['view-icon'],
            'linkHtmlOptions' => array('class' => 'view '.Yii::app()->params['view-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))', 
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_view')
        ),
        array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['update-text']),            
            'label' => Yii::app()->params['update-icon'],
            'linkHtmlOptions' => array('class' => 'edit '.Yii::app()->params['update-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
             'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_update')
        ),
          array(
            'class' => 'CLinkColumn',
            'header' => Yii::t('lang',Yii::app()->params['delete-text']),           
            'label' => Yii::app()->params['delete-icon'],
            'linkHtmlOptions' => array('class' => 'delete ' . Yii::app()->params['delete-btn']),
            'urlExpression' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
            'visible' => Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')
        ),
    ),
)); ?>
</div>
    </div>
    </div>
 </div>
     </div>
<?php 
 
    
    if (Yii::app()->authRBAC->checkAccess($this->modulo . '_delete')) {

    $this->ModalConfirmDelete(Yii::app()->params['IDGridview']);

    //$this->renderPartial('../../../../views/site/modal-delete', array());    

    } 
?>