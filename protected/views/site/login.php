<?php

$siteKey = "6LcJZAITAAAAADIvlYH6FiYmlNSMTHn_0nxAFe8g";
$lang = "es";
$datathemelight = "light";
$datathemedark = "dark";
$datatypeimage = "image";
?>
<!--<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>-->
<style type="text/css">
<!--
.Estilo1 {
	color: #000000;
	font-weight: bold;
}

.Estilo2 {
	font-size: 12px
}

.Estilo4 {
	font-size: 12px;
	color: #000;
}
-->
</style>
<body
	style="background: url(<?php echo Yii::app()->baseUrl ?>/img/panama.jpg) no-repeat; width: 100%; height: 100%; background-size: 100%;">
    <section class="container animated fadeInUp" >
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div id="login-wrapper" >
					<header>
						<div class="brand">
							<i> <img src="http://thefactoryhka.com/ve/images/favicon.ico"
								width="70" height="47" /><br />
							</i> <span class="Estilo1">EXTRANET PANAMÁ</span>
						</div>
					</header>
                                    <div class="panel panel-primary" style="" >
						<div class="panel-heading">
							<h3 class="panel-title">Acceso</h3>
						</div>
						<div class="panel-body">                   
                        
                            <?php
							$form = $this->beginWidget ( 'CActiveForm', array(
									'id' => 'form-loginaccess',
									'enableClientValidation' => false,
									'htmlOptions' => array(
											'class' => 'form-horizontal',
											'role' => 'form'),
									'clientOptions' => array(
											'validateOnSubmit' => true)) );
							?>
                            <div class="form-group">
								<div class="col-md-12">
                                    <?php echo $form->textField($model, 'username', array('class' => 'form-control input-lg', 'autocomplete' => 'off', 'placeholder' => 'Usuario')); ?>
                                    <i class="fa fa-user"></i>
                                    <?php echo $form->error($model, 'username'); ?>
                                </div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
                                    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control input-lg', 'autocomplete' => 'off', 'value' => '', 'placeholder' => 'Contraseña')); ?>
                                    <i class="fa fa-lock"></i>
                                    <?php echo $form->error($model, 'password'); ?>                                    
                                 
                                </div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<!--<div class="g-recaptcha" hl="<?php //echo $lang; ?>" data-theme="<?php //echo $datathemelight; ?>"  data-type="<?php //echo $datatypeimage; ?>" data-sitekey="<?php// echo $siteKey;?>"></div>-->
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">                                  
                                    <?php echo CHtml::submitButton('Iniciar Sesión', array('class' => 'btn btn-primary btn-block')); ?>                                     
                                     <hr />
								</div>
								<!-- <div class="col-md-12">                                  
									<?php //echo CHtml::Button('Registro Programadores', array('class' => 'btn btn-primary btn-block', 'data-toggle' => 'modal', 'data-target' => '#formRegistro')); ?>
									<hr />
								</div> -->
							</div>
                            <?php $this->endWidget(); ?> 
                    </div>

					</div>
		<?php
			foreach(Yii::app ()->user->getFlashes () as $key => $message)
			{
				
				echo '<div class="alert alert-' . $key . ' alert-dismissable">';
				echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
				echo $message . '</div>';
			}
			?>
                		<div align="center" class="Estilo2">
						<span class="Estilo4">© 2016 Thefactory HKA Todos los derechos
							reservados. <br/>Desarrollado por <img src="http://www.dascomla.com/sonaray/images/thfka.png" width="14"	height="11"><a href="http://www.thefactoryhka.com/pa">THE FACTORY HKA CORP C.A.</a>
						</span>
					</div>
				</div>
			</div>

		</div>

		</div>
	</section>

<?php

$data =  CHTML::listData(Products::model()->findAll(), 'id', 'model');
$formRegistro = $this->beginWidget ( 'CActiveForm', array(
		'id' => 'rbacusuarios-form',
		'enableClientValidation' => false,
		'clientOptions' => array(
				'validateOnSubmit' => false),
		'action' => '') );
?>
<!-- Modal Registro de Program-->
<div class="modal fade" id="formRegistro" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Registro de Programadores</h4>
            </div>
            <div class="alert alert-danger" role="alert" id='errorMessage' style="display: none"></div>
            <div class="alert alert-success" role="alert" id='successMessage'
                 style="display: none"></div>
            <div class="modal-body">
					<div class="form" id="login-form-style">
						<div class="container-fluid">
							<div class="col-md-6">
			                    <div class="row">
												<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'nombreusuario'); ?>
												<?php echo $formRegistro->textField($formRegistroUserExtranet,'nombreusuario', array('style' => 'width:100%; height: 30px', 'class' => 'registroUser', 'idName' => 'nombreusuario', 'value' => ''));?>
												<?php echo $formRegistro->error($formRegistroUserExtranet,'nombreusuario'); ?>
			                    </div>
			                    <div class="row">
												<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'nombre'); ?>
												<?php echo $formRegistro->textField($formRegistroUserExtranet,'nombre', array('style' => 'width:100%; height: 30px', 'class' => 'registroUser', 'idName' => 'nombre', 'value' => ''));?>
												<?php echo $formRegistro->error($formRegistroUserExtranet,'nombre'); ?>
			                    </div>
			                    <div class="row">
												<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'email'); ?>
												<?php echo $formRegistro->textField($formRegistroUserExtranet,'email', array('style' => 'width:100%; height: 30px', 'class' => 'registroUser', 'idName' => 'email', 'value' => ''));?>
												<?php echo $formRegistro->error($formRegistroUserExtranet,'email'); ?>
			                    </div>
			                    <div class="row">
												<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'dni_numero'); ?>
												<?php echo $formRegistro->textField($formRegistroUserExtranet,'dni_numero', array('style' => 'width:100%; height: 30px', 'class' => 'registroUser', 'idName' => 'dni_numero', 'value' => ''));?>
												<?php echo $formRegistro->error($formRegistroUserExtranet,'dni_numero'); ?>
			                    </div>
			                    <div class="row">
												<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'sistema_operativo'); ?>
												<?php echo $form->dropDownList($formRegistroUserExtranet,'sistema_operativo', CHTML::listData(OperatingSystem::model()->findAll(), 'id', 'name'), array('options' => array('0'=>array('selected'=>true)), 'class' => 'registroUser', 'idName' => 'sistema_operativo', 'prompt'=>'Seleccione SO'))?>
												<?php echo $formRegistro->error($formRegistroUserExtranet,'sistema_operativo'); ?>
			                    </div>
							</div>
							<div class="col-md-6">
                    <div class="row">
									<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'contrasena', array('style' => 'float:left; width: 50%')); ?>
									<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'confirmaPassword', array('style' => 'float:left')); ?>
									<?php echo $formRegistro->passwordField($formRegistroUserExtranet,'contrasena', array('style' => 'clear:both; float:left; width:49%; height: 30px', 'class' => 'registroUser', 'idName' => 'contrasena', 'value' => ''));?>
									<?php echo $formRegistro->passwordField($formRegistroUserExtranet,'confirmaPassword', array('style' => 'float:left; width:50%; height: 30px; margin-left: 1%', 'class' => 'registroUser', 'idName' => 'confirmaPassword', 'value' => '', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => 'Contraseñas no coinciden.'));?>
									<?php echo $formRegistro->error($formRegistroUserExtranet,'contrasena'); ?>
                    </div>
                    <div class="row">
									<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'apellido'); ?>
									<?php echo $formRegistro->textField($formRegistroUserExtranet,'apellido', array('style' => 'width:100%; height: 30px', 'class' => 'registroUser', 'idName' => 'apellido', 'value' => ''));?>
									<?php echo $formRegistro->error($formRegistroUserExtranet,'apellido'); ?>
                    </div>
								<div class="row">
									<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'sitio_web',array('style' => 'float:left; width: 50%')); ?>
									<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'softwarehouse',array('style' => 'float:left; width: 50%')); ?>
									<?php echo $formRegistro->textField($formRegistroUserExtranet,'sitio_web', array('style' => 'clear:both; float:left; width:49%; height: 30px', 'idName' => 'sitio_web', 'class' => 'registroUser', 'value' => ''));?>
									<?php echo $formRegistro->textField($formRegistroUserExtranet,'softwarehouse', array('style' => 'float:left; width:49%; height: 30px; margin-left: 1%', 'idName' => 'softwarehouse', 'class' => 'registroUser',));?>
									<?php echo $formRegistro->error($formRegistroUserExtranet,'sitio_web'); ?>
									<?php echo $formRegistro->error($formRegistroUserExtranet,'softwarehouse'); ?>
								</div>


								<div class="row">
									<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'lenguaje_programacion'); ?>
									<?php echo $form->dropDownList($formRegistroUserExtranet,'lenguaje_programacion', CHTML::listData(ProgrammingLanguages::model()->findAll(), 'id', 'name'), array('options' => array('0'=>array('selected'=>true)), 'class' => 'registroUser', 'idName' => 'lenguaje_programacion', 'prompt'=>'Seleccione Lenguaje'))?>
									<?php echo $formRegistro->error($formRegistroUserExtranet,'lenguaje_programacion'); ?>
								</div>
								<div class="row">
									<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'producto_integracion'); ?>
									<?php echo $form->dropDownList($formRegistroUserExtranet,'producto_integracion', CHTML::listData(Products::model()->findAll(), 'id', 'model'), array('options' => array('0'=>array('selected'=>true)), 'class' => 'registroUser', 'idName' => 'producto_integracion', 'prompt'=>'Seleccione Producto'))?>
									<?php echo $formRegistro->error($formRegistroUserExtranet,'producto_integracion'); ?>
								</div>
							</div>
							<div class="col-md-12">
								<div class="row">
									<?php echo $formRegistro->labelEx($formRegistroUserExtranet,'observaciones'); ?>
									<?php echo $formRegistro->textArea($formRegistroUserExtranet,'observaciones', array('class' => 'registroUser', 'idName' => 'observaciones', 'rows'=>5, 'cols'=>50));?>
									<?php echo $formRegistro->error($formRegistroUserExtranet,'observaciones'); ?>
								</div>
							</div>
						</div>
						<br/>
						<div class="container-fluid">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
									data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" id="submitRegistro">Registrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	</div>
<style>
    
    #main {
        margin: 0px auto;
    }

    .modal-content {
        box-shadow: none;
        border-radius: 0;
        border: solid 3px rgba(255, 255, 255, 0.6);
    }

    .row {
        margin-right: 0px;
        margin-left: 0px;
    }

@media (min-width: 768px){
.modal-dialog {
	width: 80% !important;
	margin: 30px auto;
}
}

.form-control {
    border: 2px solid #e8ebed;
    border-radius: 2px;
    box-shadow: none;
    height: 29px;
    padding: 0px 0px 0px 2px;
}

body{
	font-size: 80%;
}
    
</style>
<input type="hidden" value="" id="tipo" name="tipo">
<?php $this->endWidget();?>
<script type="text/javascript">
$('#submitRegistro').click(function(e){
	var form = new Object();
	$('.registroUser').each( function( index, element ) {
		form[$(this).attr('idname')] = $(this).val();
		});
	var formJSON = JSON.stringify(form);
	$.ajax({
		type: "POST",
		url : "<?php echo Yii::app()->getBaseUrl(true); ?>/roles/usuarios/create",
		cache : false,
		data: {siteRegistro: formJSON, type: $('#tipo').val(), YII_CSRF_TOKEN:'<?php echo Yii::app()->request->csrfToken ?>'},
		dataType: "json",
		success: function(response) {
			if(response.success == 'false')
			{
				$('#errorMessage').empty();
				$.each( response.message, function( i, val ) {
					//$( "#" + i ).append( document.createTextNode( " - " + val ) );
					$('#errorMessage').append("<div>" + val + "</div>" + "<br/>");
				});
				$('#errorMessage').fadeIn();
				setTimeout(function(){$('#errorMessage').fadeOut();}, 5000);
			}
			if(response.success == 'true')
			{
				$('#successMessage').text(response.message);
				$('#successMessage').fadeIn();
				setTimeout(function(){$('#successMessage').fadeOut();$('#formRegistro').modal('hide')}, 2000);
			}
		}
	})
});
$(document).ready(function() {
	//confirmPassword();
});
</script>
</body>