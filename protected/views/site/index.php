<style type="text/css">
.Estilo1 {
	color: #003366;
	font-weight: bold;
}

.Estilo2 {
	font-size: 12px
}

.centrar {
	text-align: center !important;
}

.panel-style {
	padding-top: 15px;
	padding-bottom: 15px;
}

.panel-style-home {
	padding-top: 8px;
	padding-bottom: 8px;
}
</style>
<span class="Estilo1">MODULOS DE LA EXTRANET</span>
<br>
<br>
<div class="row">
<?php if(Yii::app()->authRBAC->checkAccess($this->modulo . '_maquinas')):?>
    <div class="col-xs-12 col-sm-5 col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">MAQUINAS</h3>
			</div>
			<a href="<?php echo Yii::app()->baseUrl."/seriales/machines/index"; ?>">
				<div class=" panel-body btn btn-info btn-block btn-trans">
					<i class="fa fa-2x fa-print"></i>
				</div>
			</a>
		</div>
	</div>
<?php endif ?>
<?php if(Yii::app()->authRBAC->checkAccess($this->modulo . '_operacionesFiscales')):?>
    <div class="col-xs-12 col-sm-5 col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Operaciones Fiscales</h3>
			</div>
			<a href="<?php echo Yii::app()->baseUrl."/fiscalizacion/operations/create"; ?>">
				<div class=" panel-body btn btn-info btn-block btn-trans">
					<i class="fa fa-2x fa-sliders"></i>
				</div>
			</a>
		</div>
	</div>
<?php endif ?>
<?php if(Yii::app()->authRBAC->checkAccess($this->modulo . '_utilitarios')):?>
<div class="col-xs-12 col-sm-5 col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">DESCARGAS & UTILITARIOS</h3>
			</div>
			<a
				href="<?php echo Yii::app()->baseUrl."/filesmanage/Downloads/index/"; ?>">
				<div class=" panel-body btn btn-info btn-block btn-trans">
					<i class="fa fa-2x fa-download"></i>
				</div>
			</a>
		</div>
	</div>
<?php endif ?>
<?php if(Yii::app()->authRBAC->checkAccess($this->modulo . '_usuario')):?>
    <div class="col-xs-12 col-sm-5 col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">USUARIOS</h3>
			</div>
			<a href="<?php echo Yii::app()->baseUrl."/roles/usuarios"; ?>">
				<div class=" panel-body btn btn-info btn-block btn-trans">
					<i class="fa fa-2x fa-users"></i>
				</div>
			</a>
		</div>
	</div>
<?php endif ?>
 <?php if(Yii::app()->authRBAC->checkAccess($this->modulo . '_casasSoftware')):?>
    <div class="col-xs-12 col-sm-5 col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">CASA DE SOFTWARE</h3>
			</div>
			<a href="<?php echo Yii::app()->baseUrl."/softwarehouses/softwarehouses/"; ?>">
				<div class=" panel-body btn btn-info btn-block btn-trans">
					<i class="fa fa-2x fa-list-alt""></i>
				</div>
			</a>
		</div>
	</div>
<?php endif ?>
<?php if(Yii::app()->authRBAC->checkAccess($this->modulo . '_roles')):?>
	<div class="col-xs-12 col-sm-5 col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">ROLES</h3>
			</div>
			<a href="<?php echo Yii::app()->baseUrl."/roles/roles/"; ?>">
				<div class=" panel-body btn btn-info btn-block btn-trans">
					<i class="fa fa-2x fa-unlock"></i>
				</div>
			</a>
		</div>
	</div>
<?php endif ?>
<?php if(Yii::app()->authRBAC->checkAccess($this->modulo . '_reporteenajenaciones')):?>
	<div class="col-xs-12 col-sm-5 col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">REPORTE ENAJENACIONES</h3>
			</div>
			<a href="<?php echo Yii::app()->baseUrl."/fiscalizacion/FicalizedMachines"; ?>">
				<div class=" panel-body btn btn-info btn-block btn-trans">
					<i class="fa fa-2x fa-line-chart"></i>
				</div>
			</a>
		</div>
	</div>
<?php endif ?>
<?php if(Yii::app()->authRBAC->checkAccess($this->modulo . '_reportedistribuidores')):?>
	<div class="col-xs-12 col-sm-5 col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">REPORTE DISTRIBUIDORES</h3>
			</div>
			<a href="<?php echo Yii::app()->baseUrl."/reportes/Distribuidores"; ?>">
				<div class=" panel-body btn btn-info btn-block btn-trans">
					<i class="fa fa-2x fa-line-chart"></i>
				</div>
			</a>
		</div>
	</div>
<?php endif ?>
</div>
<div class="row">
         <?php //if(Yii::app()->authRBAC->checkAccess($this->modulo . '_perfil')):?>
<!--        <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">PERFIL</h3>                
            </div>
            <a href="<?php //echo Yii::app()->createUrl('roles/perfil/view/id/'.Yii::app()->user->id); ?>">
                <div  class=" panel-body btn btn-info btn-block btn-trans">
                    <i  class="fa fa-2x fa-cogs"></i>                                 
                </div>
            </a>
        </div>
        </div>-->
<?php //endif ?>

<!--    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">GENERAR CODIGO</h3>                
            </div>
            <a href="<?php //echo Yii::app()->baseUrl."/fiscalizacion/registerOperations/create/"; ?>">
                <div class=" panel-body btn btn-info btn-block btn-trans">
                    <i  class="fa fa-2x fa-barcode"></i>                                 
                </div>
            </a>
        </div>
    </div>
    -->

</div>

<div class="row">


	<!--     <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">EQUIPOS FISCALES</h3>                
            </div>
            <a href="<?php //echo Yii::app()->baseUrl."/fiscalizacion/machines/index/"; ?>">
                <div class=" panel-body btn btn-info btn-block btn-trans">
                    <i  class="fa fa-2x fa-print"></i>                                 
                </div>
            </a>
        </div>
    </div>-->


	<!--    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Cliente final</h3>                
            </div>
            <a href="<?php //echo Yii::app()->baseUrl."/fiscalizacion/socialReasons/index/"; ?>">
                <div class=" panel-body btn btn-info btn-block btn-trans">
                    <i  class="fa fa-2x fa-university"></i>                                 
                </div>
            </a>
        </div>
    </div>-->
    <?php // if(Yii::app()->authRBAC->checkAccess($this->modulo . '_logs')):?>   
<!--        <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">LOGS</h3>                
            </div>
            <a href="<?php //echo Yii::app()->baseUrl."/logs/logs/index/"; ?>">
                <div class=" panel-body btn btn-info btn-block btn-trans">
                    <i  class="fa fa-2x fa fa-eye"></i>                                 
                </div>
            </a>
        </div>
    </div>-->
    <?php //endif ?>
</div>







<!--<br>
<span class="Estilo1">GRAFICOS DE LOS MODULOS DE LA EXTRANET</span>
<br/>
<img src="http://thefactoryhka.com/extranet-pa/img/ico/graficos.png" />
<br/>
<span class="Estilo2">GRAFICA DE ARCHIVOS</span>-->
