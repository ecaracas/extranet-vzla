<section class="container animated fadeInUp">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div id="login-wrapper">
                <header>
                    <div class="brand">
                        <a href="index.html" class="logo">
                           <i class="fa fa-desktop"></i>
                        EXTRANET <span>PANAMA</span></a>
                    </div>
                </header>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">     
                            Recuperacion de Contraseña
                        </h3>
                    </div>
                    <div class="panel-body">                   
                        
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'form-loginaccess',
                                'enableClientValidation' => true,
                                'htmlOptions' => array(
                                    'class' => 'form-horizontal', 'role' => 'form',
                                ),
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                ),
                            ));
                            ?>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <?php echo $form->textField($model, 'nombreusuario', array('class' => 'form-control input-lg input-alfanumerico', 'autocomplete' => 'off', 'placeholder' => 'Usuario')); ?>
                                    <i class="fa fa-user"></i>
                                    <?php echo $form->error($model, 'nombreusuario'); ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-12">                                  
                                    <?php echo CHtml::submitButton('Recuperar', array('class' => 'btn btn-primary btn-block')); ?>                              
                                        <hr />
                                        <a href="<?php echo Yii::app()->createUrl('site/login'); ?>" class="btn btn-default btn-block">Volver</a>

                                </div>
                            </div>
                            <?php $this->endWidget(); ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>