<section class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div id="error-container" class="block-error animated fadeInUp">
				<header>
					<p class="text-center">Sea registrado Satifactoriamente</p>
				</header>
				<p class="text-center">Se va proceder a verificar sus datos para permitir el acceso.</p>
				<div class="row">
					<div class="col-md-12">
						<a class="btn btn-success btn-block" href="<?php echo Yii::app()->createUrl('site/login'); ?>">Ir al Inicio</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>