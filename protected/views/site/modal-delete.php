<div id="modal-confirm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4><?php echo Yii::app()->params['confirmacion-eliminar']; ?></h4>
      </div>
      <div class="modal-footer">
        <div class="div">  
            <div class="col-xs-12">
                <ul class="cmenuhorizontal">
                    <?php
                   
                    
                  
                    ?>
                    <?php echo CHtml::hiddenField('Text', '', array('id'=>$url, 'class'=>'_dialogsubmit')); ?>
                    <li class="bottom"><?php echo CHtml::link('Aceptar', $url, array('id' => 'dialogsubmit', 'class' => Yii::app()->params['delete-btn'])); ?></li>
                    <li class="bottom"><?php echo CHtml::link('Cancelar', '#', array('id' => 'dialogclose', 'class' => Yii::app()->params['cancel-btn'])); ?></li>
                </ul>
            </div>
        </div>
    </div>

   <div id="modal-content-loading" class="modal-content-loading hidden-loading">
        <?php echo CHtml::image(Yii::app()->baseUrl.'/img/cargando.gif','Loading'); ?>
    </div>
    </div>
    </div>
</div>