<?php
/* @var $this FilesDocumentController */
/* @var $data filesDocument */
?>



    	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('active')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->active); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->name); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('path')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->path); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('version')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->version); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('language')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->language); ?></div>
	
</div>
	<div class='row'>
<div class='col-xs-1'><?php echo CHtml::encode($data->getAttributeLabel('pathTypeFile')); ?></div>
	<div class='col-xs-11'><?php echo CHtml::encode($data->pathTypeFile); ?></div>
	
</div>

<hr />