<?php

/**
 * This is the model class for table "rbac_usuarios".
 *
 * The followings are the available columns in table 'rbac_usuarios':
 * 
 * @property integer $id
 * @property integer $rol_id
 * @property string $nombreusuario
 * @property string $contrasena
 * @property string $email
 * @property string $softwareHouse
 * @property string $nombre
 * @property string $apellido
 * @property integer $estatus
 * @property string $dni_numero
 * @property integer $telf_local_tipo
 * @property string $telf_local_numero
 * @property integer $principal
 * @property integer $cambio_clave
 * @property string $registro_fecha
 * @property string $registro_hora
 * @property integer $registro_usuario_id
 * @property string $modificado_fecha
 * @property string $modificado_hora
 * @property integer $modificado_usuario_id
 * @property integer $secuencia The followings are the available model relations:
 * @property RbacRoles $rol
 * @property RbacRolesOpcionesUsuarios[] $rbacRolesOpcionesUsuarioses
 */
class FormRBACUsuarios extends CFormModel
{
	public $rol_id;
	public $nombreusuario;
	public $contrasena;
	public $email;
	public $nombre;
	public $apellido;
	public $dni_numero;
	public $registro_fecha;
	public $registro_usuario_id;
	public $modificado_fecha;
	public $modificado_usuario_id;
	public $secuencia;
	public $sitio_web;
	public $softwarehouse;
	public $lenguaje_programacion;
	public $sistema_operativo;
	public $producto_integracion;
	public $observaciones;
	public $confirmaPassword;
	
	
	public function rules()
	{
		return array(
				array('rol_id, nombreusuario, contrasena, email, nombre, apellido, dni_numero, registro_fecha, registro_usuario_id, modificado_fecha, modificado_usuario_id, sitio_web, lenguaje_programacion, sistema_operativo, producto_integracion, softwarehouse', 'required'));
	}

	public function attributeLabels()
	{
		return array(
				'id' => 'ID',
				'rol_id' => 'Tipo de Usuario',
				'nombreusuario' => 'Usuario',
				'contrasena' => 'Contraseña',
				'email' => 'Email',
				'nombre' => 'Nombre',
				'apellido' => 'Apellido',
				'estatus' => 'Estatus',
				'dni_numero' => 'RUC',
				'telf_local_tipo' => 'Cod Tlf',
				'telf_local_numero' => 'Numero Tlf',
				'principal' => 'Principal',
				'cambio_clave' => 'Cambio Clave',
				'registro_fecha' => 'Registro Fecha',
				'registro_hora' => 'Registro Hora',
				'registro_usuario_id' => 'Registro Usuario',
				'modificado_fecha' => 'Modificado Fecha',
				'modificado_hora' => 'Modificado Hora',
				'modificado_usuario_id' => 'Modificado Usuario',
				'secuencia' => 'Secuencia',
				'sitio_web' => 'Sitio Web',
				'lenguaje_programacion' => 'Lenguaje de Programación',
				'sistema_operativo' => 'Sistema Operativo',
				'producto_integracion' => 'Producto a Integrar',
				'observaciones' => 'Observaciones',
				'softwarehouse' => 'Casa de software',
				'confirmaPassword' => 'Confirmar Contraseña'
		);
	}
}
