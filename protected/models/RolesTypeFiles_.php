<?php

/**
 * This is the model class for table "roles_type_files".
 *
 * The followings are the available columns in table 'roles_type_files':
 * @property integer $id
 * @property integer $id_rol
 * @property integer $id_filetype
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property FilesTypes $idFiletype
 * @property RbacRoles $idRol
 */
class RolesTypeFiles extends CActiveRecord
{
	
	public function tableName()
	{
		return 'roles_type_files';
	}

	
	public function rules()
	{
		
		return array(
			array('active', 'required'),
			array('id_rol, id_filetype, active', 'numerical', 'integerOnly'=>true),
		
			array('id, id_rol, id_filetype, active', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
			'idFiletype' => array(self::BELONGS_TO, 'FilesTypes', 'id_filetype'),
			'idRol' => array(self::BELONGS_TO, 'RbacRoles', 'id_rol'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('lang','ID'),
			'id_rol' => Yii::t('lang','Id Rol'),
			'id_filetype' => Yii::t('lang','Id Filetype'),
			'active' => Yii::t('lang','Active'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_rol',$this->id_rol);
		$criteria->compare('id_filetype',$this->id_filetype);
		$criteria->compare('active',$this->active);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
