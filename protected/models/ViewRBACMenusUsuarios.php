<?php

/**
 * This is the model class for table "view_rbac_menus_usuarios".
 *
 * The followings are the available columns in table 'view_rbac_menus_usuarios':
 * @property integer $menu_id
 * @property string $menu_descripcion
 * @property string $menu_url
 * @property integer $menu_url_tipo
 * @property string $menu_categoria
 * @property integer $menu_orden
 * @property string $menu_icono
 * @property string $menu_modulo
 * @property integer $menu_jerarquia
 * @property integer $opcion_id
 * @property string $opcion_descripcion
 * @property string $opcion_opcion
 * @property string $opcion_url
 * @property integer $opcion_url_tipo
 * @property string $opcion_categoria
 * @property integer $opcion_orden
 * @property string $opcion_icono
 * @property integer $opcion_jerarquia
 * @property integer $rol_id
 * @property string $rol_descripcion
 * @property integer $rol_principal
 * @property integer $usuario_id
 * @property string $usuario_nombreusuario
 * @property string $usuario_email
 * @property integer $rol_opcion_id
 * @property integer $rol_opcion_usuario_id
 */
class ViewRBACMenusUsuarios extends CActiveRecord
{
	
	public function tableName()
	{
		return 'view_rbac_menus_usuarios';
	}

	
	public function rules()
	{
		
		return array(
			array('menu_id, menu_url_tipo, menu_orden, menu_jerarquia, opcion_id, opcion_url_tipo, opcion_orden, opcion_jerarquia, rol_id, rol_principal, usuario_id, rol_opcion_id, rol_opcion_usuario_id', 'numerical', 'integerOnly'=>true),
			array('menu_descripcion, opcion_descripcion, opcion_opcion, rol_descripcion, usuario_nombreusuario, usuario_email', 'length', 'max'=>100),
			array('menu_url, opcion_url', 'length', 'max'=>255),
			array('menu_categoria, opcion_categoria', 'length', 'max'=>20),
			array('menu_icono, opcion_icono', 'length', 'max'=>50),
			array('menu_modulo', 'length', 'max'=>30),
		
			array('menu_id, menu_descripcion, menu_url, menu_url_tipo, menu_categoria, menu_orden, menu_icono, menu_modulo, menu_jerarquia, opcion_id, opcion_descripcion, opcion_opcion, opcion_url, opcion_url_tipo, opcion_categoria, opcion_orden, opcion_icono, opcion_jerarquia, rol_id, rol_descripcion, rol_principal, usuario_id, usuario_nombreusuario, usuario_email, rol_opcion_id, rol_opcion_usuario_id', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		
		return array(
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'menu_id' => 'Menu',
			'menu_descripcion' => 'Menu Descripcion',
			'menu_url' => 'Menu Url',
			'menu_url_tipo' => 'Menu Url Tipo',
			'menu_categoria' => 'Menu Categoria',
			'menu_orden' => 'Menu Orden',
			'menu_icono' => 'Menu Icono',
			'menu_modulo' => 'Menu Modulo',
			'menu_jerarquia' => 'Menu Jerarquia',
			'opcion_id' => 'Opcion',
			'opcion_descripcion' => 'Opcion Descripcion',
			'opcion_opcion' => 'Opcion Opcion',
			'opcion_url' => 'Opcion Url',
			'opcion_url_tipo' => 'Opcion Url Tipo',
			'opcion_categoria' => 'Opcion Categoria',
			'opcion_orden' => 'Opcion Orden',
			'opcion_icono' => 'Opcion Icono',
			'opcion_jerarquia' => 'Opcion Jerarquia',
			'rol_id' => 'Rol',
			'rol_descripcion' => 'Rol Descripcion',
			'rol_principal' => 'Rol Principal',
			'usuario_id' => 'Usuario',
			'usuario_nombreusuario' => 'Usuario Nombreusuario',
			'usuario_email' => 'Usuario Email',
			'rol_opcion_id' => 'Rol Opcion',
			'rol_opcion_usuario_id' => 'Rol Opcion Usuario',
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;

		$criteria->compare('menu_id',$this->menu_id);
		$criteria->compare('menu_descripcion',$this->menu_descripcion,true);
		$criteria->compare('menu_url',$this->menu_url,true);
		$criteria->compare('menu_url_tipo',$this->menu_url_tipo);
		$criteria->compare('menu_categoria',$this->menu_categoria,true);
		$criteria->compare('menu_orden',$this->menu_orden);
		$criteria->compare('menu_icono',$this->menu_icono,true);
		$criteria->compare('menu_modulo',$this->menu_modulo,true);
		$criteria->compare('menu_jerarquia',$this->menu_jerarquia);
		$criteria->compare('opcion_id',$this->opcion_id);
		$criteria->compare('opcion_descripcion',$this->opcion_descripcion,true);
		$criteria->compare('opcion_opcion',$this->opcion_opcion,true);
		$criteria->compare('opcion_url',$this->opcion_url,true);
		$criteria->compare('opcion_url_tipo',$this->opcion_url_tipo);
		$criteria->compare('opcion_categoria',$this->opcion_categoria,true);
		$criteria->compare('opcion_orden',$this->opcion_orden);
		$criteria->compare('opcion_icono',$this->opcion_icono,true);
		$criteria->compare('opcion_jerarquia',$this->opcion_jerarquia);
		$criteria->compare('rol_id',$this->rol_id);
		$criteria->compare('rol_descripcion',$this->rol_descripcion,true);
		$criteria->compare('rol_principal',$this->rol_principal);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('usuario_nombreusuario',$this->usuario_nombreusuario,true);
		$criteria->compare('usuario_email',$this->usuario_email,true);
		$criteria->compare('rol_opcion_id',$this->rol_opcion_id);
		$criteria->compare('rol_opcion_usuario_id',$this->rol_opcion_usuario_id);

		/* Condicion para no Mostrar los Eliminados en Gridview */
		$criteria->addcondition("estatus <> 9");

                if(Yii::app()->params['GridViewOrder'] == FALSE){
		$criteria->order = "id DESC";		
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
