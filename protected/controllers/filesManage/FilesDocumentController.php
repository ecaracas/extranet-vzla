<?php
class FilesDocumentController extends Controller
{
	/**
	 *
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 *      using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';
	
	/**
	 *
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete' ); // we only allow deletion via POST request
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * 
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array(
						'allow', /* Acciones Permitidas*/
				'actions' => array(
								'index',
								'view',
								'create',
								'update',
								'delete' ),
						'users' => array(
								'*' ) ),
				array(
						'deny',
						'users' => array(
								'*' ) ) );
	}
	
	public $modulo = "";
	
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		
		Yii::app() -> params['title'] = '';
	}
	
	public function actionView($id)
	{
		if(Yii::app() -> authRBAC->checkAccess($this -> modulo . '_view'))
		{
			
			$model = $this->loadModel($id);
			
			$this->render('view', array(
					'model' => $model ));
		}
		else
		{
			throw new CHttpException(403, Yii::app() -> params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionCreate()
	{
		if(Yii::app() -> authRBAC->checkAccess($this -> modulo . '_create'))
		{
			
			$model = new filesDocument();
			
			if(isset($_POST['filesDocument']))
			{
				$model -> attributes = $_POST['filesDocument'];
				if($model->validate())
				{
					if($model->save())
					{
						
						Yii::app() -> user->setFlash('success', Yii::app() -> params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model -> id ));
					}
					else
					{
						Yii::app() -> user->setFlash('danger', Yii::app() -> params['msjdanger']);
					}
				}
				else
				{
					Yii::app() -> user->setFlash('danger', Yii::app() -> params['msjdanger']);
				}
			}
			
			$this->render('create', array(
					'model' => $model ));
		}
		else
		{
			throw new CHttpException(403, Yii::app() -> params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionUpdate($id)
	{
		if(Yii::app() -> authRBAC->checkAccess($this -> modulo . '_update'))
		{
			
			$model = $this->loadModel($id);
			
			if(isset($_POST['filesDocument']))
			{
				$model -> attributes = $_POST['filesDocument'];
				
				if($model->validate())
				{
					if($model->save())
					{
						Yii::app() -> user->setFlash('success', Yii::app() -> params['msjsuccess']);
						$this->redirect(array(
								'view',
								'id' => $model -> id ));
					}
					else
					{
						Yii::app() -> user->setFlash('danger', Yii::app() -> params['msjdanger']);
					}
				}
				else
				{
					Yii::app() -> user->setFlash('danger', Yii::app() -> params['msjdanger']);
				}
			}
			
			$this->render('update', array(
					'model' => $model ));
		}
		else
		{
			throw new CHttpException(403, Yii::app() -> params['ErrorAccesoDenegado']);
		}
	}
	
	public function actionIndex()
	{
		if(Yii::app() -> authRBAC->checkAccess($this -> modulo . '_index'))
		{
			
			$model = new filesDocument('search');
			
			$model->unsetAttributes(); // clear any default values
			
			if(isset($_GET['filesDocument']))
			{
				$model -> attributes = $_GET['filesDocument'];
			}
			
			if(isset($_GET['filesDocument_sort']))
			{
				
				Yii::app() -> params['GridViewOrder'] = TRUE;
			}
			
			$this->render('index', array(
					'model' => $model ));
		}
		else
		{
			throw new CHttpException(403, Yii::app() -> params['ErrorAccesoDenegado']);
		}
	}
	
	public function loadModel($id)
	{
		$sql = new CDbCriteria();
		$sql -> params = array(
				':id' => intval($id) );
		$sql -> condition = "id = :id";
		$sql->addcondition("estatus <> 9");
		$model = filesDocument::model()->find($sql);
		if($model === null)
		{throw new CHttpException(404, Yii::app() -> params['Error404']);}
		return $model;
	}
}
