﻿<?php
Yii::import('application.extensions.recaptcha.*');
Yii::import('application.modules.roles.models.*');
require ('recaptchalib.php');

class SiteController extends Controller {

    public $modulo = "home";

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
        // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction'),

            'upload' => array(
                'class' => 'xupload.actions.XUploadAction',
                'path' => Yii::app()->getBasePath() . "/../uploads",
                'publicPath' => Yii::app()->getBaseUrl() . "/uploads",
                ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->validaSessionUser();
        //$this->render('index', array());
        Yii::import("xupload.models.XUploadForm");
        $model = new XUploadForm;
        $this -> render('index', array('model' => $model ));
    }

    // public function validarInformacionContribuyente()
    // {
    //
	// $id = Yii::app()->params['contribuyente_id'];
    // // $id = 50;
    // $representante = RepresentanteLegal::model()->find(array(
    // 'condition' => 'contribuyente_id = :contribuyente',
    // 'params' => array(
    // ':contribuyente' => $id ) ));
    // $domicilio = DomicilioFiscales::model()->find(array(
    // 'condition' => 'contribuyente_id = :contribuyente',
    // 'params' => array(
    // ':contribuyente' => $id ) ));
    //
	// $sql = new CDbCriteria();
    // $sql->params = array(
    // ':id' => intval($id) );
    // $sql->condition = "id = :id";
    // $sql->addcondition("estatus <> 9");
    // $contribuyente = Contribuyente::model()->find($sql);
    // if($representante != NULL and $domicilio != NULL or $contribuyente->rif_documentos != NULL or $contribuyente->rupdae_documento != NULL)
    // {
    // return false;
    // }
    // else
    // {
    // return true;
    // }
    //
	// }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm();
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" . "Reply-To: {$model->email}\r\n" . "MIME-Version: 1.0\r\n" . "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array(
            'model' => $model));
    }

    /**
     * METODO PARA EL LOGIN DEL USUARIO
     */
    public function actionLogin() {
        $formRegistroUserExtranet = new FormRBACUsuarios();
        $secret = "6LcJZAITAAAAAHrfBY5Usqx3oHsFG-kOaX8MctA2";
        $reCaptcha = new ReCaptcha($secret);
        Yii::app()->theme = 'externo';
        if (Yii::app()->user->id != NULL) {

            $this->redirect(Yii::app()->createUrl('site/index'));
        }
        $model = new LoginAccess();
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'form-loginaccess') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['LoginAccess'])) {
            if (true) { // $_POST["g-recaptcha-response"])
                // $resp = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);
                if (true) { // $resp != null && $resp->success)
                    $model->username = trim(strtolower(@$_POST['LoginAccess']['username']));
                    $model->username = $model->username;
                    $model->password = trim(@$_POST['LoginAccess']['password']);
                    $model->password = ValidateSafePassword::Hash($model->password);
                    if ($model->validate() && $model->login()) {
                        $this->redirect(Yii::app()->baseUrl);
                    }
                }
            }
        }
        $this->render('login', array('model' => $model, 'formRegistroUserExtranet' => $formRegistroUserExtranet));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect('login');
    }

    public function actionRecoverypass() {
        Yii::app()->theme = 'externo';

        $model = new RecoveryPass();

        if (isset($_POST['RecoveryPass'])) {
            $model->nombreusuario = Sanitization::StringAZ09(@$_POST['RecoveryPass']['nombreusuario']);
            $usuario = RBACUsuarios::model()->find(array(
                'condition' => 'nombreusuario = :id',
                'params' => array(
                    ':id' => $model->nombreusuario)));
            if ($usuario != NULL) {
                Yii::import('application.extensions.phpmailer.JPhpMailer');
                $mail = new JPhpMailer();
                $mail->IsSMTP();
                $mail->Mailer = "smtp";
                $mail->Host = "ssl://mail.thefactory.com.ve";
                $mail->Port = 465;
                $mail->SMTPAuth = true;
                $mail->Username = 'contacto@thefactory.com.ve';
                $mail->Password = 'Contacto@Tfhka';
                $mail->SetFrom('contactanos@thefactory.com.ve', 'Extranet Panama');
                $mail->Subject = 'Recuperacion de Contraseña';
                $contentido = '<table class="body-wrap">
    <tbody><tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td class="content-wrap">
                            <table cellpadding="0" cellspacing="0">
                                <tbody><tr>
                                    <td>
                                        <img class="img-responsive" src="img/header.jpg">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <h3>Welcom in basic email template</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        We may need to send you critical information about our service and it is important that we have an accurate email address.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block aligncenter">
                                        <a href="#" class="btn-primary">Confirm email address</a>
                                    </td>
                                </tr>
                              </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                <div class="footer">
                    <table width="100%">
                        <tbody><tr>
                            <td class="aligncenter content-block">Follow <a href="#">@Company</a> on Twitter.</td>
                        </tr>
                    </tbody></table>
                </div></div>
        </td>
        <td></td>
    </tr>
</tbody></table>';
                $mail->MsgHTML($contentido);
                $mail->AltBody = $contentido;
                $mail->AddAddress($usuario->email, ucfirst($usuario->nombre) . ' ' . ucfirst($usuario->apellido));
                if ($mail->Send()) {
                    $usuario->cambio_clave = 1;
                    Yii::app()->user->setFlash('success', 'Se ha enviado un correo electronico con los pasos de recuperacion');
                    $this->redirect('login');
                } else {
                    Yii::app()->user->setFlash('danger', 'No se ha logrado enviar el email. Contacto con Soporte');
                }
            } else {
                $model->nombreusuario = NULL;
                Yii::app()->user->setFlash('danger', 'No se ha encontrado el usuario. por favor verifique');
            }
        }
        $this->render('recoverypass', array(
            'model' => $model));
    }

    public function actionRegistros() {

        Yii::app()->theme = 'externo';
        $this->render('registros');
    }

}
