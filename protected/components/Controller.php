<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	
	/**
	 *
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 *      meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/column1';
	
	/**
	 *
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	
	/**
	 *
	 * @var array the breadcrumbs of the current page. The value of this property will
	 *      be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 *      for more details on how to specify this property.
	 */
	public $breadcrumbs = array();
	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
	}
	public function PageSize($id = NULL, $model = NULL)
	{
		$this->widget('application.extensions.PageSize.PageSize', array(
				'gridViewId' => $id,
				'pageSize' => Yii::app()->request->getParam('pageSize', null),
				'defaultPageSize' => Yii::app()->params['cantregistros_defecto_gridview'],
				'pageSizeOptions' => Yii::app()->params['registros_pagina_gridview']));
		$dataProvider = $model;
		$pageSize = Yii::app()->user->getState('pageSize', Yii::app()->params['cantregistros_defecto_gridview']);
		$dataProvider->getPagination()
			->setPageSize($pageSize);
		return $dataProvider;
	}
	public function ModalConfirmDelete($idgrid = NULL)
	{
		if($idgrid != NULL)
		{
			
                    Yii::app()->clientScript->registerScript('DeleteGridview', "
                    /* Click a boton Delete en el GridView */
                    jQuery(document).on('click', 'a.delete', function() {
                    
                        $('#modal-confirm').modal('show');
                        var url = jQuery(this).attr('href');
                        jQuery('#dialogsubmit').attr('href', url);
                        return false;
                    });
                    /* Boton de Close de Dialog */
                    jQuery(document).on('click', '#dialogclose', function() {
                        $('#modal-confirm').modal('hide');
                        return false;
                    });

                    /* Click de Confirmacion de Boton Delete */
                    jQuery(document).on('click', '#dialogsubmit', function() {
                            var url = jQuery(this).attr('href');
                            var ruta = $('._' + $(this).attr('id')).attr('id');
                            jQuery('#dialogsubmit').attr('href', ruta);
                            jQuery('#{$idgrid}').yiiGridView('update', {
                                type: 'POST',
                                url: url,
                                beforeSend: function(){
                                $('#modal-content-loading').removeClass('hidden-loading');
                                },
                                complete: function(){
                                $('#modal-content-loading').addClass('hidden-loading');
                                },
                                error: function(){
                                  $('#modal-content-loading').addClass('hidden-loading');  
                                },
                                success: function(data) {
                                jQuery('#{$idgrid}').yiiGridView('update');
                                $('#modal-confirm').modal('hide');
                                $('#modal-content-loading').addClass('hidden-loading');

                            }
                        });
                        return false;
                    });

                    ");
		}
	}
	public function ModalConfirmEmail($idgrid = NULL)
	{
		if($idgrid != NULL)
		{
			
			Yii::app()->clientScript->registerScript('DeleteGridview', "
/* Click a boton Envio en el GridView */
jQuery(document).on('click', 'a.email', function() {
    $('#modal-confirm-email').modal('show');
    var url = jQuery(this).attr('href');
    
    jQuery('#modal-body-email').html('');
            jQuery.ajax({
                'type': 'POST',
                'url': url,
                'cache': false,
                'data': jQuery(this).parents('form').serialize(),
                'success': function (html) {
                    jQuery('#modal-body-email').html(html);
                }
            });

    
    jQuery('#dialogsubmit-email').attr('href', url);
    return false;
});
/* Boton de Close de Dialog */
jQuery(document).on('click', '#dialogclose-email', function() {
    $('#modal-confirm-email').modal('hide');
    return false;
});

/* Click de Confirmacion de Boton Delete */
jQuery(document).on('click', '#dialogsubmit-email', function() {
        
        var url = jQuery(this).attr('href');
        
        jQuery.ajax({
            type: 'POST',
            url: url,
            'cache': false, 
            'data': jQuery(this).parents('form').serialize(), 
            beforeSend: function(){
            $('#modal-content-loading-email').removeClass('hidden-loading');            
            },
            complete: function(){
            $('#modal-content-loading-email').addClass('hidden-loading');
            },
            error: function(){
              $('#modal-content-loading-email').addClass('hidden-loading');  
            },
            success: function(data) {
            //jQuery('#{$idgrid}').yiiGridView('update');
            $('#modal-body-email').html(data);
            $('#modal-content-loading-email').addClass('hidden-loading');            
            }
    });
    return false;
});

");
		}
	}
	
	// public function AESdecrypt($inputText, $inputKey) {
	// $blockSize = 128;
	// $aes = new AES($inputText, $inputKey, $blockSize);
	// $dec = $aes->decrypt();
	// return $dec;
	// }
	//
	// public function AESencrypt($inputText, $inputKey) {
	// $blockSize = 128;
	// $aes = new AES($inputText, $inputKey, $blockSize);
	// $enc = $aes->encrypt();
	//
	// return $enc;
	// }
	//
	// public function ProcesamientoResultado($data) {
	// $resultado = explode('|', $data);
	//
	// if (count($resultado) > 0) {
	// return $resultado;
	// } else {
	// return FALSE;
	// }
	// }
	//
	// public function ChecksumData($data) {
	// if ($data != NULL) {
	//
	// $datos = $this->AESdecrypt($data, Yii::app()->params['KeyPrivate']);
	//
	// $datos = trim($datos);
	//
	// $datos = explode('|.|', $datos);
	//
	// if (count($datos) == 2) {
	//
	// $checksum = @$datos[0];
	//
	// $checksum_string = strtoupper(hash('SHA1', @$datos[1]));
	//
	// if ($checksum === $checksum_string) {
	//
	// $result = explode('||', @$datos[1]);
	//
	// return $result;
	// } else {
	// return FALSE;
	// }
	// } else {
	// return FALSE;
	// }
	// } else {
	//
	// return FALSE;
	// }
	// }
	//
	// public function DataProcess($data) {
	// $checksum = strtoupper(hash('sha1', $data));
	//
	// $dataFull = $checksum . '|.|' . $data;
	//
	// $dataAES = $this->AESencrypt($dataFull, Yii::app()->params['KeyPrivate']);
	//
	// return $dataAES;
	// }
	//
	// public function WebServiceServer($data = NULL, $ws = NULL, $function = NULL) {
	// $dataCifrada = $this->DataProcess($data);
	//
	// $client = new SoapClient(Yii::app()->params['urlWS'] . $function . 'WS/ws');
	//
	// $resultadoDatos = $client->$ws(Yii::app()->params['user'], Yii::app()->params['pass'], $dataCifrada, 1);
	//
	// $datosA = $this->ChecksumData($resultadoDatos);
	//
	// $total = (count($datosA) - 1);
	// $i = 0;
	// @$datos = array();
	// while ($i <= $total) {
	// $a = explode('|', $datosA[$i]);
	// @$datos[$a[0]] = $a[1];
	// $i++;
	// }
	//
	// return $datos;
	// }
	public function ToolActionsRight()
	{
		if(Yii::app()->params['panel-actions'])
		{
			echo '<div class="actions pull-right">
                    <i class="fa fa-expand"></i>
                    <i class="fa fa-chevron-down"></i>
                    <!--<i class="fa fa-times"></i>-->
                </div>';
		}
	}
	public function getFecha()
	{
		return Date('Y-m-d');
	}
	public function getHora()
	{
		return Date('H:i:s');
	}
	public function ControlMenu($categoria = NUll)
	{
		$sql = new CDbCriteria();
		
		$sql->select = "menu_id, menu_descripcion, menu_url, menu_url_tipo, menu_icono, menu_modulo, menu_jerarquia";
		
		$sql->condition = "usuario_id = :usuario_id AND menu_categoria = :menu_categoria";
		
		$sql->params = array(
				':usuario_id' => intval(Yii::app()->user->id),
				':menu_categoria' => $categoria);
		
		$sql->order = "menu_jerarquia ASC, menu_orden ASC";
		
		$sql->group = "menu_id, menu_descripcion, menu_url, menu_url_tipo, menu_orden, menu_icono, menu_modulo, menu_jerarquia";
		
		$model = ViewRBACMenusUsuarios::model()->findAll($sql);
		
		$menus = array();
		
		foreach($model as $m)
		{
			/* Menu */
			if($m->menu_jerarquia == 0)
			{
				$menus[$m->menu_id]['data'] = NULL;
				$menus[$m->menu_id]['menu_id'] = $m->menu_id;
				$menus[$m->menu_id]['menu_descripcion'] = $m->menu_descripcion;
				$menus[$m->menu_id]['menu_url'] = $m->menu_url;
				$menus[$m->menu_id]['menu_url_tipo'] = $m->menu_url_tipo;
				$menus[$m->menu_id]['menu_icono'] = $m->menu_icono;
				$menus[$m->menu_id]['menu_modulo'] = $m->menu_modulo;
				$menus[$m->menu_id]['menu_jerarquia'] = $m->menu_jerarquia;
			}
		}
		
		$cont = 1;
		foreach($model as $m)
		{
			if($m->menu_jerarquia > 0)
			{
				$menus[$m->menu_jerarquia]['data'][$m->menu_id]['menu_descripcion'] = $m->menu_descripcion;
				$menus[$m->menu_jerarquia]['data'][$m->menu_id]['menu_url'] = $m->menu_url;
				$menus[$m->menu_jerarquia]['data'][$m->menu_id]['menu_url_tipo'] = $m->menu_url_tipo;
				$menus[$m->menu_jerarquia]['data'][$m->menu_id]['menu_icono'] = $m->menu_icono;
				$menus[$m->menu_jerarquia]['data'][$m->menu_id]['menu_modulo'] = $m->menu_modulo;
				$menus[$m->menu_jerarquia]['data'][$m->menu_id]['menu_jerarquia'] = $m->menu_jerarquia;
				$cont++;
			}
		}
		
		return $menus;
	}
	public function validaSessionUser()
	{
		if(Yii::app()->user->id)
		{
			
			if(isset(Yii::app()->user->id))
			{
				
				/* Modulo de Accesos */
				
				/* Importo Modelos */
				Yii::import('application.modules.roles.models.RBACRoles');
				Yii::import('application.modules.roles.models.RBACUsuarios');
				
				/* Obtengo Datos del Usuario */
				
				$dataUser = RBACUsuarios::model()->find(array(
						'condition' => 't.estatus = 1 AND rol.estatus = 1 AND t.id = :user',
						'with' => array(
								'rol'),
						'params' => array(
								':user' => intval(Yii::app()->user->id))));
				if($dataUser != NULL)
				{
					
					Yii::app()->params['rol_id'] = $dataUser->rol_id;
					
					Yii::app()->params['nombre'] = ucfirst($dataUser->nombre) . ' ' . ucfirst($dataUser->apellido);
					Yii::app()->params['perfil'] = ucfirst(@$dataUser->rol->descripcion);
				}
				else
				{
					$this->redirect(Yii::app()->createUrl('site/login'));
				}
			}
			else
			{
				
				$this->redirect(Yii::app()->createUrl('site/login'));
			}
		}
		else
		{
			
			$this->redirect(Yii::app()->createUrl('site/login'));
		}
	}
	
	public function nombrerol()
	{
		$rol = Yii::app()->user->getState('rol');
		
		switch($rol)
		:
			
			case "1":
				$rola = "ROOT";
				break;
			case "2":
				$rola = "Super Administrador";
				break;
			case "3":
				$rola = "Distribuidor";
				break;
			case "4":
				$rola = "Soporte";
				break;
			case "5":
				$rola = "Distribuidor/Programador";
				break;
			case "6":
				$rola = "Programador";
				break;
			case "7":
				$rola = "Prueba";
				break;
			case "8":
				$rola = "Admin soporte";
				break;
                        case "9":
				$rola = "Servicio Técnico";
				break;
		endswitch
		;
		
		return @$rola;
	}
	
	/**
	 * Método para el envío de email desde cualquier instancia de la aplicación.
	 * @emails variable para indicar el email destino, puede ser un array de emails si son varios los destinos.
	 * @body cuerpo del mensaje del email.
	 * @fromName titulo del remitente.
	 * @subject titulo del email.
	 * @html por defecto es true, y sirve para indicar el formato del email si es false seria en texto.
	 */
	public function envioEmail($emails, $body, $fromName, $subject, $html = true)
	{
		if(Yii::app()->params['envioDeCorreo'])
		{
			$mailer = Yii::createComponent('application.extensions.mailer.EMailer');
			if(is_array($emails))
			{
				foreach ($emails as $value)
				{
					$mailer->AddAddress($value);
				}
			}
			else
			{
				$mailer->AddAddress($emails);
			}
			$mailer->From = 'contactanos@thefactory.com.ve';
			$mailer->CharSet = 'UTF-8';
			$mailer->FromName = $fromName;
			$mailer->WordWrap = 50;
			$mailer->IsHTML($html);
			$mailer->Subject = $subject;
			$mailer->Body = '
					<style>
						.comentario{width: 100%;word-wrap: break-word;} .container{padding-right:10px;padding-left:10px;margin-right:auto;margin-left:auto}#header,body{margin:0;padding:0}@media (min-width:768px){.container{width:100%}}@media (min-width:992px){.container{width:100%}}@media (min-width:1200px){.container{width:1000px}}body{color:#555;font:400 10pt Arial,Helvetica,sans-serif;background:#F5F5F5}#header{padding-top: 15px;border-top:0 solid #C9E0ED}#page{margin-top:0;margin-bottom:5px;background:#fff;border:0 solid #C9E0ED}#content{padding:20px}.centrado{display:block;margin-left:auto;margin-right:auto}hr{color:#f5f5f5}
					</style>
					<body>
						<div class="container" id="page">
							<div  id="header">
								<p><a  href="" target="_blank"><img class="centrado"  alt="The Factory HKA" src="http://thefactoryhka.com/extranet-pa/img/logos/logo.jpg" style="height:123px; width:287px" /></a></p>
							</div>
							<div id="content">
								<p>' . $body . '</p>
								<br />
								<p><b>NO RESPONDER ESTE EMAIL</b></p>
								<p><b>Atentamente</b></p>
								<p><b>"' . $fromName . '"</b></p>
								<p><b>RUC: 155596713-2-2015 DV: 59</b></p>
							</div>
						</div>
					</body>';
			$mailer->IsSMTP();
			$mailer->SMTPAuth = true;
			$mailer->Username = "contacto@thefactory.com.ve";
			$mailer->Mailer = "smtp-relay.gmail.com";
			$mailer->Port = 465;
			if($mailer->Send())
			{
				return true;
			}
			else
				return false;
		}
		else return true;
	}
}
