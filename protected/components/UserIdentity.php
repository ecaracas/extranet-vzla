<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	public function authenticate()
	{
		/* Valido que el usuario exista */
		Yii::import('application.modules.roles.models.RBACRoles');
		Yii::import('application.modules.roles.models.RBACUsuarios');
		
		$valido_usuario = RBACUsuarios::model()->find(array(
				'select' => 'count(*) as id',
				'condition' => 'nombreusuario = :username',
				'params' => array(':username' => $this -> username )));
		
		if($valido_usuario -> id == 1)
		{
			/* Si existe el usuario consulto el usuario con su password */
			
			$valido_clave = RBACUsuarios::model()->find(array(
					'select' => 'count(*) as id',
					'condition' => 'nombreusuario = :username AND contrasena = :password',
					'params' => array(
							':username' => $this -> username,
							':password' => $this -> password ) ));
			
			if($valido_clave -> id == 1)
			{
				
				$sql = new CDbCriteria();
				$sql -> params = array(
						':username' => $this -> username,
						':password' => $this -> password );
				$sql -> condition = 'nombreusuario = :username AND contrasena = :password';
				
				$model = RBACUsuarios::model()->find($sql);
				
				$sql -> with = array(
						'rol' );
				
				$consulto_usuario = RBACUsuarios::model()->find($sql);
				
				if($consulto_usuario -> estatus == 1)
				{
					
					$this->setState('id', $consulto_usuario->id);
					$this->setState('rol', $consulto_usuario->rol_id);
					$this->setState('dni_numero', $consulto_usuario ->dni_numero);
					$this->setState('photo', $consulto_usuario->file);
					$this->setState('nombreusuario', $consulto_usuario->nombreusuario);
					$this->setState('emailUser', $consulto_usuario->email);
					$this -> errorCode = self :: ERROR_NONE;
				}
				elseif($consulto_usuario -> estatus == 2)
				{
					/* Usuario Bloqueado */
					Yii::app() -> user->setFlash('danger', "Usuario Inactivo");
					$this -> errorCode = self :: ERROR_PASSWORD_INVALID;
				}
				elseif($consulto_usuario -> estatus == 3)
				{
					/* Usuario Suspendido */
					Yii::app() -> user->setFlash('danger', "Usuario pendiente por aprobar");
					$this -> errorCode = self :: ERROR_PASSWORD_INVALID;
				}
				elseif($consulto_usuario -> estatus == 4)
				{
					/* Usuario debe validar Email */
					Yii::app() -> user->setFlash('danger', "Usuario Suspendido");
					$this -> errorCode = self :: ERROR_PASSWORD_INVALID;
				}
				elseif($consulto_usuario -> estatus == 0)
				{
					/* Usuario debe validar Email */
					Yii::app() -> user->setFlash('danger', "Usuario esta a la espera de activacion");
					$this -> errorCode = self :: ERROR_PASSWORD_INVALID;
				}
			}
			else
			{
				/* CONTRASEÑA ERRADA */
				Yii::app() -> user->setFlash('danger', "Contraseña Errada");
				$this -> errorCode = self :: ERROR_PASSWORD_INVALID;
			}
		}
		else
		{
			/* NO EXISTE USUARIO' */
			Yii::app() -> user->setFlash('danger', "Usuario no existe");
			$this -> errorCode = self :: ERROR_PASSWORD_INVALID;
		}
		return $this -> errorCode;
	}
}
