<?php
class AuthRBAC extends CApplicationComponent
{
	public function init()
	{}
	
public function checkAccess($item = NULL) {

        if ($item != NULL) {

            $item_data = explode('_', $item);

            if (count($item_data) == 2) {

                $modulo = $item_data[0];
                $opcion = $item_data[1];

                $sql = new CDbCriteria();

                $sql->condition = "usuario_id = :usuario_id AND opcion_opcion = :opcion_opcion AND menu_modulo = :menu_modulo";

                $sql->params = array(':usuario_id' => Yii::app()->user->id, ':opcion_opcion' => $opcion, ':menu_modulo' => $modulo);
       

                $validacion = ViewRBACMenusUsuarios::model()->find($sql);

                if (count($validacion) == 1) {
                    return TRUE;
                } else {

                    return FALSE;
                }
                
            } else {

                return FALSE;
            }
        } else {

            return FALSE;
        }
    }
}
