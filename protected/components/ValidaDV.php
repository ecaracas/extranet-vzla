<?php
class ValidaDV extends CApplicationComponent
{
	public function init()
	{}
	
	public function multiplaCadena($cadena, $numero)
	{
		$i = 1;
		$valor = '';
		if($numero >= 1)
		{
			while($i <= $numero)
			{
				$valor .= (string)$cadena;
				$i++;
			}
			return $valor;
		}
		else return '';
	}
	
	public function calculateDV($rucClient)
	{
		$sw = false;
		$array = ['00'=> '00',
		'10' => '01',
		'11' => '02',
		'12' => '03',
		'13' => '04',
		'14' => '05',
		'15' => '06',
		'16' => '07',
		'17' => '08',
		'18' => '09',
		'19' => '01',
		'20' => '02',
		'21' => '03',
		'22' => '04',
		'23' => '07',
		'24' => '08',
		'25' => '09',
		'26' => '02',
		'27' => '03',
		'28' => '04',
		'29' => '05',
		'30' => '06',
		'31' => '07',
		'32' => '08',
		'33' => '09',
		'34' => '01',
		'35' => '02',
		'36' => '03',
		'37' => '04',
		'38' => '05',
		'39' => '06',
		'40' => '07',
		'41' => '08',
		'42' => '09',
		'43' => '01',
		'44' => '02',
		'45' => '03',
		'46' => '04',
		'47' => '05',
		'48' => '06',
		'49' => '07'
		];
		$rs = explode('-', $rucClient);
		$ructb = '';
		if(substr($rucClient, 0, 1) == 'E')
			$ructb = $this->multiplaCadena('0', (4 - strlen($rs[1]))) . '0000005' . '00' . '50' . $this->multiplaCadena('0', (3 - strlen($rs[1]))) . $rs[1] . $this->multiplaCadena('0', (5 - strlen($rs[2]))) . $rs[2];
		elseif($rs[1] == 'NT')
			$ructb = $this->multiplaCadena('0', (4 - strlen($rs[1]))) . '0000005' . $this->multiplaCadena('00', (2 - strlen(substr($rs[0], 0, (strlen($rs[0] - 2)))))) . substr($rs[0], 0, (strlen($rs[0] - 2))) . '43' . $this->multiplaCadena('0', 3 - strlen($rs[2])) . $rs[2] . $this->multiplaCadena('0', (5 - strlen($rs[3]))) . $rs[3];
		elseif(substr($rs[0], abs(strlen($rs[0]) - 2)) == 'AV')
			$ructb = $this->multiplaCadena('0', (4 - strlen($rs[1]))) . '0000005' . $this->multiplaCadena('00', (2 - strlen(substr($rs[0], 0, (strlen($rs[0] - 2)))))) . substr($rs[0], 0, (strlen($rs[0] - 2))) . '15' . $this->multiplaCadena('0', 3 - strlen($rs[1])) . $rs[1] . $this->multiplaCadena('0', (5 - strlen($rs[2]))) . $rs[2];
		elseif(substr($rs[0], abs(strlen($rs[0]) - 2)) == 'PI')
			$ructb = $this->multiplaCadena('0', (4 - strlen($rs[1]))) . '0000005' . $this->multiplaCadena('00', (2 - strlen(substr($rs[0], 0, (strlen($rs[0] - 2)))))) . substr($rs[0], 0, (strlen($rs[0] - 2))) . '79' . $this->multiplaCadena('0', 3 - strlen($rs[1])) . $rs[1] . $this->multiplaCadena('0', (5 - strlen($rs[2]))) . $rs[2];
		elseif($rs[0] == 'PE')
			$ructb = $this->multiplaCadena('0', (4 - strlen($rs[1]))) . '0000005' . '00' . '75' . $this->multiplaCadena('0', (3 - strlen($rs[1]))) . $rs[1] . $this->multiplaCadena('0', (5 - strlen($rs[2]))) . $rs[2];
		elseif(substr($rucClient, 0, 1) == 'N')
			$ructb = $this->multiplaCadena('0', (4 - strlen($rs[1]))) . '0000005' . '00' . '40' . $this->multiplaCadena('0', (3 - strlen($rs[1]))) . $rs[1] . $this->multiplaCadena('0', (5 - strlen($rs[2]))) . $rs[2];
		elseif(0 < strlen($rs[0]) && strlen($rs[0]) <= 2)
			$ructb = $this->multiplaCadena('0', (10 - strlen($rs[0]))) . '0000005' . $this->multiplaCadena('0', (2 - strlen($rs[0]))) . $rs[0] . '00' . $this->multiplaCadena('0', (3 - strlen($rs[1]))) . $rs[1] . $this->multiplaCadena('0', (5 - strlen($rs[2]))) . $rs[2];
		else
		{
			//RUC Jurídico
			$ructb = $this->multiplaCadena('0', (10 - strlen($rs[0]))) . $rs[0] . $this->multiplaCadena('0', (4 - strlen($rs[1]))) . $rs[1] . $this->multiplaCadena('0', (6 - strlen($rs[2]))) . $rs[2];
			$sw = $ructb[3] == '0' && $ructb[4] == '0' && $ructb[5] < '5';
		}
		//echo substr($ructb, 5, 2) . '<br/>';
		if($sw)
		{
			$ructb = substr($ructb, 0, (5 - strlen($ructb))) . $this->findValue(substr($ructb, 5, 2), $array, substr($ructb, 5, 2)) . substr($ructb, 7);
		}

		$dv1 = $this->digitDV($sw, $ructb);
		$dv2 = $this->digitDV($sw, $ructb . chr(48 + $dv1));
		$ret =  chr(48 + $dv1) . chr(48 + $dv2);
		return $ret;
}
	
	/**
	 * Metodo que devuelve el valor de key dado si existe sino devuelve el valor por defecto.
	 * @param unknown $key
	 * @param unknown $array
	 * @param unknown $default
	 */
	public function findValue($key, $array,$default)
	{
		$valor;
		if(array_key_exists($key, $array))
		{
			return $array[$key];
		}
		else return $default;
	}
	
	public function digitDV($sw, $ructb)
	{
		$j = 2;
		$nsuma = 0;
		$i = strlen($ructb);
		for ($i; $i > 0 ; $i--)
		{
			$c = substr($ructb, ($i - 1), 1);
			if($sw && $j == 12)
			{
				$sw = false;
				$j -= 1;
			}
			$nsuma += $j * (ord($c) - ord('0'));
			
			$j += 1;
		}
		$r = $nsuma % 11;
		if($r > 1)
			return 11 - $r;
		return 0;
	}
}