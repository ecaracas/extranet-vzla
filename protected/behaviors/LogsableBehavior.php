<?php

class LogsableBehavior extends CActiveRecordBehavior
{
    private $_oldattributes = array();
 
    public function afterSave($event)
    {
        include_once 'geolocalizacion/geoiploc.php'; 
        $ip = $_SERVER["REMOTE_ADDR"];
        if(Yii::app()->params['file_auditoria']== FALSE){
        if (!$this->Owner->isNewRecord) {
 
            // new attributes
            $newattributes = $this->Owner->getAttributes();
            $oldattributes = $this->getOldAttributes();
 
            // compare old and new
            foreach ($newattributes as $name => $value) {
                if (!empty($oldattributes)) {
                    $old = $oldattributes[$name];
                } else {
                    $old = '';
                }
 
                if ($value != $old) {
                    $changes = $name . ' ('.$old.') => ('.$value.'), ';
                    Yii::import('application.modules.logs.models.Logs');
                    $log=new Logs();
                    $log->user_id = Yii::app()->user->id;
                    $log->module =  get_class($this->Owner);
                    $log->country = getCountryFromIP($ip, " NamE");
                    $log->description=  'El usuario ' . Yii::app()->user->nombreusuario.' ha cambiado ' . $name;
                    $log->change = $changes;
                    $log->ip = $ip;
                    $log->date= date("Y-m-d H:i:s");
                    $log->device = htmlentities($_SERVER['HTTP_USER_AGENT']);
                    $log->save();
                } /*fin  if ($value != $old) */
            } /*fin foreach ($newattributes as $name => $value)*/
        } /*fin  if (!$this->Owner->isNewRecord)  */
        else {
                    $changes = $name . ' ('.$old.') => ('.$value.'), ';
                    Yii::import('application.modules.logs.models.Logs');
                    $log=new Logs();
                    $log->user_id = Yii::app()->user->id;
                    $log->module =  get_class($this->Owner);
                    $log->country = getCountryFromIP($ip, " NamE");
                    $log->description=  'El usuario ' . Yii::app()->user->nombreusuario.' ha cambiado ' . $name;
                    $log->change = $changes;
                    $log->ip = $ip;
                    $log->date= date("Y-m-d H:i:s");
                    $log->device = htmlentities($_SERVER['HTTP_USER_AGENT']);
                    $log->save();
        } /*find el else*/
        
        }/*fin del file_auditoria*/
        else{
                    $newattributes = $this->Owner->getAttributes();
                    $oldattributes = $this->getOldAttributes();
                    foreach ($newattributes as $name => $value) {
                       if (!empty($oldattributes)) {
                             $old = $oldattributes[$name];
                        } else {
                                $old = '';
                        }
                        if ($value != $old) {
                                $changes = $name . ' ('.$old.') => ('.$value.') ';              
                                $data = Yii::app()->user->id.",".get_class($this->Owner).",".getCountryFromIP($ip, " NamE").",".$changes.",".$ip.",".htmlentities($_SERVER['HTTP_USER_AGENT']).",".date("Y-m-d H:i:s");
                                $handle = fopen('logs.csv', "a+");                           
                                      fwrite($handle,$data.PHP_EOL);                                 
                                fclose($handle);                            
                        }
                    }           
        }        
          return parent::beforeSave($event);
    }/*fin de la funcion*/
 
    public function afterDelete($event)
    {       
                    $changes = $name . ' ('.$old.') => ('.$value.'), ';
                     Yii::import('application.modules.logs.models.Logs');
                    $log=new Logs();
                    $log->user_id = Yii::app()->user->id;
                    $log->module =        get_class($this->Owner);
                    $log->country = getCountryFromIP($ip, " NamE");
                    $log->description=  'El usuario ' . Yii::app()->user->nombreusuario.' ha eliminado ' . $name;
                    $log->ip = $ip;
                    $log->date= date("Y-m-d H:i:s");
                    $log->device = htmlentities($_SERVER['HTTP_USER_AGENT']);
                    $log->save();
    }
 
    public function afterFind($event)
    {
        // Save old values
        $this->setOldAttributes($this->Owner->getAttributes());
    }
 
    public function getOldAttributes()
    {
        return $this->_oldattributes;
    }
 
    public function setOldAttributes($value)
    {
        $this->_oldattributes=$value;
    }
    
}
